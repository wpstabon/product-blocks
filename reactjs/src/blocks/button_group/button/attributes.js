const attributes = {
	blockId: {
		type: 'string',
		default: '',
	},
	layout: {
		type: 'string',
		default: 'layout1',
	},

	/*==========================
      Text
  ==========================*/
	btnText: {
		type: 'string',
		default: '',
	},
	btntextEnable: {
		type: 'boolean',
		default: true,
	},
	btnTextTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: 16, unit: 'px' },
			height: { lg: 20, unit: 'px' },
			decoration: 'none',
			family: '',
			weight: 500,
		},
		style: [ { selector: '{{WOPB}} .wopb-button-link .wopb-button-text' } ],
	},
	btnTextColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-button-link .wopb-button-text { color:{{btnTextColor}} } ',
			},
		],
	},
	btnTextHover: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}}:hover .wopb-button-link .wopb-button-text, .editor-styles-wrapper {{WOPB}} .wopb-button-link:hover .wopb-button-text { color:{{btnTextHover}} }',
			},
		],
	},

	/*==========================
      Background Color
  ==========================*/
	btnSize: {
		type: 'string',
		default: 'custom',
		style: [
			{
				depends: [
					{ key: 'btnSize', condition: '==', value: 'custom' },
				],
			},
			{
				depends: [ { key: 'btnSize', condition: '==', value: 'xl' } ],
				selector: '{{WOPB}} .wopb-button-link { padding: 20px 40px }',
			},
			{
				depends: [ { key: 'btnSize', condition: '==', value: 'lg' } ],
				selector: '{{WOPB}} .wopb-button-link { padding: 10px 20px }',
			},
			{
				depends: [ { key: 'btnSize', condition: '==', value: 'md' } ],
				selector: '{{WOPB}} .wopb-button-link { padding: 8px 16px }',
			},
			{
				depends: [ { key: 'btnSize', condition: '==', value: 'sm' } ],
				selector: '{{WOPB}} .wopb-button-link { padding: 6px 12px }',
			},
		],
	},
	btnBgCustomSize: {
		type: 'object',
		default: {
			lg: {
				top: '14',
				bottom: '14',
				left: '28',
				right: '28',
				unit: 'px',
			},
		},
		style: [
			{
				depends: [
					{ key: 'btnSize', condition: '==', value: 'custom' },
				],
				selector:
					'{{WOPB}} .wopb-button-link { padding:{{btnBgCustomSize}}; }',
			},
		],
	},
	btnLink: {
		type: 'object',
		default: '',
	},
	btnLinkTarget: {
		type: 'string',
		default: '_self',
	},
	btnLinkNoFollow: {
		type: 'boolean',
		default: false,
	},
	btnLinkSponsored: {
		type: 'boolean',
		default: false,
	},
	btnLinkDownload: {
		type: 'boolean',
		default: false,
	},
	btnMargin: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-button-link { margin:{{btnMargin}}; }',
			},
		],
	},
	btnBgColor: {
		type: 'object',
		default: {
			openColor: 1,
			type: 'color',
			color: '#FF176B',
			gradient: {},
		},
		style: [
			{
				selector:
					'.wopb-anim-none {{WOPB}}.wp-block-product-blocks-button .wopb-button-link,.wopb-anim-style1 {{WOPB}}.wp-block-product-blocks-button .wopb-button-link, .wopb-anim-style2 {{WOPB}}.wp-block-product-blocks-button .wopb-button-link:before, .wopb-anim-style3 {{WOPB}}.wp-block-product-blocks-button .wopb-button-link, .wopb-anim-style4 {{WOPB}}.wp-block-product-blocks-button .wopb-button-link',
			},
		],
	},
	btnBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector:
					'.wp-block-product-blocks-button-group .wopb-button-wrapper {{WOPB}}.wp-block-product-blocks-button .wopb-button-link',
			},
		],
	},
	btnRadius: {
		type: 'object',
		default: {
			lg: { top: '0', bottom: '0', left: '0', right: '0', unit: 'px' },
			unit: 'px',
		},
		style: [
			{
				selector:
					'.wp-block-product-blocks-button-group .wopb-button-wrapper {{WOPB}}.wp-block-product-blocks-button .wopb-button-link { border-radius:{{btnRadius}}; }',
			},
		],
	},
	btnShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 3, right: 3, bottom: 0, left: 0 },
			color: '#FFB714',
		},
		style: [
			{
				selector:
					'.wp-block-product-blocks-button-group .wopb-button-wrapper {{WOPB}}.wp-block-product-blocks-button .wopb-button-link',
			},
		],
	},

	//Hover
	btnBgHover: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '', gradient: {} },
		style: [
			{
				selector:
					'.wopb-anim-none {{WOPB}}.wp-block-product-blocks-button .wopb-button-link:before, .wopb-anim-style1 {{WOPB}}.wp-block-product-blocks-button .wopb-button-link:before, .wopb-anim-style2 {{WOPB}}.wp-block-product-blocks-button .wopb-button-link, .wopb-anim-style3 {{WOPB}}.wp-block-product-blocks-button .wopb-button-link:before, .wopb-anim-style4 {{WOPB}}.wp-block-product-blocks-button .wopb-button-link:before',
			},
		],
	},
	btnHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector:
					'.wp-block-product-blocks-button-group .wopb-button-wrapper {{WOPB}}.wp-block-product-blocks-button .wopb-button-link:hover',
			},
		],
	},
	btnHoverRadius: {
		type: 'object',
		default: {
			lg: { top: '0', bottom: '0', left: '0', right: '0', unit: 'px' },
			unit: 'px',
		},
		style: [
			{
				selector:
					'.wp-block-product-blocks-button-group .wopb-button-wrapper {{WOPB}}.wp-block-product-blocks-button .wopb-button-link:hover { border-radius:{{btnHoverRadius}}; }',
			},
		],
	},
	btnHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 3, right: 3, bottom: 0, left: 0 },
			color: '#FFB714',
		},
		style: [
			{
				selector:
					'.wp-block-product-blocks-button-group .wopb-button-wrapper {{WOPB}}.wp-block-product-blocks-button .wopb-button-link:hover',
			},
		],
	},

	/*==========================
      Icon
  ==========================*/
	btnIconEnable: {
		type: 'boolean',
		default: false,
	},
	btnIcon: {
		type: 'string',
		default: 'arrow_right_circle_line',
	},
	btnIconPosition: {
		type: 'boolean',
		default: false,
		style: [
			{
				depends: [
					{ key: 'btnIconPosition', condition: '==', value: false },
				],
				selector: '{{WOPB}} .wopb-button-link { flex-direction: row }',
			},
			{
				depends: [
					{ key: 'btnIconPosition', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-button-link { flex-direction: row-reverse }',
			},
		],
	},
	btnIconSize: {
		type: 'object',
		default: { lg: '17', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-button-link .wopb-btnIcon-wrap svg { height: {{btnIconSize}}; width: {{btnIconSize}}; }',
			},
		],
	},
	btnIconGap: {
		type: 'object',
		default: { lg: '12', ulg: 'px' },
		style: [
			{ selector: '{{WOPB}} .wopb-button-link { gap: {{btnIconGap}}; }' },
		],
	},
	btnIconColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-button-link > .wopb-btnIcon-wrap svg { fill:{{btnIconColor}}; } ',
			},
		],
	},
	btnIconHoverColor: {
		type: 'string',
		default: '#f2f2f2',
		style: [
			{
				selector:
					'{{WOPB}}:hover .wopb-button-link > .wopb-btnIcon-wrap svg { fill:{{btnIconHoverColor}}; }',
			},
		],
	},

	/*==========================
      General Advance Settings
  ==========================*/
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [ { selector: 'div:has(> {{WOPB}}) {display:none;}' } ],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [ { selector: 'div:has(> {{WOPB}}) {display:none;}' } ],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [ { selector: 'div:has(> {{WOPB}}) {display:none;}' } ],
	},
	advanceCss: {
		type: 'string',
		default: '',
		style: [ { selector: '' } ],
	},
};
export default attributes;
