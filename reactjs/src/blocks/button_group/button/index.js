const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import Save from './Save';
import attributes from './attributes';

registerBlockType( 'product-blocks/button', {
	title: __( 'Button', 'product-blocks' ),
	parent: [ 'product-blocks/button-group' ],
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/button.svg' }
			alt="button"
		/>
	),
	category: 'product-blocks',
	description: __( 'Create & customize button', 'product-blocks' ),
	keywords: [
		__( 'button', 'product-blocks' ),
		__( 'single button', 'product-blocks' ),
		__( 'btn', 'product-blocks' ),
	],
	supports: {
		reusable: false,
		html: false,
	},
	edit: Edit,
	save: Save,
	attributes,
} );
