const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
const { Component, Fragment } = wp.element;
const { RichText } = wp.blockEditor;
const { createBlock } = wp.blocks;
import { CssGenerator } from '../../../helper/CssGenerator';
import SingleButtonSetting from './Setting';
import IconPack from '../../../helper/fields/tools/IconPack';
import ToolBarElement from '../../../helper/ToolBarElement';
class Edit extends Component {
	constructor() {
		super( ...arguments );
		this.state = {
			postsList: [],
			loading: true,
			error: false,
			section: 'Content',
		};
		this.setSection = this.setSection.bind( this );
	}

	setSection( title ) {
		const val = this.state.section == title ? '' : title;
		this.setState( { section: val } );
	}

	componentDidMount() {
		const {
			setAttributes,
			clientId,
			attributes: { blockId },
		} = this.props;
		const _client = clientId.substr( 0, 6 );
		if ( ! blockId ) {
			setAttributes( { blockId: _client } );
		} else if ( blockId && blockId != _client ) {
			setAttributes( { blockId: _client } );
		}
	}

	render() {
		const { setAttributes, name, attributes, clientId } = this.props;
		const store = {
			setAttributes,
			name,
			attributes,
			setSection: this.setSection,
			section: this.state.section,
			clientId,
		};
		const {
			blockId,
			advanceId,
			layout,
			btnIconEnable,
			btntextEnable,
			btnText,
			btnIcon,
			btnLink,
		} = attributes;

		if ( blockId ) {
			CssGenerator( attributes, 'product-blocks/button', blockId );
		}

		return (
			<Fragment>
				<InspectorControls>
					<SingleButtonSetting store={ store } />
				</InspectorControls>
				<ToolBarElement
					include={ [
						{
							type: 'layout',
							block: 'button',
							key: 'layout',
							label: __( 'Style', 'product-blocks' ),
							options: [
								{
									img: 'assets/img/blocks/button/layout1.svg',
									label: __( 'Layout 1', 'product-blocks' ),
									value: 'layout1',
								},
								{
									img: 'assets/img/blocks/button/layout2.svg',
									label: __( 'Layout 2', 'product-blocks' ),
									value: 'layout2',
								},
								{
									img: 'assets/img/blocks/button/layout3.svg',
									label: __( 'Layout 3', 'product-blocks' ),
									value: 'layout3',
								},
								{
									img: 'assets/img/blocks/button/layout4.svg',
									label: __( 'Layout 4', 'product-blocks' ),
									value: 'layout4',
								},
							],
						},
						{
							type: 'link',
							key: 'btnLink',
							label: __( 'Button Link', 'product-blocks' ),
						},
					] }
					store={ store }
				/>
				<div
					{ ...( advanceId && { id: advanceId } ) }
					className={ `wopb-block-${ blockId } ${ this.props.className } wopb-button-${ layout }` }
				>
					<a className={ `wopb-button-link` }>
						{ btnIconEnable && (
							<div className="wopb-btnIcon-wrap">
								{ IconPack[ btnIcon ] }
							</div>
						) }
						{ btntextEnable && (
							<RichText
								key="editable"
								tagName={ 'div' }
								className={ 'wopb-button-text' }
								keepPlaceholderOnFocus
								placeholder={ __(
									'Text Here…',
									'product-blocks'
								) }
								onChange={ ( value ) =>
									setAttributes( { btnText: value } )
								}
								onReplace={ (
									blocks,
									indexToSelect,
									initialPosition
								) =>
									wp.data
										.dispatch( 'core/block-editor' )
										.replaceBlocks(
											clientId,
											blocks,
											indexToSelect,
											initialPosition
										)
								}
								onSplit={ ( value, isAfterOriginal ) =>
									createBlock( 'product-blocks/button', {
										...attributes,
										btnText: value,
									} )
								}
								allowedFormats={ [] }
								value={ btnText }
							/>
						) }
					</a>
				</div>
			</Fragment>
		);
	}
}
export default Edit;
