import IconPack from '../../../helper/fields/tools/IconPack';

const { Component } = wp.element;

class Save extends Component {
	render() {
		const {
			blockId,
			advanceId,
			layout,
			btnIconEnable,
			btntextEnable,
			btnText,
			btnIcon,
			btnLink,
			btnLinkTarget,
			btnLinkNoFollow,
			btnLinkSponsored,
			btnLinkDownload,
		} = this.props.attributes;
		let linkRelation = 'noopener';
		linkRelation += btnLinkNoFollow ? ' nofollow' : '';
		linkRelation += btnLinkSponsored ? ' sponsored' : '';
		return (
			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } wopb-button-${ layout }` }
			>
				<a
					className={ `wopb-button-link` }
					{ ...( btnLink.url && {
						href: btnLink.url,
						target: btnLinkTarget,
					} ) }
					rel={ linkRelation }
					download={ btnLinkDownload && 'download' }
				>
					{ btnIconEnable && (
						<div className="wopb-btnIcon-wrap">
							{ IconPack[ btnIcon ] }
						</div>
					) }
					{ btntextEnable && (
						<div
							className="wopb-button-text"
							dangerouslySetInnerHTML={ { __html: btnText } }
						/>
					) }
				</a>
			</div>
		);
	}
}
export default Save;
