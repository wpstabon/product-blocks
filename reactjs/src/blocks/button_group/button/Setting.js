const { __ } = wp.i18n;
import {
	CommonSettings,
	CustomCssAdvanced,
	ResponsiveAdvanced,
} from '../../../helper/CommonPanel';
import { Section, Sections } from '../../../helper/Sections';

const SingleButtonSetting = ( { store } ) => {
	return (
		<Sections>
			<Section slug="button" title={ __( 'Button', 'product-blocks' ) }>
				<CommonSettings
					title={ `inline` }
					include={ [
						{
							position: 1,
							data: {
								type: 'layout',
								block: 'button',
								key: 'layout',
								label: __( 'Style', 'product-blocks' ),
								options: [
									{
										img: 'assets/img/blocks/button/layout1.svg',
										label: __(
											'Layout 1',
											'product-blocks'
										),
										value: 'layout1',
									},
									{
										img: 'assets/img/blocks/button/layout2.svg',
										label: __(
											'Layout 2',
											'product-blocks'
										),
										value: 'layout2',
									},
									{
										img: 'assets/img/blocks/button/layout3.svg',
										label: __(
											'Layout 3',
											'product-blocks'
										),
										value: 'layout3',
									},
									{
										img: 'assets/img/blocks/button/layout4.svg',
										label: __(
											'Layout 4',
											'product-blocks'
										),
										value: 'layout4',
									},
								],
								variation: {
									layout1: {
										btnBgColor: {
											openColor: 1,
											type: 'color',
											color: '#354CFF',
											gradient: {},
										},
									},
									layout2: {
										btnBgColor: {
											openColor: 0,
											type: 'color',
											color: '',
											gradient: {},
										},
									},
									layout3: {
										btnBgColor: {
											openColor: 1,
											type: 'color',
											color: '#fff',
											gradient: {},
										},
									},
									layout4: {
										btnBgColor: {
											openColor: 0,
											type: 'color',
											color: '',
											gradient: {},
										},
									},
								},
							},
						},
						{
							position: 2,
							data: {
								type: 'select',
								key: 'btnSize',
								label: __( 'Button Size', 'product-blocks' ),
								options: [
									{
										value: 'sm',
										label: __( 'Small', 'product-blocks' ),
									},
									{
										value: 'md',
										label: __( 'Medium', 'product-blocks' ),
									},
									{
										value: 'lg',
										label: __( 'Large', 'product-blocks' ),
									},
									{
										value: 'xl',
										label: __(
											'Extra Large',
											'product-blocks'
										),
									},
									{
										value: 'custom',
										label: __( 'Custom', 'product-blocks' ),
									},
								],
							},
						},
						{
							position: 3,
							data: {
								type: 'dimension',
								key: 'btnBgCustomSize',
								step: 1,
								unit: true,
								responsive: true,
								label: __(
									'Button Custom Size',
									'product-blocks'
								),
							},
						},
						{
							position: 4,
							data: {
								type: 'link',
								key: 'btnLink',
								label: __( 'Button Link', 'product-blocks' ),
							},
						},
					] }
					initialOpen={ true }
					store={ store }
				/>
				<CommonSettings
					title={ __( 'Text', 'product-blocks' ) }
					depend="btntextEnable"
					include={ [
						{
							position: 1,
							data: {
								type: 'typography',
								key: 'btnTextTypo',
								label: __(
									'Text Typography',
									'product-blocks'
								),
							},
						},
						{
							position: 2,
							data: {
								type: 'color',
								key: 'btnTextColor',
								label: __( 'Text Color', 'product-blocks' ),
							},
						},
						{
							position: 3,
							data: {
								type: 'color',
								key: 'btnTextHover',
								label: __(
									'Text Hover Color',
									'product-blocks'
								),
							},
						},
					] }
					initialOpen={ false }
					store={ store }
				/>
				<CommonSettings
					title={ __( 'Background', 'product-blocks' ) }
					include={ [
						{
							position: 1,
							data: {
								type: 'dimension',
								key: 'btnMargin',
								step: 1,
								unit: true,
								responsive: true,
								label: __( 'Margin', 'product-blocks' ),
							},
						},
						{
							position: 2,
							data: {
								type: 'tab',
								content: [
									{
										name: 'normal',
										title: __( 'Normal', 'product-blocks' ),
										options: [
											{
												type: 'color2',
												key: 'btnBgColor',
												label: __(
													'Background Color',
													'product-blocks'
												),
											},
											{
												type: 'border',
												key: 'btnBorder',
												label: __(
													'Border',
													'product-blocks'
												),
											},
											{
												type: 'dimension',
												key: 'btnRadius',
												step: 1,
												unit: true,
												responsive: true,
												label: __(
													'Border Radius',
													'product-blocks'
												),
											},
											{
												type: 'boxshadow',
												key: 'btnShadow',
												step: 1,
												unit: true,
												responsive: true,
												label: __(
													'Box shadow',
													'product-blocks'
												),
											},
										],
									},
									{
										name: 'hover',
										title: __( 'Hover', 'product-blocks' ),
										options: [
											{
												type: 'color2',
												key: 'btnBgHover',
												label: __(
													'Background Color',
													'product-blocks'
												),
											},
											{
												type: 'border',
												key: 'btnHoverBorder',
												label: __(
													'Border',
													'product-blocks'
												),
											},
											{
												type: 'dimension',
												key: 'btnHoverRadius',
												step: 1,
												unit: true,
												responsive: true,
												label: __(
													'Border Radius',
													'product-blocks'
												),
											},
											{
												type: 'boxshadow',
												key: 'btnHoverShadow',
												step: 1,
												unit: true,
												responsive: true,
												label: __(
													'Box shadow',
													'product-blocks'
												),
											},
										],
									},
								],
							},
						},
					] }
					initialOpen={ false }
					store={ store }
				/>
				<CommonSettings
					title={ __( 'Icon', 'product-blocks' ) }
					depend="btnIconEnable"
					include={ [
						{
							position: 1,
							data: {
								type: 'icon',
								key: 'btnIcon',
								label: __( 'Icon Store', 'product-blocks' ),
							},
						},
						{
							position: 2,
							data: {
								type: 'toggle',
								key: 'btnIconPosition',
								label: __(
									'Icon After Text',
									'product-blocks'
								),
							},
						},
						{
							position: 3,
							data: {
								type: 'range',
								key: 'btnIconSize',
								min: 0,
								max: 300,
								step: 1,
								responsive: true,
								unit: true,
								label: __( 'Icon Size', 'product-blocks' ),
							},
						},
						{
							position: 4,
							data: {
								type: 'range',
								key: 'btnIconGap',
								min: 0,
								max: 300,
								step: 1,
								responsive: true,
								label: __(
									'Space Between Icon and Text',
									'product-blocks'
								),
							},
						},
						{
							position: 5,
							data: {
								type: 'tab',
								content: [
									{
										name: 'normal',
										title: __( 'Normal', 'product-blocks' ),
										options: [
											{
												type: 'color',
												key: 'btnIconColor',
												label: __(
													'Icon Color',
													'product-blocks'
												),
											},
										],
									},
									{
										name: 'hover',
										title: __( 'Hover', 'product-blocks' ),
										options: [
											{
												type: 'color',
												key: 'btnIconHoverColor',
												label: __(
													'Icon Color',
													'product-blocks'
												),
											},
										],
									},
								],
							},
						},
					] }
					initialOpen={ false }
					store={ store }
				/>
			</Section>
			<Section
				slug="advanced"
				title={ __( 'Advanced', 'product-blocks' ) }
			>
				<ResponsiveAdvanced store={ store } />
				<CustomCssAdvanced store={ store } />
			</Section>
		</Sections>
	);
};

export default SingleButtonSetting;
