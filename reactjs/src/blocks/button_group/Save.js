const { InnerBlocks } = wp.blockEditor;

export default function Save( props ) {
	const { blockId, advanceId, btnAnimation } = props.attributes;

	return (
		<div
			{ ...( advanceId && { id: advanceId } ) }
			className={ `wopb-block-${ blockId }` }
		>
			<div
				className={ `wopb-block-wrapper wopb-button-wrapper wopb-anim-${ btnAnimation }` }
			>
				<InnerBlocks.Content />
			</div>
		</div>
	);
}
