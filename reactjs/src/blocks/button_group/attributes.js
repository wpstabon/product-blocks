const attributes = {
	blockId: {
		type: 'string',
		default: '',
	},
	currentPostId: { type: 'string', default: '' },
	previewImg: {
		type: 'string',
		default: '',
	},
	initPremade: {
		type: 'boolean',
		default: false,
	},

	/*==========================
      Global Style
  ==========================*/
	btnJustifyAlignment: {
		type: 'string',
		default: 'flex-start',
		style: [
			{
				depends: [
					{
						key: 'btnVeticalPosition',
						condition: '==',
						value: false,
					},
				],
				selector:
					'{{WOPB}} .wopb-button-wrapper .block-editor-block-list__layout, {{WOPB}} .wopb-button-wrapper:has( > .wp-block-product-blocks-button) { justify-content: {{btnJustifyAlignment}}; }',
			},
			{
				depends: [
					{ key: 'btnVeticalPosition', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-button-wrapper .block-editor-inner-blocks .block-editor-block-list__layout, {{WOPB}} .wopb-button-wrapper:has( > .wp-block-product-blocks-button) { align-items: {{btnJustifyAlignment}}; }',
			},
		],
	},
	btnVeticalAlignment: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [
					{
						key: 'btnVeticalPosition',
						condition: '==',
						value: false,
					},
				],
				selector:
					'{{WOPB}} .wopb-button-wrapper .block-editor-inner-blocks .block-editor-block-list__layout, {{WOPB}} .wopb-button-wrapper:has( > .wp-block-product-blocks-button) { align-items: {{btnVeticalAlignment}}; } {{WOPB}} .wopb-button-wrapper .wp-block-product-blocks-button { height: 100%; }',
			},
		],
	},
	btnVeticalPosition: {
		type: 'boolean',
		default: false,
		style: [
			{
				depends: [
					{ key: 'btnVeticalPosition', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-button-wrapper .block-editor-inner-blocks .block-editor-block-list__layout, {{WOPB}} .wopb-button-wrapper:has( > .wp-block-product-blocks-button) { flex-direction: column; }',
			},
			{
				depends: [
					{
						key: 'btnVeticalPosition',
						condition: '==',
						value: false,
					},
				],
				selector:
					'{{WOPB}} .wopb-button-wrapper .block-editor-inner-blocks .block-editor-block-list__layout, {{WOPB}} .wopb-button-wrapper:has( > .wp-block-product-blocks-button) { flex-direction: row; }',
			},
		],
	},
	btnItemSpace: {
		type: 'object',
		default: { lg: '28', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-button-wrapper .block-editor-inner-blocks .block-editor-block-list__layout, {{WOPB}} .wopb-button-wrapper:has( > .wp-block-product-blocks-button) { gap:{{btnItemSpace}};  }',
			},
		],
	},
	btnWrapMargin: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-button-wrapper .block-editor-inner-blocks .block-editor-block-list__layout, {{WOPB}} .wopb-button-wrapper:has( > .wp-block-product-blocks-button) { margin:{{btnWrapMargin}};  }',
			},
		],
	},

	/*==========================
      Title Animation
  ==========================*/
	btnAnimation: {
		type: 'string',
		default: 'none',
		style: [
			{
				depends: [
					{ key: 'btnAnimation', condition: '!=', value: 'style3' },
				],
			},
			{
				depends: [
					{ key: 'btnAnimation', condition: '==', value: 'style3' },
				],
				selector:
					'{{WOPB}} .wopb-button-wrapper.wopb-anim-style3 .wp-block-product-blocks-button:hover { box-shadow: none; }',
			},
		],
	},
	btnTranslate: {
		type: 'object',
		default: { lg: '-5', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'btnAnimation', condition: '==', value: 'style4' },
				],
				selector:
					'{{WOPB}} .wp-block-product-blocks-button:hover { transform: translate( {{btnTranslate}}, {{btnTranslate}});  }',
			},
		],
	},
	/*==========================
      General Advance Settings
  ==========================*/
	wrapBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#f5f5f5' },
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#ff176b' },
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapMargin: {
		type: 'object',
		default: { lg: { top: '', bottom: '', unit: 'px' } },
		style: [ { selector: '{{WOPB}} { margin:{{wrapMargin}}; }' } ],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [ { selector: '{{WOPB}} {display:none;}' } ],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [ { selector: '{{WOPB}} {display:none;}' } ],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [ { selector: '{{WOPB}} {display:none;}' } ],
	},
	advanceCss: {
		type: 'string',
		default: '',
		style: [ { selector: '' } ],
	},
};
export default attributes;
