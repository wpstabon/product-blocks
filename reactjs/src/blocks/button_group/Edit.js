const { __ } = wp.i18n;
const { InspectorControls, InnerBlocks } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import ButtonsSetting from './Setting';
import ToolBarElement from '../../helper/ToolBarElement';
import useBlockId from '../../helper/hooks/use-block-id';

const { useState } = wp.element;

export default function Edit( props ) {
	const [ section, setSection ] = useState( 'Layout' );

	const { setAttributes, name, attributes, clientId, className } = props;

	const { blockId, advanceId, btnAnimation, previewImg } = attributes;

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/button-group', blockId );
	}

	const store = {
		setAttributes,
		name,
		attributes,
		setSection,
		section,
		clientId,
	};

	if ( previewImg ) {
		return (
			<img
				style={ { marginTop: '0px', width: '420px' } }
				src={ previewImg }
			/>
		);
	}

	return (
		<>
			<InspectorControls>
				<ButtonsSetting store={ store } />
			</InspectorControls>
			<ToolBarElement
				include={ [
					{
						type: 'template',
					},
				] }
				store={ store }
			/>
			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div
					className={ `wopb-block-wrapper wopb-button-wrapper${
						btnAnimation ? ' wopb-anim-' + btnAnimation : ''
					}` }
				>
					<InnerBlocks
						template={ [ [ 'product-blocks/button', {} ] ] }
						allowedBlocks={ [ 'product-blocks/button' ] }
					/>
				</div>
			</div>
		</>
	);
}
