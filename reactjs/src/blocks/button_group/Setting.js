const { __ } = wp.i18n;
import {
	CommonSettings,
	CustomCssAdvanced,
	GeneralAdvanced,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Section, Sections } from '../../helper/Sections';
import TemplateModal from '../../helper/TemplateModal';

const ButtonsSetting = ( { store } ) => {
	const { btnAnimation, btnVeticalPosition } = store.attributes;

	let helpMessage = '';
	switch ( btnAnimation ) {
		case 'style1':
			helpMessage = 'Control The Background From Button Background';
			break;
		case 'style2':
			helpMessage = 'Control The Background From Button Background';
			break;
		case 'style3':
			helpMessage = 'Control The Box-shadow From Button Background';
			break;
		case 'style4':
			helpMessage = 'Control The Box-shadow From Button Background';
			break;
	}

	let verticalIcon = [
		'juststart',
		'justcenter',
		'justend',
		'justbetween',
		'justaround',
		'justevenly',
	];

	if ( btnVeticalPosition ) {
		verticalIcon = [ 'juststart', 'justcenter', 'justend' ];
	}

	return (
		<>
			<TemplateModal
				prev="https://www.wpxpo.com/wowstore/blocks/#demoid2093"
				store={ store }
			/>
			<Sections>
				<Section
					slug="global"
					title={ __( 'Global Style', 'product-blocks' ) }
				>
					<CommonSettings
						title={ `inline` }
						include={ [
							{
								position: 1,
								data: {
									type: 'range',
									key: 'btnItemSpace',
									min: 0,
									max: 300,
									step: 1,
									responsive: true,
									label: __( 'Button Gap', 'product-blocks' ),
								},
							},
							{
								position: 2,
								data: {
									type: 'alignment',
									key: 'btnJustifyAlignment',
									icons: verticalIcon,
									options: [
										'flex-start',
										'center',
										'flex-end',
										'space-between',
										'space-around',
										'space-evenly',
									],
									label: __(
										'Horizontal Alignment',
										'product-blocks'
									),
								},
							},
							{
								position: 3,
								data: {
									type: 'alignment',
									block: 'button',
									key: 'btnVeticalAlignment',
									disableJustify: true,
									icons: [
										'algnStart',
										'algnCenter',
										'algnEnd',
										'stretch',
									],
									options: [
										'flex-start',
										'center',
										'flex-end',
										'stretch',
									],
									label: __(
										'Vertical Alignment',
										'product-blocks'
									),
								},
							},
							{
								position: 4,
								data: {
									type: 'toggle',
									key: 'btnVeticalPosition',
									label: __(
										'Vertical View',
										'product-blocks'
									),
								},
							},
							{
								position: 5,
								data: {
									type: 'dimension',
									key: 'btnWrapMargin',
									step: 1,
									unit: true,
									responsive: true,
									label: __( 'Margin', 'product-blocks' ),
								},
							},
						] }
						initialOpen={ true }
						store={ store }
					/>
					<CommonSettings
						title={ __( 'Button Animation', 'product-blocks' ) }
						include={ [
							{
								position: 1,
								data: {
									type: 'select',
									key: 'btnAnimation',
									label: __(
										'Button Animation',
										'product-blocks'
									),
									options: [
										{
											value: 'none',
											label: __(
												'None',
												'product-blocks'
											),
										},
										{
											value: 'style1',
											label: __(
												'Style 1',
												'product-blocks'
											),
										},
										{
											value: 'style2',
											label: __(
												'Style 2',
												'product-blocks'
											),
										},
										{
											value: 'style3',
											label: __(
												'Style 3',
												'product-blocks'
											),
										},
										{
											value: 'style4',
											label: __(
												'Style 4',
												'product-blocks'
											),
										},
									],
									help: helpMessage,
								},
							},
							{
								position: 2,
								data: {
									type: 'range',
									key: 'btnTranslate',
									min: -20,
									max: 20,
									step: 0.5,
									responsive: true,
									label: __(
										'Button Transform',
										'product-blocks'
									),
									unit: [ 'px' ],
									help: 'Control Button Position',
								},
							},
						] }
						initialOpen={ false }
						store={ store }
					/>
				</Section>
				<Section
					slug="advanced"
					title={ __( 'Advanced', 'product-blocks' ) }
				>
					<GeneralAdvanced initialOpen={ true } store={ store } />
					<ResponsiveAdvanced store={ store } />
					<CustomCssAdvanced store={ store } />
				</Section>
			</Sections>
			{ wopbSupport() }
		</>
	);
};

export default ButtonsSetting;
