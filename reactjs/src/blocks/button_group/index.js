const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import Save from './Save';
import attributes from './attributes';

registerBlockType( 'product-blocks/button-group', {
	title: __( 'Button Group', 'product-blocks' ),
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/button-group.svg' }
			alt="button group"
		/>
	),
	category: 'product-blocks',
	description: (
		<span className="wopb-block-info">
			{ __(
				'Create & customize multiple button-styles',
				'product-blocks'
			) }
			<a
				target="_blank"
				href="https://wpxpo.com/docs/wowstore/all-blocks/button-group-block/"
				rel="noreferrer"
			>
				{ __( 'Documentation', 'product-blocks' ) }
			</a>
		</span>
	),
	keywords: [
		__( 'button', 'product-blocks' ),
		__( 'button group', 'product-blocks' ),
		__( 'btn', 'product-blocks' ),
	],
	supports: {
		align: [ 'wide', 'full' ],
		html: false,
		reusable: false,
	},
	example: {
		attributes: {
			previewImg: wopb_data.url + 'assets/img/preview/button.svg',
		},
	},
	edit: Edit,
	save: Save,
	attributes,
} );
