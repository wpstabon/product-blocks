const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Save from './Save';
import Edit from './Edit';
registerBlockType( 'product-blocks/row', {
	title: __( 'Row', 'product-blocks' ),
	icon: <img src={ wopb_data.url + 'assets/img/blocks/row.svg' } />,
	category: 'product-blocks',
	description: __( 'Add Row block for your layout.', 'product-blocks' ),
	keywords: [
		__( 'row', 'product-blocks' ),
		__( 'wrap', 'product-blocks' ),
		__( 'column', 'product-blocks' ),
	],
	supports: {
		align: [ 'center', 'wide', 'full' ],
		html: false,
		reusable: false,
	},
	example: {
		attributes: {
			previewImg: wopb_data.url + 'assets/img/preview/wrapper.svg',
		},
	},
	edit: Edit,
	save: Save,
	attributes: {
		blockId: {
			type: 'string',
			default: '',
		},
		currentPostId: { type: 'string', default: '' },
		previewImg: {
			type: 'string',
			default: '',
		},
		/*==========================
                Column Style
            ==========================*/
		layout: {
			type: 'object',
			default: {},
		},
		columnGap: {
			type: 'object',
			default: { lg: '20', sm: '10', xs: '5' },
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, {{WOPB}} > .wopb-row-wrapper > .wopb-row-content { column-gap: {{columnGap}}px;}',
				},
			],
		},
		rowGap: {
			type: 'object',
			default: { lg: '20' },
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, {{WOPB}} > .wopb-row-wrapper > .wopb-row-content { row-gap: {{rowGap}}px }',
				},
			],
		},
		inheritThemeWidth: {
			type: 'boolean',
			default: false,
		},
		rowContentWidth: {
			type: 'object',
			default: { lg: '1140', ulg: 'px' },
			style: [
				{
					depends: [
						{
							key: 'inheritThemeWidth',
							condition: '==',
							value: false,
						},
					],
					selector:
						' {{WOPB}} > .wopb-row-wrapper  > .block-editor-inner-blocks > .block-editor-block-list__layout, {{WOPB}} > .wopb-row-wrapper > .wopb-row-content { max-width: {{rowContentWidth}}; margin-left: auto !important; margin-right: auto !important;}',
				},
			],
		},
		contentHeightType: {
			type: 'string',
			default: 'normal',
			style: [
				{
					depends: [
						{
							key: 'contentHeightType',
							condition: '==',
							value: 'custom',
						},
					],
				},
				{
					depends: [
						{
							key: 'contentHeightType',
							condition: '==',
							value: 'normal',
						},
					],
				},
				{
					depends: [
						{
							key: 'contentHeightType',
							condition: '==',
							value: 'windowHeight',
						},
					],
					selector:
						'{{WOPB}} > .wopb-row-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, {{WOPB}} > .wopb-row-wrapper > .wopb-row-content { height: 100vh; }',
				},
			],
		},
		contentHeight: {
			type: 'object',
			default: { lg: '100', ulg: 'px' },
			style: [
				{
					depends: [
						{
							key: 'contentHeightType',
							condition: '==',
							value: 'custom',
						},
					],
					selector:
						'{{WOPB}} > .wopb-row-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout , {{WOPB}} > .wopb-row-wrapper > .wopb-row-content { min-height: {{contentHeight}} }',
				},
			],
		},
		contentOverflow: {
			type: 'string',
			default: 'visible',
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout,  {{WOPB}} > .wopb-row-wrapper > .wopb-row-content { overflow: {{contentOverflow}} }',
				},
			],
		},
		HtmlTag: {
			type: 'string',
			default: 'div',
		},
		enableRowLink: {
			type: 'boolean',
			default: false,
		},
		rowWrapperLink: {
			type: 'string',
			default: '',
			style: [
				{
					depends: [
						{ key: 'enableRowLink', condition: '==', value: true },
					],
				},
			],
		},
		enableNewTab: {
			type: 'boolean',
			default: false,
			style: [
				{
					depends: [
						{ key: 'enableRowLink', condition: '==', value: true },
					],
				},
			],
		},

		/*==========================
                Flex Properties
            ==========================*/
		reverseEle: {
			type: 'boolean',
			default: false,
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, {{WOPB}} > .wopb-row-wrapper > .wopb-row-content { flex-direction: row; }',
				},
				{
					depends: [
						{ key: 'reverseEle', condition: '==', value: true },
					],
					selector:
						'{{WOPB}} > .wopb-row-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, {{WOPB}} > .wopb-row-wrapper > .wopb-row-content { flex-direction: row-reverse; }',
				},
			],
		},
		ColumnAlign: {
			type: 'string',
			default: '',
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout:has( > .block-editor-block-list__block > .wp-block-product-blocks-column), {{WOPB}} > .wopb-row-wrapper > .wopb-row-content { align-items: {{ColumnAlign}} }',
				},
			],
		},

		/*==========================
                Background & Wrapper
            ==========================*/
		rowBgImg: {
			type: 'object',
			default: {
				openColor: 0,
				type: 'color',
				color: '#f5f5f5',
				size: 'cover',
				repeat: 'no-repeat',
				loop: true,
			},
			style: [
				{
					selector: '{{WOPB}} > .wopb-row-wrapper',
				},
			],
		},

		/*========== Hover Background & Wrapper =========*/
		rowWrapHoverImg: {
			type: 'object',
			default: {
				openColor: 0,
				type: 'color',
				color: '#f5f5f5',
				size: 'cover',
				repeat: 'no-repeat',
			},
			style: [
				{
					selector: '{{WOPB}} > .wopb-row-wrapper:hover',
				},
			],
		},

		/*==========================
                Overlay Background
            ==========================*/
		rowOverlayBgImg: {
			type: 'object',
			default: {
				openColor: 0,
				type: 'color',
				color: '#f5f5f5',
				size: 'cover',
				repeat: 'no-repeat',
			},
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper > .wopb-row-overlay',
				},
			],
		},
		rowOverlayOpacity: {
			type: 'string',
			default: '50',
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper > .wopb-row-overlay { opacity:{{rowOverlayOpacity}}%; }',
				},
			],
		},
		rowOverlayBgFilter: {
			type: 'object',
			default: {
				openFilter: 0,
				hue: 0,
				saturation: 100,
				brightness: 100,
				contrast: 100,
				invert: 0,
				blur: 0,
			},
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper > .wopb-row-overlay { {{rowOverlayBgFilter}} }',
				},
			],
		},
		rowOverlayBgBlend: {
			type: 'string',
			default: '',
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper > .wopb-row-overlay { mix-blend-mode:{{rowOverlayBgBlend}}; }',
				},
			],
		},
		/*========   Overlay Hover   =======*/
		rowOverlaypHoverImg: {
			type: 'object',
			default: {
				openColor: 0,
				type: 'color',
				color: '#f5f5f5',
				size: 'cover',
				repeat: 'no-repeat',
			},
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper:hover > .wopb-row-overlay',
				},
			],
		},
		rowOverlayHovOpacity: {
			type: 'string',
			default: '50',
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper:hover > .wopb-row-overlay { opacity:{{rowOverlayHovOpacity}}% }',
				},
			],
		},
		rowOverlayHoverFilter: {
			type: 'object',
			default: {
				openFilter: 0,
				hue: 0,
				saturation: 100,
				brightness: 100,
				contrast: 100,
				invert: 0,
				blur: 0,
			},
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper:hover > .wopb-row-overlay { {{rowOverlayHoverFilter}} }',
				},
			],
		},
		rowOverlayHoverBgBlend: {
			type: 'string',
			default: '',
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper:hover > .wopb-row-overlay { mix-blend-mode:{{rowOverlayHoverBgBlend}}; }',
				},
			],
		},

		/*==========================
                Spacing % Border Style
            ==========================*/
		rowWrapMargin: {
			type: 'object',
			default: { lg: { top: '', bottom: '', unit: 'px' } },
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper { margin:{{rowWrapMargin}}; }',
				},
			],
		},
		rowWrapPadding: {
			type: 'object',
			default: { lg: { top: '15', bottom: '15', unit: 'px' } },
			style: [
				{
					selector:
						'{{WOPB}}.wp-block-product-blocks-row > .wopb-row-wrapper:not(:has( > .components-resizable-box__container)), {{WOPB}}.wp-block-product-blocks-row > .wopb-row-wrapper:has( > .components-resizable-box__container) > .block-editor-inner-blocks {padding: {{rowWrapPadding}}; }',
				},
			],
		},

		/*===== Border Style ======*/
		rowWrapBorder: {
			type: 'object',
			default: {
				openBorder: 0,
				width: { top: 2, right: 2, bottom: 2, left: 2 },
				color: '#dfdfdf',
			},
			style: [
				{
					selector: '{{WOPB}} > .wopb-row-wrapper',
				},
			],
		},
		rowWrapRadius: {
			type: 'object',
			default: {
				openBorder: 0,
				width: { top: 1, right: 1, bottom: 1, left: 1 },
				color: '#009fd4',
				type: 'solid',
			},
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper { border-radius: {{rowWrapRadius}};} {{WOPB}} > .wopb-row-wrapper > .wopb-row-overlay { border-radius: {{rowWrapRadius}};}',
				},
			],
		},
		rowWrapShadow: {
			type: 'object',
			default: {
				openShadow: 0,
				width: { top: 1, right: 1, bottom: 1, left: 1 },
				color: '#009fd4',
			},
			style: [ { selector: '{{WOPB}} > .wopb-row-wrapper' } ],
		},

		/*===== Border Hover Style ======*/
		rowWrapHoverBorder: {
			type: 'object',
			default: {
				openBorder: 0,
				width: { top: 1, right: 1, bottom: 1, left: 1 },
				color: '#009fd4',
			},
			style: [
				{
					selector: '{{WOPB}} > .wopb-row-wrapper:hover',
				},
			],
		},
		rowWrapHoverRadius: {
			type: 'object',
			default: {
				openBorder: 0,
				width: { top: 1, right: 1, bottom: 1, left: 1 },
				color: '#009fd4',
				type: 'solid',
			},
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper:hover, {{WOPB}} > .wopb-row-wrapper:hover > .wopb-row-overlay { border-radius: {{rowWrapHoverRadius}};}',
				},
			],
		},
		rowWrapHoverShadow: {
			type: 'object',
			default: {
				openShadow: 0,
				width: { top: 1, right: 1, bottom: 1, left: 1 },
				color: '#009fd4',
			},
			style: [ { selector: '{{WOPB}} > .wopb-row-wrapper:hover' } ],
		},

		/*==========================
                Shape Style
            ==========================*/
		/*===== Top Shape Style ======*/
		rowTopShape: {
			type: 'string',
			default: 'empty',
		},
		rowTopGradientColor: {
			type: 'object',
			default: { openColor: 1, type: 'color', color: '#CCCCCC' },
		},
		rowTopShapeWidth: {
			type: 'object',
			default: { lg: '' },
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper > .wopb-shape-top > svg { width:calc({{rowTopShapeWidth}}% + 1.3px); } ',
				},
			],
		},
		rowTopShapeHeight: {
			type: 'object',
			default: { lg: '' },
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper > .wopb-shape-top > svg { height:{{rowTopShapeHeight}}px; } ',
				},
			],
		},
		rowTopShapeFlip: {
			type: 'boolean',
			default: false,
			style: [
				{
					depends: [
						{
							key: 'rowTopShapeFlip',
							condition: '==',
							value: false,
						},
					],
				},
				{
					depends: [
						{
							key: 'rowTopShapeFlip',
							condition: '==',
							value: true,
						},
					],
					selector:
						'{{WOPB}} > .wopb-row-wrapper > .wopb-shape-top > svg { transform:rotateY(180deg); }',
				},
			],
		},
		rowTopShapeFront: {
			type: 'boolean',
			default: false,
			style: [
				{
					depends: [
						{
							key: 'rowTopShapeFront',
							condition: '==',
							value: false,
						},
					],
				},
				{
					depends: [
						{
							key: 'rowTopShapeFront',
							condition: '==',
							value: true,
						},
					],
					selector:
						'{{WOPB}} > .wopb-row-wrapper > .wopb-shape-top { z-index: 1; }',
				},
			],
		},
		rowTopShapeOffset: {
			type: 'object',
			default: { lg: '' },
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper > .wopb-shape-top { top: {{rowTopShapeOffset}}px !important; }',
				},
			],
		},

		/*===== Bottom Shape Style ======*/
		rowBtmShape: {
			type: 'string',
			default: 'empty',
		},
		rowBtmGradientColor: {
			type: 'object',
			default: { openColor: 1, type: 'color', color: '#CCCCCC' },
		},
		rowBtmShapeWidth: {
			type: 'object',
			default: { lg: '' },
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper > .wopb-shape-bottom > svg, {{WOPB}} > .wopb-shape-bottom > svg { width: calc({{rowBtmShapeWidth}}% + 1.3px); }',
				},
			],
		},
		rowBtmShapeHeight: {
			type: 'object',
			default: { lg: '' },
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper > .wopb-shape-bottom > svg { height: {{rowBtmShapeHeight}}px; }',
				},
			],
		},
		rowBtmShapeFlip: {
			type: 'boolean',
			default: false,
			style: [
				{
					depends: [
						{
							key: 'rowBtmShapeFlip',
							condition: '==',
							value: false,
						},
					],
				},
				{
					depends: [
						{
							key: 'rowBtmShapeFlip',
							condition: '==',
							value: true,
						},
					],
					selector:
						'{{WOPB}} > .wopb-row-wrapper > .wopb-shape-bottom > svg { transform: rotateY(180deg) rotate(180deg); }',
				},
			],
		},
		rowBtmShapeFront: {
			type: 'boolean',
			default: false,
			style: [
				{
					depends: [
						{
							key: 'rowBtmShapeFront',
							condition: '==',
							value: false,
						},
					],
				},
				{
					depends: [
						{
							key: 'rowBtmShapeFront',
							condition: '==',
							value: true,
						},
					],
					selector:
						'{{WOPB}} > .wopb-row-wrapper > .wopb-shape-bottom { z-index: 1; }',
				},
			],
		},
		rowBtmShapeOffset: {
			type: 'object',
			default: { lg: '' },
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper > .wopb-shape-bottom { bottom: {{rowBtmShapeOffset}}px !important; }',
				},
			],
		},

		/*==========================
                Row Color
            ==========================*/
		rowColor: {
			type: 'string',
			default: '',
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper { color:{{rowColor}} } ',
				},
			],
		},
		rowLinkColor: {
			type: 'string',
			default: '',
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-row-wrapper a { color:{{rowLinkColor}} }',
				},
			],
		},
		rowLinkHover: {
			type: 'string',
			default: '',
			style: [
				{
					selector:
						' {{WOPB}} > .wopb-row-wrapper a:hover { color:{{rowLinkHover}}; }',
				},
			],
		},
		rowTypo: {
			type: 'object',
			default: {
				openTypography: 0,
				size: { lg: 16, unit: 'px' },
				height: { lg: 20, unit: 'px' },
				decoration: 'none',
				family: '',
				weight: 700,
			},
			style: [ { selector: '{{WOPB}} > .wopb-row-wrapper' } ],
		},

		/*==========================
                Advanced Setting
            ==========================*/
		advanceId: {
			type: 'string',
			default: '',
		},
		advanceZindex: {
			type: 'string',
			default: '',
			style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
		},
		hideExtraLarge: {
			type: 'boolean',
			default: false,
			style: [ { selector: '{{WOPB}} {display:none;}' } ],
		},
		hideDesktop: {
			type: 'boolean',
			default: false,
			style: [
				{
					selector:
						'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
				},
			],
		},
		hideTablet: {
			type: 'boolean',
			default: false,
			style: [ { selector: '{{WOPB}} {display:none;}' } ],
		},
		hideMobile: {
			type: 'boolean',
			default: false,
			style: [ { selector: '{{WOPB}} {display:none;}' } ],
		},
		advanceCss: {
			type: 'string',
			default: '',
			style: [ { selector: '' } ],
		},
	},
} );
