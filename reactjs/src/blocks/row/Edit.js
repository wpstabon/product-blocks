const { __ } = wp.i18n;
const { InspectorControls, InnerBlocks } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import {
	CustomCssAdvanced,
	ResponsiveAdvanced,
	CommonSettings,
	bgVideoBg,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Sections, Section } from '../../helper/Sections';
import RowSettings from './Setting';
import { renderShapeIcons } from './shapeIcons';
import { Resize } from './Resize';
import useDevice from '../../helper/hooks/use-device';
import useBlockId from '../../helper/hooks/use-block-id';
const { Tooltip } = wp.components;
const { serialize, parse } = wp.blocks;
const { getBlocksByClientId } = wp.data.select( 'core/block-editor' );
const { removeBlocks, updateBlockAttributes, insertBlocks } =
	wp.data.dispatch( 'core/block-editor' );

const layoutPosition = [
	{ lg: [ 100 ], sm: [ 100 ], xs: [ 100 ] },
	{ lg: [ 50, 50 ], sm: [ 50, 50 ], xs: [ 100, 100 ] },
	{ lg: [ 70, 30 ], sm: [ 50, 50 ], xs: [ 100, 100 ] },
	{ lg: [ 30, 70 ], sm: [ 50, 50 ], xs: [ 100, 100 ] },
	{
		lg: [ 33.33, 33.33, 33.34 ],
		sm: [ 33.33, 33.33, 33.34 ],
		xs: [ 100, 100, 100 ],
	},
	{ lg: [ 25, 25, 50 ], sm: [ 25, 25, 50 ], xs: [ 100, 100, 100 ] },
	{ lg: [ 50, 25, 25 ], sm: [ 50, 25, 25 ], xs: [ 100, 100, 100 ] },
	{
		lg: [ 25, 25, 25, 25 ],
		sm: [ 50, 50, 50, 50 ],
		xs: [ 100, 100, 100, 100 ],
	},
	{
		lg: [ 20, 20, 20, 20, 20 ],
		sm: [ 20, 20, 20, 20, 20 ],
		xs: [ 100, 100, 100, 100, 100 ],
	},
	{
		lg: [ 16.66, 16.66, 16.66, 16.66, 16.66, 16.7 ],
		sm: [ 16.66, 16.66, 16.66, 16.66, 16.66, 16.7 ],
		xs: [ 100, 100, 100, 100, 100, 100 ],
	},
];

const rowColumnFields = [
	{ lg: [ 100 ], sm: [ 100 ], xs: [ 100 ] },
	{ lg: [ 50, 50 ], sm: [ 50, 50 ], xs: [ 100, 100 ] },
	{
		lg: [ 33.33, 33.33, 33.34 ],
		sm: [ 33.33, 33.33, 33.34 ],
		xs: [ 100, 100, 100 ],
	},
	{
		lg: [ 25, 25, 25, 25 ],
		sm: [ 50, 50, 50, 50 ],
		xs: [ 100, 100, 100, 100 ],
	},
	{
		lg: [ 20, 20, 20, 20, 20 ],
		sm: [ 20, 20, 20, 20, 20 ],
		xs: [ 100, 100, 100, 100, 100 ],
	},
	{
		lg: [ 16.66, 16.66, 16.66, 16.66, 16.66, 16.7 ],
		sm: [ 16.66, 16.66, 16.66, 16.66, 16.66, 16.7 ],
		xs: [ 100, 100, 100, 100, 100, 100 ],
	},
];

export default function Edit( props ) {
	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		toggleSelection,
	} = props;

	const {
		blockId,
		advanceId,
		HtmlTag,
		rowBtmShape,
		rowTopShape,
		rowBgImg,
		rowOverlayBgImg,
		layout,
		rowTopGradientColor,
		rowBtmGradientColor,
		rowWrapPadding,
		align,
	} = attributes;

	useBlockId( props, true );

	const store = {
		setAttributes,
		name,
		attributes,
		clientId,
	};

	const [ device, setDevice ] = useDevice();

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/row', blockId );
	}

	const unit = rowWrapPadding[ device ]?.unit
		? rowWrapPadding[ device ]?.unit
		: 'px';

	if ( ! Object.keys( layout ).length ) {
		return (
			<div className="wopb-column-structure">
				<div className="wopb-column-structure__heading">
					Chose a Layout
				</div>
				<div className="wopb-column-structure__content">
					{ layoutPosition.map( ( data, i ) => {
						return (
							<Tooltip
								key={ i }
								delay={ 0 }
								visible={ true }
								position={ 'bottom' }
								text={ `${ data.lg.join( ' : ' ) }` }
							>
								<div
									onClick={ () =>
										setAttributes( { layout: data } )
									}
								>
									{ data.lg.map( ( e, _i ) => (
										<div
											key={ _i }
											style={ {
												width: `calc(${ e }% - 15px)`,
											} }
										></div>
									) ) }
								</div>
							</Tooltip>
						);
					} ) }
				</div>
			</div>
		);
	}

	const rowBgPaddingClass = rowBgImg?.openColor
		? ' wopbBgPadding' +
		  ( ! rowWrapPadding?.lg?.left ? ' lgL' : '' ) +
		  ( ! rowWrapPadding?.lg?.right ? ' lgR' : '' ) +
		  ( ! rowWrapPadding?.sm?.left ? ' smL' : '' ) +
		  ( ! rowWrapPadding?.sm?.right ? ' smR' : '' ) +
		  ( ! rowWrapPadding?.xs?.left ? ' xsL' : '' ) +
		  ( ! rowWrapPadding?.xs?.right ? ' xsR' : '' )
		: '';

	const getColumnLayout = ( layout ) => {
		return layout.lg.map( ( e, key ) => {
			return [
				'product-blocks/column',
				{
					columnWidth: {
						lg: layout.lg[ key ],
						sm: layout.sm[ key ],
						xs: layout.xs[ key ],
						unit: '%',
					},
				},
			];
		} );
	};

	const setPadding = ( previousLength, height, position ) => {
		const def = Object.assign( {}, rowWrapPadding[ device ], {
			[ position ]: previousLength + parseInt( height ),
		} );
		setAttributes( {
			rowWrapPadding: Object.assign( {}, rowWrapPadding, {
				[ device ]: def,
			} ),
		} );
	};

	return (
		<>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Style', 'product-blocks' ) }
					>
						<div className="wopb-field-wrap wopb-field-tag wopb-row-control">
							<label>Row Column</label>
							<div className="wopb-sub-field">
								{ rowColumnFields.map( ( el, i ) => {
									return (
										<span
											key={ i }
											className={
												`wopb-tag-button` +
												( el.lg.length ==
												layout.lg.length
													? ` active`
													: '' )
											}
											onClick={ () => {
												setAttributes( { layout: el } );
												const { innerBlocks } =
													getBlocksByClientId(
														clientId
													)[ 0 ] || [];
												if (
													innerBlocks.length >
													el.lg.length
												) {
													Array(
														innerBlocks.length -
															el.lg.length
													)
														.fill( 1 )
														.forEach(
															( e, __i ) => {
																innerBlocks[
																	innerBlocks.length -
																		( __i +
																			1 )
																].innerBlocks.forEach(
																	(
																		elem,
																		k
																	) => {
																		const serializedBlock =
																			serialize(
																				getBlocksByClientId(
																					elem.clientId
																				)
																			);
																		insertBlocks(
																			parse(
																				serializedBlock
																			),
																			0,
																			innerBlocks[
																				el
																					.lg
																					.length -
																					1
																			]
																				.clientId,
																			false
																		);
																	}
																);
																removeBlocks(
																	innerBlocks[
																		innerBlocks.length -
																			( __i +
																				1 )
																	].clientId,
																	false
																);
															}
														);
												}
												for (
													let _i = 0;
													_i < el.lg.length;
													_i++
												) {
													const width = {
														lg: el.lg[ _i ],
														sm: el.sm[ _i ],
														xs: el.xs[ _i ],
														unit: '%',
													};
													if (
														innerBlocks.length >=
														_i + 1
													) {
														updateBlockAttributes(
															innerBlocks[ _i ]
																.clientId,
															{
																columnWidth:
																	width,
															}
														);
													} else {
														const newBlock =
															wp.blocks.createBlock(
																'product-blocks/column',
																{
																	columnWidth:
																		width,
																}
															);
														insertBlocks(
															newBlock,
															_i,
															clientId,
															false
														);
													}
												}
											} }
										>
											{ i + 1 }
										</span>
									);
								} ) }
							</div>
						</div>
						<br />
						<RowSettings
							clientId={ clientId }
							layout={ layout }
							store={ store }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<CommonSettings
							title={ __( 'General', 'product-blocks' ) }
							include={ [
								{
									position: 1,
									data: {
										type: 'text',
										key: 'advanceId',
										label: __( 'ID', 'product-blocks' ),
									},
								},
								{
									position: 2,
									data: {
										type: 'range',
										key: 'advanceZindex',
										min: -100,
										max: 10000,
										step: 1,
										label: __(
											'z-index',
											'product-blocks'
										),
									},
								},
								{
									position: 3,
									data: {
										type: 'select',
										key: 'contentHeightType',
										label: __(
											'Height Type',
											'product-blocks'
										),
										options: [
											{
												value: 'normal',
												label: __(
													'Normal',
													'product-blocks'
												),
											},
											{
												value: 'windowHeight',
												label: __(
													'Window Height',
													'product-blocks'
												),
											},
											{
												value: 'custom',
												label: __(
													'Min Height',
													'product-blocks'
												),
											},
										],
									},
								},
								{
									position: 4,
									data: {
										type: 'range',
										key: 'contentHeight',
										min: 0,
										max: 1500,
										step: 1,
										responsive: true,
										unit: true,
										label: __(
											'Min Height',
											'product-blocks'
										),
									},
								},
								{
									position: 5,
									data: {
										type: 'group',
										key: 'contentOverflow',
										justify: true,
										label: __(
											'Overflow',
											'product-blocks'
										),
										options: [
											{
												value: 'visible',
												label: __(
													'Visible',
													'product-blocks'
												),
											},
											{
												value: 'hidden',
												label: __(
													'Hidden',
													'product-blocks'
												),
											},
										],
									},
								},
								{
									position: 6,
									data: {
										type: 'select',
										key: 'HtmlTag',
										block: 'column',
										beside: true,
										label: __(
											'Html Tag',
											'product-blocks'
										),
										options: [
											{
												value: 'div',
												label: __(
													'div',
													'product-blocks'
												),
											},
											{
												value: 'section',
												label: __(
													'Section',
													'product-blocks'
												),
											},
											{
												value: 'header',
												label: __(
													'header',
													'product-blocks'
												),
											},
											{
												value: 'footer',
												label: __(
													'footer',
													'product-blocks'
												),
											},
											{
												value: 'aside',
												label: __(
													'aside',
													'product-blocks'
												),
											},
											{
												value: 'main',
												label: __(
													'main',
													'product-blocks'
												),
											},
											{
												value: 'article',
												label: __(
													'article',
													'product-blocks'
												),
											},
										],
									},
								},
								{
									position: 7,
									data: {
										type: 'toggle',
										key: 'enableRowLink',
										label: __(
											'Enable Wrapper Link',
											'product-blocks'
										),
									},
								},
								{
									position: 8,
									data: {
										type: 'text',
										key: 'rowWrapperLink',
										label: __(
											'Wrapper Link',
											'product-blocks'
										),
									},
								},
								{
									position: 9,
									data: {
										type: 'toggle',
										key: 'enableNewTab',
										label: __(
											'Enable Target Blank',
											'product-blocks'
										),
									},
								},
							] }
							initialOpen={ true }
							store={ store }
						/>
						<ResponsiveAdvanced store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>
			<HtmlTag
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className } ${
					align ? `align${ align }` : ''
				} ${ rowBgPaddingClass }` }
			>
				<div className={ `wopb-row-wrapper` }>
					<Resize
						position="top"
						setLengthFunc={ setPadding }
						unit={ unit }
						previousLength={ parseInt(
							rowWrapPadding[ device ]?.top
								? rowWrapPadding[ device ]?.top
								: 0
						) }
						toggleSelection={ toggleSelection }
					/>
					{ Object.keys( rowBgImg ).length > 0 &&
						rowBgImg.openColor == 1 &&
						rowBgImg.type == 'video' &&
						rowBgImg.video &&
						bgVideoBg(
							rowBgImg.video,
							rowBgImg.loop || 0,
							rowBgImg.start || '',
							rowBgImg.end || '',
							rowBgImg.fallback || ''
						) }
					{ rowTopShape && rowTopShape != 'empty' && (
						<div className="wopb-row-shape wopb-shape-top">
							{ renderShapeIcons(
								rowTopShape,
								blockId,
								'top',
								rowTopGradientColor
							) }
						</div>
					) }
					{ rowOverlayBgImg.openColor == 1 && (
						<div className="wopb-row-overlay"></div>
					) }
					{ rowBtmShape && rowBtmShape != 'empty' && (
						<div className="wopb-row-shape wopb-shape-bottom">
							{ renderShapeIcons(
								rowBtmShape,
								blockId,
								'bottom',
								rowBtmGradientColor
							) }
						</div>
					) }
					<InnerBlocks
						renderAppender={ false }
						template={ getColumnLayout( layout ) }
						// templateLock={'all'}
						allowedBlocks={ [ 'product-blocks/column' ] }
					/>
					<Resize
						position="bottom"
						setLengthFunc={ setPadding }
						unit={ unit }
						previousLength={ parseInt(
							rowWrapPadding[ device ]?.bottom
								? rowWrapPadding[ device ]?.bottom
								: 0
						) }
						toggleSelection={ toggleSelection }
					/>
				</div>
			</HtmlTag>
		</>
	);
}
