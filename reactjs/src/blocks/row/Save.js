import { bgVideoBg } from '../../helper/CommonPanel';
import { renderShapeIcons } from './shapeIcons';

const { InnerBlocks } = wp.blockEditor;

export default function Save( props ) {
	const {
		blockId,
		advanceId,
		HtmlTag,
		rowTopShape,
		rowBtmShape,
		enableRowLink,
		rowWrapperLink,
		enableNewTab,
		rowBgImg,
		rowOverlayBgImg,
		rowTopGradientColor,
		rowBtmGradientColor,
		align,
		rowWrapPadding,
	} = props.attributes;

	const rowBgPaddingClass = rowBgImg?.openColor
		? ' wopbBgPadding' +
		  ( ! rowWrapPadding?.lg?.left ? ' lgL' : '' ) +
		  ( ! rowWrapPadding?.lg?.right ? ' lgR' : '' ) +
		  ( ! rowWrapPadding?.sm?.left ? ' smL' : '' ) +
		  ( ! rowWrapPadding?.sm?.right ? ' smR' : '' ) +
		  ( ! rowWrapPadding?.xs?.left ? ' xsL' : '' ) +
		  ( ! rowWrapPadding?.xs?.right ? ' xsR' : '' )
		: '';

	return (
		<HtmlTag
			{ ...( advanceId && { id: advanceId } ) }
			className={ `wopb-block-${ blockId } ${
				align ? `align${ align }` : ''
			} ${ rowBgPaddingClass }` }
		>
			<div className="wopb-row-wrapper">
				{ Object.keys( rowBgImg ).length > 0 &&
					rowBgImg.openColor == 1 &&
					rowBgImg.type == 'video' &&
					rowBgImg.video &&
					bgVideoBg(
						rowBgImg.video,
						rowBgImg.loop || 0,
						rowBgImg.start || '',
						rowBgImg.end || '',
						rowBgImg.fallback || ''
					) }
				{ rowTopShape && rowTopShape != 'empty' && (
					<div className="wopb-row-shape wopb-shape-top">
						{ renderShapeIcons(
							rowTopShape,
							blockId,
							'top',
							rowTopGradientColor
						) }
					</div>
				) }
				{ rowOverlayBgImg.openColor == 1 && (
					<div className="wopb-row-overlay"></div>
				) }
				{ enableRowLink && (
					<a
						href={ rowWrapperLink }
						target={ enableNewTab && '_blank' }
						className="wopb-rowwrap-url"
					></a>
				) }
				{ rowBtmShape && rowBtmShape != 'empty' && (
					<div className="wopb-row-shape wopb-shape-bottom">
						{ renderShapeIcons(
							rowBtmShape,
							blockId,
							'bottom',
							rowBtmGradientColor
						) }
					</div>
				) }
				<div className="wopb-row-content">
					<InnerBlocks.Content />
				</div>
			</div>
		</HtmlTag>
	);
}
