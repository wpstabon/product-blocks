const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import Save from './Save';
registerBlockType( 'product-blocks/column', {
	title: __( 'Column', 'product-blocks' ),
	parent: [ 'product-blocks/row' ],
	icon: <img src={ wopb_data.url + 'assets/img/blocks/column.svg' } />,
	category: 'product-blocks',
	description: __( 'Add Column block for your layout.', 'product-blocks' ),
	keywords: [
		__( 'Column', 'product-blocks' ),
		__( 'Row', 'product-blocks' ),
	],
	supports: {
		reusable: false,
		html: false,
	},
	example: {
		attributes: {
			previewImg: wopb_data.url + 'assets/img/preview/wrapper.svg',
		},
	},
	edit: Edit,
	save: Save,
	attributes: {
		// Using Double Selector For Front And Backend Side
		/* .wopb-row-content > {{WOPB}}  // This prefix for stop applying the css in backend */

		blockId: {
			type: 'string',
			default: '',
		},
		previewImg: {
			type: 'string',
			default: '',
		},
		columnOrderNumber: {
			type: 'string',
			default: '',
			style: [
				{
					selector:
						'.block-editor-block-list__block:has( > {{WOPB}}), .wopb-row-content > {{WOPB}} { order:{{columnOrderNumber}}; }',
				},
			],
		},
		columnGutter: {
			type: 'object',
			default: { lg: '0', sm: '0', xs: '0' },
		},
		columnWidth: {
			type: 'object',
			default: {},
			anotherKey: 'columnGutter',
			style: [
				{
					selector:
						'[data-wopb="{{WOPB}}"], .wopb-row-content > {{WOPB}} { flex-basis: calc({{columnWidth}} - {{columnGutter}}px);}',
				},
			],
		},
		columnHeight: {
			type: 'object',
			default: { lg: '', ulg: 'px', unit: 'px' },
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, .wopb-row-content > {{WOPB}} > .wopb-column-wrapper { min-height: {{columnHeight}};}',
				},
			],
		},

		/*==========================
                Flex Property
            ==========================*/
		columnDirection: {
			type: 'string',
			default: 'column',
			style: [
				{
					depends: [
						{
							key: 'columnDirection',
							condition: '==',
							value: 'row',
						},
					],
					selector:
						'{{WOPB}} > .wopb-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, .wopb-row-content > {{WOPB}} > .wopb-column-wrapper  { display: flex; flex-direction: {{columnDirection}} }',
				},
				{
					depends: [
						{
							key: 'columnDirection',
							condition: '==',
							value: 'column',
						},
					],
					selector:
						'{{WOPB}} > .wopb-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, .wopb-row-content > {{WOPB}} > .wopb-column-wrapper { display: flex;  flex-direction: column;}',
				},
				{
					depends: [
						{ key: 'reverseCol', condition: '==', value: true },
						{
							key: 'columnDirection',
							condition: '==',
							value: 'row',
						},
					],
					selector:
						'{{WOPB}} > .wopb-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, .wopb-row-content > {{WOPB}} > .wopb-column-wrapper { display: flex; flex-direction: row-reverse; }',
				},
				{
					depends: [
						{ key: 'reverseCol', condition: '==', value: true },
						{
							key: 'columnDirection',
							condition: '==',
							value: 'column',
						},
					],
					selector:
						'{{WOPB}} > .wopb-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, .wopb-row-content > {{WOPB}} > .wopb-column-wrapper  { display: flex; flex-direction: column-reverse; }',
				},
			],
		},
		columnJustify: {
			type: 'object',
			default: { lg: '' },
			style: [
				{
					depends: [
						{
							key: 'columnDirection',
							condition: '==',
							value: 'row',
						},
					],
					selector:
						'{{WOPB}} > .wopb-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, .wopb-row-content > {{WOPB}} > .wopb-column-wrapper { justify-content: {{columnJustify}}; }',
				},
			],
		},

		columnAlignItems: {
			type: 'string',
			default: '',
			style: [
				{
					depends: [
						{
							key: 'columnDirection',
							condition: '==',
							value: 'row',
						},
					],
					selector:
						'{{WOPB}} > .wopb-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, .wopb-row-content > {{WOPB}} > .wopb-column-wrapper { align-items: {{columnAlignItems}}; }',
				},
			],
		},
		columnAlign: {
			type: 'string',
			default: '',
			style: [
				{
					depends: [
						{
							key: 'columnDirection',
							condition: '==',
							value: 'row',
						},
					],
					selector:
						'{{WOPB}} > .wopb-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, .wopb-row-content > {{WOPB}} > .wopb-column-wrapper  { align-content: {{columnAlign}}; }',
				},
				{
					depends: [
						{
							key: 'columnDirection',
							condition: '==',
							value: 'column',
						},
					],
					selector:
						'{{WOPB}} > .wopb-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, .wopb-row-content > {{WOPB}} > .wopb-column-wrapper  { justify-content: {{columnAlign}}; }',
				},
				{
					depends: [
						{ key: 'columnWrap', condition: '==', value: 'nowrap' },
						{
							key: 'columnDirection',
							condition: '==',
							value: 'row',
						},
					],
					selector:
						'{{WOPB}} > .wopb-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, .wopb-row-content > {{WOPB}} > .wopb-column-wrapper  { align-items: {{columnAlign}}; }',
				},
			],
		},
		columnItemGap: {
			type: 'object',
			default: { lg: 16, unit: 'px' },
			style: [
				{
					depends: [
						{
							key: 'columnDirection',
							condition: '==',
							value: 'row',
						},
					],
					selector:
						'{{WOPB}} > .wopb-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, .wopb-row-content > {{WOPB}} > .wopb-column-wrapper  { gap: {{columnItemGap}}; }',
				},
			],
		},
		columnWrap: {
			type: 'string',
			default: 'wrap',
			style: [
				{
					depends: [
						{ key: 'columnWrap', condition: '==', value: 'no' },
						{
							key: 'columnDirection',
							condition: '==',
							value: 'row',
						},
					],
					selector:
						'{{WOPB}} > .wopb-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, .wopb-row-content > {{WOPB}} { flex-wrap: unset; }',
				},
				{
					depends: [
						{ key: 'columnWrap', condition: '==', value: 'wrap' },
						{
							key: 'columnDirection',
							condition: '==',
							value: 'row',
						},
					],
					selector:
						'{{WOPB}} > .wopb-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, .wopb-row-content > {{WOPB}} > .wopb-column-wrapper { flex-wrap: {{columnWrap}}; }',
				},
				{
					depends: [
						{ key: 'columnWrap', condition: '==', value: 'nowrap' },
						{
							key: 'columnDirection',
							condition: '==',
							value: 'row',
						},
					],
					selector:
						'{{WOPB}} > .wopb-column-wrapper > .block-editor-inner-blocks > .block-editor-block-list__layout, .wopb-row-content > {{WOPB}} > .wopb-column-wrapper { flex-wrap: nowrap; }',
				},
			],
		},
		reverseCol: {
			type: 'boolean',
			default: false,
		},
		/*==========================
                Background Wrapper
            ==========================*/
		colBgImg: {
			type: 'object',
			default: {
				openColor: 0,
				type: 'color',
				color: '#f5f5f5',
				size: 'cover',
				repeat: 'no-repeat',
			},
			style: [
				{
					selector: '{{WOPB}} > .wopb-column-wrapper',
				},
			],
		},

		/*========== Hover Background & Wrapper =========*/
		columnWrapHoverImg: {
			type: 'object',
			default: {
				openColor: 0,
				type: 'color',
				color: '#f5f5f5',
				size: 'cover',
				repeat: 'no-repeat',
			},
			style: [
				{
					selector: '{{WOPB}} > .wopb-column-wrapper:hover',
				},
			],
		},

		/*==========================
                Overlay
            ==========================*/
		columnOverlayImg: {
			type: 'object',
			default: {
				openColor: 0,
				type: 'color',
				color: '#f5f5f5',
				size: 'cover',
				repeat: 'no-repeat',
			},
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-column-wrapper > .wopb-column-overlay',
				},
			],
		},
		colOverlayOpacity: {
			type: 'string',
			default: '50',
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-column-wrapper > .wopb-column-overlay { opacity: {{colOverlayOpacity}}%; }',
				},
			],
		},
		columnOverlayilter: {
			type: 'object',
			default: { openFilter: 0 },
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-column-wrapper > .wopb-column-overlay { {{columnOverlayilter}} }',
				},
			],
		},
		columnOverlayBlend: {
			type: 'string',
			default: '',
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-column-wrapper > .wopb-column-overlay { mix-blend-mode:{{columnOverlayBlend}}; }',
				},
			],
		},

		/*========== Hover Overlay =========*/
		columnOverlaypHoverImg: {
			type: 'object',
			default: {
				openColor: 0,
				type: 'color',
				color: '#f5f5f5',
				size: 'cover',
				repeat: 'no-repeat',
			},
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-column-wrapper:hover > .wopb-column-overlay',
				},
			],
		},
		colOverlayHovOpacity: {
			type: 'string',
			default: '50',
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-column-wrapper:hover > .wopb-column-overlay { opacity: {{colOverlayHovOpacity}}%; }',
				},
			],
		},
		columnOverlayHoverFilter: {
			type: 'object',
			default: { openFilter: 0 },
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-column-wrapper:hover > .wopb-column-overlay { {{columnOverlayHoverFilter}} }',
				},
			],
		},
		columnOverlayHoverBlend: {
			type: 'string',
			default: '',
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-column-wrapper:hover > .wopb-column-overlay { mix-blend-mode:{{columnOverlayHoverBlend}}; }',
				},
			],
		},

		/*==========================
                Spacing & Border Style 
            ==========================*/
		colWrapMargin: {
			type: 'object',
			default: { lg: { top: '', bottom: '', unit: 'px' } },
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-column-wrapper { margin: {{colWrapMargin}}; }',
				},
			],
		},
		colWrapPadding: {
			type: 'object',
			default: {
				lg: {
					top: '0',
					bottom: '0',
					left: '0',
					right: '0',
					unit: 'px',
				},
			},
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-column-wrapper { padding: {{colWrapPadding}}; }',
				},
			],
		},

		/* =========== Border Style =========== */
		columnWrapBorder: {
			type: 'object',
			default: {
				openBorder: 0,
				width: { top: 1, right: 1, bottom: 1, left: 1 },
				color: '#505050',
			},
			style: [
				{
					selector: '{{WOPB}} > .wopb-column-wrapper',
				},
			],
		},
		columnWrapRadius: {
			type: 'object',
			default: {
				openBorder: 0,
				width: { top: 1, right: 1, bottom: 1, left: 1 },
				color: '#009fd4',
				type: 'solid',
			},
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-column-wrapper { border-radius: {{columnWrapRadius}};}',
				},
			],
		},
		columnWrapShadow: {
			type: 'object',
			default: {
				openShadow: 0,
				width: { top: 1, right: 1, bottom: 1, left: 1 },
				color: '#009fd4',
			},
			style: [
				{
					selector: '{{WOPB}} > .wopb-column-wrapper',
				},
			],
		},

		/* =========== Border Hover Style =========== */
		columnWrapHoverBorder: {
			type: 'object',
			default: {
				openBorder: 0,
				width: { top: 1, right: 1, bottom: 1, left: 1 },
				color: '#009fd4',
			},
			style: [
				{
					selector:
						'.block-editor-block-list__block:has( > {{WOPB}}):hover > {{WOPB}} > .wopb-column-wrapper, .wopb-row-content > {{WOPB}}:hover > .wopb-column-wrapper',
				},
			],
		},
		columnWrapHoverRadius: {
			type: 'object',
			default: {
				openBorder: 0,
				width: { top: 1, right: 1, bottom: 1, left: 1 },
				color: '#009fd4',
				type: 'solid',
			},
			style: [
				{
					selector:
						'.block-editor-block-list__block:has( > {{WOPB}}):hover > {{WOPB}} > .wopb-column-wrapper, .wopb-row-content > {{WOPB}}:hover > .wopb-column-wrapper { border-radius: {{columnWrapHoverRadius}};}',
				},
			],
		},
		columnWrapHoverShadow: {
			type: 'object',
			default: {
				openShadow: 0,
				width: { top: 1, right: 1, bottom: 1, left: 1 },
				color: '#009fd4',
			},
			style: [
				{
					selector:
						'.block-editor-block-list__block:has( > {{WOPB}}):hover > {{WOPB}} > .wopb-column-wrapper, .wopb-row-content > {{WOPB}}:hover > .wopb-column-wrapper',
				},
			],
		},

		/*==========================
                Column Color
            ==========================*/
		columnColor: {
			type: 'string',
			default: '',
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-column-wrapper { color: {{columnColor}} }',
				},
			],
		},
		colLinkColor: {
			type: 'string',
			default: '',
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-column-wrapper a { color: {{colLinkColor}} }',
				},
			],
		},
		colLinkHover: {
			type: 'string',
			default: '',
			style: [
				{
					selector:
						'{{WOPB}} > .wopb-column-wrapper a:hover { color: {{colLinkHover}}; }',
				},
			],
		},
		colTypo: {
			type: 'object',
			default: {
				openTypography: 0,
				size: { lg: 16, unit: 'px' },
				height: { lg: '', unit: 'px' },
				decoration: 'none',
				family: '',
				weight: 700,
			},
			style: [ { selector: '{{WOPB}} > .wopb-column-wrapper' } ],
		},

		/*==========================
                Sticky Style
            ==========================*/
		columnSticky: {
			type: 'boolean',
			default: false,
		},
		columnStickyOffset: {
			type: 'object',
			default: { lg: '0', ulg: 'px' },
			style: [
				{
					depends: [
						{ key: 'columnSticky', condition: '==', value: true },
					],
					selector:
						'{{WOPB}} { position: sticky; top: calc( 32px + {{columnStickyOffset}}); align-self: start; }',
				},
			],
		},
		/*==========================
                Advanced Setting
            ==========================*/
		advanceId: {
			type: 'string',
			default: '',
		},
		advanceZIndex: {
			type: 'string',
			default: '',
			style: [ { selector: '{{WOPB}} {z-index:{{advanceZIndex}};}' } ],
		},
		columnOverflow: {
			type: 'string',
			default: 'visible',
			style: [
				{
					selector:
						'.block-editor-block-list__block:has( > {{WOPB}}) > {{WOPB}} > .wopb-column-wrapper, .wopb-row-content > {{WOPB}} > .wopb-column-wrapper { overflow: {{columnOverflow}}; }',
				},
			],
		},
		columnEnableLink: {
			type: 'boolean',
			default: false,
		},
		columnWrapperLink: {
			type: 'string',
			default: 'example.com',
			style: [
				{
					depends: [
						{
							key: 'columnEnableLink',
							condition: '==',
							value: true,
						},
					],
				},
			],
		},
		columnTargetLink: {
			type: 'boolean',
			default: false,
			style: [
				{
					depends: [
						{
							key: 'columnEnableLink',
							condition: '==',
							value: true,
						},
					],
				},
			],
		},
		hideExtraLarge: {
			type: 'boolean',
			default: false,
			style: [ { selector: '{{WOPB}} {display:none;}' } ],
		},
		hideDesktop: {
			type: 'boolean',
			default: false,
			style: [
				{
					selector:
						'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
				},
			],
		},
		hideTablet: {
			type: 'boolean',
			default: false,
			style: [ { selector: '{{WOPB}} {display:none;}' } ],
		},
		hideMobile: {
			type: 'boolean',
			default: false,
			style: [ { selector: '{{WOPB}} {display:none;}' } ],
		},
		advanceCss: {
			type: 'string',
			default: '',
			style: [ { selector: '' } ],
		},
	},
} );
