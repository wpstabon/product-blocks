const { __ } = wp.i18n;
import { CommonSettings } from '../../../helper/CommonPanel';

const ColumnSettings = ( props ) => {
	const { store } = props;

	return (
		<>
			<CommonSettings
				title={ __( 'Column Item', 'product-blocks' ) }
				include={ [
					{
						position: 1,
						data: {
							type: 'tag',
							key: 'columnOrderNumber',
							options: [
								{
									value: '1',
									label: __( '1', 'product-blocks' ),
								},
								{
									value: '2',
									label: __( '2', 'product-blocks' ),
								},
								{
									value: '3',
									label: __( '3', 'product-blocks' ),
								},
								{
									value: '4',
									label: __( '4', 'product-blocks' ),
								},
								{
									value: '5',
									label: __( '5', 'product-blocks' ),
								},
								{
									value: '6',
									label: __( '6', 'product-blocks' ),
								},
							],
							label: __( 'Column Order', 'product-blocks' ),
						},
					},
					{
						position: 3,
						data: {
							type: 'range',
							key: 'columnHeight',
							min: 0,
							max: 1300,
							step: 1,
							unit: [ 'px', '%', 'vh', 'rem' ],
							responsive: true,
							label: __( 'Min Height', 'product-blocks' ),
						},
					},
					{
						position: 6,
						data: {
							type: 'group',
							key: 'columnDirection',
							justify: true,
							options: [
								{
									value: 'column',
									label: __( 'Vertical', 'product-blocks' ),
								},
								{
									value: 'row',
									label: __( 'Horizontal', 'product-blocks' ),
								},
							],
							label: __( 'Flex Direction', 'product-blocks' ),
						},
					},
					{
						position: 4,
						data: {
							type: 'alignment',
							key: 'columnJustify',
							disableJustify: true,
							responsive: true,
							icons: [
								'juststart',
								'justcenter',
								'justend',
								'justbetween',
								'justaround',
								'justevenly',
							],
							options: [
								'flex-start',
								'center',
								'flex-end',
								'space-between',
								'space-around',
								'space-evenly',
							],
							label: __(
								'Inside Alignment ( Horizontal )',
								'product-blocks'
							),
						},
					},
					{
						position: 5,
						data: {
							type: 'alignment',
							block: 'column-column',
							key: 'columnAlign',
							disableJustify: true,
							icons: [
								'algnStart',
								'algnCenter',
								'algnEnd',
								'stretch',
							],
							options: [
								'flex-start',
								'center',
								'flex-end',
								'space-between',
							],
							label: __(
								'Inside Content Alignment ( Vertical )',
								'product-blocks'
							),
						},
					},
					{
						position: 6,
						data: {
							type: 'alignment',
							block: 'column-column',
							key: 'columnAlignItems',
							disableJustify: true,
							icons: [ 'algnStart', 'algnCenter', 'algnEnd' ],
							options: [ 'flex-start', 'center', 'flex-end' ],
							label: __(
								'Inside Items Alignment',
								'product-blocks'
							),
						},
					},
					{
						position: 7,
						data: {
							type: 'range',
							key: 'columnItemGap',
							min: 0,
							max: 300,
							step: 1,
							responsive: true,
							label: __( 'Gap', 'product-blocks' ),
						},
					},
					{
						position: 8,
						data: {
							type: 'group',
							key: 'columnWrap',
							justify: true,
							options: [
								{
									value: 'no',
									label: __( 'No', 'product-blocks' ),
								},
								{
									value: 'wrap',
									label: __( 'Wrap', 'product-blocks' ),
								},
								{
									value: 'nowrap',
									label: __( 'No Wrap', 'product-blocks' ),
								},
							],
							label: __( 'Column Wrap', 'product-blocks' ),
						},
					},
					{
						position: 9,
						data: {
							type: 'toggle',
							key: 'reverseCol',
							label: __( 'Reverse', 'product-blocks' ),
						},
					},
				] }
				initialOpen={ true }
				store={ store }
			/>
			<CommonSettings
				title={ __( 'Background & Wrapper', 'product-blocks' ) }
				include={ [
					{
						position: 1,
						data: {
							type: 'tab',
							content: [
								{
									name: 'normal',
									title: __( 'Normal', 'product-blocks' ),
									options: [
										{
											type: 'color2',
											key: 'colBgImg',
											image: true,
											label: __(
												'Background',
												'product-blocks'
											),
										},
									],
								},
								{
									name: 'hover',
									title: __( 'Hover', 'product-blocks' ),
									options: [
										{
											type: 'color2',
											key: 'columnWrapHoverImg',
											image: true,
											label: __(
												'Background',
												'product-blocks'
											),
										},
									],
								},
							],
						},
					},
				] }
				initialOpen={ false }
				store={ store }
			/>
			<CommonSettings
				title={ __( 'Background Overlay', 'product-blocks' ) }
				include={ [
					{
						position: 1,
						data: {
							type: 'tab',
							content: [
								{
									name: 'normal',
									title: __( 'Normal', 'product-blocks' ),
									options: [
										{
											type: 'color2',
											key: 'columnOverlayImg',
											image: true,
											label: __(
												'Background',
												'product-blocks'
											),
										},
										{
											type: 'range',
											key: 'colOverlayOpacity',
											min: 0,
											max: 100,
											step: 1,
											responsive: false,
											unit: false,
											label: __(
												'Opacity',
												'product-blocks'
											),
										},
										{
											type: 'filter',
											key: 'columnOverlayilter',
											label: __(
												'CSS Filter',
												'product-blocks'
											),
										},
										{
											type: 'select',
											key: 'columnOverlayBlend',
											options: [
												{
													value: '',
													label: __(
														'Select',
														'product-blocks'
													),
												},
												{
													value: 'normal',
													label: __(
														'Normal',
														'product-blocks'
													),
												},
												{
													value: 'multiply',
													label: __(
														'Multiply',
														'product-blocks'
													),
												},
												{
													value: 'screen',
													label: __(
														'Screen',
														'product-blocks'
													),
												},
												{
													value: 'overlay',
													label: __(
														'Overlay',
														'product-blocks'
													),
												},
												{
													value: 'darken',
													label: __(
														'Darken',
														'product-blocks'
													),
												},
												{
													value: 'lighten',
													label: __(
														'Lighten',
														'product-blocks'
													),
												},
												{
													value: 'color-dodge',
													label: __(
														'Color Dodge',
														'product-blocks'
													),
												},
												{
													value: 'color-burn',
													label: __(
														'Color Burn',
														'product-blocks'
													),
												},
												{
													value: 'hard-light',
													label: __(
														'Hard Light',
														'product-blocks'
													),
												},
												{
													value: 'soft-light',
													label: __(
														'Soft Light',
														'product-blocks'
													),
												},
												{
													value: 'difference',
													label: __(
														'Difference',
														'product-blocks'
													),
												},
												{
													value: 'exclusion',
													label: __(
														'Exclusion',
														'product-blocks'
													),
												},
												{
													value: 'hue',
													label: __(
														'Hue',
														'product-blocks'
													),
												},
												{
													value: 'saturation',
													label: __(
														'Saturation',
														'product-blocks'
													),
												},
												{
													value: 'luminosity',
													label: __(
														'Luminosity',
														'product-blocks'
													),
												},
												{
													value: 'color',
													label: __(
														'Color',
														'product-blocks'
													),
												},
											],
											help: 'Notice: Background Color Requierd',
											label: __(
												'Blend Mode',
												'product-blocks'
											),
										},
									],
								},
								{
									name: 'hover',
									title: __( 'Hover', 'product-blocks' ),
									options: [
										{
											type: 'color2',
											key: 'columnOverlaypHoverImg',
											image: true,
											label: __(
												'Background',
												'product-blocks'
											),
										},
										{
											type: 'range',
											key: 'colOverlayHovOpacity',
											min: 0,
											max: 100,
											step: 1,
											responsive: false,
											unit: false,
											label: __(
												'Opacity',
												'product-blocks'
											),
										},
										{
											type: 'filter',
											key: 'columnOverlayHoverFilter',
											label: __(
												'CSS Filter',
												'product-blocks'
											),
										},
										{
											type: 'select',
											key: 'columnOverlayHoverBlend',
											options: [
												{
													value: '',
													label: __(
														'Select',
														'product-blocks'
													),
												},
												{
													value: 'normal',
													label: __(
														'Normal',
														'product-blocks'
													),
												},
												{
													value: 'multiply',
													label: __(
														'Multiply',
														'product-blocks'
													),
												},
												{
													value: 'screen',
													label: __(
														'Screen',
														'product-blocks'
													),
												},
												{
													value: 'overlay',
													label: __(
														'Overlay',
														'product-blocks'
													),
												},
												{
													value: 'darken',
													label: __(
														'Darken',
														'product-blocks'
													),
												},
												{
													value: 'lighten',
													label: __(
														'Lighten',
														'product-blocks'
													),
												},
												{
													value: 'color-dodge',
													label: __(
														'Color Dodge',
														'product-blocks'
													),
												},
												{
													value: 'color-burn',
													label: __(
														'Color Burn',
														'product-blocks'
													),
												},
												{
													value: 'hard-light',
													label: __(
														'Hard Light',
														'product-blocks'
													),
												},
												{
													value: 'soft-light',
													label: __(
														'Soft Light',
														'product-blocks'
													),
												},
												{
													value: 'difference',
													label: __(
														'Difference',
														'product-blocks'
													),
												},
												{
													value: 'exclusion',
													label: __(
														'Exclusion',
														'product-blocks'
													),
												},
												{
													value: 'hue',
													label: __(
														'Hue',
														'product-blocks'
													),
												},
												{
													value: 'saturation',
													label: __(
														'Saturation',
														'product-blocks'
													),
												},
												{
													value: 'luminosity',
													label: __(
														'Luminosity',
														'product-blocks'
													),
												},
												{
													value: 'color',
													label: __(
														'Color',
														'product-blocks'
													),
												},
											],
											help: 'Notice: Background Color Requierd',
											label: __(
												'Blend Mode',
												'product-blocks'
											),
										},
									],
								},
							],
						},
					},
				] }
				initialOpen={ false }
				store={ store }
			/>
			<CommonSettings
				title={ __( 'Spacing & Border Style', 'product-blocks' ) }
				include={ [
					{
						position: 1,
						data: {
							type: 'dimension',
							key: 'colWrapMargin',
							step: 1,
							unit: true,
							responsive: true,
							label: __( 'Margin', 'product-blocks' ),
						},
					},
					{
						position: 2,
						data: {
							type: 'dimension',
							key: 'colWrapPadding',
							step: 1,
							unit: true,
							responsive: true,
							label: __( 'Padding', 'product-blocks' ),
						},
					},
					{
						position: 3,
						data: {
							type: 'tab',
							content: [
								{
									name: 'normal',
									title: __( 'Normal', 'product-blocks' ),
									options: [
										{
											type: 'border',
											key: 'columnWrapBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
										{
											type: 'dimension',
											key: 'columnWrapRadius',
											step: 1,
											unit: true,
											responsive: true,
											label: __(
												'Border Radius',
												'product-blocks'
											),
										},
										{
											type: 'boxshadow',
											key: 'columnWrapShadow',
											step: 1,
											unit: true,
											responsive: true,
											label: __(
												'Box shadow',
												'product-blocks'
											),
										},
									],
								},
								{
									name: 'hover',
									title: __( 'Hover', 'product-blocks' ),
									options: [
										{
											type: 'border',
											key: 'columnWrapHoverBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
										{
											type: 'dimension',
											key: 'columnWrapHoverRadius',
											step: 1,
											unit: true,
											responsive: true,
											label: __(
												'Border Radius',
												'product-blocks'
											),
										},
										{
											type: 'boxshadow',
											key: 'columnWrapHoverShadow',
											step: 1,
											unit: true,
											responsive: true,
											label: __(
												'Box shadow',
												'product-blocks'
											),
										},
									],
								},
							],
						},
					},
				] }
				store={ store }
			/>
			<CommonSettings
				title={ __( 'Column Color', 'product-blocks' ) }
				include={ [
					{
						position: 1,
						data: {
							type: 'color',
							key: 'columnColor',
							label: __( 'Color', 'product-blocks' ),
						},
					},
					{
						position: 2,
						data: {
							type: 'color',
							key: 'colLinkColor',
							label: __( 'Link Color', 'product-blocks' ),
						},
					},
					{
						position: 3,
						data: {
							type: 'color',
							key: 'colLinkHover',
							label: __( 'Link Hover Color', 'product-blocks' ),
						},
					},
					{
						position: 4,
						data: {
							type: 'typography',
							key: 'colTypo',
							label: __( 'Typography', 'product-blocks' ),
						},
					},
				] }
				store={ store }
			/>
			<CommonSettings
				depend="columnSticky"
				title={ __( 'Sticky Column', 'product-blocks' ) }
				include={ [
					{
						position: 7,
						data: {
							type: 'range',
							key: 'columnStickyOffset',
							min: 0,
							max: 300,
							step: 1,
							unit: [ 'px', 'rem', 'vh' ],
							responsive: true,
							help: 'Note: Sticky Column Working only Front End',
							label: __( 'Gap', 'product-blocks' ),
						},
					},
				] }
				initialOpen={ false }
				store={ store }
			/>
		</>
	);
};
export default ColumnSettings;
