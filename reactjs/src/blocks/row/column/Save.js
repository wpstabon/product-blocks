const { InnerBlocks } = wp.blockEditor;

export default function Save( props ) {
	const {
		blockId,
		advanceId,
		columnEnableLink,
		columnWrapperLink,
		columnTargetLink,
		columnOverlayImg,
	} = props.attributes;

	return (
		<div
			{ ...( advanceId && { id: advanceId } ) }
			className={ `wopb-block-${ blockId }` }
		>
			<div className={ `wopb-column-wrapper` }>
				{ columnEnableLink && (
					<a
						className="wopb-column-link"
						target={ `${ columnTargetLink ? '_blank' : '_self' }` }
						href={ `${ columnWrapperLink }` }
					/>
				) }
				{ columnOverlayImg.openColor == 1 && (
					<div className="wopb-column-overlay" />
				) }
				<InnerBlocks.Content />
			</div>
		</div>
	);
}
