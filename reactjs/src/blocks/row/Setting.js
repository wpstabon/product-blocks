const { __ } = wp.i18n;
import { CommonSettings } from '../../helper/CommonPanel';
import { renderShapeIcons } from './shapeIcons';

const RowSettings = ( { store, clientId, layout } ) => {
	return (
		<>
			<CommonSettings
				title={ `inline` }
				include={ [
					{
						position: 1,
						data: {
							type: 'rowlayout',
							block: 'column',
							key: 'layout',
							responsive: true,
							clientId,
							layout,
							label: __( 'Layout', 'product-blocks' ),
						},
					},
					{
						position: 3,
						data: {
							type: 'range',
							key: 'columnGap',
							min: 0,
							max: 300,
							step: 1,
							responsive: true,
							clientId,
							updateChild: true,
							label: __(
								'Column Gap (Custom)',
								'product-blocks'
							),
						},
					},
					{
						position: 4,
						data: {
							type: 'range',
							key: 'rowGap',
							min: 0,
							max: 300,
							step: 1,
							responsive: true,
							label: __( 'Row Gap', 'product-blocks' ),
						},
					},
					{
						position: 5,
						data: {
							type: 'toggle',
							key: 'inheritThemeWidth',
							clientId,
							updateChild: true,
							label: __(
								'Inherit Theme Width',
								'product-blocks'
							),
						},
					},
					{
						position: 6,
						data: {
							type: 'range',
							key: 'rowContentWidth',
							min: 0,
							max: 1700,
							step: 1,
							responsive: true,
							unit: true,
							clientId,
							updateChild: true,
							label: __( 'Content Max-Width', 'product-blocks' ),
						},
					},
				] }
				initialOpen={ false }
				store={ store }
			/>

			<CommonSettings
				title={ __( 'Flex Properties', 'product-blocks' ) }
				include={ [
					{
						position: 3,
						data: {
							type: 'toggle',
							key: 'reverseEle',
							label: __( 'Reverse', 'product-blocks' ),
						},
					},
					{
						position: 5,
						data: {
							type: 'alignment',
							block: 'row-column',
							key: 'ColumnAlign',
							disableJustify: true,
							icons: [
								'algnStart',
								'algnCenter',
								'algnEnd',
								'stretch',
							],
							options: [
								'flex-start',
								'center',
								'flex-end',
								'stretch',
							],
							label: __(
								'Vertical Alignment (Align Items)',
								'product-blocks'
							),
						},
					},
				] }
				initialOpen={ false }
				store={ store }
			/>
			<CommonSettings
				title={ __( 'Background & Wrapper', 'product-blocks' ) }
				include={ [
					{
						position: 1,
						data: {
							type: 'tab',
							content: [
								{
									name: 'normal',
									title: __( 'Normal', 'product-blocks' ),
									options: [
										{
											type: 'color2',
											key: 'rowBgImg',
											image: true,
											video: true,
											label: __(
												'Background',
												'product-blocks'
											),
										},
									],
								},
								{
									name: 'hover',
									title: __( 'Hover', 'product-blocks' ),
									options: [
										{
											type: 'color2',
											key: 'rowWrapHoverImg',
											image: true,
											label: __(
												'Background',
												'product-blocks'
											),
										},
									],
								},
							],
						},
					},
				] }
				initialOpen={ false }
				store={ store }
			/>
			<CommonSettings
				title={ __( 'Background Overlay', 'product-blocks' ) }
				include={ [
					{
						position: 1,
						data: {
							type: 'tab',
							content: [
								{
									name: 'normal',
									title: __( 'Normal', 'product-blocks' ),
									options: [
										{
											type: 'color2',
											key: 'rowOverlayBgImg',
											image: true,
											label: __(
												'Color',
												'product-blocks'
											),
										},
										{
											type: 'range',
											key: 'rowOverlayOpacity',
											min: 0,
											max: 100,
											step: 1,
											responsive: false,
											unit: false,
											label: __(
												'Opacity',
												'product-blocks'
											),
										},
										{
											type: 'filter',
											key: 'rowOverlayBgFilter',
											label: __(
												'CSS Filter',
												'product-blocks'
											),
										},
										{
											type: 'select',
											key: 'rowOverlayBgBlend',
											beside: true,
											options: [
												{
													value: '',
													label: __(
														'Select',
														'product-blocks'
													),
												},
												{
													value: 'normal',
													label: __(
														'Normal',
														'product-blocks'
													),
												},
												{
													value: 'multiply',
													label: __(
														'Multiply',
														'product-blocks'
													),
												},
												{
													value: 'screen',
													label: __(
														'Screen',
														'product-blocks'
													),
												},
												{
													value: 'overlay',
													label: __(
														'Overlay',
														'product-blocks'
													),
												},
												{
													value: 'darken',
													label: __(
														'Darken',
														'product-blocks'
													),
												},
												{
													value: 'lighten',
													label: __(
														'Lighten',
														'product-blocks'
													),
												},
												{
													value: 'color-dodge',
													label: __(
														'Color Dodge',
														'product-blocks'
													),
												},
												{
													value: 'color-burn',
													label: __(
														'Color Burn',
														'product-blocks'
													),
												},
												{
													value: 'hard-light',
													label: __(
														'Hard Light',
														'product-blocks'
													),
												},
												{
													value: 'soft-light',
													label: __(
														'Soft Light',
														'product-blocks'
													),
												},
												{
													value: 'difference',
													label: __(
														'Difference',
														'product-blocks'
													),
												},
												{
													value: 'exclusion',
													label: __(
														'Exclusion',
														'product-blocks'
													),
												},
												{
													value: 'hue',
													label: __(
														'Hue',
														'product-blocks'
													),
												},
												{
													value: 'saturation',
													label: __(
														'Saturation',
														'product-blocks'
													),
												},
												{
													value: 'luminosity',
													label: __(
														'Luminosity',
														'product-blocks'
													),
												},
												{
													value: 'color',
													label: __(
														'Color',
														'product-blocks'
													),
												},
											],
											help: 'Notice: Background Color Requierd',
											label: __(
												'Blend Mode',
												'product-blocks'
											),
										},
									],
								},
								{
									name: 'hover',
									title: __( 'Hover', 'product-blocks' ),
									options: [
										{
											type: 'color2',
											key: 'rowOverlaypHoverImg',
											image: true,
											label: __(
												'Color',
												'product-blocks'
											),
										},
										{
											type: 'range',
											key: 'rowOverlayHovOpacity',
											min: 0,
											max: 100,
											step: 1,
											responsive: false,
											unit: false,
											label: __(
												'Opacity',
												'product-blocks'
											),
										},
										{
											type: 'filter',
											key: 'rowOverlayHoverFilter',
											label: __(
												'CSS Filter',
												'product-blocks'
											),
										},
										{
											type: 'select',
											key: 'rowOverlayHoverBgBlend',
											beside: true,
											options: [
												{
													value: '',
													label: __(
														'Select',
														'product-blocks'
													),
												},
												{
													value: 'normal',
													label: __(
														'Normal',
														'product-blocks'
													),
												},
												{
													value: 'multiply',
													label: __(
														'Multiply',
														'product-blocks'
													),
												},
												{
													value: 'screen',
													label: __(
														'Screen',
														'product-blocks'
													),
												},
												{
													value: 'overlay',
													label: __(
														'Overlay',
														'product-blocks'
													),
												},
												{
													value: 'darken',
													label: __(
														'Darken',
														'product-blocks'
													),
												},
												{
													value: 'lighten',
													label: __(
														'Lighten',
														'product-blocks'
													),
												},
												{
													value: 'color-dodge',
													label: __(
														'Color Dodge',
														'product-blocks'
													),
												},
												{
													value: 'color-burn',
													label: __(
														'Color Burn',
														'product-blocks'
													),
												},
												{
													value: 'hard-light',
													label: __(
														'Hard Light',
														'product-blocks'
													),
												},
												{
													value: 'soft-light',
													label: __(
														'Soft Light',
														'product-blocks'
													),
												},
												{
													value: 'difference',
													label: __(
														'Difference',
														'product-blocks'
													),
												},
												{
													value: 'exclusion',
													label: __(
														'Exclusion',
														'product-blocks'
													),
												},
												{
													value: 'hue',
													label: __(
														'Hue',
														'product-blocks'
													),
												},
												{
													value: 'saturation',
													label: __(
														'Saturation',
														'product-blocks'
													),
												},
												{
													value: 'luminosity',
													label: __(
														'Luminosity',
														'product-blocks'
													),
												},
												{
													value: 'color',
													label: __(
														'Color',
														'product-blocks'
													),
												},
											],
											help: 'Notice: Background Color Requierd',
											label: __(
												'Blend Mode',
												'product-blocks'
											),
										},
									],
								},
							],
						},
					},
				] }
				initialOpen={ false }
				store={ store }
			/>
			<CommonSettings
				title={ __( 'Spacing & Border Style', 'product-blocks' ) }
				include={ [
					{
						position: 1,
						data: {
							type: 'dimension',
							key: 'rowWrapMargin',
							step: 1,
							unit: true,
							responsive: true,
							label: __( 'Margin', 'product-blocks' ),
						},
					},
					{
						position: 2,
						data: {
							type: 'dimension',
							key: 'rowWrapPadding',
							step: 1,
							unit: true,
							responsive: true,
							label: __( 'Padding', 'product-blocks' ),
						},
					},
					{
						position: 3,
						data: {
							type: 'tab',
							content: [
								{
									name: 'normal',
									title: __( 'Normal', 'product-blocks' ),
									options: [
										{
											type: 'border',
											key: 'rowWrapBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
										{
											type: 'dimension',
											key: 'rowWrapRadius',
											step: 1,
											unit: true,
											responsive: true,
											label: __(
												'Border Radius',
												'product-blocks'
											),
										},
										{
											type: 'boxshadow',
											key: 'rowWrapShadow',
											step: 1,
											unit: true,
											responsive: true,
											label: __(
												'Box shadow',
												'product-blocks'
											),
										},
									],
								},
								{
									name: 'hover',
									title: __( 'Hover', 'product-blocks' ),
									options: [
										{
											type: 'border',
											key: 'rowWrapHoverBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
										{
											type: 'dimension',
											key: 'rowWrapHoverRadius',
											step: 1,
											unit: true,
											responsive: true,
											label: __(
												'Border Radius',
												'product-blocks'
											),
										},
										{
											type: 'boxshadow',
											key: 'rowWrapHoverShadow',
											step: 1,
											unit: true,
											responsive: true,
											label: __(
												'Box shadow',
												'product-blocks'
											),
										},
									],
								},
							],
						},
					},
				] }
				store={ store }
			/>
			<CommonSettings
				title={ __( 'Shape Divider', 'product-blocks' ) }
				include={ [
					{
						position: 1,
						data: {
							type: 'tab',
							content: [
								{
									name: 'Top',
									title: __( 'Top', 'product-blocks' ),
									options: [
										{
											type: 'select',
											key: 'rowTopShape',
											label: __(
												'Type',
												'product-blocks'
											),
											svg: true,
											options: [
												{
													value: 'empty',
													label: __(
														'Empty',
														'product-blocks'
													),
												},
												{
													value: 'tilt',
													label: __(
														'Tilt',
														'product-blocks'
													),
													svg: renderShapeIcons(
														'tilt'
													),
												},
												{
													value: 'mountain',
													label: __(
														'Mountain',
														'product-blocks'
													),
													svg: renderShapeIcons(
														'mountain'
													),
												},
												{
													value: 'waves',
													label: __(
														'Waves',
														'product-blocks'
													),
													svg: renderShapeIcons(
														'waves'
													),
												},
												{
													value: 'curve',
													label: __(
														'Curve',
														'product-blocks'
													),
													svg: renderShapeIcons(
														'curve'
													),
												},
												{
													value: 'curve_invert',
													label: __(
														'Curve Invert',
														'product-blocks'
													),
													svg: renderShapeIcons(
														'curve_invert'
													),
												},
												{
													value: 'asymmetrical_triangle',
													label: __(
														'Asymmetrical Tringle',
														'product-blocks'
													),
													svg: renderShapeIcons(
														'asymmetrical_triangle'
													),
												},
												{
													value: 'asymmetrical_triangle_invert',
													label: __(
														'Asymmetrical Tringle Invert',
														'product-blocks'
													),
													svg: renderShapeIcons(
														'asymmetrical_triangle_invert'
													),
												},
												{
													value: 'waves_invert',
													label: __(
														'Waves Invert',
														'product-blocks'
													),
													svg: renderShapeIcons(
														'waves_invert'
													),
												},
											],
										},
										{
											type: 'color2',
											key: 'rowTopGradientColor',
											label: __(
												'Color',
												'product-blocks'
											),
											extraClass: 'wopb-hide-field-item',
											customGradient: [
												{
													name: 'Plum Plate',
													gradient:
														'linear-gradient(113deg, rgb(102, 126, 234) 0%, rgb(118, 75, 162) 100%)',
													slug: 'plum-plate',
												},
												{
													name: 'Aqua Splash',
													gradient:
														'linear-gradient(15deg, rgb(19, 84, 122) 0%, rgb(128, 208, 199) 100%)',
													slug: 'aqua-splash',
												},
												{
													name: 'Teen Party',
													gradient:
														'linear-gradient(-225deg, rgb(255, 5, 124) 0%, rgb(141, 11, 147) 50%, rgb(50, 21, 117) 100%)',
													slug: 'teen-party',
												},
												{
													name: 'Fabled Sunset',
													gradient:
														'linear-gradient(-270deg, rgb(35, 21, 87) 0%, rgb(68, 16, 122) 29%, rgb(255, 19, 97) 67%, rgb(255, 248, 0) 100%)',
													slug: 'fabled-sunset',
												},
												{
													name: 'Night Call',
													gradient:
														'linear-gradient(-245deg, rgb(172, 50, 228) 0%, rgb(121, 24, 242) 48%, rgb(72, 1, 255) 100%)',
													slug: 'night-call',
												},
												{
													name: 'Itmeo Branding',
													gradient:
														'linear-gradient(18deg, rgb(42, 245, 152) 0%, rgb(0, 158, 253) 100%)',
													slug: 'itmeo-branding',
												},
												{
													name: 'Morning Salad',
													gradient:
														'linear-gradient(-255deg, rgb(183, 248, 219) 0%, rgb(80, 167, 194) 100%)',
													slug: 'morning-salad',
												},
												{
													name: 'Mind Crawl',
													gradient:
														'linear-gradient(-245deg, rgb(71, 59, 123) 0%, rgb(53, 132, 167) 51%, rgb(48, 210, 190) 100%)',
													slug: 'mind-crawl',
												},
												{
													name: 'Angel Care',
													gradient:
														'linear-gradient(-245deg, rgb(255, 226, 159) 0%, rgb(255, 169, 159) 48%, rgb(255, 113, 154) 100%)',
													slug: 'angel-care',
												},
												{
													name: 'Deep Blue',
													gradient:
														'linear-gradient(90deg, rgb(106, 17, 203) 0%, rgb(37, 117, 252) 100%)',
													slug: 'deep-blue',
												},
												{
													name: 'Mole Hall',
													gradient:
														'linear-gradient(-290deg, rgb(97, 97, 97) 0%, rgb(155, 197, 195) 100%)',
													slug: 'mole-hall',
												},
												{
													name: 'Over Sun',
													gradient:
														'linear-gradient(60deg, rgb(171, 236, 214) 0%, rgb(251, 237, 150) 100%)',
													slug: 'over-sun',
												},
												{
													name: 'Clean Mirror',
													gradient:
														'linear-gradient(45deg, rgb(147, 165, 207) 0%, rgb(228, 239, 233) 100%)',
													slug: 'clean-mirror',
												},
												{
													name: 'Strong Bliss',
													gradient:
														'linear-gradient(90deg, rgb(247, 140, 160) 0%, rgb(249, 116, 143) 19%, rgb(253, 134, 140) 60%, rgb(254, 154, 139) 100%)',
													slug: 'strong-bliss',
												},
												{
													name: 'Sweet Period',
													gradient:
														'linear-gradient(0deg, rgb(63, 81, 177) 0%, rgb(90, 85, 174) 13%, rgb(123, 95, 172) 25%, rgb(143, 106, 174) 38%, rgb(168, 106, 164) 50%)',
													slug: 'sweet-period',
												},
												{
													name: 'Purple Division',
													gradient:
														'linear-gradient(0deg, rgb(112, 40, 228) 0%, rgb(229, 178, 202) 100%)',
													slug: 'purple-division',
												},
												{
													name: 'Cold Evening',
													gradient:
														'linear-gradient(0deg, rgb(12, 52, 131) 0%, rgb(162, 182, 223) 100%, rgb(107, 140, 206) 100%, rgb(162, 182, 223) 100%)',
													slug: 'cold-evening',
												},
												{
													name: 'Desert Hump',
													gradient:
														'linear-gradient(0deg, rgb(199, 144, 129) 0%, rgb(223, 165, 121) 100%)',
													slug: 'desert-hump',
												},
												{
													name: 'Eternal Constance',
													gradient:
														'linear-gradient(0deg, rgb(9, 32, 63) 0%, rgb(83, 120, 149) 100%)',
													slug: 'ethernal-constance',
												},
												{
													name: 'Juicy Cake',
													gradient:
														'linear-gradient(0deg, rgb(225, 79, 173) 0%, rgb(249, 212, 35) 100%)',
													slug: 'juicy-cake',
												},
												{
													name: 'Rich Metal',
													gradient:
														'linear-gradient(90deg, rgb(215, 210, 204) 0%, rgb(48, 67, 82) 100%)',
													slug: 'rich-metal',
												},
											],
										},
										{
											type: 'range',
											key: 'rowTopShapeWidth',
											min: 100,
											max: 300,
											step: 1,
											responsive: true,
											label: __(
												'Width',
												'product-blocks'
											),
										},
										{
											type: 'range',
											key: 'rowTopShapeHeight',
											min: 0,
											max: 500,
											step: 1,
											responsive: true,
											label: __(
												'Height',
												'product-blocks'
											),
										},
										{
											type: 'toggle',
											key: 'rowTopShapeFlip',
											label: __(
												'Flip',
												'product-blocks'
											),
										},
										{
											type: 'toggle',
											key: 'rowTopShapeFront',
											label: __(
												'Bring to Front',
												'product-blocks'
											),
										},
										{
											type: 'range',
											key: 'rowTopShapeOffset',
											min: -100,
											max: 100,
											step: 1,
											responsive: true,
											label: __(
												'Offset',
												'product-blocks'
											),
										},
									],
								},
								{
									name: 'Bottom',
									title: __( 'Bottom', 'product-blocks' ),
									options: [
										{
											type: 'select',
											key: 'rowBtmShape',
											label: __(
												'Type',
												'product-blocks'
											),
											svgClass: 'btmShapeIcon',
											svg: true,
											options: [
												{
													value: 'empty',
													label: __(
														'Empty',
														'product-blocks'
													),
												},
												{
													value: 'tilt',
													label: __(
														'Tilt',
														'product-blocks'
													),
													svg: renderShapeIcons(
														'tilt'
													),
												},
												{
													value: 'mountain',
													label: __(
														'Mountain',
														'product-blocks'
													),
													svg: renderShapeIcons(
														'mountain'
													),
												},
												{
													value: 'waves',
													label: __(
														'Waves',
														'product-blocks'
													),
													svg: renderShapeIcons(
														'waves'
													),
												},
												{
													value: 'curve',
													label: __(
														'Curve',
														'product-blocks'
													),
													svg: renderShapeIcons(
														'curve'
													),
												},
												{
													value: 'curve_invert',
													label: __(
														'Curve Invert',
														'product-blocks'
													),
													svg: renderShapeIcons(
														'curve_invert'
													),
												},
												{
													value: 'asymmetrical_triangle',
													label: __(
														'Asymmetrical Tringle',
														'product-blocks'
													),
													svg: renderShapeIcons(
														'asymmetrical_triangle'
													),
												},
												{
													value: 'asymmetrical_triangle_invert',
													label: __(
														'Asymmetrical Tringle Invert',
														'product-blocks'
													),
													svg: renderShapeIcons(
														'asymmetrical_triangle_invert'
													),
												},
												{
													value: 'waves_invert',
													label: __(
														'Waves Invert',
														'product-blocks'
													),
													svg: renderShapeIcons(
														'waves_invert'
													),
												},
											],
										},
										{
											type: 'color2',
											key: 'rowBtmGradientColor',
											label: __(
												'Color',
												'product-blocks'
											),
											extraClass: 'wopb-hide-field-item',
											customGradient: [
												{
													name: 'Plum Plate',
													gradient:
														'linear-gradient(113deg, rgb(102, 126, 234) 0%, rgb(118, 75, 162) 100%)',
													slug: 'plum-plate',
												},
												{
													name: 'Aqua Splash',
													gradient:
														'linear-gradient(15deg, rgb(19, 84, 122) 0%, rgb(128, 208, 199) 100%)',
													slug: 'aqua-splash',
												},
												{
													name: 'Teen Party',
													gradient:
														'linear-gradient(-225deg, rgb(255, 5, 124) 0%, rgb(141, 11, 147) 50%, rgb(50, 21, 117) 100%)',
													slug: 'teen-party',
												},
												{
													name: 'Fabled Sunset',
													gradient:
														'linear-gradient(-270deg, rgb(35, 21, 87) 0%, rgb(68, 16, 122) 29%, rgb(255, 19, 97) 67%, rgb(255, 248, 0) 100%)',
													slug: 'fabled-sunset',
												},
												{
													name: 'Night Call',
													gradient:
														'linear-gradient(-245deg, rgb(172, 50, 228) 0%, rgb(121, 24, 242) 48%, rgb(72, 1, 255) 100%)',
													slug: 'night-call',
												},
												{
													name: 'Itmeo Branding',
													gradient:
														'linear-gradient(18deg, rgb(42, 245, 152) 0%, rgb(0, 158, 253) 100%)',
													slug: 'itmeo-branding',
												},
												{
													name: 'Morning Salad',
													gradient:
														'linear-gradient(-255deg, rgb(183, 248, 219) 0%, rgb(80, 167, 194) 100%)',
													slug: 'morning-salad',
												},
												{
													name: 'Mind Crawl',
													gradient:
														'linear-gradient(-245deg, rgb(71, 59, 123) 0%, rgb(53, 132, 167) 51%, rgb(48, 210, 190) 100%)',
													slug: 'mind-crawl',
												},
												{
													name: 'Angel Care',
													gradient:
														'linear-gradient(-245deg, rgb(255, 226, 159) 0%, rgb(255, 169, 159) 48%, rgb(255, 113, 154) 100%)',
													slug: 'angel-care',
												},
												{
													name: 'Deep Blue',
													gradient:
														'linear-gradient(90deg, rgb(106, 17, 203) 0%, rgb(37, 117, 252) 100%)',
													slug: 'deep-blue',
												},
												{
													name: 'Mole Hall',
													gradient:
														'linear-gradient(-290deg, rgb(97, 97, 97) 0%, rgb(155, 197, 195) 100%)',
													slug: 'mole-hall',
												},
												{
													name: 'Over Sun',
													gradient:
														'linear-gradient(60deg, rgb(171, 236, 214) 0%, rgb(251, 237, 150) 100%)',
													slug: 'over-sun',
												},
												{
													name: 'Clean Mirror',
													gradient:
														'linear-gradient(45deg, rgb(147, 165, 207) 0%, rgb(228, 239, 233) 100%)',
													slug: 'clean-mirror',
												},
												{
													name: 'Strong Bliss',
													gradient:
														'linear-gradient(90deg, rgb(247, 140, 160) 0%, rgb(249, 116, 143) 19%, rgb(253, 134, 140) 60%, rgb(254, 154, 139) 100%)',
													slug: 'strong-bliss',
												},
												{
													name: 'Sweet Period',
													gradient:
														'linear-gradient(0deg, rgb(63, 81, 177) 0%, rgb(90, 85, 174) 13%, rgb(123, 95, 172) 25%, rgb(143, 106, 174) 38%, rgb(168, 106, 164) 50%)',
													slug: 'sweet-period',
												},
												{
													name: 'Purple Division',
													gradient:
														'linear-gradient(0deg, rgb(112, 40, 228) 0%, rgb(229, 178, 202) 100%)',
													slug: 'purple-division',
												},
												{
													name: 'Cold Evening',
													gradient:
														'linear-gradient(0deg, rgb(12, 52, 131) 0%, rgb(162, 182, 223) 100%, rgb(107, 140, 206) 100%, rgb(162, 182, 223) 100%)',
													slug: 'cold-evening',
												},
												{
													name: 'Desert Hump',
													gradient:
														'linear-gradient(0deg, rgb(199, 144, 129) 0%, rgb(223, 165, 121) 100%)',
													slug: 'desert-hump',
												},
												{
													name: 'Eternal Constance',
													gradient:
														'linear-gradient(0deg, rgb(9, 32, 63) 0%, rgb(83, 120, 149) 100%)',
													slug: 'ethernal-constance',
												},
												{
													name: 'Juicy Cake',
													gradient:
														'linear-gradient(0deg, rgb(225, 79, 173) 0%, rgb(249, 212, 35) 100%)',
													slug: 'juicy-cake',
												},
												{
													name: 'Rich Metal',
													gradient:
														'linear-gradient(90deg, rgb(215, 210, 204) 0%, rgb(48, 67, 82) 100%)',
													slug: 'rich-metal',
												},
											],
										},
										{
											type: 'range',
											key: 'rowBtmShapeWidth',
											min: 100,
											max: 300,
											step: 1,
											responsive: true,
											label: __(
												'Width',
												'product-blocks'
											),
										},
										{
											type: 'range',
											key: 'rowBtmShapeHeight',
											min: 0,
											max: 500,
											step: 1,
											responsive: true,
											label: __(
												'Height',
												'product-blocks'
											),
										},
										{
											type: 'toggle',
											key: 'rowBtmShapeFlip',
											label: __(
												'Flip',
												'product-blocks'
											),
										},
										{
											type: 'toggle',
											key: 'rowBtmShapeFront',
											label: __(
												'Bring to Front',
												'product-blocks'
											),
										},
										{
											type: 'range',
											key: 'rowBtmShapeOffset',
											min: -100,
											max: 100,
											step: 1,
											responsive: true,
											label: __(
												'Offset',
												'product-blocks'
											),
										},
									],
								},
							],
						},
					},
				] }
				store={ store }
			/>
			<CommonSettings
				title={ __( 'Row Color', 'product-blocks' ) }
				include={ [
					{
						position: 1,
						data: {
							type: 'color',
							key: 'rowColor',
							label: __( 'Color', 'product-blocks' ),
						},
					},
					{
						position: 2,
						data: {
							type: 'color',
							key: 'rowLinkColor',
							label: __( 'Link Color', 'product-blocks' ),
						},
					},
					{
						position: 3,
						data: {
							type: 'color',
							key: 'rowLinkHover',
							label: __( 'Link Hover Color', 'product-blocks' ),
						},
					},
					{
						position: 4,
						data: {
							type: 'typography',
							key: 'rowTypo',
							label: __( 'Typography', 'product-blocks' ),
						},
					},
				] }
				store={ store }
			/>
		</>
	);
};
export default RowSettings;
