/** @format */

const { __ } = wp.i18n;
const { InspectorControls, RichText } = wp.blockEditor;
const { Spinner, Placeholder } = wp.components;
import Slider from 'react-slick';
import { CssGenerator } from '../../helper/CssGenerator';
import {
	SliderSetting,
	NextArrow,
	PrevArrow,
	Filter,
	LoadMore,
	Pagination,
} from '../../helper/CommonPanel';
import icons from '../../helper/icons';
import ToolBarElement from '../../helper/ToolBarElement';
import { Card, Cart, Description, Price, Review, Title } from './components';
import Settings, { AddSettingsToToolbar, features } from './Settings';
import useBlockId from '../../helper/hooks/use-block-id';
import usePrevious from '../../helper/hooks/use-previous';
import useProducts from '../../helper/hooks/use-products';
import useFluentSettings from '../../helper/hooks/useFluentSettings';

const SECTIONS = [ 'title', 'cart', 'review', 'price', 'description' ];

const { useMemo } = wp.element;

export default function Edit( props ) {
	useBlockId( props, true );

	const { setAttributes, name, attributes, clientId, className, context } =
		props;
	const prevProps = usePrevious( props );
	const { postsList, loading, error } = useProducts(
		attributes,
		prevProps?.attributes
	);
	const { section, setSection, setCurrSettings, toolbarSettings } =
		useFluentSettings( { props, prevProps } );

	const {
		blockId,
		advanceId,
		headingTag,
		headingText,
		headingStyle,
		headingShow,
		headingAlign,
		headingURL,
		filterShow,
		filterType,
		filterCat,
		filterTag,
		headingBtnText,
		subHeadingShow,
		subHeadingText,
		showArrows,
		showDots,
		slidesToShow,
		autoPlay,
		productView,
		arrowStyle,
		slideSpeed,
		imgFlip,
		filterText,
		filterAction,
		filterActionText,
		previewId,
		paginationShow,
		paginationAjax,
		paginationType,
		paginationNav,
		paginationText,
		loadMoreText,
		imgCrop,
		showCart,
		cartText,
		showImage,
		titleTag,
		titleShow,
		showPrice,
		showReview,
		imgAnimation,
		sortSection,
		disableFlip,
		showShortDesc,
		shortDescLimit,
		columns,
		contentAlign,
		overlayMetaList,
		tooltipPosition,
		ovrMetaInline,
		titleLength,
		hoverMeta,
	} = attributes;

	const store = {
		setAttributes,
		name,
		attributes,
		setSection,
		section,
		clientId,
		context,
	};

	const settings = SliderSetting( {
		arrows: showArrows,
		dots: showDots,
		slidesToShow,
		autoplay: autoPlay,
		autoplaySpeed: parseInt( slideSpeed ),
		nextArrow: (
			<NextArrow
				arrowStyle={ arrowStyle }
				setSection={ ( e ) => {
					setCurrSettings( e, 'arrow' );
				} }
			/>
		),
		prevArrow: (
			<PrevArrow
				arrowStyle={ arrowStyle }
				setSection={ ( e ) => {
					setCurrSettings( e, 'arrow' );
				} }
			/>
		),
		appendDots: ( dots ) => {
			return (
				<div
					onClick={ ( e ) => {
						setCurrSettings( e, 'dot' );
					} }
				>
					<ul> { dots } </ul>
				</div>
			);
		},
	} );

	if ( previewId ) {
		return (
			<img
				style={ { marginTop: '0px', width: '420px' } }
				src={ previewId }
			/>
		);
	}

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/product-list-1', blockId );
	}

	const HeadingTag = headingTag;
	const is_show = JSON.parse( sortSection );
	const TitleTag = titleTag;

	const enableList = useMemo( () => {
		const eList = {};
		SECTIONS.forEach( ( sec ) => {
			eList[ sec ] = is_show.includes( sec );
		} );
		return eList;
	}, [ sortSection ] );

	function renderSection( el, post, key ) {
		switch ( el ) {
			case 'title':
				return (
					<Title
						title={ post.title }
						titleShow={ titleShow }
						TitleTag={ TitleTag }
						isShow={ enableList[ el ] }
						key={ key }
						setSection={ setCurrSettings }
						titleLength={ titleLength }
					/>
				);
			case 'review':
				return (
					<Review
						showReview={ showReview }
						ratingAverage={ post.rating_average }
						isShow={ enableList[ el ] }
						key={ key }
						setSection={ setCurrSettings }
					/>
				);
			case 'price':
				return (
					<Price
						priceHtml={ post.price_html }
						showPrice={ showPrice }
						isShow={ enableList[ el ] }
						key={ key }
						setSection={ setCurrSettings }
					/>
				);
			case 'cart':
				return (
					<Cart
						isShow={ enableList[ el ] }
						showCart={ showCart }
						cartText={ cartText }
						key={ key }
						setSection={ setCurrSettings }
					/>
				);
			case 'description':
				return (
					<Description
						showShortDesc={ showShortDesc }
						excerpt={ post.excerpt }
						shortDescLimit={ shortDescLimit }
						key={ key }
						setSection={ setCurrSettings }
					/>
				);
			default:
				return null;
		}
	}

	return (
		<>
			<InspectorControls>
				<Settings store={ store } />
			</InspectorControls>

			<ToolBarElement
				include={ [
					{
						type: 'alignment',
						key: 'contentAlign',
						label: __( 'Grid Content Alignment', 'product-blocks' ),
						value: contentAlign,
						disableJustify: true,
					},
					{ type: 'template' },
					{ type: 'query' },
					{ type: 'grid_spacing' },
					{
						type: 'common',
						data: features( store ),
					},
				] }
				store={ store }
			/>

			<AddSettingsToToolbar
				store={ store }
				selected={ toolbarSettings }
			/>

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
				onClick={ ( e ) => setCurrSettings( e, 'general', 'none' ) }
			>
				<div className={ `wopb-block-wrapper` }>
					{ ( headingShow || filterShow || paginationShow ) && (
						<div className={ `wopb-heading-filter` }>
							<div className={ `wopb-heading-filter-in` }>
								{ headingShow && (
									<div
										className={ `wopb-heading-wrap wopb-heading-${ headingStyle } wopb-heading-${ headingAlign }` }
										onClick={ ( e ) => {
											setCurrSettings( e, 'heading' );
										} }
									>
										{ headingURL ? (
											<HeadingTag
												className={ `wopb-heading-inner` }
											>
												<a>
													<RichText
														key="editable"
														tagName={ 'span' }
														placeholder={ __(
															'Add Text…',
															'product-blocks'
														) }
														onChange={ ( value ) =>
															setAttributes( {
																headingText:
																	value,
															} )
														}
														value={ headingText }
													/>
												</a>
											</HeadingTag>
										) : (
											<HeadingTag
												className={ `wopb-heading-inner` }
											>
												<RichText
													key="editable"
													tagName={ 'span' }
													placeholder={ __(
														'Add Text…',
														'product-blocks'
													) }
													onChange={ ( value ) =>
														setAttributes( {
															headingText: value,
														} )
													}
													value={ headingText }
												/>
											</HeadingTag>
										) }
										{ headingStyle == 'style11' &&
											headingURL && (
												<a
													className={ `wopb-heading-btn` }
												>
													{ headingBtnText }
													{ icons.rightArrowLg }
												</a>
											) }
										{ subHeadingShow && (
											<div
												className={ `wopb-sub-heading` }
											>
												<RichText
													key="editable"
													tagName={ 'div' }
													className={
														'wopb-sub-heading-inner'
													}
													placeholder={ __(
														'Add Text…',
														'product-blocks'
													) }
													onChange={ ( value ) =>
														setAttributes( {
															subHeadingText:
																value,
														} )
													}
													value={ subHeadingText }
												/>
											</div>
										) }
									</div>
								) }
								{ ( filterShow || paginationShow ) &&
									productView == 'grid' && (
										<div
											className={ `wopb-filter-navigation` }
										>
											{ filterShow && (
												<Filter
													filterText={ filterText }
													filterType={ filterType }
													filterCat={ filterCat }
													filterTag={ filterTag }
													filterAction={
														filterAction
													}
													filterActionText={
														filterActionText
													}
													onClick={ ( e ) =>
														setCurrSettings(
															e,
															'filter'
														)
													}
												/>
											) }
										</div>
									) }
							</div>
						</div>
					) }

					{ ! error ? (
						! loading ? (
							postsList.length > 0 ? (
								<>
									{ productView === 'slide' ? (
										<div
											className={ `wopb-product-blocks-slide wopb-product-blocks-slide1` }
										>
											<Slider { ...settings }>
												{ postsList.map( ( post ) => (
													<Card
														key={ post.post_id }
														post={ post }
														showImage={ showImage }
														imgAnimation={
															imgAnimation
														}
														imgCrop={ imgCrop }
														disableFlip={
															disableFlip
														}
														setSection={
															setCurrSettings
														}
														cartText={ cartText }
														overlayMetaList={
															overlayMetaList
														}
														tooltipPosition={
															tooltipPosition
														}
														ovrMetaInline={
															ovrMetaInline
														}
													>
														{ is_show.map(
															( el, idx ) =>
																renderSection(
																	el,
																	post,
																	idx
																)
														) }
													</Card>
												) ) }
											</Slider>
										</div>
									) : (
										<div
											className={ `wopb-block-items-wrap wopb-block-row wopb-block-column-${ columns.lg } wopb-block-content-${ imgFlip }` }
										>
											{ postsList.map( ( post ) => (
												<Card
													key={ post.post_id }
													post={ post }
													showImage={ showImage }
													imgAnimation={
														imgAnimation
													}
													imgCrop={ imgCrop }
													disableFlip={ disableFlip }
													setSection={
														setCurrSettings
													}
													cartText={ cartText }
													overlayMetaList={
														overlayMetaList
													}
													tooltipPosition={
														tooltipPosition
													}
													ovrMetaInline={
														ovrMetaInline
													}
													hoverMeta={ hoverMeta }
												>
													{ is_show.map(
														( el, idx ) =>
															renderSection(
																el,
																post,
																idx
															)
													) }
												</Card>
											) ) }
										</div>
									) }
								</>
							) : (
								<Placeholder
									className={ `wopb-backend-block-loading` }
									label={ __(
										'No Products found',
										'product-blocks'
									) }
								></Placeholder>
							)
						) : (
							<Placeholder
								className={ `wopb-backend-block-loading` }
								label={ __( 'Loading…', 'product-blocks' ) }
							>
								<Spinner />
							</Placeholder>
						)
					) : (
						<Placeholder
							label={ __( 'Products are not available' ) }
						>
							<div style={ { marginBottom: 15 } }>
								{ __(
									'Make sure Add Product.',
									'product-blocks'
								) }
							</div>
						</Placeholder>
					) }
					{ paginationShow &&
						productView == 'grid' &&
						paginationType == 'pagination' && (
							<Pagination
								paginationNav={ paginationNav }
								paginationAjax={ paginationAjax }
								paginationText={ paginationText }
								onClick={ ( e ) => {
									setCurrSettings( e, 'pagination' );
								} }
							/>
						) }
					{ paginationShow &&
						productView == 'grid' &&
						paginationType == 'loadMore' && (
							<LoadMore
								loadMoreText={ loadMoreText }
								onClick={ ( e ) => {
									setCurrSettings( e, 'pagination' );
								} }
							/>
						) }
				</div>
			</div>
		</>
	);
}
