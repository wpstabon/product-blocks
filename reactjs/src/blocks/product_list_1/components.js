const { __ } = wp.i18n;
const { memo, Fragment } = wp.element;
import {
	renderGridCart,
	renderGridCompare,
	renderGridQuickview,
	renderGridWishlist,
} from '../../helper/grid_compo/GridCommon';
import { renderTitle, renderExcerpt } from '../../helper/CommonPanel';

export const Title = memo( function ( {
	title,
	titleShow,
	TitleTag,
	isShow,
	setSection,
	titleLength,
} ) {
	return title && titleShow && isShow
		? renderTitle(
				title,
				TitleTag,
				( e ) => setSection( e, 'title' ),
				undefined,
				titleLength
		  )
		: null;
} );

export const Review = memo( function ( {
	showReview,
	isShow,
	ratingAverage,
	setSection,
} ) {
	return showReview && isShow ? (
		<div
			className="wopb-star-rating wopb-component-simple"
			onClick={ ( e ) => setSection( e, 'review' ) }
		>
			<span style={ { width: ( ratingAverage / 5 ) * 100 + '%' } }>
				<strong itemProp="ratingValue" className="wopb-rating">
					{ ratingAverage }
				</strong>
				{ __( 'out of 5', 'product-blocks' ) }
			</span>
		</div>
	) : null;
} );

export const Price = memo( function Review( {
	priceHtml,
	showPrice,
	isShow,
	setSection,
} ) {
	return priceHtml && showPrice && isShow ? (
		<div
			className="wopb-product-price wopb-component-simple"
			dangerouslySetInnerHTML={ { __html: priceHtml } }
			onClick={ ( e ) => setSection( e, 'price' ) }
		></div>
	) : null;
} );

export const Cart = memo( function Cart( {
	showCart,
	isShow,
	cartText,
	setSection,
} ) {
	return showCart && isShow ? (
		<div
			className="wopb-product-btn wopb-component-simple"
			onClick={ ( e ) => setSection( e, 'cart' ) }
		>
			<a>
				{ cartText ? cartText : __( 'Add to cart', 'product-blocks' ) }
			</a>
		</div>
	) : null;
} );

export const Description = memo( function Description( {
	showShortDesc,
	excerpt,
	shortDescLimit,
	setSection,
} ) {
	return showShortDesc
		? renderExcerpt( excerpt, shortDescLimit, ( e ) =>
				setSection( e, 'desc' )
		  )
		: null;
} );

export const Card = ( {
	post,
	children,
	showImage,
	imgAnimation,
	imgCrop,
	disableFlip,
	setSection,
	cartText,
	overlayMetaList,
	tooltipPosition,
	ovrMetaInline,
	hoverMeta,
} ) => {
	const parsedOverlayMetaList = JSON.parse( overlayMetaList );
	let metaClass = ovrMetaInline ? ' wopb_f_inline' : '';
	metaClass += ! hoverMeta ? ' wopb-is-visible' : '';
	return (
		<div className={ `wopb-block-item wopb-block-media` }>
			<div className={ `wopb-block-content-wrap` }>
				{ post.image && showImage && (
					<div
						className={ `wopb-block-image wopb-block-image-${ imgAnimation }` }
					>
						<a>
							<img
								alt={ post.title || '' }
								src={ post.image[ imgCrop ] }
								className="wopb-component-simple"
								onClick={ ( e ) => setSection( e, 'image' ) }
							/>
						</a>
						{ parsedOverlayMetaList.length != 0 && (
							<div
								className={
									`wopb-product-new-meta wopb-component-simple` +
									metaClass
								}
								onClick={ ( e ) => {
									setSection( e, 'overlay_meta' );
								} }
							>
								{ parsedOverlayMetaList.map( ( val, _i ) => {
									return (
										<Fragment key={ _i }>
											{ val == '_wishlist' &&
												renderGridWishlist( {
													tooltipPosition,
												} ) }
											{ val == '_qview' &&
												renderGridQuickview( {
													tooltipPosition,
												} ) }
											{ val == '_compare' &&
												renderGridCompare( {
													tooltipPosition,
												} ) }
											{ val == '_cart' &&
												renderGridCart( {
													cartText,
													tooltipPosition,
												} ) }
										</Fragment>
									);
								} ) }
							</div>
						) }
					</div>
				) }
				<div className="wopb-block-content">{ children }</div>
			</div>
		</div>
	);
};
