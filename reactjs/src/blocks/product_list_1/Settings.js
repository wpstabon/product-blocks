const { __ } = wp.i18n;
import {
	ArrowStyle,
	ArrowStyleArg,
	CartStyle,
	CartStyleArg,
	CommonSettings,
	CustomCssAdvanced,
	DotStyle,
	DotStyleArg,
	FilterStyle,
	FilterStyleArg,
	GeneralAdvanced,
	GeneralSettingsWithQuery,
	HeadingSettings,
	HeadingSettingsArg,
	ImageStyle,
	ImageStyleArg,
	MetaElementStyle,
	OverlayMetaElementStyle,
	PaginationStyle,
	PaginationStyleArg,
	PriceStyle,
	PriceStyleArg,
	ResponsiveAdvanced,
	ReviewStyle,
	ReviewStyleArg,
	ShortDescStyle,
	ShortDescStyleArg,
	TitleStyle,
	TitleStyleArg,
	TypographyTB,
	colorIcon,
	filterFields,
	filterSettings,
	filterSpacing,
	settingsIcon,
	spacingIcon,
	styleIcon,
	titleColor,
	typoIcon,
	wopbSupport,
} from '../../helper/CommonPanel';
import LinkGenerator from '../../helper/LinkGenerator';
import { Section, Sections } from '../../helper/Sections';
import TemplateModal from '../../helper/TemplateModal';
import ToolbarSort from '../../helper/toolbar/ToolbarSort';
import WopbToolbarDropdown from '../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar } from '../../helper/ux';

export const features = ( store ) => {
	return [
		{
			type: 'group',
			key: 'productView',
			label: __( 'Product View', 'product-blocks' ),
			justify: true,
			options: [
				{ value: 'grid', label: __( 'Grid View', 'product-blocks' ) },
				{ value: 'slide', label: __( 'Slide View', 'product-blocks' ) },
			],
		},

		{
			type: 'toggle',
			key: 'headingShow',
			label: __( 'Heading', 'product-blocks' ),
		},

		{
			type: store.attributes.productView === 'slide' ? 'toggle' : '',
			key: 'showArrows',
			label: __( 'Arrows', 'product-blocks' ),
		},
		{
			type: store.attributes.productView === 'slide' ? 'toggle' : '',
			key: 'showDots',
			label: __( 'Dots', 'product-blocks' ),
		},

		{
			type: 'toggle',
			key: 'titleShow',
			label: __( 'Title', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'showImage',
			label: __( 'Image', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'showPrice',
			label: __( 'Price', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'showReview',
			label: __( 'Review', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'showCart',
			label: __( 'Cart', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'showShortDesc',
			label: __( 'Short Description', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'filterShow',
			label: __( 'Filter', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'paginationShow',
			label: __( 'Pagination', 'product-blocks' ),
		},
	];
};

export default function Settings( { store } ) {
	const { section } = store;
	const { productView } = store.attributes;

	return (
		<>
			<TemplateModal
				prev={
					LinkGenerator(
						'https://www.wpxpo.com/wowstore/blocks/?',
						'blockPreview'
					) + '#demoid426'
				}
				store={ store }
			/>
			<Sections>
				<Section
					slug="setting"
					title={ __( 'Settings', 'product-blocks' ) }
				>
					<GeneralSettingsWithQuery
						initialOpen={ section.general }
						store={ store }
						exclude={ [
							'showOutStock',
							'showInStock',
							'quickView',
							'showCompare',
						] }
					/>
					<OverlayMetaElementStyle
						store={ store }
						initialOpen={ section.overlay_meta }
					/>
					{ productView == 'slide' && (
						<ArrowStyle
							depend="showArrows"
							store={ store }
							initialOpen={ section.arrow }
						/>
					) }

					{ productView == 'slide' && (
						<DotStyle
							depend="showDots"
							store={ store }
							initialOpen={ section.dot }
						/>
					) }
					<CommonSettings
						initialOpen={ false }
						title={ __( 'Content Wrap', 'product-blocks' ) }
						store={ store }
						include={ [
							{
								data: {
									type: 'toggle',
									key: 'stackOnMobile',
									label: __(
										'Stack On Mobile',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'alignment',
									key: 'contentWrapVerticalAlign',
									disableJustify: true,
									responsive: true,
									icons: [
										'algnStart',
										'algnCenter',
										'algnEnd',
									],
									options: [ 'start', 'center', 'end' ],
									label: __(
										'Vertical Alignment',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'color2',
									key: 'contentWrapBgColor',
									label: __( 'Background', 'product-blocks' ),
								},
							},
							{
								data: {
									type: 'border',
									key: 'contentWrapBorder',
									label: __( 'Border', 'product-blocks' ),
								},
							},
							{
								data: {
									type: 'dimension',
									key: 'contentWrapRadius',
									label: __(
										'Border Radius',
										'product-blocks'
									),
									step: 1,
									unit: true,
									responsive: true,
								},
							},
							{
								data: {
									type: 'dimension',
									key: 'contentWrapPadding',
									label: __( 'Padding', 'product-blocks' ),
									step: 1,
									unit: true,
									responsive: true,
								},
							},
						] }
					/>
					<PriceStyle
						depend="showPrice"
						store={ store }
						initialOpen={ section.price }
					/>
					<ReviewStyle
						depend="showReview"
						store={ store }
						initialOpen={ section.review }
					/>
					<CartStyle
						depend="showCart"
						store={ store }
						initialOpen={ section.cart }
					/>
					<ImageStyle
						initialOpen={ section.image }
						depend="showImage"
						exclude={ [
							'imgOverlay',
							'imgOverlayType',
							'overlayColor',
							'imgOpacity',
							'imgBgColor',
							'imgHoverBgColor',
						] }
						store={ store }
						include={ [
							{
								position: 0,
								data: {
									type: 'toggle',
									key: 'imgFlip',
									label: __( 'Image Flip', 'product-blocks' ),
								},
							},
							{
								position: 5,
								data: {
									type: 'tag',
									key: 'imageScale',
									label: __(
										'Image Scale',
										'product-blocks'
									),
									options: [
										{
											value: '',
											label: __(
												'None',
												'product-blocks'
											),
										},
										{
											value: 'cover',
											label: __(
												'Cover',
												'product-blocks'
											),
										},
										{
											value: 'contain',
											label: __(
												'Contain',
												'product-blocks'
											),
										},
										{
											value: 'fill',
											label: __(
												'Fill',
												'product-blocks'
											),
										},
										{
											value: 'scale-down',
											label: __(
												'Scale Down',
												'product-blocks'
											),
										},
									],
								},
							},
						] }
					/>
					<HeadingSettings
						depend="headingShow"
						store={ store }
						initialOpen={ section.heading }
					/>
					<TitleStyle
						depend="titleShow"
						store={ store }
						initialOpen={ section.title }
					/>
					<ShortDescStyle
						depend="showShortDesc"
						exclude={ [ 'showFullShortDesc' ] }
						store={ store }
						initialOpen={ section.desc }
					/>
					{ productView == 'grid' && (
						<FilterStyle
							depend="filterShow"
							store={ store }
							initialOpen={ section.filter }
						/>
					) }
					{ productView == 'grid' && (
						<PaginationStyle
							depend="paginationShow"
							store={ store }
							initialOpen={ section.pagination }
						/>
					) }
				</Section>
				<Section
					slug="advanced"
					title={ __( 'Advanced', 'product-blocks' ) }
				>
					<GeneralAdvanced initialOpen={ true } store={ store } />
					<ResponsiveAdvanced pro={ true } store={ store } />
					<CustomCssAdvanced store={ store } />
				</Section>
			</Sections>
			{ wopbSupport() }
		</>
	);
}

export function AddSettingsToToolbar( { selected, store } ) {
	switch ( selected ) {
		case 'filter':
			return (
				<WopbToolbarGroup text={ 'Filter' }>
					<TypographyTB
						store={ store }
						attrKey={ 'fliterTypo' }
						label={ __( 'Filter Typography', 'product-blocks' ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Filter Spacing', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'filter',
								title: __( 'Filter Spacing', 'product-blocks' ),
								options: filterFields(
									filterSpacing,
									'__all',
									FilterStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Filter Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'filter',
								title: __( 'Filter Style', 'product-blocks' ),
								options: filterFields(
									null,
									[
										...filterSettings,
										'fliterTypo',
										...filterSpacing,
									],
									FilterStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Filter Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'filter',
								title: __(
									'Filter Settings',
									'product-blocks'
								),
								options: filterFields(
									filterSettings,
									'__all',
									FilterStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'title':
			return (
				<WopbToolbarGroup text={ 'Title' }>
					<ToolbarSort store={ store } data={ 'title' } />
					<TypographyTB
						store={ store }
						attrKey={ 'titleTypo' }
						label={ __( 'Title Typography', 'product-blocks' ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Title Spacing', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Title Spacing', 'product-blocks' ),
								options: filterFields(
									[ 'titlePadding' ],
									'__all',
									TitleStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ colorIcon }
						store={ store }
						label={ __( 'Title Color', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Title Color', 'product-blocks' ),
								options: filterFields(
									titleColor,
									'__all',
									TitleStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Title Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Title Settings', 'product-blocks' ),
								options: filterFields(
									null,
									[
										...titleColor,
										'titleTypo',
										'titlePadding',
									],
									TitleStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'desc':
			return (
				<WopbToolbarGroup text={ 'Description' }>
					<ToolbarSort store={ store } data={ 'description' } />
					<TypographyTB
						store={ store }
						attrKey={ 'ShortDescTypo' }
						label={ __(
							'Description Typography',
							'product-blocks'
						) }
					/>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Description Spacing', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Description Spacing',
									'product-blocks'
								),
								options: filterFields(
									[ 'ShortDescPadding' ],
									'__all',
									ShortDescStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ colorIcon }
						store={ store }
						label={ __( 'Description Color', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Description Color',
									'product-blocks'
								),
								options: filterFields(
									[ 'ShortDescColor' ],
									'__all',
									ShortDescStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Description Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Description Settings',
									'product-blocks'
								),
								options: filterFields(
									null,
									[
										'ShortDescTypo',
										'ShortDescPadding',
										'ShortDescColor',
										'showFullShortDesc',
									],
									ShortDescStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'cart':
			return (
				<WopbToolbarGroup text={ 'Cart' }>
					<ToolbarSort store={ store } data={ 'cart' } />
					<TypographyTB
						store={ store }
						attrKey={ 'cartTypo' }
						label={ __( 'Cart Typography', 'product-blocks' ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Cart Spacing', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Cart Spacing', 'product-blocks' ),
								options: filterFields(
									[ 'cartSpacing', 'cartPadding' ],
									'__all',
									CartStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Cart Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Cart Style', 'product-blocks' ),
								options: filterFields(
									[ 'cartTab' ],
									'__all',
									CartStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Cart Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Cart Settings', 'product-blocks' ),
								options: filterFields(
									[ 'cartText', 'cartActive' ],
									'__all',
									CartStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'review':
			return (
				<WopbToolbarGroup text={ 'Review' }>
					<ToolbarSort store={ store } data={ 'review' } />
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Review Margin', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Review Margin', 'product-blocks' ),
								options: filterFields(
									[ 'reviewMargin' ],
									'__all',
									ReviewStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ colorIcon }
						store={ store }
						label={ __( 'Review Color', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Review Color', 'product-blocks' ),
								options: filterFields(
									[ 'reviewEmptyColor', 'reviewFillColor' ],
									'__all',
									ReviewStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'pagination':
			return (
				<WopbToolbarGroup text={ 'Pagination' }>
					<TypographyTB
						store={ store }
						attrKey={ 'pagiTypo' }
						label={ __(
							'Pagination Typography',
							'product-blocks'
						) }
					/>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Pagination Spacing', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Pagination Spacing',
									'product-blocks'
								),
								options: filterFields(
									[
										'pagiMargin',
										'navMargin',
										'pagiPadding',
									],
									'__all',
									PaginationStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Pagination Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Pagination Style',
									'product-blocks'
								),
								options: filterFields(
									[ 'pagiTab' ],
									'__all',
									PaginationStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Pagination Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Pagination Settings',
									'product-blocks'
								),
								options: filterFields(
									null,
									[
										'pagiTypo',
										'pagiMargin',
										'navMargin',
										'pagiPadding',
										'pagiTab',
									],
									PaginationStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'price':
			return (
				<WopbToolbarGroup text={ 'Price' }>
					<ToolbarSort store={ store } data={ 'price' } />
					<TypographyTB
						store={ store }
						attrKey={ 'priceTypo' }
						label={ __( 'Price Typography', 'product-blocks' ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Price Padding', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Price Spacing', 'product-blocks' ),
								options: filterFields(
									[ 'pricePadding' ],
									'__all',
									PriceStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ colorIcon }
						store={ store }
						label={ __( 'Price Color', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Price Color', 'product-blocks' ),
								options: filterFields(
									[ 'priceColor' ],
									'__all',
									PriceStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'image':
			return (
				<WopbToolbarGroup text={ 'Image' }>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Image Dimension', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Image Dimension',
									'product-blocks'
								),
								options: filterFields(
									[ 'imgWidth', 'imgHeight', 'imgMargin' ],
									'__all',
									ImageStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Image Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Image Style', 'product-blocks' ),
								options: filterFields(
									null,
									[
										'imgOverlay',
										'imgOverlayType',
										'overlayColor',
										'imgOpacity',
										'imgWidth',
										'imgHeight',
										'imgMargin',
										'imgCrop',
										'imgAnimation',
									],
									ImageStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Image Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Image Settings', 'product-blocks' ),
								options: filterFields(
									[
										'imgCrop',
										'imgAnimation',
										{
											position: 0,
											data: {
												type: 'toggle',
												key: 'imgFlip',
												label: __(
													'Image Flip',
													'product-blocks'
												),
											},
										},
										{
											data: {
												type: 'tag',
												key: 'imageScale',
												label: __(
													'Image Scale',
													'product-blocks'
												),
												options: [
													{
														value: '',
														label: __(
															'None',
															'product-blocks'
														),
													},
													{
														value: 'cover',
														label: __(
															'Cover',
															'product-blocks'
														),
													},
													{
														value: 'contain',
														label: __(
															'Contain',
															'product-blocks'
														),
													},
													{
														value: 'fill',
														label: __(
															'Fill',
															'product-blocks'
														),
													},
													{
														value: 'scale-down',
														label: __(
															'Scale Down',
															'product-blocks'
														),
													},
												],
											},
										},
									],
									'__all',
									ImageStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'heading':
			return (
				<WopbToolbarGroup text={ 'Heading' }>
					<WopbToolbarDropdown
						buttonContent={ typoIcon }
						store={ store }
						label={ __( 'Heading Typography', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Heading Typography',
									'product-blocks'
								),
								options: filterFields(
									[ 'headingTypo', 'subHeadingTypo' ],
									'__all',
									HeadingSettingsArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Heading Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Heading Style', 'product-blocks' ),
								options: filterFields(
									null,
									[ 'headingTypo', 'subHeadingTypo' ],
									HeadingSettingsArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'arrow':
			return (
				<WopbToolbarGroup text={ 'Arrows' }>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Arrow Dimension', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Arrow Dimension',
									'product-blocks'
								),
								options: filterFields(
									null,
									[ 'arrowStyle', 'aTab' ],
									ArrowStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Arrow Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Arrow Style', 'product-blocks' ),
								options: filterFields(
									[ 'arrowStyle', 'aTab' ],
									'__all',
									ArrowStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'dot':
			return (
				<WopbToolbarGroup text={ 'Dots' }>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Dots Dimension', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Dots Dimension', 'product-blocks' ),
								options: filterFields(
									null,
									[ 'dTab' ],
									DotStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Dots Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Arrow Style', 'product-blocks' ),
								options: filterFields(
									[ 'dTab' ],
									'__all',
									DotStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		default:
			return null;
	}
}
