const attributes = {
	blockId: { type: 'string', default: '' },
	currentPostId: { type: 'string', default: '' },
	previewId: { type: 'string', default: '' },
	initPremade: {
		type: 'boolean',
		default: false,
	},
	sortSection: {
		type: 'string',
		default: '["title","description","price","review","cart"]',
	},
	productView: { type: 'string', default: 'grid' },
	columns: {
		type: 'object',
		default: { lg: '2', sm: '1' },
		style: [
			{
				depends: [
					{ key: 'productView', condition: '==', value: 'grid' },
				],
				selector:
					'{{WOPB}} .wopb-block-items-wrap { grid-template-columns: repeat({{columns}}, 1fr); }',
			},
		],
	},
	columnGridGap: {
		type: 'object',
		default: { lg: '30', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'productView', condition: '==', value: 'grid' },
				],
				selector:
					'{{WOPB}} .wopb-block-items-wrap { grid-column-gap: {{columnGridGap}}; }',
			},
		],
	},
	rowGap: {
		type: 'object',
		default: { lg: '30', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'productView', condition: '==', value: 'grid' },
				],
				selector:
					'{{WOPB}} .wopb-block-items-wrap { grid-row-gap: {{rowGap}}; }',
			},
		],
	},
	columnGap: {
		type: 'object',
		default: { lg: '10', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'productView', condition: '==', value: 'slide' },
				],
				selector:
					'{{WOPB}} .wopb-product-blocks-slide .wopb-block-item { padding: {{columnGap}}; box-sizing:border-box; }',
			},
		],
	},
	slidesToShow: {
		type: 'object',
		default: { lg: '4', sm: '2', xs: '1' },
		style: [
			{
				depends: [
					{ key: 'productView', condition: '==', value: 'slide' },
				],
			},
		],
	},
	autoPlay: {
		type: 'boolean',
		default: true,
		style: [
			{
				depends: [
					{ key: 'productView', condition: '==', value: 'slide' },
				],
			},
		],
	},
	showDots: {
		type: 'boolean',
		default: true,
		style: [
			{
				depends: [
					{ key: 'productView', condition: '==', value: 'slide' },
				],
			},
		],
	},
	showArrows: {
		type: 'boolean',
		default: true,
		style: [
			{
				depends: [
					{ key: 'productView', condition: '==', value: 'slide' },
				],
			},
		],
	},
	slideSpeed: {
		type: 'string',
		default: '3000',
		style: [
			{
				depends: [
					{ key: 'productView', condition: '==', value: 'slide' },
				],
			},
		],
	},
	showPrice: { type: 'boolean', default: true },
	showReview: { type: 'boolean', default: true },
	showCart: { type: 'boolean', default: false },
	filterShow: { type: 'boolean', default: false },
	paginationShow: { type: 'boolean', default: false },
	headingShow: { type: 'boolean', default: false },
	titleShow: { type: 'boolean', default: true },
	showShortDesc: { type: 'boolean', default: false },
	showImage: { type: 'boolean', default: true },
	disableFlip: {
		type: 'boolean',
		default: false,
		style: [
			{ depends: [ { key: 'showImage', condition: '==', value: true } ] },
		],
	},
	stackOnMobile: {
		type: 'boolean',
		default: false,
	},
	contentAlign: {
		type: 'string',
		default: 'left',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-content-wrap { text-align:{{contentAlign}}; }',
			},
		],
	},
	queryType: { type: 'string', default: 'product' },
	queryNumber: { type: 'string', default: 8 },
	queryStatus: {
		type: 'string',
		default: 'all',
		style: [
			{
				depends: [
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryExcludeStock: {
		type: 'boolean',
		default: false,
		style: [
			{
				depends: [
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryCat: {
		type: 'string',
		default: '[]',
		style: [
			{
				depends: [
					{ key: 'filterShow', condition: '!=', value: true },
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryOrderBy: { type: 'string', default: 'date' },
	queryOrder: { type: 'string', default: 'desc' },
	queryInclude: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryExclude: {
		type: 'string',
		default: '[]',
		style: [
			{
				depends: [
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryOffset: {
		type: 'string',
		default: '0',
		style: [
			{
				depends: [
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryQuick: { type: 'string', default: '' },
	queryProductSort: { type: 'string', default: 'null' },
	querySpecificProduct: {
		type: 'string',
		default: '[]',
		style: [
			{
				depends: [
					{
						key: 'queryProductSort',
						condition: '==',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryAdvanceProductSort: {
		type: 'string',
		default: 'null',
		style: [
			{
				depends: [
					{ key: 'queryProductSort', condition: '==', value: 'null' },
				],
			},
		],
	},
	queryTax: {
		type: 'string',
		default: 'product_cat',
		style: [
			{
				depends: [
					{
						key: 'queryType',
						condition: '!=',
						value: [ 'customPosts', 'posts', 'archiveBuilder' ],
					},
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryTaxValue: {
		type: 'string',
		default: '[]',
		style: [
			{
				depends: [
					{ key: 'queryTax', condition: '!=', value: '' },
					{
						key: 'queryType',
						condition: '!=',
						value: [ 'customPosts', 'posts', 'archiveBuilder' ],
					},
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryRelation: {
		type: 'string',
		default: 'OR',
		style: [
			{
				depends: [
					{ key: 'queryTaxValue', condition: '!=', value: '[]' },
					{
						key: 'queryType',
						condition: '!=',
						value: [ 'customPosts', 'posts', 'archiveBuilder' ],
					},
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryIncludeAuthor: {
		type: 'string',
		default: '[]',
		style: [
			{
				depends: [
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryExcludeAuthor: {
		type: 'string',
		default: '[]',
		style: [
			{
				depends: [
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryStockStatus: {
		type: 'string',
		default: '[]',
		style: [
			{
				depends: [
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	contentWrapVerticalAlign: {
		type: 'string',
		default: { lg: 'start' },
		style: [
			{
				depends: [
					{ key: 'stackOnMobile', condition: '==', value: false },
				],
				selector:
					'{{WOPB}} .wopb-block-item .wopb-block-content-wrap { align-items: {{contentWrapVerticalAlign}}; }',
			},
			{
				depends: [
					{ key: 'stackOnMobile', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-block-item .wopb-block-content-wrap { align-items: {{contentWrapVerticalAlign}}; } ' +
					'@media screen and (max-width:768px) { ' +
					'{{WOPB}} .wopb-block-item .wopb-block-content-wrap { flex-direction: column; text-align: {{contentWrapVerticalAlign}}; }' +
					'}',
			},
		],
	},
	contentWrapBgColor: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '' },
		style: [
			{ selector: '{{WOPB}} .wopb-block-item .wopb-block-content-wrap' },
		],
	},
	contentWrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#e0e0e0',
			type: 'solid',
		},
		style: [
			{ selector: '{{WOPB}} .wopb-block-item .wopb-block-content-wrap' },
		],
	},
	contentWrapRadius: {
		type: 'object',
		default: {
			lg: { top: '0', bottom: '0', left: '0', right: '0', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-item .wopb-block-content-wrap { border-radius:{{contentWrapRadius}}; }',
			},
		],
	},
	contentWrapPadding: {
		type: 'object',
		default: {
			lg: { top: '0', bottom: '0', left: '0', right: '0', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-item .wopb-block-content-wrap { padding:{{contentWrapPadding}}; }',
			},
		],
	},
	arrowStyle: {
		type: 'string',
		default: 'leftAngle2#rightAngle2',
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
			},
		],
	},
	arrowSize: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .slick-next svg, {{WOPB}} .slick-prev svg { width:{{arrowSize}}; }',
			},
		],
	},
	arrowWidth: {
		type: 'object',
		default: { lg: '60', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector: '{{WOPB}} .slick-arrow { width:{{arrowWidth}}; }',
			},
		],
	},
	arrowHeight: {
		type: 'object',
		default: { lg: '60', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .slick-arrow { height:{{arrowHeight}}; } {{WOPB}} .slick-arrow { line-height:{{arrowHeight}}; }',
			},
		],
	},
	arrowVartical: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .slick-next { right:{{arrowVartical}}; } {{WOPB}} .slick-prev { left:{{arrowVartical}}; }',
			},
		],
	},
	arrowHorizontal: {
		type: 'object',
		default: { lg: '' },
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-next, {{WOPB}} .slick-prev { top:{{arrowHorizontal}}; }',
			},
		],
	},
	arrowColor: {
		type: 'string',
		default: '#ffffff',
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .slick-arrow:before { color:{{arrowColor}}; } {{WOPB}} .slick-arrow svg { fill:{{arrowColor}}; }',
			},
		],
	},
	arrowHoverColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .slick-arrow:hover:before { color:{{arrowHoverColor}}; } {{WOPB}} .slick-arrow:hover svg { fill:{{arrowHoverColor}}; }',
			},
		],
	},
	arrowBg: {
		type: 'string',
		default: 'rgba(0,0,0,0.22)',
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector: '{{WOPB}} .slick-arrow { background:{{arrowBg}}; }',
			},
		],
	},
	arrowHoverBg: {
		type: 'string',
		default: '#ff176b',
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .slick-arrow:hover { background:{{arrowHoverBg}}; }',
			},
		],
	},
	arrowBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector: '{{WOPB}} .slick-arrow',
			},
		],
	},
	arrowHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector: '{{WOPB}} .slick-arrow:hover',
			},
		],
	},
	arrowRadius: {
		type: 'object',
		default: {
			lg: {
				top: '50',
				bottom: '50',
				left: '50',
				right: '50',
				unit: 'px',
			},
		},
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .slick-arrow { border-radius: {{arrowRadius}}; }',
			},
		],
	},
	arrowHoverRadius: {
		type: 'object',
		default: {
			lg: {
				top: '50',
				bottom: '50',
				left: '50',
				right: '50',
				unit: 'px',
			},
		},
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .slick-arrow:hover{ border-radius: {{arrowHoverRadius}}; }',
			},
		],
	},
	arrowShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector: '{{WOPB}} .slick-arrow',
			},
		],
	},
	arrowHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector: '{{WOPB}} .slick-arrow:hover',
			},
		],
	},
	dotSpace: {
		type: 'object',
		default: { lg: '4', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-dots { padding: 0 {{dotSpace}}; } {{WOPB}} .slick-dots li button { margin: 0 {{dotSpace}}; }',
			},
		],
	},
	dotVartical: {
		type: 'object',
		default: { lg: '-50', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector: '{{WOPB}} .slick-dots { bottom:{{dotVartical}}; }',
			},
		],
	},
	dotHorizontal: {
		type: 'object',
		default: { lg: '' },
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector: '{{WOPB}} .slick-dots { left:{{dotHorizontal}}; }',
			},
		],
	},
	dotWidth: {
		type: 'object',
		default: { lg: '10', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-dots li button { width:{{dotWidth}}; }',
			},
		],
	},
	dotHeight: {
		type: 'object',
		default: { lg: '10', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-dots li button { height:{{dotHeight}}; }',
			},
		],
	},
	dotHoverWidth: {
		type: 'object',
		default: { lg: '16', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-dots li.slick-active button { width:{{dotHoverWidth}}; }',
			},
		],
	},
	dotHoverHeight: {
		type: 'object',
		default: { lg: '16', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-dots li.slick-active button { height:{{dotHoverHeight}}; }',
			},
		],
	},
	dotBg: {
		type: 'string',
		default: '#f5f5f5',
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-dots li button { background:{{dotBg}}; }',
			},
		],
	},
	dotHoverBg: {
		type: 'string',
		default: '#000',
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-dots li button:hover, {{WOPB}} .slick-dots li.slick-active button { background:{{dotHoverBg}}; }',
			},
		],
	},
	dotBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector: '{{WOPB}} .slick-dots li button',
			},
		],
	},
	dotHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-dots li button:hover, {{WOPB}} .slick-dots li.slick-active button',
			},
		],
	},
	dotRadius: {
		type: 'object',
		default: {
			lg: {
				top: '50',
				bottom: '50',
				left: '50',
				right: '50',
				unit: 'px',
			},
		},
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-dots li button { border-radius: {{dotRadius}}; }',
			},
		],
	},
	dotHoverRadius: {
		type: 'object',
		default: {
			lg: {
				top: '50',
				bottom: '50',
				left: '50',
				right: '50',
				unit: 'px',
			},
		},
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-dots li button:hover, {{WOPB}} .slick-dots li.slick-active button { border-radius: {{dotHoverRadius}}; }',
			},
		],
	},
	dotShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector: '{{WOPB}} .slick-dots li button',
			},
		],
	},
	dotHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-dots li button:hover, {{WOPB}} .slick-dots li.slick-active button',
			},
		],
	},
	headingText: { type: 'string', default: 'Product List #1' },
	headingURL: { type: 'string', default: '' },
	headingBtnText: {
		type: 'string',
		default: 'View More',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style11' },
				],
			},
		],
	},
	headingStyle: { type: 'string', default: 'style1' },
	headingTag: { type: 'string', default: 'h2' },
	headingAlign: {
		type: 'string',
		default: 'left',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-heading-inner, {{WOPB}} .wopb-sub-heading-inner { text-align:{{headingAlign}}; }',
			},
		],
	},
	headingTypo: {
		type: 'object',
		default: {
			openTypography: 0,
			size: { lg: '20', unit: 'px' },
			height: { lg: '', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{ selector: '{{WOPB}} .wopb-heading-wrap .wopb-heading-inner' },
		],
	},
	headingColor: {
		type: 'string',
		default: '#0e1523',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { color:{{headingColor}}; }',
			},
		],
	},
	headingBorderBottomColor: {
		type: 'string',
		default: '#0e1523',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style3' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { border-bottom-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style4' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { border-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style6' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before { background-color: {{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style7' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before, {{WOPB}} .wopb-heading-inner span:after { background-color: {{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style8' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:after { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style9' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style10' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style13' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { border-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style14' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style15' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style16' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style17' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style18' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:after { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style19' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { border-color:{{headingBorderBottomColor}}; }',
			},
		],
	},
	headingBorderBottomColor2: {
		type: 'string',
		default: '#e5e5e5',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style8' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { background-color:{{headingBorderBottomColor2}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style10' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style14' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style19' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }',
			},
		],
	},
	headingBg: {
		type: 'string',
		default: '#ff176b',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style5' },
				],
				selector:
					'{{WOPB}} .wopb-heading-style5 .wopb-heading-inner span:before { border-color:{{headingBg}} transparent transparent; } {{WOPB}} .wopb-heading-inner span { background-color:{{headingBg}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style2' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span { background-color:{{headingBg}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style3' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span { background-color:{{headingBg}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style12' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span { background-color:{{headingBg}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style13' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span { background-color:{{headingBg}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style18' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { background-color:{{headingBg}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style20' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before { border-color:{{headingBg}} transparent transparent; } {{WOPB}} .wopb-heading-inner { background-color:{{headingBg}}; }',
			},
		],
	},
	headingBg2: {
		type: 'string',
		default: '#e5e5e5',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style12' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { background-color:{{headingBg2}}; }',
			},
		],
	},
	headingBtnTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '', unit: 'px' },
			decoration: 'none',
			family: '',
		},
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style11' },
				],
				selector: '{{WOPB}} .wopb-heading-btn',
			},
		],
	},
	headingBtnColor: {
		type: 'string',
		default: '#ff176b',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style11' },
				],
				selector:
					'{{WOPB}} .wopb-heading-btn { color:{{headingBtnColor}}; } {{WOPB}} .wopb-heading-btn svg { fill:{{headingBtnColor}}; }',
			},
		],
	},
	headingBtnHoverColor: {
		type: 'string',
		default: '#0a31da',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style11' },
				],
				selector:
					'{{WOPB}} .wopb-heading-btn:hover { color:{{headingBtnHoverColor}}; } {{WOPB}} .wopb-heading-btn:hover svg { fill:{{headingBtnHoverColor}}; }',
			},
		],
	},
	headingBorder: {
		type: 'string',
		default: '3',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style3' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { border-bottom-width:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style4' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { border-width:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style6' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before { width:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style7' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before, {{WOPB}} .wopb-heading-inner span:after { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style8' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before, {{WOPB}} .wopb-heading-inner:after { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style9' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style10' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before, {{WOPB}} .wopb-heading-inner:after { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style13' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { border-width:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style14' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before, {{WOPB}} .wopb-heading-inner:after { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style15' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style16' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style17' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style19' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:after { width:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style18' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:after { width:{{headingBorder}}px; }',
			},
		],
	},
	headingSpacing: {
		type: 'object',
		default: { lg: 25, unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-heading-wrap {margin-top:0; margin-bottom:{{headingSpacing}}; }',
			},
		],
	},
	headingRadius: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style2' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { border-radius:{{headingRadius}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style3' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { border-radius:{{headingRadius}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style4' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner { border-radius:{{headingRadius}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style5' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { border-radius:{{headingRadius}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style12' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner { border-radius:{{headingRadius}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style13' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner { border-radius:{{headingRadius}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style18' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner { border-radius:{{headingRadius}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style20' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner { border-radius:{{headingRadius}}; }',
			},
		],
	},
	headingPadding: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style2' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style3' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style4' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style5' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style6' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style12' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style13' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style18' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style19' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style20' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
		],
	},
	subHeadingShow: { type: 'boolean', default: false },
	subHeadingText: {
		type: 'string',
		default:
			'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut sem augue. Sed at felis ut enim dignissim sodales.',
		style: [
			{
				depends: [
					{ key: 'subHeadingShow', condition: '==', value: true },
				],
			},
		],
	},
	subHeadingTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			spacing: { lg: '0', unit: 'px' },
			height: { lg: '27', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			family: '',
			weight: '500',
		},
		style: [
			{
				depends: [
					{ key: 'subHeadingShow', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-sub-heading div',
			},
		],
	},
	subHeadingColor: {
		type: 'string',
		default: '#989898',
		style: [
			{
				depends: [
					{ key: 'subHeadingShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-sub-heading div{ color:{{subHeadingColor}}; }',
			},
		],
	},
	subHeadingSpacing: {
		type: 'object',
		default: { lg: { unit: 'px' } },
		style: [
			{
				depends: [
					{ key: 'subHeadingShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} div.wopb-sub-heading-inner{ margin:{{subHeadingSpacing}}; }',
			},
		],
	},
	priceColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				depends: [ { key: 'showPrice', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-price { color:{{priceColor}}; }',
			},
		],
	},
	priceTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			spacing: { lg: '0', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			family: '',
			weight: '',
		},
		style: [
			{
				depends: [ { key: 'showPrice', condition: '==', value: true } ],
				selector: '{{WOPB}} .wopb-product-price',
			},
		],
	},
	pricePadding: {
		type: 'object',
		default: { lg: { top: 0, bottom: 10, unit: 'px' } },
		style: [
			{
				depends: [ { key: 'showPrice', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-price { padding:{{pricePadding}}; }',
			},
		],
	},
	reviewEmptyColor: {
		type: 'string',
		default: '#E0E0E0',
		style: [
			{
				depends: [
					{ key: 'showReview', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-star-rating:before { color:{{reviewEmptyColor}}; }',
			},
		],
	},
	reviewFillColor: {
		type: 'string',
		default: '#FFAF38',
		style: [
			{
				depends: [
					{ key: 'showReview', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-star-rating span:before { color:{{reviewFillColor}}; }',
			},
		],
	},
	reviewMargin: {
		type: 'object',
		default: { lg: { top: 0, bottom: 0, unit: 'px' } },
		style: [
			{
				depends: [
					{ key: 'showReview', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-star-rating { margin:{{reviewMargin}}; }',
			},
		],
	},
	shortDescLimit: { type: 'string', default: 7 },
	ShortDescTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			spacing: { lg: '0', unit: 'px' },
			height: { lg: '22', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			family: '',
			weight: '',
		},
		style: [
			{
				depends: [
					{ key: 'showShortDesc', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-short-description',
			},
		],
	},
	ShortDescColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				depends: [
					{ key: 'showShortDesc', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-short-description { color:{{ShortDescColor}}; }',
			},
		],
	},
	ShortDescPadding: {
		type: 'object',
		default: { lg: { top: 0, bottom: 6, unit: 'px' } },
		style: [
			{
				depends: [
					{ key: 'showShortDesc', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-short-description { padding:{{ShortDescPadding}}; }',
			},
		],
	},
	titleTag: { type: 'string', default: 'h3' },
	titleLength: { type: 'string', default: 0 },
	titleTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '18', unit: 'px' },
			spacing: { lg: '0', unit: 'px' },
			height: { lg: '24', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{
				depends: [ { key: 'titleShow', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-title, {{WOPB}} .wopb-block-title a',
			},
		],
	},
	titleColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				depends: [ { key: 'titleShow', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-title a { color:{{titleColor}}; }',
			},
		],
	},
	titleHoverColor: {
		type: 'string',
		default: '#828282',
		style: [
			{
				depends: [ { key: 'titleShow', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-title a:hover { color:{{titleHoverColor}}; }',
			},
		],
	},
	titlePadding: {
		type: 'object',
		default: { lg: { top: 0, bottom: 10, unit: 'px' } },
		style: [
			{
				depends: [ { key: 'titleShow', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-title { padding:{{titlePadding}}; }',
			},
		],
	},
	cartNoFollow: { type: 'boolean', default: true },
	cartText: { type: 'string', default: '' },
	cartActive: { type: 'string', default: 'View Cart' },
	cartTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: 12, unit: 'px' },
			height: { lg: '24', unit: 'px' },
			spacing: { lg: 0, unit: 'px' },
			transform: 'uppercase',
			weight: '400',
			decoration: 'none',
			family: '',
		},
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector: '{{WOPB}} .wopb-block-content .wopb-product-btn a',
			},
		],
	},
	cartColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-content .wopb-product-btn a { color:{{cartColor}}; } {{WOPB}}  .wopb-block-content .wopb-product-btn a svg path { stroke:{{cartColor}}; }',
			},
		],
	},
	cartBgColor: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '' },
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector: '{{WOPB}} .wopb-block-content .wopb-product-btn a',
			},
		],
	},
	cartBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#070707',
			type: 'solid',
		},
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector: '{{WOPB}} .wopb-block-content .wopb-product-btn a',
			},
		],
	},
	cartRadius: {
		type: 'object',
		default: { lg: '0', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-content .wopb-product-btn a { border-radius:{{cartRadius}}; }',
			},
		],
	},
	cartHoverColor: {
		type: 'string',
		default: '#e63838',
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-content .wopb-product-btn a:hover { color:{{cartHoverColor}}; } {{WOPB}} .wopb-block-content .wopb-product-btn a:hover svg path { stroke:{{cartHoverColor}}; }',
			},
		],
	},
	cartBgHoverColor: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#ff176b' },
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-content .wopb-product-btn a:hover',
			},
		],
	},
	cartHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#ff176b',
			type: 'solid',
		},
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-content .wopb-product-btn a:hover',
			},
		],
	},
	cartHoverRadius: {
		type: 'object',
		default: { lg: '0', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-content .wopb-product-btn a:hover { border-radius:{{cartHoverRadius}}; }',
			},
		],
	},
	cartSpacing: {
		type: 'object',
		default: {
			lg: { top: 4, bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-content .wopb-product-btn { margin:{{cartSpacing}}; }',
			},
		],
	},
	cartPadding: {
		type: 'object',
		default: {
			lg: { top: '4', bottom: '4', left: '12', right: '12', unit: 'px' },
		},
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-content .wopb-product-btn a { padding:{{cartPadding}}; }',
			},
		],
	},
	//      ===  Overlay Meta start ===
	hoverMeta: {
		type: 'boolean',
		default: true,
	},
	ovrMetaInline: {
		type: 'boolean',
		default: true,
	},
	ovrMetaElemSpace: {
		type: 'object',
		default: { lg: '4', sm: '4', xs: '4', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-new-meta { gap:{{ovrMetaElemSpace}}; }',
			},
		],
	},
	tooltipPosition: {
		type: 'string',
		default: 'left',
	},
	overlayMetaHoriAlign: {
		type: 'string',
		default: 'flex-end',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-content-wrap .wopb-block-image { justify-content: {{overlayMetaHoriAlign}}; }',
			},
		],
	},
	overlayMetaVerAlign: {
		type: 'string',
		default: 'flex-start',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-content-wrap .wopb-block-image { align-items: {{overlayMetaVerAlign}}; }',
			},
		],
	},
	overlayMetaList: {
		type: 'string',
		default: '["_cart"]',
	},

	//      === Overlay Meta Background ===
	ovrMetaBg: {
		type: 'object',
		default: {
			openColor: 0,
			type: 'color',
			color: '#fff',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-new-meta',
			},
		],
	},
	ovrMetaBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#d5d5d5',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-new-meta',
			},
		],
	},
	ovrMetaShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#d5d5d5',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-new-meta',
			},
		],
	},
	ovrMetaBorderRad: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-new-meta { border-radius:{{ovrMetaBorderRad}}; }',
			},
		],
	},

	//      === Overlay Meta Background  Hover ===
	ovrMetaHvrBg: {
		type: 'object',
		default: {
			openColor: 0,
			type: 'color',
			color: '#fff',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-new-meta:hover',
			},
		],
	},
	ovrMetaHvrBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#d5d5d5',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-new-meta:hover',
			},
		],
	},
	ovrMetaHvrShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#d5d5d5',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-new-meta:hover',
			},
		],
	},
	ovrMetaHvrBorderRad: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-new-meta:hover { border-radius:{{ovrMetaHvrBorderRad}}; }',
			},
		],
	},
	ovrMetaPadding: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-new-meta { padding:{{ovrMetaPadding}}; }',
			},
		],
	},
	ovrMetaSpacing: {
		type: 'object',
		default: {
			lg: { top: 6, right: 6, bottom: 6, left: 6, unit: 'px' },
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-new-meta { margin:{{ovrMetaSpacing}}; }',
			},
		],
	},

	//      === Overlay Meta Element ===
	ovrMetaElemClr: {
		type: 'string',
		default: '#000',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-new-meta svg { color: {{ovrMetaElemClr}}; }',
			},
		],
	},
	ovrMetaElemBg: {
		type: 'object',
		default: {
			openColor: 1,
			type: 'color',
			color: '#fff',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-new-meta .wopb_meta_svg_con',
			},
		],
	},
	ovrMetaElemBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#d5d5d5',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-new-meta .wopb_meta_svg_con',
			},
		],
	},
	ovrMetaElemShadow: {
		type: 'object',
		default: {
			openShadow: 1,
			width: { top: 1, right: 1, bottom: 2, left: 0, unit: 'px' },
			color: 'rgba(0,0,0,0.16)',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-new-meta .wopb_meta_svg_con',
			},
		],
	},
	ovrMetaElemBorderRad: {
		type: 'object',
		default: {
			lg: { top: 30, right: 30, bottom: 30, left: 30 },
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-new-meta .wopb_meta_svg_con { border-radius:{{ovrMetaElemBorderRad}}; }',
			},
		],
	},

	//      === Overlay Meta Element Hover ===
	ovrMetaElemHvrClr: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-new-meta .wopb_meta_svg_con:hover svg { color: {{ovrMetaElemHvrClr}}; }',
			},
		],
	},
	ovrMetaElemHvrBg: {
		type: 'object',
		default: {
			openColor: 1,
			type: 'color',
			color: '#070707',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-new-meta .wopb_meta_svg_con:hover ',
			},
		],
	},
	ovrMetaElemHvrBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#d5d5d5',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-new-meta .wopb_meta_svg_con:hover',
			},
		],
	},
	ovrMetaElemHvrShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#d5d5d5',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-new-meta .wopb_meta_svg_con:hover',
			},
		],
	},
	ovrMetaElemHvrBorderRad: {
		type: 'object',
		default: {
			lg: { top: 30, right: 30, bottom: 30, left: 30 },
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-new-meta .wopb_meta_svg_con:hover { border-radius:{{ovrMetaElemHvrBorderRad}}; }',
			},
		],
	},
	ovrMetaElemWrapSize: {
		type: 'object',
		default: { lg: '32', sm: '26', xs: '22', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-new-meta .wopb_meta_svg_con { height:{{ovrMetaElemWrapSize}}!important; width:{{ovrMetaElemWrapSize}}!important; }',
			},
		],
	},
	ovrMetaElemSize: {
		type: 'object',
		default: { lg: '18', sm: '14', xs: '12', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-new-meta .wopb_meta_svg_con svg { height:{{ovrMetaElemSize}}; width:{{ovrMetaElemSize}}; }',
			},
		],
	},
	//      ===  Overlay Meta Ends ===

	imgFlip: {
		type: 'boolean',
		default: false,
		style: [
			{
				depends: [ { key: 'imgFlip', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-content-true .wopb-block-media, {{WOPB}} .wopb-block-content-1 .wopb-block-media { flex-direction: row-reverse; }',
			},
		],
	},
	imgCrop: {
		type: 'string',
		default: 'full',
		depends: [ { key: 'showImage', condition: '==', value: 'true' } ],
	},
	imgWidth: {
		type: 'object',
		default: { lg: '96', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-item .wopb-block-image { max-width: {{imgWidth}}; }',
			},
		],
	},
	imgHeight: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-item .wopb-block-image img { height: {{imgHeight}}; }',
			},
		],
	},
	imageScale: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-item .wopb-block-image img {object-fit: {{imageScale}};}',
			},
		],
	},
	imgAnimation: { type: 'string', default: 'none' },
	imgGrayScale: {
		type: 'object',
		default: { lg: '0', unit: '%' },
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-image img { filter: grayscale({{imgGrayScale}}); }',
			},
		],
	},
	imgHoverGrayScale: {
		type: 'object',
		default: { lg: '0', unit: '%' },
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-item:hover .wopb-block-image img { filter: grayscale({{imgHoverGrayScale}}); }',
			},
		],
	},
	imgRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-image , {{WOPB}} .wopb-block-image img { border-radius:{{imgRadius}}; }',
			},
		],
	},
	imgHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-item:hover .wopb-block-image , {{WOPB}} .wopb-block-item:hover .wopb-block-image img { border-radius:{{imgHoverRadius}}; }',
			},
		],
	},
	imgShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector: '{{WOPB}} .wopb-block-image img',
			},
		],
	},
	imgHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-item:hover .wopb-block-image img',
			},
		],
	},
	imgMargin: {
		type: 'object',
		default: {
			lg: { top: '0', right: '20', bottom: '0', left: '0', unit: 'px' },
		},
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-item .wopb-block-image { overflow: visible; margin: {{imgMargin}}; }',
			},
		],
	},
	filterType: { type: 'string', default: 'product_cat' },
	filterText: { type: 'string', default: 'all' },
	filterCat: {
		type: 'string',
		default: '[]',
		style: [
			{
				depends: [
					{
						key: 'filterType',
						condition: '==',
						value: 'product_cat',
					},
				],
			},
		],
	},
	filterTag: {
		type: 'string',
		default: '["all"]',
		style: [
			{
				depends: [
					{
						key: 'filterType',
						condition: '==',
						value: 'product_tag',
					},
				],
			},
		],
	},
	filterAction: { type: 'string', default: '[]' },
	filterActionText: {
		type: 'string',
		default:
			'Top Sale|Popular|On Sale|Most Rated|Top Rated|Featured|New Arrival',
		style: [
			{
				depends: [
					{ key: 'filterAction', condition: '!=', value: '[]' },
				],
			},
		],
	},
	filterBelowTitle: {
		type: 'boolean',
		default: false,
		style: [
			{
				depends: [
					{ key: 'filterShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-heading-filter .wopb-filter-navigation { position: relative; display: block;margin: auto 0 0 0; }',
			},
		],
	},
	filterAlign: {
		type: 'object',
		default: { lg: 'center' },
		style: [
			{
				depends: [
					{ key: 'filterBelowTitle', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-heading-filter .wopb-filter-navigation { text-align:{{filterAlign}}; }',
			},
		],
	},
	fliterTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: 14, unit: 'px' },
			height: { lg: 18, unit: 'px' },
			decoration: 'none',
			family: '',
			weight: 500,
		},
		style: [
			{
				depends: [
					{ key: 'filterShow', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-filter-wrap ul li a',
			},
		],
	},
	filterColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				depends: [
					{ key: 'filterShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-filter-wrap > ul > li > a { color:{{filterColor}}; } ' +
					'{{WOPB}} .wopb-filter-wrap > ul > li.flexMenu-viewMore > a:before { border-color:{{filterColor}}; }',
			},
		],
	},
	filterHoverColor: {
		type: 'string',
		default: '#828282',
		style: [
			{
				depends: [
					{ key: 'filterShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-filter-wrap > ul > li:not(.flexMenu-viewMore) > a:hover, {{WOPB}} .wopb-filter-wrap > ul > li:not(.flexMenu-viewMore) > a.filter-active { color:{{filterHoverColor}}; }',
			},
		],
	},
	filterBgColor: {
		type: 'string',
		style: [
			{
				depends: [
					{ key: 'filterShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-filter-wrap > ul > li.filter-item:not(.flexMenu-viewMore) > a { background:{{filterBgColor}}; } ' +
					'{{WOPB}} .wopb-filter-wrap > ul > li.filter-item.flexMenu-viewMore { background:{{filterBgColor}}; }',
			},
		],
	},
	filterHoverBgColor: {
		type: 'string',
		style: [
			{
				depends: [
					{ key: 'filterShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-filter-wrap > ul > li.filter-item:not(.flexMenu-viewMore) > a:hover, ' +
					'{{WOPB}} .wopb-filter-wrap > ul > li.filter-item:not(.flexMenu-viewMore) > a.filter-active { background:{{filterHoverBgColor}}; }',
			},
		],
	},
	filterBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{ key: 'filterShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-filter-wrap > ul > li.filter-item:not(.flexMenu-viewMore) > a, ' +
					'{{WOPB}} .wopb-filter-wrap > ul > li.filter-item.flexMenu-viewMore',
			},
		],
	},
	filterHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{ key: 'filterShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-filter-wrap > ul > li.filter-item:not(.flexMenu-viewMore) > a:hover, ' +
					'{{WOPB}} .wopb-filter-wrap > ul > li.filter-item:not(.flexMenu-viewMore) > a.filter-active',
			},
		],
	},
	filterRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'filterShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-filter-wrap > ul > li.filter-item > a, {{WOPB}} .wopb-filter-wrap > ul > li.filter-item.flexMenu-viewMore { border-radius:{{filterRadius}}; }',
			},
		],
	},
	fliterSpacing: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '0', right: '', left: '20', unit: 'px' },
		},
		style: [
			{
				depends: [
					{ key: 'filterShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-filter-wrap > ul > li { margin:{{fliterSpacing}}; }',
			},
		],
	},
	fliterPadding: {
		type: 'object',
		default: { lg: { top: '', bottom: '', unit: 'px' } },
		style: [
			{
				depends: [
					{ key: 'filterShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-filter-wrap > ul > li.filter-item:not(.flexMenu-viewMore) > a { padding:{{fliterPadding}}; } ' +
					'{{WOPB}} .wopb-filter-wrap > ul > li.filter-item.flexMenu-viewMore { padding:{{fliterPadding}}; }',
			},
		],
	},
	filterDropdownColor: {
		type: 'string',
		default: '#0e1523',
		style: [
			{
				depends: [
					{ key: 'filterShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-filter-wrap ul li.flexMenu-viewMore .flexMenu-popup li a { color:{{filterDropdownColor}}; }',
			},
		],
	},
	filterDropdownHoverColor: {
		type: 'string',
		default: '#ff176b',
		style: [
			{
				depends: [
					{ key: 'filterShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-filter-wrap ul li.flexMenu-viewMore .flexMenu-popup li a:hover { color:{{filterDropdownHoverColor}}; }',
			},
		],
	},
	filterDropdownBg: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [
					{ key: 'filterShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-filter-wrap ul li.flexMenu-viewMore .flexMenu-popup { background:{{filterDropdownBg}}; }',
			},
		],
	},
	filterDropdownRadius: {
		type: 'object',
		default: { lg: '0' },
		style: [
			{
				depends: [
					{ key: 'filterShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-filter-wrap ul li.flexMenu-viewMore .flexMenu-popup { border-radius:{{filterDropdownRadius}}; }',
			},
		],
	},
	filterDropdownPadding: {
		type: 'object',
		default: {
			lg: {
				top: '15',
				bottom: '15',
				left: '20',
				right: '20',
				unit: 'px',
			},
		},
		style: [
			{
				depends: [
					{ key: 'filterShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-filter-wrap ul li.flexMenu-viewMore .flexMenu-popup { padding:{{filterDropdownPadding}}; }',
			},
		],
	},
	filterMobile: { type: 'boolean', default: true },
	filterMobileText: {
		type: 'string',
		default: 'More',
		style: [
			{
				depends: [
					{ key: 'filterMobile', condition: '==', value: true },
				],
			},
		],
	},
	paginationType: { type: 'string', default: 'pagination' },
	loadMoreText: {
		type: 'string',
		default: 'Load More',
		style: [
			{
				depends: [
					{
						key: 'paginationType',
						condition: '==',
						value: 'loadMore',
					},
				],
			},
		],
	},
	paginationText: {
		type: 'string',
		default: 'Previous|Next',
		style: [
			{
				depends: [
					{
						key: 'paginationType',
						condition: '==',
						value: 'pagination',
					},
				],
			},
		],
	},
	paginationNav: {
		type: 'string',
		default: 'textArrow',
		style: [
			{
				depends: [
					{
						key: 'paginationType',
						condition: '==',
						value: 'pagination',
					},
				],
			},
		],
	},
	paginationAjax: {
		type: 'boolean',
		default: true,
		style: [
			{
				depends: [
					{
						key: 'paginationType',
						condition: '==',
						value: 'pagination',
					},
				],
			},
		],
	},
	navPosition: {
		type: 'string',
		default: 'topRight',
		style: [
			{
				depends: [
					{
						key: 'paginationType',
						condition: '==',
						value: 'navigation',
					},
				],
			},
		],
	},
	pagiAlign: {
		type: 'object',
		default: { lg: 'center' },
		style: [
			{
				depends: [
					{ key: 'paginationShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-loadmore, {{WOPB}} .wopb-next-prev-wrap ul, {{WOPB}} .wopb-pagination { text-align:{{pagiAlign}}; }',
			},
		],
	},
	pagiTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: 14, unit: 'px' },
			height: { lg: 20, unit: 'px' },
			decoration: 'none',
			family: '',
		},
		style: [
			{
				depends: [
					{ key: 'paginationShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-pagination li a, {{WOPB}} .wopb-loadmore-action',
			},
		],
	},
	pagiArrowSize: {
		type: 'object',
		default: { lg: '14' },
		style: [
			{
				depends: [
					{
						key: 'paginationType',
						condition: '==',
						value: 'navigation',
					},
				],
				selector:
					'{{WOPB}} .wopb-next-prev-wrap ul li a { font-size:{{pagiArrowSize}}px; }',
			},
		],
	},
	pagiColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [
					{ key: 'paginationShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-pagination li a, {{WOPB}} .wopb-next-prev-wrap ul li a, {{WOPB}} .wopb-loadmore-action { color:{{pagiColor}}; } {{WOPB}} .wopb-next-prev-wrap ul li a svg { fill:{{pagiColor}}; } {{WOPB}} .wopb-pagination li a svg { fill:{{pagiColor}}; }',
			},
		],
	},
	pagiBgColor: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#0e1523' },
		style: [
			{
				depends: [
					{ key: 'paginationShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-pagination li a, {{WOPB}} .wopb-next-prev-wrap ul li a, {{WOPB}} .wopb-loadmore-action',
			},
		],
	},
	pagiBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{ key: 'paginationShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-pagination li a, {{WOPB}} .wopb-next-prev-wrap ul li a, {{WOPB}} .wopb-loadmore-action',
			},
		],
	},
	pagiShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [
					{ key: 'paginationShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-pagination li a, {{WOPB}} .wopb-next-prev-wrap ul li a, {{WOPB}} .wopb-loadmore-action',
			},
		],
	},
	pagiRadius: {
		type: 'object',
		default: {
			lg: { top: '2', bottom: '2', left: '2', right: '2', unit: 'px' },
		},
		style: [
			{
				depends: [
					{ key: 'paginationShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-pagination li a, {{WOPB}} .wopb-next-prev-wrap ul li a, {{WOPB}} .wopb-loadmore-action { border-radius:{{pagiRadius}}; }',
			},
		],
	},
	pagiHoverColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [
					{ key: 'paginationShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-pagination li a:hover, {{WOPB}} .wopb-pagination li.pagination-active a, {{WOPB}} .wopb-next-prev-wrap ul li a:hover, {{WOPB}} .wopb-loadmore-action:hover { color:{{pagiHoverColor}}; } {{WOPB}} .wopb-pagination li a:hover svg { fill:{{pagiHoverColor}}; } {{WOPB}} .wopb-loading-active .wopb-loadmore .wopb-loadmore-action svg { fill:{{pagiHoverColor}}; } {{WOPB}} .wopb-next-prev-wrap ul li a:hover svg { fill:{{pagiHoverColor}}; }',
			},
		],
	},
	pagiHoverbg: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#ff176b' },
		style: [
			{
				depends: [
					{ key: 'paginationShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-pagination li a:hover, {{WOPB}} .wopb-pagination li.pagination-active a, {{WOPB}} .wopb-next-prev-wrap ul li a:hover, {{WOPB}} .wopb-loadmore-action:hover',
			},
		],
	},
	pagiHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{ key: 'paginationShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-pagination li a:hover, {{WOPB}} .wopb-pagination li.pagination-active a, {{WOPB}} .wopb-next-prev-wrap ul li a:hover, {{WOPB}} .wopb-loadmore-action:hover',
			},
		],
	},
	pagiHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [
					{ key: 'paginationShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-pagination li a:hover, {{WOPB}} .wopb-pagination li.pagination-active a, {{WOPB}} .wopb-next-prev-wrap ul li a:hover, {{WOPB}} .wopb-loadmore-action:hover',
			},
		],
	},
	pagiHoverRadius: {
		type: 'object',
		default: {
			lg: { top: '2', bottom: '2', left: '2', right: '2', unit: 'px' },
		},
		style: [
			{
				depends: [
					{ key: 'paginationShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-pagination li a:hover, {{WOPB}} .wopb-next-prev-wrap ul li a:hover, {{WOPB}} .wopb-loadmore-action:hover { border-radius:{{pagiHoverRadius}}; }',
			},
		],
	},
	pagiPadding: {
		type: 'object',
		default: {
			lg: { top: '8', bottom: '8', left: '14', right: '14', unit: 'px' },
		},
		style: [
			{
				depends: [
					{ key: 'paginationShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-pagination li a, {{WOPB}} .wopb-next-prev-wrap ul li a, {{WOPB}} .wopb-loadmore-action { padding:{{pagiPadding}}; }',
			},
		],
	},
	pagiGap: {
		type: 'object',
		default: { lg: '2', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'paginationShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-pagination li:not(:last-child) { margin-right: {{pagiGap}};}',
			},
		],
	},
	navMargin: {
		type: 'object',
		default: {
			lg: { top: '0', right: '0', bottom: '0', left: '0', unit: 'px' },
		},
		style: [
			{
				depends: [
					{
						key: 'paginationType',
						condition: '==',
						value: 'navigation',
					},
				],
				selector:
					'{{WOPB}} .wopb-next-prev-wrap ul { margin:{{navMargin}}; }',
			},
		],
	},
	pagiMargin: {
		type: 'object',
		default: {
			lg: { top: '30', right: '0', bottom: '0', left: '0', unit: 'px' },
		},
		style: [
			{
				depends: [
					{
						key: 'paginationType',
						condition: '!=',
						value: 'navigation',
					},
				],
				selector:
					'{{WOPB}} .wopb-pagination, {{WOPB}} .wopb-loadmore { margin:{{pagiMargin}}; }',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#f5f5f5' },
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#ff176b' },
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapMargin: {
		type: 'object',
		default: { lg: { top: '', bottom: '', unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	wrapInnerPadding: {
		type: 'object',
		default: { lg: { unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { padding:{{wrapInnerPadding}}; }',
			},
		],
	},
	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;.block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: { type: 'string', default: '', style: [ { selector: '' } ] },
};
export default attributes;
