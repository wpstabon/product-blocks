const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';
registerBlockType( 'product-blocks/product-list-1', {
	title: __( 'Product List #1', 'product-blocks' ),
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/product-list-1.svg' }
			alt={ __( 'Product List #1', 'product-blocks' ) }
			style={ { width: '100%' } }
		/>
	),
	category: 'product-blocks',
	// description: (
	//     <span className="wopb-block-info">
	//         {__(
	//             "Display products of your store in a classic list or slide view.",
	//             "product-blocks"
	//         )}
	//         <a
	//             target="_blank"
	//             href="https://wpxpo.com/docs/wowstore/all-blocks/product-list-1/"
	//         >
	//             {__("Documentation", "product-blocks")}
	//         </a>
	//     </span>
	// ),
	keywords: [
		__( 'grid', 'product-blocks' ),
		__( 'product', 'product-blocks' ),
		__( 'listing', 'product-blocks' ),
	],
	supports: {
		align: [ 'center', 'wide', 'full' ],
	},
	example: {
		attributes: {
			previewId: wopb_data.url + 'assets/img/preview/product-list-1.svg',
		},
	},
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
