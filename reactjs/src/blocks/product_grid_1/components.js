const { __ } = wp.i18n;
const { Fragment } = wp.element;
import { renderDeals } from '../../helper/CommonPanel';
import {
	renderGridCart,
	renderGridCompare,
	renderGridQuickview,
	renderGridWishlist,
} from '../../helper/grid_compo/GridCommon';
import useStyleButtonToggle from '../../helper/hooks/useStyleButtonToggle';

export function Image( {
	attributes,
	post,
	isShow,
	category,
	setCurrSettings,
} ) {
	const {
		showHot,
		showDeal,
		imgCrop,
		catShow,
		showImage,
		showSale,
		saleStyle,
		saleDesign,
		catPosition,
		imgAnimation,
		showOutStock,
		showInStock,
		cartText,
		dealText,
		hotText,
		saleText,
		overlayMetaList,
		tooltipPosition,
		ovrMetaInline,
		hoverMeta,
	} = attributes;
	const parsedOverlayMetaList = JSON.parse( overlayMetaList );
	const { showStyleButton, StyleButton } = useStyleButtonToggle( ( e ) => {
		setCurrSettings( e, 'image' );
	} );
	let metaClass = ovrMetaInline ? ' wopb_f_inline' : '';
	metaClass += ! hoverMeta ? ' wopb-is-visible' : '';

	return (
		<div
			className={ `wopb-block-image wopb-block-image-${ imgAnimation }` }
			onClick={ showStyleButton }
		>
			{ StyleButton }
			<div className={ `wopb-onsale-hot` }>
				{ showSale && post.sale && (
					<span
						className={ `wopb-onsale wopb-onsale-${ saleStyle } wopb-component-simple` }
						onClick={ ( e ) => {
							setCurrSettings( e, 'sales' );
						} }
					>
						{ saleDesign === 'digit' && '-' + post.discount }
						{ saleDesign === 'text' &&
							( saleText || __( 'Sale!', 'product-blocks' ) ) }
						{ saleDesign === 'textDigit' &&
							__( '-' + post.discount + ' Off' ) }
					</span>
				) }

				{ showHot && post.featured && (
					<span
						className={ `wopb-hot wopb-component-simple` }
						onClick={ ( e ) => {
							setCurrSettings( e, 'hots' );
						} }
					>
						{ hotText || __( 'Hot', 'product-blocks' ) }
					</span>
				) }
			</div>
			{ parsedOverlayMetaList.length != 0 && (
				<div
					className={
						`wopb-product-new-meta wopb-component-simple` +
						metaClass
					}
					onClick={ ( e ) => {
						setCurrSettings( e, 'overlay_meta' );
					} }
				>
					{ parsedOverlayMetaList.map( ( val, _i ) => {
						return (
							<Fragment key={ _i }>
								{ val == '_wishlist' &&
									renderGridWishlist( { tooltipPosition } ) }
								{ val == '_qview' &&
									renderGridQuickview( { tooltipPosition } ) }
								{ val == '_compare' &&
									renderGridCompare( { tooltipPosition } ) }
								{ val == '_cart' &&
									renderGridCart( {
										cartText,
										tooltipPosition,
									} ) }
							</Fragment>
						);
					} ) }
				</div>
			) }
			{ showDeal &&
				renderDeals( post.deal, dealText, ( e ) =>
					setCurrSettings( e, 'deals' )
				) }

			{ catPosition != 'none' && catShow && isShow && category() }

			{ post.stock == 'outofstock' && showOutStock && (
				<div className="wopb-product-outofstock">
					<span>{ __( 'Out of stock', 'product-blocks' ) }</span>
				</div>
			) }

			{ post.stock == 'instock' && showInStock && (
				<div className="wopb-product-instock">
					<span>{ __( 'In Stock', 'product-blocks' ) }</span>
				</div>
			) }

			{ showImage && post.image && (
				<a>
					<img
						className="wopb-component-simple"
						alt={ post.title || '' }
						src={ post.image[ imgCrop ] }
					/>
				</a>
			) }
		</div>
	);
}
