/** @format */

const { __ } = wp.i18n;
const { InspectorControls, RichText } = wp.blockEditor;
const { Spinner, Placeholder } = wp.components;
import Slider from 'react-slick';
import { CssGenerator } from '../../helper/CssGenerator';
import {
	SliderSetting,
	LoadMore,
	Pagination,
	Filter,
	renderExcerpt,
	renderVariationSwitcher,
	renderCategory,
	renderTitle,
	NextArrow,
	PrevArrow,
} from '../../helper/CommonPanel';
import icons from '../../helper/icons';
import ToolBarElement from '../../helper/ToolBarElement';
import { Image } from './components';
import Settings, { AddSettingsToToolbar, features } from './Settings';
import useFluentSettings from '../../helper/hooks/useFluentSettings';
import useBlockId from '../../helper/hooks/use-block-id';
import usePrevious from '../../helper/hooks/use-previous';
import useProducts from '../../helper/hooks/use-products';

const SECTION = [ 'image', 'category', 'title', 'review', 'price', 'cart' ];

const { useMemo } = wp.element;

export default function Edit( props ) {
	useBlockId( props, true );

	const { setAttributes, name, attributes, clientId, className, context } =
		props;
	const prevProps = usePrevious( props );
	const { postsList, loading, error } = useProducts(
		attributes,
		prevProps?.attributes
	);
	const { section, setSection, setCurrSettings, toolbarSettings } =
		useFluentSettings( { props, prevProps } );

	const {
		blockId,
		advanceId,
		paginationShow,
		paginationAjax,
		headingTag,
		headingText,
		headingStyle,
		headingShow,
		headingAlign,
		headingURL,
		filterShow,
		filterType,
		filterCat,
		filterTag,
		paginationType,
		headingBtnText,
		paginationNav,
		subHeadingShow,
		subHeadingText,
		showArrows,
		showDots,
		slidesToShow,
		autoPlay,
		productView,
		slideSpeed,
		filterText,
		filterAction,
		filterActionText,
		loadMoreText,
		paginationText,
		previewId,
		arrowStyle,
		sortSection,
		columns,
		catShow,
		showCart,
		titleTag,
		titleShow,
		showPrice,
		showReview,
		catPosition,
		cartText,
		showShortDesc,
		shortDescLimit,
		showVariationSwitch,
		contentAlign,
		titleLength,
	} = attributes;

	const enableList = useMemo( () => {
		const is_show = JSON.parse( sortSection );
		const res = {};
		SECTION.forEach( ( sec ) => {
			res[ sec ] = is_show.includes( sec );
		} );
		return res;
	}, [ sortSection ] );

	const store = {
		setAttributes,
		name,
		attributes,
		setSection,
		section,
		clientId,
		context,
	};

	const settings = SliderSetting( {
		arrows: showArrows,
		dots: showDots,
		slidesToShow,
		autoplay: autoPlay,
		autoplaySpeed: parseInt( slideSpeed ),
		nextArrow: (
			<NextArrow
				arrowStyle={ arrowStyle }
				setSection={ ( e ) => {
					setCurrSettings( e, 'arrow' );
				} }
			/>
		),
		prevArrow: (
			<PrevArrow
				arrowStyle={ arrowStyle }
				setSection={ ( e ) => {
					setCurrSettings( e, 'arrow' );
				} }
			/>
		),
		appendDots: ( dots ) => {
			return (
				<div
					onClick={ ( e ) => {
						setCurrSettings( e, 'dot' );
					} }
				>
					<ul> { dots } </ul>
				</div>
			);
		},
	} );

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/product-grid-1', blockId );
	}

	const HeadingTag = headingTag;

	if ( previewId ) {
		return (
			<img
				style={ { marginTop: '0px', width: '420px' } }
				src={ previewId }
			/>
		);
	}

	const TitleTag = titleTag;

	const sectionsToShow = JSON.parse( sortSection );

	function renderComponent( elem, key, post ) {
		switch ( elem ) {
			case 'description':
				return showShortDesc
					? renderExcerpt(
							post.excerpt,
							shortDescLimit,
							( e ) => setCurrSettings( e, 'desc' ),
							key
					  )
					: null;
			case 'title':
				return post.title && titleShow && enableList.title
					? renderTitle(
							post.title,
							TitleTag,
							( e ) => setCurrSettings( e, 'title' ),
							key,
							titleLength
					  )
					: null;
			case 'category':
				return catShow && catPosition == 'none' && enableList.category
					? renderCategory(
							post,
							catShow,
							catPosition,
							( e ) => setCurrSettings( e, 'category' ),
							key
					  )
					: null;
			case 'review':
				return showReview && enableList.review ? (
					<div
						key={ key }
						className="wopb-star-rating wopb-component-simple"
						onClick={ ( e ) => {
							setCurrSettings( e, 'review' );
						} }
					>
						<span
							style={ {
								width: ( post.rating_average / 5 ) * 100 + '%',
							} }
						>
							<strong
								itemProp="ratingValue"
								className="wopb-rating"
							>
								{ post.rating_average }
							</strong>
							{ __( 'out of 5', 'product-blocks' ) }
						</span>
					</div>
				) : null;
			case 'price':
				return post.price_html && showPrice && enableList.price ? (
					<div
						key={ key }
						className="wopb-product-price wopb-component-simple"
						dangerouslySetInnerHTML={ { __html: post.price_html } }
						onClick={ ( e ) => setCurrSettings( e, 'price' ) }
					></div>
				) : null;
			case 'cart':
				return showCart && enableList.cart ? (
					<div
						className="wopb-product-btn wopb-component"
						key={ key }
						onClick={ ( e ) => {
							setCurrSettings( e, 'cart' );
						} }
					>
						<a className="wopb-component-hover">
							{ cartText
								? cartText
								: __( 'Add to cart', 'product-blocks' ) }
						</a>
					</div>
				) : null;
			case 'variationSwitcher':
				return renderVariationSwitcher(
					post,
					wopb_data.isVariationSwitchActive,
					showVariationSwitch,
					( e ) => setCurrSettings( e, 'swatch' ),
					key
				);
			case 'image':
				return (
					<Image
						attributes={ attributes }
						post={ post }
						isShow={ enableList.image }
						category={ () =>
							renderCategory( post, catShow, catPosition, ( e ) =>
								setCurrSettings( e, 'category' )
							)
						}
						setCurrSettings={ setCurrSettings }
						key={ key }
					/>
				);
			default:
				return null;
		}
	}

	return (
		<>
			<InspectorControls>
				<Settings store={ store } />
			</InspectorControls>

			<ToolBarElement
				include={ [
					{
						type: 'alignment',
						key: 'contentAlign',
						label: __( 'Grid Content Alignment', 'product-blocks' ),
						value: contentAlign,
						disableJustify: true,
					},
					{ type: 'template' },
					{ type: 'query' },
					{ type: 'grid_spacing' },
					{
						type: 'common',
						data: features( store ),
					},
				] }
				store={ store }
			/>
			<AddSettingsToToolbar
				store={ store }
				selected={ toolbarSettings }
			/>

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
				onClick={ ( e ) => setCurrSettings( e, 'general', 'none' ) }
			>
				<div className={ `wopb-block-wrapper` }>
					{ ( headingShow || filterShow || paginationShow ) && (
						<div className={ `wopb-heading-filter` }>
							<div className={ `wopb-heading-filter-in` }>
								{ headingShow && (
									<div
										className={ `wopb-heading-wrap wopb-heading-${ headingStyle } wopb-heading-${ headingAlign }` }
										onClick={ ( e ) => {
											setCurrSettings( e, 'heading' );
										} }
									>
										{ headingURL ? (
											<HeadingTag
												className={ `wopb-heading-inner` }
											>
												<a>
													<RichText
														key="editable"
														tagName={ 'span' }
														placeholder={ __(
															'Add Text…',
															'product-blocks'
														) }
														onChange={ ( value ) =>
															setAttributes( {
																headingText:
																	value,
															} )
														}
														value={ headingText }
													/>
												</a>
											</HeadingTag>
										) : (
											<HeadingTag
												className={ `wopb-heading-inner` }
											>
												<RichText
													key="editable"
													tagName={ 'span' }
													placeholder={ __(
														'Add Text…',
														'product-blocks'
													) }
													onChange={ ( value ) =>
														setAttributes( {
															headingText: value,
														} )
													}
													value={ headingText }
												/>
											</HeadingTag>
										) }
										{ headingStyle == 'style11' &&
											headingURL && (
												<a
													className={ `wopb-heading-btn` }
												>
													{ headingBtnText }
													{ icons.rightArrowLg }
												</a>
											) }
										{ subHeadingShow && (
											<div
												className={ `wopb-sub-heading` }
											>
												<RichText
													key="editable"
													tagName={ 'div' }
													className={
														'wopb-sub-heading-inner'
													}
													placeholder={ __(
														'Add Text…',
														'product-blocks'
													) }
													onChange={ ( value ) =>
														setAttributes( {
															subHeadingText:
																value,
														} )
													}
													value={ subHeadingText }
												/>
											</div>
										) }
									</div>
								) }
								{ ( filterShow || paginationShow ) &&
									productView == 'grid' && (
										<div
											className={ `wopb-filter-navigation` }
										>
											{ filterShow && (
												<Filter
													filterText={ filterText }
													filterType={ filterType }
													filterCat={ filterCat }
													filterTag={ filterTag }
													filterAction={
														filterAction
													}
													filterActionText={
														filterActionText
													}
													onClick={ ( e ) =>
														setCurrSettings(
															e,
															'filter'
														)
													}
												/>
											) }
										</div>
									) }
							</div>
						</div>
					) }

					<div className={ `wopb-wrapper-main-content` }>
						{ ! error ? (
							! loading ? (
								postsList.length > 0 ? (
									<>
										{ productView === 'slide' ? (
											<div
												className={ `wopb-product-blocks-slide wopb-product-blocks-slide1` }
											>
												<Slider { ...settings }>
													{ postsList.map(
														( post, idx ) => {
															return (
																<div
																	key={ idx }
																	className={ `wopb-block-item` }
																>
																	<div
																		className={ `wopb-block-content-wrap` }
																	>
																		{ sectionsToShow.map(
																			(
																				element,
																				idx
																			) =>
																				renderComponent(
																					element,
																					idx,
																					post
																				)
																		) }
																	</div>
																</div>
															);
														}
													) }
												</Slider>
											</div>
										) : (
											<div
												className={ `wopb-block-items-wrap wopb-block-row wopb-block-column-${ columns.lg }` }
											>
												{ postsList.map(
													( post, idx ) => {
														return (
															<div
																key={ idx }
																className={ `wopb-block-item` }
															>
																<div
																	className={ `wopb-block-content-wrap` }
																>
																	{ sectionsToShow.map(
																		(
																			element,
																			idx
																		) =>
																			renderComponent(
																				element,
																				idx,
																				post
																			)
																	) }
																</div>
															</div>
														);
													}
												) }
											</div>
										) }
									</>
								) : (
									<Placeholder
										className={ `wopb-backend-block-loading` }
										label={ __(
											'No Products found',
											'product-blocks'
										) }
									></Placeholder>
								)
							) : (
								<Placeholder
									className={ `wopb-backend-block-loading` }
									label={ __( 'Loading…', 'product-blocks' ) }
								>
									<Spinner />
								</Placeholder>
							)
						) : (
							<Placeholder
								label={ __(
									'Products are not available',
									'product-blocks'
								) }
							>
								<div style={ { marginBottom: 15 } }>
									{ __(
										'Make sure Add Product.',
										'product-blocks'
									) }
								</div>
							</Placeholder>
						) }

						{ paginationShow &&
							productView == 'grid' &&
							paginationType == 'pagination' && (
								<Pagination
									paginationNav={ paginationNav }
									paginationAjax={ paginationAjax }
									paginationText={ paginationText }
									onClick={ ( e ) => {
										setCurrSettings( e, 'pagination' );
									} }
								/>
							) }
						{ paginationShow &&
							productView == 'grid' &&
							paginationType == 'loadMore' && (
								<LoadMore
									loadMoreText={ loadMoreText }
									onClick={ ( e ) => {
										setCurrSettings( e, 'pagination' );
									} }
								/>
							) }
					</div>
				</div>
			</div>
		</>
	);
}
