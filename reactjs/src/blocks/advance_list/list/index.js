const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import Save from './Save';

registerBlockType( 'product-blocks/list', {
	title: __( 'List', 'product-blocks' ),
	parent: [ 'product-blocks/advance-list' ],
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/single_list.svg' }
			alt="List"
		/>
	),
	category: 'product-blocks',
	description: __( 'Create & customize a list item.', 'product-blocks' ),
	keywords: [
		__( 'list', 'product-blocks' ),
		__( 'advanced List', 'product-blocks' ),
		__( 'icon list', 'product-blocks' ),
	],
	supports: {
		reusable: false,
		html: false,
	},
	edit: Edit,
	save: Save,
	attributes: {
		blockId: {
			type: 'string',
			default: '',
		},

		/*======= Initialize for Parent Block Attr Value Save.JS ========*/
		layout: {
			type: 'string',
			default: '',
		},
		listGroupCustomImg: {
			type: 'object',
			default: '',
		},

		listGroupIconType: {
			type: 'string',
			default: '',
		},
		listDisableText: {
			type: 'boolean',
			default: true,
		},
		listCustomIcon: {
			type: 'string',
			default: '',
		},
		listGroupSubtextEnable: {
			type: 'boolean',
			default: true,
		},
		listGroupBelowIcon: {
			type: 'boolean',
			default: false,
		},
		enableIcon: {
			type: 'boolean',
			default: true,
		},
		listLayout: {
			type: 'string',
			default: 'number',
		},
		/*==========================
                Icon
            ==========================*/
		listText: {
			type: 'string',
			default: '',
		},
		listIconType: {
			type: 'string',
			default: '',
			style: [
				{
					depends: [
						{ key: 'enableIcon', condition: '==', value: true },
						{
							key: 'listGroupIconType',
							condition: '!=',
							value: 'default',
						},
					],
				},
			],
		},
		listSinleCustomIcon: {
			type: 'string',
			default: '',
			style: [
				{
					depends: [
						{
							key: 'listGroupIconType',
							condition: '!=',
							value: 'default',
						},
						{ key: 'listIconType', condition: '==', value: 'icon' },
					],
				},
			],
		},
		listCustomImg: {
			type: 'object',
			default: '',
			style: [
				{
					depends: [
						{
							key: 'listGroupIconType',
							condition: '!=',
							value: 'default',
						},
						{
							key: 'listIconType',
							condition: '==',
							value: 'image',
						},
					],
				},
			],
		},
		listIconColor: {
			type: 'string',
			default: '',
			style: [
				{
					depends: [
						{ key: 'listLayout', condition: '!=', value: 'bullet' },
						{
							key: 'listGroupIconType',
							condition: '==',
							value: 'default',
						},
					],
					selector:
						'{{WOPB}}.wp-block-product-blocks-list .wopb-list-texticon:before { color:{{listIconColor}}; }',
				},
				{
					depends: [
						{ key: 'listLayout', condition: '==', value: 'bullet' },
						{
							key: 'listGroupIconType',
							condition: '==',
							value: 'default',
						},
					],
					selector:
						'{{WOPB}}.wp-block-product-blocks-list .wopb-list-texticon:before { background-color:{{listIconColor}}; }',
				},
				{
					depends: [
						{
							key: 'listGroupIconType',
							condition: '!=',
							value: 'default',
						},
					],
					selector:
						'{{WOPB}} .wopb-listicon-bg svg { fill:{{listIconColor}}; }',
				},
			],
		},
		listIconBg: {
			type: 'string',
			default: '',
			style: [
				{
					depends: [
						{ key: 'listLayout', condition: '!=', value: 'bullet' },
						{
							key: 'listGroupIconType',
							condition: '==',
							value: 'default',
						},
					],
					selector:
						'{{WOPB}}.wp-block-product-blocks-list .wopb-list-texticon:before { background: {{listIconBg}};}',
				},
				{
					depends: [
						{ key: 'listLayout', condition: '==', value: 'bullet' },
						{
							key: 'listGroupIconType',
							condition: '==',
							value: 'default',
						},
					],
					selector:
						'{{WOPB}}.wp-block-product-blocks-list .wopb-list-texticon { background: {{listIconBg}};}',
				},
				{
					depends: [
						{
							key: 'listGroupIconType',
							condition: '!=',
							value: 'default',
						},
					],
					selector:
						'{{WOPB}}.wp-block-product-blocks-list .wopb-listicon-bg { background: {{listIconBg}};}',
				},
			],
		},
		listIconBorder: {
			type: 'object',
			default: {
				openBorder: 0,
				width: { top: 2, right: 2, bottom: 2, left: 2 },
				color: '#dfdfdf',
			},
			style: [
				{
					depends: [
						{ key: 'listLayout', condition: '!=', value: 'bullet' },
						{
							key: 'listGroupIconType',
							condition: '==',
							value: 'default',
						},
					],
					selector:
						'{{WOPB}}.wp-block-product-blocks-list .wopb-list-texticon:before',
				},
				{
					depends: [
						{ key: 'listLayout', condition: '==', value: 'bullet' },
						{
							key: 'listGroupIconType',
							condition: '==',
							value: 'default',
						},
					],
					selector:
						'{{WOPB}}.wp-block-product-blocks-list .wopb-list-texticon',
				},
				{
					depends: [
						{
							key: 'listGroupIconType',
							condition: '!=',
							value: 'default',
						},
					],
					selector:
						'{{WOPB}}.wp-block-product-blocks-list .wopb-listicon-bg',
				},
			],
		},

		// Hover
		listIconHoverColor: {
			type: 'string',
			default: '',
			style: [
				{
					depends: [
						{ key: 'listLayout', condition: '!=', value: 'bullet' },
						{
							key: 'listGroupIconType',
							condition: '==',
							value: 'default',
						},
					],
					selector:
						'{{WOPB}}.wp-block-product-blocks-list:hover .wopb-list-texticon:before { color:{{listIconHoverColor}}; }',
				},
				{
					depends: [
						{ key: 'listLayout', condition: '==', value: 'bullet' },
						{
							key: 'listGroupIconType',
							condition: '==',
							value: 'default',
						},
					],
					selector:
						'{{WOPB}}.wp-block-product-blocks-list:hover .wopb-list-texticon:before { background-color:{{listIconHoverColor}}; }',
				},
				{
					depends: [
						{
							key: 'listGroupIconType',
							condition: '!=',
							value: 'default',
						},
					],
					selector:
						'{{WOPB}}.wp-block-product-blocks-list:hover .wopb-listicon-bg svg { fill:{{listIconHoverColor}}; }',
				},
			],
		},
		listIconHoverBg: {
			type: 'string',
			default: '',
			style: [
				{
					depends: [
						{ key: 'listLayout', condition: '!=', value: 'bullet' },
						{
							key: 'listGroupIconType',
							condition: '==',
							value: 'default',
						},
					],
					selector:
						'.block-editor-block-list__block:hover > {{WOPB}}.wp-block-product-blocks-list .wopb-list-texticon:before, .wopb-list-wrapper >  {{WOPB}}.wp-block-product-blocks-list:hover .wopb-list-texticon:before { background: {{listIconHoverBg}};}',
				},
				{
					depends: [
						{ key: 'listLayout', condition: '==', value: 'bullet' },
						{
							key: 'listGroupIconType',
							condition: '==',
							value: 'default',
						},
					],
					selector:
						'{{WOPB}}.wp-block-product-blocks-list:hover .wopb-list-texticon { background-color:{{listIconHoverBg}}; }',
				},
				{
					depends: [
						{
							key: 'listGroupIconType',
							condition: '!=',
							value: 'default',
						},
					],
					selector:
						'{{WOPB}}.wp-block-product-blocks-list:hover .wopb-listicon-bg { background: {{listIconHoverBg}};}',
				},
			],
		},
		listIconHoverBorder: {
			type: 'object',
			default: {
				openBorder: 0,
				width: { top: 2, right: 2, bottom: 2, left: 2 },
				color: '#dfdfdf',
			},
			style: [
				{
					depends: [
						{ key: 'listLayout', condition: '!=', value: 'bullet' },
						{
							key: 'listGroupIconType',
							condition: '==',
							value: 'default',
						},
					],
					selector:
						'.block-editor-block-list__block:hover > {{WOPB}}.wp-block-product-blocks-list .wopb-list-texticon:before,  .wopb-list-wrapper > {{WOPB}}.wp-block-product-blocks-list:hover .wopb-list-texticon:before',
				},
				{
					depends: [
						{ key: 'listLayout', condition: '==', value: 'bullet' },
						{
							key: 'listGroupIconType',
							condition: '==',
							value: 'default',
						},
					],
					selector:
						'.block-editor-block-list__block:hover > {{WOPB}}.wp-block-product-blocks-list .wopb-list-texticon, .wopb-list-wrapper > {{WOPB}}.wp-block-product-blocks-list:hover .wopb-list-texticon',
				},
				{
					depends: [
						{
							key: 'listGroupIconType',
							condition: '!=',
							value: 'default',
						},
					],
					selector:
						'.block-editor-block-list__block:hover > {{WOPB}}.wp-block-product-blocks-list .wopb-listicon-bg, .wopb-list-wrapper > {{WOPB}}.wp-block-product-blocks-list:hover .wopb-listicon-bg',
				},
			],
		},

		/*==========================
                Text
            ==========================*/
		listTextColor: {
			type: 'string',
			default: '',
			style: [
				{
					selector:
						'{{WOPB}} .wopb-list-title, {{WOPB}} .wopb-list-title a { color:{{listTextColor}}; } ',
				},
			],
		},
		listTextHoverColor: {
			type: 'string',
			default: '',
			style: [
				{
					selector:
						'.wopb-list-wrapper > {{WOPB}}.wp-block-product-blocks-list:hover .wopb-list-title, .wopb-list-wrapper > {{WOPB}}.wp-block-product-blocks-list:hover .wopb-list-title a, .block-editor-block-list__block:hover > {{WOPB}}.wp-block-product-blocks-list .wopb-list-title, .block-editor-block-list__block:hover > {{WOPB}}.wp-block-product-blocks-list .wopb-list-title a { color:{{listTextHoverColor}}; } ',
				},
			],
		},
		subtext: {
			type: 'string',
			default: '',
		},
		subTextSpace: {
			type: 'object',
			default: { lg: '', ulg: 'px' },
			style: [
				{
					depends: [
						{ key: 'layout', condition: '==', value: 'layout3' },
						{
							key: 'listGroupSubtextEnable',
							condition: '==',
							value: true,
						},
					],
					selector:
						'{{WOPB}} .wopb-list-subtext { margin-top:{{subTextSpace}}; }',
				},
				{
					depends: [
						{ key: 'layout', condition: '!=', value: 'layout3' },
						{
							key: 'listGroupSubtextEnable',
							condition: '==',
							value: true,
						},
					],
					selector:
						'{{WOPB}} .wopb-list-content { row-gap:{{subTextSpace}}; }',
				},
			],
		},
		subTextColor: {
			type: 'string',
			default: '',
			style: [
				{
					depends: [
						{
							key: 'listGroupSubtextEnable',
							condition: '==',
							value: true,
						},
					],
					selector:
						'{{WOPB}}  .wopb-list-subtext { color:{{subTextColor}}; }',
				},
			],
		},
		listSubtextHoverColor: {
			type: 'string',
			default: '',
			style: [
				{
					depends: [
						{
							key: 'listGroupSubtextEnable',
							condition: '==',
							value: true,
						},
					],
					selector:
						'.wopb-list-wrapper  > {{WOPB}}.wp-block-product-blocks-list:hover .wopb-list-subtext, .block-editor-block-list__block:hover > {{WOPB}}.wp-block-product-blocks-list .wopb-list-subtext { color:{{listSubtextHoverColor}}; }',
				},
			],
		},

		/*==========================
                General Advance Settings
            ==========================*/
		hideExtraLarge: {
			type: 'boolean',
			default: false,
			style: [ { selector: 'div:has(> {{WOPB}}) {display:none;}' } ],
		},
		hideDesktop: {
			type: 'boolean',
			default: false,
			style: [
				{
					selector:
						'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
				},
			],
		},
		hideTablet: {
			type: 'boolean',
			default: false,
			style: [ { selector: 'div:has(> {{WOPB}}) {display:none;}' } ],
		},
		hideMobile: {
			type: 'boolean',
			default: false,
			style: [ { selector: 'div:has(> {{WOPB}}) {display:none;}' } ],
		},
		advanceCss: {
			type: 'string',
			default: '',
			style: [ { selector: '' } ],
		},
	},
} );
