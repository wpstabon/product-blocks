const { __ } = wp.i18n;
const { InspectorControls, RichText } = wp.blockEditor;
const { Fragment } = wp.element;
const { Dropdown } = wp.components;
const { createBlock } = wp.blocks;
import { CssGenerator } from '../../../helper/CssGenerator';
import Icon from '../../../helper/fields/Icon';
import IconPack from '../../../helper/fields/tools/IconPack';
import useBlockId from '../../../helper/hooks/use-block-id';
import ListsSettings from './Setting';
const { select } = wp.data;
const { useEffect } = wp.element;

export default function Edit( props ) {
	const { setAttributes, name, attributes, clientId, className } = props;

	const {
		blockId,
		advanceId,
		listText,
		listCustomImg,
		listIconType,
		subtext,
		listSinleCustomIcon,
	} = attributes;

	useBlockId( props, false );
	const IDs = select( 'core/block-editor' ).getBlockParents( clientId );
	const parentAttr = select( 'core/block-editor' ).getBlockAttributes(
		IDs[ IDs.length - 1 ]
	);
	const {
		listGroupCustomImg,
		listGroupIconType,
		listDisableText,
		layout,
		listGroupSubtextEnable,
		listGroupBelowIcon,
		listCustomIcon,
		enableIcon,
		listLayout,
	} = parentAttr;

	useEffect( () => {
		setAttributes( {
			listGroupCustomImg,
			listGroupIconType,
			listDisableText,
			listGroupBelowIcon,
			listGroupSubtextEnable,
			listCustomIcon,
			layout,
			enableIcon,
			listLayout,
		} );
	}, [
		listGroupCustomImg,
		listGroupIconType,
		listDisableText,
		layout,
		listGroupSubtextEnable,
		listGroupBelowIcon,
		listCustomIcon,
		enableIcon,
		listLayout,
	] );

	const store = {
		setAttributes,
		name,
		attributes,
		clientId,
	};

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/list', blockId );
	}

	const SubtextBelow =
		listGroupBelowIcon && layout != 'layout3' ? 'div' : Fragment;

	function listIconValidation( val ) {
		const isValid =
			( listGroupIconType != 'default' &&
				( listIconType == val ||
					( listIconType == 'inherit' &&
						listGroupIconType == val ) ) ) ||
			( listIconType == '' && listGroupIconType == val );
		return isValid;
	}

	const imgCondition = listIconValidation( 'image' );
	const svgCondition = listIconValidation( 'icon' );

	return (
		<>
			<InspectorControls>
				<ListsSettings store={ store } />
			</InspectorControls>
			<li
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className="wopb-list-content">
					<SubtextBelow
						{ ...( listGroupBelowIcon &&
							layout != 'layout3' && {
								class: 'wopb-list-heading',
							} ) }
					>
						{ listGroupIconType == 'default' && enableIcon && (
							<div className="wopb-list-texticon wopb-listicon-bg"></div>
						) }
						{ imgCondition && enableIcon && (
							<div className="wopb-listicon-bg">
								<img
									src={
										listCustomImg.url &&
										listIconType != 'inherit'
											? listCustomImg.url
											: listGroupCustomImg.url
									}
									alt="List Image"
								/>
							</div>
						) }

						{ svgCondition && enableIcon && (
							<Dropdown
								placement="bottom right"
								className="wopb-listicon-dropdown"
								renderToggle={ ( { onToggle, onClose } ) => (
									<div
										onClick={ () => onToggle() }
										className="wopb-listicon-bg"
									>
										{
											IconPack[
												listSinleCustomIcon.length >
													0 &&
												listIconType != 'inherit'
													? listSinleCustomIcon
													: listCustomIcon
											]
										}
									</div>
								) }
								renderContent={ () => (
									<Icon
										inline={ true }
										value={ listSinleCustomIcon }
										label="Update Single Icon"
										dynamicClass=" "
										onChange={ ( v ) =>
											store.setAttributes( {
												listSinleCustomIcon: v,
												listIconType: 'icon',
											} )
										}
									/>
								) }
							/>
						) }
						{ ! listDisableText && (
							<RichText
								key="editable"
								tagName={ 'div' }
								className={ 'wopb-list-title' }
								placeholder={ __(
									'List Text…',
									'product-blocks'
								) }
								onChange={ ( value ) =>
									setAttributes( { listText: value } )
								}
								onReplace={ (
									blocks,
									indexToSelect,
									initialPosition
								) =>
									wp.data
										.dispatch( 'core/block-editor' )
										.replaceBlocks(
											clientId,
											blocks,
											indexToSelect,
											initialPosition
										)
								}
								onSplit={ ( value, isAfterOriginal ) =>
									createBlock( 'product-blocks/list', {
										...attributes,
										listText: value,
									} )
								}
								value={ listText }
							/>
						) }
					</SubtextBelow>
					{ ! listDisableText && listGroupSubtextEnable && (
						<div className="wopb-list-subtext">
							<RichText
								key="editable"
								tagName={ 'span' }
								placeholder={ __(
									'List Subtext Text…',
									'product-blocks'
								) }
								onChange={ ( value ) =>
									setAttributes( { subtext: value } )
								}
								value={ subtext }
							/>
						</div>
					) }
				</div>
			</li>
		</>
	);
}
