import IconPack from '../../../helper/fields/tools/IconPack';
const { Component, Fragment } = wp.element;
const { select } = wp.data;

export default function Save( { attributes } ) {
	const {
		blockId,
		advanceId,
		listText,
		listCustomImg,
		listIconType,
		listGroupSubtextEnable,
		subtext,
		listGroupCustomImg,
		listGroupIconType,
		listDisableText,
		listGroupBelowIcon,
		listCustomIcon,
		listSinleCustomIcon,
		layout,
		enableIcon,
	} = attributes;

	const SubtextBelow =
		listGroupBelowIcon && layout != 'layout3' ? 'div' : Fragment;

	function listIconValidation( val ) {
		const isValid =
			( listGroupIconType != 'default' &&
				( listIconType == val ||
					( listIconType == 'inherit' &&
						listGroupIconType == val ) ) ) ||
			( listIconType == '' && listGroupIconType == val );
		return isValid;
	}

	const imgCondition = listIconValidation( 'image' );
	const svgCondition = listIconValidation( 'icon' );

	return (
		<li
			{ ...( advanceId && { id: advanceId } ) }
			className={ `wopb-block-${ blockId }` }
		>
			<div className="wopb-list-content">
				{
					<SubtextBelow
						{ ...( listGroupBelowIcon &&
							layout != 'layout3' && {
								class: 'wopb-list-heading',
							} ) }
					>
						{ listGroupIconType == 'default' && enableIcon && (
							<div className="wopb-list-texticon wopb-listicon-bg"></div>
						) }
						{ imgCondition && enableIcon && (
							<div className="wopb-listicon-bg">
								<img
									src={
										listCustomImg.url
											? listCustomImg.url
											: listGroupCustomImg.url
									}
									alt="List Image"
								/>
							</div>
						) }
						{ svgCondition && enableIcon && (
							<div className="wopb-listicon-bg">
								{
									IconPack[
										listSinleCustomIcon.length > 0 &&
										listIconType != 'inherit'
											? listSinleCustomIcon
											: listCustomIcon
									]
								}
							</div>
						) }
						{ ! listDisableText && (
							<div
								className="wopb-list-title"
								dangerouslySetInnerHTML={ { __html: listText } }
							/>
						) }
					</SubtextBelow>
				}
				{ ! listDisableText && listGroupSubtextEnable && (
					<div
						className="wopb-list-subtext"
						dangerouslySetInnerHTML={ { __html: subtext } }
					/>
				) }
			</div>
		</li>
	);
}
