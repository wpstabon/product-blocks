const { __ } = wp.i18n;
import {
	CommonSettings,
	CustomCssAdvanced,
	ResponsiveAdvanced,
} from '../../../helper/CommonPanel';
import { Section, Sections } from '../../../helper/Sections';

const ListsSettings = ( { store } ) => {
	return (
		<Sections>
			<Section slug="button" title={ __( 'Button', 'product-blocks' ) }>
				<CommonSettings
					title={ `inline` }
					include={ [
						{
							position: 1,
							data: {
								type: 'tag',
								key: 'listIconType',
								label: __( 'Icon Type', 'product-blocks' ),
								options: [
									{
										value: 'icon',
										label: __( 'Icon', 'product-blocks' ),
									},
									{
										value: 'image',
										label: __( 'Image', 'product-blocks' ),
									},
									{
										value: 'inherit',
										label: __(
											'Inherit',
											'product-blocks'
										),
									},
									{
										value: 'none',
										label: __( 'None', 'product-blocks' ),
									},
								],
							},
						},
						{
							position: 2,
							data: {
								type: 'icon',
								key: 'listSinleCustomIcon',
								label: __( 'Icon Store', 'product-blocks' ),
							},
						},
						{
							position: 3,
							data: {
								type: 'media',
								key: 'listCustomImg',
								label: __(
									'Upload Custom Image',
									'product-blocks'
								),
							},
						},
						{
							position: 4,
							data: {
								type: 'separator',
								label: __( 'Icon Style', 'product-blocks' ),
							},
						},
						{
							position: 5,
							data: {
								type: 'tab',
								content: [
									{
										name: 'normal',
										title: __( 'Normal', 'product-blocks' ),
										options: [
											{
												type: 'color',
												key: 'listIconColor',
												label: __(
													'Icon Color',
													'product-blocks'
												),
											},
											{
												type: 'color',
												key: 'listIconBg',
												label: __(
													'Icon  Background',
													'product-blocks'
												),
											},
											{
												type: 'border',
												key: 'listIconBorder',
												label: __(
													'Border',
													'product-blocks'
												),
											},
										],
									},
									{
										name: 'hover',
										title: __( 'Hover', 'product-blocks' ),
										options: [
											{
												type: 'color',
												key: 'listIconHoverColor',
												label: __(
													'Icon Hover Color',
													'product-blocks'
												),
											},
											{
												type: 'color',
												key: 'listIconHoverBg',
												label: __(
													'Icon Hover Background',
													'product-blocks'
												),
											},
											{
												type: 'border',
												key: 'listIconHoverBorder',
												label: __(
													'Hover Border',
													'product-blocks'
												),
											},
										],
									},
								],
							},
						},
					] }
					initialOpen={ true }
					store={ store }
				/>
				<CommonSettings
					title={ __( 'Text', 'product-blocks' ) }
					include={ [
						{
							position: 1,
							data: {
								type: 'color',
								key: 'listTextColor',
								label: __( 'Title Color', 'product-blocks' ),
							},
						},
						{
							position: 2,
							data: {
								type: 'color',
								key: 'listTextHoverColor',
								label: __(
									'Title Hover Color',
									'product-blocks'
								),
							},
						},
						{
							position: 3,
							data: {
								type: 'separator',
								key: 'separator',
								label: __( 'Subtext', 'product-blocks' ),
							},
						},
						{
							position: 4,
							data: {
								type: 'range',
								key: 'subTextSpace',
								min: 0,
								max: 400,
								step: 1,
								responsive: true,
								label: __(
									'Space Between Text & Subtext',
									'product-blocks'
								),
								unit: [ 'px' ],
							},
						},
						{
							position: 5,
							data: {
								type: 'color',
								key: 'subTextColor',
								label: __( 'Subtext Color', 'product-blocks' ),
							},
						},
						{
							position: 9,
							data: {
								type: 'color',
								key: 'listSubtextHoverColor',
								label: __(
									'Subtext Hover Color',
									'product-blocks'
								),
							},
						},
					] }
					initialOpen={ false }
					store={ store }
				/>
			</Section>
			<Section
				slug="advanced"
				title={ __( 'Advanced', 'product-blocks' ) }
			>
				<ResponsiveAdvanced store={ store } />
				<CustomCssAdvanced store={ store } />
			</Section>
		</Sections>
	);
};

export default ListsSettings;
