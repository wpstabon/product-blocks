const { InnerBlocks } = wp.blockEditor;

export default function Save( { attributes } ) {
	const { blockId, advanceId, layout } = attributes;

	return (
		<div
			{ ...( advanceId && { id: advanceId } ) }
			className={ `wopb-block-${ blockId }` }
		>
			<ul className={ `wopb-list-wrapper wopb-list-${ layout }` }>
				<InnerBlocks.Content />
			</ul>
		</div>
	);
}
