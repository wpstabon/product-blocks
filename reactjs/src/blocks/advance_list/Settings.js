const { __ } = wp.i18n;
import {
	CommonSettings,
	CustomCssAdvanced,
	GeneralAdvanced,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Section, Sections } from '../../helper/Sections';
import TemplateModal from '../../helper/TemplateModal';

const ListsGroupSettings = ( { store } ) => {
	return (
		<>
			<TemplateModal
				prev="https://www.wpxpo.com/wowstore/blocks/#demoid2130"
				store={ store }
			/>
			<Sections>
				<Section
					slug="global"
					title={ __( 'Global Style', 'product-blocks' ) }
				>
					<CommonSettings
						title={ `inline` }
						include={ [
							{
								position: 1,
								data: {
									type: 'layout',
									block: 'advance-list',
									key: 'layout',
									label: __( 'Layout', 'product-blocks' ),
									options: [
										{
											img: 'assets/img/blocks/list/list1.svg',
											label: 'Layout 1',
											value: 'layout1',
											pro: false,
										},
										{
											img: 'assets/img/blocks/list/list2.svg',
											label: 'Layout 2',
											value: 'layout2',
											pro: false,
										},
										{
											img: 'assets/img/blocks/list/list3.svg',
											label: 'Layout 3',
											value: 'layout3',
											pro: false,
										},
									],
								},
							},
							{
								position: 2,
								data: {
									type: 'toggle',
									key: 'listInline',
									label: __(
										'Inline View',
										'product-blocks'
									),
								},
							},
							{
								position: 3,
								data: {
									type: 'alignment',
									block: 'advance-list',
									key: 'listAlignment',
									disableJustify: true,
									label: __(
										'Vertical Alignment (Align Items)',
										'product-blocks'
									),
								},
							},
							{
								position: 4,
								data: {
									type: 'range',
									key: 'listSpaceBetween',
									min: 0,
									max: 300,
									step: 1,
									responsive: true,
									label: __(
										'Space Between Items',
										'product-blocks'
									),
								},
							},
							{
								position: 5,
								data: {
									type: 'range',
									key: 'listSpaceIconText',
									min: 0,
									max: 300,
									step: 1,
									responsive: true,
									label: __(
										'Spacing Between Icon & Texts',
										'product-blocks'
									),
								},
							},
							{
								position: 6,
								data: {
									type: 'group',
									key: 'listPosition',
									justify: true,
									label: __(
										'Icon Position',
										'product-blocks'
									),
									options: [
										{
											value: 'initial',
											label: __(
												'Top',
												'product-blocks'
											),
										},
										{
											value: 'center',
											label: __(
												'Center',
												'product-blocks'
											),
										},
										{
											value: 'end',
											label: __(
												'Bottom',
												'product-blocks'
											),
										},
									],
								},
							},
						] }
						initialOpen={ true }
						store={ store }
					/>
					<CommonSettings
						title={ __( 'Icon/Image', 'product-blocks' ) }
						depend="enableIcon"
						include={ [
							{
								position: 1,
								data: {
									type: 'tag',
									key: 'listGroupIconType',
									label: __( 'Icon Type', 'product-blocks' ),
									options: [
										{
											value: 'icon',
											label: __(
												'Icon',
												'product-blocks'
											),
										},
										{
											value: 'image',
											label: __(
												'Image',
												'product-blocks'
											),
										},
										{
											value: 'default',
											label: __(
												'Default',
												'product-blocks'
											),
										},
										{
											value: '',
											label: __(
												'None',
												'product-blocks'
											),
										},
									],
								},
							},
							{
								position: 2,
								data: {
									type: 'select',
									key: 'listLayout',
									label: __( 'List Style', 'product-blocks' ),
									options: [
										{
											label: __(
												'Select',
												'product-blocks'
											),
											value: '',
										},
										{
											label: __(
												'By Abc',
												'product-blocks'
											),
											value: 'abc',
										},
										{
											label: __(
												'By Roman Number',
												'product-blocks'
											),
											value: 'roman',
										},
										{
											label: __(
												'By 123',
												'product-blocks'
											),
											value: 'number',
										},
										{
											label: __(
												'By Bullet',
												'product-blocks'
											),
											value: 'bullet',
										},
									],
								},
							},
							{
								position: 3,
								data: {
									type: 'icon',
									key: 'listCustomIcon',
									label: __( 'Icon Store', 'product-blocks' ),
								},
							},
							{
								position: 4,
								data: {
									type: 'media',
									key: 'listGroupCustomImg',
									label: __(
										'Upload Custom Image',
										'product-blocks'
									),
								},
							},
							{
								position: 5,
								data: {
									type: 'dimension',
									key: 'listImgRadius',
									step: 1,
									unit: true,
									responsive: true,
									label: __(
										'Image Radius',
										'product-blocks'
									),
								},
							},
							{
								position: 6,
								data: {
									type: 'typography',
									key: 'listIconTypo',
									label: __( 'Typography', 'product-blocks' ),
								},
							},
							{
								position: 7,
								data: {
									type: 'range',
									key: 'listGroupIconSize',
									label: __(
										'Icon/Image Size',
										'product-blocks'
									),
								},
							},
							{
								position: 8,
								data: {
									type: 'range',
									key: 'listGroupBgSize',
									label: __(
										'Background Size',
										'product-blocks'
									),
									help: 'Icon Background Color Required',
								},
							},
							{
								position: 9,
								data: {
									type: 'separator',
									label: __( 'Icon Style', 'product-blocks' ),
								},
							},
							{
								position: 10,
								data: {
									type: 'tab',
									content: [
										{
											name: 'normal',
											title: __(
												'Normal',
												'product-blocks'
											),
											options: [
												{
													type: 'color',
													key: 'listGroupIconColor',
													label: __(
														'Icon Color',
														'product-blocks'
													),
												},
												{
													type: 'color',
													key: 'listGroupIconbg',
													label: __(
														'Icon  Background',
														'product-blocks'
													),
												},
												{
													type: 'border',
													key: 'listGroupIconBorder',
													label: __(
														'Border',
														'product-blocks'
													),
												},
												{
													type: 'dimension',
													key: 'listGroupIconRadius',
													step: 1,
													unit: true,
													responsive: true,
													label: __(
														'Border Radius',
														'product-blocks'
													),
												},
											],
										},
										{
											name: 'hover',
											title: __(
												'Hover',
												'product-blocks'
											),
											options: [
												{
													type: 'color',
													key: 'listGroupHoverIconColor',
													label: __(
														'Icon Color',
														'product-blocks'
													),
												},
												{
													type: 'color',
													key: 'listGroupHoverIconbg',
													label: __(
														'Icon  Background',
														'product-blocks'
													),
												},
												{
													type: 'border',
													key: 'listGroupHoverIconBorder',
													label: __(
														'Border',
														'product-blocks'
													),
												},
												{
													type: 'dimension',
													key: 'listGroupHoverIconRadius',
													step: 1,
													unit: true,
													responsive: true,
													label: __(
														'Border Radius',
														'product-blocks'
													),
												},
											],
										},
									],
								},
							},
						] }
						initialOpen={ false }
						store={ store }
					/>
					<CommonSettings
						title={ __( 'Content', 'product-blocks' ) }
						include={ [
							{
								position: 1,
								data: {
									type: 'toggle',
									key: 'listDisableText',
									label: __(
										'Disable Text',
										'product-blocks'
									),
								},
							},
							{
								position: 2,
								data: {
									type: 'typography',
									key: 'listTextTypo',
									label: __(
										'Title Typography',
										'product-blocks'
									),
								},
							},
							{
								position: 3,
								data: {
									type: 'color',
									key: 'listGroupTitleColor',
									label: __(
										'Title Color',
										'product-blocks'
									),
								},
							},
							{
								position: 4,
								data: {
									type: 'color',
									key: 'listGroupTitleHoverColor',
									label: __(
										'Title Hover Color',
										'product-blocks'
									),
								},
							},
							{
								position: 5,
								data: {
									type: 'toggle',
									key: 'listGroupSubtextEnable',
									label: __(
										'Enable Subtext',
										'product-blocks'
									),
								},
							},
							{
								position: 6,
								data: {
									type: 'typography',
									key: 'listGroupSubtextTypo',
									label: __(
										'Subtext Typography',
										'product-blocks'
									),
								},
							},
							{
								position: 7,
								data: {
									type: 'range',
									key: 'listGroupSubtextSpace',
									unit: true,
									responsive: true,
									label: __(
										'Space Between  Text & Subtext',
										'product-blocks'
									),
								},
							},
							{
								position: 8,
								data: {
									type: 'color',
									key: 'listGroupSubtextColor',
									label: __(
										'Subtext Color',
										'product-blocks'
									),
								},
							},
							{
								position: 9,
								data: {
									type: 'color',
									key: 'listGroupSubtextHoverColor',
									label: __(
										'Subtext Hover Color',
										'product-blocks'
									),
								},
							},
							{
								position: 10,
								data: {
									type: 'toggle',
									key: 'listGroupBelowIcon',
									help: 'Layout One/Two Required ',
									label: __(
										'Midpoint Subtext',
										'product-blocks'
									),
								},
							},
						] }
						initialOpen={ false }
						store={ store }
					/>
					<CommonSettings
						title={ __( 'Content Wrap', 'product-blocks' ) }
						include={ [
							{
								position: 1,
								data: {
									type: 'dimension',
									key: 'listGroupPadding',
									step: 1,
									unit: true,
									responsive: true,
									label: __( 'Padding', 'product-blocks' ),
								},
							},
							{
								position: 2,
								data: {
									type: 'tab',
									content: [
										{
											name: 'normal',
											title: __(
												'Normal',
												'product-blocks'
											),
											options: [
												{
													type: 'color2',
													key: 'listGroupBg',
													label: __(
														'Background Color',
														'product-blocks'
													),
												},
												{
													type: 'border',
													key: 'listGroupborder',
													label: __(
														'Border',
														'product-blocks'
													),
												},
												{
													type: 'dimension',
													key: 'listGroupRadius',
													step: 1,
													unit: true,
													responsive: true,
													label: __(
														'Border Radius',
														'product-blocks'
													),
												},
											],
										},
										{
											name: 'hover',
											title: __(
												'Hover',
												'product-blocks'
											),
											options: [
												{
													type: 'color2',
													key: 'listGroupHoverBg',
													label: __(
														'Icon  Background',
														'product-blocks'
													),
												},
												{
													type: 'border',
													key: 'listGroupHovborder',
													label: __(
														'Border',
														'product-blocks'
													),
												},
												{
													type: 'dimension',
													key: 'listGroupHovRadius',
													step: 1,
													unit: true,
													responsive: true,
													label: __(
														'Border Radius',
														'product-blocks'
													),
												},
											],
										},
									],
								},
							},
						] }
						initialOpen={ false }
						store={ store }
					/>
					<CommonSettings
						title={ __( 'Separator', 'product-blocks' ) }
						depend="enableSeparator"
						include={ [
							{
								position: 1,
								data: {
									type: 'color',
									key: 'listGroupSepColor',
									label: __(
										'Border Color',
										'product-blocks'
									),
								},
							},
							{
								position: 2,
								data: {
									type: 'range',
									key: 'listGroupSepSize',
									min: 0,
									max: 30,
									label: __(
										'Separator Size',
										'product-blocks'
									),
								},
							},
							{
								position: 3,
								data: {
									type: 'select',
									key: 'listGroupSepStyle',
									options: [
										{
											value: 'none',
											label: __(
												'None',
												'product-blocks'
											),
										},
										{
											value: 'dotted',
											label: __(
												'Dotted',
												'product-blocks'
											),
										},
										{
											value: 'solid',
											label: __(
												'Solid',
												'product-blocks'
											),
										},
										{
											value: 'dashed',
											label: __(
												'Dashed',
												'product-blocks'
											),
										},
										{
											value: 'groove',
											label: __(
												'Groove',
												'product-blocks'
											),
										},
									],
									label: __(
										'Border Style',
										'product-blocks'
									),
								},
							},
						] }
						initialOpen={ false }
						store={ store }
					/>
				</Section>
				<Section
					slug="advanced"
					title={ __( 'Advanced', 'product-blocks' ) }
				>
					<GeneralAdvanced initialOpen={ true } store={ store } />
					<ResponsiveAdvanced store={ store } />
					<CustomCssAdvanced store={ store } />
				</Section>
			</Sections>
			{ wopbSupport() }
		</>
	);
};

export default ListsGroupSettings;
