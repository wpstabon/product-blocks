const { __ } = wp.i18n;
const { InspectorControls, InnerBlocks } = wp.blockEditor;
import { updateChildAttr } from '../../helper/CommonPanel';
import { CssGenerator } from '../../helper/CssGenerator';
import ListsGroupSettings from './Settings';
import ToolBarElement from '../../helper/ToolBarElement';
import useBlockId from '../../helper/hooks/use-block-id';

const { useEffect, useRef } = wp.element;

export default function Edit( props ) {
	useBlockId( props, true );
	const prevPropsRef = useRef( null );

	const { setAttributes, name, attributes, clientId, className } = props;

	const {
		previewImg,
		blockId,
		advanceId,
		layout,
		listGroupIconType,
		listCustomIcon,
		listGroupCustomImg,
		listDisableText,
		listGroupSubtextEnable,
		listGroupBelowIcon,
		enableIcon,
		listLayout,
	} = attributes;

	function needsUpdate( prevProps ) {
		return (
			prevProps.listGroupIconType != listGroupIconType ||
			prevProps.listCustomIcon != listCustomIcon ||
			prevProps.listGroupBelowIcon != listGroupBelowIcon ||
			prevProps.listGroupCustomImg != listGroupCustomImg ||
			prevProps.listDisableText != listDisableText ||
			prevProps.listGroupSubtextEnable != listGroupSubtextEnable ||
			prevProps.enableIcon != enableIcon ||
			prevProps.listLayout != listLayout ||
			prevProps.layout != layout
		);
	}

	useEffect( () => {
		const prevAttributes = prevPropsRef.current;

		if ( ! prevAttributes ) {
			prevPropsRef.current = attributes;
		} else if ( needsUpdate( prevAttributes ) ) {
			updateChildAttr( clientId );
			prevPropsRef.current = attributes;
		}
	}, [ attributes ] );

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/advance-list', blockId );
	}

	const listTemplate = [
		[ 'product-blocks/list', { listText: 'List Item' } ],
		[ 'product-blocks/list', { listText: 'List Item' } ],
		[ 'product-blocks/list', { listText: 'List Item' } ],
	];

	if ( previewImg ) {
		return (
			<img
				style={ { marginTop: '0px', width: '420px' } }
				src={ previewImg }
			/>
		);
	}

	const store = {
		setAttributes,
		name,
		attributes,
		clientId,
	};

	return (
		<>
			<InspectorControls>
				<ListsGroupSettings store={ store } />
			</InspectorControls>
			<ToolBarElement
				include={ [
					{
						type: 'template',
					},
					{
						type: 'layout',
						block: 'advance-list',
						key: 'layout',
						label: __( 'Layout', 'product-blocks' ),
						options: [
							{
								img: 'assets/img/blocks/list/list1.svg',
								label: 'Layout 1',
								value: 'layout1',
							},
							{
								img: 'assets/img/blocks/list/list2.svg',
								label: 'Layout 2',
								value: 'layout2',
							},
							{
								img: 'assets/img/blocks/list/list3.svg',
								label: 'Layout 3',
								value: 'layout3',
							},
						],
					},
				] }
				store={ store }
			/>
			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<ul className={ `wopb-list-wrapper wopb-list-${ layout }` }>
					<InnerBlocks
						template={ listTemplate }
						allowedBlocks={ [ 'product-blocks/list' ] }
					/>
				</ul>
			</div>
		</>
	);
}
