import useStyleButtonToggle from '../../helper/hooks/useStyleButtonToggle';

const { __ } = wp.i18n;

export const Card = ( { attributes, post, setSection } ) => {
	const {
		showImage,
		titleShow,
		descShow,
		countShow,
		imgAnimation,
		contentVerticalPosition,
		contentHorizontalPosition,
		readMore,
		readMoreText,
		categoryrCountText,
		imgCrop,
		titleTag,
		descLimit,
	} = attributes;

	const TitleTag = titleTag;

	const { StyleButton, showStyleButton } = useStyleButtonToggle( ( e ) =>
		setSection( e, 'image' )
	);
	const excerpt = () => {
		const shortDesc =
			post.desc.split( ' ' ).splice( 0, descLimit ).join( ' ' ) + '...';
		return (
			<div
				className="wopb-product-cat-desc wopb-component-simple"
				onClick={ ( e ) => setSection( e, 'desc' ) }
				dangerouslySetInnerHTML={ {
					__html: shortDesc,
				} }
			/>
		);
	};
	return (
		<div className={ `wopb-block-item` }>
			<div className={ `wopb-block-content-wrap wopb-category-wrap` }>
				{ showImage && (
					<div
						className={ `wopb-block-image wopb-block-image-${ imgAnimation }` }
						onClick={ showStyleButton }
					>
						{ StyleButton }
						<a href="#" className="wopb-product-cat-img">
							<img
								src={
									post.image[ imgCrop ]
										? post.image[ imgCrop ]
										: wopb_data.url +
										  'assets/img/wopb_fallback.jpg'
								}
								alt={ post.name }
								className="wopb-component-simple"
							/>
						</a>
					</div>
				) }
				{ ( titleShow || countShow || descShow || readMore ) && (
					<div
						className={ `wopb-category-content-items wopb-category-content-${ contentVerticalPosition } wopb-category-content-${ contentHorizontalPosition }` }
					>
						<div className="wopb-category-content-item">
							{ post.name && titleShow && (
								<TitleTag
									className="wopb-product-cat-title wopb-component-simple"
									onClick={ ( e ) =>
										setSection( e, 'title' )
									}
								>
									<a href="#">{ post.name }</a>
								</TitleTag>
							) }
							{ post.count && countShow && (
								<div
									className="wopb-product-cat-count wopb-component-simple"
									onClick={ ( e ) =>
										setSection( e, 'count' )
									}
								>
									{ post.count }{ ' ' }
									{ categoryrCountText ||
										__( 'products', 'product-blocks' ) }
								</div>
							) }
							{ post.desc && descShow && excerpt() }
							{ readMore && (
								<div
									className="wopb-product-readmore wopb-component-simple"
									onClick={ ( e ) =>
										setSection( e, 'read-more' )
									}
								>
									<a>
										{ readMoreText
											? readMoreText
											: __(
													'Explore',
													'product-blocks'
											  ) }
									</a>
								</div>
							) }
						</div>
					</div>
				) }
			</div>
		</div>
	);
};
