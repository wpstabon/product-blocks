const attributes = {
	blockId: { type: 'string', default: '' },
	currentPostId: { type: 'string', default: '' },
	previewId: { type: 'string', default: '' },
	initPremade: {
		type: 'boolean',
		default: false,
	},
	queryType: { type: 'string', default: 'regular' },
	queryCat: {
		type: 'string',
		default: '[]',
		style: [
			{
				depends: [
					{ key: 'queryType', condition: '!=', value: 'regular' },
				],
			},
		],
	},
	queryNumber: {
		type: 'string',
		default: 8,
		style: [
			{
				depends: [
					{ key: 'queryType', condition: '==', value: 'regular' },
				],
			},
		],
	},
	readMore: { type: 'boolean', default: false },
	productView: { type: 'string', default: 'grid' },
	columns: {
		type: 'object',
		default: { lg: '3', sm: '2', xs: '1' },
		style: [
			{
				depends: [
					{ key: 'productView', condition: '==', value: 'grid' },
				],
				selector:
					'{{WOPB}} .wopb-block-items-wrap { grid-template-columns: repeat({{columns}}, 1fr); }',
			},
		],
	},
	columnGridGap: {
		type: 'object',
		default: { lg: '30', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'productView', condition: '==', value: 'grid' },
				],
				selector:
					'{{WOPB}} .wopb-block-items-wrap { grid-column-gap: {{columnGridGap}}; }',
			},
		],
	},
	rowGap: {
		type: 'object',
		default: { lg: '30', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'productView', condition: '==', value: 'grid' },
				],
				selector:
					'{{WOPB}} .wopb-block-items-wrap { grid-row-gap: {{rowGap}}; }',
			},
		],
	},
	columnGap: {
		type: 'object',
		default: { lg: '10', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'productView', condition: '==', value: 'slide' },
				],
				selector:
					'{{WOPB}} .wopb-product-blocks-slide .wopb-block-item { padding: {{columnGap}}; box-sizing:border-box; }',
			},
		],
	},
	slidesToShow: {
		type: 'object',
		default: { lg: '4', sm: '2', xs: '1' },
		style: [
			{
				depends: [
					{ key: 'productView', condition: '==', value: 'slide' },
				],
			},
		],
	},
	autoPlay: {
		type: 'boolean',
		default: true,
		style: [
			{
				depends: [
					{ key: 'productView', condition: '==', value: 'slide' },
				],
			},
		],
	},
	showDots: {
		type: 'boolean',
		default: true,
		style: [
			{
				depends: [
					{ key: 'productView', condition: '==', value: 'slide' },
				],
			},
		],
	},
	showArrows: {
		type: 'boolean',
		default: true,
		style: [
			{
				depends: [
					{ key: 'productView', condition: '==', value: 'slide' },
				],
			},
		],
	},
	slideSpeed: {
		type: 'string',
		default: '3000',
		style: [
			{
				depends: [
					{ key: 'productView', condition: '==', value: 'slide' },
				],
			},
		],
	},
	headingShow: { type: 'boolean', default: false },
	showImage: { type: 'boolean', default: true },
	titleShow: { type: 'boolean', default: true },
	descShow: { type: 'boolean', default: true },
	countShow: { type: 'boolean', default: true },
	contentAlign: {
		type: 'string',
		default: 'center',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-category-content-item { text-align:{{contentAlign}}; }',
			},
		],
	},
	arrowStyle: {
		type: 'string',
		default: 'leftAngle2#rightAngle2',
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
			},
		],
	},
	arrowSize: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .slick-next svg, {{WOPB}} .slick-prev svg { width:{{arrowSize}}; }',
			},
		],
	},
	arrowWidth: {
		type: 'object',
		default: { lg: '60', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector: '{{WOPB}} .slick-arrow { width:{{arrowWidth}}; }',
			},
		],
	},
	arrowHeight: {
		type: 'object',
		default: { lg: '60', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .slick-arrow { height:{{arrowHeight}}; } {{WOPB}} .slick-arrow { line-height:{{arrowHeight}}; }',
			},
		],
	},
	arrowVartical: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .slick-next { right:{{arrowVartical}}; } {{WOPB}} .slick-prev { left:{{arrowVartical}}; }',
			},
		],
	},
	arrowHorizontal: {
		type: 'object',
		default: { lg: '' },
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-next, {{WOPB}} .slick-prev { top:{{arrowHorizontal}}; }',
			},
		],
	},
	arrowColor: {
		type: 'string',
		default: '#ffffff',
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .slick-arrow:before { color:{{arrowColor}}; } {{WOPB}} .slick-arrow svg { fill:{{arrowColor}}; }',
			},
		],
	},
	arrowHoverColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .slick-arrow:hover:before { color:{{arrowHoverColor}}; } {{WOPB}} .slick-arrow:hover svg { fill:{{arrowHoverColor}}; }',
			},
		],
	},
	arrowBg: {
		type: 'string',
		default: 'rgba(0,0,0,0.22)',
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector: '{{WOPB}} .slick-arrow { background:{{arrowBg}}; }',
			},
		],
	},
	arrowHoverBg: {
		type: 'string',
		default: '#ff176b',
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .slick-arrow:hover { background:{{arrowHoverBg}}; }',
			},
		],
	},
	arrowBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector: '{{WOPB}} .slick-arrow',
			},
		],
	},
	arrowHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector: '{{WOPB}} .slick-arrow:hover',
			},
		],
	},
	arrowRadius: {
		type: 'object',
		default: {
			lg: {
				top: '50',
				bottom: '50',
				left: '50',
				right: '50',
				unit: 'px',
			},
		},
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .slick-arrow { border-radius: {{arrowRadius}}; }',
			},
		],
	},
	arrowHoverRadius: {
		type: 'object',
		default: {
			lg: {
				top: '50',
				bottom: '50',
				left: '50',
				right: '50',
				unit: 'px',
			},
		},
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .slick-arrow:hover{ border-radius: {{arrowHoverRadius}}; }',
			},
		],
	},
	arrowShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector: '{{WOPB}} .slick-arrow',
			},
		],
	},
	arrowHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector: '{{WOPB}} .slick-arrow:hover',
			},
		],
	},
	dotSpace: {
		type: 'object',
		default: { lg: '4', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-dots { padding: 0 {{dotSpace}}; } {{WOPB}} .slick-dots li button { margin: 0 {{dotSpace}}; }',
			},
		],
	},
	dotVartical: {
		type: 'object',
		default: { lg: '-50', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector: '{{WOPB}} .slick-dots { bottom:{{dotVartical}}; }',
			},
		],
	},
	dotHorizontal: {
		type: 'object',
		default: { lg: '' },
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector: '{{WOPB}} .slick-dots { left:{{dotHorizontal}}; }',
			},
		],
	},
	dotWidth: {
		type: 'object',
		default: { lg: '10', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-dots li button { width:{{dotWidth}}; }',
			},
		],
	},
	dotHeight: {
		type: 'object',
		default: { lg: '10', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-dots li button { height:{{dotHeight}}; }',
			},
		],
	},
	dotHoverWidth: {
		type: 'object',
		default: { lg: '16', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-dots li.slick-active button { width:{{dotHoverWidth}}; }',
			},
		],
	},
	dotHoverHeight: {
		type: 'object',
		default: { lg: '16', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-dots li.slick-active button { height:{{dotHoverHeight}}; }',
			},
		],
	},
	dotBg: {
		type: 'string',
		default: '#f5f5f5',
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-dots li button { background:{{dotBg}}; }',
			},
		],
	},
	dotHoverBg: {
		type: 'string',
		default: '#000',
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-dots li button:hover, {{WOPB}} .slick-dots li.slick-active button { background:{{dotHoverBg}}; }',
			},
		],
	},
	dotBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector: '{{WOPB}} .slick-dots li button',
			},
		],
	},
	dotHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-dots li button:hover, {{WOPB}} .slick-dots li.slick-active button',
			},
		],
	},
	dotRadius: {
		type: 'object',
		default: {
			lg: {
				top: '50',
				bottom: '50',
				left: '50',
				right: '50',
				unit: 'px',
			},
		},
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-dots li button { border-radius: {{dotRadius}}; }',
			},
		],
	},
	dotHoverRadius: {
		type: 'object',
		default: {
			lg: {
				top: '50',
				bottom: '50',
				left: '50',
				right: '50',
				unit: 'px',
			},
		},
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-dots li button:hover, {{WOPB}} .slick-dots li.slick-active button { border-radius: {{dotHoverRadius}}; }',
			},
		],
	},
	dotShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector: '{{WOPB}} .slick-dots li button',
			},
		],
	},
	dotHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .slick-dots li button:hover, {{WOPB}} .slick-dots li.slick-active button',
			},
		],
	},
	headingText: { type: 'string', default: 'Product Category #1' },
	headingURL: { type: 'string', default: '' },
	headingBtnText: {
		type: 'string',
		default: 'View More',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style11' },
				],
			},
		],
	},
	headingStyle: { type: 'string', default: 'style1' },
	headingTag: { type: 'string', default: 'h2' },
	headingAlign: {
		type: 'string',
		default: 'left',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-heading-inner, {{WOPB}} .wopb-sub-heading-inner{ text-align:{{headingAlign}}; } {{WOPB}} .wopb-heading-filter-in{ display: block; }',
			},
		],
	},
	headingTypo: {
		type: 'object',
		default: {
			openTypography: 0,
			size: { lg: '20', unit: 'px' },
			height: { lg: '', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{ selector: '{{WOPB}} .wopb-heading-wrap .wopb-heading-inner' },
		],
	},
	headingColor: {
		type: 'string',
		default: '#0e1523',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { color:{{headingColor}}; }',
			},
		],
	},
	headingBorderBottomColor: {
		type: 'string',
		default: '#0e1523',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style3' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { border-bottom-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style4' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { border-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style6' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before { background-color: {{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style7' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before { background-color: {{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style8' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:after { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style9' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style10' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style13' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { border-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style14' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style15' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style16' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style17' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style18' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:after { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style19' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { border-color:{{headingBorderBottomColor}}; }',
			},
		],
	},
	headingBorderBottomColor2: {
		type: 'string',
		default: '#e5e5e5',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style8' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { background-color:{{headingBorderBottomColor2}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style10' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style14' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style19' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }',
			},
		],
	},
	headingBg: {
		type: 'string',
		default: '#ff176b',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style5' },
				],
				selector:
					'{{WOPB}} .wopb-heading-style5 .wopb-heading-inner span:before { border-color:{{headingBg}} transparent transparent; } {{WOPB}} .wopb-heading-inner span { background-color:{{headingBg}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style2' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span { background-color:{{headingBg}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style3' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span { background-color:{{headingBg}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style12' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span { background-color:{{headingBg}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style13' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span { background-color:{{headingBg}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style18' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { background-color:{{headingBg}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style20' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before { border-color:{{headingBg}} transparent transparent; } {{WOPB}} .wopb-heading-inner { background-color:{{headingBg}}; }',
			},
		],
	},
	headingBg2: {
		type: 'string',
		default: '#e5e5e5',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style12' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { background-color:{{headingBg2}}; }',
			},
		],
	},
	headingBtnTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '', unit: 'px' },
			decoration: 'none',
			family: '',
		},
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style11' },
				],
				selector: '{{WOPB}} .wopb-heading-btn',
			},
		],
	},
	headingBtnColor: {
		type: 'string',
		default: '#ff176b',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style11' },
				],
				selector:
					'{{WOPB}} .wopb-heading-btn { color:{{headingBtnColor}}; } {{WOPB}} .wopb-heading-btn svg { fill:{{headingBtnColor}}; }',
			},
		],
	},
	headingBtnHoverColor: {
		type: 'string',
		default: '#0a31da',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style11' },
				],
				selector:
					'{{WOPB}} .wopb-heading-btn:hover { color:{{headingBtnHoverColor}}; } {{WOPB}} .wopb-heading-btn:hover svg { fill:{{headingBtnHoverColor}}; }',
			},
		],
	},
	headingBorder: {
		type: 'string',
		default: '3',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style3' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { border-bottom-width:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style4' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { border-width:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style6' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before { width:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style7' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style8' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before, {{WOPB}} .wopb-heading-inner:after { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style9' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style10' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before, {{WOPB}} .wopb-heading-inner:after { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style13' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { border-width:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style14' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before, {{WOPB}} .wopb-heading-inner:after { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style15' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style16' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style17' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style19' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:after { width:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style18' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:after { width:{{headingBorder}}px; }',
			},
		],
	},
	headingSpacing: {
		type: 'object',
		default: { lg: 30, unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-heading-wrap {margin-top:0; margin-bottom:{{headingSpacing}}; }',
			},
		],
	},
	headingRadius: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style2' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { border-radius:{{headingRadius}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style3' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { border-radius:{{headingRadius}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style4' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner { border-radius:{{headingRadius}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style5' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { border-radius:{{headingRadius}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style12' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner { border-radius:{{headingRadius}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style13' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner { border-radius:{{headingRadius}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style18' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner { border-radius:{{headingRadius}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style20' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner { border-radius:{{headingRadius}}; }',
			},
		],
	},
	headingPadding: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style2' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style3' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style4' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style5' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style6' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style12' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style13' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style18' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style19' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style20' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
		],
	},
	subHeadingShow: { type: 'boolean', default: false },
	subHeadingText: {
		type: 'string',
		default:
			'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut sem augue. Sed at felis ut enim dignissim sodales.',
		style: [
			{
				depends: [
					{ key: 'subHeadingShow', condition: '==', value: true },
				],
			},
		],
	},
	subHeadingTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			spacing: { lg: '0', unit: 'px' },
			height: { lg: '27', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			family: '',
			weight: '500',
		},
		style: [
			{
				depends: [
					{ key: 'subHeadingShow', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-sub-heading div',
			},
		],
	},
	subHeadingColor: {
		type: 'string',
		default: '#989898',
		style: [
			{
				depends: [
					{ key: 'subHeadingShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-sub-heading div{ color:{{subHeadingColor}}; }',
			},
		],
	},
	subHeadingSpacing: {
		type: 'object',
		default: { lg: { unit: 'px' } },
		style: [
			{
				depends: [
					{ key: 'subHeadingShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} div.wopb-sub-heading-inner{ margin:{{subHeadingSpacing}}; }',
			},
		],
	},
	titleTag: { type: 'string', default: 'h3' },
	titleTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '20', unit: 'px' },
			spacing: { lg: '0', unit: 'px' },
			height: { lg: '28', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '600',
		},
		style: [
			{
				depends: [ { key: 'titleShow', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-cat-title, {{WOPB}} .wopb-product-cat-title a',
			},
		],
	},
	titleColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				depends: [ { key: 'titleShow', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-cat-title a { color:{{titleColor}}; }',
			},
		],
	},
	titleHoverColor: {
		type: 'string',
		default: '#828282',
		style: [
			{
				depends: [ { key: 'titleShow', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-cat-title a:hover { color:{{titleHoverColor}}; }',
			},
		],
	},
	titlePadding: {
		type: 'object',
		default: { lg: { top: 0, bottom: 0, unit: 'px' } },
		style: [
			{
				depends: [ { key: 'titleShow', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-cat-title { padding:{{titlePadding}}; word-wrap: break-word; }',
			},
		],
	},
	descLimit: { type: 'string', default: 5 },
	ShortDescTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '12', unit: 'px' },
			spacing: { lg: '0', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			family: '',
			weight: '',
		},
		style: [
			{
				depends: [ { key: 'descShow', condition: '==', value: true } ],
				selector: '{{WOPB}} .wopb-block-item .wopb-product-cat-desc',
			},
		],
	},
	ShortDescColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				depends: [ { key: 'descShow', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-cat-desc { color:{{ShortDescColor}}; }',
			},
		],
	},
	ShortDescPadding: {
		type: 'object',
		default: { lg: { top: 0, bottom: 0, unit: 'px' } },
		style: [
			{
				depends: [ { key: 'descShow', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-cat-desc { padding:{{ShortDescPadding}}; }',
			},
		],
	},
	contentVerticalPosition: {
		type: 'string',
		default: 'middlePosition',
		style: [
			{
				depends: [
					{
						key: 'contentVerticalPosition',
						condition: '==',
						value: 'topPosition',
					},
				],
				selector:
					'{{WOPB}} .wopb-category-content-topPosition { align-items:flex-start; }',
			},
			{
				depends: [
					{
						key: 'contentVerticalPosition',
						condition: '==',
						value: 'middlePosition',
					},
				],
				selector:
					'{{WOPB}} .wopb-category-content-middlePosition { align-items:center; }',
			},
			{
				depends: [
					{
						key: 'contentVerticalPosition',
						condition: '==',
						value: 'bottomPosition',
					},
				],
				selector:
					'{{WOPB}} .wopb-category-content-bottomPosition { align-items:flex-end; }',
			},
		],
	},
	contentHorizontalPosition: {
		type: 'string',
		default: 'centerPosition',
		style: [
			{
				depends: [
					{
						key: 'contentHorizontalPosition',
						condition: '==',
						value: 'leftPosition',
					},
				],
				selector:
					'{{WOPB}} .wopb-category-content-leftPosition { justify-content:flex-start; }',
			},
			{
				depends: [
					{
						key: 'contentHorizontalPosition',
						condition: '==',
						value: 'centerPosition',
					},
				],
				selector:
					'{{WOPB}} .wopb-category-content-centerPosition { justify-content:center; }',
			},
			{
				depends: [
					{
						key: 'contentHorizontalPosition',
						condition: '==',
						value: 'rightPosition',
					},
				],
				selector:
					'{{WOPB}} .wopb-category-content-rightPosition { justify-content:flex-end; }',
			},
		],
	},
	contenWraptWidth: {
		type: 'object',
		default: { lg: '100', unit: '%' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-category-content-item { width:{{contenWraptWidth}}; }',
			},
		],
	},
	contenWraptHeight: {
		type: 'object',
		default: { lg: '' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-category-content-item { height:{{contenWraptHeight}}; }',
			},
		],
	},
	contentWrapBg: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-category-content-item { background:{{contentWrapBg}}; }',
			},
		],
	},
	contentWrapHoverBg: {
		type: 'string',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-category-content-item:hover  { background:{{contentWrapHoverBg}}; }',
			},
		],
	},
	contentWrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-category-content-item' } ],
	},
	contentWrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-category-content-item:hover ',
			},
		],
	},
	contentWrapRadius: {
		type: 'object',
		default: {
			lg: { top: '4', bottom: '4', left: '4', right: '4', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-category-content-item { border-radius: {{contentWrapRadius}}; }',
			},
		],
	},
	contentWrapHoverRadius: {
		type: 'object',
		default: {
			lg: { top: '4', bottom: '4', left: '4', right: '4', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-category-content-item:hover  { border-radius: {{contentWrapHoverRadius}}; }',
			},
		],
	},
	contentWrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 0, right: 5, bottom: 15, left: 0 },
			color: 'rgba(0,0,0,0.15)',
		},
		style: [ { selector: '{{WOPB}} .wopb-category-content-item' } ],
	},
	contentWrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 0, right: 10, bottom: 25, left: 0 },
			color: 'rgba(0,0,0,0.25)',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-category-content-item:hover ',
			},
		],
	},
	contentWrapSpacing: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '20', right: '20', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-category-content-item { margin: {{contentWrapSpacing}}; }',
			},
		],
	},
	contentWrapPadding: {
		type: 'object',
		default: {
			lg: { top: '16', bottom: '16', left: '0', right: '0', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-category-content-item { padding: {{contentWrapPadding}}; box-sizing: content-box;}',
			},
		],
	},
	categoryrCountText: { type: 'string', default: 'products' },
	categoryrCountTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			spacing: { lg: '0', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			family: '',
			weight: '',
		},
		style: [
			{
				depends: [ { key: 'countShow', condition: '==', value: true } ],
				selector: '{{WOPB}} .wopb-product-cat-count',
			},
		],
	},
	categoryrCountColor: {
		type: 'string',
		default: '#6E6E6E',
		style: [
			{
				depends: [ { key: 'countShow', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-cat-count { color:{{categoryrCountColor}}; }',
			},
		],
	},
	categoryrCountPadding: {
		type: 'object',
		default: { lg: { top: 0, bottom: 0, unit: 'px' } },
		style: [
			{
				depends: [ { key: 'countShow', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-cat-count { padding:{{categoryrCountPadding}}; }',
			},
		],
	},
	imgCrop: {
		type: 'string',
		default: 'full',
		depends: [ { key: 'showImage', condition: '==', value: 'true' } ],
	},
	imgWidth: {
		type: 'object',
		default: { lg: '100', ulg: '%', unit: '%' },
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-item .wopb-block-image { width: {{imgWidth}}; }',
			},
		],
	},
	imgHeight: {
		type: 'object',
		default: { lg: '255', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-item .wopb-block-image img { height: {{imgHeight}}; }',
			},
		],
	},
	imageScale: {
		type: 'string',
		default: 'cover',
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-item .wopb-block-image img {object-fit: {{imageScale}};}',
			},
		],
	},
	imgAnimation: { type: 'string', default: 'zoomIn' },
	imgGrayScale: {
		type: 'object',
		default: { lg: '0', unit: '%' },
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-image img { filter: grayscale({{imgGrayScale}}); }',
			},
		],
	},
	imgHoverGrayScale: {
		type: 'object',
		default: { lg: '0', unit: '%' },
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-item:hover .wopb-block-image img { filter: grayscale({{imgHoverGrayScale}}); }',
			},
		],
	},
	imgRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-image { border-radius:{{imgRadius}}; }',
			},
		],
	},
	imgHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-item:hover .wopb-block-image { border-radius:{{imgHoverRadius}}; }',
			},
		],
	},
	imgShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector: '{{WOPB}} .wopb-block-image',
			},
		],
	},
	imgHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector: '{{WOPB}} .wopb-block-item:hover .wopb-block-image',
			},
		],
	},
	imgMargin: {
		type: 'object',
		default: {
			lg: { top: '0', right: '0', bottom: '0', left: '0', unit: 'px' },
		},
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-item .wopb-block-image { margin: {{imgMargin}}; }',
			},
		],
	},
	readMoreText: { type: 'string', default: '' },
	readMoreTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: 12, unit: 'px' },
			height: { lg: '', unit: 'px' },
			spacing: { lg: 1, unit: 'px' },
			transform: 'uppercase',
			weight: '400',
			decoration: 'none',
			family: '',
		},
		style: [
			{
				depends: [ { key: 'readMore', condition: '==', value: true } ],
				selector: '{{WOPB}} .wopb-block-item .wopb-product-readmore a',
			},
		],
	},
	readMoreColor: {
		type: 'string',
		default: '#0d1523',
		style: [
			{
				depends: [ { key: 'readMore', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-item .wopb-product-readmore a { color:{{readMoreColor}}; }',
			},
		],
	},
	readMoreBgColor: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '' },
		style: [
			{
				depends: [ { key: 'readMore', condition: '==', value: true } ],
				selector: '{{WOPB}} .wopb-block-item .wopb-product-readmore a',
			},
		],
	},
	readMoreBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [ { key: 'readMore', condition: '==', value: true } ],
				selector: '{{WOPB}} .wopb-block-item .wopb-product-readmore a',
			},
		],
	},
	readMoreRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [ { key: 'readMore', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-item .wopb-product-readmore a { border-radius:{{readMoreRadius}}; }',
			},
		],
	},
	readMoreHoverColor: {
		type: 'string',
		default: '#0c32d8',
		style: [
			{
				depends: [ { key: 'readMore', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-item .wopb-product-readmore a:hover { color:{{readMoreHoverColor}}; }',
			},
		],
	},
	readMoreBgHoverColor: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '' },
		style: [
			{
				depends: [ { key: 'readMore', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-item .wopb-product-readmore a:hover',
			},
		],
	},
	readMoreHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [ { key: 'readMore', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-item .wopb-product-readmore a:hover',
			},
		],
	},
	readMoreHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [ { key: 'readMore', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-item .wopb-product-readmore a:hover { border-radius:{{readMoreHoverRadius}}; }',
			},
		],
	},
	readMoreSpacing: {
		type: 'object',
		default: {
			lg: { top: 15, bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				depends: [ { key: 'readMore', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-item .wopb-product-readmore { margin:{{readMoreSpacing}}; line-height: unset;}',
			},
		],
	},
	readMorePadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				depends: [ { key: 'readMore', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-item .wopb-product-readmore a { padding:{{readMorePadding}}; }',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#f5f5f5' },
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#ff176b' },
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapMargin: {
		type: 'object',
		default: { lg: { top: '', bottom: '', unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	wrapInnerPadding: {
		type: 'object',
		default: { lg: { unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { padding:{{wrapInnerPadding}}; }',
			},
		],
	},
	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: { type: 'string', default: '', style: [ { selector: '' } ] },
};
export default attributes;
