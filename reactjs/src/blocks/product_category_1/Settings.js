const { __ } = wp.i18n;
import {
	ArrowStyle,
	ArrowStyleArg,
	CategoryCountStyle,
	CategoryCountStyleArg,
	ContentWrapStyle,
	CustomCssAdvanced,
	DotStyle,
	DotStyleArg,
	GeneralAdvanced,
	GeneralSettingsWithQuery,
	HeadingSettings,
	HeadingSettingsArg,
	ImageStyle,
	ImageStyleArg,
	ReadMoreStyle,
	ReadMoreStyleArg,
	ResponsiveAdvanced,
	ShortDescStyle,
	ShortDescStyleArg,
	TitleStyle,
	TitleStyleArg,
	TypographyTB,
	colorIcon,
	filterFields,
	settingsIcon,
	spacingIcon,
	styleIcon,
	titleColor,
	typoIcon,
	wopbSupport,
} from '../../helper/CommonPanel';
import LinkGenerator from '../../helper/LinkGenerator';
import { Section, Sections } from '../../helper/Sections';
import TemplateModal from '../../helper/TemplateModal';
import WopbToolbarDropdown from '../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar } from '../../helper/ux';

export const features = ( store ) => {
	return [
		{
			type: 'group',
			key: 'productView',
			label: __( 'Product View', 'product-blocks' ),
			justify: true,
			options: [
				{ value: 'grid', label: __( 'Grid View', 'product-blocks' ) },
				{ value: 'slide', label: __( 'Slide View', 'product-blocks' ) },
			],
		},

		{
			type: 'toggle',
			key: 'headingShow',
			label: __( 'Heading', 'product-blocks' ),
		},

		{
			type: store.attributes.productView === 'slide' ? 'toggle' : '',
			key: 'showArrows',
			label: __( 'Arrows', 'product-blocks' ),
		},
		{
			type: store.attributes.productView === 'slide' ? 'toggle' : '',
			key: 'showDots',
			label: __( 'Dots', 'product-blocks' ),
		},

		{
			type: 'toggle',
			key: 'titleShow',
			label: __( 'Title', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'showImage',
			label: __( 'Image', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'countShow',
			label: __( 'Count', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'descShow',
			label: __( 'Description', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'readMore',
			label: __( 'Read More', 'product-blocks' ),
		},
	];
};

export default function Settings( { store } ) {
	const { section } = store;
	const { productView } = store.attributes;

	return (
		<>
			<TemplateModal
				prev={
					LinkGenerator(
						'https://www.wpxpo.com/wowstore/blocks/?',
						'blockPreview'
					) + '#demoid427'
				}
				store={ store }
			/>
			<Sections>
				<Section
					slug="setting"
					title={ __( 'Settings', 'product-blocks' ) }
				>
					<GeneralSettingsWithQuery
						store={ store }
						blockType={ 'category' }
						exclude={ [
							'sortSection',
							'quickView',
							'showOutStock',
							'showInStock',
							'showImage',
							'showCompare',
							'disableFlip',
						] }
						initialOpen={ section.general }
					/>

					{ productView == 'slide' && (
						<ArrowStyle
							depend="showArrows"
							store={ store }
							initialOpen={ section.arrow }
						/>
					) }
					{ productView == 'slide' && (
						<DotStyle
							depend="showDots"
							store={ store }
							initialOpen={ section.dot }
						/>
					) }
					<HeadingSettings
						depend="headingShow"
						store={ store }
						initialOpen={ section.heading }
					/>
					<ContentWrapStyle store={ store } />

					<ImageStyle
						depend="showImage"
						exclude={ [
							'imgOverlay',
							'imgOverlayType',
							'overlayColor',
							'imgOpacity',
							'imgHeight',
						] }
						initialOpen={ section.image }
						store={ store }
						include={ [
							{
								position: 3,
								data: {
									type: 'range',
									key: 'imgHeight',
									min: 1,
									max: 1000,
									step: 1,
									responsive: true,
									unit: [ '%', 'px', 'em' ],
									label: __( 'Height', 'product-blocks' ),
								},
							},
							{
								position: 4,
								data: {
									type: 'tag',
									key: 'imageScale',
									label: __(
										'Image Scale',
										'product-blocks'
									),
									options: [
										{
											value: '',
											label: __(
												'None',
												'product-blocks'
											),
										},
										{
											value: 'cover',
											label: __(
												'Cover',
												'product-blocks'
											),
										},
										{
											value: 'contain',
											label: __(
												'Contain',
												'product-blocks'
											),
										},
										{
											value: 'fill',
											label: __(
												'Fill',
												'product-blocks'
											),
										},
										{
											value: 'scale-down',
											label: __(
												'Scale Down',
												'product-blocks'
											),
										},
									],
								},
							},
						] }
					/>

					<TitleStyle
						depend="titleShow"
						store={ store }
						initialOpen={ section.title }
						exclude={ [ 'titleLength' ] }
					/>
					<CategoryCountStyle
						depend="countShow"
						store={ store }
						initialOpen={ section.count }
					/>
					<ShortDescStyle
						store={ store }
						depend="descShow"
						title="Description"
						exclude={ [ 'showFullShortDesc', 'shortDescLimit' ] }
						include={ [
							{
								position: 0,
								data: {
									type: 'range',
									key: 'descLimit',
									min: 1,
									max: 200,
									label: __(
										'Description Limit',
										'product-blocks'
									),
								},
							},
						] }
						initialOpen={ section.desc }
					/>
					<ReadMoreStyle
						depend="readMore"
						store={ store }
						initialOpen={ section[ 'read-more' ] }
					/>
				</Section>
				<Section
					slug="advanced"
					title={ __( 'Advanced', 'product-blocks' ) }
				>
					<GeneralAdvanced initialOpen={ true } store={ store } />
					<ResponsiveAdvanced pro={ true } store={ store } />
					<CustomCssAdvanced store={ store } />
				</Section>
			</Sections>
			{ wopbSupport() }
		</>
	);
}

export function AddSettingsToToolbar( { selected, store } ) {
	switch ( selected ) {
		case 'title':
			return (
				<WopbToolbarGroup text={ 'Title' }>
					<TypographyTB
						store={ store }
						attrKey={ 'titleTypo' }
						label={ __( 'Title Typography', 'product-blocks' ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Title Spacing', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Title Spacing', 'product-blocks' ),
								options: filterFields(
									[ 'titlePadding' ],
									'__all',
									TitleStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ colorIcon }
						store={ store }
						label={ __( 'Title Color', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Title Color', 'product-blocks' ),
								options: filterFields(
									titleColor,
									'__all',
									TitleStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Title Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Title Settings', 'product-blocks' ),
								options: filterFields(
									null,
									[
										...titleColor,
										'titleTypo',
										'titlePadding',
									],
									TitleStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'desc':
			return (
				<WopbToolbarGroup text={ 'Desc.' }>
					<TypographyTB
						store={ store }
						attrKey={ 'ShortDescTypo' }
						label={ __(
							'Description Typography',
							'product-blocks'
						) }
					/>
					<WopbToolbarDropdown
						buttonContent={ colorIcon }
						store={ store }
						label={ __( 'Description Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Description Settings',
									'product-blocks'
								),
								options: filterFields(
									[
										{
											position: 0,
											data: {
												type: 'range',
												key: 'descLimit',
												min: 1,
												max: 200,
												label: __(
													'Description Limit',
													'product-blocks'
												),
											},
										},
										'ShortDescColor',
										'ShortDescPadding',
									],
									'__all',
									ShortDescStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'image':
			return (
				<WopbToolbarGroup text={ 'Image' }>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Image Dimension', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Image Dimension',
									'product-blocks'
								),
								options: filterFields(
									[
										'imgWidth',
										{
											data: {
												type: 'range',
												key: 'imgHeight',
												min: 1,
												max: 1000,
												step: 1,
												responsive: true,
												unit: [ '%', 'px', 'em' ],
												label: __(
													'Height',
													'product-blocks'
												),
											},
										},
										'imgMargin',
									],
									'__all',
									ImageStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Image Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Image Style', 'product-blocks' ),
								options: filterFields(
									null,
									[
										'imgOverlay',
										'imgOverlayType',
										'overlayColor',
										'imgOpacity',
										'imgWidth',
										'imgHeight',
										'imgMargin',
										'imgCrop',
										'imgAnimation',
									],
									ImageStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Image Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Image Settings', 'product-blocks' ),
								options: filterFields(
									[
										'imgCrop',
										'imgAnimation',
										{
											data: {
												type: 'tag',
												key: 'imageScale',
												label: __(
													'Image Scale',
													'product-blocks'
												),
												options: [
													{
														value: '',
														label: __(
															'None',
															'product-blocks'
														),
													},
													{
														value: 'cover',
														label: __(
															'Cover',
															'product-blocks'
														),
													},
													{
														value: 'contain',
														label: __(
															'Contain',
															'product-blocks'
														),
													},
													{
														value: 'fill',
														label: __(
															'Fill',
															'product-blocks'
														),
													},
													{
														value: 'scale-down',
														label: __(
															'Scale Down',
															'product-blocks'
														),
													},
												],
											},
										},
									],
									'__all',
									ImageStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'heading':
			return (
				<WopbToolbarGroup text={ 'Heading' }>
					<WopbToolbarDropdown
						buttonContent={ typoIcon }
						store={ store }
						label={ __( 'Heading Typography', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Heading Typography',
									'product-blocks'
								),
								options: filterFields(
									[ 'headingTypo', 'subHeadingTypo' ],
									'__all',
									HeadingSettingsArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Heading Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Heading Style', 'product-blocks' ),
								options: filterFields(
									null,
									[ 'headingTypo', 'subHeadingTypo' ],
									HeadingSettingsArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'read-more':
			return (
				<WopbToolbarGroup text={ 'Read More' }>
					<TypographyTB
						store={ store }
						attrKey={ 'readMoreTypo' }
						label={ __( 'Read More Typography', 'product-blocks' ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Read More Spacing', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Read More Spacing',
									'product-blocks'
								),
								options: filterFields(
									[ 'readMoreSpacing', 'readMorePadding' ],
									'__all',
									ReadMoreStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Read More Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Read More Style',
									'product-blocks'
								),
								options: filterFields(
									[ 'readMoreText', 'rmTab' ],
									'__all',
									ReadMoreStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'arrow':
			return (
				<WopbToolbarGroup text={ 'Arrows' }>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Arrow Dimension', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Arrow Dimension',
									'product-blocks'
								),
								options: filterFields(
									null,
									[ 'arrowStyle', 'aTab' ],
									ArrowStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Arrow Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Arrow Style', 'product-blocks' ),
								options: filterFields(
									[ 'arrowStyle', 'aTab' ],
									'__all',
									ArrowStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'dot':
			return (
				<WopbToolbarGroup text={ 'Dots' }>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Dots Dimension', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Dots Dimension', 'product-blocks' ),
								options: filterFields(
									null,
									[ 'dTab' ],
									DotStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Dots Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Arrow Style', 'product-blocks' ),
								options: filterFields(
									[ 'dTab' ],
									'__all',
									DotStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'count':
			return (
				<WopbToolbarGroup text={ 'Count' }>
					<TypographyTB
						store={ store }
						attrKey={ 'categoryrCountTypo' }
						label={ __( 'Count Typography', 'product-blocks' ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Count Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Count Style', 'product-blocks' ),
								options: filterFields(
									null,
									[ 'categoryrCountTypo' ],
									CategoryCountStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		default:
			return null;
	}
}
