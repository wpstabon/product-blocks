const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import Save from './Save';
registerBlockType( 'product-blocks/wrapper', {
	title: __( 'Wrapper', 'product-blocks' ),
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/wrapper.svg' }
			alt={ __( 'Wrapper', 'product-blocks' ) }
			style={ { width: '100%' } }
		/>
	),
	category: 'product-blocks',
	description: __(
		'Use it as a container for single or multiple blocks.',
		'product-blocks'
	),
	keywords: [
		__( 'wrapper', 'product-blocks' ),
		__( 'wrap', 'product-blocks' ),
		__( 'column', 'product-blocks' ),
		__( 'row', 'product-blocks' ),
	],
	supports: {
		align: [ 'center', 'wide', 'full' ],
	},
	example: {
		attributes: {
			previewId: wopb_data.url + 'assets/img/preview/wrapper.svg',
		},
	},
	edit: Edit,
	save: Save,
	attributes: {
		blockId: {
			type: 'string',
			default: '',
		},
		currentPostId: { type: 'string', default: '' },
		previewId: {
			type: 'string',
			default: '',
		},

		//--------------------------
		//  Wrapper Background Style Start
		//--------------------------
		imageUpload: {
			type: 'object',
			default: { id: '', url: '' },
			style: [ { selector: '{{WOPB}} div.wopb-wrapper-block' } ],
		},
		imagePosition: {
			type: 'string',
			default: '',
			style: [
				{
					selector:
						'{{WOPB}} div.wopb-wrapper-block { background-position: {{imagePosition}}; }',
				},
			],
		},
		imagePositionX: {
			type: 'object',
			default: { lg: '2', unit: 'px' },
			style: [
				{
					depends: [
						{
							key: 'imagePosition',
							condition: '==',
							value: 'custom',
						},
					],
					selector:
						'{{WOPB}} div.wopb-wrapper-block {background-position-x: {{imagePositionX}}; }',
				},
			],
		},
		imagePositionY: {
			type: 'object',
			default: { lg: '2', unit: 'px' },
			style: [
				{
					depends: [
						{
							key: 'imagePosition',
							condition: '==',
							value: 'custom',
						},
					],
					selector:
						'{{WOPB}} div.wopb-wrapper-block {background-position-y: {{imagePositionY}}; }',
				},
			],
		},
		imageSize: {
			type: 'string',
			default: 'auto',
			style: [
				{
					selector:
						'{{WOPB}} div.wopb-wrapper-block { background-size: {{imageSize}}; }',
				},
			],
		},
		imageCustomSize: {
			type: 'object',
			default: { lg: '150', unit: 'px' },
			style: [
				{
					depends: [
						{ key: 'imageSize', condition: '==', value: 'custom' },
					],
					selector:
						'{{WOPB}} div.wopb-wrapper-block { background-size: {{imageCustomSize}} auto; }',
				},
			],
		},
		imageRepeat: {
			type: 'string',
			default: '',
			style: [
				{
					selector:
						'{{WOPB}} div.wopb-wrapper-block { background-repeat: {{imageRepeat}}; }',
				},
			],
		},
		imageAttachment: {
			type: 'string',
			default: 'scroll',
			style: [
				{
					selector:
						'{{WOPB}} div.wopb-wrapper-block { background-attachment: {{imageAttachment}}; }',
				},
			],
		},
		//--------------------------
		//  Wrapper Background Style End
		//--------------------------

		//--------------------------
		//  Wrapper Style
		//--------------------------
		wrapBg: {
			type: 'object',
			default: { openColor: 1, type: 'color', color: '#f5f5f5' },
			style: [ { selector: '{{WOPB}} .wopb-wrapper-block' } ],
		},
		wrapBorder: {
			type: 'object',
			default: {
				openBorder: 0,
				width: { top: 1, right: 1, bottom: 1, left: 1 },
				color: '#009fd4',
				type: 'solid',
			},
			style: [ { selector: '{{WOPB}} .wopb-wrapper-block' } ],
		},
		wrapShadow: {
			type: 'object',
			default: {
				openShadow: 0,
				width: { top: 1, right: 1, bottom: 1, left: 1 },
				color: '#009fd4',
			},
			style: [ { selector: '{{WOPB}} .wopb-wrapper-block' } ],
		},
		wrapRadius: {
			type: 'object',
			default: { lg: '', unit: 'px' },
			style: [
				{
					selector:
						'{{WOPB}} .wopb-wrapper-block { border-radius:{{wrapRadius}}; }',
				},
			],
		},
		wrapHoverBackground: {
			type: 'object',
			default: { openColor: 0, type: 'color', color: '#ff176b' },
			style: [ { selector: '{{WOPB}} .wopb-wrapper-block:hover' } ],
		},
		wrapHoverBorder: {
			type: 'object',
			default: {
				openBorder: 0,
				width: { top: 1, right: 1, bottom: 1, left: 1 },
				color: '#009fd4',
				type: 'solid',
			},
			style: [ { selector: '{{WOPB}} .wopb-wrapper-block:hover' } ],
		},
		wrapHoverRadius: {
			type: 'object',
			default: { lg: '', unit: 'px' },
			style: [
				{
					selector:
						'{{WOPB}} .wopb-wrapper-block:hover { border-radius:{{wrapHoverRadius}}; }',
				},
			],
		},
		wrapHoverShadow: {
			type: 'object',
			default: {
				openShadow: 0,
				width: { top: 1, right: 1, bottom: 1, left: 1 },
				color: '#009fd4',
			},
			style: [ { selector: '{{WOPB}} .wopb-wrapper-block:hover' } ],
		},
		wrapMargin: {
			type: 'object',
			default: { lg: { top: '', bottom: '', unit: 'px' } },
			style: [
				{
					selector:
						'{{WOPB}} .wopb-wrapper-block { margin:{{wrapMargin}}; }',
				},
			],
		},
		wrapOuterPadding: {
			type: 'object',
			default: {
				lg: {
					top: '30',
					bottom: '30',
					left: '30',
					right: '30',
					unit: 'px',
				},
			},
			style: [
				{
					selector:
						'{{WOPB}} .wopb-wrapper-block { padding:{{wrapOuterPadding}}; }',
				},
			],
		},
		advanceId: {
			type: 'string',
			default: '',
		},
		advanceZindex: {
			type: 'number',
			default: '',
			style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
		},
		hideExtraLarge: {
			type: 'boolean',
			default: '',
			style: [ { selector: '{{WOPB}} {display:none;}' } ],
		},
		hideDesktop: {
			type: 'boolean',
			default: '',
			style: [ { selector: '{{WOPB}} {display:none;}' } ],
		},
		hideTablet: {
			type: 'boolean',
			default: '',
			style: [ { selector: '{{WOPB}} {display:none;}' } ],
		},
		hideMobile: {
			type: 'boolean',
			default: '',
			style: [ { selector: '{{WOPB}} {display:none;}' } ],
		},
		advanceCss: {
			type: 'string',
			default: '',
			style: [ { selector: '' } ],
		},
	},
} );
