const { __ } = wp.i18n;
const { InspectorControls, InnerBlocks } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import {
	GeneralAdvanced,
	CustomCssAdvanced,
	ResponsiveAdvanced,
	CommonSettings,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Sections, Section } from '../../helper/Sections';
import useBlockId from '../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const { setAttributes, name, attributes, clientId, className } = props;

	const { blockId, advanceId, previewId, imagePosition, imageSize } =
		attributes;

	useBlockId( props, true );

	const store = {
		setAttributes,
		name,
		attributes,
		clientId,
	};

	if ( previewId ) {
		return (
			<img
				style={ { marginTop: '0px', width: '420px' } }
				src={ previewId }
			/>
		);
	}

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/wrapper', blockId );
	}

	return (
		<>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Style', 'product-blocks' ) }
					>
						{ /* //--------------------------
                                //  Wrapper Background Style Start
                                //-------------------------- */ }
						<CommonSettings
							initialOpen={ true }
							title={ __( 'Background Image', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'media',
										key: 'imageUpload',
										label: __( 'Image', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'select',
										key: 'imagePosition',
										label: __(
											'Position',
											'product-blocks'
										),
										options: [
											{
												value: '',
												label: __(
													'Default',
													'product-blocks'
												),
											},
											{
												value: 'left top',
												label: __(
													'Left Top',
													'product-blocks'
												),
											},
											{
												value: 'left center',
												label: __(
													'Left Center',
													'product-blocks'
												),
											},
											{
												value: 'left bottom',
												label: __(
													'Left Bottom',
													'product-blocks'
												),
											},
											{
												value: 'right top',
												label: __(
													'Right Top',
													'product-blocks'
												),
											},
											{
												value: 'right center',
												label: __(
													'Right Center',
													'product-blocks'
												),
											},
											{
												value: 'right bottom',
												label: __(
													'Right Bottom',
													'product-blocks'
												),
											},
											{
												value: 'center top',
												label: __(
													'Center Top',
													'product-blocks'
												),
											},
											{
												value: 'center center',
												label: __(
													'Center Center',
													'product-blocks'
												),
											},
											{
												value: 'center bottom',
												label: __(
													'Center Bottom',
													'product-blocks'
												),
											},
											{
												value: 'custom',
												label: __(
													'Custom',
													'product-blocks'
												),
											},
										],
									},
								},
								imagePosition == 'custom' && {
									data: {
										type: 'range',
										key: 'imagePositionX',
										min: 0,
										max: 500,
										step: 1,
										responsive: true,
										unit: true,
										label: __(
											'Position X',
											'product-blocks'
										),
									},
								},
								imagePosition == 'custom' && {
									data: {
										type: 'range',
										key: 'imagePositionY',
										min: 0,
										max: 500,
										step: 1,
										responsive: true,
										unit: true,
										label: __(
											'Position Y',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'tag',
										key: 'imageSize',
										label: __( 'Size', 'product-blocks' ),
										options: [
											{
												value: '',
												label: __(
													'None',
													'product-blocks'
												),
											},
											{
												value: 'auto',
												label: __(
													'Auto',
													'product-blocks'
												),
											},
											{
												value: 'contain',
												label: __(
													'Contain',
													'product-blocks'
												),
											},
											{
												value: 'cover',
												label: __(
													'Cover',
													'product-blocks'
												),
											},
											{
												value: 'custom',
												label: __(
													'Custom',
													'product-blocks'
												),
											},
										],
									},
								},
								imageSize == 'custom' && {
									data: {
										type: 'range',
										key: 'imageCustomSize',
										min: 10,
										max: 1000,
										step: 2,
										responsive: true,
										unit: true,
										label: __(
											'Image Height',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'tag',
										key: 'imageRepeat',
										label: __( 'Repeat', 'product-blocks' ),
										options: [
											{
												value: '',
												label: __(
													'None',
													'product-blocks'
												),
											},
											{
												value: 'repeat',
												label: __(
													'Repeat',
													'product-blocks'
												),
											},
											{
												value: 'no-repeat',
												label: __(
													'No-repeat',
													'product-blocks'
												),
											},
											{
												value: 'repeat-x',
												label: __(
													'Repeat-x',
													'product-blocks'
												),
											},
											{
												value: 'repeat-y',
												label: __(
													'Repeat-y',
													'product-blocks'
												),
											},
											{
												value: 'space',
												label: __(
													'Space',
													'product-blocks'
												),
											},
											{
												value: 'round',
												label: __(
													'Round',
													'product-blocks'
												),
											},
										],
									},
								},
								{
									data: {
										type: 'tag',
										key: 'imageAttachment',
										label: __(
											'Attachment',
											'product-blocks'
										),
										options: [
											{
												value: '',
												label: __(
													'None',
													'product-blocks'
												),
											},
											{
												value: 'scroll',
												label: __(
													'Scroll',
													'product-blocks'
												),
											},
											{
												value: 'fixed',
												label: __(
													'Fixed',
													'product-blocks'
												),
											},
										],
									},
								},
							] }
						/>
						<GeneralAdvanced
							initialOpen={ false }
							store={ store }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>
			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ `wopb-wrapper-block` }>
					<InnerBlocks templateLock={ false } />
				</div>
			</div>
		</>
	);
}
