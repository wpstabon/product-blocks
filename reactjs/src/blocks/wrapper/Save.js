const { InnerBlocks } = wp.blockEditor;

export default function Save( props ) {
	const { blockId, advanceId } = props.attributes;

	return (
		<div
			{ ...( advanceId && { id: advanceId } ) }
			className={ `wopb-block-${ blockId }` }
		>
			<div className={ `wopb-wrapper-block` }>
				<InnerBlocks.Content />
			</div>
		</div>
	);
}
