const attributes = {
	blockId: { type: 'string', default: '' },
	currentPostId: { type: 'string', default: '' },
	// Icon
	iconType: {
		type: 'string',
		default: 'icon',
	},
	iconSvg: {
		type: 'string',
		default: 'compare_1',
		style: [
			{
				depends: [
					{ key: 'iconType', condition: '==', value: 'icon' },
				],
			},
		],
	},
	iconSize: {
		type: 'object',
		default: { lg: '24' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-menu-compare-icon svg, {{WOPB}} .wopb-menu-compare-icon img  { height: {{iconSize}}px; width: {{iconSize}}px; } ',
			},
		],
	},
	iconImage: {
		type: 'object',
		default: '',
		style: [
			{
				depends: [
					{ key: 'iconType', condition: '==', value: 'image' },
				],
			},
		],
	},
	iconImageScale: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [
					{ key: 'iconType', condition: '==', value: 'image' },
				],
				selector:
					'{{WOPB}} .wopb-menu-compare-icon img { object-fit: {{iconImageScale}}; } ',
			},
		],
	},
	iconImageRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'iconType', condition: '==', value: 'image' },
				],
				selector:
					'{{WOPB}} .wopb-menu-compare-icon img { border-radius: {{iconImageRadius}}; }',
			},
		],
	},
	iconColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				depends: [
					{ key: 'iconType', condition: '==', value: 'icon' },
				],
				selector:
					'{{WOPB}} .wopb-menu-compare-icon svg { color: {{iconColor}}; } ',
			},
		],
	},
	iconWrapBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '' },
		style: [
			{
				selector: '{{WOPB}} .wopb-menu-compare-icon',
			},
		],
	},
	iconWrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-menu-compare-icon',
			},
		],
	},
	iconWrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-menu-compare-icon { border-radius:{{iconWrapRadius}}; }',
			},
		],
	},
	iconWrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '',
		},
		style: [ { selector: '{{WOPB}} .wopb-menu-compare-icon' } ],
	},
	iconHvrColor: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [
					{ key: 'iconType', condition: '==', value: 'icon' },
				],
				selector:
					'{{WOPB}} .wopb-menu-compare-icon:hover svg { color: {{iconHvrColor}}; } ',
			},
		],
	},
	iconWrapHvrBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '' },
		style: [
			{
				selector: '{{WOPB}} .wopb-menu-compare-icon:hover',
			},
		],
	},
	iconWrapHvrBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-menu-compare-icon:hover',
			},
		],
	},
	iconWrapHvrRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-menu-compare-icon:hover { border-radius:{{iconWrapHvrRadius}}; }',
			},
		],
	},
	iconWrapHvrShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-menu-compare-icon:hover' } ],
	},
	iconWrapPadding: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-menu-compare-icon { padding:{{iconWrapPadding}}; }',
			},
		],
	},

	// count:
	showCount: {
		type: 'boolean',
		default: true,
	},
	countVerticalPos: {
		type: 'string',
		default: '-10',
		style: [
			{
				depends: [ { key: 'showCount', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-menu-compare-count { top: {{countVerticalPos}}px; } ',
			},
		],
	},
	countHorizontalPos: {
		type: 'string',
		default: '18',
		style: [
			{
				depends: [ { key: 'showCount', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-menu-compare-count { left: {{countHorizontalPos}}px; } ',
			},
		],
	},
	countBgSize: {
		type: 'object',
		default: { lg: '16' },
		style: [
			{
				depends: [ { key: 'showCount', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-menu-compare-count { height: {{countBgSize}}px; width: {{countBgSize}}px; } ',
			},
		],
	},
	countTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: 10, unit: 'px' },
			height: { lg: '14', unit: 'px' },
			transform: '',
			weight: '400',
			decoration: 'none',
			family: '',
		},
		style: [
			{
				depends: [ { key: 'showCount', condition: '==', value: true } ],
				selector: '{{WOPB}} .wopb-menu-compare-count',
			},
		],
	},
	countColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [ { key: 'showCount', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-menu-compare-count { color: {{countColor}}; } ',
			},
		],
	},
	countBg: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				depends: [ { key: 'showCount', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-menu-compare-count { background-color: {{countBg}}; } ',
			},
		],
	},
	countBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-menu-compare-count',
			},
		],
	},
	countRadius: {
		type: 'object',
		default: {
			lg: { top: 50, bottom: 50, left: 50, right: 50, unit: '%' },
			unit: '%',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-menu-compare-count { border-radius:{{countRadius}}; }',
			},
		],
	},

	//label:
	showLabel: {
		type: 'boolean',
		default: false,
	},
	labelGap: {
		type: 'object',
		default: { lg: '6', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showLabel', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-menu-compare-wrapper { gap: {{labelGap}}; } ',
			},
		],
	},
	labelText: {
		type: 'string',
		default: 'Compare',
		style: [
			{
				depends: [ { key: 'showLabel', condition: '==', value: true } ],
			},
		],
	},
	labelPosition: {
		type: 'string',
		default: 'right',
		style: [
			{
				depends: [ { key: 'showLabel', condition: '==', value: true } ],
			},
		],
	},
	labelTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: 10, unit: 'px' },
			height: { lg: '14', unit: 'px' },
			transform: '',
			weight: '400',
			decoration: 'none',
			family: '',
		},
		style: [
			{
				depends: [ { key: 'showLabel', condition: '==', value: true } ],
				selector: '{{WOPB}} .wopb-menu-compare-label',
			},
		],
	},
	labelColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				depends: [ { key: 'showLabel', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-menu-compare-label  { color: {{labelColor}}; } ',
			},
		],
	},
	labelHvrColor: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				depends: [ { key: 'showLabel', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-menu-compare-label:hover  { color: {{labelHvrColor}}; } ',
			},
		],
	},

	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: { type: 'string', default: '', style: [ { selector: '' } ] },
};
export default attributes;
