const { __ } = wp.i18n;
import {
	CommonSettings,
	CustomCssAdvanced,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Section, Sections } from '../../helper/Sections';
import icons from '../../helper/icons';

export default function Settings( { store } ) {
	const { section } = store;
	const { showCount, showLabel } = store.attributes;

	return (
		<>
			<Sections>
				<Section
					slug="setting"
					title={ __( 'Settings', 'product-blocks' ) }
				>
					<CommonSettings
						initialOpen={ section.icon }
						title={ __( 'Icon', 'product-blocks' ) }
						store={ store }
						include={ [
							{
								data: {
									type: 'tag',
									key: 'iconType',
									label: __( 'Icon Type', 'product-blocks' ),
									options: [
										{
											value: 'icon',
											label: __(
												'Icon',
												'product-blocks'
											),
										},
										{
											value: 'image',
											label: __(
												'Image',
												'product-blocks'
											),
										},
									],
								},
							},
							{
								data: {
									type: 'tag',
									key: 'iconSvg',
									label: __(
										'Choose Icon',
										'product-blocks'
									),
									options: [
										{
											value: 'compare_1',
											label: icons.compare_1,
										},
										{
											value: 'compare_3',
											label: icons.compare_3,
										},
										{
											value: 'compare_5',
											label: icons.compare_5,
										},
										{
											value: 'compare_6',
											label: icons.compare_6,
										},
									],
								},
							},
							{
								data: {
									type: 'range',
									key: 'iconSize',
									min: 0,
									max: 300,
									step: 1,
									responsive: true,
									unit: false,
									label: __( 'Icon Size', 'product-blocks' ),
								},
							},
							{
								data: {
									type: 'media',
									key: 'iconImage',
									label: __(
										'Custom Image',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'tag',
									key: 'iconImageScale',
									label: __(
										'Image Scale',
										'product-blocks'
									),
									options: [
										{
											value: '',
											label: __(
												'None',
												'product-blocks'
											),
										},
										{
											value: 'cover',
											label: __(
												'Cover',
												'product-blocks'
											),
										},
										{
											value: 'contain',
											label: __(
												'Contain',
												'product-blocks'
											),
										},
										{
											value: 'fill',
											label: __(
												'Fill',
												'product-blocks'
											),
										},
										{
											value: 'scale-down',
											label: __(
												'Scale Down',
												'product-blocks'
											),
										},
									],
								},
							},
							{
								data: {
									type: 'dimension',
									key: 'iconImageRadius',
									label: __(
										'Image Border Radius',
										'product-blocks'
									),
									step: 1,
									unit: true,
									responsive: true,
								},
							},
							{
								data: {
									type: 'tab',
									content: [
										{
											name: 'normal',
											title: __(
												'Normal',
												'product-blocks'
											),
											options: [
												{
													type: 'color',
													key: 'iconColor',
													label: __(
														'Color',
														'product-blocks'
													),
												},
												{
													type: 'color2',
													key: 'iconWrapBg',
													label: __(
														'Background',
														'product-blocks'
													),
												},
												{
													type: 'border',
													key: 'iconWrapBorder',
													label: __(
														'Border',
														'product-blocks'
													),
												},
												{
													type: 'dimension',
													key: 'iconWrapRadius',
													label: __(
														'Border Radius',
														'product-blocks'
													),
													step: 1,
													unit: true,
													responsive: true,
												},
												{
													type: 'boxshadow',
													key: 'iconWrapShadow',
													label: __(
														'BoxShadow',
														'product-blocks'
													),
												},
											],
										},
										{
											name: 'hover',
											title: __(
												'Hover',
												'product-blocks'
											),
											options: [
												{
													type: 'color',
													key: 'iconHvrColor',
													label: __(
														'Color',
														'product-blocks'
													),
												},
												{
													type: 'color2',
													key: 'iconWrapHvrBg',
													label: __(
														'Background',
														'product-blocks'
													),
												},
												{
													type: 'border',
													key: 'iconWrapHvrBorder',
													label: __(
														'Border',
														'product-blocks'
													),
												},
												{
													type: 'dimension',
													key: 'iconWrapHvrRadius',
													label: __(
														'Border Radius',
														'product-blocks'
													),
													step: 1,
													unit: true,
													responsive: true,
												},
												{
													type: 'boxshadow',
													key: 'iconWrapHvrShadow',
													label: __(
														'BoxShadow',
														'product-blocks'
													),
												},
											],
										},
									],
								},
							},
							{
								data: {
									type: 'dimension',
									key: 'iconWrapPadding',
									label: __( 'Padding', 'product-blocks' ),
									step: 1,
									unit: true,
									responsive: true,
								},
							},
						] }
					/>
					<CommonSettings
						initialOpen={ section.count }
						title={ __( 'Count', 'product-blocks' ) }
						depend={ 'showCount' }
						store={ store }
						include={ [
							{
								data: {
									type: 'range',
									key: 'countVerticalPos',
									min: -200,
									max: 200,
									step: 1,
									responsive: false,
									unit: false,
									label: __(
										'Vertical Position',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'range',
									key: 'countHorizontalPos',
									min: -200,
									max: 200,
									step: 1,
									responsive: false,
									unit: false,
									label: __(
										'Horizontal Position',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'range',
									key: 'countBgSize',
									min: 10,
									max: 200,
									step: 1,
									responsive: true,
									unit: false,
									label: __(
										'Count Background Size',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'typography',
									key: 'countTypo',
									label: __(
										'Count Typography',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'color',
									key: 'countColor',
									label: __(
										'Count Color',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'color',
									key: 'countBg',
									label: __(
										'Count Background Color',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'border',
									key: 'countBorder',
									label: __( 'Border', 'product-blocks' ),
								},
							},
							{
								data: {
									type: 'dimension',
									key: 'countRadius',
									label: __(
										'Border Radius',
										'product-blocks'
									),
									step: 1,
									unit: true,
									responsive: true,
								},
							},
						] }
					/>
					<CommonSettings
						initialOpen={ section.label }
						title={ __( 'Label', 'product-blocks' ) }
						depend={ 'showLabel' }
						store={ store }
						include={ [
							{
								data: {
									type: 'text',
									key: 'labelText',
									label: __( 'Label Text', 'product-blocks' ),
								},
							},
							{
								data: {
									type: 'tag',
									key: 'labelPosition',
									label: __(
										'Label Position',
										'product-blocks'
									),
									options: [
										{
											value: 'left',
											label: __(
												'Left',
												'product-blocks'
											),
										},
										{
											value: 'right',
											label: __(
												'Right',
												'product-blocks'
											),
										},
									],
								},
							},
							{
								data: {
									type: 'range',
									key: 'labelGap',
									min: 0,
									max: 200,
									step: 1,
									responsive: true,
									unit: [ 'px' ],
									label: __(
										'Icon/Label Gap',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'typography',
									key: 'labelTypo',
									label: __( 'Typography', 'product-blocks' ),
								},
							},
							{
								data: {
									type: 'color',
									key: 'labelColor',
									label: __( 'Color', 'product-blocks' ),
								},
							},
							{
								data: {
									type: 'color',
									key: 'labelHvrColor',
									label: __(
										'Hover Color',
										'product-blocks'
									),
								},
							},
						] }
					/>
				</Section>
				<Section
					slug="advanced"
					title={ __( 'Advanced', 'product-blocks' ) }
				>
					<ResponsiveAdvanced pro={ true } store={ store } />
					<CustomCssAdvanced store={ store } />
				</Section>
			</Sections>
			{ wopbSupport() }
		</>
	);
}
