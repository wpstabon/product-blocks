const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';
registerBlockType( 'product-blocks/menu-compare', {
	title: __( 'Menu Compare', 'product-blocks' ),
	icon: (
		<div className={ `wopb-block-inserter-icon-section` }>
			<img
				src={ wopb_data.url + 'assets/img/blocks/compare.svg' }
				alt={ __( 'Menu Compare', 'product-blocks' ) }
				style={ { width: '100%' } }
			/>
		</div>
	),
	category: 'product-blocks',
	description: __( 'Add customized Compare Icon to Menu.', 'product-blocks' ),
	keywords: [
		__( 'menu', 'product-blocks' ),
		__( 'compare', 'product-blocks' ),
	],
	supports: {
		align: [ 'center', 'wide', 'full' ],
	},
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
