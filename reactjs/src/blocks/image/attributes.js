const attributes = {
	blockId: { type: 'string', default: '' },
	currentPostId: { type: 'string', default: '' },
	imageUpload: {
		type: 'object',
		default: {
			id: '999999',
			url: wopb_data.url + '/assets/img/wopb-fallback-img.png',
		},
	},
	linkType: { type: 'string', default: 'link' },
	imgLink: { type: 'string', default: '' },
	linkTarget: { type: 'string', default: '_blank' },
	imgAlt: { type: 'string', default: 'Image' },
	imgAlignment: {
		type: 'object',
		default: { lg: 'left' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-image-block-wrapper {text-align: {{imgAlignment}};}',
			},
		],
	},
	imgWidth: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-image-block { max-width: {{imgWidth}}; }',
			},
		],
	},
	imgHeight: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-image-block {object-fit: cover; height: {{imgHeight}}; } {{WOPB}} .wopb-image-block .wopb-image { height: 100%;object-fit: cover; }',
			},
		],
	},
	imgAnimation: { type: 'string', default: 'none' },
	imgGrayScale: {
		type: 'object',
		default: { lg: '0', unit: '%' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-image { filter: grayscale({{imgGrayScale}}); }',
			},
		],
	},
	imgHoverGrayScale: {
		type: 'object',
		default: { lg: '0', unit: '%' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-image-block-wrapper:hover .wopb-image { filter: grayscale({{imgHoverGrayScale}}); }',
			},
		],
	},
	imgRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-image-block { border-radius:{{imgRadius}}; }',
			},
		],
	},
	imgHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-image-block-wrapper:hover .wopb-image-block { border-radius:{{imgHoverRadius}}; }',
			},
		],
	},
	imgShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-image-block' } ],
	},
	imgHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-image-block-wrapper:hover .wopb-image-block',
			},
		],
	},
	imgMargin: {
		type: 'object',
		default: { lg: { top: 0, bottom: 0, left: 0, right: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-image-block-wrapper { margin: {{imgMargin}}; }',
			},
		],
	},
	imgOverlay: { type: 'boolean', default: false },
	imgOverlayType: {
		type: 'string',
		default: 'default',
		style: [
			{
				depends: [
					{ key: 'imgOverlay', condition: '==', value: true },
				],
			},
		],
	},
	overlayColor: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#0e1523' },
		style: [
			{
				depends: [
					{ key: 'imgOverlayType', condition: '==', value: 'custom' },
				],
				selector: '{{WOPB}} .wopb-image-block::before',
			},
		],
	},
	imgOpacity: {
		type: 'string',
		default: 0.69999999999999996,
		style: [
			{
				depends: [
					{ key: 'imgOverlayType', condition: '==', value: 'custom' },
				],
				selector:
					'{{WOPB}} .wopb-image-block::before { opacity: {{imgOpacity}}; }',
			},
		],
	},
	headingText: { type: 'string', default: 'This is a Heading Example' },
	headingEnable: { type: 'boolean', default: false },
	headingColor: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [
					{ key: 'headingEnable', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-image-block-wrapper .wopb-image-caption { color:{{headingColor}}; }',
			},
		],
	},
	alignment: {
		type: 'object',
		default: { lg: 'left' },
		style: [
			{
				depends: [
					{ key: 'headingEnable', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-image-block-wrapper .wopb-image-caption {text-align: {{alignment}};}',
			},
		],
	},
	headingTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '24', unit: 'px' },
			decoration: '',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{
				depends: [
					{ key: 'headingEnable', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-image-block-wrapper .wopb-image-caption',
			},
		],
	},
	headingMargin: {
		type: 'object',
		default: {
			lg: { top: '10', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				depends: [
					{ key: 'headingEnable', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-image-block-wrapper .wopb-image-caption { margin:{{headingMargin}}; }',
			},
		],
	},
	buttonEnable: { type: 'boolean', default: false },
	btnText: { type: 'string', default: 'Free Download' },
	btnLink: { type: 'string', default: '#' },
	btnTarget: { type: 'string', default: '_blank' },
	btnPosition: { type: 'string', default: 'centerCenter' },
	btnTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: 14, unit: 'px' },
			height: { lg: 20, unit: 'px' },
			spacing: { lg: 0, unit: 'px' },
			transform: 'capitalize',
			weight: '',
			decoration: 'none',
			family: '',
		},
		style: [
			{
				depends: [
					{ key: 'linkType', condition: '==', value: 'button' },
				],
				selector:
					'{{WOPB}} .wopb-image-block-wrapper .wopb-image-button a',
			},
		],
	},
	btnColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [
					{ key: 'linkType', condition: '==', value: 'button' },
				],
				selector:
					'{{WOPB}} .wopb-image-block-wrapper .wopb-image-button a { color:{{btnColor}}; }',
			},
		],
	},
	btnBgColor: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#ff176b' },
		style: [
			{
				depends: [
					{ key: 'linkType', condition: '==', value: 'button' },
				],
				selector:
					'{{WOPB}} .wopb-image-block-wrapper .wopb-image-button a',
			},
		],
	},
	btnBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{ key: 'linkType', condition: '==', value: 'button' },
				],
				selector:
					'{{WOPB}} .wopb-image-block-wrapper .wopb-image-button a',
			},
		],
	},
	btnRadius: {
		type: 'object',
		default: { lg: '2', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'linkType', condition: '==', value: 'button' },
				],
				selector:
					'{{WOPB}} .wopb-image-block-wrapper .wopb-image-button a { border-radius:{{btnRadius}}; }',
			},
		],
	},
	btnShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [
					{ key: 'linkType', condition: '==', value: 'button' },
				],
				selector:
					'{{WOPB}} .wopb-image-block-wrapper .wopb-image-button a',
			},
		],
	},
	btnHoverColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [
					{ key: 'linkType', condition: '==', value: 'button' },
				],
				selector:
					'{{WOPB}} .wopb-image-block-wrapper .wopb-image-button a:hover { color:{{btnHoverColor}}; }',
			},
		],
	},
	btnBgHoverColor: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#1239e2' },
		style: [
			{
				depends: [
					{ key: 'linkType', condition: '==', value: 'button' },
				],
				selector:
					'{{WOPB}} .wopb-image-block-wrapper .wopb-image-button a:hover',
			},
		],
	},
	btnHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{ key: 'linkType', condition: '==', value: 'button' },
				],
				selector:
					'{{WOPB}} .wopb-image-block-wrapper .wopb-image-button a:hover',
			},
		],
	},
	btnHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'linkType', condition: '==', value: 'button' },
				],
				selector:
					'{{WOPB}} .wopb-image-block-wrapper .wopb-image-button a:hover { border-radius:{{btnHoverRadius}}; }',
			},
		],
	},
	btnHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [
					{ key: 'linkType', condition: '==', value: 'button' },
				],
				selector:
					'{{WOPB}} .wopb-image-block-wrapper .wopb-image-button a:hover',
			},
		],
	},
	btnSacing: {
		type: 'object',
		default: { lg: { top: 0, bottom: 0, left: 0, right: 0, unit: 'px' } },
		style: [
			{
				depends: [
					{ key: 'linkType', condition: '==', value: 'button' },
				],
				selector:
					'{{WOPB}} .wopb-image-block-wrapper .wopb-image-button a { margin:{{btnSacing}}; }',
			},
		],
	},
	btnPadding: {
		type: 'object',
		default: {
			lg: { top: '6', bottom: '6', left: '12', right: '12', unit: 'px' },
		},
		style: [
			{
				depends: [
					{ key: 'linkType', condition: '==', value: 'button' },
				],
				selector:
					'{{WOPB}} .wopb-image-block-wrapper .wopb-image-button a { padding:{{btnPadding}}; }',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#f5f5f5' },
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#ff176b' },
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapMargin: {
		type: 'object',
		default: { lg: { top: '', bottom: '', unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: { type: 'string', default: '', style: [ { selector: '' } ] },
};

export default attributes;
