const { __ } = wp.i18n;
const { InspectorControls, RichText, MediaUpload } = wp.blockEditor;
const { TextControl, PanelBody, SelectControl } = wp.components;
import { Sections, Section } from '../../helper/Sections';
import Alignment from '../../helper/fields/Alignment';
import Color from '../../helper/fields/Color';
import Dimension from '../../helper/fields/Dimension';
import Media from '../../helper/fields/Media';
import RadioImage from '../../helper/fields/RadioImage';
import Toggle from '../../helper/fields/Toggle';
import Typography from '../../helper/fields/Typography';
import { CssGenerator } from '../../helper/CssGenerator';
import {
	ImageStyle,
	ButtonStyle,
	GeneralAdvanced,
	CustomCssAdvanced,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../helper/CommonPanel';
import useBlockId from '../../helper/hooks/use-block-id';

const { useState, useEffect } = wp.element;

export default function Edit( props ) {
	const [ device, setDevice ] = useState( 'lg' );
	const [ section, setSection ] = useState( 'Content' );

	const { setAttributes, name, attributes, clientId, className } = props;

	const {
		blockId,
		advanceId,
		headingText,
		imageUpload,
		imgLink,
		imgAlt,
		imgOverlay,
		imgOverlayType,
		imgAnimation,
		headingColor,
		headingTypo,
		headingEnable,
		headingMargin,
		linkTarget,
		alignment,
		btnLink,
		btnText,
		btnTarget,
		btnPosition,
		linkType,
	} = attributes;

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/image', blockId );
	}

	const store = {
		setAttributes,
		name,
		attributes,
		setSection,
		section,
		clientId,
	};

	return (
		<>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Setting', 'product-blocks' ) }
					>
						<PanelBody
							title={ __( 'Image Upload', 'product-blocks' ) }
						>
							<Media
								label={ __( 'Upload Image', 'product-blocks' ) }
								multiple={ false }
								type={ [ 'image' ] }
								panel={ true }
								value={ imageUpload }
								onChange={ ( val ) =>
									setAttributes( { imageUpload: val } )
								}
							/>
							<TextControl
								label={ __(
									'Image Alt Text',
									'product-blocks'
								) }
								value={ imgAlt }
								onChange={ ( val ) =>
									setAttributes( { imgAlt: val } )
								}
							/>
							<RadioImage
								label={ __( 'Link Type', 'product-blocks' ) }
								isText={ true }
								options={ [
									{
										label: __( 'Link', 'product-blocks' ),
										value: 'link',
									},
									{
										label: __( 'Button', 'product-blocks' ),
										value: 'button',
									},
								] }
								value={ linkType }
								onChange={ ( val ) =>
									setAttributes( { linkType: val } )
								}
							/>

							{ linkType == 'link' && (
								<>
									<TextControl
										label={ __( 'Link', 'product-blocks' ) }
										value={ imgLink }
										onChange={ ( val ) =>
											setAttributes( { imgLink: val } )
										}
									/>
									<SelectControl
										label={ __( 'Link Target' ) }
										value={ linkTarget || '' }
										options={ [
											{
												label: __(
													'Self',
													'product-blocks'
												),
												value: '_self',
											},
											{
												label: __(
													'Blank',
													'product-blocks'
												),
												value: '_blank',
											},
										] }
										onChange={ ( val ) =>
											setAttributes( { linkTarget: val } )
										}
									/>
								</>
							) }
							{ linkType == 'button' && (
								<>
									<TextControl
										label={ __(
											'Button Text',
											'product-blocks'
										) }
										value={ btnText }
										onChange={ ( val ) =>
											setAttributes( { btnText: val } )
										}
									/>
									<TextControl
										label={ __(
											'Button Link',
											'product-blocks'
										) }
										value={ btnLink }
										onChange={ ( val ) =>
											setAttributes( { btnLink: val } )
										}
									/>
									<SelectControl
										label={ __(
											'Button Link Target',
											'product-blocks'
										) }
										value={ btnTarget || '' }
										options={ [
											{
												label: __(
													'Self',
													'product-blocks'
												),
												value: '_self',
											},
											{
												label: __(
													'Blank',
													'product-blocks'
												),
												value: '_blank',
											},
										] }
										onChange={ ( val ) =>
											setAttributes( { btnTarget: val } )
										}
									/>
									<SelectControl
										label={ __(
											'Button Position',
											'product-blocks'
										) }
										value={ btnPosition || '' }
										options={ [
											{
												label: __(
													'Left Top',
													'product-blocks'
												),
												value: 'leftTop',
											},
											{
												label: __(
													'Right Top',
													'product-blocks'
												),
												value: 'rightTop',
											},
											{
												label: __(
													'Center Center',
													'product-blocks'
												),
												value: 'centerCenter',
											},
											{
												label: __(
													'Bottom Left',
													'product-blocks'
												),
												value: 'bottomLeft',
											},
											{
												label: __(
													'Bottom Right',
													'product-blocks'
												),
												value: 'bottomRight',
											},
										] }
										onChange={ ( val ) =>
											setAttributes( {
												btnPosition: val,
											} )
										}
									/>
								</>
							) }
							<Toggle
								label={ __(
									'Caption Enable',
									'product-blocks'
								) }
								value={ headingEnable }
								onChange={ ( val ) =>
									setAttributes( { headingEnable: val } )
								}
							/>
						</PanelBody>
						<ImageStyle
							store={ store }
							exclude={ [ 'imgCropSmall', 'imgCrop' ] }
							include={ [
								{
									position: 1,
									data: {
										type: 'alignment',
										key: 'imgAlignment',
										label: 'Image Align',
										responsive: true,
										disableJustify: true,
									},
								},
							] }
						/>
						{ headingEnable && (
							<PanelBody
								title={ __( 'Caption', 'product-blocks' ) }
							>
								<Alignment
									label={ __(
										'Alignment',
										'product-blocks'
									) }
									value={ alignment }
									onChange={ ( val ) =>
										setAttributes( { alignment: val } )
									}
									disableJustify
									responsive
									device={ device }
									setDevice={ ( value ) =>
										setDevice( value )
									}
								/>
								<Color
									label={ __(
										'Caption Color',
										'product-blocks'
									) }
									value={ headingColor }
									onChange={ ( val ) =>
										setAttributes( { headingColor: val } )
									}
								/>
								<Typography
									label={ __(
										'Caption Typography',
										'product-blocks'
									) }
									value={ headingTypo }
									onChange={ ( val ) =>
										setAttributes( { headingTypo: val } )
									}
									device={ device }
									setDevice={ ( value ) =>
										setDevice( value )
									}
								/>
								<Dimension
									label={ __(
										'Caption Margin',
										'product-blocks'
									) }
									value={ headingMargin }
									onChange={ ( val ) =>
										setAttributes( { headingMargin: val } )
									}
									min={ 0 }
									max={ 100 }
									unit={ [ 'px', 'em', '%' ] }
									responsive
									device={ device }
									setDevice={ ( value ) =>
										setDevice( value )
									}
									clientId={ clientId }
								/>
							</PanelBody>
						) }
						{ linkType == 'button' && (
							<ButtonStyle store={ store } />
						) }
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>
			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ `wopb-block-wrapper` }>
					<figure className={ `wopb-image-block-wrapper` }>
						<div
							className={ `wopb-image-block wopb-image-block-${ imgAnimation } ${
								imgOverlay === true
									? 'wopb-image-block-overlay wopb-image-block-' +
									  imgOverlayType
									: ''
							}` }
						>
							{ imageUpload.url &&
								( linkType == 'link' && imgLink ? (
									<a>
										<img
											className={ `wopb-image` }
											src={ imageUpload.url }
											alt={ imgAlt ? imgAlt : 'Image' }
										/>
									</a>
								) : (
									<img
										className={ `wopb-image` }
										src={ imageUpload.url }
										alt={ imgAlt ? imgAlt : 'Image' }
									/>
								) ) }
							{ linkType == 'button' && btnLink && (
								<div
									className={ `wopb-image-button wopb-image-button-${ btnPosition }` }
								>
									<a href={ btnLink }>{ btnText }</a>
								</div>
							) }
						</div>
						{ headingEnable && headingText && (
							<RichText
								key="editable"
								tagName={ 'figcaption' }
								className={ `wopb-image-caption` }
								keepPlaceholderOnFocus
								placeholder={ __(
									'Add Text…',
									'product-blocks'
								) }
								onChange={ ( value ) =>
									setAttributes( { headingText: value } )
								}
								value={ headingText }
							/>
						) }
					</figure>
				</div>
			</div>
		</>
	);
}
