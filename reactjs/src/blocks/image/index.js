const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';
registerBlockType( 'product-blocks/image', {
	title: __( 'Image', 'product-blocks' ),
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/image.svg' }
			alt={ __( 'Image', 'product-blocks' ) }
			style={ { width: '100%' } }
		/>
	),
	example: wopb_data.url + 'assets/img/blocks/image.svg',
	category: 'product-blocks',
	description: __(
		'Display images and make them visually appealing.',
		'product-blocks'
	),
	keywords: [
		__( 'image', 'product-blocks' ),
		__( 'media', 'product-blocks' ),
		__( 'gallery', 'product-blocks' ),
		__( 'img', 'product-blocks' ),
	],
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
