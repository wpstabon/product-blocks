/** @format */

import useStyleButtonToggle from '../../helper/hooks/useStyleButtonToggle';
const { __ } = wp.i18n;

export const Card = ( { post, attributes, postCount, setSection, index } ) => {
	const {
		showImage,
		layout,
		countShow,
		imgAnimation,
		categoryCountPosition,
		imgCrop,
		columnGridGap,
		rowGap,
		columns,
		hideEmptyImageCategory,
		fallbackImg,
	} = attributes;
	if ( hideEmptyImageCategory && ! post.image ) {
		postCount = postCount - 1;
		return null;
	}
	let blockItemClass = '';
	if ( rowGap.lg == 0 ) {
		blockItemClass += ' wopb-row-gap-' + rowGap.lg;
	}
	if ( columnGridGap.lg == 0 ) {
		blockItemClass += ' wopb-column-gap-' + columnGridGap.lg;
	}
	if ( index > columns.lg ) {
		blockItemClass += ' wopb-last-row-item';
		if ( postCount % columns.lg != 0 && index == postCount ) {
			blockItemClass += ' wopb-last-rest-item';
		}
	}
	if ( index % columns.lg != 0 && index != postCount ) {
		blockItemClass += ' wopb-center-column-item';
	}

	const { StyleButton, showStyleButton } = useStyleButtonToggle( ( e ) =>
		setSection( e, 'image' )
	);

	return (
		<div className={ `wopb-block-item` + blockItemClass }>
			<div
				className={
					`wopb-block-content-wrap wopb-category-wrap` +
					( layout ? ` wopb-product-cat-` + layout : '' )
				}
			>
				{ showImage && (
					<div
						className={ `wopb-block-image wopb-block-image-${ imgAnimation }` }
						onClick={ showStyleButton }
					>
						{ StyleButton }
						<a
							href="#"
							className={
								`wopb-product-cat-img` +
								( post.count &&
								countShow &&
								categoryCountPosition == 'imageTop'
									? ' imageTop'
									: '' )
							}
						>
							{ categoryCountPosition == 'imageTop' && (
								<CategoryCount
									post={ post }
									attributes={ attributes }
									setSection={ setSection }
								/>
							) }
							<img
								src={
									post.image
										? post.image[ imgCrop ]
										: fallbackImg.url
										? fallbackImg.url
										: wopb_data.url +
										  `assets/img/wopb_fallback.jpg`
								}
								alt={ post.name }
								className="wopb-component-simple"
							/>
						</a>
					</div>
				) }

				<CategoryContent
					post={ post }
					attributes={ attributes }
					setSection={ setSection }
				/>
			</div>
		</div>
	);
};

const CategoryCount = ( { post, attributes, setSection } ) => {
	const { countShow, categoryCountPosition, categoryrCountText } = attributes;

	if ( post.count && countShow ) {
		return (
			<span
				className="wopb-product-cat-count wopb-component-simple"
				onClick={ ( e ) => setSection( e, 'count' ) }
			>
				{ categoryCountPosition == 'withTitle' && <>(</> }
				{ post.count }
				{ categoryrCountText && <> { categoryrCountText }</> }
				{ categoryCountPosition == 'withTitle' && <>)</> }
			</span>
		);
	}
};

const CategoryContent = ( { post, attributes, setSection } ) => {
	const {
		titleShow,
		descShow,
		countShow,
		readMore,
		readMoreText,
		categoryCountPosition,
		titleTag,
		descLimit,
	} = attributes;

	const TitleTag = titleTag;
	const excerpt = () => {
		const shortDesc =
			post.desc.split( ' ' ).splice( 0, descLimit ).join( ' ' ) + '...';
		return (
			<div
				className="wopb-product-cat-desc wopb-component-simple"
				onClick={ ( e ) => setSection( e, 'desc' ) }
				dangerouslySetInnerHTML={ {
					__html: shortDesc,
				} }
			/>
		);
	};
	return (
		<>
			{ ( titleShow || countShow || descShow || readMore ) && (
				<div className={ `wopb-category-content-items` }>
					<div className="wopb-category-content-item">
						{ post.name && titleShow && (
							<TitleTag className="wopb-product-cat-title">
								<a
									href="#"
									className="wopb-component-simple"
									onClick={ ( e ) => {
										e.preventDefault();
										setSection( e, 'title' );
									} }
								>
									{ post.name }
								</a>
								{ categoryCountPosition == 'withTitle' && (
									<CategoryCount
										post={ post }
										attributes={ attributes }
										setSection={ setSection }
									/>
								) }
							</TitleTag>
						) }
						{ post.count &&
							countShow &&
							categoryCountPosition == 'afterTitle' && (
								<CategoryCount
									post={ post }
									attributes={ attributes }
									setSection={ setSection }
								/>
							) }
						{ post.desc && descShow && excerpt() }
						{ readMore && (
							<div
								className="wopb-product-readmore wopb-component-simple"
								onClick={ ( e ) =>
									setSection( e, 'read-more' )
								}
							>
								<a>
									{ readMoreText
										? readMoreText
										: __( 'Read More', 'product-blocks' ) }
								</a>
							</div>
						) }
					</div>
				</div>
			) }
		</>
	);
};
