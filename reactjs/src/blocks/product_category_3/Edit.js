/** @format */

const { __ } = wp.i18n;
const { InspectorControls, RichText } = wp.blockEditor;
const { Spinner, Placeholder } = wp.components;
import Slider from 'react-slick';
import { CssGenerator } from '../../helper/CssGenerator';
import {
	SliderSetting,
	ProBlockLock,
	NextArrow,
	PrevArrow,
} from '../../helper/CommonPanel';
import icons from '../../helper/icons';
import ToolBarElement from '../../helper/ToolBarElement';
import { Card } from './components';
import Settings, {
	AddSettingsToToolbar,
	defLayout,
	features,
} from './Settings';
import useBlockId from '../../helper/hooks/use-block-id';
import usePrevious from '../../helper/hooks/use-previous';
import useProducts from '../../helper/hooks/use-products';
import useFluentSettings from '../../helper/hooks/useFluentSettings';

export default function Edit( props ) {
	useBlockId( props, true );

	const { setAttributes, name, attributes, clientId, className, context } =
		props;
	const prevProps = usePrevious( props );
	const { postsList, loading, error } = useProducts(
		attributes,
		prevProps?.attributes,
		{
			isCategory: true,
		}
	);
	const { section, setSection, setCurrSettings, toolbarSettings } =
		useFluentSettings( { props, prevProps } );

	const {
		blockId,
		advanceId,
		headingTag,
		headingText,
		headingStyle,
		headingShow,
		headingAlign,
		headingURL,
		headingBtnText,
		subHeadingShow,
		subHeadingText,
		showArrows,
		showDots,
		slidesToShow,
		autoPlay,
		productView,
		arrowStyle,
		slideSpeed,
		previewId,
		columns,
		contentAlign,
	} = attributes;

	const store = {
		setAttributes,
		name,
		attributes,
		clientId,
		section,
		setSection,
		context,
	};

	if ( previewId ) {
		return (
			<img
				style={ { marginTop: '0px', width: '420px' } }
				src={ previewId }
			/>
		);
	}

	const settings = SliderSetting( {
		arrows: showArrows,
		dots: showDots,
		slidesToShow,
		autoplay: autoPlay,
		autoplaySpeed: parseInt( slideSpeed ),
		nextArrow: (
			<NextArrow
				arrowStyle={ arrowStyle }
				setSection={ ( e ) => {
					setCurrSettings( e, 'arrow' );
				} }
			/>
		),
		prevArrow: (
			<PrevArrow
				arrowStyle={ arrowStyle }
				setSection={ ( e ) => {
					setCurrSettings( e, 'arrow' );
				} }
			/>
		),
		appendDots: ( dots ) => {
			return (
				<div
					onClick={ ( e ) => {
						setCurrSettings( e, 'dot' );
					} }
				>
					<ul> { dots } </ul>
				</div>
			);
		},
	} );

	if ( blockId ) {
		CssGenerator(
			attributes,
			'product-blocks/product-category-3',
			blockId
		);
	}
	const HeadingTag = headingTag;
	const postCount = postsList.length;

	// if (!attributes.initPremade) {
	//     return <InitBlockPremade store={store} />;
	// }

	return (
		<>
			<InspectorControls>
				<Settings store={ store } />
			</InspectorControls>

			<ToolBarElement
				include={ [
					{
						type: 'alignment',
						key: 'contentAlign',
						label: __(
							'Category Content Alignment',
							'product-blocks'
						),
						value: contentAlign,
						disableJustify: true,
					},
					defLayout,
					{ type: 'template' },
					{ type: 'query', blockType: 'category' },
					{ type: 'grid_spacing' },
					{
						type: 'common',
						data: features( store ),
					},
				] }
				store={ store }
			/>

			<AddSettingsToToolbar
				store={ store }
				selected={ toolbarSettings }
			/>

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
				onClick={ ( e ) => setCurrSettings( e, 'general', 'none' ) }
			>
				<ProBlockLock
					img={
						wopb_data.url +
						'assets/img/blocks/product-category-3.svg'
					}
					name={ 'Product Category #3' }
					upgradeLink={
						'https://www.wpxpo.com/wowstore/pricing/?utm_source=db-wstore-editor&utm_medium=product_category_3-pro&utm_campaign=wstore-dashboard'
					}
					detailsLink={
						'https://www.wpxpo.com/wowstore/blocks/?utm_source=db-wstore-editor&utm_medium=product_category_3-demo&utm_campaign=wstore-dashboard' +
						'#demoid1606'
					}
				/>
				<div
					className={ `wopb-block-wrapper wopb-product-category-3-wrapper ${
						! wopb_data.active ? ' wopb-wrapper-pro' : ''
					}` }
				>
					{ headingShow && (
						<div className={ `wopb-heading-filter` }>
							<div className={ `wopb-heading-filter-in` }>
								{ headingShow && (
									<div
										className={ `wopb-heading-wrap wopb-heading-${ headingStyle } wopb-heading-${ headingAlign }` }
										onClick={ ( e ) =>
											setCurrSettings( e, 'heading' )
										}
									>
										{ headingURL ? (
											<HeadingTag
												className={ `wopb-heading-inner` }
											>
												<a>
													<RichText
														key="editable"
														tagName={ 'span' }
														placeholder={ __(
															'Add Text…',
															'product-blocks'
														) }
														onChange={ ( value ) =>
															setAttributes( {
																headingText:
																	value,
															} )
														}
														value={ headingText }
													/>
												</a>
											</HeadingTag>
										) : (
											<HeadingTag
												className={ `wopb-heading-inner` }
											>
												<RichText
													key="editable"
													tagName={ 'span' }
													placeholder={ __(
														'Add Text…',
														'product-blocks'
													) }
													onChange={ ( value ) =>
														setAttributes( {
															headingText: value,
														} )
													}
													value={ headingText }
												/>
											</HeadingTag>
										) }
										{ headingStyle == 'style11' &&
											headingURL && (
												<a
													className={ `wopb-heading-btn` }
												>
													{ headingBtnText }
													{ icons.rightArrowLg }
												</a>
											) }
										{ subHeadingShow && (
											<div
												className={ `wopb-sub-heading` }
											>
												<RichText
													key="editable"
													tagName={ 'div' }
													className={
														'wopb-sub-heading-inner'
													}
													placeholder={ __(
														'Add Text…',
														'product-blocks'
													) }
													onChange={ ( value ) =>
														setAttributes( {
															subHeadingText:
																value,
														} )
													}
													value={ subHeadingText }
												/>
											</div>
										) }
									</div>
								) }
							</div>
						</div>
					) }

					{ ! error ? (
						! loading ? (
							postsList.length > 0 ? (
								<>
									{ productView === 'slide' ? (
										<div
											className={ `wopb-product-blocks-slide wopb-product-blocks-slide1` }
										>
											<Slider { ...settings }>
												{ postsList.map(
													( post, index ) => {
														return (
															<Card
																attributes={
																	attributes
																}
																post={ post }
																postCount={
																	postCount
																}
																key={
																	post.term_id
																}
																setSection={
																	setCurrSettings
																}
																index={
																	index + 1
																}
															/>
														);
													}
												) }
											</Slider>
										</div>
									) : (
										<div
											className={ `wopb-block-items-wrap wopb-block-row wopb-block-column-${ columns.lg }` }
										>
											{ postsList.map(
												( post, index ) => {
													return (
														<Card
															attributes={
																attributes
															}
															post={ post }
															postCount={
																postCount
															}
															key={ post.term_id }
															setSection={
																setCurrSettings
															}
															index={ index + 1 }
														/>
													);
												}
											) }
										</div>
									) }
								</>
							) : (
								<Placeholder
									className={ `wopb-backend-block-loading` }
									label={ __(
										'No Categories found',
										'product-blocks'
									) }
								></Placeholder>
							)
						) : (
							<Placeholder
								className={ `wopb-backend-block-loading` }
								label={ __( 'Loading…', 'product-blocks' ) }
							>
								{ ' ' }
								<Spinner />
							</Placeholder>
						)
					) : (
						<Placeholder
							label={ __(
								'Category are not available',
								'product-blocks'
							) }
						>
							<div style={ { marginBottom: 15 } }>
								{ __(
									'Make sure Add Category.',
									'product-blocks'
								) }
							</div>
						</Placeholder>
					) }
				</div>
			</div>
		</>
	);
}
