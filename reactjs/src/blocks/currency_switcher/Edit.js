const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import {
	GeneralAdvanced,
	CustomCssAdvanced,
	CommonSettings,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Sections, Section } from '../../helper/Sections';
import useBlockId from '../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const { setAttributes, name, attributes, clientId, className } = props;

	const {
		blockId,
		advanceId,
		prefixText,
		currencySymbolPosition,
		showFlag,
		countryNameShow,
	} = attributes;

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/currency-switcher', blockId );
	}

	const store = {
		setAttributes,
		name,
		attributes,
		clientId,
	};

	return (
		<>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Settings', 'product-blocks' ) }
					>
						<>
							<div className={ `wopb-block-use-instruction` }>
								<a
									className={ `wopb-link` }
									href="https://wpxpo.com/docs/wowstore/add-ons/currency-switcher-addon/?utm_source=db-wstore-edito&utm_medium=currency-switcher-docs&utm_campaign=wstore-dashboard"
									target="_blank"
									rel="noreferrer"
								>
									<span className="dashicons dashicons-media-document" />
									<span className={ `wopb-title` }>
										{ __(
											'How to use currency switcher',
											'product-blocks'
										) }
									</span>
								</a>
							</div>
							<CommonSettings
								initialOpen={ true }
								title="inline"
								store={ store }
								include={ [
									{
										data: {
											type: 'toggle',
											key: 'countryNameShow',
											label: __(
												'Show Country',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'select',
											key: 'currencySymbolPosition',
											label: __(
												'Currency Symbol Position',
												'product-blocks'
											),
											options: [
												{
													value: 'leftDollar',
													label: __(
														'($) United States (US) dollar USD',
														'product-blocks'
													),
												},
												{
													value: 'rightDollar',
													label: __(
														'United States (US) dollar($) USD',
														'product-blocks'
													),
												},
											],
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'containerRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'containerMargin',
											label: __(
												'Margin',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'containerPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] }
							/>
							<CommonSettings
								initialOpen={ false }
								depend="showFlag"
								title={ __( 'Flag', 'product-blocks' ) }
								store={ store }
								include={ [
									{
										data: {
											type: 'range',
											key: 'flagSpace',
											label: __(
												'Space',
												'product-blocks'
											),
											min: 0,
											max: 100,
											step: 1,
										},
									},
									{
										data: {
											type: 'range',
											key: 'flagHeight',
											label: __(
												'Height',
												'product-blocks'
											),
											min: 0,
											max: 150,
											step: 1,
										},
									},
									{
										data: {
											type: 'range',
											key: 'flagWidth',
											label: __(
												'Width',
												'product-blocks'
											),
											min: 0,
											max: 150,
											step: 1,
										},
									},
									{
										data: {
											type: 'border',
											key: 'flagBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'flagRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] }
							/>
							<CommonSettings
								initialOpen={ false }
								title={ __( 'Arrow', 'product-blocks' ) }
								store={ store }
								include={ [
									{
										data: {
											type: 'range',
											key: 'arrowSize',
											label: __(
												'Arrow Size',
												'product-blocks'
											),
											min: 10,
											max: 40,
											step: 1,
										},
									},
								] }
							/>
							<CommonSettings
								initialOpen={ false }
								title={ __( 'FIELD', 'product-blocks' ) }
								store={ store }
								include={ [
									{
										data: {
											type: 'typography',
											key: 'fieldTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'fieldTextColor',
											label: __(
												'Text Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'fieldTextHoverColor',
											label: __(
												'Text Hover Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color2',
											key: 'fieldBg',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color2',
											key: 'fieldHoverBg',
											label: __(
												'Hover Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'fieldBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'fieldHoverBorder',
											label: __(
												'Hover Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color2',
											key: 'optionBg',
											label: __(
												'Item Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color2',
											key: 'optionHoverBg',
											label: __(
												'Item Hover Background',
												'product-blocks'
											),
										},
									},
								] }
							/>
							<CommonSettings
								initialOpen={ false }
								title={ __( 'Prefix', 'product-blocks' ) }
								store={ store }
								include={ [
									{
										data: {
											type: 'text',
											key: 'prefixText',
											label: __(
												'Prefix Text',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'prefixColor',
											label: __(
												'Text Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'prefixTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
								] }
							/>
						</>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced initialOpen={ true } store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>
			<div
				id={ advanceId ? advanceId : '' }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ 'wopb-block-wrapper ' }>
					<div className="wopb-currency-switcher-prefixText">
						{ prefixText }
					</div>
					<div className="wopb-currency-switcher-container">
						<div className="wopb-selected-currency-container">
							<div
								className="wopb-selected-currency"
								value="wopb-selected-currency"
							>
								{ showFlag && (
									<img
										src="https://raw.githubusercontent.com/wpxpo/wpxpo_profile/main/country_flags/usd.png"
										alt="flag"
									/>
								) }
								{ currencySymbolPosition == 'leftDollar'
									? '( $ )'
									: '' }
								{ countryNameShow &&
									'United States (US) dollar' }{ ' ' }
								{ currencySymbolPosition == 'rightDollar'
									? '( $ )'
									: '' }
								USD
							</div>
							<div className="wopb-currency-arrow"></div>
						</div>
						<div
							name="wopb_current_currency"
							className="wopb-set-default-currency"
							style={ { display: 'none' } }
						>
							<ul className="wopb-select-container">
								<li className="" value="TZS">
									{ showFlag && (
										<img
											src="https://raw.githubusercontent.com/wpxpo/wpxpo_profile/main/country_flags/tzs.png"
											alt="flag"
										/>
									) }
									{ currencySymbolPosition == 'leftDollar'
										? '(Sh)'
										: '' }
									{ countryNameShow && 'Tanzanian shilling' }{ ' ' }
									{ currencySymbolPosition == 'rightDollar'
										? '( Sh )'
										: '' }
									TZS
								</li>
								<li className="" value="AED">
									{ showFlag && (
										<img
											src="https://raw.githubusercontent.com/wpxpo/wpxpo_profile/main/country_flags/aed.png"
											alt="flag"
										/>
									) }
									{ currencySymbolPosition == 'leftDollar'
										? '(د.إ)'
										: '' }
									{ countryNameShow &&
										'United Arab Emirates dirham' }{ ' ' }
									{ currencySymbolPosition == 'rightDollar'
										? '(د.إ)'
										: '' }
									AED
								</li>
								<li className="" value="BDT">
									{ showFlag && (
										<img
											src="https://raw.githubusercontent.com/wpxpo/wpxpo_profile/main/country_flags/bdt.png"
											alt="flag"
										/>
									) }
									{ currencySymbolPosition == 'leftDollar'
										? '(৳)'
										: '' }
									{ countryNameShow && 'Bangladeshi taka' }{ ' ' }
									{ currencySymbolPosition == 'rightDollar'
										? '(৳)'
										: '' }
									BDT
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</>
	);
}
