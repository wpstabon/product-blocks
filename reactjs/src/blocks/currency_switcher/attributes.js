const attributes = {
	blockId: { type: 'string', default: '' },
	currentPostId: { type: 'string', default: '' },
	currencySymbolPosition: { type: 'string', default: 'leftDollar' },
	countryNameShow: { type: 'boolean', default: true },
	containerRadius: {
		type: 'object',
		default: { lg: { unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-selected-currency-container , {{WOPB}} .wopb-block-wrapper .wopb-set-default-currency { border-radius:{{containerRadius}}; }',
			},
		],
	},
	containerMargin: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-currency-switcher-container { margin:{{containerMargin}}; }',
			},
		],
	},
	containerPadding: {
		type: 'object',
		default: {
			lg: { top: 10, right: 20, bottom: 10, left: 10, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-selected-currency-container, {{WOPB}} .wopb-block-wrapper .wopb-select-container { padding:{{containerPadding}}; }',
			},
		],
	},
	showFlag: { type: 'boolean', default: true },
	flagSpace: {
		type: 'string',
		default: '12',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-currency-switcher-container img { margin-right: {{flagSpace}}px; }',
			},
		],
	},
	flagHeight: {
		type: 'string',
		default: '28',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-currency-switcher-container img { height: {{flagHeight}}px; }',
			},
		],
	},
	flagWidth: {
		type: 'string',
		default: '48',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-currency-switcher-container img { width:{{flagWidth}}px; }',
			},
		],
	},
	flagBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#A1A1A1',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-currency-switcher-container img ',
			},
		],
	},
	flagRadius: {
		type: 'object',
		default: { lg: { unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-currency-switcher-container img { border-radius:{{flagRadius}}; }',
			},
		],
	},
	arrowSize: {
		type: 'string',
		default: '12',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-currency-arrow , {{WOPB}} .wopb-block-wrapper .wopb-currency-arrow::before { height: {{arrowSize}}px; width:{{arrowSize}}px; }',
			},
		],
	},
	fieldTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: '',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-select-container li, {{WOPB}} .wopb-block-wrapper .wopb-selected-currency',
			},
		],
	},
	fieldTextColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-selected-currency-container, {{WOPB}} .wopb-block-wrapper .wopb-currency-switcher-container .wopb-set-default-currency , {{WOPB}} .wopb-block-wrapper .wopb-currency-switcher-container .wopb-currency-arrow { color:{{fieldTextColor}}; }',
			},
		],
	},
	fieldTextHoverColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-selected-currency-container:hover, {{WOPB}} .wopb-block-wrapper .wopb-currency-switcher-container .wopb-set-default-currency li:hover , {{WOPB}} .wopb-block-wrapper .wopb-currency-switcher-container:hover .wopb-currency-arrow { color:{{fieldTextHoverColor}}; }',
			},
		],
	},
	fieldBg: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#fff' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-selected-currency-container ',
			},
		],
	},
	fieldHoverBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-selected-currency-container:hover ',
			},
		],
	},
	fieldBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#A1A1A1',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-selected-currency-container , {{WOPB}} .wopb-block-wrapper .wopb-currency-switcher-container .wopb-set-default-currency .wopb-select-container',
			},
		],
	},
	fieldHoverBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#A1A1A1',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-selected-currency-container:hover',
			},
		],
	},
	optionBg: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#fff' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-currency-switcher-container .wopb-set-default-currency , {{WOPB}} .wopb-block-wrapper .wopb-currency-switcher-container .wopb-select-container ',
			},
		],
	},
	optionHoverBg: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#E0E0E0' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-currency-switcher-container .wopb-select-container li:hover ',
			},
		],
	},
	prefixText: { type: 'string', default: 'Currency Switcher' },
	prefixColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-currency-switcher-prefixText { color:{{prefixColor}}; }',
			},
		],
	},
	prefixTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: '500',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-currency-switcher-prefixText',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#f5f5f5' },
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#ff176b' },
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapMargin: {
		type: 'object',
		default: { lg: { top: '', bottom: '', unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: { type: 'string', default: '', style: [ { selector: '' } ] },
};
export default attributes;
