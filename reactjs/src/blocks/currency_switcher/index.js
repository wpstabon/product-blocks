const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';

registerBlockType( 'product-blocks/currency-switcher', {
	title: __( 'Currency Switcher', 'product-blocks' ),
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/currency_switcher.svg' }
			alt={ __( 'Image', 'product-blocks' ) }
			style={ { width: '100%' } }
		/>
	),
	category: 'product-blocks',
	description: __(
		'Let shoppers convert the currency of your WooCommerce store.',
		'product-blocks'
	),
	keywords: [
		__( 'currency', 'product-blocks' ),
		__( 'switcher', 'product-blocks' ),
	],
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
