const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';

registerBlockType( 'product-blocks/filter', {
	title: __( 'Product Filtering', 'product-blocks' ),
	icon: (
		<div className={ `wopb-block-inserter-icon-section` }>
			<img
				src={ wopb_data.url + 'assets/img/blocks/filter.svg' }
				alt={ __( 'Image', 'product-blocks' ) }
				style={ { width: '100%' } }
			/>
			{ ! wopb_data.active && (
				<span className={ `wopb-pro-label` }>Pro</span>
			) }
		</div>
	),
	category: 'product-blocks',
	description: __(
		'Add filtering options to let shoppers find products effortlessly.',
		'product-blocks'
	),
	keywords: [
		__( 'price', 'product-blocks' ),
		__( 'title', 'product-blocks' ),
		__( 'section', 'product-blocks' ),
	],
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
