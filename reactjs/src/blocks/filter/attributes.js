const { __ } = wp.i18n;

const attributes = {
	blockId: { type: 'string', default: '' },
	currentPostId: { type: 'string', default: '' },
	initialBlockTargetSelect: { type: 'boolean', default: true },
	repeatableFilter: {
		type: 'array',
		fields: {
			type: { type: 'string', default: 'price' },
			label: { type: 'string', default: 'Filter By Price' },
		},
		default: [
			{ type: 'search', label: 'Filter By Search' },
			{ type: 'price', label: 'Filter By Price' },
			{ type: 'product_cat', label: 'Filter By Category' },
			{ type: 'status', label: 'Filter By Status' },
			{ type: 'rating', label: 'Filter By Rating' },
		],
	},
	sortingItems: {
		default: [
			{ label: __( 'Select Sort By', 'product-blocks' ), value: '' },
			{
				label: __( 'Default Sorting', 'product-blocks' ),
				value: 'default',
			},
			{
				label: __( 'Sort by popularity', 'product-blocks' ),
				value: 'popular',
			},
			{
				label: __( 'Sort by latest', 'product-blocks' ),
				value: 'latest',
			},
			{
				label: __( 'Sort by average rating', 'product-blocks' ),
				value: 'rating',
			},
			{
				label: __( 'Sort by price: low to high', 'product-blocks' ),
				value: 'price_low',
			},
			{
				label: __( 'Sort by price: high to low', 'product-blocks' ),
				value: 'price_high',
			},
		],
	},
	blockTarget: { type: 'string', default: '' },
	clearFilter: { type: 'boolean', default: true },
	filterHeading: { type: 'boolean', default: true },
	productCount: { type: 'boolean', default: true },
	expandTaxonomy: { type: 'boolean', default: false },
	enableTaxonomyRelation: { type: 'boolean', default: false },
	viewTaxonomyLimit: { type: 'string', default: 10 },

	// Filter Head Settings
	filterHeadText: {
		type: 'string',
		default: 'Filter',
	},
	filterHeadColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-title-section .wopb-filter-title, {{WOPB}} .wopb-filter-title-section .wopb-filter-icon { color:{{filterHeadColor}}; }',
			},
		],
	},
	filterHeadTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '18', unit: 'px' },
			height: { lg: '24', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '600',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-title-section .wopb-filter-title',
			},
		],
	},
	filterHeadSpace: {
		type: 'object',
		default: { lg: '20', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-title-section { margin-bottom: {{filterHeadSpace}}; }',
			},
		],
	},

	// Filter Clear Settings
	clearColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-active-item { color:{{clearColor}}; }',
			},
		],
	},
	clearBg: {
		type: 'string',
		default: '#E6E6E6',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-active-item { background-color:{{clearBg}}; } {{WOPB}} .wopb-filter-remove-section { border-color:{{clearBg}}; }',
			},
		],
	},
	clearAllColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-remove-all { color:{{clearAllColor}}; }',
			},
		],
	},
	clearAllBg: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-remove-all { background-color:{{clearAllBg}}; }',
			},
		],
	},
	clearTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '12', unit: 'px' },
			height: { lg: '16', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-active-item, {{WOPB}} .wopb-filter-remove-all',
			},
		],
	},
	clearBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#E6E6E6',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-active-item, {{WOPB}} .wopb-filter-remove-all',
			},
		],
	},
	clearRadius: {
		type: 'object',
		default: {
			lg: {
				top: '16',
				bottom: '16',
				left: '16',
				right: '16',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-active-item, {{WOPB}} .wopb-filter-remove-all { border-radius:{{clearRadius}}; }',
			},
		],
	},
	clearPadding: {
		type: 'object',
		default: {
			lg: { top: '8', bottom: '8', left: '12', right: '12', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-active-item, {{WOPB}} .wopb-filter-remove-all { padding:{{clearPadding}}; }',
			},
		],
	},

	// label Settings
	labelColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-block .wopb-filter-header .wopb-filter-label, {{WOPB}} .wopb-relation-heading { color:{{labelColor}}; }',
			},
		],
	},
	labelTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '24', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '600',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-block .wopb-filter-header .wopb-filter-label, {{WOPB}} .wopb-relation-heading',
			},
		],
	},
	labelBtmSpace: {
		type: 'object',
		default: { lg: '16', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-block .wopb-filter-header, {{WOPB}} .wopb-relation-heading { margin-bottom: {{labelBtmSpace}}; }',
			},
		],
	},
	filterItemColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-body label { color:{{filterItemColor}}; }',
			},
		],
	},
	filterItemTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-filter-body label',
			},
		],
	},
	togglePlusMinus: { type: 'boolean', default: true },
	togglePlusMinusInitialOpen: {
		type: 'boolean',
		default: true,
		style: [
			{
				depends: [
					{ key: 'togglePlusMinus', condition: '==', value: true },
				],
			},
		],
	},

	toggleInitialMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				depends: [
					{ key: 'togglePlusMinus', condition: '==', value: true },
					{
						key: 'togglePlusMinusInitialOpen',
						condition: '==',
						value: true,
					},
				],
			},
		],
	},

	togglePlusMinusSize: {
		type: 'object',
		default: { lg: '17', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'togglePlusMinus', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-filter-block .wopb-filter-header .wopb-filter-toggle .dashicons { font-size: {{togglePlusMinusSize}}; }',
			},
		],
	},
	togglePlusMinusColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				depends: [
					{ key: 'togglePlusMinus', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-filter-block .wopb-filter-header .wopb-filter-toggle .dashicons { color:{{togglePlusMinusColor}}; }',
			},
		],
	},

	// Input Fields
	inputFieldBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#6E6E6E',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-body .wopb-filter-price-input, {{WOPB}} .wopb-filter-body .wopb-filter-sorting-input, {{WOPB}} .wopb-filter-body input[type="checkbox"]',
			},
		],
	},
	inputFieldPadding: {
		type: 'object',
		default: {
			lg: { top: '8', bottom: '8', left: '12', right: '12', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-body .wopb-filter-sorting-input { padding: {{inputFieldPadding}}; }',
			},
		],
	},
	inputFieldRadius: {
		type: 'object',
		default: {
			lg: { top: '2', bottom: '2', left: '2', right: '2', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-body .wopb-filter-price-input, {{WOPB}} .wopb-filter-body .wopb-filter-sorting-input, {{WOPB}} .wopb-filter-body input[type="checkbox"] { border-radius:{{inputFieldRadius}}; } ',
			},
		],
	},
	inputRangeFill: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-price-range-bar { background-color: {{inputRangeFill}}; } {{WOPB}} .wopb-price-range-input { color: {{inputRangeFill}}; }',
			},
		],
	},
	inputRangeEmp: {
		type: 'string',
		default: '#6E6E6E',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-price-range { background-color : {{inputRangeEmp}}; }',
			},
		],
	},
	inputCheckClr: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-body input[type="checkbox"]:checked::before { color : {{inputCheckClr}}; }',
			},
		],
	},
	inputCheckBg: {
		type: 'string',
		default: '#000',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-body input[type="checkbox"]:checked { background : {{inputCheckBg}}; }',
			},
		],
	},
	inputNumColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-body .wopb-filter-price-input { color : {{inputNumColor}}; }',
			},
		],
	},
	inputNumBg: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-body .wopb-filter-price-input { background-color : {{inputNumBg}}; } ',
			},
		],
	},
	inputSelectColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-body .wopb-filter-sorting-input  { color : {{inputSelectColor}}; }',
			},
		],
	},
	inputSelectBg: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-body .wopb-filter-sorting-input  { background-color : {{inputSelectBg}}; }',
			},
		],
	},

	searchBoxColor: {
		type: 'string',
		default: '#3b3b3b',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-block .wopb-search-filter-body input { color:{{searchBoxColor}}; }',
			},
		],
	},
	searchBoxBackgroundColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-block .wopb-search-filter-body input { background:{{searchBoxBackgroundColor}}; }',
			},
		],
	},
	searchBoxBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#9c9c9c',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-block .wopb-search-filter-body input',
			},
		],
	},
	searchBoxRadius: {
		type: 'object',
		default: {
			lg: { top: 40, right: 40, bottom: 40, left: 40, unit: 'px' },
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-block .wopb-search-filter-body input { border-radius:{{searchBoxRadius}}; }',
			},
		],
	},
	searchBoxPadding: {
		type: 'object',
		default: {
			lg: {
				top: '13',
				bottom: '13',
				left: '14',
				right: '14',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-filter-block .wopb-search-filter-body input { padding:{{searchBoxPadding}}; }',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#f5f5f5' },
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#ff176b' },
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapMargin: {
		type: 'object',
		default: { lg: { top: '', bottom: '', unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: { type: 'string', default: '', style: [ { selector: '' } ] },
};
export default attributes;
