const { memo } = wp.element;
const { __ } = wp.i18n;

export const FilterTitleContent = memo( function FilterTitleContent( {
	filterHeadText,
} ) {
	return (
		<div className={ `wopb-filter-title-section` }>
			<span className={ `wopb-filter-title` }>{ filterHeadText }</span>
			<span
				className={ `dashicons dashicons-filter wopb-filter-icon` }
			></span>
		</div>
	);
} );

export const FilterHeaderContent = memo( function FilterHeaderContent( {
	headerLabel,
	togglePlusMinus,
} ) {
	return (
		<div className={ `wopb-filter-header` }>
			<span className={ `wopb-filter-label` }>{ headerLabel }</span>
			{ togglePlusMinus && (
				<div className={ `wopb-filter-toggle` }>
					<span
						className={ `dashicons dashicons-plus-alt2 wopb-filter-plus` }
					></span>
					<span
						className={ `dashicons dashicons-minus wopb-filter-minus` }
					></span>
				</div>
			) }
		</div>
	);
} );

export const StatusContent = ( { statusList, productCount } ) => {
	return (
		<div className={ `wopb-filter-check-list` }>
			{ statusList.map( ( status, id ) => {
				return (
					<div key={ id } className="wopb-filter-item">
						<div className={ `wopb-item-content` }>
							<label htmlFor={ `status_` + id }>
								<input
									type={ `checkbox` }
									className={ `wopb-status-input` }
									id={ `status_` + id }
									value={ status.value }
								/>
								{ status.label }{ ' ' }
								{ productCount ? '(' + status.count + ')' : '' }
							</label>
						</div>
					</div>
				);
			} ) }
		</div>
	);
};

export const StarContent = memo( function StarContent() {
	return (
		<div className={ `wopb-filter-check-list wopb-filter-ratings` }>
			{ [ ...Array( 6 ).keys() ].map( ( key, index ) => {
				const filledStartCount = 5 - index;
				if ( filledStartCount > 0 ) {
					return (
						<div key={ key } className="wopb-filter-item">
							<div className={ `wopb-item-content` }>
								<label
									htmlFor={
										`filter-rating-` + filledStartCount
									}
								>
									<input
										type={ `checkbox` }
										className={ `wopb-rating-input` }
										value={ filledStartCount }
										id={
											`filter-rating-` + filledStartCount
										}
									/>
									{ [ ...Array( filledStartCount ) ].map(
										( childKey, childIndex ) => {
											return (
												<span
													key={ childIndex }
													className={ `dashicons dashicons-star-filled` }
												></span>
											);
										}
									) }
									{ [ ...Array( index ) ].map(
										( childKey, childIndex ) => {
											return (
												<span
													key={ childIndex }
													className={ `dashicons dashicons-star-empty` }
												></span>
											);
										}
									) }
								</label>
							</div>
						</div>
					);
				}
			} ) }
		</div>
	);
} );

export const Search = memo( function Search( { label, togglePlusMinus } ) {
	return (
		<div className={ `wopb-filter-section` }>
			<FilterHeaderContent
				headerLabel={ label }
				togglePlusMinus={ togglePlusMinus }
			/>
			<div className={ `wopb-filter-body` }>
				<div className={ `wopb-search-filter-body` }>
					<input
						type={ `hidden` }
						className={ `wopb-filter-slug` }
						value={ `search` }
					/>
					<input
						type={ `text` }
						className={ `wopb-filter-search-input` }
						placeholder={ `Search Products...` }
					/>
					<span className={ `wopb-search-icon` }>
						<img
							src={ wopb_data.url + 'assets/img/svg/search.svg' }
							alt={ __( 'Image', 'product-blocks' ) }
						/>
					</span>
				</div>
			</div>
		</div>
	);
} );

export const Price = memo( function Price( { label, togglePlusMinus } ) {
	return (
		<div className={ `wopb-filter-section` }>
			<FilterHeaderContent
				headerLabel={ label }
				togglePlusMinus={ togglePlusMinus }
			/>
			<div className={ `wopb-filter-body wopb-price-range-slider` }>
				<input
					type={ `hidden` }
					className={ `wopb-filter-slug` }
					value={ `price` }
				/>
				<div className={ `wopb-price-range` }>
					<span className={ `wopb-price-range-bar` } />
					<input
						min={ 0 }
						step={ 1 }
						value={ 0 }
						max={ 10000 }
						type={ `range` }
						className={ `wopb-price-range-input wopb-min-range` }
						readOnly
					/>
					<input
						min={ 0 }
						step={ 1 }
						max={ 10000 }
						value={ 10000 }
						type={ `range` }
						className={ `wopb-price-range-input wopb-max-range` }
						readOnly
					/>
				</div>
				<span className="wopb-input-group">
					<input
						type="number"
						className="wopb-filter-price-input wopb-filter-price-min"
						value="0"
						min="0"
					/>
					<input
						type="number"
						className="wopb-filter-price-input wopb-filter-price-max"
						value="10000"
						min="0"
						max="10000"
					/>
				</span>
			</div>
		</div>
	);
} );

export const Status = memo( function Status( {
	label,
	statusList,
	productCount,
	togglePlusMinus,
} ) {
	return (
		<div className={ `wopb-filter-section` }>
			<FilterHeaderContent
				headerLabel={ label }
				togglePlusMinus={ togglePlusMinus }
			/>
			<div className={ `wopb-filter-body` }>
				<input
					type={ `hidden` }
					className={ `wopb-filter-slug` }
					value={ `status` }
				/>
				<StatusContent
					statusList={ statusList }
					productCount={ productCount }
				/>
			</div>
		</div>
	);
} );

export const Rating = ( { label, togglePlusMinus } ) => {
	return (
		<div className={ `wopb-filter-section` }>
			<FilterHeaderContent
				headerLabel={ label }
				togglePlusMinus={ togglePlusMinus }
			/>
			<div className={ `wopb-filter-body` }>
				<input
					type={ `hidden` }
					className={ `wopb-filter-slug` }
					value={ `rating` }
				/>
				<StarContent />
			</div>
		</div>
	);
};

export const SortBy = ( { label, sortingItems, togglePlusMinus } ) => {
	return (
		<div className={ `wopb-filter-section` }>
			<FilterHeaderContent
				headerLabel={ label }
				togglePlusMinus={ togglePlusMinus }
			/>
			<div className={ `wopb-filter-body` }>
				<input
					type={ `hidden` }
					className={ `wopb-filter-slug` }
					value={ `sorting` }
				/>
				<select
					name={ `sortBy` }
					className={ `select wopb-filter-sorting-input` }
				>
					{ sortingItems.map( ( item, id ) => {
						return (
							<option key={ id } value={ item.value }>
								{ item.label }
							</option>
						);
					} ) }
				</select>
			</div>
		</div>
	);
};

export const Default = ( { productTaxonomyList, type, label, attributes } ) => {
	return productTaxonomyList.map( ( item, index ) => {
		if ( item.name == type ) {
			const terms = item.terms;
			const taxLimit =
				attributes.viewTaxonomyLimit > 3
					? 3
					: attributes.viewTaxonomyLimit;
			return (
				<div key={ index } className={ `wopb-filter-section` }>
					<FilterHeaderContent
						headerLabel={ label }
						togglePlusMinus={ attributes.togglePlusMinus }
					/>
					<div className={ `wopb-filter-body` }>
						<input
							type={ `hidden` }
							className={ `wopb-filter-slug` }
							value={ `product_attribute` }
							data-taxonomy={ item.name }
						/>
						<div className={ `wopb-filter-check-list` }>
							{ Object.keys( terms )
								.slice( 0, taxLimit )
								.map( ( key ) => {
									const term = terms[ key ];
									const childTerms =
										term.child_terms != undefined &&
										Object.keys( term.child_terms ).length >
											0
											? term.child_terms
											: '';
									return (
										<div
											key={ key }
											className="wopb-filter-item"
										>
											<div
												className={ `wopb-item-content` }
											>
												<label
													htmlFor={
														`tax_term_` +
														term.term_id
													}
												>
													<input
														type={ `checkbox` }
														value={ term.term_id }
														id={
															`tax_term_` +
															term.term_id
														}
														className={ `wopb-filter-tax-term-input` }
													/>
													{ item.attribute !=
														undefined &&
														item.attribute
															.attribute_type ===
															'color' && (
															<span
																className={ `wopb-filter-tax-color` }
																style={ {
																	backgroundColor: `${ term.slug }`,
																} }
															></span>
														) }
													{ term.name }
													{ attributes.productCount
														? '(' + term.count + ')'
														: '' }
												</label>
												{ childTerms && (
													<div
														className={
															`wopb-filter-check-list` +
															( ! attributes.expandTaxonomy
																? ' wopb-d-none'
																: '' )
														}
													>
														{ Object.keys(
															childTerms
														).map( ( childKey ) => {
															const term =
																childTerms[
																	childKey
																];
															return (
																<div
																	key={
																		childKey
																	}
																	className={ `wopb-item-content wopb-filter-extended-item` }
																>
																	<label
																		htmlFor={
																			`tax_term_` +
																			term.term_id
																		}
																	>
																		<input
																			type={ `checkbox` }
																			value={
																				term.term_id
																			}
																			id={
																				`tax_term_` +
																				term.term_id
																			}
																			className={ `wopb-filter-tax-term-input` }
																		/>
																		{
																			term.name
																		}
																		{ attributes.productCount
																			? '(' +
																			  term.count +
																			  ')'
																			: '' }
																	</label>
																</div>
															);
														} ) }
													</div>
												) }
											</div>

											{ childTerms && (
												<div
													className={ `wopb-filter-child-toggle` }
												>
													<span
														className={
															`dashicons dashicons-arrow-right-alt2 wopb-filter-right-toggle` +
															( attributes.expandTaxonomy
																? ' wopb-d-none'
																: '' )
														}
													></span>
													<span
														className={
															`dashicons dashicons-arrow-down-alt2 wopb-filter-down-toggle` +
															( ! attributes.expandTaxonomy
																? ' wopb-d-none'
																: '' )
														}
													></span>
												</div>
											) }
										</div>
									);
								} ) }
						</div>
					</div>
				</div>
			);
		}
	} );
};
