const { __ } = wp.i18n;
const { addQueryArgs } = wp.url;
const { InspectorControls } = wp.blockEditor;
const { Modal } = wp.components;
import { CssGenerator } from '../../helper/CssGenerator';
import LinkGenerator from '../../helper/LinkGenerator';
import TemplateModal from '../../helper/TemplateModal';
import {
	GeneralAdvanced,
	CustomCssAdvanced,
	ResponsiveAdvanced,
	CommonSettings,
	wopbSupport,
	ProBlockLock,
} from '../../helper/CommonPanel';
import { Sections, Section } from '../../helper/Sections';

import {
	Default,
	FilterTitleContent,
	Price,
	Rating,
	Search,
	SortBy,
	Status,
} from './components';
import useBlockId from '../../helper/hooks/use-block-id';

const { useState, useEffect } = wp.element;

export default function Edit( props ) {
	const [ statusList, setStatusList ] = useState( [] );
	const [ openFilterUsesModal, setOpenFilterUsesModal ] = useState( false );

	const { setAttributes, name, attributes, clientId, className } = props;

	const {
		blockId,
		initialBlockTargetSelect,
		repeatableFilter,
		advanceId,
		clearFilter,
		filterHeading,
		togglePlusMinus,
		togglePlusMinusInitialOpen,
		toggleInitialMobile,
		sortingItems,
		filterHeadText,
	} = attributes;

	function fetchProducts() {
		const attrData = { wpnonce: wopb_data.security, action_type: 'status' };
		const query = addQueryArgs( '/wopb/common', attrData );

		wp.apiFetch( { path: query } )
			.then( ( obj ) => {
				setStatusList( obj );
			} )
			.catch( ( error ) => {
				console.log( error );
			} )
			.finally( () => {} );
	}

	useEffect( () => {
		fetchProducts();
	}, [] );

	useBlockId( props, true );

	function setTargetBlock( value, store ) {
		if ( value !== '' ) {
			store.setAttributes( {
				initialBlockTargetSelect: true,
				blockTarget: value,
			} );
		}
	}

	function filterUsesModalCLose( target ) {
		if ( target.closest( '.components-button' ) != null ) {
			setOpenFilterUsesModal( false );
		}
	}

	const store = {
		setAttributes,
		name,
		attributes,
		clientId,
	};

	const productTaxonomyList = wopb_data.productTaxonomyList;
	const mobilePreview = document.querySelector( '.is-mobile-preview' );
	const filterSelectionList = [
		{ value: 'search', label: __( 'Search', 'product-blocks' ) },
		{ value: 'price', label: __( 'Pricing', 'product-blocks' ) },
		{ value: 'status', label: __( 'Status', 'product-blocks' ) },
		{ value: 'rating', label: __( 'Rating', 'product-blocks' ) },
		{ value: 'sort_by', label: __( 'Sort By', 'product-blocks' ) },
	];
	productTaxonomyList.map( ( item, index ) => {
		const filterValue = item.name;
		const filterLabel = item.label;
		filterSelectionList.push( {
			value: filterValue,
			label: __( filterLabel, 'product-blocks' ),
		} );
	} );

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/filter', blockId );
	}

	let wrapperClass = '',
		wrapperClassMobile = '';
	wrapperClass += ! wopb_data.active ? 'wopb-wrapper-pro' : '';
	if ( togglePlusMinus ) {
		if (
			! togglePlusMinusInitialOpen ||
			( mobilePreview && toggleInitialMobile )
		) {
			wrapperClass += ' wopb-filter-toggle-initial-close ';
		} else if ( togglePlusMinusInitialOpen ) {
			wrapperClass += ' wopb-filter-toggle-initial-open ';
		}
	}
	const targetGridList = [
		{ value: '', label: __( 'Select any grid', 'product-blocks' ) },
		{
			value: 'product-grid-1',
			label: __( 'Product Grid 1', 'product-blocks' ),
		},
		{
			value: 'product-grid-2',
			label: __( 'Product Grid 2', 'product-blocks' ),
		},
		{
			value: 'product-grid-3',
			label: __( 'Product Grid 3', 'product-blocks' ),
		},
		{
			value: 'product-list-1',
			label: __( 'Product List 1', 'product-blocks' ),
		},
	];

	function renderContent( element, id ) {
		switch ( element.type ) {
			case 'search':
				return (
					<Search
						label={ element.label }
						key={ id }
						togglePlusMinus={ togglePlusMinus }
					/>
				);

			case 'price':
				return (
					<Price
						label={ element.label }
						key={ id }
						togglePlusMinus={ togglePlusMinus }
					/>
				);
			case 'status':
				return (
					<Status
						label={ element.label }
						statusList={ statusList }
						productCount={ attributes.productCount }
						key={ id }
						togglePlusMinus={ togglePlusMinus }
					/>
				);
			case 'rating':
				return (
					<Rating
						label={ element.label }
						key={ id }
						togglePlusMinus={ togglePlusMinus }
					/>
				);
			case 'sort_by':
				return (
					<SortBy
						label={ element.label }
						sortingItems={ sortingItems }
						key={ id }
						togglePlusMinus={ togglePlusMinus }
					/>
				);
			default:
				return (
					<Default
						productTaxonomyList={ productTaxonomyList }
						attributes={ attributes }
						label={ element.label }
						type={ element.type }
						key={ id }
					/>
				);
		}
	}

	return (
		<>
			<InspectorControls>
				<TemplateModal
					prev={
						LinkGenerator(
							'https://www.wpxpo.com/wowstore/blocks/?',
							'blockPreview'
						) + '#demoid34658'
					}
					store={ store }
				/>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Settings', 'product-blocks' ) }
					>
						{ initialBlockTargetSelect && (
							<>
								<div className={ `wopb-block-use-instruction` }>
									{ openFilterUsesModal && (
										<Modal
											className="wopb-filter-uses-modal"
											title={ __(
												'How to use filter',
												'product-blocks'
											) }
											onRequestClose={ ( e ) =>
												filterUsesModalCLose( e.target )
											}
										>
											<iframe
												width="650"
												height="350"
												src="https://www.youtube.com/embed/-qo7JEYhf_s"
												title={ __(
													'How to add Product Filter to WooCommerce Shop Page',
													'product-blocks'
												) }
												allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
												allowFullScreen
											/>
										</Modal>
									) }
									<a
										className={ `wopb-link` }
										onClick={ () =>
											setOpenFilterUsesModal( true )
										}
									>
										<span className="dashicons dashicons-video-alt3" />
										<span className={ `wopb-title` }>
											{ __(
												'How to use filter',
												'product-blocks'
											) }
										</span>
									</a>
								</div>
								<CommonSettings
									initialOpen={ true }
									title={ __( 'General', 'product-blocks' ) }
									store={ store }
									include={ [
										{
											position: 1,
											data: {
												type: 'select',
												key: 'blockTarget',
												label: __(
													'Grid(Select grid where will filter implement)',
													'product-blocks'
												),
												options: targetGridList,
											},
										},
										{
											position: 2,
											data: {
												type: 'repeatable',
												key: 'repeatableFilter',
												label: __(
													'Filter Data',
													'product-blocks'
												),
												fields: [
													{
														type: 'select',
														key: 'type',
														label: __(
															'Filter Type',
															'product-blocks'
														),
														options:
															filterSelectionList,
													},
													{
														type: 'text',
														key: 'label',
														label: __(
															'Label',
															'product-blocks'
														),
													},
												],
											},
										},
										{
											position: 3,
											data: {
												type: 'toggle',
												key: 'clearFilter',
												label: __(
													'Clear Filter',
													'product-blocks'
												),
											},
										},
										{
											position: 4,
											data: {
												type: 'toggle',
												key: 'filterHeading',
												label: __(
													'Filter Heading',
													'product-blocks'
												),
											},
										},
										{
											position: 5,
											data: {
												type: 'toggle',
												key: 'productCount',
												label: __(
													'Product Count',
													'product-blocks'
												),
											},
										},
										{
											position: 6,
											data: {
												type: 'toggle',
												key: 'expandTaxonomy',
												label: __(
													'Expand Taxonomy',
													'product-blocks'
												),
											},
										},
										{
											position: 7,
											data: {
												type: 'toggle',
												key: 'enableTaxonomyRelation',
												label: __(
													'Taxonomy Relation',
													'product-blocks'
												),
											},
										},
										{
											position: 8,
											data: {
												type: 'number',
												key: 'viewTaxonomyLimit',
												min: 2,
												label: __(
													'Taxonomy Limit',
													'product-blocks'
												),
											},
										},
									] }
								/>
								{ filterHeading && (
									<CommonSettings
										initialOpen={ false }
										title={ __(
											'Filter Heading',
											'product-blocks'
										) }
										store={ store }
										include={ [
											{
												position: 1,
												data: {
													type: 'text',
													key: 'filterHeadText',
													label: __(
														'Heading text',
														'product-blocks'
													),
												},
											},
											{
												position: 2,
												data: {
													type: 'color',
													key: 'filterHeadColor',
													label: __(
														'Color',
														'product-blocks'
													),
												},
											},
											{
												position: 3,
												data: {
													type: 'typography',
													key: 'filterHeadTypo',
													label: __(
														'Typography',
														'product-blocks'
													),
												},
											},
											{
												position: 4,
												data: {
													type: 'range',
													key: 'filterHeadSpace',
													min: 0,
													max: 100,
													step: 1,
													responsive: true,
													unit: [ 'px' ],
													label: __(
														'Bottom Space',
														'product-blocks'
													),
												},
											},
										] }
									/>
								) }
								{ clearFilter && (
									<CommonSettings
										initialOpen={ false }
										title={ __(
											'Clear Filter',
											'product-blocks'
										) }
										store={ store }
										include={ [
											{
												position: 1,
												data: {
													type: 'color',
													key: 'clearColor',
													label: __(
														'Color',
														'product-blocks'
													),
												},
											},
											{
												position: 2,
												data: {
													type: 'color',
													key: 'clearBg',
													label: __(
														'Background',
														'product-blocks'
													),
												},
											},
											{
												position: 3,
												data: {
													type: 'color',
													key: 'clearAllColor',
													label: __(
														'Clear All Color',
														'product-blocks'
													),
												},
											},
											{
												position: 5,
												data: {
													type: 'color',
													key: 'clearAllBg',
													label: __(
														'Clear All Background',
														'product-blocks'
													),
												},
											},
											{
												position: 7,
												data: {
													type: 'typography',
													key: 'clearTypo',
													label: __(
														'Typography',
														'product-blocks'
													),
												},
											},
											{
												position: 9,
												data: {
													type: 'border',
													key: 'clearBorder',
													label: __(
														'Input Border',
														'product-blocks'
													),
												},
											},
											{
												position: 11,
												data: {
													type: 'dimension',
													key: 'clearRadius',
													label: __(
														'Border Radius',
														'product-blocks'
													),
													step: 1,
													unit: true,
													responsive: true,
												},
											},
											{
												position: 13,
												data: {
													type: 'dimension',
													key: 'clearPadding',
													label: __(
														'Padding',
														'product-blocks'
													),
													step: 1,
													unit: true,
													responsive: true,
												},
											},
										] }
									/>
								) }
								<CommonSettings
									initialOpen={ false }
									title={ __(
										'Filtering Label & Content',
										'product-blocks'
									) }
									store={ store }
									include={ [
										{
											position: 1,
											data: {
												type: 'color',
												key: 'labelColor',
												label: __(
													'Color',
													'product-blocks'
												),
											},
										},
										{
											position: 3,
											data: {
												type: 'typography',
												key: 'labelTypo',
												label: __(
													'Label Typography',
													'product-blocks'
												),
											},
										},
										{
											position: 5,
											data: {
												type: 'range',
												key: 'labelBtmSpace',
												min: 0,
												max: 100,
												step: 1,
												responsive: true,
												unit: [ 'px' ],
												label: __(
													'Label Bottom Space',
													'product-blocks'
												),
											},
										},
										{
											position: 1,
											data: {
												type: 'color',
												key: 'filterItemColor',
												label: __(
													'Filter Item Color',
													'product-blocks'
												),
											},
										},
										{
											position: 3,
											data: {
												type: 'typography',
												key: 'filterItemTypo',
												label: __(
													'Filter Item Typography',
													'product-blocks'
												),
											},
										},
									] }
								/>
								<CommonSettings
									initialOpen={ false }
									title={ __(
										'Toggle Plus / Minus',
										'product-blocks'
									) }
									store={ store }
									include={ [
										{
											position: 1,
											data: {
												type: 'toggle',
												key: 'togglePlusMinus',
												label: __(
													'Enable Toggle',
													'product-blocks'
												),
											},
										},
										{
											position: 2,
											data: {
												type: 'toggle',
												key: 'togglePlusMinusInitialOpen',
												label: __(
													'Initially Open',
													'product-blocks'
												),
											},
										},
										{
											position: 3,
											data: {
												type: 'toggle',
												key: 'toggleInitialMobile',
												label: __(
													'Initially Close in Mobile',
													'product-blocks'
												),
											},
										},
										{
											position: 4,
											data: {
												type: 'range',
												key: 'togglePlusMinusSize',
												min: 0,
												max: 80,
												step: 1,
												responsive: true,
												unit: [ 'px', 'em' ],
												label: __(
													'Toggle Size',
													'product-blocks'
												),
											},
										},
										{
											position: 5,
											data: {
												type: 'color',
												key: 'togglePlusMinusColor',
												label: __(
													'Toggle Color',
													'product-blocks'
												),
											},
										},
									] }
								/>
								<CommonSettings
									initialOpen={ false }
									title={ __(
										'Input Fields',
										'product-blocks'
									) }
									store={ store }
									include={ [
										{
											position: 3,
											data: {
												type: 'border',
												key: 'inputFieldBorder',
												label: __(
													'Input Border',
													'product-blocks'
												),
											},
										},
										{
											position: 13,
											data: {
												type: 'dimension',
												key: 'inputFieldPadding',
												label: __(
													'Input Padding',
													'product-blocks'
												),
												step: 1,
												unit: true,
												responsive: true,
											},
										},
										{
											position: 13,
											data: {
												type: 'dimension',
												key: 'inputFieldRadius',
												label: __(
													'Input Radius',
													'product-blocks'
												),
												step: 1,
												unit: true,
												responsive: true,
											},
										},
										{
											position: 3,
											data: {
												type: 'tab',
												content: [
													{
														name: 'range',
														title: __(
															'Range',
															'product-blocks'
														),
														options: [
															{
																type: 'color',
																key: 'inputRangeFill',
																label: __(
																	'Filled Color',
																	'product-blocks'
																),
															},
															{
																type: 'color',
																key: 'inputRangeEmp',
																label: __(
																	'Empty Color',
																	'product-blocks'
																),
															},
														],
													},
													{
														name: 'checkbox',
														title: __(
															'Checkbox',
															'product-blocks'
														),
														options: [
															{
																type: 'color',
																key: 'inputCheckClr',
																label: __(
																	'Mark Color(checked)',
																	'product-blocks'
																),
															},
															{
																type: 'color',
																key: 'inputCheckBg',
																label: __(
																	'Background(checked)',
																	'product-blocks'
																),
															},
														],
													},
													{
														name: 'number',
														title: __(
															'Number',
															'product-blocks'
														),
														options: [
															{
																type: 'color',
																key: 'inputNumColor',
																label: __(
																	'Color',
																	'product-blocks'
																),
															},
															{
																type: 'color',
																key: 'inputNumBg',
																label: __(
																	'Background',
																	'product-blocks'
																),
															},
														],
													},
													{
														name: 'select',
														title: __(
															'Select',
															'product-blocks'
														),
														options: [
															{
																type: 'color',
																key: 'inputSelectColor',
																label: __(
																	'Color',
																	'product-blocks'
																),
															},
															{
																type: 'color',
																key: 'inputSelectBg',
																label: __(
																	'Background',
																	'product-blocks'
																),
															},
														],
													},
												],
											},
										},
									] }
								/>
								{ repeatableFilter.some(
									( x ) => x.type === 'search'
								) && (
									<CommonSettings
										initialOpen={ false }
										title={ __(
											'Search Style',
											'product-blocks'
										) }
										store={ store }
										include={ [
											{
												position: 1,
												data: {
													type: 'color',
													key: 'searchBoxColor',
													label: __(
														'Color',
														'product-blocks'
													),
												},
											},
											{
												position: 2,
												data: {
													type: 'color',
													key: 'searchBoxBackgroundColor',
													label: __(
														'Background Color',
														'product-blocks'
													),
												},
											},
											{
												position: 3,
												data: {
													type: 'border',
													key: 'searchBoxBorder',
													label: __(
														'Input Border',
														'product-blocks'
													),
												},
											},
											{
												position: 4,
												data: {
													type: 'dimension',
													key: 'searchBoxRadius',
													label: __(
														'Border Radius',
														'product-blocks'
													),
													step: 1,
													unit: true,
													responsive: true,
												},
											},
											{
												position: 4,
												data: {
													type: 'dimension',
													key: 'searchBoxPadding',
													label: __(
														'Padding',
														'product-blocks'
													),
													step: 1,
													unit: true,
													responsive: true,
												},
											},
										] }
									/>
								) }
							</>
						) }
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced initialOpen={ true } store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<ProBlockLock
					img={ wopb_data.url + 'assets/img/blocks/filter.svg' }
					name={ 'Product Filtering' }
					upgradeLink={
						'https://www.wpxpo.com/wowstore/pricing/?utm_source=db-wstore-editor&utm_medium=product-filter-pro&utm_campaign=wstore-dashboard'
					}
					detailsLink={
						'https://www.wpxpo.com/wowstore/woocommerce-product-filter/?utm_source=db-wstore-editor&utm_medium=product-filter-demo&utm_campaign=wstore-dashboard'
					}
				/>
				<div
					className={
						`wopb-product-wrapper wopb-filter-block ` + wrapperClass
					}
				>
					{ ! initialBlockTargetSelect && (
						<div className={ `wopb-block-select-picker` }>
							<FilterTitleContent />
							<div>
								<span>
									{ __(
										'Grid(Select grid where will filter implement)',
										'product-blocks'
									) }
								</span>
							</div>
							<select
								name={ `initialBlockTargetSelect` }
								className={ `select w-100` }
								onChange={ ( event ) =>
									setTargetBlock( event.target.value, store )
								}
							>
								<option value="">
									{ __( 'Select Grid', 'product-blocks' ) }
								</option>
								{ targetGridList.map( ( item, index ) => {
									return (
										<option
											key={ index }
											value={ item.value }
										>
											{ item.label }
										</option>
									);
								} ) }
							</select>
						</div>
					) }

					{ initialBlockTargetSelect && (
						<>
							{ filterHeading && (
								<FilterTitleContent
									filterHeadText={ filterHeadText }
								/>
							) }
							{ clearFilter && (
								<div className={ `wopb-filter-remove-section` }>
									<div className="wopb-active-items">
										<span
											className={ `wopb-filter-active-item` }
										>
											{ __( 'Min', 'product-blocks' ) }:
											91{ ' ' }
											<span
												className={ `dashicons dashicons-no-alt wopb-filter-remove-icon` }
											></span>
										</span>
										<span
											className={ `wopb-filter-active-item` }
										>
											{ __( 'Green', 'product-blocks' ) }{ ' ' }
											<span
												className={ `dashicons dashicons-no-alt wopb-filter-remove-icon` }
											></span>
										</span>
										<span
											className={ `wopb-filter-active-item` }
										>
											{ __( 'XS', 'product-blocks' ) }{ ' ' }
											<span
												className={ `dashicons dashicons-no-alt wopb-filter-remove-icon` }
											></span>
										</span>
									</div>
									<span
										className={ `wopb-filter-remove-all` }
									>
										{ __( 'Clear All', 'product-blocks' ) }{ ' ' }
										<span
											className={ `dashicons dashicons-no-alt wopb-filter-remove-icon` }
										></span>
									</span>
								</div>
							) }
							{ repeatableFilter.map( ( element, id ) => {
								return renderContent( element, id );
							} ) }
							<div className="wopb-filter-section">
								<div className="wopb-filter-body">
									<input
										type="hidden"
										className="wopb-filter-slug wopb-filter-slug-reset wopb-d-none"
										value="reset"
									/>
								</div>
							</div>
						</>
					) }
				</div>
			</div>
		</>
	);
}
