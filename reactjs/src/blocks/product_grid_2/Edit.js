/** @format */

const { __ } = wp.i18n;
const { InspectorControls, RichText } = wp.blockEditor;
const { Spinner, Placeholder } = wp.components;
import Slider from 'react-slick';
import { CssGenerator } from '../../helper/CssGenerator';
import {
	renderCategory,
	SliderSetting,
	renderVariationSwitcher,
	NextArrow,
	PrevArrow,
	LoadMore,
	Pagination,
	Filter,
} from '../../helper/CommonPanel';
import icons from '../../helper/icons';
import ToolBarElement from '../../helper/ToolBarElement';

import { Card } from './components';
import Settings, { AddSettingsToToolbar, features } from './Settings';
import useBlockId from '../../helper/hooks/use-block-id';
import usePrevious from '../../helper/hooks/use-previous';
import useFluentSettings from '../../helper/hooks/useFluentSettings';
import useProducts from '../../helper/hooks/use-products';

export default function Edit( props ) {
	useBlockId( props, true );

	const { setAttributes, name, attributes, clientId, className, context } =
		props;
	const prevProps = usePrevious( props );
	const { postsList, loading, error } = useProducts(
		attributes,
		prevProps?.attributes
	);
	const { section, setSection, setCurrSettings, toolbarSettings } =
		useFluentSettings( { props, prevProps } );

	const {
		blockId,
		advanceId,
		paginationShow,
		paginationAjax,
		headingTag,
		headingText,
		headingStyle,
		headingShow,
		headingAlign,
		headingURL,
		filterShow,
		filterType,
		filterCat,
		filterTag,
		paginationType,
		titleTag,
		headingBtnText,
		paginationNav,
		subHeadingShow,
		subHeadingText,
		showArrows,
		showDots,
		slidesToShow,
		autoPlay,
		productView,
		arrowStyle,
		slideSpeed,
		filterText,
		filterAction,
		filterActionText,
		loadMoreText,
		paginationText,
		catShow,
		catPosition,
		showVariationSwitch,
		columns,
		previewId,
		contentAlign,
	} = attributes;

	const store = {
		setAttributes,
		name,
		attributes,
		setSection,
		section,
		clientId,
		context,
	};

	const settings = SliderSetting( {
		arrows: showArrows,
		dots: showDots,
		slidesToShow,
		autoplay: autoPlay,
		autoplaySpeed: parseInt( slideSpeed ),
		nextArrow: (
			<NextArrow
				arrowStyle={ arrowStyle }
				setSection={ ( e ) => {
					setCurrSettings( e, 'arrow' );
				} }
			/>
		),
		prevArrow: (
			<PrevArrow
				arrowStyle={ arrowStyle }
				setSection={ ( e ) => {
					setCurrSettings( e, 'arrow' );
				} }
			/>
		),
		appendDots: ( dots ) => {
			return (
				<div
					onClick={ ( e ) => {
						setCurrSettings( e, 'dot' );
					} }
				>
					<ul> { dots } </ul>
				</div>
			);
		},
	} );

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/product-grid-2', blockId );
	}

	const HeadingTag = headingTag;
	const TitleTag = titleTag;

	if ( previewId ) {
		return (
			<img
				style={ { marginTop: '0px', width: '420px' } }
				src={ previewId }
			/>
		);
	}

	// if (!attributes.initPremade) {
	//     return <InitBlockPremade store={store} />;
	// }

	return (
		<>
			<InspectorControls>
				<Settings store={ store } />
			</InspectorControls>

			<ToolBarElement
				include={ [
					{
						type: 'alignment',
						key: 'contentAlign',
						label: __( 'Grid Content Alignment', 'product-blocks' ),
						value: contentAlign,
						disableJustify: true,
					},
					{ type: 'template' },
					{ type: 'query' },
					{ type: 'grid_spacing' },
					{
						type: 'common',
						data: features( store ),
					},
				] }
				store={ store }
			/>
			<AddSettingsToToolbar
				store={ store }
				selected={ toolbarSettings }
			/>

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
				onClick={ ( e ) => setCurrSettings( e, 'general', 'none' ) }
			>
				<div className={ `wopb-block-wrapper` }>
					{ ( headingShow || filterShow || paginationShow ) && (
						<div className={ `wopb-heading-filter` }>
							<div className={ `wopb-heading-filter-in` }>
								{ headingShow && (
									<div
										className={ `wopb-heading-wrap wopb-heading-${ headingStyle } wopb-heading-${ headingAlign }` }
										onClick={ ( e ) => {
											setCurrSettings( e, 'heading' );
										} }
									>
										{ headingURL ? (
											<HeadingTag
												className={ `wopb-heading-inner` }
											>
												<a>
													<RichText
														key="editable"
														tagName={ 'span' }
														placeholder={ __(
															'Add Text…',
															'product-blocks'
														) }
														onChange={ ( value ) =>
															setAttributes( {
																headingText:
																	value,
															} )
														}
														value={ headingText }
													/>
												</a>
											</HeadingTag>
										) : (
											<HeadingTag
												className={ `wopb-heading-inner` }
											>
												<RichText
													key="editable"
													tagName={ 'span' }
													placeholder={ __(
														'Add Text…',
														'product-blocks'
													) }
													onChange={ ( value ) =>
														setAttributes( {
															headingText: value,
														} )
													}
													value={ headingText }
												/>
											</HeadingTag>
										) }
										{ headingStyle == 'style11' &&
											headingURL && (
												<a
													className={ `wopb-heading-btn` }
												>
													{ headingBtnText }
													{ icons.rightArrowLg }
												</a>
											) }
										{ subHeadingShow && (
											<div
												className={ `wopb-sub-heading` }
											>
												<RichText
													key="editable"
													tagName={ 'div' }
													className={
														'wopb-sub-heading-inner'
													}
													placeholder={ __(
														'Add Text…',
														'product-blocks'
													) }
													onChange={ ( value ) =>
														setAttributes( {
															subHeadingText:
																value,
														} )
													}
													value={ subHeadingText }
												/>
											</div>
										) }
									</div>
								) }
								{ ( filterShow || paginationShow ) &&
									productView == 'grid' && (
										<div
											className={ `wopb-filter-navigation` }
										>
											{ filterShow && (
												<Filter
													filterText={ filterText }
													filterType={ filterType }
													filterCat={ filterCat }
													filterTag={ filterTag }
													filterAction={
														filterAction
													}
													filterActionText={
														filterActionText
													}
													onClick={ ( e ) =>
														setCurrSettings(
															e,
															'filter'
														)
													}
												/>
											) }
										</div>
									) }
							</div>
						</div>
					) }

					<div className={ `wopb-wrapper-main-content` }>
						{ ! error ? (
							! loading ? (
								postsList.length > 0 ? (
									<>
										{ productView === 'slide' ? (
											<div
												className={ `wopb-product-blocks-slide wopb-product-blocks-slide1` }
											>
												<Slider { ...settings }>
													{ postsList.map(
														( post, idx ) => {
															const category =
																renderCategory(
																	post,
																	catShow,
																	catPosition,
																	( e ) =>
																		setCurrSettings(
																			e,
																			'category'
																		)
																);
															const variationSwitcherData =
																showVariationSwitch
																	? renderVariationSwitcher(
																			post,
																			wopb_data.isVariationSwitchActive,
																			showVariationSwitch,
																			(
																				e
																			) =>
																				setCurrSettings(
																					e,
																					'swatch'
																				)
																	  )
																	: '';

															return (
																<Card
																	TitleTag={
																		TitleTag
																	}
																	attributes={
																		attributes
																	}
																	category={
																		category
																	}
																	variationSwitcherData={
																		variationSwitcherData
																	}
																	key={
																		post.post_id
																	}
																	post={
																		post
																	}
																	setSection={
																		setCurrSettings
																	}
																/>
															);
														}
													) }
												</Slider>
											</div>
										) : (
											<div
												className={ `wopb-block-items-wrap wopb-block-row wopb-block-column-${ columns.lg }` }
											>
												{ postsList.map(
													( post, idx ) => {
														const category =
															renderCategory(
																post,
																catShow,
																catPosition,
																( e ) =>
																	setCurrSettings(
																		e,
																		'category'
																	)
															);
														const variationSwitcherData =
															showVariationSwitch
																? renderVariationSwitcher(
																		post,
																		wopb_data.isVariationSwitchActive,
																		showVariationSwitch,
																		( e ) =>
																			setCurrSettings(
																				e,
																				'swatch'
																			)
																  )
																: '';

														return (
															<Card
																TitleTag={
																	TitleTag
																}
																attributes={
																	attributes
																}
																category={
																	category
																}
																variationSwitcherData={
																	variationSwitcherData
																}
																key={ idx }
																post={ post }
																setSection={
																	setCurrSettings
																}
															/>
														);
													}
												) }
											</div>
										) }
									</>
								) : (
									<Placeholder
										className={ `wopb-backend-block-loading` }
										label={ __(
											'No Products found',
											'product-blocks'
										) }
									></Placeholder>
								)
							) : (
								<Placeholder
									className={ `wopb-backend-block-loading` }
									label={ __( 'Loading…', 'product-blocks' ) }
								>
									<Spinner />
								</Placeholder>
							)
						) : (
							<Placeholder
								label={ __(
									'Products are not available',
									'product-blocks'
								) }
							>
								<div style={ { marginBottom: 15 } }>
									{ __(
										'Make sure Add Product.',
										'product-blocks'
									) }
								</div>
							</Placeholder>
						) }
						{ paginationShow &&
							productView == 'grid' &&
							paginationType == 'pagination' && (
								<Pagination
									paginationNav={ paginationNav }
									paginationAjax={ paginationAjax }
									paginationText={ paginationText }
									onClick={ ( e ) => {
										setCurrSettings( e, 'pagination' );
									} }
								/>
							) }
						{ paginationShow &&
							productView == 'grid' &&
							paginationType == 'loadMore' && (
								<LoadMore
									loadMoreText={ loadMoreText }
									onClick={ ( e ) => {
										setCurrSettings( e, 'pagination' );
									} }
								/>
							) }
					</div>
				</div>
			</div>
		</>
	);
}
