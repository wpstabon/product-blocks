const { __ } = wp.i18n;
import {
	ArrowStyle,
	ArrowStyleArg,
	CartStyle,
	CartStyleArg,
	CategoryStyle,
	CategoryStyleArg,
	CommonSettings,
	CustomCssAdvanced,
	DealStyle,
	DotStyle,
	DotStyleArg,
	FilterStyle,
	FilterStyleArg,
	GeneralAdvanced,
	GeneralSettingsWithQuery,
	HeadingSettings,
	HeadingSettingsArg,
	HotStyle,
	HotStyleArg,
	ImageStyle,
	ImageStyleArg,
	MetaElementStyle,
	OverlayMetaElementStyle,
	PaginationStyle,
	PaginationStyleArg,
	PriceStyle,
	PriceStyleArg,
	ResponsiveAdvanced,
	ReviewStyle,
	ReviewStyleArg,
	SalesStyle,
	SalesStyleArg,
	ShortDescStyle,
	ShortDescStyleArg,
	TitleStyle,
	TitleStyleArg,
	TypographyTB,
	colorIcon,
	filterFields,
	filterSettings,
	filterSpacing,
	settingsIcon,
	spacingIcon,
	styleIcon,
	titleColor,
	typoIcon,
	wopbSupport,
} from '../../helper/CommonPanel';
import LinkGenerator from '../../helper/LinkGenerator';
import { Section, Sections } from '../../helper/Sections';
import TemplateModal from '../../helper/TemplateModal';
import WopbToolbarDropdown from '../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar } from '../../helper/ux';

export const features = ( store ) => {
	return [
		{
			type: 'group',
			key: 'productView',
			label: __( 'Product View', 'product-blocks' ),
			justify: true,
			options: [
				{ value: 'grid', label: __( 'Grid View', 'product-blocks' ) },
				{ value: 'slide', label: __( 'Slide View', 'product-blocks' ) },
			],
		},

		{
			type: 'toggle',
			key: 'headingShow',
			label: __( 'Heading', 'product-blocks' ),
		},

		{
			type: store.attributes.productView === 'slide' ? 'toggle' : '',
			key: 'showArrows',
			label: __( 'Arrows', 'product-blocks' ),
		},
		{
			type: store.attributes.productView === 'slide' ? 'toggle' : '',
			key: 'showDots',
			label: __( 'Dots', 'product-blocks' ),
		},

		{
			type: 'toggle',
			key: 'titleShow',
			label: __( 'Title', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'showImage',
			label: __( 'Image', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'showSale',
			label: __( 'Sale', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'showHot',
			label: __( 'Hot', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'showInStock',
			label: __( 'In Stock', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'showOutStock',
			label: __( 'Out of Stock', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'showDeal',
			label: __( 'Deal', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'catShow',
			label: __( 'Category', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'showPrice',
			label: __( 'Price', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'showReview',
			label: __( 'Review', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'showCart',
			label: __( 'Cart', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'showShortDesc',
			label: __( 'Short Description', 'product-blocks' ),
		},
		{
			type: store.attributes.productView === 'grid' ? 'toggle' : '',
			key: 'filterShow',
			label: __( 'Filter', 'product-blocks' ),
		},
		{
			type: store.attributes.productView === 'grid' ? 'toggle' : '',
			key: 'paginationShow',
			label: __( 'Pagination', 'product-blocks' ),
		},

		{
			type: wopb_data.isVariationSwitchActive === 'true' ? 'toggle' : '',
			key: 'showVariationSwitch',
			label: __( 'Variation Swatch', 'product-blocks' ),
		},
	];
};

export default function Settings( { store } ) {
	const { section } = store;
	const { productView } = store.attributes;

	return (
		<>
			<TemplateModal
				prev={
					LinkGenerator(
						'https://www.wpxpo.com/wowstore/blocks/?',
						'blockPreview'
					) + '#demoid425'
				}
				store={ store }
			/>
			<Sections>
				<Section
					slug="setting"
					title={ __( 'Settings', 'product-blocks' ) }
				>
					<GeneralSettingsWithQuery
						store={ store }
						exclude={ [
							'sortSection',
							'quickView',
							'showCompare',
						] }
						initialOpen={ section.general }
					/>
					<OverlayMetaElementStyle
						store={ store }
						initialOpen={ section.overlay_meta }
					/>
					{ productView == 'slide' && (
						<ArrowStyle
							depend="showArrows"
							store={ store }
							initialOpen={ section.arrow }
						/>
					) }

					{ productView == 'slide' && (
						<DotStyle
							depend="showDots"
							store={ store }
							initialOpen={ section.dot }
						/>
					) }

					<PriceStyle
						depend="showPrice"
						store={ store }
						initialOpen={ section.price }
					/>

					<ReviewStyle
						depend="showReview"
						store={ store }
						initialOpen={ section.review }
					/>

					<ShortDescStyle
						depend="showShortDesc"
						exclude={ [ 'showFullShortDesc' ] }
						store={ store }
						initialOpen={ section.desc }
					/>

					<SalesStyle
						depend="showSale"
						store={ store }
						initialOpen={ section.sales }
					/>
					<HotStyle
						depend="showHot"
						store={ store }
						initialOpen={ section.hots }
					/>
					<DealStyle
						depend="showDeal"
						store={ store }
						initialOpen={ section.deal }
					/>
					<CartStyle
						depend="showCart"
						store={ store }
						initialOpen={ section.cart }
					/>
					<ImageStyle
						depend="showImage"
						exclude={ [
							'imgOverlay',
							'imgOverlayType',
							'overlayColor',
							'imgOpacity',
						] }
						initialOpen={ section.image }
						store={ store }
						include={ [
							{
								position: 4,
								data: {
									type: 'tag',
									key: 'imageScale',
									label: __(
										'Image Scale',
										'product-blocks'
									),
									options: [
										{
											value: '',
											label: __(
												'None',
												'product-blocks'
											),
										},
										{
											value: 'cover',
											label: __(
												'Cover',
												'product-blocks'
											),
										},
										{
											value: 'contain',
											label: __(
												'Contain',
												'product-blocks'
											),
										},
										{
											value: 'fill',
											label: __(
												'Fill',
												'product-blocks'
											),
										},
										{
											value: 'scale-down',
											label: __(
												'Scale Down',
												'product-blocks'
											),
										},
									],
								},
							},
						] }
					/>
					<HeadingSettings
						depend="headingShow"
						store={ store }
						initialOpen={ section.heading }
					/>
					<TitleStyle
						depend="titleShow"
						store={ store }
						initialOpen={ section.title }
					/>
					<CategoryStyle
						depend="catShow"
						exclude={ [ 'catPosition' ] }
						store={ store }
						initialOpen={ section.category }
						include={ [
							{
								position: 1,
								data: {
									type: 'select',
									key: 'catPosition',
									label: __( 'Position', 'product-blocks' ),
									options: [
										{
											value: 'none',
											label: __(
												'After Title',
												'product-blocks'
											),
										},
										{
											value: 'beforeTitle',
											label: __(
												'Before Title',
												'product-blocks'
											),
										},
										{
											value: 'topLeft',
											label: __(
												'Over Image(Top Left)',
												'product-blocks'
											),
										},
										{
											value: 'topRight',
											label: __(
												'Over Image(Top Right)',
												'product-blocks'
											),
										},
										{
											value: 'bottomLeft',
											label: __(
												'Over Image(Bottom Left)',
												'product-blocks'
											),
										},
										{
											value: 'bottomRight',
											label: __(
												'Over Image(Bottom Right)',
												'product-blocks'
											),
										},
										{
											value: 'centerCenter',
											label: __(
												'Over Image(Center)',
												'product-blocks'
											),
										},
									],
								},
							},
						] }
					/>
					{ productView == 'grid' && (
						<FilterStyle
							depend="filterShow"
							store={ store }
							initialOpen={ section.filter }
						/>
					) }
					{ productView == 'grid' && (
						<PaginationStyle
							depend="paginationShow"
							store={ store }
							initialOpen={ section.meta }
						/>
					) }
					{ wopb_data.isVariationSwitchActive === 'true' && (
						<CommonSettings
							initialOpen={ section.swatch }
							depend="showVariationSwitch"
							title={ __(
								'Variation Swatches',
								'product-blocks'
							) }
							store={ store }
							youtube="https://www.youtube.com/watch?v=i65S6QiFgFE"
							include={ [
								{
									position: 1,
									data: {
										type: 'select',
										key: 'variationSwitchPosition',
										label: __(
											'Position',
											'product-blocks'
										),
										options: [
											{
												value: 'before_title',
												label: __(
													'Before Title',
													'product-blocks'
												),
											},
											{
												value: 'after_title',
												label: __(
													'After Title',
													'product-blocks'
												),
											},
											{
												value: 'before_price',
												label: __(
													'Before Price',
													'product-blocks'
												),
											},
											{
												value: 'after_price',
												label: __(
													'After Price',
													'product-blocks'
												),
											},
										],
									},
								},
								{
									position: 2,
									data: {
										type: 'range',
										key: 'variationSwitchWidth',
										min: 0,
										max: 100,
										step: 1,
										responsive: true,
										unit: [ 'px', 'em', '%' ],
										label: __( 'Width', 'product-blocks' ),
									},
								},
								{
									position: 3,
									data: {
										type: 'range',
										key: 'variationSwitchHeight',
										min: 0,
										max: 100,
										step: 1,
										responsive: true,
										unit: [ 'px', 'em' ],
										label: __( 'Height', 'product-blocks' ),
									},
								},
								{
									position: 4,
									data: {
										type: 'color',
										key: 'variationSwitchLabelBg',
										label: __(
											'Label Background Color',
											'product-blocks'
										),
									},
								},
								{
									position: 5,
									data: {
										type: 'color',
										key: 'variationSwitchLabelColor',
										label: __(
											'Label Color',
											'product-blocks'
										),
									},
								},
								{
									position: 6,
									data: {
										type: 'typography',
										key: 'variationSwitchLabelSize',
										label: __(
											'Label Typography',
											'product-blocks'
										),
									},
								},
								{
									position: 7,
									data: {
										type: 'dimension',
										key: 'variationSwitchLabelPadding',
										label: __(
											'Label Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
					) }
				</Section>
				<Section
					slug="advanced"
					title={ __( 'Advanced', 'product-blocks' ) }
				>
					<GeneralAdvanced initialOpen={ true } store={ store } />
					<ResponsiveAdvanced pro={ true } store={ store } />
					<CustomCssAdvanced store={ store } />
				</Section>
			</Sections>
			{ wopbSupport() }
		</>
	);
}

export function AddSettingsToToolbar( { selected, store } ) {
	switch ( selected ) {
		case 'filter':
			return (
				<WopbToolbarGroup text={ 'Filter' }>
					<TypographyTB
						store={ store }
						attrKey={ 'fliterTypo' }
						label={ __( 'Filter Typography', 'product-blocks' ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Filter Spacing', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'filter',
								title: __( 'Filter Spacing', 'product-blocks' ),
								options: filterFields(
									filterSpacing,
									'__all',
									FilterStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Filter Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'filter',
								title: __( 'Filter Style', 'product-blocks' ),
								options: filterFields(
									null,
									[
										...filterSettings,
										'fliterTypo',
										...filterSpacing,
									],
									FilterStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Filter Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'filter',
								title: __(
									'Filter Settings',
									'product-blocks'
								),
								options: filterFields(
									filterSettings,
									'__all',
									FilterStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'title':
			return (
				<WopbToolbarGroup text={ 'Title' }>
					<TypographyTB
						store={ store }
						attrKey={ 'titleTypo' }
						label={ __( 'Title Typography', 'product-blocks' ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Title Spacing', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Title Spacing', 'product-blocks' ),
								options: filterFields(
									[ 'titlePadding' ],
									'__all',
									TitleStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ colorIcon }
						store={ store }
						label={ __( 'Title Color', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Title Color', 'product-blocks' ),
								options: filterFields(
									titleColor,
									'__all',
									TitleStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Title Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Title Settings', 'product-blocks' ),
								options: filterFields(
									null,
									[
										...titleColor,
										'titleTypo',
										'titlePadding',
									],
									TitleStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'desc':
			return (
				<WopbToolbarGroup text={ 'Description' }>
					<TypographyTB
						store={ store }
						attrKey={ 'ShortDescTypo' }
						label={ __(
							'Description Typography',
							'product-blocks'
						) }
					/>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Description Spacing', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Description Spacing',
									'product-blocks'
								),
								options: filterFields(
									[ 'ShortDescPadding' ],
									'__all',
									ShortDescStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ colorIcon }
						store={ store }
						label={ __( 'Description Color', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Description Color',
									'product-blocks'
								),
								options: filterFields(
									[ 'ShortDescColor' ],
									'__all',
									ShortDescStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Description Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Description Settings',
									'product-blocks'
								),
								options: filterFields(
									null,
									[
										'ShortDescTypo',
										'ShortDescPadding',
										'ShortDescColor',
										'showFullShortDesc',
									],
									ShortDescStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'category':
			return (
				<WopbToolbarGroup text={ 'Category' }>
					<TypographyTB
						store={ store }
						attrKey={ 'catTypo' }
						label={ __( 'Category Typography', 'product-blocks' ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Category Spacing', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Category Spacing',
									'product-blocks'
								),
								options: filterFields(
									[
										'catSacing', // bruh
										'catPadding',
									],
									'__all',
									CategoryStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Category Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Category Style', 'product-blocks' ),
								options: filterFields(
									[ 'catTab', 'catRadius' ],
									'__all',
									CategoryStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Category Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Category Settings',
									'product-blocks'
								),
								options: filterFields(
									[ 'enableCatLink', 'catPosition' ],
									'__all',
									CategoryStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'cart':
			return (
				<WopbToolbarGroup text={ 'Cart' }>
					<TypographyTB
						store={ store }
						attrKey={ 'cartTypo' }
						label={ __( 'Cart Typography', 'product-blocks' ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Cart Spacing', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Cart Spacing', 'product-blocks' ),
								options: filterFields(
									[ 'cartSpacing', 'cartPadding' ],
									'__all',
									CartStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Cart Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Cart Style', 'product-blocks' ),
								options: filterFields(
									[ 'cartTab' ],
									'__all',
									CartStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Cart Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Cart Settings', 'product-blocks' ),
								options: filterFields(
									[ 'cartText', 'cartActive' ],
									'__all',
									CartStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'review':
			return (
				<WopbToolbarGroup text={ 'Review' }>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Review Margin', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Review Margin', 'product-blocks' ),
								options: filterFields(
									[ 'reviewMargin' ],
									'__all',
									ReviewStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ colorIcon }
						store={ store }
						label={ __( 'Review Color', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Review Color', 'product-blocks' ),
								options: filterFields(
									[ 'reviewEmptyColor', 'reviewFillColor' ],
									'__all',
									ReviewStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'sales':
			return (
				<WopbToolbarGroup text={ 'Sales' }>
					<TypographyTB
						store={ store }
						attrKey={ 'salesTypo' }
						label={ __( 'Sales Typography', 'product-blocks' ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ colorIcon }
						store={ store }
						label={ __( 'Sales Color', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Sales Color', 'product-blocks' ),
								options: filterFields(
									[ 'salesColor', 'salesBgColor' ],
									'__all',
									SalesStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Sales Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Sales Style', 'product-blocks' ),
								options: filterFields(
									[
										'saleStyle',
										'salesRadius',
										'salesPadding',
									],
									'__all',
									SalesStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Sales Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Sales Settings', 'product-blocks' ),
								options: filterFields(
									[
										'saleText',
										'salePosition',
										'saleDesign',
									],
									'__all',
									SalesStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'hots':
			return (
				<WopbToolbarGroup text={ 'Hots' }>
					<TypographyTB
						store={ store }
						attrKey={ 'hotTypo' }
						label={ __( 'Hots Typography', 'product-blocks' ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Hots Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Hots Style', 'product-blocks' ),
								options: filterFields(
									null,
									[ 'hotTypo' ],
									HotStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'pagination':
			return (
				<WopbToolbarGroup text={ 'Pagination' }>
					<TypographyTB
						store={ store }
						attrKey={ 'pagiTypo' }
						label={ __(
							'Pagination Typography',
							'product-blocks'
						) }
					/>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Pagination Spacing', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Pagination Spacing',
									'product-blocks'
								),
								options: filterFields(
									[
										'pagiMargin',
										'navMargin',
										'pagiPadding',
									],
									'__all',
									PaginationStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Pagination Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Pagination Style',
									'product-blocks'
								),
								options: filterFields(
									[ 'pagiTab' ],
									'__all',
									PaginationStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Pagination Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Pagination Settings',
									'product-blocks'
								),
								options: filterFields(
									[
										'paginationType',
										'loadMoreText',
										'paginationText',
										'pagiAlign',
										'paginationNav',
										'paginationAjax',
										'navPosition',
										'pagiArrowSize',
									],
									'__all',
									PaginationStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'price':
			return (
				<WopbToolbarGroup text={ 'Price' }>
					<TypographyTB
						store={ store }
						attrKey={ 'priceTypo' }
						label={ __( 'Price Typography', 'product-blocks' ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Price Padding', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Price Spacing', 'product-blocks' ),
								options: filterFields(
									[ 'pricePadding' ],
									'__all',
									PriceStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ colorIcon }
						store={ store }
						label={ __( 'Price Color', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Price Color', 'product-blocks' ),
								options: filterFields(
									[ 'priceColor' ],
									'__all',
									PriceStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'image':
			return (
				<WopbToolbarGroup text={ 'Image' }>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Image Dimension', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Image Dimension',
									'product-blocks'
								),
								options: filterFields(
									[ 'imgWidth', 'imgHeight', 'imgMargin' ],
									'__all',
									ImageStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Image Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Image Style', 'product-blocks' ),
								options: filterFields(
									null,
									[
										'imgOverlay',
										'imgOverlayType',
										'overlayColor',
										'imgOpacity',
										'imgWidth',
										'imgHeight',
										'imgMargin',
										'imgCrop',
										'imgAnimation',
									],
									ImageStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Image Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Image Settings', 'product-blocks' ),
								options: filterFields(
									[
										'imgCrop',
										'imgAnimation',
										{
											data: {
												type: 'tag',
												key: 'imageScale',
												label: __(
													'Image Scale',
													'product-blocks'
												),
												options: [
													{
														value: '',
														label: __(
															'None',
															'product-blocks'
														),
													},
													{
														value: 'cover',
														label: __(
															'Cover',
															'product-blocks'
														),
													},
													{
														value: 'contain',
														label: __(
															'Contain',
															'product-blocks'
														),
													},
													{
														value: 'fill',
														label: __(
															'Fill',
															'product-blocks'
														),
													},
													{
														value: 'scale-down',
														label: __(
															'Scale Down',
															'product-blocks'
														),
													},
												],
											},
										},
									],
									'__all',
									ImageStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'heading':
			return (
				<WopbToolbarGroup text={ 'Heading' }>
					<WopbToolbarDropdown
						buttonContent={ typoIcon }
						store={ store }
						label={ __( 'Heading Typography', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Heading Typography',
									'product-blocks'
								),
								options: filterFields(
									[ 'headingTypo', 'subHeadingTypo' ],
									'__all',
									HeadingSettingsArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Heading Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Heading Style', 'product-blocks' ),
								options: filterFields(
									null,
									[ 'headingTypo', 'subHeadingTypo' ],
									HeadingSettingsArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'swatch':
			return (
				<WopbToolbarGroup text={ 'Var. Swatch' }>
					<WopbToolbarDropdown
						buttonContent={ typoIcon }
						store={ store }
						label={ __(
							'Var. Swatch Typography',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Var. Swatch Typography',
									'product-blocks'
								),
								options: filterFields(
									[
										{
											data: {
												type: 'typography',
												key: 'variationSwitchLabelSize',
												label: __(
													'Label Typography',
													'product-blocks'
												),
											},
										},
									],
									null,
									[]
								),
							},
						] ) }
					/>

					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __(
							'Var. Swatch Dimension',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Var. Swatch Dimension',
									'product-blocks'
								),
								options: filterFields(
									[
										{
											data: {
												type: 'range',
												key: 'variationSwitchWidth',
												min: 0,
												max: 100,
												step: 1,
												responsive: true,
												unit: [ 'px', 'em', '%' ],
												label: __(
													'Width',
													'product-blocks'
												),
											},
										},
										{
											data: {
												type: 'range',
												key: 'variationSwitchHeight',
												min: 0,
												max: 100,
												step: 1,
												responsive: true,
												unit: [ 'px', 'em' ],
												label: __(
													'Height',
													'product-blocks'
												),
											},
										},
									],
									null,
									[]
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Var. Swatch Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Var. Swatch Style',
									'product-blocks'
								),
								options: filterFields(
									[
										{
											data: {
												type: 'select',
												key: 'variationSwitchPosition',
												label: __(
													'Position',
													'product-blocks'
												),
												options: [
													{
														value: 'before_title',
														label: __(
															'Before Title',
															'product-blocks'
														),
													},
													{
														value: 'after_title',
														label: __(
															'After Title',
															'product-blocks'
														),
													},
													{
														value: 'before_price',
														label: __(
															'Before Price',
															'product-blocks'
														),
													},
													{
														value: 'after_price',
														label: __(
															'After Price',
															'product-blocks'
														),
													},
												],
											},
										},
										{
											data: {
												type: 'color',
												key: 'variationSwitchLabelBg',
												label: __(
													'Label Background Color',
													'product-blocks'
												),
											},
										},
										{
											data: {
												type: 'color',
												key: 'variationSwitchLabelColor',
												label: __(
													'Label Color',
													'product-blocks'
												),
											},
										},
										{
											data: {
												type: 'dimension',
												key: 'variationSwitchLabelPadding',
												label: __(
													'Label Padding',
													'product-blocks'
												),
												step: 1,
												unit: true,
												responsive: true,
											},
										},
									],
									null,
									[]
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'arrow':
			return (
				<WopbToolbarGroup text={ 'Arrows' }>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Arrow Dimension', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Arrow Dimension',
									'product-blocks'
								),
								options: filterFields(
									null,
									[ 'arrowStyle', 'aTab' ],
									ArrowStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Arrow Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Arrow Style', 'product-blocks' ),
								options: filterFields(
									[ 'arrowStyle', 'aTab' ],
									'__all',
									ArrowStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'dot':
			return (
				<WopbToolbarGroup text={ 'Dots' }>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Dots Dimension', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Dots Dimension', 'product-blocks' ),
								options: filterFields(
									null,
									[ 'dTab' ],
									DotStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Dots Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								options: filterFields(
									[ 'dTab' ],
									'__all',
									DotStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		default:
			return null;
	}
}
