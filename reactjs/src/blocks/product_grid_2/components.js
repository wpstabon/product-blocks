const { __ } = wp.i18n;
const { Fragment } = wp.element;
import {
	renderTitle,
	renderDeals,
	renderExcerpt,
} from '../../helper/CommonPanel';
import {
	renderGridCart,
	renderGridCompare,
	renderGridQuickview,
	renderGridWishlist,
} from '../../helper/grid_compo/GridCommon';
import useStyleButtonToggle from '../../helper/hooks/useStyleButtonToggle';

export const Card = ( {
	attributes,
	post,
	category,
	variationSwitcherData,
	TitleTag,
	setSection,
} ) => {
	return (
		<div className={ `wopb-block-item` }>
			<div className={ `wopb-block-content-wrap` }>
				<Image
					attributes={ attributes }
					category={ category }
					post={ post }
					setSection={ setSection }
				/>
				<div className="wopb-product-grid3-overlay"></div>
				<Body
					attributes={ attributes }
					category={ category }
					variationSwitcherData={ variationSwitcherData }
					TitleTag={ TitleTag }
					post={ post }
					setSection={ setSection }
				/>
			</div>
		</div>
	);
};

export const Body = ( {
	attributes,
	category,
	variationSwitcherData,
	post,
	TitleTag,
	setSection,
} ) => {
	const {
		catShow,
		showCart,
		titleShow,
		showPrice,
		showReview,
		catPosition,
		cartText,
		showShortDesc,
		shortDescLimit,
		showVariationSwitch,
		variationSwitchPosition,
		titleLength,
	} = attributes;

	return (
		<div className="wopb-product-grid3-content">
			{ variationSwitchPosition == 'before_title' &&
				showVariationSwitch &&
				titleShow &&
				variationSwitcherData }
			{ catShow && catPosition == 'beforeTitle' && category }
			{ post.title &&
				titleShow &&
				renderTitle(
					post.title,
					TitleTag,
					( e ) => setSection( e, 'title' ),
					undefined,
					titleLength
				) }
			{ variationSwitchPosition == 'after_title' &&
				showVariationSwitch &&
				titleShow &&
				variationSwitcherData }

			{ catShow && catPosition == 'none' && category }
			{ variationSwitchPosition == 'before_price' &&
				showVariationSwitch &&
				showPrice &&
				variationSwitcherData }
			{ post.price_html && showPrice && (
				<div
					className="wopb-product-price wopb-component-simple"
					onClick={ ( e ) => setSection( e, 'price' ) }
					dangerouslySetInnerHTML={ { __html: post.price_html } }
				></div>
			) }
			{ variationSwitchPosition == 'after_price' &&
				showVariationSwitch &&
				showPrice &&
				variationSwitcherData }
			{ ( showShortDesc || showReview || showCart ) && (
				<div className="wopb-fade-in-block">
					{ showReview && (
						<div
							className="wopb-star-rating wopb-component-simple"
							onClick={ ( e ) => setSection( e, 'review' ) }
						>
							<span
								style={ {
									width:
										( post.rating_average / 5 ) * 100 + '%',
								} }
							>
								<strong
									itemProp="ratingValue"
									className="wopb-rating"
								>
									{ post.rating_average }
								</strong>
								{ __( 'out of 5', 'product-blocks' ) }
							</span>
						</div>
					) }
					{ showShortDesc && (
						<div className={ `wopb-short-description` }>
							{ renderExcerpt(
								post.excerpt,
								shortDescLimit,
								( e ) => setSection( e, 'desc' )
							) }
						</div>
					) }
					{ variationSwitchPosition == 'before_cart' &&
						showVariationSwitch &&
						showCart &&
						variationSwitcherData }
					<div className="wopb-quick-cart wopb-grid3-quick-cart">
						{ showCart && (
							<div
								className="wopb-product-btn wopb-component-simple"
								onClick={ ( e ) => setSection( e, 'cart' ) }
							>
								<a>
									{ cartText
										? cartText
										: __(
												'Add to cart',
												'product-blocks'
										  ) }
								</a>
							</div>
						) }
					</div>
					{ variationSwitchPosition == 'after_cart' &&
						showVariationSwitch &&
						showCart &&
						variationSwitcherData }
				</div>
			) }
		</div>
	);
};

export const Image = ( { attributes, post, category, setSection } ) => {
	const {
		showHot,
		showDeal,
		imgCrop,
		catShow,
		showImage,
		showSale,
		saleStyle,
		saleDesign,
		catPosition,
		imgAnimation,
		showOutStock,
		showInStock,
		cartText,
		dealText,
		hotText,
		saleText,
		overlayMetaList,
		tooltipPosition,
		ovrMetaInline,
		hoverMeta,
	} = attributes;

	const parsedOverlayMetaList = JSON.parse( overlayMetaList );
	const { StyleButton, showStyleButton } = useStyleButtonToggle( ( e ) =>
		setSection( e, 'image' )
	);
	let metaClass = ovrMetaInline ? ' wopb_f_inline' : '';
	metaClass += ! hoverMeta ? ' wopb-is-visible' : '';
	return (
		<div
			className={ `wopb-block-image wopb-block-image-${ imgAnimation }` }
		>
			{ StyleButton }
			<div className={ `wopb-onsale-hot` }>
				{ showSale && post.sale && (
					<span
						className={ `wopb-onsale wopb-onsale-${ saleStyle } wopb-component-simple` }
						onClick={ ( e ) => setSection( e, 'sales' ) }
					>
						{ saleDesign === 'digit' && '-' + post.discount }
						{ saleDesign === 'text' &&
							( saleText || __( 'Sale!', 'product-blocks' ) ) }
						{ saleDesign === 'textDigit' &&
							__( '-' + post.discount + ' Off' ) }
					</span>
				) }
				{ showHot && (
					<span
						className={ `wopb-hot wopb-component-simple` }
						onClick={ ( e ) => setSection( e, 'hots' ) }
					>
						{ hotText || __( 'Hot', 'product-blocks' ) }
					</span>
				) }
			</div>

			{ parsedOverlayMetaList.length != 0 && (
				<div
					className={
						`wopb-product-new-meta wopb-component-simple` +
						metaClass
					}
					onClick={ ( e ) => {
						setSection( e, 'overlay_meta' );
					} }
				>
					{ parsedOverlayMetaList.map( ( val, _i ) => {
						return (
							<Fragment key={ _i }>
								{ val == '_wishlist' &&
									renderGridWishlist( { tooltipPosition } ) }
								{ val == '_qview' &&
									renderGridQuickview( { tooltipPosition } ) }
								{ val == '_compare' &&
									renderGridCompare( { tooltipPosition } ) }
								{ val == '_cart' &&
									renderGridCart( {
										cartText,
										tooltipPosition,
									} ) }
							</Fragment>
						);
					} ) }
				</div>
			) }

			{ catShow &&
				catPosition != 'none' &&
				catPosition != 'beforeTitle' &&
				category }

			{ post.stock == 'outofstock' && showOutStock && (
				<div className="wopb-product-outofstock">
					<span>{ __( 'Out of stock', 'product-blocks' ) }</span>
				</div>
			) }

			{ post.stock == 'instock' && showInStock && (
				<div className="wopb-product-instock">
					<span>{ __( 'In Stock', 'product-blocks' ) }</span>
				</div>
			) }

			{ showDeal && renderDeals( post.deal, dealText ) }

			{ showImage && post.image && (
				<a>
					<img
						alt={ post.title || '' }
						src={ post.image[ imgCrop ] }
						className="wopb-component-simple"
						onClick={ showStyleButton }
					/>
				</a>
			) }
		</div>
	);
};
