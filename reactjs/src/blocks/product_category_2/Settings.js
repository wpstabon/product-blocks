const { __ } = wp.i18n;
import {
	CategoryCountStyle,
	CategoryCountStyleArg,
	ContentWrapStyle,
	CustomCssAdvanced,
	GeneralAdvanced,
	GeneralSettingsWithQuery,
	HeadingSettings,
	HeadingSettingsArg,
	ImageStyle,
	ImageStyleArg,
	ReadMoreStyle,
	ReadMoreStyleArg,
	ResponsiveAdvanced,
	ShortDescStyle,
	ShortDescStyleArg,
	TitleStyle,
	TitleStyleArg,
	TypographyTB,
	colorIcon,
	filterFields,
	settingsIcon,
	spacingIcon,
	styleIcon,
	titleColor,
	typoIcon,
	wopbSupport,
} from '../../helper/CommonPanel';
import LinkGenerator from '../../helper/LinkGenerator';
import { Section, Sections } from '../../helper/Sections';
import TemplateModal from '../../helper/TemplateModal';
import icons from '../../helper/icons';
import WopbToolbarDropdown from '../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar } from '../../helper/ux';

export const features = ( store ) => {
	return [
		{
			type: 'toggle',
			key: 'headingShow',
			label: __( 'Heading', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'titleShow',
			label: __( 'Title', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'countShow',
			label: __( 'Count', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'descShow',
			label: __( 'Description', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'readMore',
			label: __( 'Read More', 'product-blocks' ),
		},
	];
};

export const defLayout = {
	type: 'layout',
	block: 'product-category-2',
	key: 'layout',
	pro: true,
	options: [
		{
			icon: icons.cg1layout1,
			label: __( 'Layout 1' ),
			value: '1',
			pro: false,
		},
		{
			icon: icons.cg1layout2,
			label: __( 'Layout 2' ),
			value: '2',
			pro: false,
		},
		{
			icon: icons.cg1layout3,
			label: __( 'Layout 3' ),
			value: '3',
			pro: true,
		},
		{
			icon: icons.cg1layout4,
			label: __( 'Layout 4' ),
			value: '4',
			pro: true,
		},
		{
			icon: icons.cg1layout5,
			label: __( 'Layout 5' ),
			value: '5',
			pro: true,
		},
		{
			icon: icons.cg1layout6,
			label: __( 'Layout 6' ),
			value: '6',
			pro: true,
		},
		{
			icon: icons.cg1layout7,
			label: __( 'Layout 7' ),
			value: '7',
			pro: false,
		},
		{
			icon: icons.cg1layout8,
			label: __( 'Layout 8' ),
			value: '8',
			pro: false,
		},
		{
			icon: icons.cg1layout9,
			label: __( 'Layout 9' ),
			value: '9',
			pro: true,
		},
	],
};

export default function Settings( { store } ) {
	const { section } = store;

	return (
		<>
			<TemplateModal
				prev={
					LinkGenerator(
						'https://www.wpxpo.com/wowstore/blocks/?',
						'blockPreview'
					) + '#demoid504'
				}
				store={ store }
			/>
			<Sections>
				<Section
					slug="setting"
					title={ __( 'Settings', 'product-blocks' ) }
				>
					<GeneralSettingsWithQuery
						blockType={ 'category' }
						include={ [ { position: 0, data: defLayout } ] }
						initialOpen={ section.general }
						store={ store }
						exclude={ [
							'showCompare',
							'productView',
							'columns',
							'columnGap',
							'rowGap',
							'slidesToShow',
							'autoPlay',
							'slideSpeed',
							'sortSection',
							'showPrice',
							'quickView',
							'showOutStock',
							'showInStock',
							'showImage',
							'disableFlip',
						] }
					/>
					<HeadingSettings
						depend="headingShow"
						store={ store }
						initialOpen={ section.heading }
					/>
					<ContentWrapStyle store={ store } />
					<ImageStyle
						initialOpen={ section.image }
						exclude={ [
							'imgOverlay',
							'imgOverlayType',
							'overlayColor',
							'imgOpacity',
							'imgHeight',
							'imgWidth',
						] }
						store={ store }
						include={ [
							{
								position: 2,
								data: {
									type: 'range',
									key: 'imgWidth',
									min: 1,
									max: 1000,
									step: 1,
									responsive: true,
									unit: [ '%', 'px', 'em' ],
									label: __( 'Width', 'product-blocks' ),
								},
							},
							{
								position: 3,
								data: {
									type: 'range',
									key: 'imgHeight',
									min: 1,
									max: 1000,
									step: 1,
									responsive: true,
									unit: [ '%', 'px', 'em' ],
									label: __( 'Height', 'product-blocks' ),
								},
							},
							{
								position: 4,
								data: {
									type: 'tag',
									key: 'imageScale',
									label: __(
										'Image Scale',
										'product-blocks'
									),
									options: [
										{
											value: '',
											label: __(
												'None',
												'product-blocks'
											),
										},
										{
											value: 'cover',
											label: __(
												'Cover',
												'product-blocks'
											),
										},
										{
											value: 'contain',
											label: __(
												'Contain',
												'product-blocks'
											),
										},
										{
											value: 'fill',
											label: __(
												'Fill',
												'product-blocks'
											),
										},
										{
											value: 'scale-down',
											label: __(
												'Scale Down',
												'product-blocks'
											),
										},
									],
								},
							},
						] }
					/>

					<TitleStyle
						depend="titleShow"
						store={ store }
						initialOpen={ section.title }
						exclude={ [ 'titleLength' ] }
					/>
					<CategoryCountStyle
						depend="countShow"
						store={ store }
						initialOpen={ section.count }
					/>
					<ShortDescStyle
						depend="descShow"
						title="Description"
						store={ store }
						exclude={ [ 'showFullShortDesc', 'shortDescLimit' ] }
						include={ [
							{
								position: 0,
								data: {
									type: 'range',
									key: 'descLimit',
									min: 1,
									max: 200,
									label: __(
										'Description Limit',
										'product-blocks'
									),
								},
							},
						] }
						initialOpen={ section.desc }
					/>
					<ReadMoreStyle
						depend="readMore"
						store={ store }
						initialOpen={ section[ 'read-more' ] }
					/>
				</Section>
				<Section
					slug="advanced"
					title={ __( 'Advanced', 'product-blocks' ) }
				>
					<GeneralAdvanced initialOpen={ true } store={ store } />
					<ResponsiveAdvanced pro={ true } store={ store } />
					<CustomCssAdvanced store={ store } />
				</Section>
			</Sections>
			{ wopbSupport() }
		</>
	);
}

export function AddSettingsToToolbar( { selected, store } ) {
	switch ( selected ) {
		case 'title':
			return (
				<WopbToolbarGroup text={ 'Title' }>
					<TypographyTB
						store={ store }
						attrKey={ 'titleTypo' }
						label={ __( 'Title Typography', 'product-blocks' ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Title Spacing', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Title Spacing', 'product-blocks' ),
								options: filterFields(
									[ 'titlePadding' ],
									'__all',
									TitleStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ colorIcon }
						store={ store }
						label={ __( 'Title Color', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Title Color', 'product-blocks' ),
								options: filterFields(
									titleColor,
									'__all',
									TitleStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Title Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Title Settings', 'product-blocks' ),
								options: filterFields(
									null,
									[
										...titleColor,
										'titleTypo',
										'titlePadding',
									],
									TitleStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'desc':
			return (
				<WopbToolbarGroup text={ 'Desc.' }>
					<TypographyTB
						store={ store }
						attrKey={ 'ShortDescTypo' }
						label={ __(
							'Description Typography',
							'product-blocks'
						) }
					/>
					<WopbToolbarDropdown
						buttonContent={ colorIcon }
						store={ store }
						label={ __( 'Description Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Description Settings',
									'product-blocks'
								),
								options: filterFields(
									[
										{
											position: 0,
											data: {
												type: 'range',
												key: 'descLimit',
												min: 1,
												max: 200,
												label: __(
													'Description Limit',
													'product-blocks'
												),
											},
										},
										'ShortDescColor',
										'ShortDescPadding',
									],
									'__all',
									ShortDescStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'image':
			return (
				<WopbToolbarGroup text={ 'Image' }>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Image Dimension', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Image Dimension',
									'product-blocks'
								),
								options: filterFields(
									[
										{
											data: {
												type: 'range',
												key: 'imgWidth',
												min: 1,
												max: 1000,
												step: 1,
												responsive: true,
												unit: [ '%', 'px', 'em' ],
												label: __(
													'Width',
													'product-blocks'
												),
											},
										},
										{
											data: {
												type: 'range',
												key: 'imgHeight',
												min: 1,
												max: 1000,
												step: 1,
												responsive: true,
												unit: [ '%', 'px', 'em' ],
												label: __(
													'Height',
													'product-blocks'
												),
											},
										},
										'imgMargin',
									],
									'__all',
									ImageStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Image Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Image Style', 'product-blocks' ),
								options: filterFields(
									null,
									[
										'imgOverlay',
										'imgOverlayType',
										'overlayColor',
										'imgOpacity',
										'imgWidth',
										'imgHeight',
										'imgMargin',
										'imgCrop',
										'imgAnimation',
									],
									ImageStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Image Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Image Settings', 'product-blocks' ),
								options: filterFields(
									[
										'imgCrop',
										'imgAnimation',
										{
											data: {
												type: 'tag',
												key: 'imageScale',
												label: __(
													'Image Scale',
													'product-blocks'
												),
												options: [
													{
														value: '',
														label: __(
															'None',
															'product-blocks'
														),
													},
													{
														value: 'cover',
														label: __(
															'Cover',
															'product-blocks'
														),
													},
													{
														value: 'contain',
														label: __(
															'Contain',
															'product-blocks'
														),
													},
													{
														value: 'fill',
														label: __(
															'Fill',
															'product-blocks'
														),
													},
													{
														value: 'scale-down',
														label: __(
															'Scale Down',
															'product-blocks'
														),
													},
												],
											},
										},
									],
									'__all',
									ImageStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'heading':
			return (
				<WopbToolbarGroup text={ 'Heading' }>
					<WopbToolbarDropdown
						buttonContent={ typoIcon }
						store={ store }
						label={ __( 'Heading Typography', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Heading Typography',
									'product-blocks'
								),
								options: filterFields(
									[ 'headingTypo', 'subHeadingTypo' ],
									'__all',
									HeadingSettingsArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Heading Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Heading Style', 'product-blocks' ),
								options: filterFields(
									null,
									[ 'headingTypo', 'subHeadingTypo' ],
									HeadingSettingsArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'read-more':
			return (
				<WopbToolbarGroup text={ 'Read More' }>
					<TypographyTB
						store={ store }
						attrKey={ 'readMoreTypo' }
						label={ __( 'Read More Typography', 'product-blocks' ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Read More Spacing', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Read More Spacing',
									'product-blocks'
								),
								options: filterFields(
									[ 'readMoreSpacing', 'readMorePadding' ],
									'__all',
									ReadMoreStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Read More Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Read More Style',
									'product-blocks'
								),
								options: filterFields(
									[ 'readMoreText', 'rmTab' ],
									'__all',
									ReadMoreStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		case 'count':
			return (
				<WopbToolbarGroup text={ 'Count' }>
					<TypographyTB
						store={ store }
						attrKey={ 'categoryrCountTypo' }
						label={ __( 'Count Typography', 'product-blocks' ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Count Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Count Style', 'product-blocks' ),
								options: filterFields(
									null,
									[ 'categoryrCountTypo' ],
									CategoryCountStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		default:
			return null;
	}
}
