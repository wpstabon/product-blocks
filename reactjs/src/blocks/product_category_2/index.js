const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';
registerBlockType( 'product-blocks/product-category-2', {
	title: __( 'Product Category #2' ),
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/product-category-2.svg' }
			alt={ __( 'Product Category #2', 'product-blocks' ) }
			style={ { width: '100%' } }
		/>
	),
	category: 'product-blocks',
	// description: <span className="wopb-block-info">
	//     {__('Display Product Categories of your store in grid or slide view.', 'product-blocks')}
	//     <a target="_blank" href='https://wpxpo.com/docs/wowstore/all-blocks/product-category-2/' >{__('Documentation', 'product-blocks')}</a>
	// </span>,
	keywords: [
		__( 'category', 'product-blocks' ),
		__( 'taxonomy', 'product-blocks' ),
	],
	supports: {
		align: [ 'center', 'wide', 'full' ],
	},
	example: {
		attributes: {
			previewId:
				wopb_data.url + 'assets/img/preview/product-category-2.svg',
		},
	},
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
