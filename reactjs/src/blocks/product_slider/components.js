import icons from '../../helper/icons';
const { __ } = wp.i18n;

export function TaxonomyContent( { product, attributes, setSection } ) {
	const { showTaxonomy, taxonomyType } = attributes;
	if ( showTaxonomy && ( product.category.length || product.tag.length ) ) {
		return (
			<div
				className={ `wopb-product-taxonomies wopb-component-simple` }
				onClick={ ( e ) => {
					localStorage.setItem( 'wopb-slider-1-tab', 'taxonomy' );
					setSection( e, 'contents' );
				} }
			>
				{ taxonomyType == 'product_cat' &&
					product.category &&
					product.category.length &&
					product.category.map( ( v, k ) => (
						<a href={ `#` } key={ k } className={ `wopb-taxonomy` }>
							{ v.name }
						</a>
					) ) }
				{ taxonomyType == 'product_tag' &&
					product.tag &&
					product.tag.length &&
					product.tag.map( ( v, k ) => (
						<a href={ `#` } key={ k } className={ `wopb-taxonomy` }>
							{ v.name }
						</a>
					) ) }
			</div>
		);
	}
}

export function PriceContent( { product, attributes, setSection } ) {
	const { showPrice, showPriceLabel, priceLabelText } = attributes;
	if ( showPrice ) {
		let priceHtml = `<span class="wopb-prices wopb-${ product.type }-price">${ product.price_html }</span>`;
		if ( showPriceLabel && priceLabelText ) {
			priceHtml =
				`<span class="wopb-price-label">${ priceLabelText }</span>` +
				priceHtml;
		}
		return (
			<div
				className={ `wopb-product-price-section wopb-component-simple` }
				onClick={ ( e ) => {
					localStorage.setItem( 'wopb-slider-1-tab', 'price' );
					setSection( e, 'contents' );
				} }
			>
				<span
					className={ `wopb-product-price` }
					dangerouslySetInnerHTML={ { __html: priceHtml } }
				/>
			</div>
		);
	}
}

export function CartContent( { attributes, setSection } ) {
	const { showCart, showQty, showPlusMinus, cartText, plusMinusPosition } =
		attributes;
	if ( showCart ) {
		return (
			<div
				className={ `wopb-product-cart-section wopb-component` }
				onClick={ ( e ) => setSection( e, 'add-to-cart' ) }
			>
				<form
					className={ `cart wopb-component-hover` }
					onSubmit={ ( e ) => e.preventDefault() }
				>
					{ showQty && (
						<div className={ `quantity wopb-qty-wrap` }>
							{ showPlusMinus && plusMinusPosition == 'both' && (
								<span
									className={ `wopb-cart-minus wopb-add-to-cart-minus` }
								>
									{ icons.minusIcon }
								</span>
							) }
							<input
								type="number"
								className={ `input-text qty text` }
								step="1"
								min="1"
								max=""
								name="quantity"
								value="1"
								title="Qty"
								size={ 4 }
								placeholder=""
								inputMode="numeric"
							/>
							{ showPlusMinus && plusMinusPosition == 'both' && (
								<span
									className={ `wopb-cart-plus wopb-add-to-cart-plus` }
								>
									{ icons.plusIcon }
								</span>
							) }
							{ showPlusMinus &&
								( plusMinusPosition == 'left' ||
									plusMinusPosition == 'right' ) && (
									<span
										className={ `wopb-cart-plus-minus-icon` }
									>
										<PlusMinusContent />
									</span>
								) }
						</div>
					) }
					<button
						className={ `wopb-product-cart single_add_to_cart_button ajax_add_to_cart` }
						style={ { cursor: 'pointer' } }
					>
						{ __( cartText, 'product-blocks' ) }
					</button>
				</form>
			</div>
		);
	}
}

export function PlusMinusContent() {
	return (
		<>
			<span className={ `wopb-cart-plus wopb-add-to-cart-plus` }>
				{ icons.plusIcon }
			</span>
			<span className={ `wopb-cart-minus wopb-add-to-cart-minus` }>
				{ icons.minusIcon }
			</span>
		</>
	);
}

export function ContentSection( { product, attributes, setSection } ) {
	const {
		showTitle,
		showDescription,
		titleTag,
		taxonomyUnderTitle,
		descriptionLimit,
		priceOverDescription,
		titleHoverEffect,
		titleLength,
	} = attributes;
	let productTitle = product.title;
	if ( titleLength && titleLength != 0 && productTitle ) {
		const titleArr = productTitle.split( ' ' );
		const titleArrL = titleArr.length;
		productTitle =
			titleArr.splice( 0, titleLength ).join( ' ' ) +
			( titleArrL > titleLength ? '...' : '' );
	}
	const ProductTitleTag = titleTag;
	const productExcerpt =
		product.excerpt.length > descriptionLimit
			? product.excerpt.slice( 0, descriptionLimit ) + '...'
			: product.excerpt;

	return (
		<div className={ `wopb-content-section` }>
			{ ! taxonomyUnderTitle && (
				<TaxonomyContent
					product={ product }
					attributes={ attributes }
					setSection={ setSection }
				/>
			) }
			{ showTitle && (
				<ProductTitleTag
					className={ `wopb-product-title ${
						titleHoverEffect === 'none'
							? ''
							: `wopb-title-${ titleHoverEffect }`
					} wopb-component-simple` }
					onClick={ ( e ) => {
						setSection( e, 'contents' );
						localStorage.setItem( 'wopb-slider-1-tab', 'title' );
					} }
				>
					<a dangerouslySetInnerHTML={ { __html: productTitle } }></a>
				</ProductTitleTag>
			) }
			{ taxonomyUnderTitle && (
				<TaxonomyContent
					product={ product }
					attributes={ attributes }
					setSection={ setSection }
				/>
			) }
			{ priceOverDescription && (
				<PriceContent
					product={ product }
					attributes={ attributes }
					setSection={ setSection }
				/>
			) }
			{ showDescription && (
				<div
					className={ `wopb-product-excerpt wopb-component-simple` }
					dangerouslySetInnerHTML={ {
						__html: __( productExcerpt, 'product-blocks' ),
					} }
					onClick={ ( e ) => {
						localStorage.setItem(
							'wopb-slider-1-tab',
							'description'
						);
						setSection( e, 'contents' );
					} }
				/>
			) }
			{ ! priceOverDescription && (
				<PriceContent
					product={ product }
					attributes={ attributes }
					setSection={ setSection }
				/>
			) }
			{
				<CartContent
					attributes={ attributes }
					setSection={ setSection }
				/>
			}
		</div>
	);
}

export function ImageSection( { product, attributes, data, setSection } ) {
	const { showSaleBadge, saleStyle, showImgOverlay } = attributes;
	let shapeClass = '';
	let shape = null;
	if ( saleStyle === 'shape1' ) {
		shapeClass = ' wopb-onsale-shape';
		switch ( saleStyle ) {
			case 'shape1':
				shape = icons.saleShape1;
				break;
			default:
				break;
		}
	}
	return (
		<div
			className={ `wopb-image-section wopb-component-simple` }
			onClick={ ( e ) => setSection( e, 'image' ) }
		>
			<span className={ `wopb-product-image` }>
				{ showImgOverlay && (
					<div
						className={ `wopb-image-overlay wopb-component-simple` }
						onClick={ ( e ) => setSection( e, 'image' ) }
					/>
				) }
				{ product.image.large ? (
					<img
						className={ `wopb-block-image wopb-component-simple` }
						alt={ product.title || '' }
						src={ product.image.large }
						onClick={ ( e ) => setSection( e, 'image' ) }
					/>
				) : (
					<img
						className={ `wopb-block-image wopb-fallback-image wopb-component-simple` }
						alt={ product.title || '' }
						src={
							wopb_data.url + `assets/img/wopb-fallback-img.png`
						}
						onClick={ ( e ) => setSection( e, 'image' ) }
					/>
				) }
				{ showSaleBadge && product.sale && product.discount && (
					<div className={ `wopb-onsale-hot` + shapeClass }>
						{ shape && (
							<span className={ `wopb-sale-shape` }>
								{ shape }
								{
									<SaleStyle
										product={ product }
										attributes={ attributes }
										setSection={ setSection }
									/>
								}
							</span>
						) }
						{ ! shape && (
							<SaleStyle
								product={ product }
								attributes={ attributes }
								setSection={ setSection }
							/>
						) }
					</div>
				) }
			</span>
		</div>
	);
}

export function SaleStyle( { product, attributes, setSection } ) {
	const { saleDesign, saleText, saleStyle } = attributes;
	return (
		<span
			className={ `wopb-onsale wopb-onsale-${ saleStyle }` }
			onClick={ ( e ) => setSection( e, 'sales' ) }
		>
			{ saleDesign === 'digit' && '-' + product.discount }
			{ saleDesign === 'text' &&
				( saleText || __( 'Sale!', 'product-blocks' ) ) }
			{ saleDesign === 'textDigit' &&
				__( '-' + product.discount + ' Off' ) }
		</span>
	);
}
