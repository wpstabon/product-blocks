/** @format */

const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
const { Spinner, Placeholder } = wp.components;
import Slider from 'react-slick';
import { CssGenerator } from '../../helper/CssGenerator';
import { SliderSetting, NextArrow, PrevArrow } from '../../helper/CommonPanel';
import ToolBarElement from '../../helper/ToolBarElement';
import { ContentSection, ImageSection } from './components';
import Settings, {
	AddSettingsToToolbar,
	defLayout,
	features,
} from './settings';
import useBlockId from '../../helper/hooks/use-block-id';
import usePrevious from '../../helper/hooks/use-previous';
import useProducts from '../../helper/hooks/use-products';
import useFluentSettings from '../../helper/hooks/useFluentSettings';

const data = {
	percentage: '10%',
	images: [
		wopb_data.url + 'assets/img/blocks/builder/sample/1.jpg',
		wopb_data.url + 'assets/img/blocks/builder/sample/2.jpg',
		wopb_data.url + 'assets/img/blocks/builder/sample/3.jpg',
		wopb_data.url + 'assets/img/blocks/builder/sample/1.jpg',
		wopb_data.url + 'assets/img/blocks/builder/sample/2.jpg',
		wopb_data.url + 'assets/img/blocks/builder/sample/3.jpg',
	],
	images_thumb: [
		wopb_data.url + 'assets/img/blocks/builder/sample/1.jpg',
		wopb_data.url + 'assets/img/blocks/builder/sample/2.jpg',
		wopb_data.url + 'assets/img/blocks/builder/sample/3.jpg',
		wopb_data.url + 'assets/img/blocks/builder/sample/1.jpg',
		wopb_data.url + 'assets/img/blocks/builder/sample/2.jpg',
		wopb_data.url + 'assets/img/blocks/builder/sample/3.jpg',
	],
};

export default function Edit( props ) {
	useBlockId( props, true );

	const { setAttributes, name, attributes, clientId, className, context } =
		props;
	const prevProps = usePrevious( props );
	const {
		postsList: productsList,
		loading,
		error,
	} = useProducts( attributes, prevProps?.attributes );
	const { section, setSection, setCurrSettings, toolbarSettings } =
		useFluentSettings( { props, prevProps } );

	const {
		blockId,
		advanceId,
		showImage,
		showArrows,
		showDots,
		slideAnimation,
		autoPlay,
		slideSpeed,
		arrowStyle,
		slidesToShow,
		previewId,
		contentAlign,
	} = attributes;

	const store = {
		setAttributes,
		name,
		attributes,
		setSection,
		section,
		clientId,
		context,
	};

	if ( previewId ) {
		return (
			<img
				style={ { marginTop: '0px', width: '420px' } }
				src={ previewId }
			/>
		);
	}

	const settings = SliderSetting( {
		arrows: showArrows,
		dots: showDots,
		slidesToShow,
		autoplay: autoPlay,
		autoplaySpeed: parseInt( slideSpeed ),
		fade: slideAnimation == 'fade' ? true : false,
		nextArrow: (
			<NextArrow
				arrowStyle={ arrowStyle }
				setSection={ ( e ) => {
					setCurrSettings( e, 'arrow' );
				} }
			/>
		),
		prevArrow: (
			<PrevArrow
				arrowStyle={ arrowStyle }
				setSection={ ( e ) => {
					setCurrSettings( e, 'arrow' );
				} }
			/>
		),
		appendDots: ( dots ) => {
			return (
				<div
					onClick={ ( e ) => {
						setCurrSettings( e, 'dot' );
					} }
				>
					<ul> { dots } </ul>
				</div>
			);
		},
	} );

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/product-slider', blockId );
	}

	// if (!attributes.initPremade) {
	//     return <InitBlockPremade store={store} />;
	// }

	return (
		<>
			<InspectorControls>
				<Settings store={ store } />
			</InspectorControls>

			<ToolBarElement
				include={ [
					{
						type: 'alignment',
						key: 'contentAlign',
						label: __( 'Grid Content Alignment', 'product-blocks' ),
						value: contentAlign,
						disableJustify: true,
					},
					defLayout,
					{ type: 'template' },
					{ type: 'query' },
					// { type: "grid_spacing" },
					{
						type: 'common',
						data: features( store ),
					},
				] }
				store={ store }
			/>

			<AddSettingsToToolbar
				store={ store }
				selected={ toolbarSettings }
			/>

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
				onClick={ ( e ) => setCurrSettings( e, 'general', 'none' ) }
			>
				<div className={ `wopb-block-wrapper` }>
					<div
						className={ `wopb-slider-section wopb-product-blocks-slide` }
					>
						{ ! error ? (
							! loading ? (
								productsList.length > 0 ? (
									<Slider { ...settings }>
										{ productsList.map(
											( product, idx ) => {
												return (
													<div
														className={ `wopb-block-item` }
														onClick={ ( e ) =>
															setCurrSettings(
																e,
																'background'
															)
														}
														key={ product.post_id }
													>
														<div
															className={ `wopb-slide-wrap` }
														>
															<ContentSection
																product={
																	product
																}
																attributes={
																	attributes
																}
																setSection={
																	setCurrSettings
																}
															/>
															{ showImage && (
																<ImageSection
																	data={
																		data
																	}
																	product={
																		product
																	}
																	attributes={
																		attributes
																	}
																	setSection={
																		setCurrSettings
																	}
																/>
															) }
														</div>
													</div>
												);
											}
										) }
									</Slider>
								) : (
									<Placeholder
										className={ `wopb-backend-block-loading` }
										label={ __(
											'No Products found',
											'product-blocks'
										) }
									></Placeholder>
								)
							) : (
								<Placeholder
									className={ `wopb-backend-block-loading` }
									label={ __( 'Loading…', 'product-blocks' ) }
								>
									{ ' ' }
									<Spinner />
								</Placeholder>
							)
						) : (
							<Placeholder
								label={ __(
									'Products are not available',
									'product-blocks'
								) }
							>
								<div style={ { marginBottom: 15 } }>
									{ __(
										'Make sure Add Product.',
										'product-blocks'
									) }
								</div>
							</Placeholder>
						) }
					</div>
				</div>
			</div>
		</>
	);
}
