const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';

registerBlockType( 'product-blocks/product-slider', {
	title: __( 'Product Slider 1', 'product-blocks' ),
	icon: (
		<div className={ `wopb-block-inserter-icon-section` }>
			<img
				src={ wopb_data.url + 'assets/img/blocks/slider.svg' }
				alt={ __( 'Product Slider', 'product-blocks' ) }
				style={ { width: '100%' } }
			/>
		</div>
	),
	example: {
		attributes: {
			previewId:
				wopb_data.url + 'assets/img/preview/product-slider-1.svg',
		},
	},
	category: 'product-blocks',
	keywords: [
		__( 'Slider', 'product-blocks' ),
		__( 'Product Slider', 'product-blocks' ),
	],
	supports: {
		align: [ 'center', 'wide', 'full' ],
	},
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
