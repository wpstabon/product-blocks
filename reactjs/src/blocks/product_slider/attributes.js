const attributes = {
	blockId: { type: 'string', default: '' },
	currentPostId: { type: 'string', default: '' },
	initPremade: {
		type: 'boolean',
		default: false,
	},
	align: { type: 'string', default: 'full' },
	previewId: { type: 'string', default: '' },

	//General Style
	layout: {
		type: 'string',
		default: 'layout2',
		style: [
			{
				depends: [
					{ key: 'layout', condition: '==', value: 'layout1' },
				],
			},
			{
				depends: [
					{ key: 'layout', condition: '==', value: 'layout2' },
				],
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-block-item .wopb-slide-wrap { flex-direction: row-reverse; }',
			},
			{
				depends: [
					{ key: 'layout', condition: '==', value: 'layout3' },
				],
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-block-item .wopb-slide-wrap { position: relative;flex-direction: row-reverse; }' +
					'{{WOPB}} .wopb-block-wrapper .wopb-slide-wrap .wopb-content-section {position: absolute; z-index: 9;}',
			},
		],
	},
	height: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-block-item { height: {{height}}; }',
			},
		],
	},
	slideAnimation: {
		type: 'string',
		default: 'default',
	},
	stackOnMobile: {
		type: 'boolean',
		default: true,
		style: [
			{
				depends: [
					{ key: 'stackOnMobile', condition: '==', value: true },
					{ key: 'layout', condition: '==', value: 'layout1' },
				],
				selector:
					'@media screen and (max-width:768px) { {{WOPB}} .wopb-block-wrapper .wopb-block-item .wopb-slide-wrap { flex-direction: column; } }',
			},
			{
				depends: [
					{ key: 'stackOnMobile', condition: '==', value: true },
					{ key: 'layout', condition: '==', value: 'layout2' },
				],
				selector:
					'@media screen and (max-width:768px) { {{WOPB}} .wopb-block-wrapper .wopb-block-item .wopb-slide-wrap { flex-direction: column-reverse; } }',
			},
			{
				depends: [
					{ key: 'stackOnMobile', condition: '==', value: false },
				],
			},
		],
	},
	autoPlay: {
		type: 'boolean',
		default: true,
	},
	slideSpeed: {
		type: 'string',
		default: '3000',
	},
	slidesToShow: {
		type: 'object',
		default: { lg: '1', sm: '1', xs: '1' },
	},
	sliderMaxWidth: {
		type: 'object',
		default: { lg: '1110', sm: '600', xs: '300', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-slider-section { max-width:{{sliderMaxWidth}}; }',
			},
		],
	},
	itemGap: {
		type: 'object',
		default: { lg: 64, sm: 64, xs: 15, unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-item .wopb-slide-wrap { gap:{{itemGap}}; }',
			},
		],
	},
	sliderAlign: {
		type: 'object',
		default: { lg: 'start' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-item .wopb-slide-wrap { justify-content:{{sliderAlign}}; } ',
			},
		],
	},
	showImage: {
		type: 'boolean',
		default: true,
	},
	showTitle: {
		type: 'boolean',
		default: true,
	},
	showTaxonomy: {
		type: 'boolean',
		default: true,
	},
	showDescription: {
		type: 'boolean',
		default: true,
	},
	showPrice: {
		type: 'boolean',
		default: true,
	},
	showCart: {
		type: 'boolean',
		default: true,
	},
	showArrows: {
		type: 'boolean',
		default: true,
	},
	showDots: {
		type: 'boolean',
		default: true,
	},

	//Query Style
	queryType: { type: 'string', default: 'product' },
	queryNumber: { type: 'string', default: 8 },
	queryStatus: {
		type: 'string',
		default: 'all',
		style: [
			{
				depends: [
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryExcludeStock: {
		type: 'boolean',
		default: false,
		style: [
			{
				depends: [
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryCat: {
		type: 'string',
		default: '[]',
		style: [
			{
				depends: [
					{ key: 'filterShow', condition: '!=', value: true },
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryOrderBy: {
		type: 'string',
		default: 'date',
		style: [
			{
				depends: [
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryOrder: {
		type: 'string',
		default: 'desc',
		style: [
			{
				depends: [
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryInclude: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryExclude: {
		type: 'string',
		default: '[]',
		style: [
			{
				depends: [
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryOffset: {
		type: 'string',
		default: '0',
		style: [
			{
				depends: [
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryQuick: { type: 'string', default: '' },
	queryProductSort: { type: 'string', default: 'null' },
	querySpecificProduct: {
		type: 'string',
		default: '[]',
		style: [
			{
				depends: [
					{
						key: 'queryProductSort',
						condition: '==',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryAdvanceProductSort: {
		type: 'string',
		default: 'null',
		style: [
			{
				depends: [
					{ key: 'queryProductSort', condition: '==', value: 'null' },
				],
			},
		],
	},
	queryTax: {
		type: 'string',
		default: 'product_cat',
		style: [
			{
				depends: [
					{
						key: 'queryType',
						condition: '!=',
						value: [ 'customPosts', 'posts', 'archiveBuilder' ],
					},
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryTaxValue: {
		type: 'string',
		default: '[]',
		style: [
			{
				depends: [
					{ key: 'queryTax', condition: '!=', value: '' },
					{
						key: 'queryType',
						condition: '!=',
						value: [ 'customPosts', 'posts', 'archiveBuilder' ],
					},
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryRelation: {
		type: 'string',
		default: 'OR',
		style: [
			{
				depends: [
					{ key: 'queryTaxValue', condition: '!=', value: '[]' },
					{
						key: 'queryType',
						condition: '!=',
						value: [ 'customPosts', 'posts', 'archiveBuilder' ],
					},
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryIncludeAuthor: {
		type: 'string',
		default: '[]',
		style: [
			{
				depends: [
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryExcludeAuthor: {
		type: 'string',
		default: '[]',
		style: [
			{
				depends: [
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},
	queryStockStatus: {
		type: 'string',
		default: '[]',
		style: [
			{
				depends: [
					{
						key: 'queryProductSort',
						condition: '!=',
						value: 'choose_specific',
					},
				],
			},
		],
	},

	//Background Style
	bgColor: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#1E1E1E' },
		style: [
			{ selector: '{{WOPB}} .wopb-block-wrapper .wopb-slider-section' },
		],
	},
	margin: {
		type: 'object',
		default: {
			lg: { left: 0, right: 0, top: 0, bottom: 0, unit: 'px' },
			xs: { left: 0, right: 0, top: 0, bottom: 0, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-slider-section .wopb-slide-wrap { margin:{{margin}}; }',
			},
		],
	},
	padding: {
		type: 'object',
		default: {
			lg: {
				left: '32',
				right: '32',
				top: '32',
				bottom: '32',
				unit: 'px',
			},
			xs: { left: '', right: '', top: '', bottom: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-slider-section .wopb-slide-wrap { padding:{{padding}}; }',
			},
		],
	},
	bgOverlay: {
		type: 'boolean',
		default: true,
	},
	border: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#dfb0a8',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-block-wrapper .wopb-slider-section',
			},
		],
	},
	radius: {
		type: 'object',
		default: { lg: '2', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-slider-section { border-radius:{{radius}}; }',
			},
		],
	},
	boxshadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-block-wrapper .wopb-slider-section',
			},
		],
	},
	hoverBgOverlay: {
		type: 'boolean',
		default: true,
	},
	hoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#ff176b',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-slider-section:hover',
			},
		],
	},
	hoverRadius: {
		type: 'object',
		default: { lg: '2', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-slider-section:hover { border-radius:{{hoverRadius}}; }',
			},
		],
	},
	hoverBoxshadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-slider-section:hover',
			},
		],
	},

	//Content Style
	contentWidth: {
		type: 'object',
		default: { lg: '480', sm: '300', xs: '250', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper .wopb-slider-section .wopb-content-section { max-width:{{contentWidth}}; }',
			},
		],
	},
	contentAlign: {
		type: 'object',
		default: { lg: 'left' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-item .wopb-content-section { text-align:{{contentAlign}}; } ' +
					'{{WOPB}} .wopb-block-item .wopb-product-taxonomies, {{WOPB}} .wopb-product-price-section .wopb-product-price, {{WOPB}} .wopb-block-item .wopb-product-cart-section form.cart { justify-content:{{contentAlign}}; }',
			},
		],
	},
	contentPadding: {
		type: 'object',
		default: { lg: { unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-item .wopb-content-section { padding:{{contentPadding}}; }',
			},
		],
	},
	//Taxonomy Style
	taxonomyUnderTitle: {
		type: 'boolean',
		default: false,
		style: [
			{
				depends: [
					{ key: 'showTaxonomy', condition: '==', value: true },
				],
			},
		],
	},
	taxonomyType: {
		type: 'string',
		default: 'product_cat',
		style: [
			{
				depends: [
					{ key: 'showTaxonomy', condition: '==', value: true },
				],
			},
		],
	},
	taxonomySpacing: {
		type: 'object',
		default: {
			lg: 24,
			unit: 'px',
		},
		style: [
			{
				depends: [
					{ key: 'showTaxonomy', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-taxonomies { margin-bottom:{{taxonomySpacing}}; }',
			},
		],
	},
	taxonomyItemSpacing: {
		type: 'object',
		default: { lg: 10, xs: 5, unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showTaxonomy', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-content-section .wopb-product-taxonomies { gap:{{taxonomyItemSpacing}}; }',
			},
		],
	},
	taxonomyTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: 16, sm: 14, xs: 10, unit: 'px' },
			height: { lg: 20, unit: 'px' },
			decoration: 'none',
			family: '',
			weight: '',
			spacing: '',
			transform: 'uppercase',
		},
		style: [
			{
				depends: [
					{ key: 'showTaxonomy', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-product-taxonomies .wopb-taxonomy',
			},
		],
	},
	taxonomyPadding: {
		type: 'object',
		default: {
			lg: { top: '8', bottom: '8', left: '24', right: '24', unit: 'px' },
			xs: { top: '3', bottom: '3', left: '7', right: '7', unit: 'px' },
		},
		style: [
			{
				depends: [
					{ key: 'showTaxonomy', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-taxonomies .wopb-taxonomy { padding:{{taxonomyPadding}}; }',
			},
		],
	},
	taxonomyColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [
					{ key: 'showTaxonomy', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-taxonomies .wopb-taxonomy { color:{{taxonomyColor}}; }',
			},
		],
	},
	taxonomyBgColor: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '' },
		style: [
			{
				depends: [
					{ key: 'showTaxonomy', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-product-taxonomies .wopb-taxonomy',
			},
		],
	},
	taxonomyBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#fff',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{ key: 'showTaxonomy', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-product-taxonomies .wopb-taxonomy',
			},
		],
	},
	taxonomyRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showTaxonomy', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-taxonomies .wopb-taxonomy { border-radius:{{taxonomyRadius}}; }',
			},
		],
	},
	taxonomyHoverColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				depends: [
					{ key: 'showTaxonomy', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-taxonomies .wopb-taxonomy:hover { color:{{taxonomyHoverColor}}; }',
			},
		],
	},
	taxonomyHoverBgColor: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#fff' },
		style: [
			{
				depends: [
					{ key: 'showTaxonomy', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-taxonomies .wopb-taxonomy:hover',
			},
		],
	},
	taxonomyHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#dfb0a8',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{ key: 'showTaxonomy', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-taxonomies .wopb-taxonomy:hover',
			},
		],
	},
	taxonomyHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showTaxonomy', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-taxonomies .wopb-taxonomy:hover { border-radius:{{taxonomyHoverRadius}}; }',
			},
		],
	},

	//Title style
	titleTag: {
		type: 'string',
		default: 'h3',
		style: [
			{
				depends: [ { key: 'showTitle', condition: '==', value: true } ],
			},
		],
	},
	titleLength: { type: 'string', default: 0 },
	titleWidth: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showTitle', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-title { max-width: {{titleWidth}}; }',
			},
		],
	},
	titleTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: 40, sm: 25, xs: 16, unit: 'px' },
			height: { lg: 49, sm: 30, xs: 21, unit: 'px' },
			decoration: 'none',
			family: '',
			weight: 600,
			transform: 'capitalize',
		},
		style: [
			{
				depends: [ { key: 'showTitle', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-title, .editor-styles-wrapper {{WOPB}} .wopb-product-title',
			},
		],
	},
	titleColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [ { key: 'showTitle', condition: '==', value: true } ],
				selector:
					'.editor-styles-wrapper {{WOPB}} .wopb-product-title, {{WOPB}} .wopb-product-title { color:{{titleColor}}; }',
			},
		],
	},
	titleHoverColor: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [ { key: 'showTitle', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-title:hover { color:{{titleHoverColor}}; }',
			},
		],
	},
	titleSpacing: {
		type: 'object',
		default: {
			lg: 24,
			xs: 14,
			unit: 'px',
		},
		style: [
			{
				depends: [ { key: 'showTitle', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-title { margin-bottom:{{titleSpacing}} !important;  margin-top: unset !important; }',
			},
		],
	},
	titleHoverEffect: {
		type: 'string',
		default: 'none',
	},
	titleAnimationColor: {
		type: 'string',
		default: 'black',
		style: [
			{
				depends: [
					{
						key: 'titleHoverEffect',
						condition: '==',
						value: 'style1',
					},
				],
				selector:
					'{{WOPB}} .wopb-title-style1 a {  cursor: pointer; text-decoration: none; display: inline; padding-bottom: 2px; transition: all 0.35s linear !important; background: linear-gradient( to bottom, {{titleAnimationColor}} 0%, {{titleAnimationColor}} 98% );  background-size: 0px 2px; background-repeat: no-repeat; background-position: left 100%; }',
			},
			{
				depends: [
					{
						key: 'titleHoverEffect',
						condition: '==',
						value: 'style2',
					},
				],
				selector:
					"{{WOPB}} .wopb-title-style2 a:hover {  border-bottom:none; padding-bottom: 2px; background-position:0 100%; background-repeat: repeat; background-image: url(\"data:image/svg+xml;charset=utf8,%3Csvg id='squiggle-link' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:ev='http://www.w3.org/2001/xml-events' viewBox='0 0 10 18'%3E%3Cstyle type='text/css'%3E.squiggle%7Banimation:shift .5s linear infinite;%7D@keyframes shift %7Bfrom %7Btransform:translateX(-10px);%7Dto %7Btransform:translateX(0);%7D%7D%3C/style%3E%3Cpath fill='none' stroke='{{titleAnimationColor}}' stroke-width='1' class='squiggle' d='M0,17.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5' /%3E%3C/svg%3E\"); }",
			},
			{
				depends: [
					{
						key: 'titleHoverEffect',
						condition: '==',
						value: 'style3',
					},
				],
				selector:
					"{{WOPB}} .wopb-title-style3 a {  text-decoration: none; $thetransition: all 1s cubic-bezier(1,.25,0,.75) 0s; position: relative; transition: all 0.35s ease-out; padding-bottom: 3px; border-bottom:none; padding-bottom: 2px; background-position:0 100%; background-repeat: repeat; background-image: url(\"data:image/svg+xml;charset=utf8,%3Csvg id='squiggle-link' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:ev='http://www.w3.org/2001/xml-events' viewBox='0 0 10 18'%3E%3Cstyle type='text/css'%3E.squiggle%7Banimation:shift .5s linear infinite;%7D@keyframes shift %7Bfrom %7Btransform:translateX(-10px);%7Dto %7Btransform:translateX(0);%7D%7D%3C/style%3E%3Cpath fill='none' stroke='{{titleAnimationColor}}' stroke-width='1' class='squiggle' d='M0,17.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5' /%3E%3C/svg%3E\"); }",
			},
			{
				depends: [
					{
						key: 'titleHoverEffect',
						condition: '==',
						value: 'style4',
					},
				],
				selector:
					'{{WOPB}} .wopb-title-style4 a { cursor: pointer; font-weight: 600; text-decoration: none; display: inline; padding-bottom: 2px; transition: all 0.3s linear !important; background: linear-gradient( to bottom, {{titleAnimationColor}} 0%, {{titleAnimationColor}} 98% );  background-size: 100% 2px; background-repeat: no-repeat; background-position: left 100%; }',
			},
			{
				depends: [
					{
						key: 'titleHoverEffect',
						condition: '==',
						value: 'style5',
					},
				],
				selector:
					"{{WOPB}} .wopb-title-style5 a:hover{ text-decoration: none; transition: all 0.35s ease-out; border-bottom:none; padding-bottom: 2px; background-position:0 100%; background-repeat: repeat; background-size:auto; background-image: url(\"data:image/svg+xml;charset=utf8,%3Csvg id='squiggle-link' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:ev='http://www.w3.org/2001/xml-events' viewBox='0 0 10 18'%3E%3Cstyle type='text/css'%3E.squiggle%7Banimation:shift .5s linear infinite;%7D@keyframes shift %7Bfrom %7Btransform:translateX(-10px);%7Dto %7Btransform:translateX(0);%7D%7D%3C/style%3E%3Cpath fill='none' stroke='{{titleAnimationColor}}' stroke-width='1' class='squiggle' d='M0,17.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5 c 2.5,0,2.5,-1.5,5,-1.5 s 2.5,1.5,5,1.5' /%3E%3C/svg%3E\"); } ",
			},
			{
				depends: [
					{
						key: 'titleHoverEffect',
						condition: '==',
						value: 'style5',
					},
				],
				selector:
					'{{WOPB}} .wopb-title-style5 a { cursor: pointer; text-decoration: none; display: inline; padding-bottom: 2px; transition: all 0.3s linear !important; background: linear-gradient( to bottom, {{titleAnimationColor}} 0%, {{titleAnimationColor}} 98% );  background-size: 100% 2px; background-repeat: no-repeat; background-position: left 100%; }',
			},
			{
				depends: [
					{
						key: 'titleHoverEffect',
						condition: '==',
						value: 'style6',
					},
				],
				selector:
					'{{WOPB}} .wopb-title-style6 a{ background-image: linear-gradient(120deg, {{titleAnimationColor}} 0%, {{titleAnimationColor}} 100%); background-repeat: no-repeat; background-size: 100% 2px; background-position: 0 88%; transition: background-size 0.15s ease-in; padding: 5px 5px; }',
			},
			{
				depends: [
					{
						key: 'titleHoverEffect',
						condition: '==',
						value: 'style7',
					},
				],
				selector:
					'{{WOPB}} .wopb-title-style7 a { cursor: pointer; text-decoration: none; display: inline; padding-bottom: 2px; transition: all 0.3s linear !important; background: linear-gradient( to bottom, {{titleAnimationColor}} 0%, {{titleAnimationColor}} 98% );  background-size: 0px 2px; background-repeat: no-repeat; background-position: right 100%; } ',
			},
			{
				depends: [
					{
						key: 'titleHoverEffect',
						condition: '==',
						value: 'style8',
					},
				],
				selector:
					'{{WOPB}} .wopb-title-style8 a { cursor: pointer; text-decoration: none; display: inline; padding-bottom: 2px; transition: all 0.3s linear !important; background: linear-gradient( to bottom, {{titleAnimationColor}} 0%, {{titleAnimationColor}} 98% );  background-size: 0px 2px; background-repeat: no-repeat; background-position: center 100%; } ',
			},
			{
				depends: [
					{
						key: 'titleHoverEffect',
						condition: '==',
						value: 'style9',
					},
				],
				selector:
					'{{WOPB}} .wopb-title-style9 a { background-image: linear-gradient(120deg,{{titleAnimationColor}} 0%, {{titleAnimationColor}} 100%); background-repeat: no-repeat; background-size: 100% 10px; background-position: 0 88%; transition: 0.3s ease-in; padding: 3px 5px; } ',
			},
		],
	},

	//Description Style
	descriptionWidth: {
		type: 'object',
		default: { lg: '', xs: '', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showDescription', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-excerpt { max-width: {{descriptionWidth}}; }',
			},
		],
	},
	descriptionLimit: {
		type: 'string',
		default: 190,
	},
	descriptionTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: 14, sm: 12, xs: 12, unit: 'px' },
			height: { lg: 22, sm: 19, xs: 17, unit: 'px' },
			decoration: 'none',
			family: '',
			weight: '400',
		},
		style: [
			{
				depends: [
					{ key: 'showDescription', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-product-excerpt',
			},
		],
	},
	descriptionColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [
					{ key: 'showDescription', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-excerpt { color:{{descriptionColor}}; }',
			},
		],
	},
	descriptionHoverColor: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [
					{ key: 'showDescription', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-excerpt:hover { color:{{descriptionHoverColor}}; }',
			},
		],
	},
	descriptionSpacing: {
		type: 'object',
		default: {
			lg: 32,
			sm: 20,
			xs: 16,
			unit: 'px',
		},
		style: [
			{
				depends: [
					{ key: 'showDescription', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-excerpt { margin-bottom:{{descriptionSpacing}}; }',
			},
		],
	},

	//Price Style
	priceOverDescription: {
		type: 'boolean',
		default: false,
		style: [
			{
				depends: [ { key: 'showPrice', condition: '==', value: true } ],
			},
		],
	},
	priceTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: 20, sm: 15, xs: 12, unit: 'px' },
			height: { lg: 28, sm: 20, xs: 17, unit: 'px' },
			decoration: 'none',
			family: '',
			weight: 400,
		},
		style: [
			{
				depends: [ { key: 'showPrice', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-price-section .wopb-product-price',
			},
		],
	},
	salePriceColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [ { key: 'showPrice', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-price-section .wopb-product-price ins { color:{{salePriceColor}}; }' +
					'{{WOPB}} .wopb-product-price-section .wopb-product-price ins bdi { color:{{salePriceColor}}; }',
			},
		],
	},
	regularPriceColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [ { key: 'showPrice', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-price-section .wopb-product-price :not(del) > .woocommerce-Price-amount, {{WOPB}} .wopb-product-price-section .wopb-product-price :not(ins) > .woocommerce-Price-amount { color:{{regularPriceColor}}; }' +
					'{{WOPB}} .wopb-product-price-section .wopb-grouped-price,{{WOPB}} .wopb-product-price-section .wopb-variable-price { color:{{regularPriceColor}}; }' +
					'{{WOPB}} .wopb-product-price-section .wopb-product-price del { color:{{regularPriceColor}}; }' +
					'{{WOPB}} .wopb-product-price-section .wopb-product-price bdi { color:{{regularPriceColor}}; }' +
					'{{WOPB}} .wopb-product-price-section .wopb-product-price del bdi { color:{{regularPriceColor}}; }',
			},
		],
	},
	priceItemSpacing: {
		type: 'object',
		default: {
			lg: 12,
			unit: 'px',
		},
		style: [
			{
				depends: [ { key: 'showPrice', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-price-section .wopb-product-price .wopb-prices { gap:{{priceItemSpacing}}; }',
			},
		],
	},
	priceSpacing: {
		type: 'object',
		default: {
			lg: 48,
			sm: 25,
			xs: 15,
			unit: 'px',
		},
		style: [
			{
				depends: [ { key: 'showPrice', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-price-section { margin-bottom:{{priceSpacing}}; }',
			},
		],
	},
	showPriceLabel: {
		type: 'boolean',
		default: true,
		style: [
			{
				depends: [ { key: 'showPrice', condition: '==', value: true } ],
			},
		],
	},
	priceLabelText: {
		type: 'string',
		default: 'Price: ',
		style: [
			{
				depends: [
					{ key: 'showPrice', condition: '==', value: true },
					{ key: 'showPriceLabel', condition: '==', value: true },
				],
			},
		],
	},
	priceLabelTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: 22, sm: 15, xs: 12, unit: 'px' },
			height: { lg: 28, sm: 20, xs: 17, unit: 'px' },
			decoration: 'none',
			family: '',
			weight: 500,
		},
		style: [
			{
				depends: [
					{ key: 'showPrice', condition: '==', value: true },
					{ key: 'showPriceLabel', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-price-section .wopb-product-price .wopb-price-label',
			},
		],
	},
	priceLabelColor: {
		type: 'string',
		default: '#A1A1A1',
		style: [
			{
				depends: [
					{ key: 'showPrice', condition: '==', value: true },
					{ key: 'showPriceLabel', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-price-section .wopb-product-price .wopb-price-label { color:{{priceLabelColor}}; }',
			},
		],
	},

	//Add To Cart Style
	showQty: {
		type: 'boolean',
		default: true,
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
			},
		],
	},
	showPlusMinus: {
		type: 'boolean',
		default: true,
		style: [
			{
				depends: [
					{ key: 'showCart', condition: '==', value: true },
					{ key: 'showQty', condition: '==', value: true },
				],
			},
		],
	},
	cartQtyColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [
					{ key: 'showCart', condition: '==', value: true },
					{ key: 'showQty', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-cart-section .quantity > input {color:{{cartQtyColor}};}',
			},
		],
	},
	cartQtyBg: {
		type: 'string',
		default: 'none',
		style: [
			{
				depends: [
					{ key: 'showCart', condition: '==', value: true },
					{ key: 'showQty', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-cart-section .quantity > input {background:{{cartQtyBg}};}',
			},
		],
	},
	cartQtyBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#fff',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{ key: 'showCart', condition: '==', value: true },
					{ key: 'showQty', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-cart-section .quantity > input',
			},
		],
	},
	cartQtyWrapWidth: {
		type: 'string',
		default: 64,
		style: [
			{
				depends: [
					{ key: 'showCart', condition: '==', value: true },
					{ key: 'showQty', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-cart-section form.cart .quantity > input.qty { max-width: {{cartQtyWrapWidth}}px; width: {{cartQtyWrapWidth}}px; }',
			},
		],
	},
	cartQtyRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				depends: [
					{ key: 'showCart', condition: '==', value: true },
					{ key: 'showQty', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-cart-section .quantity input {border-radius:{{cartQtyRadius}};}',
			},
		],
	},
	cartQtyPadding: {
		type: 'object',
		default: {
			lg: { top: 14, bottom: 14, left: 28, right: 0, unit: 'px' },
			sm: { top: 5, bottom: 5, left: 0, right: 0, unit: 'px' },
			xs: { top: 2, bottom: 3, left: 0, right: 0, unit: 'px' },
		},
		style: [
			{
				depends: [
					{ key: 'showCart', condition: '==', value: true },
					{ key: 'showQty', condition: '==', value: true },
				],
				selector:
					"{{WOPB}} .wopb-product-cart-section .quantity input[type='number'] { padding:{{cartQtyPadding}} !important; }",
			},
		],
	},
	plusMinusPosition: {
		type: 'string',
		default: 'right',
		style: [
			{
				depends: [
					{
						key: 'plusMinusPosition',
						condition: '==',
						value: 'left',
					},
					{ key: 'showCart', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-cart-section form.cart .quantity {flex-direction:row-reverse;}',
			},
			{
				depends: [
					{
						key: 'plusMinusPosition',
						condition: '==',
						value: 'right',
					},
					{ key: 'showCart', condition: '==', value: true },
				],
			},
			{
				depends: [
					{
						key: 'plusMinusPosition',
						condition: '==',
						value: 'both',
					},
					{ key: 'showCart', condition: '==', value: true },
				],
			},
		],
	},
	plusMinusColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [
					{ key: 'showCart', condition: '==', value: true },
					{ key: 'showQty', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-cart-section .quantity .wopb-cart-plus > svg , {{WOPB}} .wopb-product-cart-section .quantity .wopb-cart-minus > svg {fill:{{plusMinusColor}};}',
			},
		],
	},
	plusMinusBg: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [
					{ key: 'showCart', condition: '==', value: true },
					{ key: 'showQty', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-cart-section .quantity .wopb-cart-plus , {{WOPB}} .wopb-product-cart-section .quantity .wopb-cart-minus {background:{{plusMinusBg}};}',
			},
		],
	},
	plusMinusHoverColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [
					{ key: 'showCart', condition: '==', value: true },
					{ key: 'showQty', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-cart-section .quantity .wopb-cart-plus:hover svg , {{WOPB}} .wopb-product-cart-section .quantity .wopb-cart-minus:hover svg {fill:{{plusMinusHoverColor}};}',
			},
		],
	},
	plusMinusHoverBg: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [
					{ key: 'showCart', condition: '==', value: true },
					{ key: 'showQty', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-cart-section .quantity .wopb-cart-plus:hover , {{WOPB}} .wopb-product-cart-section .quantity .wopb-cart-minus:hover {background:{{plusMinusHoverBg}};}',
			},
		],
	},
	plusMinusSize: {
		type: 'string',
		default: 10,
		style: [
			{
				depends: [
					{ key: 'showCart', condition: '==', value: true },
					{ key: 'showQty', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-cart-section .quantity .wopb-cart-plus > svg , {{WOPB}} .wopb-product-cart-section .quantity .wopb-cart-minus > svg { height:{{plusMinusSize}}px; width:{{plusMinusSize}}px; }',
			},
		],
	},
	plusMinusGap: {
		type: 'string',
		default: 4,
		style: [
			{
				depends: [
					{ key: 'showCart', condition: '==', value: true },
					{ key: 'showQty', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-cart-section .wopb-cart-plus-minus-icon, {{WOPB}} .wopb-product-cart-section .quantity {gap:{{plusMinusGap}}px; }',
			},
		],
	},
	plusMinusBorder: {
		type: 'object',
		default: {
			openBorder: 0.1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#fff',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{ key: 'showCart', condition: '==', value: true },
					{ key: 'showQty', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-cart-section .quantity .wopb-cart-plus , {{WOPB}} .wopb-product-cart-section .quantity .wopb-cart-minus',
			},
		],
	},
	plusMinusRadius: {
		type: 'object',
		default: { lg: 0, unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showCart', condition: '==', value: true },
					{ key: 'showQty', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-cart-section .quantity .wopb-cart-plus , {{WOPB}} .wopb-product-cart-section .quantity .wopb-cart-minus {border-radius:{{plusMinusRadius}}; }',
			},
		],
	},
	plusMinusPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: 10, right: 10, unit: 'px' },
		},
		style: [
			{
				depends: [
					{ key: 'showCart', condition: '==', value: true },
					{ key: 'showQty', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-product-cart-section .quantity .wopb-cart-plus , {{WOPB}} .wopb-product-cart-section .quantity .wopb-cart-minus {padding:{{plusMinusPadding}};}',
			},
		],
	},
	cartText: {
		type: 'string',
		default: 'Add To Cart',
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
			},
		],
	},
	cartSpacing: {
		type: 'object',
		default: { lg: 3, unit: 'px' },
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-cart-section { margin-top:{{cartSpacing}}; }',
			},
		],
	},
	cartTextTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: 16, sm: 14, xs: 12, unit: 'px' },
			height: { lg: 20, xs: 17, unit: 'px' },
			decoration: 'none',
			family: '',
			transform: '',
			weight: 500,
		},
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-cart-section .wopb-product-cart',
			},
		],
	},
	cartButtonPadding: {
		type: 'object',
		default: {
			lg: {
				top: '14',
				bottom: '14',
				left: '24',
				right: '24',
				unit: 'px',
			},
			sm: { top: 4, bottom: 4, left: 15, right: 15, unit: 'px' },
			xs: { top: 6, bottom: 6, left: 8, right: 8, unit: 'px' },
		},
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-cart-section .wopb-product-cart { padding:{{cartButtonPadding}}; }',
			},
		],
	},
	cartTextColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-cart-section .wopb-product-cart { color:{{cartTextColor}}; }',
			},
		],
	},
	cartBgColor: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#fff' },
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-cart-section .wopb-product-cart',
			},
		],
	},
	cartButtonBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 0, right: 0, bottom: 0, left: 0 },
			color: '#070707',
			type: 'solid',
		},
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-cart-section .wopb-product-cart',
			},
		],
	},
	cartButtonRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-cart-section .wopb-product-cart { border-radius:{{cartButtonRadius}}; }',
			},
		],
	},
	cartButtonBoxShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 7, right: 7, bottom: 0, left: 0 },
			color: '#16005D94',
		},
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-cart-section .wopb-product-cart',
			},
		],
	},
	cartTextHoverColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-cart-section .wopb-product-cart:hover { color:{{cartTextHoverColor}}; }',
			},
		],
	},
	cartHoverBgColor: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#FF176B' },
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-cart-section .wopb-product-cart:hover',
			},
		],
	},
	cartButtonHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#dfb0a8',
			type: 'solid',
		},
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-cart-section .wopb-product-cart:hover',
			},
		],
	},
	cartButtonHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-cart-section .wopb-product-cart:hover { border-radius:{{cartButtonHoverRadius}}; }',
			},
		],
	},
	cartButtonHoverBoxShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-product-cart-section .wopb-product-cart:hover',
			},
		],
	},

	//Image Style
	showImgOverlay: {
		type: 'boolean',
		default: false,
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
			},
		],
	},
	imgOverlayBg: {
		type: 'object',
		default: '#57575780',
		style: [
			{
				depends: [
					{ key: 'showImage', condition: '==', value: true },
					{ key: 'showImgOverlay', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-image-section .wopb-product-image .wopb-image-overlay { background:{{imgOverlayBg}}; }',
			},
		],
	},
	imgHeight: {
		type: 'object',
		default: { lg: '464', sm: '300', xs: '', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-image-section .wopb-product-image img, {{WOPB}} .wopb-image-section .wopb-block-image-empty { height: {{imgHeight}}; }',
			},
		],
	},
	imgWidth: {
		type: 'object',
		default: { lg: 448, sm: 300, xs: 300, unit: 'px' },
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-image-section .wopb-product-image { max-width: {{imgWidth}}; }',
			},
		],
	},
	imageScale: {
		type: 'string',
		default: 'cover',
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-image-section .wopb-product-image img {object-fit: {{imageScale}};}',
			},
		],
	},
	imgSectionPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-slide-wrap .wopb-image-section { padding:{{imgSectionPadding}}; }',
			},
		],
	},
	showSaleBadge: {
		type: 'boolean',
		default: true,
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
			},
		],
	},
	imgBorder: {
		type: 'object',
		default: {
			openBorder: 0.0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-image-section .wopb-product-image img',
			},
		],
	},
	imgRadius: {
		type: 'object',
		default: { lg: 0, unit: 'px' },
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-image-section .wopb-product-image img {border-radius:{{imgRadius}}; }' +
					'{{WOPB}} .wopb-image-section .wopb-product-image .wopb-image-overlay {border-radius:{{imgRadius}}; }',
			},
		],
	},
	imgShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-image-section .wopb-product-image img',
			},
		],
	},
	imgHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0.0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-image-section .wopb-product-image img:hover',
			},
		],
	},
	imgHoverRadius: {
		type: 'object',
		default: { lg: 0, unit: 'px' },
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-image-section .wopb-product-image img:hover {border-radius:{{imgHoverRadius}}; }',
			},
		],
	},
	imgHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [ { key: 'showCart', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-image-section .wopb-product-image img:hover',
			},
		],
	},

	//Sale Style

	saleText: { type: 'string', default: 'Sale!' },
	salePosition: {
		type: 'string',
		default: 'topRight',
		style: [
			{
				depends: [
					{ key: 'salePosition', condition: '==', value: 'topLeft' },
				],
				selector: '{{WOPB}} .wopb-onsale-hot{top:15px;left:15px;}',
			},
			{
				depends: [
					{ key: 'salePosition', condition: '==', value: 'topRight' },
				],
				selector: '{{WOPB}} .wopb-onsale-hot{top:15px;right:15px;}',
			},
			{
				depends: [
					{
						key: 'salePosition',
						condition: '==',
						value: 'buttonLeft',
					},
				],
				selector: '{{WOPB}} .wopb-onsale-hot{bottom:15px;left:15px;}',
			},
			{
				depends: [
					{
						key: 'salePosition',
						condition: '==',
						value: 'buttonRight',
					},
				],
				selector: '{{WOPB}} .wopb-onsale-hot{bottom:15px;right:15px;}',
			},
		],
	},
	saleDesign: {
		type: 'string',
		default: 'digit',
		style: [
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
				],
			},
		],
	},
	saleStyle: {
		type: 'string',
		default: 'shape1',
		style: [
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
				],
			},
		],
	},
	shapeSize: {
		type: 'object',
		default: { lg: 60, xs: 40, unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
					{ key: 'saleStyle', condition: '!=', value: 'classic' },
					{ key: 'saleStyle', condition: '!=', value: 'ribbon' },
				],
				selector:
					'{{WOPB}} .wopb-product-image .wopb-sale-shape { height:{{shapeSize}}; }',
			},
		],
	},
	shapeColor: {
		type: 'string',
		default: '#63B646',
		style: [
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
					{ key: 'saleStyle', condition: '!=', value: 'classic' },
					{ key: 'saleStyle', condition: '!=', value: 'ribbon' },
				],
				selector:
					'{{WOPB}} .wopb-product-image .wopb-sale-shape svg { fill:{{shapeColor}}; } {{WOPB}} .wopb-product-image .wopb-sale-shape svg path { stroke:{{shapeColor}}; }',
			},
		],
	},
	salesColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-onsale { color:{{salesColor}}; }',
			},
		],
	},
	salesBgColor: {
		type: 'string',
		default: '#63B646',
		style: [
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
					{ key: 'saleStyle', condition: '==', value: 'classic' },
				],
				selector:
					'{{WOPB}} .wopb-onsale { background:{{salesBgColor}}; } {{WOPB}} .wopb-onsale.wopb-onsale-ribbon:before { border-left: 23px solid {{salesBgColor}}; border-right: 23px solid {{salesBgColor}}; }',
			},
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
					{ key: 'saleStyle', condition: '==', value: 'ribbon' },
				],
				selector:
					'{{WOPB}} .wopb-onsale { background:{{salesBgColor}}; } {{WOPB}} .wopb-onsale.wopb-onsale-ribbon:before { border-left: 23px solid {{salesBgColor}}; border-right: 24px solid {{salesBgColor}}; }',
			},
		],
	},
	salesTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', xs: 14, unit: 'px' },
			spacing: { lg: '0', unit: 'px' },
			height: { lg: '14', xs: 19, unit: 'px' },
			decoration: 'none',
			transform: 'uppercase',
			family: '',
			weight: '500',
		},
		style: [
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-onsale',
			},
		],
	},
	salesPadding: {
		type: 'object',
		default: { lg: { top: 4, bottom: 4, left: 8, right: 8, unit: 'px' } },
		style: [
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
					{ key: 'saleStyle', condition: '==', value: 'classic' },
				],
				selector:
					'{{WOPB}} .wopb-onsale.wopb-onsale-classic { padding:{{salesPadding}}; }',
			},
		],
	},
	salesMargin: {
		type: 'object',
		default: {
			lg: { top: 11, bottom: 0, left: 11, right: 0, unit: 'px' },
			xs: { top: 5, bottom: '', left: 20, right: '', unit: '%' },
		},
		style: [
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-onsale-hot { margin:{{salesMargin}}; }',
			},
		],
	},
	salesRadius: {
		type: 'object',
		default: { lg: '0', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
					{ key: 'saleStyle', condition: '==', value: 'classic' },
				],
				selector:
					'{{WOPB}} .wopb-onsale.wopb-onsale-classic { border-radius:{{salesRadius}}; }',
			},
		],
	},

	//Arrow style
	arrowHideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'@media (min-width:1280px) { ' +
					'{{WOPB}} .wopb-slider-section .slick-arrow {display:none !important;} .block-editor-block-list__block {{WOPB}} .wopb-slider-section .slick-arrow {display:flex !important;}' +
					'}',
			},
		],
	},
	arrowHideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'@media screen and (max-width:1280px) { ' +
					'{{WOPB}} .wopb-slider-section .slick-arrow {display:none !important;} .block-editor-block-list__block {{WOPB}} .wopb-slider-section .slick-arrow {display:flex !important;}' +
					'}',
			},
		],
	},
	arrowHideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'@media screen and (max-width: 992px) { ' +
					'{{WOPB}} .wopb-slider-section .slick-arrow {display:none !important;} .block-editor-block-list__block {{WOPB}} .wopb-slider-section .slick-arrow {display:flex !important;} ' +
					'}',
			},
		],
	},
	arrowHideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'@media screen and (max-width:768px) { ' +
					'{{WOPB}} .wopb-slider-section .slick-arrow {display:none !important;} .block-editor-block-list__block {{WOPB}} .wopb-slider-section .slick-arrow {display:block !important;}' +
					'}',
			},
		],
	},
	arrowStyle: {
		type: 'string',
		default: 'leftAngle2#rightAngle2',
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
			},
		],
	},
	arrowSize: {
		type: 'object',
		default: { lg: '15', sm: '15', xs: 10, unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-next svg, {{WOPB}} .wopb-slider-section .slick-prev svg { width:{{arrowSize}}; }',
			},
		],
	},
	arrowWidth: {
		type: 'object',
		default: { lg: '50', sm: '40', xs: 25, unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-arrow { width:{{arrowWidth}}; }',
			},
		],
	},
	arrowHeight: {
		type: 'object',
		default: { lg: '50', sm: '40', xs: 25, unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-arrow { height:{{arrowHeight}}; } {{WOPB}} .wopb-slider-section .slick-arrow { line-height:{{arrowHeight}}; }',
			},
		],
	},
	arrowVertical: {
		type: 'object',
		default: { lg: '' },
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-next, {{WOPB}} .wopb-slider-section .slick-prev { top:{{arrowVertical}}; }',
			},
		],
	},
	arrowItemSpace: {
		type: 'object',
		default: { lg: -90, sm: -55, xs: -45, unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-next { right:{{arrowItemSpace}}; } {{WOPB}} .wopb-slider-section .slick-prev { left:{{arrowItemSpace}}; }',
			},
		],
	},
	arrowColor: {
		type: 'string',
		default: '#ffffff',
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-arrow:before { color:{{arrowColor}}; } {{WOPB}} .wopb-slider-section .slick-arrow svg { fill:{{arrowColor}}; }',
			},
		],
	},
	arrowHoverColor: {
		type: 'string',
		default: '#000',
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-arrow:hover:before { color:{{arrowHoverColor}}; } {{WOPB}} .wopb-slider-section .slick-arrow:hover svg { fill:{{arrowHoverColor}}; }',
			},
		],
	},
	arrowBg: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-arrow { background:{{arrowBg}}; }',
			},
		],
	},
	arrowHoverBg: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-arrow:hover { background:{{arrowHoverBg}}; }',
			},
		],
	},
	arrowBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#fff',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-slider-section .slick-arrow',
			},
		],
	},
	arrowHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: '', right: '', bottom: '', left: '' },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-slider-section .slick-arrow:hover',
			},
		],
	},
	arrowRadius: {
		type: 'object',
		default: {
			lg: {
				top: '50',
				bottom: '50',
				left: '50',
				right: '50',
				unit: 'px',
			},
		},
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-arrow { border-radius: {{arrowRadius}}; }',
			},
		],
	},
	arrowHoverRadius: {
		type: 'object',
		default: {
			lg: {
				top: '50',
				bottom: '50',
				left: '50',
				right: '50',
				unit: 'px',
			},
		},
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-arrow:hover{ border-radius: {{arrowHoverRadius}}; }',
			},
		],
	},
	arrowShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 0, right: 0, bottom: 0, left: 0 },
			color: '',
		},
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-slider-section .slick-arrow',
			},
		],
	},
	arrowHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [
					{ key: 'showArrows', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-slider-section .slick-arrow:hover',
			},
		],
	},

	//Dot Style
	dotSpace: {
		type: 'object',
		default: { lg: '12', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-dots { padding: 0 {{dotSpace}}; } {{WOPB}} .wopb-slider-section .slick-dots li { margin: 0 {{dotSpace}}; }',
			},
		],
	},
	dotVertical: {
		type: 'object',
		default: { lg: '-40', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-dots { bottom:{{dotVertical}}; }',
			},
		],
	},
	dotHorizontal: {
		type: 'object',
		default: { lg: '' },
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-dots { left:{{dotHorizontal}}; }',
			},
		],
	},
	dotWidth: {
		type: 'object',
		default: { lg: '10', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-dots li button { width:{{dotWidth}}; }',
			},
		],
	},
	dotHeight: {
		type: 'object',
		default: { lg: '10', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-dots li button { height:{{dotHeight}}; }',
			},
		],
	},
	dotHoverWidth: {
		type: 'object',
		default: { lg: '32', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-dots li.slick-active button { width:{{dotHoverWidth}}; }',
			},
		],
	},
	dotHoverHeight: {
		type: 'object',
		default: { lg: '10', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-dots li.slick-active button { height:{{dotHoverHeight}}; }',
			},
		],
	},
	dotBg: {
		type: 'string',
		default: '#f5f5f5',
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-dots li:not(.slick-active) button { background:{{dotBg}}; }',
			},
		],
	},
	dotHoverBg: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-dots li button:hover, {{WOPB}} .wopb-slider-section .slick-dots li.slick-active button { background:{{dotHoverBg}}; }',
			},
		],
	},
	dotBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector: '{{WOPB}} .wopb-slider-section .slick-dots li button',
			},
		],
	},
	dotHoverBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#fff',
			type: 'solid',
		},
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-dots li button:hover, {{WOPB}} .wopb-slider-section .slick-dots li.slick-active button',
			},
		],
	},
	dotRadius: {
		type: 'object',
		default: {
			lg: {
				top: '50',
				bottom: '50',
				left: '50',
				right: '50',
				unit: 'px',
			},
		},
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-dots li button { border-radius: {{dotRadius}}; }',
			},
		],
	},
	dotHoverRadius: {
		type: 'object',
		default: {
			lg: {
				top: '50',
				bottom: '50',
				left: '50',
				right: '50',
				unit: 'px',
			},
		},
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-dots li button:hover, {{WOPB}} .wopb-slider-section .slick-dots li.slick-active button { border-radius: {{dotHoverRadius}}; }',
			},
		],
	},
	dotShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector: '{{WOPB}} .wopb-slider-section .slick-dots li button',
			},
		],
	},
	dotHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [ { key: 'showDots', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-slider-section .slick-dots li button:hover, {{WOPB}} .wopb-slider-section .slick-dots li.slick-active button',
			},
		],
	},

	//Wrapper Style
	wrapBg: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#070707' },
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#ff176b' },
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapMargin: {
		type: 'object',
		default: { lg: { top: '', bottom: '', unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: { left: 72, right: 72, top: 72, bottom: 72, unit: 'px' },
			xs: { left: 50, right: 50, top: 50, bottom: 50, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: { type: 'string', default: '', style: [ { selector: '' } ] },
};

export default attributes;
