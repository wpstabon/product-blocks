import { SalesStyleArg } from '../../helper/CommonPanel';

const { __ } = wp.i18n;

const toolbarHOF = ( settings ) =>
	settings.map( ( sett ) => ( { ...sett.data } ) );

export const tax_settings = {
	name: 'taxonomy',
	title: __( 'Taxonomy', 'product-blocks' ),
	options: [
		{
			type: 'toggle',
			key: 'taxonomyUnderTitle',
			label: __( 'Taxonomy Under Title', 'product-blocks' ),
		},
		{
			type: 'select',
			key: 'taxonomyType',
			label: __( 'Taxonomy Type', 'product-blocks' ),
			options: [
				{
					value: 'product_cat',
					label: __( 'Product Category', 'product-blocks' ),
				},
				{
					value: 'product_tag',
					label: __( 'Product Tag', 'product-blocks' ),
				},
			],
		},
		{
			type: 'range',
			key: 'taxonomySpacing',
			min: 1,
			max: 100,
			step: 1,
			unit: true,
			responsive: true,
			label: __( 'Taxonomy Spacing', 'product-blocks' ),
		},
		{
			type: 'range',
			key: 'taxonomyItemSpacing',
			min: 1,
			max: 400,
			step: 1,
			unit: true,
			responsive: true,
			label: __( 'Spacing Between Items', 'product-blocks' ),
		},
		{
			type: 'typography',
			key: 'taxonomyTypo',
			label: __( 'Typography', 'product-blocks' ),
		},
		{
			type: 'dimension',
			key: 'taxonomyPadding',
			label: __( 'Padding', 'product-blocks' ),
			step: 1,
			unit: true,
			responsive: true,
		},
		{
			type: 'tab',
			content: [
				{
					name: 'normal',
					title: __( 'Normal', 'product-blocks' ),
					options: [
						{
							type: 'color',
							key: 'taxonomyColor',
							label: __( 'Color', 'product-blocks' ),
						},
						{
							type: 'color2',
							key: 'taxonomyBgColor',
							label: __( 'Background Color', 'product-blocks' ),
						},
						{
							type: 'border',
							key: 'taxonomyBorder',
							label: __( 'Border', 'product-blocks' ),
						},
						{
							type: 'dimension',
							key: 'taxonomyRadius',
							label: __( 'Border Radius', 'product-blocks' ),
							step: 1,
							unit: true,
							responsive: true,
						},
					],
				},
				{
					name: 'hover',
					title: __( 'Hover', 'product-blocks' ),
					options: [
						{
							type: 'color',
							key: 'taxonomyHoverColor',
							label: __( 'Hover Color', 'product-blocks' ),
						},
						{
							type: 'color2',
							key: 'taxonomyHoverBgColor',
							label: __( 'Hover Bg Color', 'product-blocks' ),
						},
						{
							type: 'border',
							key: 'taxonomyHoverBorder',
							label: __( 'Hover Border', 'product-blocks' ),
						},
						{
							type: 'dimension',
							key: 'taxonomyHoverRadius',
							label: __( 'Hover Radius', 'product-blocks' ),
							step: 1,
							unit: true,
							responsive: true,
						},
					],
				},
			],
		},
	],
};

export const title_settings = {
	name: 'title',
	title: __( 'Title', 'product-blocks' ),
	options: [
		{
			type: 'tag',
			key: 'titleTag',
			label: __( 'Title Tag', 'product-blocks' ),
		},
		{
			type: 'range',
			key: 'titleLength',
			min: 0,
			max: 40,
			step: 1,
			label: __( 'Title Max Length', 'product-blocks' ),
		},
		{
			type: 'range',
			key: 'titleWidth',
			min: 1,
			max: 1200,
			step: 1,
			unit: true,
			responsive: true,
			label: __( 'Title Width', 'product-blocks' ),
		},
		{
			type: 'typography',
			key: 'titleTypo',
			label: __( 'Typography', 'product-blocks' ),
		},
		{
			type: 'color',
			key: 'titleColor',
			label: __( 'Title Color', 'product-blocks' ),
		},
		{
			type: 'color',
			key: 'titleHoverColor',
			label: __( 'Title Hover Color', 'product-blocks' ),
		},
		{
			type: 'range',
			key: 'titleSpacing',
			min: 0,
			max: 400,
			step: 1,
			unit: true,
			responsive: true,
			label: __( 'Title Spacing', 'product-blocks' ),
		},
		{
			type: 'select',
			key: 'titleHoverEffect',
			label: __( 'Title Hover Effects', 'product-blocks' ),
			options: [
				{ value: 'none', label: __( 'None', 'product-blocks' ) },
				{
					value: 'style1',
					label: __( 'On Hover Underline', 'product-blocks' ),
					pro: true,
				},
				{
					value: 'style2',
					label: __( 'On Hover Wave', 'product-blocks' ),
					pro: true,
				},
				{
					value: 'style3',
					label: __( 'Wave', 'product-blocks' ),
					pro: true,
				},
				{
					value: 'style4',
					label: __( 'underline', 'product-blocks' ),
					pro: true,
				},
				{
					value: 'style5',
					label: __( 'underline and wave', 'product-blocks' ),
					pro: true,
				},
				{
					value: 'style6',
					label: __( 'Underline Background', 'product-blocks' ),
					pro: true,
				},
				{
					value: 'style7',
					label: __( 'Underline Right To Left', 'product-blocks' ),
					pro: true,
				},
				{
					value: 'style8',
					label: __( 'Underline center', 'product-blocks' ),
					pro: true,
				},
				{
					value: 'style9',
					label: __( 'Underline Background 2', 'product-blocks' ),
					pro: true,
				},
				{
					value: 'style10',
					label: __( 'Underline Hover Spacing', 'product-blocks' ),
					pro: true,
				},
				{
					value: 'style11',
					label: __( 'Hover word spacing', 'product-blocks' ),
					pro: true,
				},
			],
		},
		{
			type: 'color',
			key: 'titleAnimationColor',
			label: __( 'Animation Color', 'product-blocks' ),
		},
	],
};

export const desc_settings = {
	name: 'description',
	title: __( 'Description', 'product-blocks' ),
	options: [
		{
			type: 'range',
			key: 'descriptionWidth',
			min: 0,
			max: 1200,
			step: 1,
			unit: true,
			responsive: true,
			label: __( 'Description Width', 'product-blocks' ),
		},
		{
			type: 'range',
			key: 'descriptionLimit',
			min: 0,
			max: 800,
			step: 1,
			label: __( 'Description Limit', 'product-blocks' ),
		},
		{
			type: 'range',
			key: 'descriptionSpacing',
			min: 0,
			max: 400,
			step: 1,
			unit: true,
			responsive: true,
			label: __( 'Description Spacing', 'product-blocks' ),
		},
		{
			type: 'typography',
			key: 'descriptionTypo',
			label: __( 'Typography', 'product-blocks' ),
		},
		{
			type: 'color',
			key: 'descriptionColor',
			label: __( 'Description Color', 'product-blocks' ),
		},
		{
			type: 'color',
			key: 'descriptionHoverColor',
			label: __( 'Description Hover Color', 'product-blocks' ),
		},
	],
};

export const price_settings = {
	name: 'price',
	title: __( 'Price', 'product-blocks' ),
	options: [
		{
			type: 'toggle',
			key: 'priceOverDescription',
			label: __( 'Price Over Description', 'product-blocks' ),
		},
		{
			type: 'typography',
			key: 'priceTypo',
			label: __( 'Typography', 'product-blocks' ),
		},
		{
			type: 'color',
			key: 'salePriceColor',
			label: __( 'Sale Price Color', 'product-blocks' ),
		},
		{
			type: 'color',
			key: 'regularPriceColor',
			label: __( 'Regular Price Color', 'product-blocks' ),
		},
		{
			type: 'range',
			key: 'priceItemSpacing',
			min: 0,
			max: 100,
			step: 1,
			unit: true,
			responsive: true,
			label: __( 'Space Between Sale & Regular Price', 'product-blocks' ),
		},
		{
			type: 'range',
			key: 'priceSpacing',
			min: 0,
			max: 100,
			step: 1,
			unit: true,
			responsive: true,
			label: __( 'Price Spacing', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'showPriceLabel',
			label: __( 'Show Price Label', 'product-blocks' ),
		},
		{
			type: 'text',
			key: 'priceLabelText',
			label: __( 'Price Label Text', 'product-blocks' ),
		},
		{
			type: 'typography',
			key: 'priceLabelTypo',
			label: __( 'Price Label Typography', 'product-blocks' ),
		},
		{
			type: 'color',
			key: 'priceLabelColor',
			label: __( 'Price Label Color', 'product-blocks' ),
		},
	],
};

export const bg_settings = [
	{
		data: {
			type: 'color2',
			key: 'wrapBg',
			label: __( 'Wrapper Background', 'product-blocks' ),
		},
	},
	{
		data: {
			type: 'dimension',
			key: 'wrapOuterPadding',
			label: __( 'Wrapper Padding', 'product-blocks' ),
			step: 1,
			unit: true,
			responsive: true,
		},
	},
	{
		data: {
			type: 'separator',
			key: 'containerSeparator',
			label: __( 'Container Style', 'product-blocks' ),
		},
	},
	{
		data: {
			type: 'color2',
			key: 'bgColor',
			label: __( 'Background Color', 'product-blocks' ),
		},
	},
	{
		data: {
			type: 'dimension',
			key: 'margin',
			label: __( 'Background Margin', 'product-blocks' ),
			step: 1,
			unit: true,
			responsive: true,
		},
	},
	{
		data: {
			type: 'dimension',
			key: 'padding',
			label: __( 'Background Padding', 'product-blocks' ),
			step: 1,
			unit: true,
			responsive: true,
		},
	},
	{
		data: {
			type: 'tab',
			key: 'bgTab',
			content: [
				{
					name: 'normal',
					title: __( 'Normal', 'product-blocks' ),
					options: [
						// { type:'toggle',key:'bgOverlay',label:__('Background Overlay', 'product-blocks') },
						{
							type: 'border',
							key: 'border',
							label: __( 'Border', 'product-blocks' ),
						},
						{
							type: 'dimension',
							key: 'radius',
							label: __( 'Border Radius', 'product-blocks' ),
							step: 1,
							unit: true,
							responsive: true,
						},
						{
							type: 'boxshadow',
							key: 'boxshadow',
							label: __( 'BoxShadow', 'product-blocks' ),
						},
					],
				},
				{
					name: 'hover',
					title: __( 'Hover', 'product-blocks' ),
					options: [
						// { type:'toggle',key:'hoverBgOverlay',label:__('Background Overlay', 'product-blocks') },
						{
							type: 'border',
							key: 'hoverBorder',
							label: __( 'Border', 'product-blocks' ),
						},
						{
							type: 'dimension',
							key: 'hoverRadius',
							label: __( 'Border Radius', 'product-blocks' ),
							step: 1,
							unit: true,
							responsive: true,
						},
						{
							type: 'boxshadow',
							key: 'hoverBoxshadow',
							label: __( 'BoxShadow', 'product-blocks' ),
						},
					],
				},
			],
		},
	},
];

export const bg = toolbarHOF( bg_settings );

export const getContentsSettings = ( contentTabs ) => {
	return [
		{
			data: {
				type: 'range',
				key: 'contentWidth',
				min: 1,
				max: 1500,
				step: 1,
				responsive: true,
				unit: true,
				label: __( 'Width', 'product-blocks' ),
			},
		},
		{
			data: {
				type: 'alignment',
				key: 'contentAlign',
				responsive: true,
				label: __( 'Alignment', 'product-blocks' ),
				disableJustify: true,
			},
		},
		{
			data: {
				type: 'dimension',
				key: 'contentPadding',
				label: __( 'Padding', 'product-blocks' ),
				step: 1,
				unit: true,
				responsive: true,
			},
		},
		{
			data: {
				type: 'tab',
				key: 'cTab',
				content: contentTabs,
			},
		},
	];
};

export const contents = ( contentTabs ) =>
	toolbarHOF( getContentsSettings( contentTabs ) );

export const cart_settings = [
	{
		data: {
			type: 'toggle',
			key: 'showQty',
			label: __( 'Enable Quantity', 'product-blocks' ),
		},
	},
	{
		data: {
			type: 'toggle',
			key: 'showPlusMinus',
			label: __( 'Enable Plus Minus', 'product-blocks' ),
		},
	},
	{
		data: {
			type: 'text',
			key: 'cartText',
			label: __( 'Cart Text', 'product-blocks' ),
		},
	},
	{
		data: {
			type: 'range',
			key: 'cartSpacing',
			min: 0,
			max: 100,
			step: 1,
			unit: true,
			responsive: true,
			label: __( 'Add to Cart Spacing', 'product-blocks' ),
		},
	},
	{
		data: {
			type: 'divider',
			key: 'cartStyleDivider',
			label: __( 'Style', 'product-blocks' ),
		},
	},
	{
		data: {
			type: 'tab',
			key: 'qTab',
			content: [
				{
					name: 'quantity',
					title: __( 'Quantity', 'product-blocks' ),
					options: [
						{
							type: 'color',
							key: 'cartQtyColor',
							label: __( 'Quantity Color', 'product-blocks' ),
						},
						{
							type: 'color',
							key: 'cartQtyBg',
							label: __(
								'Quantity Background',
								'product-blocks'
							),
						},
						{
							type: 'border',
							key: 'cartQtyBorder',
							label: __( 'Border', 'product-blocks' ),
						},
						{
							type: 'range',
							min: 1,
							max: 160,
							key: 'cartQtyWrapWidth',
							label: __( 'Wrapper Width', 'product-blocks' ),
						},
						{
							type: 'dimension',
							key: 'cartQtyRadius',
							label: __( 'Border Radius', 'product-blocks' ),
							step: 1,
							unit: true,
							responsive: true,
						},
						{
							type: 'dimension',
							key: 'cartQtyPadding',
							label: __( 'Quantity Padding', 'product-blocks' ),
							step: 1,
							unit: true,
							responsive: true,
						},
					],
				},
				{
					name: 'plusMinus',
					title: __( 'Plus Minus', 'product-blocks' ),
					options: [
						{
							type: 'select',
							key: 'plusMinusPosition',
							label: 'Quantity Position',
							options: [
								{
									value: 'both',
									label: __( 'Both Side', 'product-blocks' ),
								},
								{
									value: 'right',
									label: __( 'Right Side', 'product-blocks' ),
								},
								{
									value: 'left',
									label: __( 'Left Side', 'product-blocks' ),
								},
							],
						},
						{
							type: 'color',
							key: 'plusMinusColor',
							label: __( 'Icon Color', 'product-blocks' ),
						},
						{
							type: 'color',
							key: 'plusMinusBg',
							label: __( 'Icon Background', 'product-blocks' ),
						},
						{
							type: 'color',
							key: 'plusMinusHoverColor',
							label: __( 'Icon Hover Color', 'product-blocks' ),
						},
						{
							type: 'color',
							key: 'plusMinusHoverBg',
							label: __(
								'Icon Hover Background',
								'product-blocks'
							),
						},
						{
							type: 'range',
							min: 1,
							max: 40,
							key: 'plusMinusSize',
							label: __( 'Icon Size', 'product-blocks' ),
						},
						{
							type: 'range',
							min: 0,
							max: 60,
							key: 'plusMinusGap',
							label: __( 'Icons/Quantity Gap', 'product-blocks' ),
						},
						{
							type: 'border',
							key: 'plusMinusBorder',
							label: __( 'Border', 'product-blocks' ),
						},
						{
							type: 'dimension',
							key: 'plusMinusRadius',
							label: __( 'Border Radius', 'product-blocks' ),
							step: 1,
							unit: true,
							responsive: true,
						},
						{
							type: 'dimension',
							key: 'plusMinusPadding',
							label: __( 'Icon Padding', 'product-blocks' ),
							step: 1,
							unit: true,
							responsive: true,
						},
					],
				},
				{
					name: 'button',
					title: __( 'Button', 'product-blocks' ),
					options: [
						{
							type: 'typography',
							key: 'cartTextTypo',
							label: __( 'Typography', 'product-blocks' ),
						},
						{
							type: 'dimension',
							key: 'cartButtonPadding',
							label: __( 'Padding', 'product-blocks' ),
							step: 1,
							unit: true,
							responsive: true,
						},
						{
							type: 'tab',
							key: 'bTab',
							content: [
								{
									name: 'normal',
									title: __( 'Normal', 'product-blocks' ),
									options: [
										{
											type: 'color',
											key: 'cartTextColor',
											label: __(
												'Text Color',
												'product-blocks'
											),
										},
										{
											type: 'color2',
											key: 'cartBgColor',
											label: __(
												'Background Color',
												'product-blocks'
											),
										},
										{
											type: 'border',
											key: 'cartButtonBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
										{
											type: 'dimension',
											key: 'cartButtonRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
										{
											type: 'boxshadow',
											key: 'cartButtonBoxShadow',
											label: __(
												'Box Shadow',
												'product-blocks'
											),
										},
									],
								},
								{
									name: 'hover',
									title: __( 'Hover', 'product-blocks' ),
									options: [
										{
											type: 'color',
											key: 'cartTextHoverColor',
											label: __(
												'Text Hover Color',
												'product-blocks'
											),
										},
										{
											type: 'color2',
											key: 'cartHoverBgColor',
											label: __(
												'Hover Background Color',
												'product-blocks'
											),
										},
										{
											type: 'border',
											key: 'cartButtonHoverBorder',
											label: __(
												'Hover Border',
												'product-blocks'
											),
										},
										{
											type: 'dimension',
											key: 'cartButtonHoverRadius',
											label: __(
												'Hover Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
										{
											type: 'boxshadow',
											key: 'cartButtonHoverBoxShadow',
											label: __(
												'Hover Box Shadow',
												'product-blocks'
											),
										},
									],
								},
							],
						},
					],
				},
			],
		},
	},
];

export const cart = toolbarHOF( cart_settings );

export const image_settings = [
	{
		data: {
			type: 'toggle',
			key: 'showImgOverlay',
			label: __( 'Image Overlay', 'product-blocks' ),
		},
	},
	{
		data: {
			type: 'color',
			key: 'imgOverlayBg',
			label: __( 'Overlay Background', 'product-blocks' ),
		},
	},
	{
		data: {
			type: 'range',
			key: 'imgHeight',
			min: 0,
			max: 1000,
			step: 1,
			responsive: true,
			unit: [ 'px', 'em', '%' ],
			label: __( 'Height', 'product-blocks' ),
		},
	},
	{
		data: {
			type: 'range',
			key: 'imgWidth',
			min: 0,
			max: 1000,
			step: 1,
			responsive: true,
			unit: [ 'px', 'em', '%' ],
			label: __( 'Width', 'product-blocks' ),
		},
	},
	{
		data: {
			type: 'tag',
			key: 'imageScale',
			label: __( 'Image Scale', 'product-blocks' ),
			options: [
				{
					value: '',
					label: __( 'None', 'product-blocks' ),
				},
				{
					value: 'cover',
					label: __( 'Cover', 'product-blocks' ),
				},
				{
					value: 'contain',
					label: __( 'Contain', 'product-blocks' ),
				},
				{
					value: 'fill',
					label: __( 'Fill', 'product-blocks' ),
				},
				{
					value: 'scale-down',
					label: __( 'Scale Down', 'product-blocks' ),
				},
			],
		},
	},
	{
		data: {
			type: 'dimension',
			key: 'imgSectionPadding',
			label: __( 'Padding', 'product-blocks' ),
			step: 1,
			unit: true,
			responsive: true,
		},
	},
	{
		data: {
			type: 'tab',
			key: 'imgTab',
			content: [
				{
					name: 'Normal',
					title: __( 'Normal', 'product-blocks' ),
					options: [
						{
							type: 'border',
							key: 'imgBorder',
							label: __( 'Border', 'product-blocks' ),
						},
						{
							type: 'dimension',
							key: 'imgRadius',
							label: __( 'Border Radius', 'product-blocks' ),
							step: 1,
							unit: true,
							responsive: true,
						},
						{
							type: 'boxshadow',
							key: 'imgShadow',
							label: __( 'Box Shadow', 'product-blocks' ),
						},
					],
				},
				{
					name: 'hover',
					title: __( 'Hover', 'product-blocks' ),
					options: [
						{
							type: 'border',
							key: 'imgHoverBorder',
							label: __( 'Hover Border', 'product-blocks' ),
						},
						{
							type: 'range',
							key: 'imgHoverGrayScale',
							label: __( 'Hover Gray Scale', 'product-blocks' ),
							min: 0,
							max: 100,
							step: 1,
							unit: [ '%' ],
							responsive: true,
						},
						{
							type: 'dimension',
							key: 'imgHoverRadius',
							label: __( 'Hover Radius', 'product-blocks' ),
							step: 1,
							unit: true,
							responsive: true,
						},
						{
							type: 'boxshadow',
							key: 'imgHoverShadow',
							label: __( 'Hover Box Shadow', 'product-blocks' ),
						},
					],
				},
			],
		},
	},
];

export const image = toolbarHOF( image_settings );

export const sales_settings = [
	{
		position: 3,
		data: {
			type: 'select',
			key: 'saleStyle',
			label: __( 'Sales Style', 'product-blocks' ),
			options: [
				{
					value: 'classic',
					label: __( 'Classic', 'product-blocks' ),
				},
				{
					value: 'ribbon',
					label: __( 'Ribbon', 'product-blocks' ),
				},
				{
					value: 'shape1',
					label: __( 'Shape 1', 'product-blocks' ),
				},
			],
		},
	},
	{
		position: 4,
		data: {
			type: 'range',
			key: 'shapeSize',
			min: 10,
			max: 500,
			step: 1,
			responsive: true,
			unit: true,
			label: __( 'Shape Size', 'product-blocks' ),
		},
	},
	{
		position: 5,
		data: {
			type: 'color',
			key: 'shapeColor',
			label: __( 'Shape Color', 'product-blocks' ),
		},
	},
	{
		data: {
			type: 'dimension',
			key: 'salesMargin',
			label: __( 'Sales Margin', 'product-blocks' ),
			step: 1,
			unit: true,
			responsive: true,
		},
	},
];

export const sales = [ ...SalesStyleArg, ...toolbarHOF( sales_settings ) ];
