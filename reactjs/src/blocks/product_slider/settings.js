/** @format */

const { __ } = wp.i18n;
import {
	ArrowStyle,
	ArrowStyleArg,
	colorIcon,
	CommonSettings,
	CustomCssAdvanced,
	DotStyle,
	DotStyleArg,
	filterFields,
	GeneralAdvanced,
	productSort,
	ResponsiveAdvanced,
	SalesStyle,
	SalesStyleArg,
	settingsIcon,
	spacingIcon,
	styleIcon,
	TypographyTB,
	wopbSupport,
} from '../../helper/CommonPanel';
import LinkGenerator from '../../helper/LinkGenerator';
import { Section, Sections } from '../../helper/Sections';
import TemplateModal from '../../helper/TemplateModal';
import icons from '../../helper/icons';
import WopbToolbarDropdown from '../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar } from '../../helper/ux';
import {
	bg,
	bg_settings,
	cart,
	cart_settings,
	contents,
	desc_settings,
	getContentsSettings,
	image,
	image_settings,
	price_settings,
	sales_settings,
	tax_settings,
	title_settings,
} from './constants';
const { useMemo } = wp.element;

export const features = ( store ) => {
	return [
		{
			type: 'toggle',
			key: 'showTitle',
			label: __( 'Title', 'product-blocks' ),
		},

		{
			type: 'toggle',
			key: 'showImage',
			label: __( 'Image', 'product-blocks' ),
		},

		{
			type: 'toggle',
			key: 'showTaxonomy',
			label: __( 'Taxonomy', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'showDescription',
			label: __( 'Description', 'product-blocks' ),
		},

		{
			type: 'toggle',
			key: 'showArrows',
			label: __( 'Arrows', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'showDots',
			label: __( 'Dots', 'product-blocks' ),
		},

		{
			type: 'toggle',
			key: 'showPrice',
			label: __( 'Price', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'showCart',
			label: __( 'Cart', 'product-blocks' ),
		},
		{
			type: 'toggle',
			key: 'showSaleBadge',
			label: __( 'Sale Tag', 'product-blocks' ),
		},
	];
};

export const defLayout = {
	type: 'layout',
	block: 'product-slider',
	key: 'layout',
	pro: true,
	options: [
		{
			icon: icons.sliderLayout1,
			label: __( 'Layout 1', 'product-blocks' ),
			value: 'layout1',
			pro: false,
		},
		{
			icon: icons.sliderLayout2,
			label: __( 'Layout 2', 'product-blocks' ),
			value: 'layout2',
			pro: false,
		},
		{
			icon: icons.sliderLayout3,
			label: __( 'Layout 3', 'product-blocks' ),
			value: 'layout3',
			pro: false,
		},
	],
	variation: {
		layout1: {
			contentAlign: { lg: 'left' },
			showImgOverlay: false,
			sliderAlign: { lg: 'start' },
		},
		layout2: {
			contentAlign: { lg: 'left' },
			showImgOverlay: false,
			sliderAlign: { lg: 'start' },
		},
		layout3: {
			contentAlign: { lg: 'center' },
			showImgOverlay: true,
			sliderAlign: { lg: 'center' },
		},
	},
};

export default function Settings( { store } ) {
	const {
		showTaxonomy,
		showDots,
		showTitle,
		showDescription,
		showPrice,
		showCart,
		showImage,
		showSaleBadge,
		showArrows,
	} = store.attributes;
	const { section } = store;

	const contentTabs = useMemo( () => {
		const contentTabs = [];
		if ( showTaxonomy ) {
			contentTabs.push( tax_settings );
		}
		if ( showTitle ) {
			contentTabs.push( title_settings );
		}
		if ( showDescription ) {
			contentTabs.push( desc_settings );
		}
		if ( showPrice ) {
			contentTabs.push( price_settings );
		}
		return contentTabs;
	}, [ showTaxonomy, showTitle, showDescription, showPrice ] );

	return (
		<>
			<TemplateModal
				prev={
					LinkGenerator( 'https://www.wpxpo.com/wowstore/blocks/' ) +
					'#demoid1910'
				}
				store={ store }
			/>
			<Sections>
				<Section
					slug="setting"
					title={ __( 'Settings', 'product-blocks' ) }
				>
					<CommonSettings
						initialOpen={ section.general }
						title={ __( 'General + Query', 'product-blocks' ) }
						store={ store }
						include={ [
							{ data: defLayout },
							{
								data: {
									type: 'range',
									key: 'height',
									min: 1,
									max: 800,
									step: 1,
									responsive: true,
									unit: true,
									label: __( 'Height', 'product-blocks' ),
								},
							},
							{
								data: {
									type: 'select',
									key: 'slideAnimation',
									label: __(
										'Slide Animation',
										'product-blocks'
									),
									options: [
										{
											value: 'default',
											label: __(
												'Default',
												'product-blocks'
											),
										},
										{
											value: 'fade',
											label: __(
												'Fade',
												'product-blocks'
											),
										},
									],
								},
							},
							{
								data: {
									type: 'toggle',
									key: 'stackOnMobile',
									label: __(
										'Stack On Mobile',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'toggle',
									key: 'autoPlay',
									label: __( 'Auto Play', 'product-blocks' ),
								},
							},
							{
								data: {
									type: 'range',
									key: 'slideSpeed',
									min: 1,
									max: 10000,
									step: 1,
									label: __(
										'Slider Speed',
										'product-blocks'
									),
								},
							},

							{
								data: {
									type: 'separator',
									fancy: true,
									label: __(
										'Query Builder',
										'product-blocks'
									),
								},
							},

							...productSort.map( ( data ) => {
								return {
									data,
								};
							} ),

							{
								data: { type: 'separator' },
							},

							{
								data: {
									type: 'range',
									key: 'sliderMaxWidth',
									min: 1,
									max: 2000,
									step: 1,
									responsive: true,
									unit: true,
									label: __(
										'Container Width',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'range',
									key: 'itemGap',
									min: 1,
									max: 500,
									step: 1,
									responsive: true,
									unit: true,
									label: __( 'Gap', 'product-blocks' ),
								},
							},
							{
								data: {
									type: 'alignment',
									key: 'sliderAlign',
									responsive: true,
									label: __(
										'Alignment (Justify Content)',
										'product-blocks'
									),
									disableJustify: true,
									icons: [
										'juststart',
										'justcenter',
										'justend',
										'justaround',
										'justbetween',
										'justevenly',
									],
									options: [
										'start',
										'center',
										'end',
										'space-around',
										'space-between',
										'space-evenly',
									],
								},
							},
							{
								data: {
									type: 'toggle',
									key: 'showTaxonomy',
									label: __(
										'Show Taxonomy',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'toggle',
									key: 'showTitle',
									label: __( 'Show Title', 'product-blocks' ),
								},
							},
							{
								data: {
									type: 'toggle',
									key: 'showDescription',
									label: __(
										'Show Description',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'toggle',
									key: 'showPrice',
									label: __( 'Show Price', 'product-blocks' ),
								},
							},
							{
								data: {
									type: 'toggle',
									key: 'showCart',
									label: __(
										'Show Add to Cart',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'toggle',
									key: 'showImage',
									label: __( 'Show Image', 'product-blocks' ),
								},
							},
							{
								data: {
									type: 'toggle',
									key: 'showArrows',
									label: __(
										'Show Arrows',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'toggle',
									key: 'showDots',
									label: __( 'Show Dots', 'product-blocks' ),
								},
							},
							{
								data: {
									type: 'toggle',
									key: 'showSaleBadge',
									label: __(
										'Show Sale Tag',
										'product-blocks'
									),
								},
							},
						] }
					/>

					<CommonSettings
						initialOpen={ section.background }
						title={ __( 'Background', 'product-blocks' ) }
						store={ store }
						include={ bg_settings }
					/>
					<CommonSettings
						initialOpen={ section.contents }
						title={ __( 'Contents', 'product-blocks' ) }
						store={ store }
						include={ getContentsSettings( contentTabs ) }
					/>
					{ showCart && (
						<CommonSettings
							initialOpen={ section[ 'add-to-cart' ] }
							title={ __( 'Add to Cart', 'product-blocks' ) }
							store={ store }
							include={ cart_settings }
						/>
					) }

					{ showImage && (
						<>
							<CommonSettings
								initialOpen={ section.image }
								title={ __( 'Image', 'product-blocks' ) }
								store={ store }
								include={ image_settings }
							/>
							{ showSaleBadge && (
								<SalesStyle
									initialOpen={ section.sales }
									store={ store }
									exclude={ [ 'saleStyle' ] }
									include={ sales_settings }
								/>
							) }
						</>
					) }

					{ showArrows && (
						<ArrowStyle
							store={ store }
							initialOpen={ section.arrow }
							title={ `Arrow` }
							exclude={ [ 'arrowHorizontal', 'arrowVartical' ] }
							include={ [
								{
									position: 0,
									data: {
										type: 'toggle',
										key: 'arrowHideExtraLarge',
										label: __(
											'Hide On Extra Large',
											'product-blocks'
										),
									},
								},
								{
									position: 1,
									data: {
										type: 'toggle',
										key: 'arrowHideDesktop',
										label: __(
											'Hide On Desktop',
											'product-blocks'
										),
									},
								},
								{
									position: 2,
									data: {
										type: 'toggle',
										key: 'arrowHideTablet',
										label: __(
											'Hide On Tablet',
											'product-blocks'
										),
									},
								},
								{
									position: 3,
									data: {
										type: 'toggle',
										key: 'arrowHideMobile',
										label: __(
											'Hide On Mobile',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'arrowVertical',
										min: -200,
										max: 1000,
										step: 1,
										responsive: true,
										unit: true,
										label: __(
											'Vertical Position',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'arrowItemSpace',
										min: -500,
										max: 1000,
										step: 1,
										responsive: true,
										unit: true,
										label: __(
											'Horizontal Position',
											'product-blocks'
										),
									},
								},
							] }
						/>
					) }

					{ showDots && (
						<DotStyle
							store={ store }
							initialOpen={ section.dot }
							exclude={ [ 'dotVartical' ] }
							include={ [
								{
									position: 1,
									data: {
										type: 'range',
										key: 'dotVertical',
										min: -200,
										max: 700,
										step: 1,
										unit: true,
										responsive: true,
										label: __(
											'Vertical Position',
											'product-blocks'
										),
									},
								},
							] }
						/>
					) }
				</Section>
				<Section
					slug="advanced"
					title={ __( 'Advanced', 'product-blocks' ) }
				>
					<GeneralAdvanced
						initialOpen={ true }
						store={ store }
						exclude={ [
							'wrapOuterPadding',
							'wrapBg',
							'wrapShadow',
							'wrapHoverBackground',
							'wrapHoverShadow',
						] }
					/>
					<ResponsiveAdvanced pro={ true } store={ store } />
					<CustomCssAdvanced store={ store } />
				</Section>
			</Sections>
			{ wopbSupport() }
		</>
	);
}

export function AddSettingsToToolbar( { selected, store } ) {
	const { showTaxonomy, showTitle, showDescription, showPrice } =
		store.attributes;

	const contentTabs = useMemo( () => {
		const contentTabs = [];
		if ( showTaxonomy ) {
			contentTabs.push( tax_settings );
		}
		if ( showTitle ) {
			contentTabs.push( title_settings );
		}
		if ( showDescription ) {
			contentTabs.push( desc_settings );
		}
		if ( showPrice ) {
			contentTabs.push( price_settings );
		}
		return contentTabs;
	}, [ showTaxonomy, showTitle, showDescription, showPrice ] );

	switch ( selected ) {
		case 'background':
			return (
				<WopbToolbarGroup text={ 'Background' }>
					<WopbToolbarDropdown
						buttonContent={ colorIcon }
						store={ store }
						label={ __( 'Background Color', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Background Color',
									'product-blocks'
								),
								options: filterFields(
									[ 'wrapBg', 'bgColor' ],
									'__all',
									bg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Background Spacing', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Background Spacing',
									'product-blocks'
								),
								options: filterFields(
									[ 'wrapOuterPadding', 'margin', 'padding' ],
									'__all',
									bg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Background Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Background Style',
									'product-blocks'
								),
								options: filterFields(
									[ 'bgTab' ],
									'__all',
									bg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'contents':
			return (
				<WopbToolbarGroup text={ 'Con.' }>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Contents Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Contents Style', 'product-blocks' ),
								options: contents( contentTabs ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'add-to-cart':
			return (
				<WopbToolbarGroup text={ 'Cart' }>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Cart Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Cart Style', 'product-blocks' ),
								options: filterFields(
									[ 'qTab' ],
									'__all',
									cart
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Cart Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Cart Settings', 'product-blocks' ),
								options: filterFields(
									null,
									[ 'qTab', 'cartStyleDivider' ],
									cart
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'image':
			return (
				<WopbToolbarGroup text={ 'Image' }>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Image Dimension', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Image Dimension',
									'product-blocks'
								),
								options: filterFields(
									[
										'imgHeight',
										'imgWidth',
										'imgSectionPadding',
									],
									'__all',
									image
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Image Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Image Style', 'product-blocks' ),
								options: filterFields(
									null,
									[
										'imgHeight',
										'imgWidth',
										'imgSectionPadding',
									],
									image
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'sales':
			return (
				<WopbToolbarGroup text={ 'Sales' }>
					<TypographyTB
						store={ store }
						attrKey={ 'salesTypo' }
						label={ __( 'Sales Typography', 'product-blocks' ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Sales Dimension', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Sales Dimension',
									'product-blocks'
								),
								options: filterFields(
									[
										{
											data: {
												type: 'range',
												key: 'shapeSize',
												min: 10,
												max: 500,
												step: 1,
												responsive: true,
												unit: true,
												label: __(
													'Shape Size',
													'product-blocks'
												),
											},
										},
										'salesPadding',
										{
											data: {
												type: 'dimension',
												key: 'salesMargin',
												label: __(
													'Sales Margin',
													'product-blocks'
												),
												step: 1,
												unit: true,
												responsive: true,
											},
										},
									],
									'__all',
									SalesStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ colorIcon }
						store={ store }
						label={ __( 'Sales Color', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Sales Color', 'product-blocks' ),
								options: filterFields(
									[
										'salesColor',
										'salesBgColor',
										{
											position: 5,
											data: {
												type: 'color',
												key: 'shapeColor',
												label: __(
													'Shape Color',
													'product-blocks'
												),
											},
										},
									],
									'__all',
									SalesStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Sales Styles', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Sales Styles', 'product-blocks' ),
								options: filterFields(
									[
										{
											data: {
												type: 'select',
												key: 'saleStyle',
												label: __(
													'Sales Style',
													'product-blocks'
												),
												options: [
													{
														value: 'classic',
														label: __(
															'Classic',
															'product-blocks'
														),
													},
													{
														value: 'ribbon',
														label: __(
															'Ribbon',
															'product-blocks'
														),
													},
													{
														value: 'shape1',
														label: __(
															'Shape 1',
															'product-blocks'
														),
													},
												],
											},
										},
									],
									[
										'saleStyle',
										'salesTypo',
										'salesColor',
										'salesBgColor',
										'salesPadding',
										'shapeColor',
									],
									SalesStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'arrow':
			return (
				<WopbToolbarGroup text={ 'Arrow' }>
					<WopbToolbarDropdown
						buttonContent={ colorIcon }
						store={ store }
						label={ __( 'Arrow Dimension', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Arrow Dimension',
									'product-blocks'
								),
								options: filterFields(
									[
										'arrowSize',
										'arrowWidth',
										'arrowHeight',
										{
											data: {
												type: 'range',
												key: 'arrowVertical',
												min: -200,
												max: 1000,
												step: 1,
												responsive: true,
												unit: true,
												label: __(
													'Vertical Position',
													'product-blocks'
												),
											},
										},
										{
											data: {
												type: 'range',
												key: 'arrowItemSpace',
												min: -500,
												max: 1000,
												step: 1,
												responsive: true,
												unit: true,
												label: __(
													'Horizontal Position',
													'product-blocks'
												),
											},
										},
									],
									'__all',
									ArrowStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Arrow Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Arrow Style', 'product-blocks' ),
								options: filterFields(
									[ 'arrowStyle', 'aTab' ],
									'__all',
									ArrowStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Arrow Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Arrow Settings', 'product-blocks' ),
								options: filterFields(
									[
										{
											position: 0,
											data: {
												type: 'toggle',
												key: 'arrowHideExtraLarge',
												label: __(
													'Hide On Extra Large',
													'product-blocks'
												),
											},
										},
										{
											position: 1,
											data: {
												type: 'toggle',
												key: 'arrowHideDesktop',
												label: __(
													'Hide On Desktop',
													'product-blocks'
												),
											},
										},
										{
											position: 2,
											data: {
												type: 'toggle',
												key: 'arrowHideTablet',
												label: __(
													'Hide On Tablet',
													'product-blocks'
												),
											},
										},
										{
											position: 3,
											data: {
												type: 'toggle',
												key: 'arrowHideMobile',
												label: __(
													'Hide On Mobile',
													'product-blocks'
												),
											},
										},
									],
									[
										'arrowSize',
										'arrowWidth',
										'arrowHeight',
										'arrowVartical',
										'arrowHorizontal',
										'aTab',
										'arrowStyle',
									],
									ArrowStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'dot':
			return (
				<WopbToolbarGroup text={ 'Dots' }>
					<WopbToolbarDropdown
						buttonContent={ spacingIcon }
						store={ store }
						label={ __( 'Dots Dimension', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Dots Dimension', 'product-blocks' ),
								options: filterFields(
									[
										'dotSpace',
										{
											position: 1,
											data: {
												type: 'range',
												key: 'dotVertical',
												min: -200,
												max: 700,
												step: 1,
												unit: true,
												responsive: true,
												label: __(
													'Vertical Position',
													'product-blocks'
												),
											},
										},
										'dotHorizontal',
									],
									'__all',
									DotStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						buttonContent={ styleIcon }
						store={ store }
						label={ __( 'Dots Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Dots Style', 'product-blocks' ),
								options: filterFields(
									null,
									[
										'dotSpace',
										'dotVartical',
										'dotHorizontal',
									],
									DotStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
	}
}
