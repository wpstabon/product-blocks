const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';

registerBlockType( 'product-blocks/heading', {
	title: __( 'Heading', 'product-blocks' ),
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/heading.svg' }
			alt={ __( 'Image', 'product-blocks' ) }
			style={ { width: '100%' } }
		/>
	),
	category: 'product-blocks',
	description: __(
		'Display visually appealing headings and titles.',
		'product-blocks'
	),
	keywords: [
		__( 'heading', 'product-blocks' ),
		__( 'title', 'product-blocks' ),
		__( 'section', 'product-blocks' ),
	],
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
