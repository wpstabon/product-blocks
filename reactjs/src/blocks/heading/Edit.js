const { __ } = wp.i18n;
const { InspectorControls, RichText } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import {
	GeneralAdvanced,
	CustomCssAdvanced,
	HeadingSettings,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../helper/CommonPanel';
import icons from '../../helper/icons';
import { Sections, Section } from '../../helper/Sections';
import ToolBarElement from '../../helper/ToolBarElement';
import useBlockId from '../../helper/hooks/use-block-id';

const { useState } = wp.element;

export default function Edit( props ) {
	const { setAttributes, name, attributes, clientId, className } = props;

	const [ section, setSection ] = useState( 'Content' );

	const {
		blockId,
		advanceId,
		headingTag,
		headingText,
		headingStyle,
		headingAlign,
		headingURL,
		headingBtnText,
		subHeadingShow,
		subHeadingText,
	} = attributes;

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/heading', blockId );
	}

	const store = {
		setAttributes,
		name,
		attributes,
		setSection,
		section,
		clientId,
	};

	const HeadingTag = headingTag;

	return (
		<>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Settings', 'product-blocks' ) }
					>
						<HeadingSettings initialOpen={ true } store={ store } />
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced initialOpen={ true } store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<ToolBarElement include={ [] } store={ store } />

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ `wopb-block-wrapper` }>
					<div
						className={ `wopb-heading-wrap wopb-heading-${ headingStyle } wopb-heading-${ headingAlign }` }
					>
						{ headingURL ? (
							<HeadingTag className={ `wopb-heading-inner` }>
								<a>
									<RichText
										key="editable"
										tagName={ 'span' }
										keepPlaceholderOnFocus
										placeholder={ __(
											'Add Text…',
											'product-blocks'
										) }
										onChange={ ( value ) =>
											setAttributes( {
												headingText: value,
											} )
										}
										value={ headingText }
									/>
								</a>
							</HeadingTag>
						) : (
							<HeadingTag className={ `wopb-heading-inner` }>
								<RichText
									key="editable"
									tagName={ 'span' }
									keepPlaceholderOnFocus
									placeholder={ __(
										'Add Text…',
										'product-blocks'
									) }
									onChange={ ( value ) =>
										setAttributes( { headingText: value } )
									}
									value={ headingText }
								/>
							</HeadingTag>
						) }
						{ headingStyle == 'style11' && headingURL && (
							<a className={ `wopb-heading-btn` }>
								{ headingBtnText }
								{ icons.rightArrowLg }
							</a>
						) }
						{ subHeadingShow && (
							<div className={ `wopb-sub-heading` }>
								<RichText
									key="editable"
									tagName={ 'div' }
									className={ 'wopb-sub-heading-inner' }
									keepPlaceholderOnFocus
									placeholder={ __(
										'Add Text…',
										'product-blocks'
									) }
									onChange={ ( value ) =>
										setAttributes( {
											subHeadingText: value,
										} )
									}
									value={ subHeadingText }
								/>
							</div>
						) }
					</div>
				</div>
			</div>
		</>
	);
}
