const attributes = {
	blockId: { type: 'string', default: '' },
	currentPostId: { type: 'string', default: '' },
	headingText: { type: 'string', default: 'This is a Heading Example' },
	headingURL: { type: 'string', default: '' },
	headingBtnText: {
		type: 'string',
		default: 'View More',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style11' },
				],
			},
		],
	},
	headingStyle: { type: 'string', default: 'style1' },
	headingTag: {
		type: 'string',
		default: 'h2',
		style: [
			{
				selector:
					'{{WOPB}} span.wopb-heading-inner { display:inline-flex; }',
			},
		],
	},
	headingAlign: {
		type: 'string',
		default: 'left',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-heading-inner, {{WOPB}} .wopb-sub-heading-inner, {{WOPB}} .wopb-heading-wrap { text-align:{{headingAlign}}; }',
			},
		],
	},
	headingTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '32', unit: 'px' },
			height: { lg: '40', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '600',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span',
			},
		],
	},
	headingColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { color:{{headingColor}}; }',
			},
		],
	},
	headingBorderBottomColor: {
		type: 'string',
		default: '#0e1523',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style3' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { border-bottom-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style4' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { border-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style6' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before { background-color: {{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style7' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before , {{WOPB}} .wopb-heading-inner span:after { background-color: {{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style8' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:after { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style9' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style10' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style13' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { border-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style14' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style15' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style16' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style17' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style18' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:after { background-color:{{headingBorderBottomColor}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style19' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { border-color:{{headingBorderBottomColor}}; }',
			},
		],
	},
	headingBorderBottomColor2: {
		type: 'string',
		default: '#e5e5e5',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style8' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { background-color:{{headingBorderBottomColor2}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style10' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style14' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style19' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:after { background-color:{{headingBorderBottomColor2}}; }',
			},
		],
	},
	headingBg: {
		type: 'string',
		default: '#ff176b',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style5' },
				],
				selector:
					'{{WOPB}} .wopb-heading-style5 .wopb-heading-inner span:before { border-color:{{headingBg}} transparent transparent; } {{WOPB}} .wopb-heading-inner span { background-color:{{headingBg}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style2' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span { background-color:{{headingBg}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style3' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span { background-color:{{headingBg}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style12' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span { background-color:{{headingBg}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style13' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span { background-color:{{headingBg}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style18' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { background-color:{{headingBg}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style20' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before { border-color:{{headingBg}} transparent transparent; } {{WOPB}} .wopb-heading-inner { background-color:{{headingBg}}; }',
			},
		],
	},
	headingBg2: {
		type: 'string',
		default: '#e5e5e5',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style12' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { background-color:{{headingBg2}}; }',
			},
		],
	},
	headingBtnTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '', unit: 'px' },
			decoration: 'none',
			family: '',
		},
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style11' },
				],
				selector: '{{WOPB}} .wopb-heading-btn',
			},
		],
	},
	headingBtnColor: {
		type: 'string',
		default: '#ff176b',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style11' },
				],
				selector:
					'{{WOPB}} .wopb-heading-btn { color:{{headingBtnColor}}; } {{WOPB}} .wopb-heading-btn svg { fill:{{headingBtnColor}}; }',
			},
		],
	},
	headingBtnHoverColor: {
		type: 'string',
		default: '#0a31da',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style11' },
				],
				selector:
					'{{WOPB}} .wopb-heading-btn:hover { color:{{headingBtnHoverColor}}; } {{WOPB}} .wopb-heading-btn:hover svg { fill:{{headingBtnHoverColor}}; }',
			},
		],
	},
	headingBorder: {
		type: 'string',
		default: '3',
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style3' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { border-bottom-width:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style4' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { border-width:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style6' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before { width:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style7' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before , {{WOPB}} .wopb-heading-inner span:after { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style8' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before, {{WOPB}} .wopb-heading-inner:after { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style9' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style10' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before, {{WOPB}} .wopb-heading-inner:after { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style13' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner { border-width:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style14' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before, {{WOPB}} .wopb-heading-inner:after { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style15' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style16' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner span:before { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style17' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:before { height:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style19' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:after { width:{{headingBorder}}px; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style18' },
				],
				selector:
					'{{WOPB}} .wopb-heading-inner:after { width:{{headingBorder}}px; }',
			},
		],
	},
	headingSpacing: {
		type: 'object',
		default: { lg: 30, sm: 10, unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-heading-wrap {margin-top:0; margin-bottom:{{headingSpacing}} !important; }',
			},
		],
	},
	headingRadius: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style2' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { border-radius:{{headingRadius}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style3' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { border-radius:{{headingRadius}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style4' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner { border-radius:{{headingRadius}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style5' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { border-radius:{{headingRadius}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style12' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner { border-radius:{{headingRadius}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style13' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner { border-radius:{{headingRadius}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style18' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner { border-radius:{{headingRadius}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style20' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner { border-radius:{{headingRadius}}; }',
			},
		],
	},
	headingPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '8', right: '', unit: 'px' },
		},
		style: [
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style2' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style3' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style4' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style5' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style6' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style12' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style13' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style18' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style19' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
			{
				depends: [
					{ key: 'headingStyle', condition: '==', value: 'style20' },
				],
				selector:
					'{{WOPB}} .wopb-heading-wrap .wopb-heading-inner span { padding:{{headingPadding}}; }',
			},
		],
	},
	subHeadingShow: { type: 'boolean', default: false },
	subHeadingText: {
		type: 'string',
		default:
			'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ut sem augue. Sed at felis ut enim dignissim sodales.',
		style: [
			{
				depends: [
					{ key: 'subHeadingShow', condition: '==', value: true },
				],
			},
		],
	},
	subHeadingTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			spacing: { lg: '0', unit: 'px' },
			height: { lg: '27', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			family: '',
			weight: '500',
		},
		style: [
			{
				depends: [
					{ key: 'subHeadingShow', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-sub-heading div',
			},
		],
	},
	subHeadingColor: {
		type: 'string',
		default: '#989898',
		style: [
			{
				depends: [
					{ key: 'subHeadingShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-sub-heading div{ color:{{subHeadingColor}}; }',
			},
		],
	},
	subHeadingSpacing: {
		type: 'object',
		default: { lg: { unit: 'px' } },
		style: [
			{
				depends: [
					{ key: 'subHeadingShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} div.wopb-sub-heading-inner{ margin:{{subHeadingSpacing}}; }',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#f5f5f5' },
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#ff176b' },
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapMargin: {
		type: 'object',
		default: { lg: { top: '', bottom: '', unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: { type: 'string', default: '', style: [ { selector: '' } ] },
};
export default attributes;
