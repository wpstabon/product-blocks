const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';
registerBlockType( 'product-blocks/product-grid-3', {
	title: __( 'Product Grid #3', 'product-blocks' ),
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/product-grid-3.svg' }
			alt={ __( 'Product Grid #3', 'product-blocks' ) }
			style={ { width: '100%' } }
		/>
	),
	category: 'product-blocks',
	// description: (
	//     <span className="wopb-block-info">
	//         {__(
	//             "Display desired products of your store in a grid or slide view.",
	//             "product-blocks"
	//         )}
	//         <a
	//             target="_blank"
	//             href="https://wpxpo.com/docs/wowstore/all-blocks/product-grid-3/"
	//         >
	//             {__("Documentation", "product-blocks")}
	//         </a>
	//     </span>
	// ),
	keywords: [
		__( 'grid', 'product-blocks' ),
		__( 'product', 'product-blocks' ),
	],
	supports: {
		align: [ 'center', 'wide', 'full' ],
	},
	example: {
		attributes: {
			previewId: wopb_data.url + 'assets/img/preview/product-grid-3.svg',
		},
	},
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
