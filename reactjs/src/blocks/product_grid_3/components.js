const { Fragment } = wp.element;
const { __ } = wp.i18n;
import {
	renderTitle,
	renderDeals,
	renderExcerpt,
} from '../../helper/CommonPanel';
import useStyleButtonToggle from '../../helper/hooks/useStyleButtonToggle';
import {
	renderGridCart,
	renderGridCompare,
	renderGridQuickview,
	renderGridWishlist,
} from '../../helper/grid_compo/GridCommon';

export const Card = ( {
	attributes,
	variationSwitcherData,
	post,
	category,
	TitleTag,
	setSection,
} ) => {
	return (
		<div className={ `wopb-block-item` }>
			<div className={ `wopb-block-content-wrap` }>
				<CardImage
					attributes={ attributes }
					post={ post }
					category={ category }
					setSection={ setSection }
				/>
				<CardBody
					attributes={ attributes }
					variationSwitcherData={ variationSwitcherData }
					post={ post }
					category={ category }
					TitleTag={ TitleTag }
					setSection={ setSection }
				/>
			</div>
		</div>
	);
};

export const CardImage = ( { attributes, post, category, setSection } ) => {
	const {
		showDeal,
		imgCrop,
		catShow,
		showCart,
		showImage,
		showSale,
		showHot,
		saleStyle,
		saleDesign,
		catPosition,
		imgAnimation,
		showOutStock,
		showInStock,
		cartText,
		dealText,
		hotText,
		saleText,
		overlayMetaList,
		tooltipPosition,
		ovrMetaInline,
		hoverMeta,
	} = attributes;
	const parsedOverlayMetaList = JSON.parse( overlayMetaList );
	const { StyleButton, showStyleButton } = useStyleButtonToggle( ( e ) =>
		setSection( e, 'image' )
	);
	let metaClass = ovrMetaInline ? ' wopb_f_inline' : '';
	metaClass += ! hoverMeta ? ' wopb-is-visible' : '';

	return (
		<div
			className={ `wopb-block-image wopb-block-image-${ imgAnimation }` }
		>
			{ StyleButton }
			{ ( showSale || showHot ) && (
				<div className={ `wopb-onsale-hot` }>
					{ showSale && post.sale && (
						<span
							className={ `wopb-onsale wopb-onsale-${ saleStyle } wopb-component-simple` }
							onClick={ ( e ) => setSection( e, 'sales' ) }
						>
							{ saleDesign === 'digit' && '-' + post.discount }
							{ saleDesign === 'text' &&
								( saleText ||
									__( 'Sale!', 'product-blocks' ) ) }
							{ saleDesign === 'textDigit' &&
								__( '-' + post.discount + ' Off' ) }
						</span>
					) }
					{ showHot && post.featured && (
						<span
							className={ `wopb-hot wopb-component-simple` }
							onClick={ ( e ) => setSection( e, 'hots' ) }
						>
							{ hotText || __( 'Hot', 'product-blocks' ) }
						</span>
					) }
				</div>
			) }

			{ parsedOverlayMetaList.length != 0 && (
				<div
					className={
						`wopb-product-new-meta wopb-component-simple` +
						metaClass
					}
					onClick={ ( e ) => {
						setSection( e, 'overlay_meta' );
					} }
				>
					{ parsedOverlayMetaList.map( ( val, _i ) => {
						return (
							<Fragment key={ _i }>
								{ val == '_wishlist' &&
									renderGridWishlist( { tooltipPosition } ) }
								{ val == '_qview' &&
									renderGridQuickview( { tooltipPosition } ) }
								{ val == '_compare' &&
									renderGridCompare( { tooltipPosition } ) }
								{ val == '_cart' &&
									renderGridCart( {
										cartText,
										tooltipPosition,
									} ) }
							</Fragment>
						);
					} ) }
				</div>
			) }
			{ showCart && (
				<div className="wopb-quick-cart">
					<div
						className="wopb-product-btn wopb-component-simple"
						onClick={ ( e ) => setSection( e, 'cart' ) }
					>
						<a>
							{ cartText
								? cartText
								: __( 'Add to cart', 'product-blocks' ) }
						</a>
					</div>
				</div>
			) }

			{ catPosition != 'none' && catShow && category }
			{ post.stock == 'outofstock' && showOutStock && (
				<div className="wopb-product-outofstock">
					<span>{ __( 'Out of stock', 'product-blocks' ) }</span>
				</div>
			) }
			{ post.stock == 'instock' && showInStock && (
				<div className="wopb-product-instock">
					<span>{ __( 'In Stock', 'product-blocks' ) }</span>
				</div>
			) }
			{ showDeal && renderDeals( post.deal, dealText ) }
			{ showImage && post.image && (
				<a>
					<img
						alt={ post.title || '' }
						src={ post.image[ imgCrop ] }
						className="wopb-component-simple"
						onClick={ showStyleButton }
					/>
				</a>
			) }
		</div>
	);
};

export const CardBody = ( {
	attributes,
	variationSwitcherData,
	post,
	category,
	TitleTag,
	setSection,
} ) => {
	const {
		contentLayout,
		catShow,
		titleShow,
		showPrice,
		showReview,
		catPosition,
		shortDescLimit,
		showShortDesc,
		showVariationSwitch,
		variationSwitchPosition,
		titleLength,
	} = attributes;

	return (
		<div
			className={ `wopb-pg-cl ${
				contentLayout != '' ? 'wopb-pg-cl' + contentLayout : ''
			}` }
		>
			{ ( catShow || showPrice ) && (
				<div>
					{ variationSwitchPosition == 'before_price' &&
						showVariationSwitch &&
						showPrice &&
						variationSwitcherData }
					<div className="wopb-product-cat-price">
						{ catShow && catPosition == 'none' && category }
						{ post.price_html && showPrice && (
							<div
								onClick={ ( e ) => setSection( e, 'price' ) }
								className="wopb-product-price wopb-component-simple"
								dangerouslySetInnerHTML={ {
									__html: post.price_html,
								} }
							></div>
						) }
					</div>

					{ variationSwitchPosition == 'after_price' &&
						showVariationSwitch &&
						showPrice &&
						variationSwitcherData }
				</div>
			) }
			{ ( titleShow || showReview ) && (
				<div className="wopb-product-title-review">
					{ variationSwitchPosition == 'before_title' &&
						showVariationSwitch &&
						titleShow &&
						variationSwitcherData }
					{ post.title &&
						titleShow &&
						renderTitle(
							post.title,
							TitleTag,
							( e ) => setSection( e, 'title' ),
							undefined,
							titleLength
						) }
					{ showReview && (
						<div
							className="wopb-star-rating wopb-component-simple"
							onClick={ ( e ) => setSection( e, 'review' ) }
						>
							<span
								style={ {
									width:
										( post.rating_average / 5 ) * 100 + '%',
								} }
							>
								<strong
									itemProp="ratingValue"
									className="wopb-rating"
								>
									{ post.rating_average }
								</strong>
								{ __( 'out of 5', 'product-blocks' ) }
							</span>
						</div>
					) }

					{ variationSwitchPosition == 'after_title' &&
						showVariationSwitch &&
						titleShow &&
						variationSwitcherData }
				</div>
			) }
			{ showShortDesc && (
				<div className={ `wopb-short-description` }>
					{ ' ' }
					{ renderExcerpt( post.excerpt, shortDescLimit, ( e ) =>
						setSection( e, 'desc' )
					) }
				</div>
			) }
		</div>
	);
};
