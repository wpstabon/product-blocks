const { RichText } = wp.blockEditor;
import icons from '../../helper/icons';
const { __ } = wp.i18n;

export const SaleBadge = ( { attributes, setAttributes, forSave = false } ) => {
	const { saleStyle, salesLocation } = attributes;
	let onsaleClass = '';
	onsaleClass += ' wopb-location-' + salesLocation;
	let shape;
	switch ( saleStyle ) {
		case 'shape1':
			onsaleClass += ' wopb-onsale-shape';
			shape = icons.saleShape1;
			break;
		case 'shape2':
			onsaleClass += ' wopb-shape2';
			break;
		case 'shape3':
			onsaleClass += ' wopb-shape3';
			break;
		default:
			break;
	}
	const saleContent = (
		<SaleStyle
			attributes={ attributes }
			setAttributes={ setAttributes }
			forSave={ forSave }
		/>
	);
	return (
		<div className={ `wopb-onsale-hot` + onsaleClass }>
			{ shape && (
				<span className={ `wopb-sale-shape` }>
					{ shape }
					{ saleContent }
				</span>
			) }
			{ ! shape && saleContent }
		</div>
	);
};

const SaleStyle = ( { attributes, setAttributes, forSave = false } ) => {
	const { salesText, saleStyle } = attributes;

	if ( forSave ) {
		return (
			<span
				className={ `wopb-onsale wopb-onsale-${ saleStyle }` }
				dangerouslySetInnerHTML={ { __html: salesText } }
			/>
		);
	}

	return (
		<RichText
			key="editable"
			tagName={ 'span' }
			keepPlaceholderOnFocus
			placeholder={ __( 'Add Text…', 'product-blocks' ) }
			className={ `wopb-onsale wopb-onsale-${ saleStyle }` }
			onChange={ ( value ) => setAttributes( { salesText: value } ) }
			value={ salesText }
		/>
	);
};
