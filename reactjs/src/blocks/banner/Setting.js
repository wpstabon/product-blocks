const { __ } = wp.i18n;
import {
	CommonSettings,
	CustomCssAdvanced,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Section, Sections } from '../../helper/Sections';
import TemplateModal from '../../helper/TemplateModal';

const BannerSetting = ( { store } ) => {
	const { saleStyle, salesLocation } = store.attributes;
	let salesLocationOptions = [
		{ value: 'top_left', label: __( 'Top Left', 'product-blocks' ) },
		{ value: 'top_center', label: __( 'Top Center', 'product-blocks' ) },
		{ value: 'top_right', label: __( 'Top Right', 'product-blocks' ) },
		{ value: 'bottom_left', label: __( 'Bottom Left', 'product-blocks' ) },
		{
			value: 'bottom_center',
			label: __( 'Bottom Center', 'product-blocks' ),
		},
		{
			value: 'bottom_right',
			label: __( 'Bottom Right', 'product-blocks' ),
		},
		{ value: 'middle_left', label: __( 'Middle Left', 'product-blocks' ) },
		{
			value: 'middle_center',
			label: __( 'Middle Center', 'product-blocks' ),
		},
		{
			value: 'middle_right',
			label: __( 'Middle Right', 'product-blocks' ),
		},
	];
	if ( saleStyle == 'shape2' || saleStyle == 'shape3' ) {
		salesLocationOptions = [
			{ value: 'top_left', label: __( 'Top Left', 'product-blocks' ) },
			{ value: 'top_right', label: __( 'Top Right', 'product-blocks' ) },
			{
				value: 'bottom_left',
				label: __( 'Bottom Left', 'product-blocks' ),
			},
			{
				value: 'bottom_right',
				label: __( 'Bottom Right', 'product-blocks' ),
			},
		];
	}
	const shape2Default = {
		top_left: {
			salesPadding: {
				lg: { top: 50, bottom: 20, left: 0, right: 15, unit: 'px' },
			},
			salesMargin: {
				lg: { top: -20, bottom: 0, left: -40, right: 0, unit: 'px' },
			},
			salesLocation: 'top_left',
		},
		top_right: {
			salesPadding: {
				lg: { top: 50, bottom: 20, left: 15, right: 0, unit: 'px' },
			},
			salesMargin: {
				lg: { top: -20, bottom: 0, left: 0, right: -40, unit: 'px' },
			},
		},
		bottom_left: {
			salesPadding: {
				lg: { top: 20, bottom: 50, left: 15, right: 0, unit: 'px' },
			},
			salesMargin: {
				lg: { top: 0, bottom: -20, left: -40, right: 0, unit: 'px' },
			},
		},
		bottom_right: {
			salesPadding: {
				lg: { top: 20, bottom: 50, left: 0, right: 15, unit: 'px' },
			},
			salesMargin: {
				lg: { top: 0, bottom: -20, left: 0, right: -40, unit: 'px' },
			},
		},
	};
	const locationVariation =
		saleStyle === 'shape2' ? { variation: shape2Default } : {};

	return (
		<>
			<TemplateModal
				prev="https://www.wpxpo.com/wowstore/blocks/#demoid2130"
				store={ store }
			/>
			<Sections>
				<Section
					slug="settings"
					title={ __( 'Settings', 'product-blocks' ) }
				>
					<CommonSettings
						title={ __( 'General', 'product-blocks' ) }
						include={ [
							{
								data: {
									type: 'layout',
									block: 'button',
									key: 'layout',
									label: __( 'Style', 'product-blocks' ),
									options: [
										{
											img: 'assets/img/blocks/banner/layout1.svg',
											label: __(
												'Layout 1',
												'product-blocks'
											),
											value: 'layout1',
										},
										{
											img: 'assets/img/blocks/banner/layout2.svg',
											label: __(
												'Layout 2',
												'product-blocks'
											),
											value: 'layout2',
										},
										{
											img: 'assets/img/blocks/banner/layout3.svg',
											label: __(
												'Layout 3',
												'product-blocks'
											),
											value: 'layout3',
										},
										{
											img: 'assets/img/blocks/banner/layout4.svg',
											label: __(
												'Layout 4',
												'product-blocks'
											),
											value: 'layout4',
										},
									],
								},
							},
							{
								data: {
									type: 'range',
									key: 'bannerWidth',
									min: 1,
									max: 2000,
									step: 1,
									responsive: true,
									unit: true,
									label: __(
										'Banner Width',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'alignment',
									key: 'bannerAlign',
									label: __(
										'Banner Alignment',
										'product-blocks'
									),
									responsive: true,
									disableJustify: true,
									options: [
										'flex-start',
										'center',
										'flex-end',
									],
								},
							},
							{
								data: {
									type: 'alignment',
									key: 'bannerContentAlign',
									responsive: true,
									label: __(
										'Content Alignment',
										'product-blocks'
									),
									disableJustify: true,
									icons: [
										'juststart',
										'justcenter',
										'justend',
									],
									options: [ 'start', 'center', 'end' ],
								},
							},
							{
								data: {
									type: 'range',
									key: 'gap',
									min: 1,
									max: 500,
									step: 1,
									responsive: true,
									unit: [ 'px' ],
									label: __( 'Gap', 'product-blocks' ),
								},
							},
							{
								data: {
									type: 'toggle',
									key: 'stackOnMobile',
									label: __(
										'Stack On Mobile',
										'product-blocks'
									),
								},
							},
						] }
						initialOpen={ true }
						store={ store }
					/>
					<CommonSettings
						title={ __( 'Image', 'product-blocks' ) }
						depend="showImage"
						include={ [
							{
								data: {
									type: 'media',
									key: 'img',
									label: __( 'Image', 'product-blocks' ),
								},
							},
							{
								data: {
									type: 'range',
									key: 'imgHeight',
									min: 1,
									max: 2000,
									step: 1,
									responsive: true,
									unit: [ 'px' ],
									label: __(
										'Image Height',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'range',
									key: 'imgWidth',
									min: 1,
									max: 2000,
									step: 1,
									responsive: true,
									unit: [ 'px' ],
									label: __(
										'Image Width',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'tag',
									key: 'imageScale',
									label: __(
										'Image Scale',
										'product-blocks'
									),
									options: [
										{
											value: '',
											label: __(
												'None',
												'product-blocks'
											),
										},
										{
											value: 'cover',
											label: __(
												'Cover',
												'product-blocks'
											),
										},
										{
											value: 'contain',
											label: __(
												'Contain',
												'product-blocks'
											),
										},
										{
											value: 'fill',
											label: __(
												'Fill',
												'product-blocks'
											),
										},
										{
											value: 'scale-down',
											label: __(
												'Scale Down',
												'product-blocks'
											),
										},
									],
								},
							},
							{
								data: {
									type: 'dimension',
									key: 'imgMargin',
									label: __( 'Margin', 'product-blocks' ),
									step: 1,
									unit: true,
									responsive: true,
								},
							},
							{
								data: {
									type: 'border',
									key: 'imgBorder',
									label: __(
										'Image Border',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'dimension',
									key: 'imgRadius',
									label: __(
										'Border Radius',
										'product-blocks'
									),
									step: 1,
									unit: true,
									responsive: true,
								},
							},
						] }
						initialOpen={ false }
						store={ store }
					/>
					<CommonSettings
						title={ __( 'Content', 'product-blocks' ) }
						include={ [
							{
								data: {
									type: 'dimension',
									key: 'contentMargin',
									label: __(
										'Content Margin',
										'product-blocks'
									),
									step: 1,
									unit: true,
									responsive: true,
								},
							},
							{
								data: {
									type: 'dimension',
									key: 'contentPadding',
									label: __(
										'Content Padding',
										'product-blocks'
									),
									step: 1,
									unit: true,
									responsive: true,
								},
							},
						] }
						initialOpen={ false }
						store={ store }
					/>
					<CommonSettings
						title={ __( 'Background', 'product-blocks' ) }
						include={ [
							{
								data: {
									type: 'toggle',
									key: 'showBannerLink',
									label: __(
										'Banner Link',
										'product-blocks'
									),
								},
							},
							{ data: { type: 'text', key: 'bannerLink' } },
							{
								data: {
									type: 'toggle',
									key: 'bannerNewTab',
									label: __(
										'Open in New Tab',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'border',
									key: 'bannerBorder',
									label: __(
										'Banner Border',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'dimension',
									key: 'bannerRadius',
									label: __(
										'Banner Radius',
										'product-blocks'
									),
									step: 1,
									unit: true,
									responsive: true,
								},
							},
							{
								data: {
									type: 'dimension',
									key: 'bannerPadding',
									label: __( 'Padding', 'product-blocks' ),
									step: 1,
									unit: true,
									responsive: true,
								},
							},
							{
								data: {
									type: 'boxshadow',
									key: 'bannerBoxShadow',
									label: __( 'Box Shadow', 'product-blocks' ),
								},
							},
							{
								data: {
									type: 'color2',
									key: 'bannerBackground',
									image: true,
									label: __(
										'Background Fill',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'toggle',
									key: 'showBgOverlay',
									label: __(
										'Background Overlay',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'color',
									key: 'bgOverlay',
									label: __(
										'Overlay Background',
										'product-blocks'
									),
								},
							},
						] }
						initialOpen={ false }
						store={ store }
					/>
					<CommonSettings
						title={ __( 'Offer Badge', 'product-blocks' ) }
						depend="showSaleBadge"
						include={ [
							{
								data: {
									type: 'select',
									key: 'saleStyle',
									label: __(
										'Badge Style',
										'product-blocks'
									),
									options: [
										{
											value: 'classic',
											label: __(
												'Classic',
												'product-blocks'
											),
										},
										{
											value: 'ribbon',
											label: __(
												'Ribbon',
												'product-blocks'
											),
										},
										{
											value: 'shape1',
											label: __(
												'Shape 1',
												'product-blocks'
											),
										},
										{
											value: 'shape2',
											label: __(
												'Shape 2',
												'product-blocks'
											),
										},
										{
											value: 'shape3',
											label: __(
												'Shape 3',
												'product-blocks'
											),
										},
									],
									variation: {
										classic: {
											salesPadding: {
												lg: {
													top: 17,
													bottom: 17,
													left: 20,
													right: 20,
													unit: 'px',
												},
											},
											salesMargin: {
												lg: {
													top: 22,
													bottom: 0,
													left: -45,
													right: 0,
													unit: 'px',
												},
											},
										},
										ribbon: {
											salesPadding: {
												lg: {
													top: 17,
													bottom: 17,
													left: 20,
													right: 20,
													unit: 'px',
												},
											},
											salesMargin: {
												lg: {
													top: 22,
													bottom: 0,
													left: -45,
													right: 0,
													unit: 'px',
												},
											},
										},
										shape1: {
											salesPadding: {
												lg: {
													top: 17,
													bottom: 17,
													left: 20,
													right: 20,
													unit: 'px',
												},
											},
											salesMargin: {
												lg: {
													top: 22,
													bottom: 0,
													left: -45,
													right: 0,
													unit: 'px',
												},
											},
										},
										shape2:
											shape2Default[ salesLocation ] ??
											shape2Default.top_left,
										shape3: {
											salesPadding: {
												lg: {
													top: 17,
													bottom: 17,
													left: 20,
													right: 20,
													unit: 'px',
												},
											},
											salesMargin: {
												lg: {
													top: 22,
													bottom: 0,
													left: -45,
													right: 0,
													unit: 'px',
												},
											},
										},
									},
								},
							},
							{
								data: {
									type: 'text',
									key: 'salesText',
									label: __( 'Text', 'product-blocks' ),
								},
							},
							{
								data: {
									type: 'typography',
									key: 'salesTypo',
									label: __( 'Typography', 'product-blocks' ),
								},
							},
							{
								data: {
									type: 'color',
									key: 'salesColor',
									label: __( 'Text Color', 'product-blocks' ),
								},
							},
							{
								data: {
									type: 'color',
									key: 'salesBgColor',
									label: __(
										'Background Color',
										'product-blocks'
									),
								},
							},
							{
								data: {
									type: 'border',
									key: 'salesBorder',
									label: __( 'Border', 'product-blocks' ),
								},
							},
							{
								data: {
									type: 'range',
									key: 'salesSize',
									min: 10,
									max: 500,
									step: 1,
									responsive: true,
									unit: true,
									label: __( 'Badge Size', 'product-blocks' ),
								},
							},
							{
								data: {
									type: 'dimension',
									key: 'salesRadius',
									label: __(
										'Border Radius',
										'product-blocks'
									),
									step: 1,
									unit: true,
									responsive: true,
								},
							},
							{
								data: {
									type: 'dimension',
									key: 'salesPadding',
									label: __( 'Padding', 'product-blocks' ),
									step: 1,
									unit: true,
									responsive: true,
								},
							},
							{
								data: {
									type: 'dimension',
									key: 'salesMargin',
									label: __( 'Margin', 'product-blocks' ),
									step: 1,
									unit: true,
									responsive: true,
								},
							},
							{
								data: {
									type: 'group',
									key: 'salesPosition',
									label: __(
										'Badge Position',
										'product-blocks'
									),
									justify: true,
									options: [
										{
											value: 'banner',
											label: __(
												'Entire Banner',
												'product-blocks'
											),
										},
										{
											value: 'image',
											label: __(
												'Image',
												'product-blocks'
											),
										},
									],
								},
							},
							{
								data: {
									type: 'select',
									key: 'salesLocation',
									label: __(
										'Badge Location',
										'product-blocks'
									),
									options: salesLocationOptions,
									...locationVariation,
								},
							},
						] }
						initialOpen={ false }
						store={ store }
					/>
				</Section>
				<Section
					slug="advanced"
					title={ __( 'Advanced', 'product-blocks' ) }
				>
					<ResponsiveAdvanced store={ store } />
					<CustomCssAdvanced store={ store } />
				</Section>
			</Sections>
			{ wopbSupport() }
		</>
	);
};

export default BannerSetting;
