const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import Save from './Save';
import attributes from './attributes';

registerBlockType( 'product-blocks/banner', {
	title: __( 'Banner Maker', 'product-blocks' ),
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/banner.svg' }
			alt="banner"
		/>
	),
	category: 'product-blocks',
	description: (
		<span className="wopb-block-info">
			{ __( 'Create & customize banner.', 'product-blocks' ) }
			<a
				target="_blank"
				href="https://wpxpo.com/docs/wowstore/all-blocks/banner-maker-block/"
				rel="noreferrer"
			>
				{ __( 'Documentation', 'product-blocks' ) }
			</a>
		</span>
	),
	keywords: [
		__( 'banner', 'product-blocks' ),
		__( 'offer', 'product-blocks' ),
		__( 'banner maker', 'product-blocks' ),
		__( 'image', 'product-blocks' ),
	],

	supports: {
		align: [ 'center', 'wide', 'full' ],
	},
	example: {
		attributes: {
			previewImg: wopb_data.url + 'assets/img/preview/banner.svg',
		},
	},
	edit: Edit,
	save: Save,
	attributes,
} );
