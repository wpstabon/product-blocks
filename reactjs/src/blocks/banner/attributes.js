const attributes = {
	blockId: {
		type: 'string',
		default: '',
	},
	currentPostId: { type: 'string', default: '' },
	previewImg: {
		type: 'string',
		default: '',
	},
	initPremade: {
		type: 'boolean',
		default: false,
	},
	layout: {
		type: 'string',
		default: 'layout1',
		style: [
			{
				depends: [
					{ key: 'layout', condition: '==', value: 'layout1' },
					{ key: 'showImage', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-banner-wrapper .wopb-banner-section { grid-template-columns: 1fr 1fr; } {{WOPB}} .wopb-banner-wrapper .wopb-content-wrapper { order: 0; }',
			},
			{
				depends: [
					{ key: 'layout', condition: '==', value: 'layout1' },
					{ key: 'showImage', condition: '==', value: false },
				],
				selector:
					'{{WOPB}} .wopb-banner-wrapper .wopb-banner-section { grid-template-columns: 1fr; } {{WOPB}} .wopb-banner-wrapper .wopb-content-wrapper { order: 0; }',
			},
			{
				depends: [
					{ key: 'layout', condition: '==', value: 'layout2' },
					{ key: 'showImage', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-banner-wrapper .wopb-banner-section { grid-template-columns: 1fr 1fr; } {{WOPB}} .wopb-banner-wrapper .wopb-content-wrapper { order: 1; }',
			},
			{
				depends: [
					{ key: 'layout', condition: '==', value: 'layout2' },
					{ key: 'showImage', condition: '==', value: false },
				],
				selector:
					'{{WOPB}} .wopb-banner-wrapper .wopb-banner-section { grid-template-columns: 1fr; } {{WOPB}} .wopb-banner-wrapper .wopb-content-wrapper { order: 1; }',
			},
			{
				depends: [
					{ key: 'stackOnMobile', condition: '==', value: true },
				],
				selector:
					'@media screen and (max-width:768px) { ' +
					'{{WOPB}} .wopb-banner-wrapper .wopb-banner-section { grid-template-columns: none; }' +
					'}',
			},
			{
				depends: [
					{ key: 'layout', condition: '==', value: 'layout3' },
				],
				selector:
					'{{WOPB}} .wopb-banner-wrapper .wopb-content-wrapper { order: 0; }',
			},
			{
				depends: [
					{ key: 'layout', condition: '==', value: 'layout4' },
				],
				selector:
					'{{WOPB}} .wopb-banner-wrapper .wopb-content-wrapper { order: 1; }',
			},
		],
	},
	bannerWidth: {
		type: 'object',
		default: { lg: '1110', sm: '', xs: '', unit: 'px' },
		style: [
			{
				selector:
					'.editor-styles-wrapper {{WOPB}} .wopb-banner-wrapper .wopb-banner-section, {{WOPB}} .wopb-banner-wrapper .wopb-banner-link { max-width:{{bannerWidth}}; }',
			},
		],
	},
	bannerAlign: {
		type: 'object',
		default: { lg: 'center' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-banner-wrapper { justify-content: {{bannerAlign}}; } ',
			},
		],
	},
	bannerContentAlign: {
		type: 'object',
		default: { lg: 'start' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-banner-wrapper .wopb-banner-section { justify-items:{{bannerContentAlign}}; } ',
			},
		],
	},
	gap: {
		type: 'object',
		default: { lg: 52, sm: 20, xs: 20, unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-banner-wrapper .wopb-banner-section { gap:{{gap}}; }',
			},
		],
	},
	stackOnMobile: {
		type: 'boolean',
		default: true,
	},

	//Content
	contentAlign: {
		type: 'object',
		default: { lg: 'left' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-banner-wrapper .wopb-banner-section .wopb-content-wrapper { text-align:{{contentAlign}}; } ',
			},
		],
	},
	contentMargin: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-banner-wrapper .wopb-banner-section .wopb-content-wrapper { margin:{{contentMargin}}; }',
			},
		],
	},
	contentPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-banner-wrapper .wopb-banner-section .wopb-content-wrapper { padding:{{contentPadding}}; }',
			},
		],
	},

	//Entire banner
	showBannerLink: {
		type: 'boolean',
		default: false,
	},
	bannerLink: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [
					{ key: 'showBannerLink', condition: '==', value: true },
				],
			},
		],
	},
	bannerNewTab: {
		type: 'boolean',
		default: true,
		style: [
			{
				depends: [
					{ key: 'showBannerLink', condition: '==', value: true },
				],
			},
		],
	},
	bannerBorder: {
		type: 'object',
		default: {
			openBorder: 0.0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-banner-wrapper .wopb-banner-section',
			},
		],
	},
	bannerRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-banner-wrapper .wopb-banner-section { border-radius:{{bannerRadius}}; }',
			},
		],
	},
	bannerPadding: {
		type: 'object',
		default: {
			lg: {
				top: '48',
				bottom: '48',
				left: '72',
				right: '72',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} > .wopb-banner-wrapper .wopb-banner-section { padding:{{bannerPadding}}; }',
			},
		],
	},
	bannerBoxShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 0, left: 0 },
			color: '#333',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-banner-wrapper .wopb-banner-section',
			},
		],
	},
	bannerBackground: {
		type: 'object',
		default: {
			openColor: 1,
			type: 'color',
			color: '#6E6E6E',
			size: 'cover',
			repeat: 'no-repeat',
		},
		style: [
			{
				selector:
					'{{WOPB}} > .wopb-banner-wrapper .wopb-banner-section',
			},
		],
	},
	showBgOverlay: {
		type: 'boolean',
		default: false,
	},
	bgOverlay: {
		type: 'string',
		default: 'rgba(87,87,87,.5019607843)',
		style: [
			{
				depends: [
					{ key: 'showBgOverlay', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} > .wopb-banner-wrapper .wopb-banner-section .wopb-banner-overlay { background:{{bgOverlay}}; }',
			},
		],
	},

	//Image
	showImage: {
		type: 'boolean',
		default: true,
	},
	img: {
		type: 'object',
		default: '',
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
			},
		],
	},
	imgHeight: {
		type: 'object',
		default: { lg: '368', ulg: 'px', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-banner-wrapper .wopb-banner-section .wopb-image-wrapper .wopb-image { height: {{imgHeight}}; }',
			},
		],
	},
	imgWidth: {
		type: 'object',
		default: { lg: '448', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-banner-wrapper .wopb-banner-section .wopb-image-wrapper { max-width: {{imgWidth}}; }',
			},
		],
	},
	imageScale: {
		type: 'string',
		default: 'cover',
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-banner-wrapper .wopb-image-wrapper img {object-fit: {{imageScale}};}',
			},
		],
	},
	imgMargin: {
		type: 'object',
		default: {
			lg: { top: '0', right: '0', bottom: '0', left: '0', unit: 'px' },
		},
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-banner-wrapper .wopb-banner-section .wopb-image-wrapper .wopb-image { margin: {{imgMargin}}; }',
			},
		],
	},
	imgBorder: {
		type: 'object',
		default: {
			openBorder: 0.0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-banner-wrapper .wopb-banner-section .wopb-image-wrapper .wopb-image',
			},
		],
	},
	imgRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [ { key: 'showImage', condition: '==', value: true } ],
				selector:
					'{{WOPB}} .wopb-block-image-empty, {{WOPB}} .wopb-banner-wrapper .wopb-banner-section .wopb-image-wrapper .wopb-image { border-radius:{{imgRadius}}; }',
			},
		],
	},

	//Offer badge
	showSaleBadge: {
		type: 'boolean',
		default: true,
	},
	saleStyle: {
		type: 'string',
		default: 'classic',
		style: [
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
				],
			},
		],
	},
	salesText: {
		type: 'string',
		default: '55%<br>OFF',
		style: [
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
				],
			},
		],
	},
	salesTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '20', xs: 14, unit: 'px' },
			spacing: { lg: '0', unit: 'px' },
			height: { lg: '22', xs: 19, unit: 'px' },
			decoration: 'none',
			transform: 'uppercase',
			family: 'Roboto',
			weight: '700',
		},
		style: [
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-onsale',
			},
		],
	},
	salesColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-onsale { color:{{salesColor}}; }',
			},
		],
	},
	salesBgColor: {
		type: 'string',
		default: '#63B646',
		style: [
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
					{ key: 'saleStyle', condition: '==', value: 'classic' },
				],
				selector:
					'{{WOPB}} .wopb-onsale { background:{{salesBgColor}}; } ' +
					'{{WOPB}} .wopb-onsale.wopb-onsale-ribbon:before { border-left: 23px solid {{salesBgColor}}; border-right: 23px solid {{salesBgColor}}; }',
			},
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
					{ key: 'saleStyle', condition: '==', value: 'ribbon' },
				],
				selector:
					'{{WOPB}} .wopb-onsale { background:{{salesBgColor}}; } ' +
					'{{WOPB}} .wopb-onsale.wopb-onsale-ribbon:before { border-left: 23px solid {{salesBgColor}}; border-right: 24px solid {{salesBgColor}}; }',
			},
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
					{ key: 'saleStyle', condition: '==', value: 'shape1' },
				],
				selector:
					'{{WOPB}} .wopb-onsale-shape svg { fill:{{salesBgColor}}; } ' +
					'{{WOPB}} .wopb-onsale-shape svg path { stroke:{{salesBgColor}}; }',
			},
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
					{ key: 'saleStyle', condition: '==', value: 'shape2' },
				],
				selector:
					'{{WOPB}} .wopb-onsale-hot { background:{{salesBgColor}}; }',
			},
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
					{ key: 'saleStyle', condition: '==', value: 'shape3' },
				],
				selector:
					'{{WOPB}} .wopb-onsale-hot { background:{{salesBgColor}}; }',
			},
		],
	},
	salesBorder: {
		type: 'object',
		default: {
			openBorder: 0.0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
					{ key: 'saleStyle', condition: '==', value: 'classic' },
				],
				selector:
					'{{WOPB}} .wopb-banner-wrapper .wopb-banner-section .wopb-onsale-hot .wopb-onsale',
			},
		],
	},
	salesSize: {
		type: 'object',
		default: { lg: '90', xs: 40, unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
					{ key: 'saleStyle', condition: '==', value: 'shape1' },
				],
				selector:
					'{{WOPB}} .wopb-banner-wrapper .wopb-banner-section .wopb-sale-shape { height:{{salesSize}}; }',
			},
		],
	},
	salesRadius: {
		type: 'object',
		default: {
			lg: { top: 50, right: 50, bottom: 50, left: 50 },
			unit: 'px',
		},
		style: [
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
					{ key: 'saleStyle', condition: '==', value: 'classic' },
				],
				selector:
					'{{WOPB}} .wopb-onsale.wopb-onsale-classic { border-radius:{{salesRadius}}; }',
			},
		],
	},
	salesPadding: {
		type: 'object',
		default: {
			lg: { top: 17, bottom: 17, left: 20, right: 20, unit: 'px' },
		},
		style: [
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
					{ key: 'saleStyle', condition: '==', value: 'classic' },
				],
				selector:
					'{{WOPB}} .wopb-onsale.wopb-onsale-classic { padding:{{salesPadding}}; }',
			},
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
					{ key: 'saleStyle', condition: '==', value: 'shape2' },
				],
				selector:
					'{{WOPB}} .wopb-onsale-hot { padding:{{salesPadding}}; }',
			},
		],
	},
	salesMargin: {
		type: 'object',
		default: {
			lg: { top: 22, bottom: 0, left: -45, right: 0, unit: 'px' },
			xs: { top: 5, bottom: '', left: 20, right: '', unit: '%' },
		},
		style: [
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
					{ key: 'saleStyle', condition: '!=', value: 'shape3' },
				],
				selector:
					'{{WOPB}} .wopb-onsale-hot { margin:{{salesMargin}}; }',
			},
		],
	},
	salesPosition: {
		type: 'string',
		default: 'image',
		style: [
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
				],
			},
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
					{ key: 'saleStyle', condition: '==', value: 'shape2' },
					{ key: 'salesPosition', condition: '==', value: 'image' },
				],
				selector: '{{WOPB}} .wopb-image-wrapper { overflow:hidden; }',
			},
			{
				depends: [
					{ key: 'showSaleBadge', condition: '==', value: true },
					{ key: 'saleStyle', condition: '==', value: 'shape3' },
					{ key: 'salesPosition', condition: '==', value: 'image' },
				],
				selector: '{{WOPB}} .wopb-image-wrapper { overflow:hidden; }',
			},
		],
	},
	salesLocation: {
		type: 'string',
		default: 'top_left',
		style: [
			{
				depends: [
					{
						key: 'salesLocation',
						condition: '==',
						value: 'top_left',
					},
					{ key: 'showSaleBadge', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-onsale-hot {top:0;left:0;}',
			},
			{
				depends: [
					{
						key: 'salesLocation',
						condition: '==',
						value: 'top_center',
					},
					{ key: 'showSaleBadge', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-onsale-hot{top:0;}',
			},
			{
				depends: [
					{
						key: 'salesLocation',
						condition: '==',
						value: 'top_right',
					},
					{ key: 'showSaleBadge', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-onsale-hot {top:0;right:0;}',
			},
			{
				depends: [
					{
						key: 'salesLocation',
						condition: '==',
						value: 'bottom_left',
					},
					{ key: 'showSaleBadge', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-onsale-hot {bottom:0;left:0;}',
			},
			{
				depends: [
					{
						key: 'salesLocation',
						condition: '==',
						value: 'bottom_center',
					},
					{ key: 'showSaleBadge', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-onsale-hot {bottom:0;}',
			},
			{
				depends: [
					{
						key: 'salesLocation',
						condition: '==',
						value: 'bottom_right',
					},
					{ key: 'showSaleBadge', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-onsale-hot {bottom:0;right:0;}',
			},
			{
				depends: [
					{
						key: 'salesLocation',
						condition: '==',
						value: 'middle_left',
					},
					{ key: 'showSaleBadge', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-onsale-hot {left:0;}',
			},
			{
				depends: [
					{
						key: 'salesLocation',
						condition: '==',
						value: 'middle_center',
					},
					{ key: 'salesPosition', condition: '==', value: 'image' },
					{ key: 'showSaleBadge', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-image-wrapper {align-items:center;}',
			},
			{
				depends: [
					{
						key: 'salesLocation',
						condition: '==',
						value: 'middle_center',
					},
					{ key: 'showSaleBadge', condition: '==', value: true },
				],
			},
			{
				depends: [
					{
						key: 'salesLocation',
						condition: '==',
						value: 'middle_right',
					},
					{ key: 'showSaleBadge', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-onsale-hot {right:0;}',
			},
		],
	},

	/*==========================
      General Advance Settings
  ==========================*/
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [ { selector: 'div:has(> {{WOPB}}) {display:none;}' } ],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [ { selector: 'div:has(> {{WOPB}}) {display:none;}' } ],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [ { selector: 'div:has(> {{WOPB}}) {display:none;}' } ],
	},
	advanceCss: {
		type: 'string',
		default: '',
		style: [ { selector: '' } ],
	},
};
export default attributes;
