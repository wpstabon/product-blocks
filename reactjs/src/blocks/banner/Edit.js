const { __ } = wp.i18n;
const { InspectorControls, InnerBlocks } = wp.blockEditor;
const { MediaUpload } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import BannerSetting from './Setting';
import ToolBarElement from '../../helper/ToolBarElement';
import icons from '../../helper/icons';
import { SaleBadge } from './components';
import useBlockId from '../../helper/hooks/use-block-id';

const { useState } = wp.element;

export default function Edit( props ) {
	const [ section, setSection ] = useState( 'Content' );

	const { setAttributes, name, attributes, clientId, className } = props;

	const {
		blockId,
		advanceId,
		layout,
		showImage,
		img,
		showSaleBadge,
		salesPosition,
		showBgOverlay,
		previewImg,
	} = attributes;

	useBlockId( props, true );
	const store = {
		setAttributes,
		name,
		attributes,
		setSection,
		section,
		clientId,
	};

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/banner', blockId );
	}

	if ( previewImg ) {
		return (
			<img
				style={ { marginTop: '0px', width: '420px' } }
				src={ previewImg }
			/>
		);
	}

	return (
		<>
			<InspectorControls>
				<BannerSetting store={ store } />
			</InspectorControls>
			<ToolBarElement
				include={ [
					{
						type: 'template',
					},
					{
						type: 'layout',
						block: 'button',
						key: 'layout',
						label: __( 'Style', 'product-blocks' ),
						options: [
							{
								img: 'assets/img/blocks/banner/layout1.svg',
								label: __( 'Layout 1', 'product-blocks' ),
								value: 'layout1',
							},
							{
								img: 'assets/img/blocks/banner/layout2.svg',
								label: __( 'Layout 2', 'product-blocks' ),
								value: 'layout2',
							},
							{
								img: 'assets/img/blocks/banner/layout3.svg',
								label: __( 'Layout 3', 'product-blocks' ),
								value: 'layout3',
							},
							{
								img: 'assets/img/blocks/banner/layout4.svg',
								label: __( 'Layout 4', 'product-blocks' ),
								value: 'layout4',
							},
						],
					},
				] }
				store={ store }
			/>
			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className } wopb-banner-${ layout }` }
			>
				<div className={ `wopb-banner-wrapper` }>
					<div className={ `wopb-banner-section` }>
						{ showBgOverlay && (
							<div className={ `wopb-banner-overlay` } />
						) }
						{ showSaleBadge && salesPosition == 'banner' && (
							<SaleBadge
								attributes={ attributes }
								setAttributes={ setAttributes }
							/>
						) }
						<div className={ `wopb-content-wrapper` }>
							<InnerBlocks
								template={ [
									[
										'product-blocks/heading',
										{
											headingText:
												'Flash\u003cbr\u003eSale',
											headingStyle: 'style1',
											headingTag: 'div',
											headingTypo: {
												openTypography: 1,
												size: { lg: 64, unit: 'px' },
												height: { lg: 64, unit: 'px' },
												style: 'italic',
												family: '',
												weight: 700,
												transform: 'uppercase',
											},
											headingColor: '#fff',
											headingSpacing: {
												lg: 24,
												unit: 'px',
											},
										},
									],
									[
										'product-blocks/heading',
										{
											headingText:
												'Get ready for unbeatable deals at our 72-Hour \u003cbr\u003eFlash Sale! For a limited time only',
											headingStyle: 'style1',
											headingTag: 'div',
											headingTypo: {
												openTypography: 1,
												size: { lg: 14, unit: 'px' },
												height: { lg: 22, unit: 'px' },
												family: '',
												weight: 400,
											},
											headingColor: '#fff',
											headingSpacing: {
												lg: 24,
												unit: 'px',
											},
										},
									],
									[
										'product-blocks/heading',
										{
											headingText:
												'Start Form \u003cb\u003e$20\u003c/b\u003e Only',
											headingTag: 'div',
											headingTypo: {
												openTypography: 1,
												size: { lg: 26, unit: 'px' },
												height: { lg: 36, unit: 'px' },
												family: '',
												weight: 400,
											},
											headingColor: '#fff',
											headingSpacing: {
												lg: 32,
												unit: 'px',
											},
										},
									],
									[
										'product-blocks/button-group',
										{},
										[
											[
												'product-blocks/button',
												{
													btnText: 'SHOP NOW',
													btnBgCustomSize: {
														lg: {
															top: '14',
															bottom: '14',
															left: '28',
															right: '28',
															unit: 'px',
														},
													},
													btnTextTypo: {
														openTypography: 1,
														size: {
															lg: 16,
															unit: 'px',
														},
														height: {
															lg: 20,
															unit: 'px',
														},
														family: '',
														transform: 'uppercase',
														style: 'italic',
														weight: 500,
													},
													btnTextColor: '#070707',
													btnIconColor: '#070707',
													btnBgColor: {
														openColor: 1,
														type: 'color',
														color: '#fff',
													},
													btnBorder: {
														width: {
															top: 1,
															right: 1,
															bottom: 1,
															left: 1,
														},
														type: 'solid',
														color: '#070707',
														openBorder: 1,
													},
													btnRadius: {
														lg: {
															top: '0',
															right: '0',
															bottom: '0',
															left: '0',
															unit: 'px',
														},
														unit: 'px',
													},
													btnShadow: {
														width: {
															top: '4',
															right: '4',
															bottom: '0',
															left: '0',
															unit: 'px',
														},
														color: '#070707',
														openShadow: 1,
													},
												},
											],
										],
									],
								] }
							/>
						</div>
						{ showImage && (
							<div className={ `wopb-image-wrapper` }>
								<MediaUpload
									onSelect={ ( val ) =>
										setAttributes( { img: val } )
									}
									allowedTypes={ [ 'image' ] }
									multiple={ false }
									value={ img }
									render={ ( { open } ) => (
										<>
											{ img.url ? (
												<img
													className={ `wopb-image` }
													src={ img.url }
													alt=""
													style={ { cursor: 'copy' } }
													onClick={ open }
												/>
											) : (
												<div
													className={ `wopb-image-uploader` }
													onClick={ open }
												>
													<span
														className={ `wopb-plus-icon` }
													>
														{ icons.plusIcon2 }
													</span>
													<span
														className={ `wopb-instruction` }
													>
														{ __(
															'Upload Image',
															'product-blocks'
														) }
													</span>
												</div>
											) }
										</>
									) }
								/>
								{ showSaleBadge && salesPosition == 'image' && (
									<SaleBadge
										attributes={ attributes }
										setAttributes={ setAttributes }
									/>
								) }
								{ ! img.url && (
									<img
										className={ `wopb-image wopb-fallback-image` }
										src={
											wopb_data.url +
											`assets/img/wopb-fallback-img.png`
										}
										alt=""
									/>
								) }
							</div>
						) }
					</div>
				</div>
			</div>
		</>
	);
}
