const { InnerBlocks } = wp.blockEditor;
import { SaleBadge } from './components';

export default function Save( props ) {
	const { attributes, className } = props;

	const {
		blockId,
		advanceId,
		layout,
		showImage,
		img,
		showSaleBadge,
		salesPosition,
		showBannerLink,
		bannerLink,
		bannerNewTab,
		showBgOverlay,
	} = attributes;

	return (
		<div
			{ ...( advanceId && { id: advanceId } ) }
			className={ `wopb-block-${ blockId } ${ className } wopb-banner-${ layout }` }
		>
			<div className={ `wopb-banner-wrapper` }>
				<span
					className={ `wopb-banner-link` }
					{ ...( showBannerLink &&
						bannerLink && {
							href: bannerLink,
							target: bannerNewTab ? '_blank' : '_self',
						} ) }
				>
					<div className={ `wopb-banner-section` }>
						{ showBgOverlay && (
							<div className={ `wopb-banner-overlay` } />
						) }
						{ showSaleBadge && salesPosition == 'banner' && (
							<SaleBadge
								attributes={ attributes }
								setAttributes={ () => {} }
								forSave={ true }
							/>
						) }
						<div className={ `wopb-content-wrapper` }>
							<InnerBlocks.Content />
						</div>
						{ showImage && (
							<div className={ `wopb-image-wrapper` }>
								{ showSaleBadge && salesPosition == 'image' && (
									<SaleBadge
										attributes={ attributes }
										setAttributes={ () => {} }
										forSave={ true }
									/>
								) }
								{ img.url && (
									<img
										className={ `wopb-image` }
										src={ img.url }
										alt=""
									/>
								) }
								{ ! img.url && (
									<img
										className={ `wopb-image wopb-fallback-image` }
										src={
											wopb_data.url +
											`assets/img/wopb-fallback-img.png`
										}
										alt=""
									/>
								) }
							</div>
						) }
					</div>
				</span>
			</div>
		</div>
	);
}
