const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import useBlockId from '../../helper/hooks/use-block-id';
import icons from '../../helper/icons';
import Settings from './Settings';
const { useState, useEffect } = wp.element;

const Edit = ( props ) => {
	const { setAttributes, name, attributes, clientId, className } = props;
	const [ section, setSection ] = useState( '' );

	const {
		blockId,
		advanceId,
		showLabel,
		labelText,
		iconSvg,
		showCount,
		labelPosition,
		iconType,
		iconImage,
	} = attributes;

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/menu-wishlist', blockId );
	}

	const store = {
		setAttributes,
		name,
		attributes,
		setSection,
		section,
		clientId,
	};

	return (
		<>
			<InspectorControls>
				<Settings store={ store } />
			</InspectorControls>
			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<a
					className={ `wopb-menu-wishlist-wrapper wopb-label-${ labelPosition }` }
				>
					<div className="wopb-menu-wishlist-icon">
						{ iconType == 'icon' ? (
							icons[ iconSvg ]
						) : (
							<img
								src={
									iconImage?.url
										? iconImage?.url
										: wopb_data.url +
										  'assets/img/wopb_fallback.jpg'
								}
								alt="Wishlist Icon"
							/>
						) }
						{ showCount && (
							<div className="wopb-menu-wishlist-count">10</div>
						) }
					</div>
					{ showLabel && labelText && (
						<div
							className={ `wopb-menu-wishlist-label` }
							dangerouslySetInnerHTML={ { __html: labelText } }
						/>
					) }
				</a>
			</div>
		</>
	);
};

export default Edit;
