const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';
registerBlockType( 'product-blocks/product-search', {
	title: __( 'Search - WowStore', 'product-blocks' ),
	icon: (
		<div className={ `wopb-block-inserter-icon-section` }>
			<img
				src={ wopb_data.url + 'assets/img/blocks/search-block.svg' }
				alt={ __( 'Product Search', 'product-blocks' ) }
				style={ { width: '100%' } }
			/>
			{ ! wopb_data.active && (
				<span className={ `wopb-pro-label` }>
					{ __( 'Pro', 'product-blocks' ) }
				</span>
			) }
		</div>
	),
	category: 'product-blocks',
	description: (
		<span className="wopb-block-info">
			{ __( 'Search product by this block', 'product-blocks' ) }
			<a
				target="_blank"
				href="https://wpxpo.com/docs/wowstore/all-blocks/product-search-block/"
				rel="noreferrer"
			>
				{ __( 'Documentation', 'product-blocks' ) }
			</a>
		</span>
	),
	keywords: [
		__( 'Search', 'product-blocks' ),
		__( 'Product Search', 'product-blocks' ),
		__( 'Ajax Search', 'product-blocks' ),
	],
	edit: Edit,
	attributes,
	save() {
		return null;
	},
} );
