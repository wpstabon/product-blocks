const { memo } = wp.element;
const { __ } = wp.i18n;
import icons from '../../helper/icons';

export function SearchInputContent( { state, dispatch, attr } ) {
	return (
		<div className={ `wopb-input-section` }>
			<input
				type="text"
				className={ `wopb-search-input` }
				placeholder={ __( attr.searchPlaceHolder, 'product-blocks' ) }
				onChange={ ( e ) => {
					dispatch( {
						type: 'update',
						payload: { search: e.target.value },
					} );
				} }
				value={ state.search }
			/>
			{ ( state.taxTermList || state.productList ) && ! state.loading && (
				<span
					className={ `dashicons dashicons-no-alt wopb-clear` }
					onClick={ () => {
						dispatch( {
							type: 'update',
							payload: {
								search: '',
								taxTermList: '',
								productList: '',
							},
						} );
					} }
				/>
			) }
			{ state.loading && (
				<span className="wopb-loader-container wopb-spin-loader" />
			) }
		</div>
	);
}

export const SearchCategoryContent = memo( function SearchCategoryContent( {
	showSearchCategory,
	dispatch,
} ) {
	const categories = wopb_data.product_category;
	if ( showSearchCategory ) {
		return (
			<div className="wopb-search-category">
				<span className={ `wopb-separator wopb-left-separator` } />
				<div className="wopb-dropdown-select">
					<span className={ `wopb-selected-item` }>
						<span className={ `wopb-selected-text` }>
							{ __( 'All Categories', 'product-blocks' ) }
						</span>
						<i className="dashicons dashicons-arrow-down-alt2"></i>
					</span>
					<ul className={ `wopb-select-items` }>
						<li
							value=""
							onClick={ ( e ) => {
								dispatch( {
									type: 'update',
									payload: { category: '' },
								} );
							} }
						>
							{ __( 'All Categories', 'product-blocks' ) }
						</li>
						{ Object.keys( categories ).map( ( key ) => {
							const category = categories[ key ];
							return (
								<li
									value={ category.term_id }
									key={ key }
									onClick={ ( e ) => {
										dispatch( {
											type: 'update',
											payload: {
												category: category.term_id,
											},
										} );
									} }
								>
									{ category.name }
								</li>
							);
						} ) }
					</ul>
				</div>
				<span className={ `wopb-separator wopb-right-separator` } />
			</div>
		);
	}
} );

export const SearchIconContent = memo( function SearchIconContent( {
	showSearchIcon,
	fetchSearchItem,
} ) {
	return showSearchIcon ? (
		<a className={ `wopb-search-icon` } onClick={ () => fetchSearchItem() }>
			{ icons.search }
		</a>
	) : null;
} );

export function SearchItemContent( { attributes, state } ) {
	const {
		showProductImage,
		showProductTitle,
		productListLayout,
		showMoreResult,
		itemCatLabel,
		itemProductLabel,
	} = attributes;

	const { productList, taxTermList } = state;

	const viewLimit = 6;
	return (
		<>
			{ taxTermList && Object.keys( taxTermList ).length > 0 && (
				<>
					<div
						className={ `wopb-search-item-label wopb-search-item` }
					>
						{ __( itemCatLabel, 'product-blocks' ) }
					</div>
					<div className={ `wopb-tax-term-items wopb-search-items` }>
						{ Object.keys( taxTermList ).map( ( key, idx ) => {
							const term = taxTermList[ key ];
							let extendedItemClass = '';
							if ( idx + 1 > viewLimit && showMoreResult ) {
								extendedItemClass =
									' wopb-extended-item wopb-d-none';
							}
							return (
								<div
									className={
										`wopb-search-item` + extendedItemClass
									}
									key={ idx }
								>
									<a className="wopb-item-term">
										{ term.name }
									</a>
								</div>
							);
						} ) }
					</div>
				</>
			) }
			{ productList && productList.length > 0 && (
				<>
					<div
						className={ `wopb-search-item-label wopb-search-item` }
					>
						{ __( itemProductLabel, 'product-blocks' ) }
					</div>
					<div className={ `wopb-search-items` }>
						{ productList.map( ( product, idx ) => {
							let extendedItemClass = '';
							if ( idx + 1 > viewLimit && showMoreResult ) {
								extendedItemClass =
									' wopb-extended-item wopb-d-none';
							}
							return (
								<div
									className={
										`wopb-search-item wopb-block-item` +
										extendedItemClass
									}
									key={ idx }
								>
									<div className={ `wopb-item-details` }>
										{ showProductImage && (
											<span
												className={ `wopb-item-image` }
											>
												<img src={ product.image } />
											</span>
										) }
										<div
											className={ `wopb-item-title-section` }
										>
											{ showProductTitle && (
												<a
													href={ product.permalink }
													className={ `wopb-item-title` }
													dangerouslySetInnerHTML={ {
														__html: product.title,
													} }
												/>
											) }
											<RatingContent
												showProductRating={
													attributes.showProductRating
												}
												ratingAverage={
													product.rating_average
												}
											/>
											{ ( productListLayout == 2 ||
												productListLayout == 3 ) && (
												<PriceContent
													showProductPrice={
														attributes.showProductPrice
													}
													priceHtml={
														product.price_html
													}
												/>
											) }
										</div>
									</div>
									{ productListLayout == 1 && (
										<PriceContent
											showProductPrice={
												attributes.showProductPrice
											}
											priceHtml={ product.price_html }
										/>
									) }
								</div>
							);
						} ) }
					</div>
					{ productList.length > 0 && (
						<MoreResultContent
							showMoreResult={ showMoreResult }
							viewLimit={ viewLimit }
							totalProduct={ state.totalProduct }
						/>
					) }
				</>
			) }
			{ Object.keys( taxTermList ).length < 1 &&
				productList.length < 1 && (
					<div className="wopb-empty-result">
						<h2>{ __( 'No Result Found', 'product-blocks' ) }</h2>
					</div>
				) }
		</>
	);
}

export const RatingContent = memo( function RatingContent( {
	showProductRating,
	ratingAverage,
} ) {
	if ( showProductRating ) {
		const ratingAverageVal = ratingAverage || 0;
		const ratingAverageStyle = ratingAverageVal
			? ( ratingAverageVal / 5 ) * 100
			: 0;
		return (
			<div className={ `wopb-rating-section` }>
				<div className={ `wopb-star-rating` }>
					<span
						className={ `wopb-star-fill` }
						style={ { width: ratingAverageStyle } }
					>
						<strong itemProp="ratingValue" className="wopb-rating">
							{ ratingAverageVal }
						</strong>
					</span>
				</div>
			</div>
		);
	}
} );

export const PriceContent = memo( function PriceContent( {
	showProductPrice,
	priceHtml,
} ) {
	if ( showProductPrice ) {
		return (
			<div
				className={ `wopb-item-price` }
				dangerouslySetInnerHTML={ { __html: priceHtml } }
			/>
		);
	}
	return null;
} );

export const MoreResultContent = memo( function MoreResultContent( {
	showMoreResult,
	viewLimit,
	totalProduct,
} ) {
	if ( showMoreResult && totalProduct > viewLimit ) {
		const restProductCount = totalProduct - viewLimit;
		return (
			<div className={ `wopb-load-more-section` }>
				<a className={ `wopb-load-more` }>
					{ __(
						'More results..(' + restProductCount + ')',
						'product-blocks'
					) }
				</a>
				<a className={ `wopb-less-result wopb-d-none` }>
					{ __( 'Less results', 'product-blocks' ) }
				</a>
			</div>
		);
	}
	return null;
} );
