import LinkGenerator from '../../helper/LinkGenerator';

const { __ } = wp.i18n;
const { apiFetch } = wp;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import {
	GeneralAdvanced,
	CustomCssAdvanced,
	ResponsiveAdvanced,
	CommonSettings,
	wopbSupport,
	ProBlockLock,
	InitBlockPremade,
} from '../../helper/CommonPanel';
import { Sections, Section } from '../../helper/Sections';
import ToolBarElement from '../../helper/ToolBarElement';
import TemplateModal from '../../helper/TemplateModal';
import {
	SearchCategoryContent,
	SearchIconContent,
	SearchInputContent,
	SearchItemContent,
} from './components';
import useBlockId from '../../helper/hooks/use-block-id';

const { useEffect, useReducer, useCallback } = wp.element;

const ACTION_TYPES = {
	UPDATE: 'update',
};

const reducer = ( state, action ) => {
	switch ( action.type ) {
		case ACTION_TYPES.UPDATE:
			return {
				...state,
				...action.payload,
			};

		default:
			return state;
	}
};

export default function Edit( props ) {
	const [ state, dispatch ] = useReducer( reducer, {
		taxTermList: '',
		productList: '',
		totalProduct: '',
		search: '',
		category: '',
		productBlockID: [],
		loading: false,
		error: false,
		section: 'Content',
	} );

	const { setAttributes, name, attributes, clientId, className } = props;

	const {
		blockId,
		advanceId,
		productListLayout,
		showSearchCategory,
		showSearchIcon,
	} = attributes;

	const fetchSearchItem = useCallback( () => {
		if ( state.search && state.search.length >= 3 ) {
			dispatch( {
				type: ACTION_TYPES.UPDATE,
				payload: { loading: true },
			} );
			apiFetch( {
				path: '/wopb/product-search',
				method: 'POST',
				data: {
					search: state.search,
					category: state.category,
					attr: attributes,
					source: 'block_editor',
				},
			} )
				.then( ( res ) => {
					dispatch( {
						type: ACTION_TYPES.UPDATE,
						payload: {
							taxTermList: res.tax_terms,
							productList: res.products,
							totalProduct: res.total_product,
							loading: false,
						},
					} );
				} )
				.catch( ( error ) => {
					console.log( error );
					dispatch( {
						type: ACTION_TYPES.UPDATE,
						payload: { loading: false },
					} );
				} );
		} else {
			dispatch( {
				type: ACTION_TYPES.UPDATE,
				payload: { loading: false },
			} );
		}
	}, [ state.search, state.category, props.attributes ] );

	useBlockId( props, true );

	useEffect( () => {
		const debouncedSearch = setTimeout( () => {
			fetchSearchItem();
		}, 1000 );

		return () => clearTimeout( debouncedSearch );
	}, [ state.search ] );

	useEffect( () => {
		fetchSearchItem();
	}, [ state.category ] );

	const store = {
		setAttributes,
		name,
		attributes,
		clientId,
	};

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/product-search', blockId );
	}

	// if (!attributes.initPremade) {
	//     return <InitBlockPremade store={store} />;
	// }

	return (
		<>
			<InspectorControls>
				<TemplateModal
					prev={
						LinkGenerator(
							'https://www.wpxpo.com/wowstore/blocks/'
						) + '#demoid1653'
					}
					store={ store }
				/>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Settings', 'product-blocks' ) }
					>
						<CommonSettings
							initialOpen={ true }
							title={ __( 'General', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'select',
										key: 'searchLayout',
										image: true,
										imageSelect: true,
										label: __(
											'Choose Search Field Layout',
											'product-blocks'
										),
										options: [
											{
												value: '1',
												label: __(
													'Layout 1',
													'product-blocks'
												),
												img:
													wopb_data.url +
													'assets/img/blocks/product_search/search-layout-1.svg',
											},
											{
												value: '2',
												label: __(
													'Layout 2',
													'product-blocks'
												),
												img:
													wopb_data.url +
													'assets/img/blocks/product_search/search-layout-2.svg',
											},
											{
												value: '3',
												label: __(
													'Layout 3',
													'product-blocks'
												),
												img:
													wopb_data.url +
													'assets/img/blocks/product_search/search-layout-3.svg',
											},
											{
												value: '4',
												label: __(
													'Layout 4',
													'product-blocks'
												),
												img:
													wopb_data.url +
													'assets/img/blocks/product_search/search-layout-4.svg',
											},
											{
												value: '5',
												label: __(
													'Layout 5',
													'product-blocks'
												),
												img:
													wopb_data.url +
													'assets/img/blocks/product_search/search-layout-5.svg',
											},
										],
										variation: {
											1: {
												showSearchCategory: true,
											},
											2: {
												showSearchCategory: true,
											},
											3: {
												showSearchCategory: true,
											},
											4: {
												showSearchCategory: true,
											},
											5: {
												showSearchCategory: false,
											},
										},
									},
								},
								{
									data: {
										type: 'select',
										key: 'productListLayout',
										image: true,
										imageSelect: true,
										label: __(
											'Search Product List Layout',
											'product-blocks'
										),
										options: [
											{
												value: '1',
												label: __(
													'Layout 1',
													'product-blocks'
												),
												img:
													wopb_data.url +
													'assets/img/blocks/product_search/product-layout-1.svg',
											},
											{
												value: '2',
												label: __(
													'Layout 2',
													'product-blocks'
												),
												img:
													wopb_data.url +
													'assets/img/blocks/product_search/product-layout-2.svg',
											},
											{
												value: '3',
												label: __(
													'Layout 3',
													'product-blocks'
												),
												img:
													wopb_data.url +
													'assets/img/blocks/product_search/product-layout-3.svg',
											},
										],
										variation: {
											1: {
												columns: {
													lg: 1,
													sm: 1,
													xs: 1,
												},
												imageWidth: {
													lg: 40,
													unit: 'px',
												},
												itemSeparatorWidth: {
													lg: {
														left: 0,
														right: 1,
														top: 0,
														bottom: 1,
														unit: 'px',
													},
												},
											},
											2: {
												columns: {
													lg: 2,
													sm: 2,
													xs: 1,
												},
												imageWidth: {
													lg: 60,
													unit: 'px',
												},
												itemSeparatorWidth: {
													lg: {
														left: 0,
														right: 1,
														top: 0,
														bottom: 1,
														unit: 'px',
													},
												},
											},
											3: {
												columns: {
													lg: 3,
													sm: 3,
													xs: 2,
												},
												imageWidth: {
													lg: 90,
													unit: 'px',
												},
												itemSeparatorWidth: {
													lg: {
														left: 0,
														right: 0,
														top: 0,
														bottom: 0,
														unit: 'px',
													},
												},
											},
										},
									},
								},
								{
									data: {
										type: 'range',
										key: 'searchBlockWidth',
										min: 0,
										max: 800,
										step: 1,
										responsive: true,
										unit: true,
										label: __( 'Width', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'alignment',
										key: 'searchBlockAlign',
										responsive: false,
										label: __(
											'Alignment',
											'product-blocks'
										),
										disableJustify: true,
									},
								},
								{
									data: {
										type: 'range',
										key: 'columns',
										min: 1,
										max: 12,
										step: 1,
										responsive: true,
										label: __(
											'Columns',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'columnGap',
										min: 0,
										max: 100,
										step: 1,
										responsive: true,
										unit: [ 'px', 'em' ],
										label: __(
											'Column Gap',
											'product-blocks'
										),
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ false }
							title={ __( 'Search Form', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'text',
										key: 'searchPlaceHolder',
										label: __(
											'Placeholder Text',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'searchFormHeight',
										min: 0,
										max: 100,
										step: 1,
										responsive: true,
										unit: true,
										label: __(
											'Form Height',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'searchFormBorder',
										label: __(
											'Form Border',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'searchFormRadius',
										label: __(
											'Form Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'color2',
										key: 'searchFormBgColor',
										label: __(
											'Form Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'searchInputPadding',
										label: __(
											'Search Input Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'typography',
										key: 'searchInputTypo',
										label: __(
											'Search Input Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'searchInputColor',
										label: __(
											'Search Input Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'searchInputPlaceholderColor',
										label: __(
											'Input Placeholder Color',
											'product-blocks'
										),
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ false }
							depend="showSearchCategory"
							title={ __( 'Category', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'typography',
										key: 'selectedCategoryTypo',
										label: __(
											'Selected Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'categoryTypo',
										label: __(
											'Dropdown Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'categoryColor',
										label: __(
											'Text Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'categoryOptionColor',
										label: __(
											'Option Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'categoryOptionHvrColor',
										label: __(
											'Option Hover Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'categoryOptionHvrBg',
										label: __(
											'Option Hover BG',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'categoryOptionWrapBg',
										label: __(
											'Option Wrap BG',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'categoryWidth',
										min: 0,
										max: 500,
										step: 1,
										responsive: true,
										unit: true,
										label: __( 'Width', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'range',
										key: 'categoryLeftSeparatorWidth',
										min: 0,
										max: 100,
										step: 1,
										responsive: true,
										unit: true,
										label: __(
											'Left Separator Width',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'categoryRightSeparatorWidth',
										min: 0,
										max: 100,
										step: 1,
										responsive: true,
										unit: true,
										label: __(
											'Right Separator Width',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'categorySeparatorHeight',
										min: 0,
										max: 100,
										step: 1,
										responsive: true,
										unit: true,
										label: __(
											'Separator Height',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'categorySeparatorColor',
										label: __(
											'Separator Color',
											'product-blocks'
										),
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ false }
							depend="showSearchIcon"
							title={ __( 'Search Button', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'toggle',
										key: 'searchRedirect',
										label: __(
											'Click To Go Search Result Page',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'searchIconSize',
										min: 0,
										max: 100,
										step: 1,
										responsive: true,
										unit: true,
										label: __(
											'Icon Size',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'tab',
										content: [
											{
												name: 'normal',
												title: __(
													'Normal',
													'product-blocks'
												),
												options: [
													{
														type: 'color',
														key: 'searchIconColor',
														label: __(
															'Color',
															'product-blocks'
														),
													},
													{
														type: 'color2',
														key: 'searchButtonBgColor',
														label: __(
															'Button Bg Color',
															'product-blocks'
														),
													},
													{
														type: 'dimension',
														key: 'searchButtonRadius',
														label: __(
															'Button Radius',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
												],
											},
											{
												name: 'hover',
												title: __(
													'Hover',
													'product-blocks'
												),
												options: [
													{
														type: 'color',
														key: 'searchIconHoverColor',
														label: __(
															'Hover Color',
															'product-blocks'
														),
													},
													{
														type: 'color2',
														key: 'searchButtonBgHoverColor',
														label: __(
															'Button Hover Bg Color',
															'product-blocks'
														),
													},
													{
														type: 'dimension',
														key: 'searchButtonHoverRadius',
														label: __(
															'Button Hover Radius',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
												],
											},
										],
									},
								},
								{
									data: {
										type: 'range',
										key: 'searchButtonWidth',
										min: 0,
										max: 100,
										step: 1,
										responsive: true,
										unit: true,
										label: __(
											'Button Width',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'searchButtonMargin',
										label: __(
											'Button Margin',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'searchButtonPadding',
										label: __(
											'Button Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ false }
							depend="showMoreResult"
							title={ __(
								'More Result Button',
								'product-blocks'
							) }
							store={ store }
							include={ [
								{
									data: {
										type: 'typography',
										key: 'moreResultTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'moreResultColor',
										label: __(
											'Text Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'moreResultHoverColor',
										label: __(
											'Hover Color',
											'product-blocks'
										),
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ false }
							title={ __( 'Dropdown Style', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color2',
										key: 'dropdownBg',
										label: __(
											'Dropdown Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'dropdownBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'dropdownRadius',
										label: __( 'Radius', 'product-blocks' ),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'boxshadow',
										key: 'dropdownShadow',
										label: __(
											'Box Shadow',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'itemSpacingX',
										label: __(
											'Item Spacing X',
											'product-blocks'
										),
										min: 1,
										max: 150,
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'range',
										key: 'itemSpacingY',
										label: __(
											'Item Spacing Y',
											'product-blocks'
										),
										min: 1,
										max: 150,
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'color',
										key: 'itemSeparatorColor',
										label: __(
											'Separator Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'itemSeparatorWidth',
										responsive: true,
										label: __(
											'Separator Width',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'text',
										key: 'itemCatLabel',
										label: __(
											'Category Label',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'text',
										key: 'itemProductLabel',
										label: __(
											'Product Label',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'itemLabelTypo',
										label: __(
											'Label Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'itemLabelColor',
										label: __(
											'Label Color',
											'product-blocks'
										),
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ false }
							depend="showProductImage"
							title={ __( 'Product Image', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'range',
										key: 'imageWidth',
										min: 0,
										max: 500,
										step: 1,
										responsive: true,
										unit: true,
										label: __(
											'Image Width',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'imageHeight',
										min: 0,
										max: 100,
										step: 1,
										responsive: true,
										unit: true,
										label: __(
											'Image Height',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'imageRadius',
										label: __(
											'Image Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ false }
							depend="showProductTitle"
							title={ __( 'Product Title', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'typography',
										key: 'titleTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'titleColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'titleHoverColor',
										label: __(
											'Hover Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'searchHighlightColor',
										label: __(
											'Search Highlighted Color',
											'product-blocks'
										),
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ false }
							depend="showProductPrice"
							title={ __( 'Product Price', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'typography',
										key: 'priceTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'priceColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ false }
							depend="showProductRating"
							title={ __( 'Product Rating', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'range',
										key: 'ratingSize',
										min: 0,
										max: 100,
										step: 1,
										responsive: true,
										label: __(
											'Rating Size',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'ratingFillColor',
										label: __(
											'Fill Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'ratingEmptyColor',
										label: __(
											'Empty Color',
											'product-blocks'
										),
									},
								},
							] }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced initialOpen={ true } store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<ToolBarElement
				include={ [
					{
						type: 'template',
					},
				] }
				store={ store }
			/>
			<ProBlockLock
				img={ wopb_data.url + 'assets/img/blocks/search-block.svg' }
				name={ 'Product Search' }
				upgradeLink={
					'https://www.wpxpo.com/wowstore/pricing/?utm_source=db-wstore-editor&utm_medium=product-search-pro&utm_campaign=wstore-dashboard'
				}
				detailsLink={
					'https://www.wpxpo.com/wowstore/blocks/?utm_source=db-wstore-editor&utm_medium=product-search-demo&utm_campaign=wstore-dashboard' +
					'#demoid1653'
				}
			/>
			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div
					className={ `wopb-block-wrapper wopb-product-search-block ${
						! wopb_data.active ? 'wopb-wrapper-pro' : ''
					}` }
				>
					<div className={ `wopb-search-section` }>
						<form action={ `javascript:` }>
							<SearchInputContent
								state={ state }
								dispatch={ dispatch }
								attr={ attributes }
							/>
							<SearchCategoryContent
								showSearchCategory={ showSearchCategory }
								dispatch={ dispatch }
							/>
							<SearchIconContent
								showSearchIcon={ showSearchIcon }
								fetchSearchItem={ fetchSearchItem }
							/>
						</form>
					</div>
					{ ( state.taxTermList || state.productList ) && (
						<div
							className={
								`wopb-search-result wopb-layout-` +
								productListLayout
							}
						>
							<SearchItemContent
								attributes={ attributes }
								state={ state }
							/>
						</div>
					) }
				</div>
			</div>
		</>
	);
}
