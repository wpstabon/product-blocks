const attributes = {
	blockId: { type: 'string', default: '' },
	currentPostId: { type: 'string', default: '' },
	searchLayout: {
		type: 'string',
		default: '1',
		style: [
			{ depends: [ { key: 'searchLayout', condition: '==', value: 1 } ] },
			{
				depends: [
					{ key: 'searchLayout', condition: '==', value: 1 },
					{ key: 'showSearchIcon', condition: '==', value: false },
				],
				selector:
					'{{WOPB}} .wopb-search-section .wopb-search-category .wopb-right-separator {display:none;}',
			},
			{
				depends: [ { key: 'searchLayout', condition: '==', value: 2 } ],
				selector:
					'{{WOPB}} .wopb-search-section .wopb-input-section {order:2}{{WOPB}} .wopb-search-section .wopb-search-category {order:3;}{{WOPB}} .wopb-search-section .wopb-search-category .wopb-right-separator {display:none;}',
			},
			{
				depends: [ { key: 'searchLayout', condition: '==', value: 3 } ],
				selector:
					'{{WOPB}} .wopb-search-section .wopb-input-section {order:2}{{WOPB}} .wopb-search-section .wopb-search-icon {order:3}{{WOPB}} .wopb-search-section .wopb-search-category .wopb-left-separator {display:none;}',
			},
			{
				depends: [ { key: 'searchLayout', condition: '==', value: 4 } ],
				selector:
					'{{WOPB}} .wopb-search-section .wopb-search-category {order:3;}{{WOPB}} .wopb-search-section .wopb-search-category .wopb-right-separator {display:none;}',
			},
			{ depends: [ { key: 'searchLayout', condition: '==', value: 5 } ] },
			{
				depends: [
					{ key: 'searchLayout', condition: '==', value: 5 },
					{ key: 'showSearchIcon', condition: '==', value: false },
				],
				selector:
					'{{WOPB}} .wopb-search-section .wopb-search-category .wopb-right-separator {border-right:none;}',
			},
		],
	},
	productListLayout: {
		type: 'string',
		default: '1',
		style: [
			{
				depends: [
					{ key: 'productListLayout', condition: '==', value: 1 },
				],
			},
			{
				depends: [
					{ key: 'productListLayout', condition: '==', value: 2 },
				],
			},
			{
				depends: [
					{ key: 'productListLayout', condition: '==', value: 3 },
				],
				selector:
					'{{WOPB}} .wopb-search-result .wopb-item-details {flex-direction: column;}{{WOPB}} .wopb-search-result .wopb-item-title-section .wopb-item-price {justify-content: center;}{{WOPB}} .wopb-search-result .wopb-item-title-section {text-align: center;}',
			},
		],
	},
	columns: {
		type: 'object',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-search-result .wopb-search-items { grid-template-columns: repeat({{columns}}, 1fr); }',
			},
		],
	},
	searchBlockWidth: {
		type: 'object',
		default: { lg: '100', unit: '%' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-search-block { width: {{searchBlockWidth}}; }',
			},
		],
	},
	searchBlockAlign: {
		type: 'string',
		default: 'left',
		style: [
			{
				selector:
					'{{WOPB}} { display: flex;justify-content:{{searchBlockAlign}}; }',
			},
		],
	},
	columnGap: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-search-result .wopb-search-items{ column-gap: {{columnGap}}; }',
			},
		],
	},
	searchInputTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '',
		},
		style: [ { selector: '{{WOPB}} .wopb-search-input' } ],
	},
	searchPlaceHolder: {
		type: 'string',
		default: 'Search for products...',
	},
	searchFormHeight: {
		type: 'object',
		default: { lg: '48', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-search-section form { height: {{searchFormHeight}}; }',
			},
		],
	},
	searchFormBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#A1A1A1',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-search-section form' } ],
	},
	searchFormRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-search-section form { border-radius:{{searchFormRadius}}; }',
			},
		],
	},
	searchInputPadding: {
		type: 'object',
		default: { lg: { left: '20', right: '10', unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-search-input { padding:{{searchInputPadding}} !important; }',
			},
		],
	},
	searchInputColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-search-input { color:{{searchInputColor}} !important; } {{WOPB}} .wopb-search-input:focus { color:{{searchInputColor}} !important;caret-color: {{searchInputColor}}; } {{WOPB}} .wopb-input-section .wopb-clear { color:{{searchInputColor}} !important; }{{WOPB}} .wopb-input-section .wopb-loader-container::before, {{WOPB}} .wopb-input-section .wopb-loader-container::after { border-color:{{searchInputColor}} {{searchInputColor}} transparent transparent !important; }',
			},
		],
	},
	searchInputPlaceholderColor: {
		type: 'string',
		default: '#6E6E6E',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-search-input::placeholder { color : {{searchInputPlaceholderColor}}; }',
			},
		],
	},
	searchFormBgColor: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#fff' },
		style: [ { selector: '{{WOPB}} .wopb-search-section form' } ],
	},
	showSearchCategory: { type: 'boolean', default: true },
	selectedCategoryTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{
				depends: [
					{ key: 'showSearchCategory', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-category .wopb-selected-item .wopb-selected-text, {{WOPB}} .wopb-search-category .wopb-selected-item .dashicons',
			},
		],
	},
	categoryTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{
				depends: [
					{ key: 'showSearchCategory', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-category .wopb-select-items li',
			},
		],
	},
	categoryColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				depends: [
					{ key: 'showSearchCategory', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-category .wopb-selected-item { color:{{categoryColor}}; }',
			},
		],
	},
	categoryOptionColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				depends: [
					{ key: 'showSearchCategory', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-category .wopb-select-items li { color:{{categoryOptionColor}}; }',
			},
		],
	},
	categoryOptionHvrColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				depends: [
					{ key: 'showSearchCategory', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-category .wopb-select-items li:hover { color:{{categoryOptionHvrColor}}; }',
			},
		],
	},
	categoryOptionHvrBg: {
		type: 'string',
		default: '#E6E6E6',
		style: [
			{
				depends: [
					{ key: 'showSearchCategory', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-category .wopb-select-items li:hover { background-color:{{categoryOptionHvrBg}}; }',
			},
		],
	},
	categoryOptionWrapBg: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [
					{ key: 'showSearchCategory', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-category .wopb-select-items { background-color:{{categoryOptionWrapBg}}; }',
			},
		],
	},
	categoryWidth: {
		type: 'object',
		default: { lg: '165', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showSearchCategory', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-category { width: {{categoryWidth}}; }',
			},
		],
	},
	categoryLeftSeparatorWidth: {
		type: 'object',
		default: { lg: '1', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showSearchCategory', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-section .wopb-search-category .wopb-separator.wopb-left-separator {width: {{categoryLeftSeparatorWidth}};}',
			},
		],
	},
	categoryRightSeparatorWidth: {
		type: 'object',
		default: { lg: '1', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showSearchCategory', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-section .wopb-search-category .wopb-separator.wopb-right-separator {width: {{categoryRightSeparatorWidth}};}',
			},
		],
	},
	categorySeparatorHeight: {
		type: 'object',
		default: { lg: '100', ulg: '%', unit: '%' },
		style: [
			{
				depends: [
					{ key: 'showSearchCategory', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-section .wopb-search-category .wopb-separator{ height: {{categorySeparatorHeight}}; }',
			},
		],
	},
	categorySeparatorColor: {
		type: 'string',
		default: '#A1A1A1',
		style: [
			{
				depends: [
					{ key: 'showSearchCategory', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-section .wopb-search-category .wopb-separator { background-color:{{categorySeparatorColor}};}',
			},
		],
	},
	showSearchIcon: { type: 'boolean', default: true },
	searchRedirect: { type: 'boolean', default: false },
	searchIconSize: {
		type: 'object',
		default: { lg: '20', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showSearchIcon', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-icon svg { height: {{searchIconSize}}; }',
			},
		],
	},
	searchIconColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [
					{ key: 'showSearchIcon', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-icon svg { fill:{{searchIconColor}}; } {{WOPB}} .wopb-search-icon svg path { stroke:{{searchIconColor}}; }',
			},
		],
	},
	searchButtonBgColor: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#070707' },
		style: [
			{
				depends: [
					{ key: 'showSearchIcon', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-search-icon',
			},
		],
	},
	searchButtonRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showSearchIcon', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-icon { border-radius:{{searchButtonRadius}}; }',
			},
		],
	},
	searchIconHoverColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [
					{ key: 'showSearchIcon', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-icon:hover svg { fill:{{searchIconHoverColor}}; } {{WOPB}} .wopb-search-icon:hover svg path { stroke:{{searchIconHoverColor}}; }',
			},
		],
	},
	searchButtonBgHoverColor: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#FF176B' },
		style: [
			{
				depends: [
					{ key: 'showSearchIcon', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-search-icon:hover',
			},
		],
	},
	searchButtonHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showSearchIcon', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-icon:hover { border-radius:{{SearchButtonHoverRadius}}; }',
			},
		],
	},
	searchButtonWidth: {
		type: 'object',
		default: { lg: '40', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showSearchIcon', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-icon { width: {{searchButtonWidth}}; }',
			},
		],
	},
	searchTextGap: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showSearchIcon', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-input {letter-spacing: {{searchTextGap}}; }',
			},
		],
	},
	searchButtonMargin: {
		type: 'object',
		default: {
			lg: { top: '4', bottom: '4', left: '4', right: '4', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-search-icon { margin:{{searchButtonMargin}}; }',
			},
		],
	},
	searchButtonPadding: {
		type: 'object',
		default: {
			lg: { top: '0', bottom: '0', left: '5', right: '5', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-search-icon { padding:{{searchButtonPadding}}; }',
			},
		],
	},
	showMoreResult: { type: 'boolean', default: true },
	moreResultTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '15', unit: 'px' },
			height: { lg: '', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{
				depends: [
					{ key: 'showMoreResult', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-load-more, {{WOPB}} .wopb-less-result',
			},
		],
	},
	moreResultColor: {
		type: 'string',
		default: '#2189fa',
		style: [
			{
				depends: [
					{ key: 'showMoreResult', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-load-more, {{WOPB}} .wopb-less-result { color:{{moreResultColor}}; }',
			},
		],
	},
	moreResultHoverColor: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [
					{ key: 'showMoreResult', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-load-more:hover, {{WOPB}} .wopb-less-result:hover { color:{{moreResultHoverColor}}; }',
			},
		],
	},
	dropdownBg: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#fff' },
		style: [ { selector: '{{WOPB}} .wopb-search-result' } ],
	},
	dropdownWidth: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-search-result { max-width: {{dropdownWidth}}; }',
			},
		],
	},
	dropdownBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 0, bottom: 1, left: 1 },
			color: '#A1A1A1',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-search-result' } ],
	},
	dropdownRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-search-result { border-radius:{{dropdownRadius}}; }',
			},
		],
	},
	dropdownShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '',
		},
		style: [ { selector: '{{WOPB}} .wopb-search-result' } ],
	},
	itemSpacingX: {
		type: 'object',
		default: { lg: 15, unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-search-result .wopb-search-item {padding-left:{{itemSpacingX}};padding-right:{{itemSpacingX}}; }',
			},
		],
	},
	itemSpacingY: {
		type: 'object',
		default: { lg: 15, unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-search-result .wopb-search-item {padding-top:{{itemSpacingY}};padding-bottom:{{itemSpacingY}}; }',
			},
		],
	},
	itemSeparatorColor: {
		type: 'string',
		default: '#A1A1A1',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-search-result .wopb-search-item { border-color:{{itemSeparatorColor}}; }{{WOPB}} .wopb-search-result .wopb-search-item-label.wopb-search-item { border-color:{{itemSeparatorColor}}; }{{WOPB}} .wopb-search-result .wopb-load-more-section {border-color: {{itemSeparatorColor}};}',
			},
		],
	},
	itemSeparatorWidth: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '1', left: '', right: '1', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-search-result .wopb-search-item { border-style:solid;border-width:{{itemSeparatorWidth}}; }',
			},
		],
	},
	itemCatLabel: { type: 'string', default: 'Categories' },
	itemProductLabel: { type: 'string', default: 'Products' },
	itemLabelTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '15', unit: 'px' },
			height: { lg: '', unit: 'px' },
			decoration: 'none',
			transform: 'uppercase',
			family: '',
			weight: '',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-search-result .wopb-search-item-label',
			},
		],
	},
	itemLabelColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-search-result .wopb-search-item-label { color:{{itemLabelColor}}; }',
			},
		],
	},
	showProductImage: { type: 'boolean', default: true },
	imageWidth: {
		type: 'object',
		default: { lg: '40', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showProductImage', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-result .wopb-item-image { width: {{imageWidth}}; }',
			},
		],
	},
	imageHeight: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showProductImage', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-result .wopb-item-image { height: {{imageHeight}}; }',
			},
		],
	},
	imageRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showProductImage', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-result .wopb-item-image img { border-radius:{{imageRadius}}; }',
			},
		],
	},
	showProductTitle: { type: 'boolean', default: true },
	titleTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{
				depends: [
					{ key: 'showProductTitle', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-search-result .wopb-item-title',
			},
		],
	},
	titleColor: {
		type: 'string',
		default: '#333',
		style: [
			{
				depends: [
					{ key: 'showProductTitle', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-result .wopb-item-title, {{WOPB}} .wopb-search-result .wopb-item-title p { color:{{titleColor}}; }',
			},
		],
	},
	titleHoverColor: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [
					{ key: 'showProductTitle', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-result .wopb-item-title:hover, {{WOPB}} .wopb-search-result .wopb-item-title p:hover { color:{{titleHoverColor}}; }',
			},
		],
	},
	searchHighlightColor: {
		type: 'string',
		default: '#333',
		style: [
			{
				depends: [
					{ key: 'showProductTitle', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-result .wopb-highlight { color:{{searchHighlightColor}}; }',
			},
		],
	},
	showProductPrice: { type: 'boolean', default: true },
	priceTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{
				depends: [
					{ key: 'showProductPrice', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-search-result .wopb-item-price',
			},
		],
	},
	priceColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				depends: [
					{ key: 'showProductPrice', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-result .wopb-item-price { color:{{priceColor}}; }',
			},
		],
	},
	showProductRating: { type: 'boolean', default: true },
	ratingSize: {
		type: 'object',
		default: { lg: '12', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'showProductRating', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-result .wopb-star-rating { font-size:{{ratingSize}}; }',
			},
		],
	},
	ratingFillColor: {
		type: 'string',
		default: '#FFAF38',
		style: [
			{
				depends: [
					{ key: 'showProductRating', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-result .wopb-star-rating .wopb-star-fill { color:{{ratingFillColor}}; }',
			},
		],
	},
	ratingEmptyColor: {
		type: 'string',
		default: '#E0E0E0',
		style: [
			{
				depends: [
					{ key: 'showProductRating', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-search-result .wopb-star-rating:before { color:{{ratingEmptyColor}}; }',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#f5f5f5' },
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#ff176b' },
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapMargin: {
		type: 'object',
		default: { lg: { top: '', bottom: '', unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: { type: 'string', default: '', style: [ { selector: '' } ] },
};
export default attributes;
