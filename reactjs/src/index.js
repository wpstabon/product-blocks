const { unregisterBlockType } = wp.blocks;
const { domReady } = wp;
const { subscribe } = wp.data;
const { __ } = wp.i18n;

// Save Style CSS within Database/ File
import ProductxSettings from './helper/ProductxSettings';
import Library from './helper/common/Library.js';
import { ParseCssGen, ParseWidgetGen } from './helper/ParseCss';

import './blocks/product_grid_1';
import './blocks/product_grid_2';
import './blocks/product_grid_3';
import './blocks/product_list_1';
import './blocks/product_category_1';
import './blocks/product_category_2';
import './blocks/product_category_3';
import './blocks/image';
import './blocks/heading';
import './blocks/product_search';
import './blocks/wrapper';
import './blocks/filter';
import './blocks/currency_switcher';
import './blocks/row';
import './blocks/row/column';
import './blocks/product_slider';
import './blocks/button_group';
import './blocks/button_group/button';
import './blocks/advance_list';
import './blocks/advance_list/list';
import './blocks/banner';
import './blocks/menu_wishlist';
import './blocks/menu_compare';

// Builder Blocks
import './builder/product_title';
import './builder/product_short';
import './builder/product_description';
import './builder/product_stock';
import './builder/product_price';
import './builder/product_rating';
import './builder/product_meta';
import './builder/product_breadcrumb';
import './builder/product_image';
import './builder/product_cart';
import './builder/product_additional_info/index.js';
import './builder/product_tab';
import './builder/product_review';
import './builder/archive_title';
import './builder/cart_table';
import './builder/cart_total';
import './builder/free_shipping_progress_bar';
import './builder/checkout/checkout_coupon';
import './builder/checkout/checkout_billing';
import './builder/checkout/checkout_shipping';
import './builder/checkout/checkout_additional_information';
import './builder/checkout/checkout_login';
import './builder/checkout/checkout_payment_method';
import './builder/checkout/checkout_order_review';
import './builder/thank_you/order_conformation';
import './builder/thank_you/address';
import './builder/thank_you/order_details';
import './builder/thank_you/order_payment';
import './builder/my_account';
import './builder/social_share';

window.wopbDevice = 'lg';
window.bindCssProductX = false;
let ticker = false;

wp.blocks.updateCategory( 'product-blocks', {
	icon: (
		<img
			className="wopb-insert-box-popup"
			style={ { height: '25px', marginTop: '-5px' } }
			src={ wopb_data.url + 'assets/img/logo-sm.svg' }
			alt={ 'WooCommerce Product Blocks' }
		/>
	),
} );

// css save function from gutenberg post/page/custom editor
const savePostCss = () => {
	const unsubscribe = subscribe( () => {
		const {
			isSavingPost,
			isAutosavingPost,
			isPreviewingPost,
			isEditedPostSaveable,
		} = wp.data.select( 'core/editor' );
		const editor = wp.data.select( 'core/editor' );
		if ( editor != null ) {
			if ( isEditedPostSaveable() && ! isPreviewingPost() ) {
				ticker = false;
				window.bindCssProductX = false;
			}
			if ( ! isAutosavingPost() ) {
				if ( isSavingPost() ) {
					if ( window.bindCssProductX == false ) {
						window.bindCssProductX = true;
						ticker = true;
						ParseCssGen( { handleBindCss: false, preview: false } );
					}
				}
			} else if (
				isPreviewingPost() &&
				window.bindCssProductX == false
			) {
				window.bindCssProductX = true;
				ParseCssGen( { handleBindCss: true, preview: true } );
			}
		}
	} );
};
// FSE Editor is save
jQuery( document ).on(
	'click',
	'.editor-entities-saved-states__save-button',
	function () {
		if ( ! ticker ) {
			ticker = true;
			ParseCssGen( { handleBindCss: true, preview: false } );
			setTimeout( () => {
				ticker = false;
			}, 1000 );
		}
	}
);

// save css inside widget area
const saveWidgetAction = () => {
	setTimeout( () => {
		jQuery( '.edit-widgets-header__actions .components-button' ).click(
			() => {
				const data = [];
				if ( window.bindCssProductX === false ) {
					jQuery( '.block-editor-block-list__block' ).each(
						function () {
							if (
								jQuery( this )
									.data( 'type' )
									.includes( 'product-blocks/' )
							) {
								data.push( {
									id: jQuery( this ).data( 'block' ),
									name: jQuery( this ).data( 'type' ),
								} );
							}
						}
					);
					if ( data.length > 0 ) {
						ParseWidgetGen( data );
					}
				}
			}
		);
	}, 0 );
};

function appendImportButton() {
	const unsubscribe = subscribe( () => {
		let toolbar =
			document.querySelector( '.editor-header__toolbar' ) ??
			document.querySelector( '.edit-post-header__toolbar' );
		if ( ! toolbar ) {
			toolbar = document.querySelector( '.edit-post-header-toolbar' );
			if ( ! toolbar ) {
				return;
			}
		}
		let buttonDiv = document.createElement( 'div' );
		if ( toolbar.querySelector( '.sab-toolbar-insert-layout' ) ) {
			buttonDiv = toolbar.querySelector( '.sab-toolbar-insert-layout' );
		}
		buttonDiv.className = 'sab-toolbar-insert-layout';
		const html = `<button id="wopbInsertButton" class="wopb-popup-button" aria-label="Insert Layout">
                        <img src="${
							wopb_data.url + 'assets/img/logo-sm.svg'
						}">${
							wopb_data.builder_type
								? __( 'Builder Library', 'product-blocks' )
								: __( 'Block Library', 'product-blocks' )
						}
                    </button>`;
		buttonDiv.innerHTML += html;
		toolbar.appendChild( buttonDiv );
		document
			.getElementById( 'wopbInsertButton' )
			.addEventListener( 'click', wopbInsertLayout );
		unsubscribe();
	} );
}

function wopbInsertLayout() {
	const node = document.createElement( 'div' );
	node.className = 'wopb-builder-modal wopb-blocks-layouts';
	document.body.appendChild( node );
	wp.element.render( <Library isShow={ true } />, node );
	document.body.classList.add( 'wopb-popup-open' );
}

domReady( function () {
	if ( document.querySelector( '.widgets-php' ) ) {
		saveWidgetAction();
	} else if ( document.querySelector( '.block-editor-page' ) ) {
		savePostCss();
		if ( wopb_data.hide_import_btn != 'yes' ) {
			appendImportButton();
		}
	}

	// Unregister Block Type Depending on Settings
	const productBlocks = {
		heading: 'heading',
		image: 'image',
		product_grid_1: 'product-grid-1',
		product_grid_2: 'product-grid-2',
		product_grid_3: 'product-grid-3',
		product_list_1: 'product-list-1',
		product_category_1: 'product-category-1',
		product_category_2: 'product-category-2',
		product_category_3: 'product-category-3',
		product_search: 'product-search',
		product_filter: 'filter',
		wrapper: 'wrapper',
		currency_switcher: 'currency-switcher',
		product_slider: 'product-slider',
		button_group: 'button-group',
		advance_list: 'advance-list',
		banner: 'banner',
		wishlist: 'menu-wishlist',
		compare: 'menu-compare',

		// Builder Blocks
		builder_product_title: 'product-title',
		builder_product_short_description: 'product-short',
		builder_product_description: 'product-description',
		builder_product_stock: 'product-stock',
		builder_product_price: 'product-price',
		builder_product_rating: 'product-rating',
		builder_product_meta: 'product-meta',
		builder_product_breadcrumb: 'product-breadcrumb',
		builder_product_image: 'product-image',
		builder_product_cart: 'product-cart',
		builder_product_additional_info: 'product-additional-info',
		builder_product_tab: 'product-tab',
		builder_product_review: 'product-review',
		builder_archive_title: 'archive-title',
		builder_thankyou_address: 'thankyou-address',
		builder_thankyou_order_conformation: 'thankyou-order-conformation',
		builder_thankyou_order_details: 'thankyou-order-details',
		builder_thankyou_order_payment: 'thankyou-order-payment',
		builder_social_share: 'social-share',
		builder_cart_table: 'cart-table',
		builder_cart_total: 'cart-total',
		builder_free_shipping_progress_bar: 'free-shipping-progress-bar',
		builder_checkout_login: 'checkout-login',
		builder_checkout_billing: 'checkout-billing',
		builder_checkout_shipping: 'checkout-shipping',
		builder_checkout_additional_information:
			'checkout-additional-information',
		builder_checkout_coupon: 'checkout-coupon',
		builder_checkout_payment_method: 'checkout-payment-method',
		builder_checkout_order_review: 'checkout-order-review',
		builder_my_account: 'my-account',
	};

	//Normal block check
	Object.keys( productBlocks ).forEach( ( key ) => {
		if (
			wopb_data.settings.hasOwnProperty( key ) &&
			wopb_data.settings[ key ] !== 'yes'
		) {
			unregisterBlockType( 'product-blocks/' + productBlocks[ key ] );
		}
	} );
} );

wp.plugins.registerPlugin( 'wopb-productx-settings', {
	render() {
		return <ProductxSettings />;
	},
} );
