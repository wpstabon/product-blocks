const { useState, useRef, useEffect } = wp.element;

const Units = ( { unit, value, setSettings } ) => {
	const myRef = useRef();
	const [ isOpen, setOpen ] = useState( false );

	const handleClickOutside = ( e ) => {
		if ( ! myRef.current.contains( e.target ) ) {
			setOpen( false );
		}
	};

	useEffect( () => {
		document.addEventListener( 'mousedown', handleClickOutside );
		return () =>
			document.removeEventListener( 'mousedown', handleClickOutside );
	}, [ handleClickOutside ] );

	return (
		<span
			ref={ myRef }
			className={ isOpen ? 'wopb-unit-open' : 'wopb-unit-close' }
		>
			<div className="wopb-field-unit-list">
				<span
					className="wopb-unit-checked"
					onClick={ () => setOpen( true ) }
				>
					{ value || 'px' }
				</span>
				<div className="wopb-field-unit-dropdown">
					{ ( typeof unit === 'object'
						? unit
						: [ 'px', 'em', 'rem', '%', 'vh', 'vw' ]
					).map( ( val, k ) => (
						<span
							key={ k }
							className={ val == value ? 'wopb-unit-active' : '' }
							onClick={ () => {
								setSettings( val, 'unit' );
							} }
						>
							{ val }
						</span>
					) ) }
				</div>
			</div>
		</span>
	);
};

export default Units;
