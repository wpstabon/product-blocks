const { __ } = wp.i18n;
const { useState, useRef, useEffect } = wp.element;
import icons from '../../icons';

const ResponsiveDevice = ( props ) => {
	const { setDevice, device } = props;
	const myRef = useRef();

	const _set = ( value ) => {
		setDevice( value );
	};

	return (
		<div ref={ myRef } className={ `wopb-responsive-device` }>
			<div
				className={
					`wopb-responsive-dropdown wopb-responsive-` + device
				}
			>
				<span onClick={ () => _set( 'lg' ) }>{ icons.desktop }</span>
				<span
					className={ device == 'sm' ? 'wopb-responsive-active' : '' }
					onClick={ () => _set( 'sm' ) }
				>
					{ icons.tablet }
				</span>
				<span
					className={ device == 'xs' ? 'wopb-responsive-active' : '' }
					onClick={ () => _set( 'xs' ) }
				>
					{ icons.mobile }
				</span>
			</div>
		</div>
	);
};
export default ResponsiveDevice;
