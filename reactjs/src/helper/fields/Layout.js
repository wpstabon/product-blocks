import LinkGenerator from '../LinkGenerator';
const Layout = ( { value, options, label, onChange, block, name } ) => {
	const getPostAttr = ( block, val ) => {
		const data = {};
		data.layout = val;
		if ( name ) {
			data[ name ] = val;
		}
		switch ( block ) {
			case 'product-category-2':
				data.queryNumber =
					val == '1' || val == '2' || val == '3'
						? '5'
						: val == '9'
						? '3'
						: '4';
				break;
			default:
				break;
		}
		return data;
	};

	const setValue = ( val, isPro ) => {
		const data = getPostAttr( block, val );
		if ( isPro ) {
			if ( wopb_data.active ) {
				onChange( data );
			} else {
				window.open(
					LinkGenerator(
						'https://www.wpxpo.com/wowstore/all-features/',
						'blockProFeature'
					),
					'_blank'
				);
			}
		} else {
			onChange( data );
		}
	};

	return (
		<div className="wopb-field-wrap wopb-field-layout">
			{ label && <label>{ label }</label> }
			<div className="wopb-field-layout-wrapper">
				{ options.map( ( data, index ) => {
					return (
						<div
							key={ index }
							onClick={ () => setValue( data.value, data.pro ) }
							className={
								data.value == value
									? 'wopb-field-layout-items active'
									: 'wopb-field-layout-items'
							}
						>
							<div className="wopb-field-layout-item">
								<div className="wopb-field-layout-content">
									{ data.pro && ! wopb_data.active && (
										<span className="wopb-field-layout-pro">
											Pro Feature
										</span>
									) }
									{ data.img ? (
										<img src={ wopb_data.url + data.img } />
									) : (
										data.icon
									) }
								</div>
							</div>
						</div>
					);
				} ) }
			</div>
		</div>
	);
};
export default Layout;
