const { __ } = wp.i18n;
const { useState, useEffect } = wp.element;

const Template = ( props ) => {
	const { clientId, attributes, label, name } = props.store;
	const [ state, setState ] = useState( {
		designList: [],
		error: false,
		reload: false,
		reloadId: '',
	} );
	const { designList, error, reload, reloadId } = state;

	useEffect( () => {
		const fetchData = async () => {
			const blockName = name.split( '/' );
			wp.apiFetch( {
				path: '/wopb/v2/get_preset_data',
				method: 'POST',
				data: {
					type: 'design',
				},
			} ).then( ( response ) => {
				if ( response.success ) {
					const data = JSON.parse( response.data );
					setState( {
						...state,
						designList: data[ blockName[ 1 ] ],
					} );
				}
			} );
		};
		fetchData();
	}, [] );

	const _changeVal = ( designID, isPro ) => {
		setState( { ...state, reload: true, reloadId: designID } );
		if ( isPro ) {
			if ( wopb_data.active ) {
				_changeDesign( designID );
			}
		} else {
			_changeDesign( designID );
		}
	};

	const _changeDesign = ( designID ) => {
		const designEndpoint = 'https://wopb.wpxpo.com/wp-json/restapi/v2/';
		const { replaceBlock } = wp.data.dispatch( 'core/block-editor' );
		const removeItem = [
			'queryNumber',
			'queryType',
			'queryTax',
			'queryCat',
			'queryTag',
			'queryOrderBy',
			'queryOrder',
			'queryInclude',
			'queryExclude',
			'queryOffset',
			'metaKey',
		];

		fetch( designEndpoint + 'single-design', {
			method: 'POST',
			body: new URLSearchParams(
				'license=' + wopb_data.license + '&design_id=' + designID
			),
		} )
			.then( ( response ) => response.text() )
			.then( ( jsonData ) => {
				jsonData = JSON.parse( jsonData );
				if ( jsonData.success && jsonData.rawData ) {
					const parseData = wp.blocks.parse( jsonData.rawData );
					let attr = parseData[ 0 ].attributes;
					for ( let i = 0; i < removeItem.length; i++ ) {
						if ( attr[ removeItem[ i ] ] ) {
							delete attr[ removeItem[ i ] ];
						}
					}
					attr = Object.assign( {}, attributes, attr );
					parseData[ 0 ].attributes = attr;
					setState( {
						...state,
						error: false,
						reload: false,
						reloadId: '',
					} );
					replaceBlock( clientId, parseData );
				} else {
					setState( {
						...state,
						error: true,
						reload: false,
						reloadId: '',
					} );
				}
			} )
			.catch( ( error ) => {
				console.error( error );
			} );
	};

	if ( typeof designList === 'undefined' ) {
		return null;
	}

	return (
		<div className="wopb-field-wrap wopb-field-template">
			{ label && <label>{ label }</label> }
			<div className="wopb-sub-field-template">
				{ designList.map( ( data, k ) => {
					return (
						<div
							key={ k }
							className={ `wopb-field-template-item ${
								data.pro && ! wopb_data.active
									? 'wopb-field-premium'
									: ''
							}` }
						>
							<img src={ data.image } loading="lazy" />
							{ data.pro && ! wopb_data.active && (
								<span className="wopb-field-premium-badge">
									Premium
								</span>
							) }
							{ data.pro && ! wopb_data.active ? (
								<div className="wopb-field-premium-lock">
									<a
										href={ wopb_data.premium_link }
										target="_blank"
										rel="noreferrer"
									>
										Upgrade to Pro
									</a>
								</div>
							) : data.pro && error ? (
								<div className="wopb-field-premium-lock">
									<a
										href={ wopb_data.premium_link }
										target="_blank"
										rel="noreferrer"
									>
										Get License
									</a>
								</div>
							) : (
								<div className="wopb-field-premium-lock">
									<button
										className="wopb-popup-btn"
										onClick={ () =>
											_changeVal( data.ID, data.pro )
										}
									>
										Import
										{ reload && reloadId == data.ID && (
											<span className="dashicons dashicons-update rotate" />
										) }
									</button>
								</div>
							) }
						</div>
					);
				} ) }
			</div>
		</div>
	);
};
export default Template;
