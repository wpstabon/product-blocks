const listKeys = {
	description: 'showShortDesc',
	title: 'titleShow',
	category: 'catShow',
	review: 'showReview',
	price: 'showPrice',
	cart: 'showCart',
	variationSwitcher: 'showVariationSwitch',
	image: 'showImage',
};

export const sortSetValue = (
	type,
	val,
	value,
	onChange,
	store = undefined
) => {
	let list = JSON.parse( value );

	// Filters disabled components
	if ( store ) {
		list = list.filter(
			( l ) => store.attributes[ listKeys[ l ] ] ?? true
		);
	}

	const _index = list.indexOf( val );
	if ( type == 'up' ) {
		if ( _index > 0 ) {
			const temp = list[ _index ];
			list[ _index ] = list[ _index - 1 ];
			list[ _index - 1 ] = temp;
			onChange( JSON.stringify( list ) );
		}
	} else if ( _index + 1 < list.length ) {
		const temp = list[ _index ];
		list[ _index ] = list[ _index + 1 ];
		list[ _index + 1 ] = temp;
		onChange( JSON.stringify( list ) );
	}
};

const Sort = ( { label, value, onChange } ) => {
	const options = JSON.parse( value );

	return (
		<div className="wopb-field-wrap wopb-field-sort">
			{ label && <label>{ label }</label> }
			<div className="wopb-sub-field">
				{ options.map( ( data, index ) => {
					return (
						<div
							key={ index }
							className={
								index == 0
									? 'wopb-button-sort active'
									: 'wopb-button-sort'
							}
						>
							<span className="wopb-button-sort-title">
								{ data.replace( /_/g, ' ' ) }
							</span>
							<div className="wopb-button-sort-arrow">
								<span
									className={ index == 0 ? 'disabled' : '' }
									onClick={ () =>
										sortSetValue(
											'up',
											data,
											value,
											onChange
										)
									}
								>
									<span className="dashicons dashicons-arrow-up-alt2" />
								</span>
								<span
									className={
										index - 1 == options.length
											? 'disabled'
											: ''
									}
									onClick={ () =>
										sortSetValue(
											'down',
											data,
											value,
											onChange
										)
									}
								>
									<span className="dashicons dashicons-arrow-down-alt2" />
								</span>
							</div>
						</div>
					);
				} ) }
			</div>
		</div>
	);
};
export default Sort;
