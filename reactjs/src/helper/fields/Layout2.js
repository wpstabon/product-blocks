const { useState } = wp.element;
import LinkGenerator from '../LinkGenerator';
import IconPack from './tools/IconPack';

const Layout2 = ( {
	value,
	options,
	label,
	pro,
	block,
	onChange,
	name,
	isInline,
} ) => {
	const [ isOpen, setIsOpen ] = useState( false );

	const getPostAttr = ( block, val ) => {
		const data = {};
		data.layout = val;
		if ( name ) {
			data[ name ] = val;
		}
		switch ( block ) {
			case 'product-category-2':
				data.queryNumber =
					val == '1' || val == '2' || val == '3'
						? '5'
						: val == '9'
						? '3'
						: '4';
				break;
			default:
				break;
		}
		return data;
	};

	const setValue = ( val, isPro ) => {
		const data = getPostAttr( block, val );
		if ( isPro ) {
			if ( wopb_data.active ) {
				onChange( data );
			} else {
				window.open(
					LinkGenerator(
						'https://www.wpxpo.com/wowstore/pricing/',
						'blockProFeature'
					),
					'_blank'
				);
			}
		} else {
			onChange( data );
		}
	};

	return (
		<div className={ `wopb-field-wrap` }>
			<div
				className={ `wopb-field-layout2 ${
					isInline
						? 'wopb-field-layout2-inline'
						: 'wopb-field-layout2-block'
				}` }
			>
				<label className="wopb-field-layout2-label">
					{ label || 'Layout' }
				</label>
				<div
					className="wopb-field-layout2-select"
					onClick={ () => setIsOpen( ( prev ) => ! prev ) }
				>
					{ options
						.filter( ( data ) => data.value == value )
						.map( ( data, i ) => {
							return (
								<div
									key={ i }
									className="wopb-field-layout2-select-items"
								>
									<div className="wopb-field-layout2-img">
										{ data.img ? (
											<img
												src={ wopb_data.url + data.img }
											/>
										) : (
											data.icon
										) }

										{ data.pro && ! wopb_data.active && (
											<span
												className={
													'wopb-field-layout2-pro'
												}
											>
												Pro
											</span>
										) }
									</div>
									<span className="wopb-field-layout2-select-text">
										{ data.label }
										<span
											className={ `wopb-field-layout2-select-icon ${
												isOpen
													? 'wopb-dropdown-icon-rotate'
													: ''
											}` }
										>
											{ IconPack.collapse_bottom_line }
										</span>
									</span>
								</div>
							);
						} ) }

					{ isOpen && (
						<div className="wopb-field-layout2-dropdown">
							{ options
								.filter( ( data ) => data.value != value )
								.map( ( data, i ) => {
									return (
										<div
											key={ i }
											onClick={ () =>
												setValue( data.value, data.pro )
											}
											className="wopb-field-layout2-select-items wopb-field-layout2-dropdown-items"
										>
											<div className="wopb-field-layout2-img">
												{ data.img ? (
													<img
														src={
															wopb_data.url +
															data.img
														}
													/>
												) : (
													data.icon
												) }

												{ data.pro &&
													! wopb_data.active && (
														<span
															className={
																'wopb-field-layout2-pro'
															}
														>
															Pro
														</span>
													) }
											</div>
											<span>{ data.label }</span>
										</div>
									);
								} ) }
						</div>
					) }
				</div>
			</div>
		</div>
	);
};
export default Layout2;
