const { Button, ButtonGroup } = wp.components;

const GroupButton = ( props ) => {
	const { value, options, label, justify, disabled, onChange } = props;

	const _set = ( val ) => {
		onChange( disabled && value == val ? '' : val );
	};

	return (
		<div className="wopb-field-wrap wopb-field-groupbutton">
			{ label && <label>{ label }</label> }
			<div
				className={ `wopb-sub-field ${
					justify ? 'wopb-groupbtn-justify' : ''
				}` }
			>
				<ButtonGroup>
					{ options.map( ( data, k ) => {
						return (
							<Button
								key={ k }
								isSmall
								variant={ data.value == value ? 'primary' : '' }
								onClick={ () => _set( data.value ) }
							>
								{ data.label }
							</Button>
						);
					} ) }
				</ButtonGroup>
			</div>
		</div>
	);
};
export default GroupButton;
