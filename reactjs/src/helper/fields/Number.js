import ResponsiveDevice from './tools/ResponsiveDevice';
import Units from './tools/Units';

const Number = ( props ) => {
	const {
		value,
		onChange,
		responsive,
		min,
		max,
		unit,
		label,
		multiple,
		setDevice,
		device,
		step,
	} = props;

	const _steps = () => ( _filterValue( 'unit' ) == 'em' ? 0.001 : step || 1 );

	const _filterValue = ( type = '' ) => {
		if ( type == 'unit' ) {
			return value
				? responsive
					? value[ 'u' + device ] || 'px'
					: value.unit || 'px'
				: 'px';
		}
		return value
			? responsive
				? value[ device ] || ''
				: value.value || value
			: '';
	};

	const setSettings = ( val, type ) => {
		let final = value ? { ...value } : {};
		if ( device ) {
			if ( unit && ! final.hasOwnProperty( 'u' + device ) ) {
				final[ 'u' + device ] = 'px';
			}
		}
		if ( type == 'unit' && responsive ) {
			//final = value || {};
			final[ 'u' + device ] = val;
		} else {
			final = responsive
				? Object.assign( {}, value, { [ device ]: val } )
				: typeof value === 'object'
				? Object.assign( {}, value, { [ type ]: val } )
				: val;
			final = min ? ( final < min ? min : final ) : final < 0 ? 0 : final;
			final = max
				? final > max
					? max
					: final
				: final > 1000
				? 1000
				: final;
		}
		onChange( final );
	};

	return (
		<div className="wopb-field-wrap wopb-field-number">
			<div className="wopb-field-label-responsive">
				{ label && <label>{ label }</label> }
				{ responsive && ! multiple && (
					<ResponsiveDevice
						setDevice={ setDevice }
						device={ device }
					/>
				) }
				{ unit && (
					<Units
						unit={ unit }
						value={ _filterValue( 'unit' ) }
						setSettings={ setSettings }
					/>
				) }
			</div>
			<div className="wopb-responsive-label wopb-field-unit">
				<input
					type="number"
					step={ _steps() }
					min={ min }
					max={ max }
					onChange={ ( v ) => setSettings( v.target.value, 'value' ) }
					value={ _filterValue() }
				/>
			</div>
		</div>
	);
};

export default Number;
