const { __ } = wp.i18n;
const { Fragment, useState, useEffect, useRef } = wp.element;
import ResponsiveDevice from './tools/ResponsiveDevice';
import icons from '../../helper/icons';

const Select = ( {
	label,
	responsive,
	options,
	multiple,
	value,
	pro,
	image,
	imageSelect,
	setDevice,
	device,
	onChange,
	help,
	sort,
	clear,
	condition,
	clientId,
	beside,
	svg,
	svgClass,
} ) => {
	const myRef = useRef();

	const [ isOpen, setOpen ] = useState( false );

	const handleClickOutside = ( e ) => {
		if ( ! myRef.current.contains( e.target ) ) {
			setOpen( false );
		}
	};

	useEffect( () => {
		document.addEventListener( 'mousedown', handleClickOutside );
		return () =>
			document.removeEventListener( 'mousedown', handleClickOutside );
	}, [ handleClickOutside ] );

	const _filterValue = () => {
		if ( ! multiple ) {
			return value ? ( responsive ? value[ device ] || '' : value ) : '';
		}
		return value || [];
	};

	const _set = ( val ) => {
		if ( pro && ! wopb_data.active ) {
			return;
		}
		if ( ! multiple ) {
			let noCondition = true;
			if ( condition ) {
				Object.keys( condition ).forEach( function ( key ) {
					if ( val == key ) {
						noCondition = false;
						onChange(
							Object.assign(
								{},
								{ [ keys ]: val },
								condition[ key ]
							)
						);
					}
				} );
			}
			if ( noCondition ) {
				const final = responsive
					? Object.assign( {}, value, { [ device ]: val } )
					: val;
				onChange( final );
			}
		} else {
			const filter = value.length ? value : [];
			if ( value.indexOf( val ) == -1 ) {
				filter.push( val );
			}
			onChange( filter );
		}
	};

	const getLabel = ( val, svg ) => {
		const data = options.filter( ( attr ) => attr.value == val );
		if ( data[ 0 ] ) {
			return svg && data[ 0 ].svg ? data[ 0 ].svg : data[ 0 ].label;
		}
	};
	const getImage = ( val ) => {
		const data = options.filter( ( attr ) => attr.value == val );
		if ( data[ 0 ] ) {
			return <img alt={ data[ 0 ].label || '' } src={ data[ 0 ].img } />;
		}
	};

	const removeItem = ( val ) => {
		onChange( value.filter( ( item ) => item != val ) );
	};

	const upValue = ( index ) => {
		if ( pro && ! wopb_data.active ) {
			return;
		}

		const myArray = value.slice( 0 );
		if ( index > 0 ) {
			myArray[ index ] = myArray.splice(
				index - 1,
				1,
				myArray[ index ]
			)[ 0 ];
			onChange( myArray );
		}
	};

	const downValue = ( index ) => {
		if ( pro && ! wopb_data.active ) {
			return;
		}
		const myArray = value.slice( 0 );
		if ( value.length > index + 1 ) {
			myArray[ index + 1 ] = myArray.splice(
				index,
				1,
				myArray[ index + 1 ]
			)[ 0 ];
			onChange( myArray );
		}
	};

	return (
		<Fragment>
			<div
				ref={ myRef }
				className={ `wopb-field-wrap wopb-field-select ${
					pro && ! wopb_data.active ? 'wopb-pro-field' : ''
				}` }
			>
				<div className={ `${ beside ? 'wopb-field-beside' : '' }` }>
					<div className="wopb-label-control">
						{ label && <label>{ label }</label> }
						{ responsive && ! multiple && (
							<ResponsiveDevice
								setDevice={ setDevice }
								device={ device }
							/>
						) }
					</div>
					<div
						className={ `wopb-popup-select ${
							svg ? 'svgIcon ' + svgClass : ''
						}` }
					>
						{ clear && (
							<div
								className="wopb-base-control-btn dashicons dashicons-image-rotate"
								onClick={ () => _set( '' ) }
							/>
						) }
						<span
							className={
								( isOpen ? 'isOpen ' : '' ) +
								' wopb-selected-text' +
								( sort ? ' wopb-selected-dropdown--icon' : '' )
							}
							onClick={ () => setOpen( ! isOpen ) }
						>
							{ multiple ? (
								<span className="wopb-search-value wopb-multiple-value">
									{ value.map( ( item, k ) => (
										<span
											key={ k }
											onClick={ () => removeItem( item ) }
										>
											{ sort && (
												<span className="wopb-updown-container">
													<span
														onClick={ ( e ) => {
															e.stopPropagation();
															upValue( k );
														} }
														className="wopb-select-swap dashicons dashicons-arrow-up-alt2"
													/>
													<span
														onClick={ ( e ) => {
															e.stopPropagation();
															downValue( k );
														} }
														className="wopb-select-swap dashicons dashicons-arrow-down-alt2"
													/>
												</span>
											) }
											{ getLabel( item ) }
											<span className="wopb-select-close">
												×
											</span>
										</span>
									) ) }
								</span>
							) : (
								<span className="wopb-search-value">
									{ imageSelect
										? getImage( _filterValue() )
										: getLabel( _filterValue(), svg ) }
								</span>
							) }

							{ sort && (
								<span className="wopb-search-divider"></span>
							) }
							<span className="wopb-search-icon">
								<i
									className={
										'dashicons dashicons-arrow-' +
										( isOpen ? 'up-alt2' : 'down-alt2' )
									}
								/>
							</span>
						</span>

						{ isOpen && (
							<ul>
								{ options.map( ( item, k ) => (
									<li
										key={ k }
										className={
											value === item.value
												? 'wopb-active'
												: ''
										}
										onClick={ () => {
											setOpen( ! isOpen );
											if ( item.pro ) {
												if ( wopb_data.active ) {
													_set( item.value );
												}
											} else {
												_set( item.value );
											}
										} }
										value={ item.value }
									>
										{ svg && item.svg ? (
											item.svg
										) : image ? (
											<img
												alt={ item.label || '' }
												src={ item.img }
											/>
										) : (
											item.label.replaceAll(
												'&amp;',
												'&'
											)
										) }
										{ icons[ item.icon ] || '' }{ ' ' }
										{ item.pro && ! wopb_data.active ? (
											<span
												className="wopb-pro-text"
												onClick={ () =>
													window.open(
														item.link
															? item.link
															: 'https://www.wpxpo.com/wowstore/pricing/?utm_source=db-wstore-editor&utm_medium=pro-features&utm_campaign=wstore-dashboard',
														'_blank'
													)
												}
											>
												[Pro]
											</span>
										) : (
											''
										) }
									</li>
								) ) }
								{ options.length < 1 && (
									<li>You already selected all option</li>
								) }
							</ul>
						) }
					</div>
				</div>
				{ help && (
					<div className="wopb-sub-help">
						<i>{ help }</i>
					</div>
				) }
				{ pro && ! wopb_data.active && (
					<div className="wopb-field-pro-message">
						{ __( 'To Enable This Feature', 'product-blocks' ) }{ ' ' }
						<a
							href={
								'https://www.wpxpo.com/wowstore/pricing/?utm_source=db-wstore-editor&utm_medium=pro-features&utm_campaign=wstore-dashboard'
							}
							target="_blank"
							rel="noreferrer"
						>
							{ __( 'Upgrade to Pro', 'product-blocks' ) }
						</a>
					</div>
				) }
			</div>
		</Fragment>
	);
};
export default Select;
