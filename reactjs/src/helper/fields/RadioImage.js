const RadioImage = ( { value, options, label, onChange, isText } ) => {
	return (
		<div className="wopb-field-wrap wopb-field-radio-image">
			{ label && <label>{ label }</label> }
			<div className="wopb-field-radio-image-content">
				{ isText ? (
					options.map( ( data, index ) => {
						return (
							<div
								key={ index }
								onClick={ () => onChange( data.value ) }
							>
								<span
									className={
										data.value == value ? 'active' : ''
									}
								>
									{ data.label }
								</span>
							</div>
						);
					} )
				) : (
					<div className="wopb-field-radio-image-items">
						{ options.map( ( data, index ) => {
							return (
								<div
									key={ index }
									className={
										data.value == value
											? 'wopb-radio-image active'
											: 'wopb-radio-image'
									}
									onClick={ () => onChange( data.value ) }
								>
									<img
										className={
											data.value == value ? 'active' : ''
										}
										src={ data.url }
									/>
								</div>
							);
						} ) }
					</div>
				) }
			</div>
		</div>
	);
};
export default RadioImage;
