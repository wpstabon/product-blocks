/** @format */

const Separator = ( props ) => {
	const { label, fancy = false } = props;

	if ( fancy && label ) {
		return (
			<div className="wopb-separator-ribbon-wrapper">
				<div className="wopb-separator-ribbon">{ label }</div>
			</div>
		);
	}

	return (
		<div className={ 'wopb-field-wrap wopb-field-separator' }>
			<div>{ label && <span>{ label }</span> }</div>
		</div>
	);
};
export default Separator;
