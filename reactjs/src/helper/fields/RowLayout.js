const { dispatch } = wp.data;
const { Tooltip } = wp.components;
import ResponsiveDevice from './tools/ResponsiveDevice';

const allLayout = {
	col_1: {
		lg: [ [ 100 ] ],
		sm: [ [ 100 ] ],
		xs: [ [ 100 ] ],
	},
	col_2: {
		lg: [
			[ 50, 50 ],
			[ 70, 30 ],
			[ 30, 70 ],
			[ 100, 100 ],
		],
		sm: [
			[ 50, 50 ],
			[ 70, 30 ],
			[ 30, 70 ],
			[ 100, 100 ],
		],
		xs: [
			[ 50, 50 ],
			[ 70, 30 ],
			[ 30, 70 ],
			[ 100, 100 ],
		],
	},
	col_3: {
		lg: [
			[ 33.33, 33.33, 33.34 ],
			[ 25, 25, 50 ],
			[ 50, 25, 25 ],
			[ 25, 50, 25 ],
			[ 20, 60, 20 ],
			[ 15, 70, 15 ],
			[ 100, 100, 100 ],
		],
		sm: [
			[ 33.33, 33.33, 33.34 ],
			[ 25, 25, 50 ],
			[ 50, 25, 25 ],
			[ 25, 50, 25 ],
			[ 20, 60, 20 ],
			[ 15, 70, 15 ],
			[ 100, 50, 50 ],
			[ 50, 50, 100 ],
			[ 100, 100, 100 ],
		],
		xs: [
			[ 33.33, 33.33, 33.34 ],
			[ 25, 25, 50 ],
			[ 50, 25, 25 ],
			[ 25, 50, 25 ],
			[ 20, 60, 20 ],
			[ 15, 70, 15 ],
			[ 100, 50, 50 ],
			[ 50, 50, 100 ],
			[ 100, 100, 100 ],
		],
	},
	col_4: {
		lg: [
			[ 25, 25, 25, 25 ],
			[ 20, 20, 20, 40 ],
			[ 40, 20, 20, 20 ],
		],
		sm: [
			[ 25, 25, 25, 25 ],
			[ 20, 20, 20, 40 ],
			[ 40, 20, 20, 20 ],
			[ 50, 50, 50, 50 ],
			[ 100, 100, 100, 100 ],
		],
		xs: [
			[ 25, 25, 25, 25 ],
			[ 20, 20, 20, 40 ],
			[ 40, 20, 20, 20 ],
			[ 50, 50, 50, 50 ],
			[ 100, 100, 100, 100 ],
		],
	},
	col_5: {
		lg: [
			[ 20, 20, 20, 20, 20 ],
			[ 100, 100, 100, 100, 100 ],
		],
		sm: [
			[ 20, 20, 20, 20, 20 ],
			[ 100, 100, 100, 100, 100 ],
		],
		xs: [
			[ 20, 20, 20, 20, 20 ],
			[ 100, 100, 100, 100, 100 ],
		],
	},
	col_6: {
		lg: [ [ 16.66, 16.66, 16.66, 16.66, 16.66, 16.7 ] ],
		sm: [
			[ 16.66, 16.66, 16.66, 16.66, 16.66, 16.7 ],
			[ 50, 50, 50, 50, 50, 50 ],
			[ 33.33, 33.33, 33.34, 33.33, 33.33, 33.34 ],
			[ 100, 100, 100, 100, 100, 100 ],
		],
		xs: [
			[ 16.66, 16.66, 16.66, 16.66, 16.66, 16.7 ],
			[ 50, 50, 50, 50, 50, 50 ],
			[ 33.33, 33.33, 33.34, 33.33, 33.33, 33.34 ],
			[ 100, 100, 100, 100, 100, 100 ],
		],
	},
};

const RowLayout = ( props ) => {
	const { value, label, layout, responsive, device, setDevice, clientId } =
		props;
	const innerBlocks = wp.data
		.select( 'core/block-editor' )
		.getBlocks( clientId );

	return (
		<div className={ `wopb-field-wrap wopb-field-layout` }>
			<div className={ 'wopb-columnLayout-label' }>
				{ label && <label>{ label }</label> }
				{ responsive && (
					<ResponsiveDevice
						setDevice={ setDevice }
						device={ device }
					/>
				) }
			</div>
			<div className={ `wopb-field-layout-wrapper wopb-panel-column` }>
				{ allLayout[ 'col_' + value.lg.length ][ device ].map(
					( data, k ) => {
						return (
							<Tooltip
								delay={ 0 }
								visible={ true }
								position={ 'bottom' }
								text={ data.join( ' : ' ) }
								key={ k }
							>
								<div
									className={
										`wopb-row-size wopb-field-layout-items ` +
										( value[ device ] == data.join( ',' )
											? 'active'
											: '' )
									}
									onClick={ () => {
										dispatch(
											'core/block-editor'
										).updateBlockAttributes( clientId, {
											layout: Object.assign( {}, layout, {
												[ device ]: data,
											} ),
										} );
										if ( innerBlocks?.length > 0 ) {
											innerBlocks.forEach( ( el, _i ) => {
												dispatch(
													'core/block-editor'
												).updateBlockAttributes(
													el.clientId,
													{
														columnWidth:
															Object.assign(
																{},
																el.attributes
																	.columnWidth,
																{
																	[ device ]:
																		data[
																			_i
																		],
																}
															),
													}
												);
											} );
										}
									} }
								>
									{ data.map( ( el, _k, arr ) => {
										if (
											( arr.length == 4 ||
												arr.length == 6 ) &&
											( JSON.stringify( arr ) ==
												JSON.stringify( [
													50, 50, 50, 50,
												] ) ||
												JSON.stringify( arr ) ==
													JSON.stringify( [
														50, 50, 50, 50, 50, 50,
													] ) ||
												JSON.stringify( arr ) ==
													JSON.stringify( [
														33.33, 33.33, 33.34,
														33.33, 33.33, 33.34,
													] ) )
										) {
											return (
												<div
													className="wopb-layout-half"
													style={ {
														width: `calc(${ el }% - 5px )`,
													} }
													key={ _k }
												></div>
											);
										}
										return (
											<div
												className={ `${
													el > 99
														? 'wopb-layout-vertical'
														: ''
												}` }
												style={ {
													width: `${ el }%`,
												} }
												key={ _k }
											></div>
										);
									} ) }
								</div>
							</Tooltip>
						);
					}
				) }
			</div>
		</div>
	);
};
export default RowLayout;
