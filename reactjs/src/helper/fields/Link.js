const { __ } = wp.i18n;
const { useState } = wp.element;
import { CommonSettings } from '../CommonPanel';

const Link = ( { label, onChange, value, store } ) => {
	const [ toggle, setToggle ] = useState( false );
	const removeUrl = ( id ) => {
		onChange( { url: '' } );
	};

	const changeUrl = ( val ) => {
		onChange( { url: val.target.value } );
	};

	return (
		<div className="wopb-field-wrap wopb-field-link">
			{ label && <label>{ label }</label> }
			<div>
				<span className={ `wopb-link-section` }>
					<span className="wopb-link-input">
						<input
							type="text"
							onChange={ ( v ) => changeUrl( v ) }
							value={ value.url }
						/>
						<span className={ `wopb-input-inner-options` }>
							{ value.url && (
								<span
									className="wopb-image-close-icon dashicons dashicons-no-alt"
									onClick={ () => removeUrl() }
								/>
							) }
						</span>
					</span>
					<span className={ `wopb-link-options` }>
						<span
							className={ `wopb-collapse-section` }
							onClick={ () => {
								setToggle( toggle ? false : true );
							} }
						>
							<span
								className={ `wopb-short-collapse ${
									toggle && ' active'
								}` }
								type="button"
							/>
						</span>
					</span>
				</span>
				{ toggle && (
					<div className={ `wopb-short-content active` }>
						<CommonSettings
							include={ [
								{
									position: 1,
									data: {
										type: 'select',
										key: 'btnLinkTarget',
										label: __(
											'Link Target',
											'product-blocks'
										),
										options: [
											{
												value: '_self',
												label: __(
													'Same Tab/Window',
													'product-blocks'
												),
											},
											{
												value: '_blank',
												label: __(
													'Open in New Tab',
													'product-blocks'
												),
											},
										],
									},
								},
								{
									position: 2,
									data: {
										type: 'toggle',
										key: 'btnLinkNoFollow',
										label: __(
											'No Follow',
											'product-blocks'
										),
									},
								},
								{
									position: 3,
									data: {
										type: 'toggle',
										key: 'btnLinkSponsored',
										label: __(
											'Sponsored',
											'product-blocks'
										),
									},
								},
								{
									position: 4,
									data: {
										type: 'toggle',
										key: 'btnLinkDownload',
										label: __(
											'Download',
											'product-blocks'
										),
									},
								},
							] }
							initialOpen={ true }
							store={ store }
						/>
					</div>
				) }
			</div>
		</div>
	);
};
export default Link;
