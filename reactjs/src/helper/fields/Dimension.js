const { useState } = wp.element;
import ResponsiveDevice from './tools/ResponsiveDevice';
import Units from './tools/Units';

const Dimension = ( {
	value,
	onChange,
	device,
	label,
	setDevice,
	unit,
	noLock,
	dataLabel,
	responsive,
} ) => {
	const [ lock, setLock ] = useState( value?.isLocked ?? false );

	const _filterUnit = () => {
		if ( value ) {
			if ( responsive ) {
				return value[ device ]
					? value[ device ].unit
						? value[ device ].unit
						: 'px'
					: 'px';
			}
			return value.unit || 'px';
		}
		return 'px';
	};

	const _filterValue = ( val ) => {
		if ( typeof value === 'object' && Object.keys( value ).length > 0 ) {
			if ( val ) {
				return responsive
					? value[ device ]
						? value[ device ][ val ] || ''
						: ''
					: value[ val ];
			}
			return responsive ? value[ device ] || '' : value;
		}
		return '';
	};

	const setSettings = ( action, position ) => {
		let data =
			lock && position != 'unit'
				? { top: action, right: action, bottom: action, left: action }
				: { [ position ]: action };
		data = Object.assign(
			{},
			responsive ? value[ device ] || {} : value,
			data
		);
		data.unit = data.unit || 'px';
		const final = Object.assign(
			{},
			value,
			responsive ? { [ device ]: data } : data,
			{ isLocked: lock }
		);
		onChange( final );
	};

	return (
		<div className="wopb-field-wrap wopb-field-dimension">
			{ ( label || responsive ) && (
				<div className="wopb-label-control">
					{ label && <label>{ label }</label> }
					{ responsive && (
						<ResponsiveDevice
							setDevice={ setDevice }
							device={ device }
						/>
					) }
					{ unit && (
						<Units
							unit={ unit }
							value={ _filterUnit() }
							setSettings={ setSettings }
						/>
					) }
				</div>
			) }
			<div
				className={
					'wopb-dimension-input' +
					( ! noLock ? ' wopb-base-control-hasLock' : '' )
				}
			>
				{ [ 'top', 'right', 'bottom', 'left' ].map( ( val, index ) => (
					<span key={ index }>
						<input
							type="number"
							value={ _filterValue( val ) }
							onChange={ ( v ) =>
								setSettings( v.target.value, val )
							}
						/>
						<span>{ dataLabel ? dataLabel[ index ] : val }</span>
					</span>
				) ) }
				{ ! noLock && (
					<button
						className={ ( lock ? 'active ' : '' ) + '' }
						onClick={ () => setLock( ! lock ) }
					>
						{ lock ? (
							<span
								className={ 'dashicons dashicons-admin-links' }
							/>
						) : (
							<span
								className={
									'dashicons dashicons-editor-unlink'
								}
							/>
						) }
					</button>
				) }
			</div>
		</div>
	);
};
export default Dimension;
