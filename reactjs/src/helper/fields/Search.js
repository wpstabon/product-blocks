const { __ } = wp.i18n;
const { apiFetch } = wp;
const { useState, useEffect, useRef } = wp.element;
const { Spinner } = wp.components;

const Search = ( props ) => {
	const myRef = useRef();
	const [ isOpen, setOpen ] = useState( false );
	const [ defValue, setDefValue ] = useState( [] );
	const [ searchValue, setSearchValue ] = useState( '' );
	const [ loading, setLoading ] = useState( true );
	const { label, value, onChange, search, condition, pro } = props;

	const fetchData = async ( term = '' ) => {
		setLoading( true );
		apiFetch( {
			path: '/wopb/v1/search',
			method: 'POST',
			data: { type: search, term, condition },
		} ).then( ( res ) => {
			if ( res.success ) {
				if ( value.length > 0 && res.data.length > 0 ) {
					const _val = JSON.stringify( value );
					const myArray = res.data.filter( function ( el ) {
						return _val.indexOf( JSON.stringify( el ) ) < 0;
					} );
					setDefValue( myArray );
					setLoading( false );
				} else {
					setDefValue( res.data );
					setLoading( false );
				}
			}
		} );
	};

	const handleClickOutside = ( e ) => {
		if ( ! myRef.current.contains( e.target ) ) {
			setOpen( false );
		}
	};

	useEffect( () => {
		document.addEventListener( 'mousedown', handleClickOutside );
		return () =>
			document.removeEventListener( 'mousedown', handleClickOutside );
	}, [] );

	const fetchOpen = ( status = true ) => {
		if ( pro && ! wopb_data.active ) {
			return;
		}

		if ( status ) {
			setLoading( true );
			fetchData();
		}
		setOpen( status );
	};

	const removeValue = ( index ) => {
		if ( pro && ! wopb_data.active ) {
			return;
		}

		value.splice( index, 1 );
		const myArray = defValue.filter( function ( el ) {
			return value.indexOf( el ) < 0;
		} );
		setDefValue( myArray );

		onChange( value );
	};

	const setValue = ( val ) => {
		if ( pro && ! wopb_data.active ) {
			return;
		}
		value.push( val );
		const myArray = defValue.filter( function ( el ) {
			return value.indexOf( el ) < 0;
		} );
		setDefValue( myArray );
		onChange( value );
		setOpen( false );
	};

	const fetchValue = ( val ) => {
		const v = val.target.value;
		if ( v.length > 0 ) {
			fetchData( v );
		} else {
			fetchData();
		}
		setSearchValue( v );
	};

	const upValue = ( index ) => {
		if ( pro && ! wopb_data.active ) {
			return;
		}
		const myArray = value.slice( 0 );
		if ( index > 0 ) {
			myArray[ index ] = myArray.splice(
				index - 1,
				1,
				myArray[ index ]
			)[ 0 ];
			setDefValue( myArray );
			onChange( myArray );
		}
	};
	const downValue = ( index ) => {
		if ( pro && ! wopb_data.active ) {
			return;
		}
		const myArray = value.slice( 0 );
		if ( value.length > index + 1 ) {
			myArray[ index + 1 ] = myArray.splice(
				index,
				1,
				myArray[ index + 1 ]
			)[ 0 ];
			setDefValue( myArray );
			onChange( myArray );
		}
	};

	return (
		<div
			className={ `wopb-field-wrap wopb-field-select ${
				pro && ! wopb_data.active ? 'wopb-pro-field' : ''
			}` }
		>
			{ label && (
				<span className={ `wopb-label-control` }>
					<label>{ label }</label>
				</span>
			) }
			<div ref={ myRef } className={ `wopb-popup-select` }>
				<span
					className={
						( isOpen ? 'isOpen ' : '' ) +
						' wopb-selected-text wopb-selected-dropdown--icon'
					}
				>
					<span
						onClick={ () => fetchOpen( true ) }
						className="wopb-search-value wopb-multiple-value"
					>
						{ value.map( ( item, k ) => {
							return (
								<span key={ k }>
									<span className="wopb-updown-container">
										<span
											onClick={ ( e ) => {
												e.stopPropagation();
												upValue( k );
											} }
											className="wopb-select-swap dashicons dashicons-arrow-up-alt2"
										/>
										<span
											onClick={ ( e ) => {
												e.stopPropagation();
												downValue( k );
											} }
											className="wopb-select-swap dashicons dashicons-arrow-down-alt2"
										/>
									</span>
									{ item.title.replaceAll( '&amp;', '&' ) }
									<span
										className="wopb-select-close"
										onClick={ () => removeValue( k ) }
									>
										×
									</span>
								</span>
							);
						} ) }
					</span>
					<span className="wopb-search-divider"></span>
					<span
						className="wopb-search-icon wopb-search-dropdown--icon"
						onClick={ () => fetchOpen( ! isOpen ) }
					>
						<i
							className={
								'dashicons dashicons-arrow-' +
								( isOpen ? 'up-alt2' : 'down-alt2' )
							}
						/>
					</span>
				</span>
				<span>
					{ isOpen && (
						<ul>
							<div className="wopb-select-search">
								<input
									type="text"
									placeholder={ __(
										'Search here for more…',
										'product-blocks'
									) }
									value={ searchValue }
									onChange={ ( v ) => fetchValue( v ) }
									autoComplete="off"
								/>
								{ loading && <Spinner /> }
							</div>
							{ defValue.map( ( item, k ) => (
								<li
									key={ k }
									onClick={ () => setValue( item ) }
								>
									{ item.title.replaceAll( '&amp;', '&' ) }
								</li>
							) ) }
						</ul>
					) }
				</span>
			</div>
			{ pro && ! wopb_data.active && (
				<div className="wopb-field-pro-message">
					{ __( 'To Enable This Feature', 'product-blocks' ) }{ ' ' }
					<a
						href={
							'https://www.wpxpo.com/wowstore/pricing/?utm_source=db-wstore-editor&utm_medium=pro-features&utm_campaign=wstore-dashboard'
						}
						target="_blank"
						rel="noreferrer"
					>
						{ __( 'Upgrade to Pro', 'product-blocks' ) }
					</a>
				</div>
			) }
		</div>
	);
};
export default Search;
