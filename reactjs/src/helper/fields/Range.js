/** @format */

const { __ } = wp.i18n;
import { updateChildAttr } from '../CommonPanel';
import ResponsiveDevice from './tools/ResponsiveDevice';
import Units from './tools/Units';
let isFocus = false;
const Range = ( {
	value,
	unit,
	onChange,
	label,
	responsive,
	pro,
	device,
	setDevice,
	step,
	min,
	max,
	help,
	clientId,
	updateChild,
	dimensionKey,
} ) => {
	const _steps = () => ( _filterValue( 'unit' ) == 'em' ? 0.01 : step || 1 );

	const _dimensionFilterUnit = () => {
		if ( value ) {
			if ( responsive ) {
				return value[ device ]
					? value[ device ].unit
						? value[ device ].unit
						: 'px'
					: 'px';
			}
			return value.unit || 'px';
		}
		return 'px';
	};

	const _dimensionFilterValue = () => {
		return responsive
			? value[ device ]
				? value[ device ][ dimensionKey ] || ''
				: ''
			: value[ dimensionKey ];
	};

	const _dimensionSetSettings = ( action, type ) => {
		if ( type === 'unit' ) {
			return responsive
				? {
						...value,
						[ device ]: {
							...value[ device ],
							unit: action,
						},
				  }
				: {
						...value,
						unit: action,
				  };
		}
		let data = { [ dimensionKey ]: action };
		data = Object.assign(
			{},
			responsive ? value[ device ] || {} : value,
			data
		);
		data.unit = data.unit || 'px';
		return Object.assign(
			{},
			value,
			responsive ? { [ device ]: data } : data
		);
	};

	const _filterValue = ( type ) => {
		if ( type === 'unit' ) {
			return dimensionKey
				? _dimensionFilterUnit()
				: value
				? responsive
					? value[ 'u' + device ] || 'px'
					: value.unit || 'px'
				: 'px';
		}
		return dimensionKey
			? _dimensionFilterValue()
			: value
			? responsive
				? value[ device ] || ''
				: value
			: '';
	};

	const setSettings = ( val, type, source = '' ) => {
		let final = value ? { ...value } : {};
		if ( dimensionKey ) {
			final = _dimensionSetSettings( val, type );
		} else {
			final = value ? { ...value } : {};
			if ( device ) {
				if ( unit && ! final.hasOwnProperty( 'u' + device ) ) {
					final[ 'u' + device ] = final.hasOwnProperty( 'unit' )
						? final.unit
						: 'px';
				}
			}
			if ( type === 'unit' && responsive ) {
				final[ 'u' + device ] = val;
			} else {
				final = responsive
					? Object.assign( {}, value, final, { [ device ]: val } )
					: val;
				if ( source === 'minMaxCheck' ) {
					final = minMaxCheck( final );
				}
			}
		}
		changeValue( final );
	};

	const minMaxCheck = ( final ) => {
		final = min ? ( final < min ? min : final ) : final < 0 ? 0 : final;
		final = max
			? final > max
				? max
				: final
			: final > 1000
			? 1000
			: final;
		return final;
	};

	const changeValue = ( final ) => {
		if ( pro ) {
			if ( wopb_data.active ) {
				onChange( final );
			}
		} else {
			onChange( final );
		}
		if ( updateChild ) {
			updateChildAttr( clientId );
		}
	};

	const inputValueCheck = ( value ) => {
		setSettings( value, '', 'minMaxCheck' );
	};

	return (
		<div
			className={
				'wopb-field-wrap wopb-field-range  ' +
				( responsive ? 'wopb-base-control-responsive' : '' ) +
				' ' +
				( pro && ! wopb_data.active ? 'wopb-pro-field' : '' )
			}
		>
			<div className="wopb-field-label-responsive">
				{ label && <label>{ label }</label> }
				{ responsive && (
					<ResponsiveDevice
						setDevice={ setDevice }
						device={ device }
					/>
				) }
				{ unit && (
					<Units
						unit={ unit }
						value={ _filterValue( 'unit' ) }
						setSettings={ setSettings }
					/>
				) }
			</div>
			<div className="wopb-range-control">
				<div className="wopb-range-input">
					<input
						type="range"
						min={ min }
						max={ max }
						value={ _filterValue() }
						step={ _steps() }
						onChange={ ( e ) =>
							setSettings( e.target.value, '', 'minMaxCheck' )
						}
					/>
					<input
						type="number"
						step={ _steps() }
						min={ min }
						max={ max }
						onChange={ ( e ) => setSettings( e.target.value, '' ) }
						onFocus={ ( e ) => ( isFocus = true ) }
						onBlur={ ( e ) => ( isFocus = false ) }
						onMouseOut={ ( e ) => {
							if ( isFocus ) {
								inputValueCheck( e.target.value );
							}
						} }
						value={ _filterValue() }
					/>
				</div>
			</div>
			{ help && (
				<div className="wopb-sub-help">
					<i>{ help }</i>
				</div>
			) }
			{ pro && ! wopb_data.active && (
				<div className="wopb-field-pro-message">
					{ __( 'To Enable This Feature', 'product-blocks' ) }{ ' ' }
					<a
						href={
							'https://www.wpxpo.com/wowstore/pricing/?utm_source=db-wstore-editor&utm_medium=pro-features&utm_campaign=wstore-dashboard'
						}
						target="_blank"
						rel="noreferrer"
					>
						{ __( 'Upgrade to Pro', 'product-blocks' ) }
					</a>
				</div>
			) }
		</div>
	);
};
export default Range;
