import icons from '../../helper/icons';
import ResponsiveDevice from './tools/ResponsiveDevice';

const Alignment = ( props ) => {
	const {
		value,
		onChange,
		options,
		responsive,
		device,
		label,
		disableJustify,
		toolbar,
		setDevice,
		alignIcons,
	} = props;

	const _set = ( val ) => {
		onChange(
			responsive ? Object.assign( {}, value, { [ device ]: val } ) : val
		);
	};

	const align = options
		? options
		: disableJustify
		? [ 'left', 'center', 'right' ]
		: [ 'left', 'center', 'right', 'justify' ];
	const finalVal = value
		? responsive
			? value[ device ] || ''
			: value
		: '';
	const iconSVG =
		alignIcons && alignIcons.length > 0
			? alignIcons
			: [ 'left', 'center', 'right', 'justify' ];

	return (
		<div
			className={ `wopb-field-wrap wopb-field-alignment ${
				toolbar ? 'wopb-align-inline' : ''
			}${ alignIcons?.length ? ' wopb-row-alignment' : '' }` }
		>
			<div className="wopb-field-label-responsive">
				{ label && <label>{ label }</label> }
				{ responsive && (
					<ResponsiveDevice
						setDevice={ setDevice }
						device={ device }
					/>
				) }
			</div>
			<div className="wopb-sub-field">
				{ align.map( ( data, k ) => {
					return (
						<span
							key={ k }
							onClick={ () => _set( data ) }
							className={
								'wopb-align-button' +
								( data == finalVal ? ' alignActive' : '' )
							}
						>
							{ icons[ iconSVG[ k ] ] }
							{ toolbar ? 'Align text ' + data : '' }
						</span>
					);
				} ) }
			</div>
		</div>
	);
};

export default Alignment;
