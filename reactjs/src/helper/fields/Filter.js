const { __ } = wp.i18n;
const { Dropdown } = wp.components;
import icons from '../icons';

const inputSource = [
	{ key: 'hue', label: __( 'Hue', 'product-blocks' ) },
	{ key: 'saturation', label: __( 'Saturation', 'product-blocks' ) },
	{
		key: 'brightness',
		min: 0,
		max: 200,
		label: __( 'Brightness', 'product-blocks' ),
	},
	{
		key: 'contrast',
		min: 0,
		max: 200,
		label: __( 'Contrast', 'product-blocks' ),
	},
	{ key: 'invert', label: __( 'Invert', 'product-blocks' ) },
	{ key: 'blur', min: 0, max: 50, label: __( 'Blur', 'product-blocks' ) },
];

const Filter = ( { value, label, onChange } ) => {
	const isActive = value && value.openFilter ? true : false;

	const _filterValue = ( type ) => {
		return value.hasOwnProperty( type ) ? value[ type ] : '';
	};

	const setSettings = ( val, type ) => {
		onChange( Object.assign( {}, value, { [ type ]: val } ) );
	};

	return (
		<div className="wopb-field-wrap wopb-field-imgfilter">
			{ label && <label>{ label }</label> }
			<Dropdown
				className="wopb-range-control"
				renderToggle={ ( { isOpen, onToggle } ) => (
					<div className="wopb-edit-btn">
						<div className="wopb-typo-control">
							{ isActive && (
								<div
									className={ `active wopb-base-control-btn dashicons dashicons-image-rotate` }
									onClick={ () => {
										if ( isOpen ) {
											onToggle();
										}
										setSettings( 0, 'openFilter' );
									} }
								></div>
							) }
							<div
								className={ `${
									isActive && 'active '
								} wopb-icon-style` }
								onClick={ () => {
									onToggle();
									setSettings( 1, 'openFilter' );
								} }
							>
								{ icons.setting }
							</div>
						</div>
					</div>
				) }
				renderContent={ () => (
					<div
						className={
							'wopb-common-popup wopb-color-popup wopb-imgfilter-range'
						}
					>
						{ inputSource.map( ( data, k ) => {
							return (
								<div key={ k } className="wopb-range-input">
									<span>{ data.label }</span>
									<input
										type="range"
										min={ data.min || 0 }
										max={ data.max || 100 }
										value={ _filterValue( data.key ) }
										step={ 1 }
										onChange={ ( e ) =>
											setSettings(
												e.target.value,
												data.key
											)
										}
									/>
									<input
										type="number"
										min={ data.min || 0 }
										max={ data.max || 100 }
										value={ _filterValue( data.key ) }
										step={ 1 }
										onChange={ ( e ) =>
											setSettings(
												e.target.value,
												data.key
											)
										}
									/>
								</div>
							);
						} ) }
					</div>
				) }
			/>
		</div>
	);
};
export default Filter;
