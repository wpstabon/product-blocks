const Divider = ( { label } ) => {
	return (
		<div className="wopb-field-wrap wopb-field-divider">
			{ label && <div className="wopb-field-label">{ label }</div> }
			<hr />
		</div>
	);
};
export default Divider;
