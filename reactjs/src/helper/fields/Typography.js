const { __ } = wp.i18n;
const { useState, useEffect } = wp.element;
const { Dropdown, SelectControl, TabPanel, ToolbarButton } = wp.components;
import GoogleList from './tools/GoogleFonts';
import FontList from './Fonts';
import Number from './Number';
import icons from '../../helper/icons';
import ResponsiveDevice from './tools/ResponsiveDevice';
import { getCustomFonts, typoIcon } from '../CommonPanel';
const { fontSizes } = wp.data.select( 'core/block-editor' ).getSettings();

const Typography = ( {
	value,
	disableClear,
	label,
	device,
	setDevice,
	onChange,
	isToolbar,
} ) => {
	const [ state, setState ] = useState( {
		open: false,
		text: '',
		custom: false,
	} );
	const { open, text, custom } = state;
	const CustomList = getCustomFonts();

	const styleOptions = [
		{ value: 'normal', label: __( 'Normal', 'product-blocks' ) },
		{ value: 'italic', label: __( 'Italic', 'product-blocks' ) },
		{ value: 'oblique', label: __( 'Oblique', 'product-blocks' ) },
		{ value: 'initial', label: __( 'Initial', 'product-blocks' ) },
		{ value: 'inherit', label: __( 'Inherit', 'product-blocks' ) },
	];

	const decorationOptions = [
		{ value: 'none', label: __( 'None', 'product-blocks' ) },
		{ value: 'inherit', label: __( 'Inherit', 'product-blocks' ) },
		{ value: 'underline', label: __( 'Underline', 'product-blocks' ) },
		{ value: 'overline', label: __( 'Overline', 'product-blocks' ) },
		{
			value: 'line-through',
			label: __( 'Line Through', 'product-blocks' ),
		},
	];

	useEffect( () => {
		const fetchData = async () => {
			const _size = getSize();
			let hasItem = false;
			(
				fontSizes || [ { size: 13 }, { size: 16 }, { size: 20 } ]
			).forEach( ( data ) => {
				if ( _size == data.size ) {
					hasItem = true;
				}
			} );
			if ( ! hasItem && typeof value.size === 'undefined' ) {
				hasItem = true;
			}
			setState( { ...state, custom: hasItem } );
		};
		fetchData();
	}, [] );

	const _getWeight = ( value ) => {
		if ( value && value.family && value.family != 'none' ) {
			let weight = GoogleList.filter( ( o ) => {
				return o.n == value.family;
			} );
			if ( weight.length == 0 ) {
				weight = FontList.filter( ( o ) => {
					return o.n == value.family;
				} );
			}
			if ( weight.length == 0 ) {
				weight = CustomList.filter( ( o ) => {
					return o.n == value.family;
				} );
			}
			if ( typeof weight[ 0 ] !== 'undefined' ) {
				weight = weight[ 0 ].v;
				return weight.map( ( w ) => {
					return { value: w, label: w };
				} );
			}
			return [ { value: '', label: '- None -' } ];
		}
		return [
			{ value: '100', label: '100' },
			{ value: '200', label: '200' },
			{ value: '300', label: '300' },
			{ value: '400', label: '400' },
			{ value: '500', label: '500' },
			{ value: '600', label: '600' },
			{ value: '700', label: '700' },
			{ value: '800', label: '800' },
			{ value: '900', label: '900' },
		];
	};

	const setSettings = ( type, val, source ) => {
		if ( type == 'family' ) {
			if ( val ) {
				setState( { ...state, open: false } );
				val = {
					[ type ]: val,
					type: ( source == 'google'
						? GoogleList
						: source == 'custom_font'
						? CustomList
						: FontList
					).filter( ( o ) => {
						return o.n == val;
					} )[ 0 ].f,
				};
				const fontWeight = _getWeight( val );
				if ( fontWeight[ 0 ] ) {
					val = { ...val, weight: fontWeight[ 0 ].value || '' };
				}
			}
		} else if ( type == 'define' ) {
			val = {
				size: Object.assign( {}, value.size, { [ device ]: val } ),
			};
		} else {
			val = { [ type ]: val };
		}
		onChange( Object.assign( {}, value, val ) );
	};

	const searchText = ( val ) => {
		setState( { ...state, text: val } );
	};

	const _fontArray = ( fonts ) => {
		return fonts.filter( ( name ) => {
			if ( text ) {
				if ( name.n.toLowerCase().indexOf( text ) !== -1 ) {
					return { value: name.n };
				}
			} else {
				return { value: name.n };
			}
		} );
	};

	const getSize = () => {
		return value?.size ? value?.size[ device ] : '';
	};

	const transform = value && value.transform ? value.transform : '';
	const isActive = value && value.openTypography ? true : false;
	const _size = getSize();

	const googleDisabled =
		wopb_data.settings.hasOwnProperty( 'disable_google_font' ) &&
		wopb_data.settings.disable_google_font == 'yes'
			? true
			: false;
	const _System = _fontArray( FontList );
	const _Google = _fontArray( GoogleList );
	const _CustomFont = _fontArray( CustomList );

	const clickOutSide = ( e ) => {
		if (
			! e.target.closest( '.wopb-family-field-data' ) &&
			! e.target.closest( '.wopb-family-field' ) &&
			open
		) {
			setState( { ...state, open: ! open } );
		}
	};
	const typoHtml = () => {
		return (
			<div
				className="wopb-field-typography"
				onClick={ ( e ) => clickOutSide( e ) }
			>
				<div className="wopb-typo-option">
					{ custom ? (
						<Number
							min={ 1 }
							max={ 300 }
							step={ 1 }
							unit={ [ 'px', 'em' ] }
							responsive
							typo={ true }
							label={ __( 'Size', 'product-blocks' ) }
							value={ value && value.size }
							device={ device }
							setDevice={ setDevice }
							onChange={ ( v ) => setSettings( 'size', v ) }
						/>
					) : (
						<>
							<div className="wopb-field-label-responsive">
								<label>Size</label>
								<ResponsiveDevice
									setDevice={ setDevice }
									device={ device }
								/>
							</div>
							<div className="wopb-default-font-field">
								{ (
									fontSizes || [
										{ size: 13 },
										{ size: 16 },
										{ size: 20 },
									]
								).map( ( data, k ) => {
									return (
										<span
											key={ k }
											className={
												'wopb-default-font' +
												( _size == data.size
													? ' active'
													: '' )
											}
											onClick={ () => {
												setSettings(
													'define',
													data.size
												);
												setState( {
													...state,
													custom: false,
												} );
											} }
										>
											{ data.size }
										</span>
									);
								} ) }
							</div>
						</>
					) }
					<div
						className={
							`wopb-typo-custom-setting` +
							( custom ? ' active' : '' )
						}
						onClick={ () =>
							setState( { ...state, custom: ! custom } )
						}
					>
						<span className="dashicons dashicons-admin-settings" />
					</div>
				</div>

				<div className="wopb-typ-container">
					<div className="wopb-typo-section">
						<div className="wopb-typo-family wopb-typo-family-wrap">
							<div className="wopb-typo-family">
								<label>Family</label>
								<span
									className="wopb-family-field-data"
									onClick={ () =>
										setState( { ...state, open: ! open } )
									}
								>
									{ ( value && value.family ) ||
										__( 'Select Font', 'product-blocks' ) }
									{ open ? (
										<span className="dashicons dashicons-arrow-up-alt2"></span>
									) : (
										<span className="dashicons dashicons-arrow-down-alt2"></span>
									) }
								</span>
								{ open && (
									<span className="wopb-family-field">
										<span className="wopb-family-list">
											<input
												type="text"
												onChange={ ( e ) =>
													searchText( e.target.value )
												}
												placeholder="Search.."
												value={ text }
											/>
											{ _CustomFont.length > 0 && (
												<>
													<span className="wopb-family-disabled">
														Custom Fonts
													</span>
													{ _CustomFont.map(
														( item, k ) => (
															<span
																className="wopb_custom_font_option"
																key={ k }
																onClick={ () =>
																	setSettings(
																		'family',
																		item.n,
																		'custom_font'
																	)
																}
															>
																{ item.n }{ ' ' }
																{ ! wopb_data.active && (
																	<a
																		href={
																			'https://www.wpxpo.com/wowstore/pricing/?utm_source=db-wstore-editor&utm_medium=custom-font&utm_campaign=wstore-dashboard'
																		}
																		target="blank"
																	>
																		(PRO)
																	</a>
																) }
															</span>
														)
													) }
												</>
											) }
											{ _System.length > 0 && (
												<>
													<span className="wopb-family-disabled">
														System Fonts
													</span>
													{ _System.map(
														( item, k ) => (
															<span
																key={ k }
																onClick={ () =>
																	setSettings(
																		'family',
																		item.n
																	)
																}
															>
																{ item.n }
															</span>
														)
													) }
												</>
											) }
											{ googleDisabled ? (
												<span
													className="wopb-family-disabled"
													style={ { color: 'red' } }
												>
													Google Fonts Disabled
												</span>
											) : (
												<>
													{ _Google.length > 0 && (
														<>
															<span className="wopb-family-disabled">
																Google Fonts
															</span>
															{ _Google.map(
																( item, k ) => (
																	<span
																		key={
																			k
																		}
																		onClick={ () =>
																			setSettings(
																				'family',
																				item.n,
																				'google'
																			)
																		}
																	>
																		{
																			item.n
																		}
																	</span>
																)
															) }
														</>
													) }
												</>
											) }
										</span>
									</span>
								) }
							</div>
						</div>
					</div>
					<div className="wopb-typo-section">
						<SelectControl
							label={ __( 'Weight', 'product-blocks' ) }
							value={ value && value.weight }
							options={ _getWeight( value ) }
							onChange={ ( v ) => setSettings( 'weight', v ) }
						/>
					</div>
				</div>
				<div className="wopb-typ-container wopb-typ-family">
					<div className="wopb-typo-section">
						<Number
							min={ 1 }
							max={ 300 }
							step={ 1 }
							responsive
							device={ device }
							setDevice={ setDevice }
							unit={ [ 'px', 'em' ] }
							label={ __( 'Height', 'product-blocks' ) }
							value={ value && value.height }
							onChange={ ( v ) => setSettings( 'height', v ) }
						/>
					</div>
					<div className="wopb-typo-section">
						<Number
							min={ -10 }
							max={ 30 }
							responsive
							step={ 0.1 }
							unit={ [ 'px', 'em' ] }
							device={ device }
							setDevice={ setDevice }
							label={ __( 'Spacing', 'product-blocks' ) }
							value={ value && value.spacing }
							onChange={ ( v ) => setSettings( 'spacing', v ) }
						/>
					</div>
				</div>
				<div className="wopb-typ-container">
					<div className="wopb-typo-section">
						<SelectControl
							label={ __( 'Decoration', 'product-blocks' ) }
							value={ value && value.decoration }
							options={ decorationOptions }
							onChange={ ( v ) => setSettings( 'decoration', v ) }
						/>
					</div>
					<div className="wopb-typo-section">
						<SelectControl
							label={ __( 'Style', 'product-blocks' ) }
							value={ value && value.style }
							options={ styleOptions }
							onChange={ ( v ) => setSettings( 'style', v ) }
						/>
					</div>
					<div className="wopb-typo-section">
						<label>{ __( 'Transform', 'product-blocks' ) }</label>
						<div className="wopb-transform-style">
							<span
								className={
									transform == 'uppercase' ? 'active' : ''
								}
								onClick={ () =>
									setSettings(
										'transform',
										transform == 'uppercase'
											? ''
											: 'uppercase'
									)
								}
							>
								AB
							</span>
							<span
								className={
									transform == 'lowercase' ? 'active' : ''
								}
								onClick={ () =>
									setSettings(
										'transform',
										transform == 'lowercase'
											? ''
											: 'lowercase'
									)
								}
							>
								ab
							</span>
							<span
								className={
									transform == 'capitalize' ? 'active' : ''
								}
								onClick={ () =>
									setSettings(
										'transform',
										transform == 'capitalize'
											? ''
											: 'capitalize'
									)
								}
							>
								Ab
							</span>
						</div>
					</div>
				</div>
			</div>
		);
	};

	return (
		<div
			className={ `${
				isToolbar ? '' : 'wopb-field-wrap wopb-field-typo'
			}` }
		>
			<div className="wopb-field-header wopb-flex-content">
				{ ! isToolbar && (
					<div className="wopb-field-label-responsive">
						{ label && <label>{ label }</label> }
					</div>
				) }
				<Dropdown
					className="wopb-range-control"
					renderToggle={ ( { isOpen, onToggle } ) => {
						if ( isToolbar ) {
							return (
								<ToolbarButton
									className="wopb-typography-toolbar-btn"
									label={
										label ||
										__( 'Typography', 'product-blocks' )
									}
									onClick={ () => {
										onToggle();
										setSettings( 'openTypography', 1 );
									} }
								>
									{ typoIcon }
								</ToolbarButton>
							);
						}
						return (
							<div className="wopb-edit-btn">
								<div className="wopb-typo-control">
									{ ! disableClear && isActive && (
										<div
											className="active wopb-base-control-btn dashicons dashicons-image-rotate"
											onClick={ () => {
												if ( isOpen ) {
													onToggle();
												}
												setSettings(
													'openTypography',
													0
												);
											} }
										></div>
									) }
									<div
										className={
											( isActive ? 'active ' : '' ) +
											' wopb-icon-style'
										}
										onClick={ () => {
											onToggle();
											setSettings( 'openTypography', 1 );
										} }
									>
										{ icons.setting }
									</div>
								</div>
							</div>
						);
					} }
					renderContent={ () => {
						// if (isToolbar) {
						//     return (
						//         <TabPanel
						//             className="wopb-toolbar-tab"
						//             tabs={ [
						//                 {
						//                     name: "typo",
						//                     title: label
						//                 }
						//             ] }
						//         >
						//             {
						//                 (tab) => typoHtml()
						//             }
						//         </TabPanel>
						//     );
						// }
						return typoHtml();
					} }
				/>
			</div>
		</div>
	);
};
export default Typography;
