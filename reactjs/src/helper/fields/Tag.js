import LinkGenerator from '../LinkGenerator';

const Tag = ( { value, options, label, pro, disabled, onChange } ) => {
	const tagValue = options || [
		{ value: 'h1', label: 'H1' },
		{ value: 'h2', label: 'H2' },
		{ value: 'h3', label: 'H3' },
		{ value: 'h4', label: 'H4' },
		{ value: 'h5', label: 'H5' },
		{ value: 'h6', label: 'H6' },
		{ value: 'p', label: 'P' },
		{ value: 'div', label: 'DIV' },
		{ value: 'span', label: 'SPAN' },
	];

	const _set = ( val ) => {
		if ( pro && ! wopb_data.active ) {
			return;
		}
		if ( disabled && value == val ) {
			onChange( '' );
		} else {
			onChange( val );
		}
	};

	return (
		<div
			className={ `wopb-field-wrap wopb-field-tag ${
				pro && ! wopb_data.active ? 'wopb-pro-field' : ''
			}` }
		>
			{ label && <label>{ label }</label> }
			<div className="wopb-sub-field">
				{ tagValue.map( ( data, index ) => {
					return (
						<span
							key={ index }
							className={
								data.value == value
									? 'wopb-tag-button active'
									: 'wopb-tag-button'
							}
							onClick={ () => _set( data.value ) }
						>
							{ data.label }
						</span>
					);
				} ) }
			</div>
			{ pro && ! wopb_data.active && (
				<div className="wopb-field-pro-message">
					Unlock It! Explore More{ ' ' }
					<a
						href={ LinkGenerator(
							'https://www.wpxpo.com/wowstore/all-features/',
							'blockProFeature'
						) }
						target="_blank"
						rel="noreferrer"
					>
						Pro Features
					</a>
				</div>
			) }
		</div>
	);
};
export default Tag;
