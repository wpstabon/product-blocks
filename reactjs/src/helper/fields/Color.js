import LinkGenerator from '../LinkGenerator';

const { __ } = wp.i18n;
const { ColorPicker, Dropdown } = wp.components;
const { Fragment } = wp.element;
const editor = wp.data.select( 'core/editor' );

const Color = ( props ) => {
	const { label, value, onChange, disableClear, pro, inline } = props;

	const _setColor = ( color ) => {
		if ( pro && ! wopb_data.active ) {
			return;
		}
		onChange(
			'rgba(' +
				color.rgb.r +
				',' +
				color.rgb.g +
				',' +
				color.rgb.b +
				',' +
				color.rgb.a +
				')'
		);
	};

	const _getPreset = () => {
		const _global = localStorage.getItem( 'wopbGlobal' );
		const settings = JSON.parse(
			( _global && _global !== '[object Object]' ) || '[]'
		);
		return {
			theme: settings.productxColorOnly
				? []
				: editor
				? editor.getEditorSettings().colors
				: [],
			colors: [
				'var(--productx-color1)',
				'var(--productx-color2)',
				'var(--productx-color3)',
				'var(--productx-color4)',
				'var(--productx-color5)',
				'var(--productx-color6)',
				'var(--productx-color7)',
				'var(--productx-color8)',
				'var(--productx-color9)',
			],
		};
	};

	const PresetColor = () => {
		const colorField = _getPreset( true );
		const computedStyles = getComputedStyle( document.body );
		return (
			<div className="wopb-preset-color">
				{ colorField.theme.length > 0 && (
					<Fragment>
						<div>Theme Color</div>
						{ colorField.theme.map( ( val, k ) => {
							return (
								<span
									key={ k }
									onClick={ () => onChange( val.color ) }
									style={ { backgroundColor: val.color } }
								/>
							);
						} ) }
					</Fragment>
				) }
				{ colorField.colors.length > 0 && (
					<Fragment>
						<div>WowStore Color</div>
						{ colorField.colors.map( ( val, k ) => {
							const colorVar =
								val.match( /var\((--[^)]+)\)/ )[ 1 ];
							const color = colorVar
								? computedStyles
										.getPropertyValue( colorVar )
										.trim()
								: val;
							return (
								<span
									key={ k }
									onClick={ () => onChange( color ?? val ) }
									style={ { backgroundColor: val } }
								/>
							);
						} ) }
					</Fragment>
				) }
			</div>
		);
	};

	return (
		<div
			className={ `wopb-field-wrap wopb-field-color ${
				inline ? 'wopb-color-inline' : 'wopb-color-block'
			} ${ pro && ! wopb_data.active ? 'wopb-pro-field' : '' }` }
		>
			{ label && ! inline && (
				<span className="wopb-color-label">
					<label>{ label }</label>
				</span>
			) }
			{ inline ? (
				<Fragment>
					{ /*<div className="wopb-color-inline__label">*/ }
					{ /*    <div className="wopb-color-label__content">*/ }
					{ /*        { */ }
					{ /*          inline &&  label &&*/ }
					{ /*            <label className="wopb-color-label">{label}</label>*/ }
					{ /*        }*/ }
					{ /*        { value &&*/ }
					{ /*            <span className="wopb-color-preview" style={{backgroundColor: value}}></span>*/ }
					{ /*        }*/ }
					{ /*    </div>*/ }
					{ /*    <div className="wopb-color-label__content">*/ }
					{ /*        { (!disableClear && value) && */ }
					{ /*            <div className="wopb-clear dashicons dashicons-image-rotate" onClick={() => {onChange('')}} />*/ }
					{ /*        }*/ }
					{ /*        <Dropdown*/ }
					{ /*                className="wopb-range-control"*/ }
					{ /*                renderToggle={({ onToggle, onClose }) => (*/ }
					{ /*                    <div className="wopb-control-button" onClick={() => onToggle()} style={{background:value}} />*/ }
					{ /*                )}*/ }
					{ /*                renderContent={() => (*/ }
					{ /*                    <div className="wopb-common-popup wopb-color-popup">*/ }
					{ /*                        <ColorPicker*/ }
					{ /*                            color={value?value:''}*/ }
					{ /*                            onChangeComplete={color => _setColor(color)}/>*/ }
					{ /*                    </div>*/ }
					{ /*                )}*/ }
					{ /*            />*/ }
					{ /*    </div>*/ }
					{ /*</div>*/ }
					<div className="wopb-color-popup">
						<ColorPicker
							color={ value ? value : '' }
							onChangeComplete={ ( color ) => _setColor( color ) }
						/>
						<PresetColor />
					</div>
				</Fragment>
			) : (
				<div className="wopb-sub-field">
					{ label && (
						<span className="wopb-color-label">
							{ ! disableClear && value && (
								<div
									className="wopb-clear dashicons dashicons-image-rotate"
									onClick={ () => {
										onChange( '' );
									} }
								/>
							) }
						</span>
					) }
					<Dropdown
						className="wopb-range-control"
						placement="top left"
						renderToggle={ ( { onToggle, onClose } ) => (
							<div
								className="wopb-control-button"
								onClick={ () => onToggle() }
								style={ { background: value } }
							/>
						) }
						renderContent={ () => (
							<div className="wopb-common-popup wopb-color-popup">
								<ColorPicker
									color={ value ? value : '' }
									onChangeComplete={ ( color ) =>
										_setColor( color )
									}
								/>
								<PresetColor />
							</div>
						) }
					/>
				</div>
			) }
			{ pro && ! wopb_data.active && (
				<div className="wopb-field-pro-message">
					{ __( 'Unlock It! Explore More', 'product-blocks' ) }{ ' ' }
					<a
						href={ LinkGenerator(
							'https://www.wpxpo.com/wowstore/all-features/',
							'blockProFeature'
						) }
						target="_blank"
						rel="noreferrer"
					>
						{ __( 'Pro Features', 'product-blocks' ) }
					</a>
				</div>
			) }
		</div>
	);
};
export default Color;
