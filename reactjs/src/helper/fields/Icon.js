import IconPack from './tools/IconPack';
const { Dropdown } = wp.components;
const { useState } = wp.element;

const Icon = ( { value, label, onChange, inline, dynamicClass } ) => {
	const [ searchQuery, setSearchQuery ] = useState( '' );
	const [ iconType, setIconType ] = useState( '' );

	const _filterValue = () => {
		let iconSlug = Object.keys( IconPack );
		if ( searchQuery ) {
			iconSlug = iconSlug.filter( ( el ) =>
				el.includes( searchQuery.toLowerCase() )
			);
		}
		if ( iconType ) {
			iconSlug = iconSlug.filter( ( el ) => el.includes( iconType ) );
		}
		return iconSlug;
	};

	return (
		<div className={ `wopb-field-wrap wopb-field-icons` }>
			{ label && ! inline && <label>{ label }</label> }

			{ ! inline && (
				<div className="wopb-sub-field">
					<Dropdown
						position="bottom right"
						className=""
						renderToggle={ ( { onToggle, onClose } ) => (
							<div className="wopb-icon-input">
								{ value && IconPack[ value ] && (
									<>
										<div onClick={ () => onToggle() }>
											{ IconPack[ value ] }
										</div>
										<div
											className="wopb-icon-close"
											onClick={ () => onChange( '' ) }
										></div>
									</>
								) }
								<span onClick={ () => onToggle() }></span>
							</div>
						) }
						renderContent={ () => (
							<div className="wopb-sub-dropdown wopb-icondropdown-container">
								<div className="wopb-search-icon">
									<input
										type="text"
										placeholder="Search Icons"
										onChange={ ( e ) =>
											setSearchQuery( e.target.value )
										}
									/>
								</div>
								<div className="wopb-searchIcon-filter">
									Icon Type Name
									<select
										onChange={ ( e ) =>
											setIconType( e.target.value )
										}
									>
										<option selected value="">
											Show All Icon
										</option>
										<option value="solid">
											Solid Icon
										</option>
										<option value="line">Line Icon</option>
									</select>
								</div>
								<div className="wopb-dropdown-icon">
									{ _filterValue().map( ( key ) => {
										return (
											<span
												onClick={ () =>
													onChange( key )
												}
												key={ key }
												className={ `${
													value == key ? 'active' : ''
												}` }
											>
												{ IconPack[ key ] }
											</span>
										);
									} ) }
								</div>
							</div>
						) }
					/>
				</div>
			) }
			{ inline && (
				<div className="wopb-sub-dropdown wopb-icondropdown-container">
					<div className="wopb-search-icon">
						<input
							type="text"
							placeholder="Search Icons"
							onChange={ ( e ) =>
								setSearchQuery( e.target.value )
							}
						/>
					</div>
					<div className="wopb-searchIcon-filter">
						Icon Type Name
						<select
							onChange={ ( e ) => setIconType( e.target.value ) }
						>
							<option selected value="">
								Show All Icon
							</option>
							<option value="solid">Solid Icon</option>
							<option value="line">Line Icon</option>
						</select>
					</div>
					<div className="wopb-dropdown-icon">
						{ _filterValue().map( ( key ) => {
							return (
								<span
									onClick={ () => onChange( key ) }
									key={ key }
									className={ `${
										value == key ? 'active' : ''
									}` }
								>
									{ IconPack[ key ] }
								</span>
							);
						} ) }
					</div>
				</div>
			) }
		</div>
	);
};
export default Icon;
