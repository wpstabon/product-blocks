const { __ } = wp.i18n;
const { MediaUpload } = wp.blockEditor;

const Media = ( { type, label, multiple, onChange, value } ) => {
	const setSettings = ( media ) => {
		if ( multiple ) {
			const medias = [];
			media.forEach( ( single ) => {
				if ( single && single.url ) {
					medias.push( { url: single.url, id: single.id } );
				}
			} );
			onChange( value ? value.concat( medias ) : medias );
		} else if ( media && media.url ) {
			onChange( { url: media.url, id: media.id } );
		}
	};

	const removeImage = ( id ) => {
		if ( multiple ) {
			const _v = value.slice();
			_v.splice( id, 1 );
			onChange( _v );
		} else {
			onChange( {} );
		}
	};

	const isUrl = ( url ) => {
		if (
			[ 'wbm', 'jpg', 'jpeg', 'gif', 'png' ].indexOf(
				url.split( '.' ).pop().toLowerCase()
			) != -1
		) {
			return url;
		}
		return wopb_data.url + 'assets/img/wopb-fallback-img.png';
	};

	const changeUrl = ( val ) => {
		if ( ! multiple ) {
			onChange( { url: val.target.value, id: 99999 } );
		}
	};

	return (
		<div className="wopb-field-wrap wopb-field-media">
			{ label && <label>{ label }</label> }
			<MediaUpload
				onSelect={ ( val ) => setSettings( val ) }
				allowedTypes={ type || [ 'image' ] }
				multiple={ multiple || false }
				value={ value }
				render={ ( { open } ) => (
					<div className="wopb-media-img">
						{ multiple ? (
							<div className="wopb-multiple-img">
								{ value.length > 0 &&
									value.map( ( v, index ) => {
										return (
											<span
												key={ index }
												className="wopb-media-image-item"
											>
												<img
													src={ isUrl( v.url ) }
													alt={ __(
														'image',
														'product-blocks'
													) }
												/>
												<span
													className="wopb-image-close-icon dashicons dashicons-no-alt"
													onClick={ () =>
														removeImage( index )
													}
												/>
											</span>
										);
									} ) }
								<div
									onClick={ open }
									className={ 'wopb-placeholder-image' }
								>
									<span>
										{ __( 'Upload', 'product-blocks' ) }
									</span>
								</div>
							</div>
						) : value && value.url ? (
							<span className="wopb-media-image-item">
								<input
									type="text"
									onChange={ ( v ) => changeUrl( v ) }
									value={ value.url }
								/>
								<div
									onClick={ open }
									className="wopb-placeholder-image"
								>
									<span>
										{ __( 'Upload', 'product-blocks' ) }
									</span>
								</div>
								<span
									className="wopb-image-close-icon dashicons dashicons-no-alt"
									onClick={ () => removeImage() }
								/>
							</span>
						) : (
							<span>
								<input
									type="text"
									onChange={ ( v ) => changeUrl( v ) }
									value=""
								/>
								<div
									onClick={ open }
									className="wopb-placeholder-image"
								>
									<span>
										{ __( 'Upload', 'product-blocks' ) }
									</span>
								</div>
							</span>
						) }
					</div>
				) }
			/>
		</div>
	);
};
export default Media;
