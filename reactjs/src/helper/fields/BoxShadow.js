const { __ } = wp.i18n;
const { Dropdown, ToggleControl } = wp.components;
import Dimension from './Dimension';
import Color from './Color';
import icons from '../../helper/icons';

const BoxShadow = ( { value, label, onChange } ) => {
	const isActive = value && value.openShadow ? true : false;
	const defShadow = {
		inset: '',
		width: { top: 4, right: 3, bottom: 2, left: 1 },
		color: '#555d66',
	};

	const setSettings = ( type, val ) => {
		onChange(
			Object.assign( {}, defShadow, value, { [ type ]: val || 0 } )
		);
	};

	return (
		<div className="wopb-field-wrap wopb-popup-control wopb-field-shadow">
			{ label && <label>{ label }</label> }
			<Dropdown
				className="wopb-range-control"
				position="top right"
				renderToggle={ ( { isOpen, onToggle } ) => (
					<div className="wopb-edit-btn">
						{ isActive && (
							<div
								className="active wopb-base-control-btn dashicons dashicons-image-rotate"
								onClick={ () => {
									if ( isOpen ) {
										onToggle();
									}
									setSettings( 'openShadow', 0 );
								} }
							></div>
						) }
						<div
							className={ `${
								isActive && 'active '
							} wopb-icon-style` }
							onClick={ () => {
								onToggle();
								setSettings( 'openShadow', 1 );
							} }
							aria-expanded={ isOpen }
						>
							{ icons.setting }
						</div>
					</div>
				) }
				renderContent={ () => (
					<div className="wopb-common-popup">
						<Dimension
							label={ __( 'Shadow', 'product-blocks' ) }
							value={ value.width || '' }
							onChange={ ( val ) => setSettings( 'width', val ) }
							dataLabel={ [
								'offset-x',
								'offset-y',
								'blur',
								'spread',
							] }
							min={ 0 }
							max={ 100 }
							step={ 1 }
						/>
						<Color
							inline
							inset
							label={ __( 'Color', 'product-blocks' ) }
							value={ value.color || '' }
							onChange={ ( val ) => setSettings( 'color', val ) }
						/>
						<ToggleControl
							label={ __( 'Inset', 'product-blocks' ) }
							checked={ value.inset ? 1 : 0 }
							onChange={ ( val ) =>
								setSettings( 'inset', val ? 'inset' : '' )
							}
						/>
					</div>
				) }
			/>
		</div>
	);
};
export default BoxShadow;
