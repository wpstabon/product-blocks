const { __ } = wp.i18n;
import LinkGenerator from '../LinkGenerator';
import Color from './Color';
import Range from './Range';
const {
	Dropdown,
	GradientPicker,
	TextControl,
	ToggleControl,
	SelectControl,
	FocalPointPicker,
} = wp.components;
const { MediaUpload } = wp.blockEditor;
const globalSettings = wp.data.select( 'core/editor' );

const Color2 = ( props ) => {
	const {
		value,
		label,
		pro,
		onChange,
		clip,
		image,
		video,
		extraClass,
		customGradient,
	} = props;
	const colorDef = {
		openColor: 0,
		type: 'color',
		color: '#037fff',
		gradient:
			'linear-gradient(90deg, rgb(6, 147, 227) 0%, rgb(155, 81, 224) 100%)',
		clip: false,
	};

	const _set = ( val, type ) => {
		if ( pro && ! wopb_data.active ) {
			return;
		}
		// const def = type == 'type' ? val : type || 'color'

		// for Compatiblity
		const __val = value;
		if ( typeof __val.gradient === 'object' ) {
			__val.gradient = _compatiblity( __val.gradient );
		}

		onChange(
			Object.assign(
				{},
				colorDef,
				__val,
				{ openColor: 1 },
				{ [ type ]: val }
			)
		);
	};

	// for Compatiblity
	const _compatiblity = ( data ) => {
		if ( typeof data === 'object' ) {
			if ( data.type == 'linear' ) {
				return (
					'linear-gradient(' +
					data.direction +
					'deg, ' +
					data.color1 +
					' ' +
					data.start +
					'%, ' +
					data.color2 +
					' ' +
					data.stop +
					'%)'
				);
			}
			return (
				'radial-gradient( circle at ' +
				data.radial +
				' , ' +
				data.color1 +
				' ' +
				data.start +
				'%,' +
				data.color2 +
				' ' +
				data.stop +
				'%);'
			);
		}
		return data || {};
	};

	return (
		<div
			className={ `wopb-field-wrap wopb-field-color2 ${
				pro && ! wopb_data.active ? 'wopb-pro-field' : ''
			} ${ video && image ? 'wopb-field-video-image' : '' }` }
		>
			{ label && <label>{ label }</label> }
			<div className="wopb-sub-field">
				{ value.openColor ? (
					<div
						className="active wopb-base-control-btn dashicons dashicons-image-rotate"
						onClick={ () => {
							_set( 0, 'openColor' );
						} }
					/>
				) : (
					''
				) }
				<div className="wopb-color2-btn__group">
					<Dropdown
						className="wopb-range-control"
						renderToggle={ ( { onToggle, onClose } ) => (
							<div className="wopb-button-group">
								<button
									className={ `${
										value.type == 'color' && value.openColor
											? 'active'
											: ''
									} wopb-icon-style` }
									onClick={ () => {
										onToggle();
										_set( 'color', 'type' );
									} }
								>
									Solid
								</button>
							</div>
						) }
						renderContent={ () => (
							<div className="wopb-popup-select wopb-common-popup">
								<Color
									label={ 'Color' }
									inline
									disableClear
									value={ value.color || '#16d03e' }
									onChange={ ( val ) => {
										_set( val, 'color' );
									} }
								/>
							</div>
						) }
					/>
					<Dropdown
						className="wopb-range-control"
						renderToggle={ ( { onToggle, onClose } ) => (
							<div className="wopb-button-group">
								<button
									className={ `${
										value.type == 'gradient' &&
										value.openColor
											? 'active'
											: ''
									} wopb-icon-style` }
									onClick={ () => {
										onToggle();
										_set( 'gradient', 'type' );
									} }
								>
									Gradient
								</button>
							</div>
						) }
						renderContent={ () => (
							<div className="wopb-popup-select wopb-common-popup">
								<GradientPicker
									__nextHasNoMargin={ true }
									value={
										typeof value.gradient === 'object'
											? _compatiblity( value.gradient )
											: value.gradient
									}
									onChange={ ( val ) =>
										_set( val, 'gradient' )
									}
									gradients={
										( globalSettings
											? globalSettings.getEditorSettings()
													.gradients
											: [] ) || []
									}
								/>
							</div>
						) }
					/>
					{ image && (
						<Dropdown
							className="wopb-range-control"
							renderToggle={ ( { onToggle, onClose } ) => (
								<div className="wopb-button-group">
									<button
										className={ `${
											value.type == 'image' &&
											value.openColor
												? 'active'
												: ''
										} wopb-icon-style` }
										onClick={ () => {
											onToggle();
											_set( 'image', 'type' );
										} }
									>
										Image
									</button>
								</div>
							) }
							renderContent={ () => (
								<div className="wopb-image-control--popup">
									<MediaUpload
										onSelect={ ( v ) => {
											if ( v.url ) {
												_set( v.url, 'image' );
											}
										} }
										allowedTypes={ [ 'image' ] }
										value={ value.image || '' }
										render={ ( { open } ) => (
											<div className="wopb-field-media">
												<span className="wopb-media-image-item">
													{ value.image ? (
														<div className="wopb-imgvalue-wrap">
															<input
																type="text"
																value={
																	value.image
																}
																placeholder="Image Url"
																onChange={ (
																	v
																) =>
																	_set(
																		v.target
																			.value,
																		'image'
																	)
																}
															/>
															<span
																className={
																	'wopb-image-close-icon dashicons dashicons-no-alt'
																}
																onClick={ () =>
																	_set(
																		'',
																		'image'
																	)
																}
															/>
														</div>
													) : (
														<input
															type="text"
															placeholder="Image Url"
															onChange={ ( v ) =>
																_set(
																	v.target
																		.value,
																	'image'
																)
															}
														/>
													) }
													<div
														className={
															'wopb-placeholder-image'
														}
													>
														{ value.image ? (
															<div className="wopb-focalpoint-wrapper">
																<FocalPointPicker
																	url={
																		value.image
																	}
																	value={
																		value.position
																	}
																	// label={label}
																	onDragStart={ (
																		position
																	) =>
																		_set(
																			position,
																			'position'
																		)
																	}
																	onDrag={ (
																		position
																	) =>
																		_set(
																			position,
																			'position'
																		)
																	}
																	onChange={ (
																		position
																	) =>
																		_set(
																			position,
																			'position'
																		)
																	}
																/>
															</div>
														) : (
															<span
																onClick={ open }
																className="dashicons dashicons-plus-alt2 wopb-media-upload"
															/>
														) }
													</div>
												</span>
											</div>
										) }
									/>
									<div className="wopb-popup-select wopb-common-popup">
										<SelectControl
											label={ __(
												'Attachment',
												'product-blocks'
											) }
											value={ value.attachment }
											options={ [
												{ label: 'Default', value: '' },
												{
													label: 'Scroll',
													value: 'scroll',
												},
												{
													label: 'fixed',
													value: 'Fixed',
												},
											] }
											onChange={ ( v ) =>
												_set( v, 'attachment' )
											}
										/>
										<SelectControl
											label={ __(
												'Repeat',
												'product-blocks'
											) }
											value={ value.repeat }
											options={ [
												{ label: 'Default', value: '' },
												{
													label: 'No-repeat',
													value: 'no-repeat',
												},
												{
													label: 'Repeat',
													value: 'repeat',
												},
												{
													label: 'Repeat-x',
													value: 'repeat-x',
												},
												{
													label: 'Repeat-y',
													value: 'repeat-y',
												},
											] }
											onChange={ ( v ) =>
												_set( v, 'repeat' )
											}
										/>
										<SelectControl
											label={ __(
												'Size',
												'product-blocks'
											) }
											value={ value.size }
											options={ [
												{ label: 'Default', value: '' },
												{
													label: 'Auto',
													value: 'auto',
												},
												{
													label: 'Cover',
													value: 'cover',
												},
												{
													label: 'Contain',
													value: 'contain',
												},
												// { label: 'Custom', value: 'initial' }
											] }
											onChange={ ( v ) =>
												_set( v, 'size' )
											}
										/>
									</div>
									<div className="wopb-editor-fallback-bg">
										<Color
											label={ 'Fallback Color' }
											value={
												value.fallbackColor ??
												value.color
											}
											onChange={ ( v ) => {
												_set( v, 'fallbackColor' );
											} }
										/>
									</div>
								</div>
							) }
						/>
					) }
					{ video && (
						<Dropdown
							className="wopb-range-control"
							renderToggle={ ( { onToggle, onClose } ) => (
								<div className="wopb-button-group">
									<button
										className={ `${
											value.type == 'video' &&
											value.openColor
												? 'active'
												: ''
										} wopb-icon-style` }
										onClick={ () => {
											onToggle();
											_set( 'video', 'type' );
										} }
									>
										Video
									</button>
								</div>
							) }
							renderContent={ () => (
								<div className="wopb-popup-select wopb-common-popup">
									<TextControl
										value={ value.video }
										placeholder={ __(
											'Only Youtube & Vimeo  & Self Hosted Url',
											'product-blocks'
										) }
										label={ __(
											'Video URL',
											'product-blocks'
										) }
										onChange={ ( v ) => _set( v, 'video' ) }
									/>
									<Range
										value={ value.start || 0 }
										min={ 1 }
										max={ 12000 }
										step={ 1 }
										label={ __(
											'Start Time(Seconds)',
											'product-blocks'
										) }
										onChange={ ( v ) => _set( v, 'start' ) }
									/>
									<Range
										value={ value.end || '' }
										min={ 1 }
										max={ 12000 }
										step={ 1 }
										label={ __(
											'End Time(Seconds)',
											'product-blocks'
										) }
										onChange={ ( v ) => _set( v, 'end' ) }
									/>
									<ToggleControl
										label={ __(
											'Loop Video',
											'product-blocks'
										) }
										checked={ value.loop ? 1 : 0 }
										onChange={ ( v ) => _set( v, 'loop' ) }
									/>
									<MediaUpload
										onSelect={ ( v ) => {
											if ( v.url ) {
												_set( v.url, 'fallback' );
											}
										} }
										allowedTypes={ [ 'image' ] }
										value={ value.fallback || '' }
										render={ ( { open } ) => (
											<>
												<div className="wopb-field-media">
													<div className="wopb-fallback-lebel">
														Video Fallback Image
													</div>
													<span className="wopb-media-image-item">
														{ value.fallback && (
															<input
																type="text"
																value={
																	value.fallback
																}
																onChange={ (
																	v
																) =>
																	changeUrl(
																		v
																	)
																}
															/>
														) }
														<div
															className={
																'wopb-placeholder-image'
															}
														>
															{ value.fallback ? (
																<>
																	<img
																		src={
																			value.fallback
																		}
																		allt="Fallback Image"
																	/>
																	<span
																		className={
																			'wopb-image-close-icon dashicons dashicons-no-alt'
																		}
																		onClick={ () =>
																			_set(
																				'',
																				'fallback'
																			)
																		}
																	/>
																</>
															) : (
																<span
																	onClick={
																		open
																	}
																	className="dashicons dashicons-plus-alt2 wopb-media-upload"
																/>
															) }
														</div>
													</span>
												</div>
											</>
										) }
									/>
								</div>
							) }
						/>
					) }
				</div>
			</div>
			{ pro && ! wopb_data.active && (
				<div className="wopb-field-pro-message">
					{ __( 'Unlock It! Explore More', 'product-blocks' ) }{ ' ' }
					<a
						href={ LinkGenerator(
							'https://www.wpxpo.com/wowstore/all-features/',
							'blockProFeature'
						) }
						target="_blank"
						rel="noreferrer"
					>
						{ __( 'Pro Features', 'product-blocks' ) }
					</a>
				</div>
			) }
		</div>
	);
};
export default Color2;
