import { updateChildAttr } from '../CommonPanel';
import LinkGenerator from '../LinkGenerator';

const { __ } = wp.i18n;
const { Fragment } = wp.element;
const { ToggleControl } = wp.components;

const Toggle = ( {
	value,
	label,
	pro,
	help,
	onChange,
	clientId,
	updateChild,
} ) => {
	const setValue = ( val ) => {
		if ( pro ) {
			if ( wopb_data.active ) {
				onChange( val );
			} else if ( val == false ) {
				onChange( val );
			}
		} else {
			onChange( val );
		}
		if ( updateChild ) {
			updateChildAttr( clientId );
		}
	};

	return (
		<div className="wopb-field-wrap wopb-field-toggle">
			<div className="wopb-sub-field">
				<ToggleControl
					help={ help }
					checked={ value }
					label={
						label ? (
							<Fragment>
								{ label }{ ' ' }
								{ pro && ! wopb_data.active ? (
									<a
										className={ `wopb-field-pro-message-inline` }
										href={ LinkGenerator(
											'https://www.wpxpo.com/wowstore/all-features/',
											'blockProFeature'
										) }
										target="_blank"
										rel="noreferrer"
									>
										{ __(
											'Pro Features',
											'product-blocks'
										) }
									</a>
								) : (
									''
								) }
							</Fragment>
						) : (
							''
						)
					}
					onChange={ ( val ) => setValue( val ) }
				/>
			</div>
		</div>
	);
};
export default Toggle;
