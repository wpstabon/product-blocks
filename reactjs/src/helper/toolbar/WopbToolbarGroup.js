const { __ } = wp.i18n;
import {
	colorIcon,
	metaTextIcon,
	settingsIcon,
	styleIcon,
	typoIcon,
} from '../CommonPanel';
import LinkGenerator from '../LinkGenerator';

const { BlockControls } = wp.blockEditor;
const { Toolbar, ToolbarButton } = wp.components;

export default function WopbToolbarGroup( {
	children,
	text,
	pro = false,
	textScroll = false,
} ) {
	const freeUser = pro && ! wopb_data.active;
	// const freeUser = true; // testing

	return (
		<BlockControls>
			<div className="wopb-toolbar-group">
				{ text && (
					<div
						className={ `wopb-toolbar-group-text ${
							textScroll ? 'wopb-toolbar-text-ticker-wrapper' : ''
						}` }
					>
						<div className="wopb-toolbar-group-text-inner">
							{ __( text, 'product-blocks' ) +
								( freeUser ? ' (Pro)' : '' ) }
						</div>
					</div>
				) }
				{ freeUser ? (
					<div className="wopb-toolbar-group-block">
						<a
							href={ LinkGenerator(
								'https://www.wpxpo.com/wowstore/pricing',
								'blockProFeature'
							) }
							target="_blank"
							rel="noreferrer"
						>
							<div className="wopb-toolbar-group-block-overlay">
								Unlock It
							</div>
						</a>

						<PlaceholderButtons />
					</div>
				) : (
					<Toolbar>{ children }</Toolbar>
				) }
			</div>
		</BlockControls>
	);
}

function PlaceholderButtons() {
	return (
		<Toolbar>
			<ToolbarButton label="Typography">{ typoIcon }</ToolbarButton>
			<ToolbarButton label="Color">{ colorIcon }</ToolbarButton>
			<ToolbarButton label="Style">{ styleIcon }</ToolbarButton>
			<ToolbarButton label="Text">{ metaTextIcon }</ToolbarButton>
			<ToolbarButton label="Settings">{ settingsIcon }</ToolbarButton>
		</Toolbar>
	);
}
