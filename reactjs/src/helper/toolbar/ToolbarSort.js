import { sortSetValue } from '../fields/Sort';

export default function ToolbarSort( { store, key = 'sortSection', data } ) {
	const value = store.attributes[ key ];

	return (
		<div className="wopb-toolbar-sort">
			<span
				title="Move Element Up"
				role="button"
				aria-label="Move element up"
				className="wopb-toolbar-sort-btn"
				onClick={ () => {
					sortSetValue(
						'up',
						data,
						value,
						( v ) => store.setAttributes( { [ key ]: v } ),
						store
					);
				} }
			>
				<svg
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden="true"
					focusable="false"
				>
					<path d="M6.5 12.4L12 8l5.5 4.4-.9 1.2L12 10l-4.5 3.6-1-1.2z" />
				</svg>
			</span>
			<span
				title="Move Element Down"
				role="button"
				aria-label="Move element down"
				className="wopb-toolbar-sort-btn"
				onClick={ () => {
					sortSetValue(
						'down',
						data,
						value,
						( v ) => store.setAttributes( { [ key ]: v } ),
						store
					);
				} }
			>
				<svg
					viewBox="0 0 24 24"
					xmlns="http://www.w3.org/2000/svg"
					aria-hidden="true"
					focusable="false"
				>
					<path d="M17.5 11.6L12 16l-5.5-4.4.9-1.2L12 14l4.5-3.6 1 1.2z" />
				</svg>
			</span>
		</div>
	);
}
