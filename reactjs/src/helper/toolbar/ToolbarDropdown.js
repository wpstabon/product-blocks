const { ToolbarDropdownMenu } = wp.components;

const POPOVER_PROPS = {
	className: 'wopb-toolbar-dropdown',
};

export default function ToolbarDropdown( { options, value, onChange, label } ) {
	return (
		<ToolbarDropdownMenu
			popoverProps={ POPOVER_PROPS }
			icon={
				options.find( ( opt ) => opt.value === value )?.icon || (
					<>value</>
				)
			}
			label={ label }
			controls={ options.map( ( opt ) => {
				return {
					icon: opt.icon,
					title: opt.title,
					isActive: opt.value === value,
					onClick() {
						onChange( opt.value );
					},
					role: 'menuitemradio',
				};
			} ) }
		/>
	);
}
