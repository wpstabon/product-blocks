import { ToolbarSettings } from '../CommonPanel';

const { Dropdown, ToolbarButton } = wp.components;

export default function WopbToolbarDropdown( {
	store,
	content,
	buttonContent,
	label,
} ) {
	return (
		<Dropdown
			contentClassName="wopb-custom-toolbar-wrapper"
			renderToggle={ ( { onToggle } ) => (
				<span>
					<ToolbarButton onClick={ () => onToggle() } label={ label }>
						<span className="wopb-click-toolbar-settings">
							{ buttonContent }
						</span>
					</ToolbarButton>
				</span>
			) }
			renderContent={ () => (
				<ToolbarSettings store={ store } include={ content } />
			) }
		/>
	);
}
