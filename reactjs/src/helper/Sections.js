const { Component, Fragment } = wp.element;

class Sections extends React.Component {
	constructor( props ) {
		super( props );
		this.state = { current: this.props.children[ 0 ].props.slug };
	}

	componentDidMount() {
		jQuery( '.edit-post-sidebar' ).on( 'scroll', function () {
			if ( jQuery( this ).scrollTop() > 130 ) {
				jQuery( '.wopb-section-wrap' ).addClass( 'wopb-section-fixed' );
			} else {
				jQuery( '.wopb-section-wrap' ).removeClass(
					'wopb-section-fixed'
				);
			}
		} );
	}

	render() {
		const section = this.props.children;
		const { current } = this.state;

		return (
			<div className={ `wopb-section-tab` }>
				<div className="wopb-section-wrap">
					<div className="wopb-section-wrap-inner">
						{ section.map( ( tab, k ) => (
							<div
								key={ k }
								className={
									'wopb-section-title' +
									( tab.props.slug === current
										? ' active'
										: '' )
								}
								onClick={ () =>
									this.setState( { current: tab.props.slug } )
								}
							>
								<span
									className={
										tab.props.slug == 'setting'
											? 'dashicons dashicons-admin-tools'
											: tab.props.slug == 'advanced'
											? 'dashicons dashicons-admin-generic'
											: 'dashicons dashicons-admin-appearance'
									}
								></span>
								<span>{ tab.props.title }</span>
							</div>
						) ) }
					</div>
				</div>
				<div className={ `wopb-section-content` }>
					{ section.map( ( tab ) =>
						tab.props.slug === current ? tab : ''
					) }
				</div>
			</div>
		);
	}
}

class Section extends React.Component {
	render() {
		const { children } = this.props;
		return (
			<Fragment>
				{ ' ' }
				{ Array.isArray( children )
					? children.map( ( item ) => item )
					: children }{ ' ' }
			</Fragment>
		);
	}
}

export { Sections, Section };
