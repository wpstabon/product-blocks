/** @format */

export const SETTINGS_SECTION = {
	general: false,
	query_builder: false,
	heading: false,
	title: false,
	category: false,
	image: false,
	filter: false,
	pagination: false,
	'read-more': false,
	price: false,
	review: false,
	desc: false,
	sales: false,
	hots: false,
	deal: false,
	cart: false,
	swatch: false,
	arrow: false,
	dot: false,
	qv: false,
	count: false,
};

export function scrollSidebarSettings( id ) {
	function scroll( id ) {
		const _id = 'wopb-sidebar-' + id;
		const section = document.getElementById( _id );
		if ( section ) {
			section.classList.add( 'wopb-sidebar-highlight' );
			section.style.scrollMarginTop = '51px';
			section.scrollIntoView( {
				behavior: 'smooth',
			} );
			setTimeout( function () {
				section.classList.remove( 'wopb-sidebar-highlight' );
			}, 2000 );
		} else {
			throw new Error( 'Could not find settings section: ' + _id );
		}
	}

	id = id.replace( / /g, '-' ).toLowerCase();
	setTimeout( () => {
		try {
			scroll( id );
		} catch {
			setTimeout( () => scroll( id ), 500 ); // for slow devices
		}
	}, 500 );
}

/**
 *
 * @param {string} title
 * @return string
 */
export function getId( title ) {
	switch ( title ) {
		case 'General + Query':
			return 'general';
		case 'Short Description':
			return 'desc';
		case 'Variation Swatches':
			return 'swatch';
		case 'Large Image Arrow':
			return 'arrow';
		case 'Quick View Style':
			return 'qv';
		case 'Wishlist Style':
			return 'wish';
		case 'Wishlist, Compare, Quickview, Cart':
			return 'overlay_meta';
		case 'Ship to Different address checkbox':
			return 'ship-add';
		case 'Checkbox & Label':
			return 'check-label';
		default:
			return title?.replace( / /g, '-' ).toLowerCase();
	}
}

/**
 *
 * @param {Object} content
 * @return object
 */
export function formatSettingsForToolbar( content ) {
	return [
		{
			isToolbar: true,
			data: {
				type: 'tab_toolbar',
				content,
			},
		},
	];
}

export function getData( data ) {
	return data.map( ( item ) => item.data );
}

export function saveSelectedSection( title ) {
	localStorage.setItem( 'wopb_selected_settings_section', title );
}

export function saveToolbar( title ) {
	localStorage.setItem( 'wopb_selected_toolbar', title );
}

export function resetState() {
	if ( localStorage.getItem( 'wopb_settings_reset_disabled' ) !== 'true' ) {
		localStorage.removeItem( 'wopb_selected_settings_section' );
		localStorage.removeItem( 'wopb_selected_toolbar' );
		localStorage.removeItem( 'wopb_settings_save_state' );
		localStorage.removeItem( 'wopb_prev_sel_block' );
		localStorage.removeItem( 'wopb_settings_active_tab' );
	}
}

export function saveTab( title, tab ) {
	localStorage.setItem( 'wopb_settings_active_tab', title + '###' + tab );
}

export function restoreTab( title ) {
	if ( localStorage.getItem( 'wopb_settings_save_state' ) === 'true' ) {
		const tabData = localStorage.getItem( 'wopb_settings_active_tab' );
		if ( tabData ) {
			const [ savedTitle, tab ] = tabData.split( '###' );
			return savedTitle === title ? tab : undefined;
		}
	}

	return undefined;
}

export function restoreState( sectionCb, toolbarCb ) {
	const shouldRestore =
		localStorage.getItem( 'wopb_settings_save_state' ) === 'true';
	if ( shouldRestore ) {
		const savedSection = localStorage.getItem(
			'wopb_selected_settings_section'
		);
		if ( savedSection ) {
			sectionCb( savedSection );
		}

		const savedToolbar = localStorage.getItem( 'wopb_selected_toolbar' );
		if ( savedToolbar ) {
			toolbarCb( savedToolbar );
		}

		resetState();
	}
}
