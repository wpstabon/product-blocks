/** @format */

const { __ } = wp.i18n;
const { useState } = wp.element;
const { BlockControls } = wp.blockEditor;
const { Modal, Toolbar, Dropdown, ToolbarButton } = wp.components;
import icons from './icons';
import IconPack from './fields/tools/IconPack';
import { FieldGenerator } from './FieldGenerator';
import {
	QuerySettings,
	Layout,
	CommonToolBar,
	spacingIcon,
	filterFields,
	SpacingArg,
} from '../helper/CommonPanel';
import DesignTemplate from './common/DesignTemplate';
import { formatSettingsForToolbar } from './ux';
import useDevice from './hooks/use-device';
// import DesignTemplate from './DesignTemplate';

const ToolBarElement = ( props ) => {
	const { include, store /*, store:{setAttributes}, */ } = props;
	const [ isOpen, setOpen ] = useState( false );
	const TemplateModal = () => setOpen( true );
	const closeModal = () => setOpen( false );
	const [ device, setDevice ] = useDevice();

	return (
		<BlockControls>
			{ include.map( ( e, k ) => {
				if ( e.type == 'alignment' ) {
					const _val = e.value
						? e.responsive
							? device
							: e.value
						: 'left';
					return (
						<Toolbar key={ k }>
							<Dropdown
								key={ k }
								renderToggle={ ( { onToggle } ) => (
									<ToolbarButton
										className={ 'wopb-toolbar-align' }
										onClick={ () => onToggle() }
										label={ e.label || 'Alignment' }
									>
										{ icons[ _val ] }
									</ToolbarButton>
								) }
								renderContent={ () => (
									<FieldGenerator
										store={ store }
										data={ [
											{
												...e,
												label: null,
											},
										] }
									/>
								) }
							/>
						</Toolbar>
					);
				} else if ( e.type == 'template' ) {
					return (
						<Toolbar key={ k }>
							<ToolbarButton
								className="wopb-toolbar-template__btn"
								onClick={ TemplateModal }
								label={ 'Design Library' }
							>
								<span className="dashicons dashicons-images-alt2"></span>
							</ToolbarButton>
							{ isOpen && (
								<Modal
									isFullScreen
									onRequestClose={ closeModal }
								>
									<DesignTemplate
										store={ store }
										closeModal={ closeModal }
									/>
								</Modal>
							) }
						</Toolbar>
					);
				} else if ( e.type == 'layout' || e.type === 'layout2' ) {
					return (
						<Toolbar key={ k }>
							<Dropdown
								key={ k }
								contentClassName="wopb-layout-toolbar"
								renderToggle={ ( { onToggle } ) => (
									<ToolbarButton
										label={ 'Layout' }
										onClick={ () => onToggle() }
									>
										<span className="dashicons dashicons-schedule" />
									</ToolbarButton>
								) }
								renderContent={ () => (
									<div className={ `wopb-field-wrap` }>
										<Layout
											title={ 'inline' }
											store={ store }
											block={ e.block }
											options={ e.options }
											col={ e.col }
											data={
												e.type === 'layout'
													? e
													: {
															...e,
															type: 'layout',
													  }
											}
										/>
									</div>
								) }
							/>
						</Toolbar>
					);
				} else if ( e.type == 'query' ) {
					return (
						<Toolbar key={ k }>
							<Dropdown
								key={ k }
								contentClassName="wopb-query-toolbar"
								renderToggle={ ( { onToggle } ) => (
									<span
										className={ `wopb-query-toolbar__btn` }
									>
										<ToolbarButton
											label={ __(
												'Product Sort',
												'product-blocks'
											) }
											icon={ icons.loop }
											onClick={ () => onToggle() }
										/>
									</span>
								) }
								renderContent={ () => (
									<div className={ `wopb-field-wrap` }>
										<QuerySettings
											exclude={ e.exclude }
											include={ e.include }
											title={ 'inline' }
											blockType={ e.blockType || '' }
											store={ store }
										/>
									</div>
								) }
							/>
						</Toolbar>
					);
				} else if ( e.type == 'link' ) {
					return (
						<Toolbar key={ k }>
							<Dropdown
								key={ k }
								contentClassName="wopb-common-toolbar"
								renderToggle={ ( { onToggle } ) => (
									<ToolbarButton
										name="link"
										label={ 'Link' }
										icon={ IconPack.link }
										onClick={ () => onToggle() }
									></ToolbarButton>
								) }
								renderContent={ () => (
									<CommonToolBar
										title={ 'inline' }
										store={ store }
										block={ e.block }
										options={ e.options }
										col={ e.col }
										data={ e }
									/>
								) }
							/>
						</Toolbar>
					);
				} else if ( e.type === 'grid_spacing' ) {
					return (
						<Toolbar key={ k }>
							<Dropdown
								key={ k }
								contentClassName="wopb-query-toolbar"
								renderToggle={ ( { onToggle } ) => (
									<ToolbarButton
										label={
											e.label ||
											__(
												'Grid Spacing',
												'product-blocks'
											)
										}
										icon={ spacingIcon }
										onClick={ () => onToggle() }
									></ToolbarButton>
								) }
								renderContent={ () => (
									<FieldGenerator
										store={ store }
										isToolbar={ true }
										title="inline"
										data={ filterFields(
											e.include,
											e.exclude,
											SpacingArg
										) }
									/>
								) }
							/>
						</Toolbar>
					);
				} else if ( e.type === 'block_spacing' ) {
					return (
						<Toolbar key={ k }>
							<Dropdown
								key={ k }
								contentClassName="wopb-query-toolbar"
								renderToggle={ ( { onToggle } ) => (
									<ToolbarButton
										label={
											e.label ||
											__( 'Spacing', 'product-blocks' )
										}
										icon={ spacingIcon }
										onClick={ () => onToggle() }
									></ToolbarButton>
								) }
								renderContent={ () => (
									<FieldGenerator
										store={ store }
										isToolbar={ true }
										title="inline"
										data={ filterFields(
											[
												{
													data: {
														type: 'range',
														key: 'wrapMargin',
														min: 0,
														max: 100,
														step: 1,
														unit: true,
														responsive: true,
														dimensionKey: 'top',
														label: __(
															'Margin Top',
															'product-blocks'
														),
													},
												},
												{
													data: {
														type: 'range',
														key: 'wrapMargin',
														min: 0,
														max: 100,
														step: 1,
														unit: true,
														responsive: true,
														dimensionKey: 'bottom',
														label: __(
															'Margin Bottom',
															'product-blocks'
														),
													},
												},
											],
											'__all',
											SpacingArg
										) }
									/>
								) }
							/>
						</Toolbar>
					);
				} else if ( e.type === 'common' ) {
					return (
						<Toolbar key={ k }>
							<Dropdown
								key={ k }
								contentClassName="wopb-query-toolbar"
								renderToggle={ ( { onToggle } ) => (
									<ToolbarButton
										label={
											e.label ||
											__( 'Features', 'product-blocks' )
										}
										icon={ icons.setting || e.icon }
										onClick={ () => onToggle() }
									></ToolbarButton>
								) }
								renderContent={ () => (
									<FieldGenerator
										store={ store }
										isToolbar={ true }
										title="inline"
										data={ e.data }
									/>
								) }
							/>
						</Toolbar>
					);
				}
			} ) }
		</BlockControls>
	);
};

export default ToolBarElement;
