import DesignTemplate from './common/DesignTemplate';
const { __ } = wp.i18n;

const { Modal } = wp.components;
const { useState } = wp.element;
const TemplateModal = ( { store, prev, doc = undefined } ) => {
	const [ isOpen, setOpen ] = useState( false );
	const TemplateModal = () => setOpen( true );
	const closeModal = () => setOpen( false );

	return (
		<>
			<div className="wopb-ready-design">
				<div>
					<button
						className="wopb-ready-design-btns patterns"
						onClick={ TemplateModal }
					>
						{ ' ' }
						{ __( 'Design Library', 'product-blocks' ) }{ ' ' }
					</button>
					<a
						className="wopb-ready-design-btns preview"
						target="_blank"
						href={ prev }
						rel="noreferrer"
					>
						{ __( 'Blocks Preview', 'product-blocks' ) }
					</a>
				</div>
			</div>
			{ isOpen && (
				<Modal isFullScreen onRequestClose={ closeModal }>
					<DesignTemplate store={ store } closeModal={ closeModal } />
				</Modal>
			) }
		</>
	);
};

export default TemplateModal;
