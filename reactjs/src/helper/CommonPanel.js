const { __ } = wp.i18n;
const { addQueryArgs } = wp.url;
const { dispatch } = wp.data;
import { FieldGenerator } from './FieldGenerator';
import icons from '../helper/icons';
import DesignTemplate from './common/DesignTemplate';
import useDevice from './hooks/use-device';
const { Modal, Toolbar, Dropdown, ToolbarButton } = wp.components;

const fetchImageSize = [];
const cat = [];
const tag = [];
const postTypes = [];
const stockStatus = [];

export const typoIcon = (
	<img src={ wopb_data.url + 'assets/img/toolbar/typography.svg' } />
);
export const colorIcon = (
	<img src={ wopb_data.url + 'assets/img/toolbar/color.svg' } />
);
export const spacingIcon = (
	<img src={ wopb_data.url + 'assets/img/toolbar/spacing.svg' } />
);
export const settingsIcon = (
	<img src={ wopb_data.url + 'assets/img/toolbar/setting.svg' } />
);
export const styleIcon = (
	<img src={ wopb_data.url + 'assets/img/toolbar/style.svg' } />
);
export const metaTextIcon = (
	<img src={ wopb_data.url + 'assets/img/toolbar/meta-text.svg' } />
);

const fetchTaxonomy = () => {
	const query = addQueryArgs( '/wopb/common', {
		wpnonce: wopb_data.security,
	} );
	wp.apiFetch( { path: query } ).then( ( obj ) => {
		// Category
		if ( obj.cat ) {
			Object.keys( obj.cat ).forEach( function ( key ) {
				cat.push( { value: key, label: obj.cat[ key ] } );
			} );
		}
		// Tag List
		if ( obj.tag ) {
			Object.keys( obj.tag ).forEach( function ( key ) {
				tag.push( { value: key, label: obj.tag[ key ] } );
			} );
		}
		// Image Size
		if ( obj.image ) {
			Object.keys( obj.image ).forEach( function ( key ) {
				fetchImageSize.push( { value: key, label: obj.image[ key ] } );
			} );
		}

		const objPost = JSON.parse( obj.post_type );
		Object.keys( objPost ).forEach( function ( key ) {
			postTypes.push( { value: key, label: objPost[ key ] } );
		} );

		if ( obj.stock_status.length > 0 ) {
			obj.stock_status.forEach( function ( v ) {
				stockStatus.push( v );
			} );
		}

		localStorage.setItem( 'wopbDevice', 'lg' );
		localStorage.setItem( 'wopbGlobal', JSON.stringify( obj.global ) );
		localStorage.setItem(
			'wopbPostTypeTaxonomy',
			JSON.stringify( obj.post_type_taxonomy )
		);
	} );
};
fetchTaxonomy();

export const filterFields = ( include, exclude, args ) => {
	let data = args.slice( 0 );
	if ( exclude ) {
		if ( exclude === '__all' ) {
			data = [];
		} else {
			exclude.forEach( ( val ) => {
				// data = data.filter( el => el.key != val );
				data = data.filter( ( el ) => {
					if ( el.type == 'tab' ) {
						el.content.forEach( ( content ) => {
							content.options = content.options.filter(
								( option ) => option.key != val
							);
							return content;
						} );
						return el;
					}
					return el.key != val;
				} );
			} );
		}
	}
	if ( include ) {
		include.forEach( ( val ) => {
			if ( typeof val === 'string' ) {
				const newData = args.find( ( el ) => el.key === val );
				if ( newData ) {
					data.push( newData );
				}
			} else if ( val.data ) {
				if ( data[ val.position ] ) {
					if ( data[ val.position ].key != val.data.key ) {
						data.splice( val.position, 0, val.data );
					}
				} else {
					data.push( val.data );
				}
			}
		} );
	}
	return data;
};

const proLink =
	'https://www.wpxpo.com/wowstore/pricing/?utm_source=db-wstore-editor&utm_medium=option-pro-features&utm_campaign=wstore-dashboard';
// Query - Content
let productSortLabel = '- Select Product Sort -';
if ( wopb_data.isBuilder && wopb_data.builder_type == 'archive' ) {
	productSortLabel = 'Archive Builder';
}
const queryProductSortOptions = [
	{ value: 'null', label: __( productSortLabel, 'product-blocks' ) },
	{
		value: 'choose_specific',
		label: __( 'Choose Specific Products', 'product-blocks' ),
	},
];
if ( wopb_data.isBuilder && wopb_data.builder_type == 'cart' ) {
	queryProductSortOptions.push( {
		value: 'crosssell',
		label: __( 'Cross-sell Products', 'product-blocks' ),
	} );
} else if (
	wopb_data.isBuilder &&
	wopb_data.builder_type == 'single_product'
) {
	queryProductSortOptions.push( {
		value: 'upsells',
		label: __( 'Up-sell Products', 'product-blocks' ),
	} );
} else if (
	! wopb_data.isBuilder ||
	( wopb_data.isBuilder && wopb_data.builder_type != 'archive' )
) {
	queryProductSortOptions.push( {
		value: 'recent_view',
		label: __( 'Recent View Products', 'product-blocks' ),
	} );
}

/**
 * @type array
 */
export const productSort = [
	{
		type: 'select',
		key: 'queryProductSort',
		label: __( 'Product Sort', 'product-blocks' ),
		options: queryProductSortOptions,
	},
	{
		type: 'search',
		key: 'querySpecificProduct',
		search: 'product',
		label: __( 'Choose Specific Products', 'product-blocks' ),
		pro: true,
	},
];
const avdSortProLink =
	'https://www.wpxpo.com/wowstore/pricing/?utm_source=db-wstore-editor&utm_medium=advanced-sort&utm_campaign=wstore-dashboard';
if (
	! wopb_data.isBuilder ||
	( wopb_data.isBuilder &&
		wopb_data.builder_type != 'cart' &&
		wopb_data.builder_type != 'archive' )
) {
	productSort.push( {
		type: 'select',
		key: 'queryAdvanceProductSort',
		label: __( 'Advanced Sort', 'product-blocks' ),
		options: [
			{
				value: 'null',
				label: __( '- Select Advance Sort -', 'product-blocks' ),
			},
			{
				value: 'random_post_7_days',
				label: __( 'Random Product (7 Days)', 'product-blocks' ),
				pro: false,
			},
			{
				value: 'random_post_30_days',
				label: __( 'Random Product (30 Days)', 'product-blocks' ),
				pro: false,
			},
			{
				value: 'random_post_90_days',
				label: __( 'Random Product (90 Days)', 'product-blocks' ),
				pro: true,
				link: avdSortProLink,
			},
			{
				value: 'random_post',
				label: __( 'Random Product(All Time)', 'product-blocks' ),
				pro: false,
			},
			{
				value: 'sales_day_1',
				label: __( 'Most Sales (Last 24hr)', 'product-blocks' ),
				pro: false,
			},
			{
				value: 'sales_day_7',
				label: __( 'Most Sales (Last 7 Days)', 'product-blocks' ),
				pro: false,
			},
			{
				value: 'sales_day_30',
				label: __( 'Most Sales (Last 1 Month)', 'product-blocks' ),
				pro: false,
			},
			{
				value: 'sales_day_90',
				label: __( 'Most Sales (Last 3 Month)', 'product-blocks' ),
				pro: false,
			},
			{
				value: 'sales_day_all',
				label: __( 'Most Sales (All Time)', 'product-blocks' ),
				pro: false,
			},
			{
				value: 'related_tag',
				label: __( 'Related Product (Tag)', 'product-blocks' ),
				pro: false,
			},
			{
				value: 'related_category',
				label: __( 'Related Product (Category)', 'product-blocks' ),
				pro: false,
			},
			{
				value: 'related_cat_tag',
				label: __(
					'Related Product (Category & Tag)',
					'product-blocks'
				),
				pro: false,
			},
			{
				value: 'top_rated',
				label: __( 'Top Rated Product (Average)', 'product-blocks' ),
				pro: false,
			},
			{
				value: 'most_rated',
				label: __(
					'Most Rated Product (Review Count)',
					'product-blocks'
				),
				pro: false,
			},
			{
				value: 'view_day_1',
				label: __( 'Most View Products(Last 24hr)', 'product-blocks' ),
				pro: true,
				link: avdSortProLink,
			},
			{
				value: 'view_day_7',
				label: __(
					'Most View Products(Last 7 Days)',
					'product-blocks'
				),
				pro: true,
				link: avdSortProLink,
			},
			{
				value: 'view_day_30',
				label: __(
					'Most View Products(Last 1 Month)',
					'product-blocks'
				),
				pro: true,
				link: avdSortProLink,
			},
			{
				value: 'view_day_90',
				label: __(
					'Most View Products(Last 3 Month)',
					'product-blocks'
				),
				pro: true,
				link: avdSortProLink,
			},
			{
				value: 'view_day_all',
				label: __( 'Most View Products(All Time)', 'product-blocks' ),
			},
		],
	} );
}
let queryStatus = '';
if (
	! wopb_data.isBuilder ||
	( wopb_data.isBuilder && wopb_data.builder_type == 'home_page' )
) {
	queryStatus = [
		{
			type: 'tag',
			key: 'queryStatus',
			label: __( 'Product Status', 'product-blocks' ),
			options: [
				{ value: 'all', label: __( 'All', 'product-blocks' ) },
				{
					value: 'featured',
					label: __( 'Featured', 'product-blocks' ),
				},
				{ value: 'onsale', label: __( 'On Sale', 'product-blocks' ) },
			],
		},
	];
}
productSort.push(
	{
		type: 'range',
		key: 'queryNumber',
		min: -1,
		max: 200,
		label: __( 'Number of Products', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'queryOffset',
		min: 0,
		max: 50,
		step: 1,
		label: __( 'Offset Product', 'product-blocks' ),
	},
	...queryStatus,
	{
		type: 'select',
		key: 'queryOrderBy',
		label: __( 'Order By', 'product-blocks' ),
		options: [
			{
				value: 'menu_order',
				label: __( 'Menu Order', 'product-blocks' ),
			},
			{ value: 'date', label: __( 'Date', 'product-blocks' ) },
			{ value: 'title', label: __( 'Title', 'product-blocks' ) },
			{ value: 'price', label: __( 'Price', 'product-blocks' ) },
			{ value: 'sku', label: __( 'SKU', 'product-blocks' ) },
			{ value: 'rand', label: 'Random' },
			{ value: 'modified', label: 'Last Modified Date' },
			{
				value: 'popular',
				label: __( 'Popular By Sales', 'product-blocks' ),
				pro: true,
				link: proLink,
			},
			{
				value: 'popular_view',
				label: __( 'Popular By View', 'product-blocks' ),
				pro: true,
				link: proLink,
			},
			{
				value: 'top_rated',
				label: __( 'Top Rated', 'product-blocks' ),
				pro: true,
				link: proLink,
			},
			{
				value: 'stock_status',
				label: __( 'Stock Status', 'product-blocks' ),
			},
		],
	},
	{
		type: 'tag',
		key: 'queryOrder',
		label: __( 'Order (ASC/DESC)', 'product-blocks' ),
		options: [
			{ value: 'asc', label: __( 'Ascending', 'product-blocks' ) },
			{ value: 'desc', label: __( 'Descending', 'product-blocks' ) },
		],
	},
	{
		type: 'select',
		key: 'queryStockStatus',
		label: __( 'Stock Status', 'product-blocks' ),
		multiple: true,
		options: stockStatus,
		pro: true,
		link: proLink,
	}
);
if (
	! wopb_data.isBuilder ||
	( wopb_data.isBuilder &&
		( wopb_data.builder_type === 'home_page' ||
			wopb_data.builder_type === 'shop' ||
			wopb_data.builder_type === 'single_product' ) )
) {
	productSort.push(
		{ type: 'separator' },
		{
			type: 'select',
			key: 'queryTax',
			label: __( 'Taxonomy', 'product-blocks' ),
			options: [],
		},
		{
			type: 'search',
			key: 'queryTaxValue',
			label: __( 'Taxonomy Value', 'product-blocks' ),
			multiple: true,
			search: 'taxValue',
		},
		{
			type: 'tag',
			key: 'queryRelation',
			label: __( 'Taxonomy Relation', 'product-blocks' ),
			options: [
				{ value: 'OR', label: __( 'OR', 'product-blocks' ) },
				{ value: 'AND', label: __( 'AND', 'product-blocks' ) },
			],
		},

		{ type: 'separator' },

		{
			type: 'search',
			key: 'queryIncludeAuthor',
			search: 'author',
			label: __( 'Include Author', 'product-blocks' ),
			pro: true,
			link: proLink,
		},
		{
			type: 'search',
			key: 'queryExcludeAuthor',
			search: 'author',
			label: __( 'Exclude Author', 'product-blocks' ),
			pro: true,
		},
		{
			type: 'search',
			key: 'queryExclude',
			search: 'productExclude',
			label: __( 'Exclude Product', 'product-blocks' ),
			placeholder: 'Separate ID by Comma',
		}
	);
}
const QuerySettingsArg = ( store ) => [ ...productSort ];

// Category Query - Content
const QueryCatArchiveArg = [
	{
		type: 'select',
		key: 'queryType',
		label: __( 'Query Type', 'product-blocks' ),
		options: [
			{ value: 'regular', label: __( 'Regular', 'product-blocks' ) },
			{ value: 'child', label: __( 'Child Of', 'product-blocks' ) },
			{ value: 'parent', label: __( 'Parent', 'product-blocks' ) },
			{ value: 'custom', label: __( 'Custom', 'product-blocks' ) },
		],
	},
	{
		type: 'search',
		key: 'queryCat',
		search: 'product_cat',
		label: __( 'Category', 'product-blocks' ),
		multiple: true,
		options: cat,
	},
	{
		type: 'range',
		key: 'queryNumber',
		min: -1,
		max: 200,
		label: __( 'Number of Categories', 'product-blocks' ),
	},
];

// Category - Style
export const CategoryStyleArg = [
	{
		type: 'toggle',
		key: 'enableCatLink',
		label: __( 'Enable Link', 'product-blocks' ),
	},
	{
		type: 'select',
		key: 'catPosition',
		label: __( 'Position', 'product-blocks' ),
		options: [
			{ value: 'none', label: __( 'None', 'product-blocks' ) },
			{
				value: 'topLeft',
				label: __( 'Over Image(Top Left)', 'product-blocks' ),
			},
			{
				value: 'topRight',
				label: __( 'Over Image(Top Right)', 'product-blocks' ),
			},
			{
				value: 'bottomLeft',
				label: __( 'Over Image(Bottom Left)', 'product-blocks' ),
			},
			{
				value: 'bottomRight',
				label: __( 'Over Image(Bottom Right)', 'product-blocks' ),
			},
			{
				value: 'centerCenter',
				label: __( 'Over Image(Center)', 'product-blocks' ),
			},
		],
	},
	{
		type: 'typography',
		key: 'catTypo',
		label: __( 'Typography', 'product-blocks' ),
	},
	{
		type: 'tab',
		key: 'catTab',
		content: [
			{
				name: 'normal',
				title: __( 'Normal', 'product-blocks' ),
				options: [
					{
						type: 'color',
						key: 'catColor',
						label: __( 'Color', 'product-blocks' ),
					},
					{
						type: 'color2',
						key: 'catBgColor',
						label: __( 'Background Color', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'catBorder',
						label: __( 'Border', 'product-blocks' ),
					},
				],
			},
			{
				name: 'hover',
				title: __( 'Hover', 'product-blocks' ),
				options: [
					{
						type: 'color',
						key: 'catHoverColor',
						label: __( 'Hover Color', 'product-blocks' ),
					},
					{
						type: 'color2',
						key: 'catBgHoverColor',
						label: __( 'Hover Bg Color', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'catHoverBorder',
						label: __( 'Hover Border', 'product-blocks' ),
					},
				],
			},
		],
	},
	{
		type: 'dimension',
		key: 'catRadius',
		label: __( 'Border Radius', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'catSacing',
		label: __( 'Spacing', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'catPadding',
		label: __( 'Padding', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
];

// General - Settings
const GeneralSettingsArg = [
	{
		type: 'group',
		key: 'productView',
		label: __( 'Product View', 'product-blocks' ),
		justify: true,
		options: [
			{ value: 'grid', label: __( 'Grid View', 'product-blocks' ) },
			{ value: 'slide', label: __( 'Slide View', 'product-blocks' ) },
		],
	},
	{
		type: 'range',
		key: 'columns',
		min: 1,
		max: 12,
		step: 1,
		responsive: true,
		label: __( 'Columns', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'columnGridGap',
		min: 0,
		max: 80,
		step: 1,
		responsive: true,
		unit: [ 'px', 'em' ],
		label: __( 'Column Grid Gap', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'rowGap',
		min: 0,
		max: 100,
		step: 1,
		responsive: true,
		unit: [ 'px', 'em' ],
		label: __( 'Row Gap', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'columnGap',
		min: 0,
		max: 100,
		step: 1,
		responsive: true,
		unit: [ 'px', 'em' ],
		label: __( 'Column Gap', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'slidesToShow',
		min: 1,
		max: 12,
		step: 1,
		responsive: true,
		label: __( 'Slides To Show', 'product-blocks' ),
	},
	{
		type: 'toggle',
		key: 'autoPlay',
		label: __( 'Auto Play', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'slideSpeed',
		min: 1,
		max: 10000,
		step: 1,
		label: __( 'Slide Speed', 'product-blocks' ),
	},
	{
		type: 'toggle',
		key: 'quickView',
		label: __( 'Show Quick View', 'product-blocks' ),
	},
	{
		type: 'toggle',
		key: 'showCompare',
		label: __( 'Show Compare', 'product-blocks' ),
	},
	{
		type: 'toggle',
		key: 'showOutStock',
		label: __( 'Show Out of Stock', 'product-blocks' ),
	},
	{
		type: 'toggle',
		key: 'showInStock',
		label: __( 'Show In Stock', 'product-blocks' ),
	},
	{
		type: 'toggle',
		key: 'disableFlip',
		label: __( 'Disable Image Flip', 'product-blocks' ),
	},
	{
		type: 'toggle',
		key: 'showVideo',
		label: __( 'Show Video on Image', 'product-blocks' ),
	},
	{
		type: 'alignment',
		key: 'contentAlign',
		responsive: false,
		label: __( 'Alignment', 'product-blocks' ),
		disableJustify: true,
	},
	{
		type: 'sort',
		key: 'sortSection',
		label: __( 'Sort Element', 'product-blocks' ),
	},
];

const GeneralSettingsArgWithQuery = [
	{
		type: 'group',
		key: 'productView',
		label: __( 'Product View', 'product-blocks' ),
		justify: true,
		options: [
			{ value: 'grid', label: __( 'Grid View', 'product-blocks' ) },
			{ value: 'slide', label: __( 'Slide View', 'product-blocks' ) },
		],
	},

	{ type: 'separator' },

	{
		type: 'range',
		key: 'columns',
		min: 1,
		max: 12,
		step: 1,
		responsive: true,
		label: __( 'Columns', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'columnGridGap',
		min: 0,
		max: 80,
		step: 1,
		responsive: true,
		unit: [ 'px', 'em' ],
		label: __( 'Column Grid Gap', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'rowGap',
		min: 0,
		max: 100,
		step: 1,
		responsive: true,
		unit: [ 'px', 'em' ],
		label: __( 'Row Gap', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'columnGap',
		min: 0,
		max: 100,
		step: 1,
		responsive: true,
		unit: [ 'px', 'em' ],
		label: __( 'Column Gap', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'slidesToShow',
		min: 1,
		max: 12,
		step: 1,
		responsive: true,
		label: __( 'Slides To Show', 'product-blocks' ),
	},
	{
		type: 'toggle',
		key: 'autoPlay',
		label: __( 'Auto Play', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'slideSpeed',
		min: 1,
		max: 10000,
		step: 1,
		label: __( 'Slide Speed', 'product-blocks' ),
	},

	// {type: "separator"},

	// { type:'sort',key:'sortSection',label:__('Sort Element', 'product-blocks') },

	{
		type: 'separator',
		fancy: true,
		label: __( 'Query Builder', 'product-blocks' ),
	},

	...productSort,

	{ type: 'separator' },

	{
		type: 'toggle',
		key: 'quickView',
		label: __( 'Show Quick View', 'product-blocks' ),
	},
	{
		type: 'toggle',
		key: 'showCompare',
		label: __( 'Show Compare', 'product-blocks' ),
	},
	{
		type: 'toggle',
		key: 'showOutStock',
		label: __( 'Show Out of Stock', 'product-blocks' ),
	},
	{
		type: 'toggle',
		key: 'showInStock',
		label: __( 'Show In Stock', 'product-blocks' ),
	},
	{
		type: 'toggle',
		key: 'disableFlip',
		label: __( 'Disable Image Flip', 'product-blocks' ),
	},
	{
		type: 'alignment',
		key: 'contentAlign',
		responsive: false,
		label: __( 'Alignment', 'product-blocks' ),
		disableJustify: true,
	},
];

const GeneralSettingsArgWithQueryCat = [
	{
		type: 'group',
		key: 'productView',
		label: __( 'Product View', 'product-blocks' ),
		justify: true,
		options: [
			{ value: 'grid', label: __( 'Grid View', 'product-blocks' ) },
			{ value: 'slide', label: __( 'Slide View', 'product-blocks' ) },
		],
	},

	{ type: 'separator' },

	{
		type: 'range',
		key: 'columns',
		min: 1,
		max: 12,
		step: 1,
		responsive: true,
		label: __( 'Columns', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'columnGridGap',
		min: 0,
		max: 80,
		step: 1,
		responsive: true,
		unit: [ 'px', 'em' ],
		label: __( 'Column Grid Gap', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'rowGap',
		min: 0,
		max: 100,
		step: 1,
		responsive: true,
		unit: [ 'px', 'em' ],
		label: __( 'Row Gap', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'columnGap',
		min: 0,
		max: 100,
		step: 1,
		responsive: true,
		unit: [ 'px', 'em' ],
		label: __( 'Column Gap', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'slidesToShow',
		min: 1,
		max: 12,
		step: 1,
		responsive: true,
		label: __( 'Slides To Show', 'product-blocks' ),
	},
	{
		type: 'toggle',
		key: 'autoPlay',
		label: __( 'Auto Play', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'slideSpeed',
		min: 1,
		max: 10000,
		step: 1,
		label: __( 'Slide Speed', 'product-blocks' ),
	},

	// {type: "separator"},

	// { type:'sort',key:'sortSection',label:__('Sort Element', 'product-blocks') },

	{ type: 'separator' },

	...QueryCatArchiveArg,

	{ type: 'separator' },

	{
		type: 'toggle',
		key: 'quickView',
		label: __( 'Show Quick View', 'product-blocks' ),
	},
	{
		type: 'toggle',
		key: 'showCompare',
		label: __( 'Show Compare', 'product-blocks' ),
	},
	{
		type: 'toggle',
		key: 'showOutStock',
		label: __( 'Show Out of Stock', 'product-blocks' ),
	},
	{
		type: 'toggle',
		key: 'showInStock',
		label: __( 'Show In Stock', 'product-blocks' ),
	},
	{
		type: 'toggle',
		key: 'disableFlip',
		label: __( 'Disable Image Flip', 'product-blocks' ),
	},
	{
		type: 'alignment',
		key: 'contentAlign',
		responsive: false,
		label: __( 'Alignment', 'product-blocks' ),
		disableJustify: true,
	},
];

// Cart - Style
export const CartStyleArg = [
	{
		type: 'toggle',
		key: 'cartNoFollow',
		label: __( 'Link No Follow', 'product-blocks' ),
	},
	{
		type: 'text',
		key: 'cartText',
		label: __( 'Cart Text', 'product-blocks' ),
	},
	{
		type: 'text',
		key: 'cartActive',
		label: __( 'Cart Active Text', 'product-blocks' ),
	},
	{
		type: 'typography',
		key: 'cartTypo',
		label: __( 'Typography', 'product-blocks' ),
	},
	{
		type: 'tab',
		key: 'cartTab',
		content: [
			{
				name: 'normal',
				title: __( 'Normal', 'product-blocks' ),
				options: [
					{
						type: 'color',
						key: 'cartColor',
						label: __( 'Color', 'product-blocks' ),
					},
					{
						type: 'color2',
						key: 'cartBgColor',
						label: __( 'Background Color', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'cartBorder',
						label: __( 'Border', 'product-blocks' ),
					},
					{
						type: 'dimension',
						key: 'cartRadius',
						label: __( 'Border Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
				],
			},
			{
				name: 'hover',
				title: __( 'Hover', 'product-blocks' ),
				options: [
					{
						type: 'color',
						key: 'cartHoverColor',
						label: __( 'Hover Color', 'product-blocks' ),
					},
					{
						type: 'color2',
						key: 'cartBgHoverColor',
						label: __( 'Hover Bg Color', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'cartHoverBorder',
						label: __( 'Hover Border', 'product-blocks' ),
					},
					{
						type: 'dimension',
						key: 'cartHoverRadius',
						label: __( 'Hover Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
				],
			},
		],
	},
	{
		type: 'dimension',
		key: 'cartSpacing',
		label: __( 'Spacing', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'cartPadding',
		label: __( 'Padding', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
];

// Arrow - Style
export const ArrowStyleArg = [
	{
		type: 'select',
		key: 'arrowStyle',
		label: __( 'Arrow Style', 'product-blocks' ),
		options: [
			{
				value: 'leftAngle#rightAngle',
				label: __( 'Style 1', 'product-blocks' ),
				icon: 'leftAngle',
			},
			{
				value: 'leftAngle2#rightAngle2',
				label: __( 'Style 2', 'product-blocks' ),
				icon: 'leftAngle2',
			},
			{
				value: 'leftArrowLg#rightArrowLg',
				label: __( 'Style 3', 'product-blocks' ),
				icon: 'leftArrowLg',
			},
		],
	},
	{
		type: 'range',
		key: 'arrowSize',
		min: 0,
		max: 80,
		step: 1,
		responsive: true,
		unit: true,
		label: __( 'Size', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'arrowWidth',
		min: 0,
		max: 80,
		step: 1,
		responsive: true,
		unit: true,
		label: __( 'Width', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'arrowHeight',
		min: 0,
		max: 80,
		step: 1,
		responsive: true,
		unit: true,
		label: __( 'Height', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'arrowVartical',
		min: -200,
		max: 500,
		step: 1,
		responsive: true,
		unit: true,
		label: __( 'Vertical Position', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'arrowHorizontal',
		min: -500,
		max: 500,
		step: 1,
		responsive: true,
		unit: true,
		label: __( 'Horizontal Position', 'product-blocks' ),
	},
	{
		type: 'tab',
		key: 'aTab',
		content: [
			{
				name: 'normal',
				title: __( 'Normal', 'product-blocks' ),
				options: [
					{
						type: 'color',
						key: 'arrowColor',
						label: __( 'Color', 'product-blocks' ),
					},
					{
						type: 'color',
						key: 'arrowBg',
						label: __( 'Background Color', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'arrowBorder',
						label: __( 'Border', 'product-blocks' ),
					},
					{
						type: 'dimension',
						key: 'arrowRadius',
						label: __( 'Border Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
					{
						type: 'boxshadow',
						key: 'arrowShadow',
						label: __( 'BoxShadow', 'product-blocks' ),
					},
				],
			},
			{
				name: 'hover',
				title: __( 'Hover', 'product-blocks' ),
				options: [
					{
						type: 'color',
						key: 'arrowHoverColor',
						label: __( 'Hover Color', 'product-blocks' ),
					},
					{
						type: 'color',
						key: 'arrowHoverBg',
						label: __( 'Hover Bg Color', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'arrowHoverBorder',
						label: __( 'Hover Border', 'product-blocks' ),
					},
					{
						type: 'dimension',
						key: 'arrowHoverRadius',
						label: __( 'Hover Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
					{
						type: 'boxshadow',
						key: 'arrowHoverShadow',
						label: __( 'Hover BoxShadow', 'product-blocks' ),
					},
				],
			},
		],
	},
];

// Dot - Style
export const DotStyleArg = [
	{
		type: 'range',
		key: 'dotSpace',
		min: 0,
		max: 100,
		step: 1,
		unit: true,
		responsive: true,
		label: __( 'Space', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'dotVartical',
		min: -200,
		max: 700,
		step: 1,
		unit: true,
		responsive: true,
		label: __( 'Vertical Position', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'dotHorizontal',
		min: -800,
		max: 800,
		step: 1,
		unit: true,
		responsive: true,
		label: __( 'Horizontal Position', 'product-blocks' ),
	},
	{
		type: 'tab',
		key: 'dTab',
		content: [
			{
				name: 'normal',
				title: 'Normal',
				options: [
					{
						type: 'range',
						key: 'dotWidth',
						min: 0,
						max: 100,
						step: 1,
						responsive: true,
						label: __( 'Width', 'product-blocks' ),
					},
					{
						type: 'range',
						key: 'dotHeight',
						min: 0,
						max: 100,
						step: 1,
						unit: true,
						responsive: true,
						label: __( 'Height', 'product-blocks' ),
					},
					{
						type: 'color',
						key: 'dotBg',
						label: __( 'Background Color', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'dotBorder',
						label: __( 'Border', 'product-blocks' ),
					},
					{
						type: 'dimension',
						key: 'dotRadius',
						label: __( 'Border Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
					{
						type: 'boxshadow',
						key: 'dotShadow',
						label: __( 'BoxShadow', 'product-blocks' ),
					},
				],
			},
			{
				name: 'hover',
				title: 'Active',
				options: [
					{
						type: 'range',
						key: 'dotHoverWidth',
						min: 0,
						max: 100,
						step: 1,
						responsive: true,
						label: __( 'Width', 'product-blocks' ),
					},
					{
						type: 'range',
						key: 'dotHoverHeight',
						min: 0,
						max: 100,
						step: 1,
						unit: true,
						responsive: true,
						label: __( 'Height', 'product-blocks' ),
					},
					{
						type: 'color',
						key: 'dotHoverBg',
						label: __( 'Active Bg Color', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'dotHoverBorder',
						label: __( 'Active Border', 'product-blocks' ),
					},
					{
						type: 'dimension',
						key: 'dotHoverRadius',
						label: __( 'Active Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
					{
						type: 'boxshadow',
						key: 'dotHoverShadow',
						label: __( 'Active BoxShadow', 'product-blocks' ),
					},
				],
			},
		],
	},
];

// Heading - Content
export const HeadingSettingsArg = [
	{
		type: 'text',
		key: 'headingText',
		label: __( 'Add Heading', 'product-blocks' ),
	},
	{
		type: 'text',
		key: 'headingURL',
		label: __( 'Heading URL', 'product-blocks' ),
	},
	{
		type: 'alignment',
		key: 'headingAlign',
		label: __( 'Alignment', 'product-blocks' ),
		disableJustify: true,
	},
	{
		type: 'select',
		key: 'headingStyle',
		image: true,
		label: __( 'Heading Style', 'product-blocks' ),
		options: [
			{
				value: 'style1',
				label: __( 'Style 1', 'product-blocks' ),
				img: wopb_data.url + 'assets/img/blocks/heading/hstyle1.png',
			},
			{
				value: 'style2',
				label: __( 'Style 2', 'product-blocks' ),
				img: wopb_data.url + 'assets/img/blocks/heading/hstyle2.png',
			},
			{
				value: 'style3',
				label: __( 'Style 3', 'product-blocks' ),
				img: wopb_data.url + 'assets/img/blocks/heading/hstyle3.png',
			},
			{
				value: 'style4',
				label: __( 'Style 4', 'product-blocks' ),
				img: wopb_data.url + 'assets/img/blocks/heading/hstyle4.png',
			},
			{
				value: 'style5',
				label: __( 'Style 5', 'product-blocks' ),
				img: wopb_data.url + 'assets/img/blocks/heading/hstyle5.png',
			},
			{
				value: 'style6',
				label: __( 'Style 6', 'product-blocks' ),
				img: wopb_data.url + 'assets/img/blocks/heading/hstyle6.png',
			},
			{
				value: 'style7',
				label: __( 'Style 7', 'product-blocks' ),
				img: wopb_data.url + 'assets/img/blocks/heading/hstyle7.png',
			},
			{
				value: 'style8',
				label: __( 'Style 8', 'product-blocks' ),
				img: wopb_data.url + 'assets/img/blocks/heading/hstyle8.png',
			},
			{
				value: 'style9',
				label: __( 'Style 9', 'product-blocks' ),
				img: wopb_data.url + 'assets/img/blocks/heading/hstyle9.png',
			},
			{
				value: 'style10',
				label: __( 'Style 10', 'product-blocks' ),
				img: wopb_data.url + 'assets/img/blocks/heading/hstyle10.png',
			},
			{
				value: 'style11',
				label: __( 'Style 11', 'product-blocks' ),
				img: wopb_data.url + 'assets/img/blocks/heading/hstyle11.png',
			},
			{
				value: 'style12',
				label: __( 'Style 12', 'product-blocks' ),
				img: wopb_data.url + 'assets/img/blocks/heading/hstyle12.png',
			},
			{
				value: 'style13',
				label: __( 'Style 13', 'product-blocks' ),
				img: wopb_data.url + 'assets/img/blocks/heading/hstyle13.png',
			},
			{
				value: 'style14',
				label: __( 'Style 14', 'product-blocks' ),
				img: wopb_data.url + 'assets/img/blocks/heading/hstyle14.png',
			},
			{
				value: 'style15',
				label: __( 'Style 15', 'product-blocks' ),
				img: wopb_data.url + 'assets/img/blocks/heading/hstyle15.png',
			},
			{
				value: 'style16',
				label: __( 'Style 16', 'product-blocks' ),
				img: wopb_data.url + 'assets/img/blocks/heading/hstyle16.png',
			},
			{
				value: 'style17',
				label: __( 'Style 17', 'product-blocks' ),
				img: wopb_data.url + 'assets/img/blocks/heading/hstyle17.png',
			},
			{
				value: 'style18',
				label: __( 'Style 18', 'product-blocks' ),
				img: wopb_data.url + 'assets/img/blocks/heading/hstyle18.png',
			},
			{
				value: 'style19',
				label: __( 'Style 19', 'product-blocks' ),
				img: wopb_data.url + 'assets/img/blocks/heading/hstyle19.png',
			},
			{
				value: 'style20',
				label: __( 'Style 20', 'product-blocks' ),
				img: wopb_data.url + 'assets/img/blocks/heading/hstyle20.png',
			},
		],
	},
	{
		type: 'tag',
		key: 'headingTag',
		label: __( 'Heading Tag', 'product-blocks' ),
	},
	{
		type: 'text',
		key: 'headingBtnText',
		label: __( 'Button Text', 'product-blocks' ),
	},

	// Style
	{
		type: 'typography',
		key: 'headingTypo',
		label: __( 'Typography', 'product-blocks' ),
	},
	{
		type: 'color',
		key: 'headingColor',
		label: __( 'Color', 'product-blocks' ),
	},
	{
		type: 'color',
		key: 'headingBg',
		label: __( 'Background', 'product-blocks' ),
	},
	{
		type: 'color',
		key: 'headingBg2',
		label: __( 'Background 2', 'product-blocks' ),
	},
	{
		type: 'color',
		key: 'headingBorderBottomColor',
		label: __( 'Border Color', 'product-blocks' ),
		clip: true,
	},
	{
		type: 'color',
		key: 'headingBorderBottomColor2',
		label: __( 'Border Color 2', 'product-blocks' ),
		clip: true,
	},
	{
		type: 'range',
		key: 'headingBorder',
		label: __( 'Border Size', 'product-blocks' ),
		min: 1,
		max: 20,
		step: 1,
	},
	{
		type: 'typography',
		key: 'headingBtnTypo',
		label: __( 'Button Typography', 'product-blocks' ),
	},
	{
		type: 'color',
		key: 'headingBtnColor',
		label: __( 'Button Color', 'product-blocks' ),
	},
	{
		type: 'color',
		key: 'headingBtnHoverColor',
		label: __( 'Button Hover Color', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'headingSpacing',
		label: __( 'Spacing', 'product-blocks' ),
		min: 1,
		max: 150,
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'headingRadius',
		label: __( 'Border Radius', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'headingPadding',
		label: __( 'Padding', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},

	// Sub-Heading
	{
		type: 'toggle',
		key: 'subHeadingShow',
		label: __( 'Sub Heading', 'product-blocks' ),
	},
	{
		type: 'textarea',
		key: 'subHeadingText',
		label: __( 'Sub Heading Text', 'product-blocks' ),
	},
	{
		type: 'typography',
		key: 'subHeadingTypo',
		label: __( 'Typography', 'product-blocks' ),
	},
	{
		type: 'color',
		key: 'subHeadingColor',
		label: __( 'Color', 'product-blocks' ),
	},
	{
		type: 'dimension',
		key: 'subHeadingSpacing',
		label: __( 'Spacing', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
];

// Image - Style
export const ImageStyleArg = [
	{
		type: 'select',
		key: 'imgCrop',
		label: __( 'Large Item', 'product-blocks' ),
		options: fetchImageSize,
	},
	{
		type: 'select',
		key: 'imgAnimation',
		label: __( 'Hover Animation', 'product-blocks' ),
		options: [
			{ value: 'none', label: __( 'No Animation', 'product-blocks' ) },
			{ value: 'zoomIn', label: __( 'Zoom In', 'product-blocks' ) },
			{ value: 'zoomOut', label: __( 'Zoom Out', 'product-blocks' ) },
			{ value: 'opacity', label: __( 'Opacity', 'product-blocks' ) },
			{
				value: 'roateLeft',
				label: __( 'Rotate Left', 'product-blocks' ),
			},
			{
				value: 'rotateRight',
				label: __( 'Rotate Right', 'product-blocks' ),
			},
		],
	},
	{
		type: 'range',
		key: 'imgWidth',
		min: 0,
		max: 1000,
		step: 1,
		responsive: true,
		unit: [ 'px', 'em', '%' ],
		label: __( 'Width', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'imgHeight',
		min: 0,
		max: 1000,
		step: 1,
		responsive: true,
		unit: [ 'px', 'em', '%' ],
		label: __( 'Height', 'product-blocks' ),
	},
	{
		type: 'tab',
		key: 'imgTab',
		content: [
			{
				name: 'Normal',
				title: __( 'Normal', 'product-blocks' ),
				options: [
					{
						type: 'color',
						key: 'imgBgColor',
						label: __( 'Background Color', 'product-blocks' ),
					},
					{
						type: 'range',
						key: 'imgGrayScale',
						label: __( 'Gray Scale', 'product-blocks' ),
						min: 0,
						max: 100,
						step: 1,
						unit: [ '%' ],
						responsive: true,
					},
					{
						type: 'dimension',
						key: 'imgRadius',
						label: __( 'Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
					{
						type: 'boxshadow',
						key: 'imgShadow',
						label: __( 'BoxShadow', 'product-blocks' ),
					},
				],
			},
			{
				name: 'tab2',
				title: __( 'Hover', 'product-blocks' ),
				options: [
					{
						type: 'color',
						key: 'imgHoverBgColor',
						label: __( 'Background Color', 'product-blocks' ),
					},
					{
						type: 'range',
						key: 'imgHoverGrayScale',
						label: __( 'Hover Gray Scale', 'product-blocks' ),
						min: 0,
						max: 100,
						step: 1,
						unit: [ '%' ],
						responsive: true,
					},
					{
						type: 'dimension',
						key: 'imgHoverRadius',
						label: __( 'Hover Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
					{
						type: 'boxshadow',
						key: 'imgHoverShadow',
						label: __( 'Hover BoxShadow', 'product-blocks' ),
					},
				],
			},
		],
	},
	{
		type: 'dimension',
		key: 'imgMargin',
		label: __( 'Margin', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'toggle',
		key: 'imgOverlay',
		label: __( 'Overlay', 'product-blocks' ),
	},
	{
		type: 'select',
		key: 'imgOverlayType',
		label: __( 'Overlay Type', 'product-blocks' ),
		options: [
			{ value: 'default', label: __( 'Default', 'product-blocks' ) },
			{
				value: 'simgleGradient',
				label: __( 'Simple Gradient', 'product-blocks' ),
			},
			{
				value: 'multiColour',
				label: __( 'Multi Colour', 'product-blocks' ),
			},
			{ value: 'flat', label: __( 'Flat', 'product-blocks' ) },
			{ value: 'custom', label: __( 'Custom', 'product-blocks' ) },
		],
	},
	{
		type: 'color2',
		key: 'overlayColor',
		label: __( 'Custom Color', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'imgOpacity',
		min: 0,
		max: 1,
		step: 0.05,
		label: __( 'Overlay Opacity', 'product-blocks' ),
	},
];

// Title - Style
export const TitleStyleArg = [
	{ type: 'tag', key: 'titleTag', label: __( 'Tag', 'product-blocks' ) },
	{
		type: 'range',
		key: 'titleLength',
		min: 0,
		max: 40,
		step: 1,
		label: __( 'Title Max Words', 'product-blocks' ),
	},
	{
		type: 'color',
		key: 'titleColor',
		label: __( 'Color', 'product-blocks' ),
	},
	{
		type: 'color',
		key: 'titleHoverColor',
		label: __( 'Hover Color', 'product-blocks' ),
	},
	{
		type: 'typography',
		key: 'titleTypo',
		label: __( 'Typography', 'product-blocks' ),
	},
	{
		type: 'dimension',
		key: 'titlePadding',
		label: __( 'Padding', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
];

export const titleColor = [ 'titleColor', 'titleHoverColor' ];

// Price - Style
export const PriceStyleArg = [
	{
		type: 'color',
		key: 'priceColor',
		label: __( 'Color', 'product-blocks' ),
	},
	{
		type: 'typography',
		key: 'priceTypo',
		label: __( 'Typography', 'product-blocks' ),
	},
	{
		type: 'dimension',
		key: 'pricePadding',
		label: __( 'Padding', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
];

// Review - Style
export const ReviewStyleArg = [
	{
		type: 'color',
		key: 'reviewEmptyColor',
		label: __( 'Empty Color', 'product-blocks' ),
	},
	{
		type: 'color',
		key: 'reviewFillColor',
		label: __( 'Fill Color', 'product-blocks' ),
	},
	{
		type: 'dimension',
		key: 'reviewMargin',
		label: __( 'Margin', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
];

// Short Description - Style
export const ShortDescStyleArg = [
	{
		type: 'toggle',
		key: 'showFullShortDesc',
		label: __( 'Show Full Short Description', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'shortDescLimit',
		min: 1,
		max: 200,
		label: __( 'Short Description Limit', 'product-blocks' ),
	},
	{
		type: 'typography',
		key: 'ShortDescTypo',
		label: __( 'Typography', 'product-blocks' ),
	},
	{
		type: 'color',
		key: 'ShortDescColor',
		label: __( 'Color', 'product-blocks' ),
	},
	{
		type: 'dimension',
		key: 'ShortDescPadding',
		label: __( 'Padding', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
];

// Category Count - Style
export const CategoryCountStyleArg = [
	{
		type: 'text',
		key: 'categoryrCountText',
		label: __( 'Count Text', 'product-blocks' ),
	},
	{
		type: 'typography',
		key: 'categoryrCountTypo',
		label: __( 'Typography', 'product-blocks' ),
	},
	{
		type: 'color',
		key: 'categoryrCountColor',
		label: __( 'Color', 'product-blocks' ),
	},
	{
		type: 'dimension',
		key: 'categoryrCountPadding',
		label: __( 'Padding', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
];

// Sales - Style
export const SalesStyleArg = [
	{ type: 'text', key: 'saleText', label: __( 'Text', 'product-blocks' ) },
	{
		type: 'tag',
		key: 'salePosition',
		label: __( 'Sale Position', 'product-blocks' ),
		options: [
			{ value: 'topRight', label: __( 'Top Right', 'product-blocks' ) },
			{ value: 'topLeft', label: __( 'Top Left', 'product-blocks' ) },
			{
				value: 'buttonLeft',
				label: __( 'Bottom Left', 'product-blocks' ),
			},
			{
				value: 'buttonRight',
				label: __( 'Bottom Right', 'product-blocks' ),
			},
		],
	},
	{
		type: 'tag',
		key: 'saleDesign',
		label: __( 'Sales Design', 'product-blocks' ),
		options: [
			{ value: 'text', label: __( 'Text (Sales)', 'product-blocks' ) },
			{ value: 'digit', label: __( 'Digit (-10%)', 'product-blocks' ) },
			{ value: 'textDigit', label: __( '(-10%) Off', 'product-blocks' ) },
		],
	},
	{
		type: 'group',
		key: 'saleStyle',
		label: __( 'Sales Style', 'product-blocks' ),
		justify: true,
		options: [
			{ value: 'classic', label: __( 'Classic', 'product-blocks' ) },
			{ value: 'ribbon', label: __( 'Ribbon', 'product-blocks' ) },
		],
	},
	{
		type: 'color',
		key: 'salesColor',
		label: __( 'Color', 'product-blocks' ),
	},
	{
		type: 'color',
		key: 'salesBgColor',
		label: __( 'Background Color', 'product-blocks' ),
	},
	{
		type: 'typography',
		key: 'salesTypo',
		label: __( 'Typography', 'product-blocks' ),
	},
	{
		type: 'dimension',
		key: 'salesRadius',
		label: __( 'Border Radius', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'salesPadding',
		label: __( 'Padding', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
];

// Hot - Style
export const HotStyleArg = [
	{ type: 'text', key: 'hotText', label: __( 'Text', 'product-blocks' ) },
	{ type: 'color', key: 'hotColor', label: __( 'Color', 'product-blocks' ) },
	{
		type: 'color',
		key: 'hotBgColor',
		label: __( 'Background Color', 'product-blocks' ),
	},
	{
		type: 'typography',
		key: 'hotTypo',
		label: __( 'Typography', 'product-blocks' ),
	},
	{
		type: 'dimension',
		key: 'hotRadius',
		label: __( 'Border Radius', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'hotPadding',
		label: __( 'Padding', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
];

// Wishlist - Style
export const WishlistStyleArg = [
	{
		type: 'color',
		key: 'wishlistColor',
		label: __( 'Wishlist Color', 'product-blocks' ),
	},
	{
		type: 'color',
		key: 'wishlistHoverColor',
		label: __( 'Wishlist Hover Color', 'product-blocks' ),
	},
];

// Deal - Style
const DealStyleArg = [
	{
		type: 'text',
		key: 'dealText',
		label: __( 'Label Text', 'product-blocks' ),
	},
	{ type: 'color', key: 'dealColor', label: __( 'Color', 'product-blocks' ) },
	{
		type: 'color',
		key: 'dealBgColor',
		label: __( 'Background Color', 'product-blocks' ),
	},
	{
		type: 'typography',
		key: 'dealCountTypo',
		label: __( 'Conut Typography', 'product-blocks' ),
	},
	{
		type: 'typography',
		key: 'dealDayTypo',
		label: __( 'Day Typography', 'product-blocks' ),
	},
	{
		type: 'dimension',
		key: 'dealRadius',
		label: __( 'Border Radius', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'range',
		key: 'dealSpacer',
		min: 0,
		max: 20,
		step: 1,
		unit: true,
		responsive: true,
		label: __( 'Spacer', 'product-blocks' ),
	},
	{
		type: 'dimension',
		key: 'dealPadding',
		label: __( 'Padding', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
];

// Quick View - Style
export const QuickViewStyleArg = [
	{
		type: 'color',
		key: 'quickViewColor',
		label: __( 'Color', 'product-blocks' ),
	},
	{
		type: 'color',
		key: 'quickViewHoverColor',
		label: __( 'Hover Color', 'product-blocks' ),
	},
	{
		type: 'color',
		key: 'quickViewBg',
		label: __( 'Background', 'product-blocks' ),
	},
	{
		type: 'color',
		key: 'quickViewHoverBg',
		label: __( 'Hover Background', 'product-blocks' ),
	},
	{
		type: 'dimension',
		key: 'quickViewPadding',
		label: __( 'Padding', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
];

// Meta Elements - Style
const MetaElementStyleArg = [
	{
		type: 'tab',
		content: [
			{
				name: 'normal',
				title: __( 'Normal', 'product-blocks' ),
				options: [
					{
						type: 'color',
						key: 'metaElementColor',
						label: __( 'Color', 'product-blocks' ),
					},
					{
						type: 'color',
						key: 'metaElementBg',
						label: __( 'Background Color', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'metaElementBorder',
						label: __( 'Border', 'product-blocks' ),
					},
					{
						type: 'dimension',
						key: 'metaElementRadius',
						label: __( 'Border Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
					{
						type: 'boxshadow',
						key: 'metaElementShadow',
						label: __( 'BoxShadow', 'product-blocks' ),
					},
				],
			},
			{
				name: 'hover',
				title: __( 'Hover', 'product-blocks' ),
				options: [
					{
						type: 'color',
						key: 'metaElementHoverColor',
						label: __( 'Hover Color', 'product-blocks' ),
					},
					{
						type: 'color',
						key: 'metaElementHoverBg',
						label: __( 'Hover Bg Color', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'metaElementHoverBorder',
						label: __( 'Hover Border', 'product-blocks' ),
					},
					{
						type: 'dimension',
						key: 'metaElementHoverRadius',
						label: __( 'Hover Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
					{
						type: 'boxshadow',
						key: 'metaElementHoverShadow',
						label: __( 'Hover BoxShadow', 'product-blocks' ),
					},
				],
			},
		],
	},
	{
		type: 'range',
		key: 'metaElementSpacer',
		min: 0,
		max: 60,
		step: 1,
		unit: true,
		responsive: true,
		label: __( 'Spacer', 'product-blocks' ),
	},
	{
		type: 'dimension',
		key: 'metaElementPadding',
		label: __( 'Padding', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
];

const OverlayMetaStyleArg = [
	{
		type: 'toggle',
		key: 'hoverMeta',
		label: __( 'Visible Only Hover', 'product-blocks' ),
	},
	{
		type: 'toggle',
		key: 'ovrMetaInline',
		label: __( 'Inline View', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'ovrMetaElemSpace',
		min: 0,
		max: 400,
		step: 1,
		responsive: true,
		label: __( 'Space Between', 'product-blocks' ),
		unit: [ 'px' ],
	},
	{
		type: 'tag',
		key: 'tooltipPosition',
		label: __( 'Tooltip Posisition', 'product-blocks' ),
		options: [
			{ value: 'top', label: __( 'Top', 'product-blocks' ) },
			{ value: 'left', label: __( 'Left', 'product-blocks' ) },
			{ value: 'right', label: __( 'Right', 'product-blocks' ) },
		],
	},
	{
		type: 'alignment',
		key: 'overlayMetaHoriAlign',
		disableJustify: true,
		icons: [ 'juststart', 'justcenter', 'justend' ],
		options: [ 'flex-start', 'center', 'flex-end' ],
		label: __( 'Horizontal Position', 'product-blocks' ),
	},
	{
		type: 'alignment',
		key: 'overlayMetaVerAlign',
		disableJustify: true,
		icons: [ 'algnStart', 'algnCenter', 'algnEnd' ],
		options: [ 'flex-start', 'center', 'flex-end' ],
		label: __( 'Vertical Position', 'product-blocks' ),
	},
	{
		type: 'select',
		sort: true,
		key: 'overlayMetaList',
		multiple: true,
		label: __( 'Meta to Show', 'product-blocks' ),
		options: [
			{ value: '_compare', label: __( 'Compare', 'product-blocks' ) },
			{ value: '_wishlist', label: __( 'Wishlist', 'product-blocks' ) },
			{ value: '_cart', label: __( 'Cart', 'product-blocks' ) },
			{ value: '_qview', label: __( 'Quick View', 'product-blocks' ) },
		],
	},
	{
		type: 'separator',
		label: __( 'Overlay Meta Background', 'product-blocks' ),
	},
	{
		type: 'tab',
		content: [
			{
				name: 'normal',
				title: __( 'Normal', 'product-blocks' ),
				options: [
					{
						type: 'color2',
						key: 'ovrMetaBg',
						label: __( 'Background', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'ovrMetaBorder',
						label: __( 'Border', 'product-blocks' ),
					},
					{
						type: 'boxshadow',
						key: 'ovrMetaShadow',
						label: __( 'BoxShadow', 'product-blocks' ),
					},
					{
						type: 'dimension',
						key: 'ovrMetaBorderRad',
						label: __( 'Border Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
				],
			},
			{
				name: 'hover',
				title: __( 'Hover', 'product-blocks' ),
				options: [
					{
						type: 'color2',
						key: 'ovrMetaHvrBg',
						label: __( 'Background', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'ovrMetaHvrBorder',
						label: __( 'Border', 'product-blocks' ),
					},
					{
						type: 'boxshadow',
						key: 'ovrMetaHvrShadow',
						label: __( 'BoxShadow', 'product-blocks' ),
					},
					{
						type: 'dimension',
						key: 'ovrMetaHvrBorderRad',
						label: __( 'Border Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
				],
			},
		],
	},
	{
		type: 'dimension',
		key: 'ovrMetaPadding',
		label: __( 'Padding', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'ovrMetaSpacing',
		label: __( 'Spacing', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},

	{
		type: 'separator',
		label: __( 'Overlay Meta Element', 'product-blocks' ),
	},
	{
		type: 'tab',
		content: [
			{
				name: 'normal',
				title: __( 'Normal', 'product-blocks' ),
				options: [
					{
						type: 'color',
						key: 'ovrMetaElemClr',
						label: __( 'Color', 'product-blocks' ),
					},
					{
						type: 'color2',
						key: 'ovrMetaElemBg',
						label: __( 'Background', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'ovrMetaElemBorder',
						label: __( 'Border', 'product-blocks' ),
					},
					{
						type: 'boxshadow',
						key: 'ovrMetaElemShadow',
						label: __( 'BoxShadow', 'product-blocks' ),
					},
					{
						type: 'dimension',
						key: 'ovrMetaElemBorderRad',
						label: __( 'Border Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
				],
			},
			{
				name: 'hover',
				title: __( 'Hover', 'product-blocks' ),
				options: [
					{
						type: 'color',
						key: 'ovrMetaElemHvrClr',
						label: __( 'Color', 'product-blocks' ),
					},
					{
						type: 'color2',
						key: 'ovrMetaElemHvrBg',
						label: __( 'Background', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'ovrMetaElemHvrBorder',
						label: __( 'Border', 'product-blocks' ),
					},
					{
						type: 'boxshadow',
						key: 'ovrMetaElemHvrShadow',
						label: __( 'BoxShadow', 'product-blocks' ),
					},
					{
						type: 'dimension',
						key: 'ovrMetaElemHvrBorderRad',
						label: __( 'Border Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
				],
			},
		],
	},
	{
		type: 'range',
		key: 'ovrMetaElemWrapSize',
		min: 20,
		max: 200,
		step: 1,
		responsive: true,
		label: __( 'Wrap Size', 'product-blocks' ),
		unit: [ 'px' ],
	},
	{
		type: 'range',
		key: 'ovrMetaElemSize',
		min: 5,
		max: 100,
		step: 1,
		responsive: true,
		label: __( 'Icon Size', 'product-blocks' ),
		unit: [ 'px' ],
	},
];

// ReadMore - Style
export const ReadMoreStyleArg = [
	{
		type: 'text',
		key: 'readMoreText',
		label: __( 'Read More Text', 'product-blocks' ),
	},
	{
		type: 'typography',
		key: 'readMoreTypo',
		label: __( 'Typography', 'product-blocks' ),
	},
	{
		type: 'tab',
		key: 'rmTab',
		content: [
			{
				name: 'normal',
				title: __( 'Normal', 'product-blocks' ),
				options: [
					{
						type: 'color',
						key: 'readMoreColor',
						label: __( 'Color', 'product-blocks' ),
					},
					{
						type: 'color2',
						key: 'readMoreBgColor',
						label: __( 'Background Color', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'readMoreBorder',
						label: __( 'Border', 'product-blocks' ),
					},
					{
						type: 'dimension',
						key: 'readMoreRadius',
						label: __( 'Border Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
				],
			},
			{
				name: 'hover',
				title: __( 'Hover', 'product-blocks' ),
				options: [
					{
						type: 'color',
						key: 'readMoreHoverColor',
						label: __( 'Hover Color', 'product-blocks' ),
					},
					{
						type: 'color2',
						key: 'readMoreBgHoverColor',
						label: __( 'Hover Bg Color', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'readMoreHoverBorder',
						label: __( 'Hover Border', 'product-blocks' ),
					},
					{
						type: 'dimension',
						key: 'readMoreHoverRadius',
						label: __( 'Hover Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
				],
			},
		],
	},
	{
		type: 'dimension',
		key: 'readMoreSpacing',
		label: __( 'Spacing', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'readMorePadding',
		label: __( 'Padding', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
];

// Filter - Styles
export const FilterStyleArg = [
	{
		type: 'text',
		key: 'filterText',
		label: __( 'All Filter Text', 'product-blocks' ),
	},
	{
		type: 'group',
		key: 'filterType',
		label: __( 'Filter Type', 'product-blocks' ),
		justify: true,
		options: [
			{ value: 'product_cat', label: __( 'Category', 'product-blocks' ) },
			{ value: 'product_tag', label: __( 'Tag', 'product-blocks' ) },
		],
	},
	{
		type: 'search',
		key: 'filterCat',
		search: 'product_cat',
		label: __( 'Category', 'product-blocks' ),
		multiple: true,
		options: cat,
	},
	{
		type: 'search',
		key: 'filterTag',
		search: 'product_tag',
		label: __( 'Tags', 'product-blocks' ),
		multiple: true,
		options: tag,
	},
	{
		type: 'select',
		key: 'filterAction',
		label: __( 'Action Filter', 'product-blocks' ),
		multiple: true,
		pro: true,
		options: [
			{ value: '', label: __( '- Select -', 'product-blocks' ) },
			{ value: 'top_sale', label: __( 'Top Sale', 'product-blocks' ) },
			{
				value: 'popular',
				label: __( 'Popular (View)', 'product-blocks' ),
			},
			{ value: 'on_sale', label: __( 'On Sale', 'product-blocks' ) },
			{
				value: 'most_rated',
				label: __( 'Most Rated', 'product-blocks' ),
			},
			{ value: 'top_rated', label: __( 'Top Rated', 'product-blocks' ) },
			{ value: 'featured', label: __( 'Featured', 'product-blocks' ) },
			{ value: 'arrival', label: __( 'New Arrival', 'product-blocks' ) },
		],
	},
	{
		type: 'textarea',
		key: 'filterActionText',
		label: __( 'Action Text', 'product-blocks' ),
	},
	{
		type: 'toggle',
		key: 'filterBelowTitle',
		label: __( 'Below Heading', 'product-blocks' ),
	},
	{
		type: 'alignment',
		key: 'filterAlign',
		responsive: true,
		label: __( 'Alignment', 'product-blocks' ),
		disableJustify: true,
	},
	{
		type: 'typography',
		key: 'fliterTypo',
		label: __( 'Typography', 'product-blocks' ),
	},
	{
		type: 'tab',
		content: [
			{
				name: 'normal',
				title: __( 'Normal', 'product-blocks' ),
				options: [
					{
						type: 'color',
						key: 'filterColor',
						label: __( 'Color', 'product-blocks' ),
					},
					{
						type: 'color',
						key: 'filterBgColor',
						label: __( 'Background Color', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'filterBorder',
						label: __( 'Border', 'product-blocks' ),
					},
				],
			},
			{
				name: 'hover',
				title: __( 'Hover', 'product-blocks' ),
				options: [
					{
						type: 'color',
						key: 'filterHoverColor',
						label: __( 'Hover Color', 'product-blocks' ),
					},
					{
						type: 'color',
						key: 'filterHoverBgColor',
						label: __( 'Hover Bg Color', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'filterHoverBorder',
						label: __( 'Hover Border', 'product-blocks' ),
					},
				],
			},
		],
	},
	{
		type: 'dimension',
		key: 'filterRadius',
		label: __( 'Border Radius', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'fliterSpacing',
		label: __( 'Margin', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'fliterPadding',
		label: __( 'Padding', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'color',
		key: 'filterDropdownColor',
		label: __( 'Dropdown Text Color', 'product-blocks' ),
	},
	{
		type: 'color',
		key: 'filterDropdownHoverColor',
		label: __( 'Dropdown Hover Color', 'product-blocks' ),
	},
	{
		type: 'color',
		key: 'filterDropdownBg',
		label: __( 'Dropdown Background', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'filterDropdownRadius',
		min: 0,
		max: 300,
		step: 1,
		responsive: true,
		unit: [ 'px', 'em', '%' ],
		label: __( 'Dropdown Radius', 'product-blocks' ),
	},
	{
		type: 'dimension',
		key: 'filterDropdownPadding',
		label: __( 'Dropdown Padding', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'toggle',
		key: 'filterMobile',
		label: __( 'Enable Mobile Dropdown', 'product-blocks' ),
		pro: true,
	},
	{
		type: 'text',
		key: 'filterMobileText',
		label: __( 'Menu Text', 'product-blocks' ),
		help: __( 'Mobile Device Filter Menu Text', 'product-blocks' ),
	},
];

export const filterSettings = [
	'filterText',
	'filterType',
	'filterCat',
	'filterTag',
	'filterAction',
	'filterActionText',
	'filterBelowTitle',
	'filterMobile',
	'filterMobileText',
];

export const filterSpacing = [ 'fliterSpacing', 'fliterPadding' ];

// Content Wrap - Style
const ContentWrapStyleArg = [
	{
		type: 'tag',
		key: 'contentVerticalPosition',
		label: __( 'Vertical Position', 'product-blocks' ),
		pro: true,
		disabled: true,
		options: [
			{ value: 'topPosition', label: __( 'Top', 'product-blocks' ) },
			{
				value: 'middlePosition',
				label: __( 'Middle', 'product-blocks' ),
			},
			{
				value: 'bottomPosition',
				label: __( 'Bottom', 'product-blocks' ),
			},
		],
	},
	{
		type: 'tag',
		key: 'contentHorizontalPosition',
		label: __( 'Horizontal Position', 'product-blocks' ),
		pro: true,
		disabled: true,
		options: [
			{ value: 'leftPosition', label: __( 'Left', 'product-blocks' ) },
			{
				value: 'centerPosition',
				label: __( 'Center', 'product-blocks' ),
			},
			{ value: 'rightPosition', label: __( 'Right', 'product-blocks' ) },
		],
	},
	{
		type: 'range',
		key: 'contenWraptWidth',
		min: 0,
		max: 800,
		step: 1,
		unit: true,
		responsive: true,
		label: __( 'Width', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'contenWraptHeight',
		min: 0,
		max: 500,
		step: 1,
		unit: true,
		responsive: true,
		label: __( 'Height', 'product-blocks' ),
	},
	{
		type: 'tab',
		content: [
			{
				name: 'normal',
				title: __( 'Normal', 'product-blocks' ),
				options: [
					{
						type: 'color',
						key: 'contentWrapBg',
						label: __( 'Background Color', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'contentWrapBorder',
						label: __( 'Border', 'product-blocks' ),
					},
					{
						type: 'dimension',
						key: 'contentWrapRadius',
						label: __( 'Border Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
					{
						type: 'boxshadow',
						key: 'contentWrapShadow',
						label: __( 'BoxShadow', 'product-blocks' ),
					},
				],
			},
			{
				name: 'hover',
				title: __( 'Hover', 'product-blocks' ),
				options: [
					{
						type: 'color',
						key: 'contentWrapHoverBg',
						label: __( 'Hover Bg Color', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'contentWrapHoverBorder',
						label: __( 'Hover Border', 'product-blocks' ),
					},
					{
						type: 'dimension',
						key: 'contentWrapHoverRadius',
						label: __( 'Hover Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
					{
						type: 'boxshadow',
						key: 'contentWrapHoverShadow',
						label: __( 'Hover BoxShadow', 'product-blocks' ),
					},
				],
			},
		],
	},
	{
		type: 'dimension',
		key: 'contentWrapSpacing',
		label: __( 'Spacing', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'contentWrapPadding',
		label: __( 'Padding', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
];

// Pagination - Style
export const PaginationStyleArg = [
	{
		type: 'group',
		key: 'paginationType',
		label: __( 'Pagination Type', 'product-blocks' ),
		justify: true,
		options: [
			{ value: 'loadMore', label: __( 'Load More', 'product-blocks' ) },
			{
				value: 'pagination',
				label: __( 'Pagination', 'product-blocks' ),
			},
		],
	},
	{
		type: 'text',
		key: 'loadMoreText',
		label: __( 'Loadmore Text', 'product-blocks' ),
	},
	{
		type: 'text',
		key: 'paginationText',
		label: __( 'Pagination Text', 'product-blocks' ),
	},
	{
		type: 'alignment',
		key: 'pagiAlign',
		responsive: true,
		label: __( 'Alignment', 'product-blocks' ),
		disableJustify: true,
	},
	{
		type: 'group',
		key: 'paginationNav',
		label: __( 'Pagination', 'product-blocks' ),
		justify: true,
		options: [
			{
				value: 'textArrow',
				label: __( 'Text & Arrow', 'product-blocks' ),
			},
			{ value: 'onlyarrow', label: __( 'Only Arrow', 'product-blocks' ) },
		],
	},
	{
		type: 'toggle',
		key: 'paginationAjax',
		label: __( 'Ajax Pagination', 'product-blocks' ),
	},
	{
		type: 'select',
		key: 'navPosition',
		label: __( 'Navigation Position', 'product-blocks' ),
		options: [
			{ value: 'topRight', label: __( 'Top Right', 'product-blocks' ) },
			{ value: 'bottomLeft', label: __( 'Bottom', 'product-blocks' ) },
		],
	},
	{
		type: 'typography',
		key: 'pagiTypo',
		label: __( 'Typography', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'pagiArrowSize',
		min: 0,
		max: 100,
		step: 1,
		responsive: true,
		label: __( 'Arrow Size', 'product-blocks' ),
	},
	{
		type: 'tab',
		key: 'pagiTab',
		content: [
			{
				name: 'normal',
				title: 'Normal',
				options: [
					{
						type: 'color',
						key: 'pagiColor',
						label: __( 'Color', 'product-blocks' ),
						clip: true,
					},
					{
						type: 'color2',
						key: 'pagiBgColor',
						label: __( 'Background Color', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'pagiBorder',
						label: __( 'Border', 'product-blocks' ),
					},
					{
						type: 'boxshadow',
						key: 'pagiShadow',
						label: __( 'BoxShadow', 'product-blocks' ),
					},
					{
						type: 'dimension',
						key: 'pagiRadius',
						label: __( 'Border Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
				],
			},
			{
				name: 'hover',
				title: 'Hover',
				options: [
					{
						type: 'color',
						key: 'pagiHoverColor',
						label: __( 'Hover Color', 'product-blocks' ),
						clip: true,
					},
					{
						type: 'color2',
						key: 'pagiHoverbg',
						label: __( 'Hover Bg Color', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'pagiHoverBorder',
						label: __( 'Hover Border', 'product-blocks' ),
					},
					{
						type: 'boxshadow',
						key: 'pagiHoverShadow',
						label: __( 'BoxShadow', 'product-blocks' ),
					},
					{
						type: 'dimension',
						key: 'pagiHoverRadius',
						label: __( 'Hover Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
				],
			},
		],
	},
	{
		type: 'dimension',
		key: 'pagiMargin',
		label: __( 'Margin', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'navMargin',
		label: __( 'Margin', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'pagiPadding',
		label: __( 'Padding', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'range',
		key: 'pagiGap',
		min: 0,
		max: 300,
		step: 1,
		responsive: true,
		unit: [ 'px', 'em', '%' ],
		label: __( 'Pagination Gap', 'product-blocks' ),
	},
];

// General - Advanced
const GeneralAdvanceedArg = [
	{ type: 'text', key: 'advanceId', label: __( 'ID', 'product-blocks' ) },
	{
		type: 'range',
		key: 'advanceZindex',
		min: -100,
		max: 10000,
		step: 1,
		label: __( 'z-index', 'product-blocks' ),
	},
	{
		type: 'dimension',
		key: 'wrapMargin',
		label: __( 'Margin', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'wrapOuterPadding',
		label: __( 'Padding', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'tab',
		content: [
			{
				name: 'normal',
				title: 'Normal',
				options: [
					{
						type: 'color2',
						key: 'wrapBg',
						label: __( 'Background', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'wrapBorder',
						label: __( 'Border', 'product-blocks' ),
					},
					{
						type: 'boxshadow',
						key: 'wrapShadow',
						label: __( 'BoxShadow', 'product-blocks' ),
					},
					{
						type: 'dimension',
						key: 'wrapRadius',
						label: __( 'Border Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
				],
			},
			{
				name: 'hover',
				title: 'Hover',
				options: [
					{
						type: 'color2',
						key: 'wrapHoverBackground',
						label: __( 'Background', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'wrapHoverBorder',
						label: __( 'Hover Border', 'product-blocks' ),
					},
					{
						type: 'boxshadow',
						key: 'wrapHoverShadow',
						label: __( 'Hover BoxShadow', 'product-blocks' ),
					},
					{
						type: 'dimension',
						key: 'wrapHoverRadius',
						label: __( 'Hover Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
				],
			},
		],
	},
];

// Button - Style
export const ButtonStyleArg = [
	{
		type: 'typography',
		key: 'btnTypo',
		label: __( 'Typography', 'product-blocks' ),
	},
	{
		type: 'tab',
		key: 'bTab',
		content: [
			{
				name: 'normal',
				title: 'Normal',
				options: [
					{
						type: 'color',
						key: 'btnColor',
						label: __( 'Color', 'product-blocks' ),
					},
					{
						type: 'color2',
						key: 'btnBgColor',
						label: __( 'Background Color', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'btnBorder',
						label: __( 'Border', 'product-blocks' ),
					},
					{
						type: 'dimension',
						key: 'btnRadius',
						label: __( 'Border Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
					{
						type: 'boxshadow',
						key: 'btnShadow',
						label: __( 'BoxShadow', 'product-blocks' ),
					},
				],
			},
			{
				name: 'hover',
				title: 'Hover',
				options: [
					{
						type: 'color',
						key: 'btnHoverColor',
						label: __( 'Hover Color', 'product-blocks' ),
					},
					{
						type: 'color2',
						key: 'btnBgHoverColor',
						label: __( 'Hover Bg Color', 'product-blocks' ),
					},
					{
						type: 'border',
						key: 'btnHoverBorder',
						label: __( 'Hover Border', 'product-blocks' ),
					},
					{
						type: 'dimension',
						key: 'btnHoverRadius',
						label: __( 'Hover Radius', 'product-blocks' ),
						step: 1,
						unit: true,
						responsive: true,
					},
					{
						type: 'boxshadow',
						key: 'btnHoverShadow',
						label: __( 'Hover BoxShadow', 'product-blocks' ),
					},
				],
			},
		],
	},

	{
		type: 'dimension',
		key: 'btnSacing',
		label: __( 'Spacing', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'btnPadding',
		label: __( 'Padding', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
];

const ResponsiveAdvancedArg = [
	{
		type: 'toggle',
		key: 'hideExtraLarge',
		label: __( 'Hide On Extra Large', 'product-blocks' ),
	},
	{
		type: 'toggle',
		key: 'hideDesktop',
		label: __( 'Hide On Desktop', 'product-blocks' ),
	},
	{
		type: 'toggle',
		key: 'hideTablet',
		label: __( 'Hide On Tablet', 'product-blocks' ),
	},
	{
		type: 'toggle',
		key: 'hideMobile',
		label: __( 'Hide On Mobile', 'product-blocks' ),
	},
];

// Custom CSS - Advanced
const CustomCssAdvancedArg = [
	{
		type: 'textarea',
		key: 'advanceCss',
		placeholder: __(
			'Add {{WOPB}} before the selector to wrap element.',
			'product-blocks'
		),
	},
];

export const SpacingArg = [
	{
		type: 'range',
		key: 'columns',
		min: 1,
		max: 12,
		step: 1,
		responsive: true,
		label: __( 'Columns', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'columnGridGap',
		min: 0,
		max: 80,
		step: 1,
		responsive: true,
		unit: [ 'px', 'em' ],
		label: __( 'Column Grid Gap', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'rowGap',
		min: 0,
		max: 100,
		step: 1,
		responsive: true,
		unit: [ 'px', 'em' ],
		label: __( 'Row Gap', 'product-blocks' ),
	},
	{
		type: 'range',
		key: 'columnGap',
		min: 0,
		max: 100,
		step: 1,
		responsive: true,
		unit: [ 'px', 'em' ],
		label: __( 'Column Gap', 'product-blocks' ),
	},
	{ type: 'separator' },
	{
		type: 'dimension',
		key: 'wrapMargin',
		label: __( 'Margin', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
	{
		type: 'dimension',
		key: 'wrapOuterPadding',
		label: __( 'Padding', 'product-blocks' ),
		step: 1,
		unit: true,
		responsive: true,
	},
];

const HeadingSettings = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( 'Heading', 'product-blocks' ) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields(
				props.include,
				props.exclude,
				HeadingSettingsArg
			) }
		/>
	);
};

const GeneralSettings = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( 'General', 'product-blocks' ) }
			store={ props.store }
			data={ filterFields(
				props.include,
				props.exclude,
				GeneralSettingsArg
			) }
		/>
	);
};

export const GeneralSettingsWithQuery = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( 'General + Query', 'product-blocks' ) }
			store={ props.store }
			data={ filterFields(
				props.include,
				props.exclude,
				props.blockType && props.blockType == 'category'
					? GeneralSettingsArgWithQueryCat
					: GeneralSettingsArgWithQuery
			) }
		/>
	);
};

const QuerySettings = ( props ) => {
	return (
		<FieldGenerator
			youtube={
				'https://wpxpo.com/docs/wowstore/query-builder-for-product-sorting/'
			}
			initialOpen={ props.initialOpen || false }
			title={
				props.title == 'inline'
					? ''
					: __( 'Product Sort', 'product-blocks' )
			}
			store={ props.store }
			data={ filterFields(
				props.include,
				props.exclude,
				props.blockType && props.blockType == 'category'
					? QueryCatArchiveArg
					: QuerySettingsArg( props.store )
			) }
		/>
	);
};

const TitleStyle = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( 'Title', 'product-blocks' ) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields( props.include, props.exclude, TitleStyleArg ) }
		/>
	);
};

const ImageStyle = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( 'Image', 'product-blocks' ) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields( props.include, props.exclude, ImageStyleArg ) }
		/>
	);
};

const PriceStyle = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( 'Price', 'product-blocks' ) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields( props.include, props.exclude, PriceStyleArg ) }
		/>
	);
};

const ReviewStyle = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( 'Review', 'product-blocks' ) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields(
				props.include,
				props.exclude,
				ReviewStyleArg
			) }
		/>
	);
};

const ShortDescStyle = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( props.title ?? 'Short Description', 'product-blocks' ) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields(
				props.include,
				props.exclude,
				ShortDescStyleArg
			) }
		/>
	);
};

const CategoryCountStyle = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( 'Count', 'product-blocks' ) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields(
				props.include,
				props.exclude,
				CategoryCountStyleArg
			) }
		/>
	);
};

const SalesStyle = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( 'Sales', 'product-blocks' ) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields( props.include, props.exclude, SalesStyleArg ) }
		/>
	);
};

const HotStyle = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( 'Hots', 'product-blocks' ) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields( props.include, props.exclude, HotStyleArg ) }
		/>
	);
};

const DealStyle = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( 'Deal', 'product-blocks' ) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields( props.include, props.exclude, DealStyleArg ) }
		/>
	);
};

const QuickViewStyle = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( 'Quick View Style', 'product-blocks' ) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields(
				props.include,
				props.exclude,
				QuickViewStyleArg
			) }
		/>
	);
};

const WishlistStyle = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( 'Wishlist Style', 'product-blocks' ) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields(
				props.include,
				props.exclude,
				WishlistStyleArg
			) }
		/>
	);
};

const MetaElementStyle = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( 'Overlay Meta Elements', 'product-blocks' ) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields(
				props.include,
				props.exclude,
				MetaElementStyleArg
			) }
		/>
	);
};
const OverlayMetaElementStyle = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __(
				'Wishlist, Compare, Quickview, Cart',
				'product-blocks'
			) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields(
				props.include,
				props.exclude,
				OverlayMetaStyleArg
			) }
		/>
	);
};

const ReadMoreStyle = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( 'Read More', 'product-blocks' ) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields(
				props.include,
				props.exclude,
				ReadMoreStyleArg
			) }
		/>
	);
};

const ContentWrapStyle = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( 'Content Style', 'product-blocks' ) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields(
				props.include,
				props.exclude,
				ContentWrapStyleArg
			) }
		/>
	);
};

const PaginationStyle = ( props ) => {
	return (
		<FieldGenerator
			youtube={ 'https://www.youtube.com/watch?v=CaK1YpR8oZI' }
			initialOpen={ props.initialOpen || false }
			title={ __( 'Pagination', 'product-blocks' ) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields(
				props.include,
				props.exclude,
				PaginationStyleArg
			) }
		/>
	);
};

const FilterStyle = ( props ) => {
	return (
		<FieldGenerator
			youtube={ 'https://www.youtube.com/watch?v=DD6-dvZoXos' }
			initialOpen={ props.initialOpen || false }
			title={ __( 'Filter', 'product-blocks' ) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields(
				props.include,
				props.exclude,
				FilterStyleArg
			) }
		/>
	);
};

const CategoryStyle = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( 'Category', 'product-blocks' ) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields(
				props.include,
				props.exclude,
				CategoryStyleArg
			) }
		/>
	);
};

const DotStyle = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( 'Dot', 'product-blocks' ) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields( props.include, props.exclude, DotStyleArg ) }
		/>
	);
};

const ArrowStyle = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __(
				props.title ? props.title : 'Large Image Arrow',
				'product-blocks'
			) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields( props.include, props.exclude, ArrowStyleArg ) }
		/>
	);
};

const CartStyle = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( 'Cart', 'product-blocks' ) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields( props.include, props.exclude, CartStyleArg ) }
		/>
	);
};

const GeneralAdvanced = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( 'General', 'product-blocks' ) }
			store={ props.store }
			data={ filterFields(
				props.include,
				props.exclude,
				GeneralAdvanceedArg
			) }
		/>
	);
};

const ButtonStyle = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( props.title ?? 'Button Style', 'product-blocks' ) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields(
				props.include,
				props.exclude,
				ButtonStyleArg
			) }
		/>
	);
};

const ResponsiveAdvanced = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( 'Responsive', 'product-blocks' ) }
			store={ props.store }
			pro={ props.pro }
			data={ filterFields(
				props.include,
				props.exclude,
				ResponsiveAdvancedArg
			) }
		/>
	);
};

const CustomCssAdvanced = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={ __( 'Custom CSS', 'product-blocks' ) }
			store={ props.store }
			data={ filterFields(
				props.include,
				props.exclude,
				CustomCssAdvancedArg
			) }
		/>
	);
};

const Layout = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={
				props.title == 'inline' ? '' : __( 'Layout', 'product-blocks' )
			}
			block={ props.block }
			store={ props.store }
			data={ [ props.data ] }
		/>
	);
};

const CommonToolBar = ( props ) => {
	return (
		<FieldGenerator
			initialOpen={ props.initialOpen || false }
			title={
				props.title == 'inline' ? '' : __( 'Common', 'product-blocks' )
			}
			block={ props.block }
			store={ props.store }
			data={ [ props.data ] }
		/>
	);
};

const CommonSettings = ( props ) => {
	return (
		<FieldGenerator
			youtube={ props.youtube || __( '' ) }
			initialOpen={ props.initialOpen || false }
			title={ props.title || __( 'Common', 'product-blocks' ) }
			store={ props.store }
			depend={ props.depend }
			data={ filterFields( props.include, props.exclude, [] ) }
		/>
	);
};

export const ToolbarSettings = ( props ) => {
	return (
		<FieldGenerator
			isToolbar={ true }
			doc={ props.doc }
			depend={ props.depend }
			youtube={ props.youtube }
			title={ 'inline' }
			store={ props.store }
			data={ filterFields( props.include, props.exclude, [] ) }
		/>
	);
};

export function TypographyTB( props ) {
	return (
		<FieldGenerator
			doc={ props.doc }
			depend={ props.depend }
			youtube={ props.youtube }
			store={ props.store }
			data={ [
				{
					type: 'typography',
					key: props.attrKey,
					label: props.label,
					isToolbar: true,
				},
			] }
		/>
	);
}

export function AlignmentTB( props ) {
	const [ device, setDevice ] = useDevice();
	const {
		responsive = true,
		store,
		label,
		disableJustify = true,
		attrKey,
	} = props;
	const value = store.attributes[ attrKey ];
	const _val = value ? ( responsive ? value[ device ] : value ) : 'left';

	return (
		<Toolbar>
			<Dropdown
				renderToggle={ ( { onToggle } ) => (
					<ToolbarButton
						className={ 'wopb-toolbar-align' }
						onClick={ () => onToggle() }
						label={ label || 'Alignment' }
					>
						{ icons[ _val ] }
					</ToolbarButton>
				) }
				renderContent={ () => (
					<FieldGenerator
						store={ store }
						data={ [
							{
								type: 'alignment',
								key: attrKey,
								label,
								value,
								responsive,
								disableJustify,
							},
						] }
					/>
				) }
			/>
		</Toolbar>
	);
}

export function UltpToolbarDropdown( props ) {
	return (
		<FieldGenerator
			doc={ props.doc }
			depend={ props.depend }
			youtube={ props.youtube }
			store={ props.store }
			data={ [
				{
					type: 'toolbar_dropdown',
					label: props.label,
					options: props.options,
					key: props.attrKey,
				},
			] }
		/>
	);
}

/**
 *
 * @param {any} props
 * @return JSX.Element
 */
export const NextArrow = ( props ) => {
	const { className, onClick, arrowStyle, setSection } = props;
	const data = arrowStyle.split( '#' );
	return (
		<div
			className={ className }
			onClick={ ( e ) => {
				setSection( e );
				onClick();
			} }
		>
			{ icons[ data[ 1 ] ] }
		</div>
	);
};

/**
 *
 * @param {any} props
 * @return JSX.Element
 */
export const PrevArrow = ( props ) => {
	const { className, onClick, arrowStyle, setSection } = props;
	const data = arrowStyle.split( '#' );
	return (
		<div
			className={ className }
			onClick={ ( e ) => {
				setSection( e );
				onClick();
			} }
		>
			{ icons[ data[ 0 ] ] }
		</div>
	);
};

const SliderSetting = ( settings = {} ) => {
	let slideBreakPoints = [];
	if ( typeof settings.slidesToShow === 'string' ) {
		slideBreakPoints = [
			settings.slidesToShow,
			settings.slidesToShow,
			2,
			1,
		];
	} else {
		slideBreakPoints = [
			settings.slidesToShow.lg,
			settings.slidesToShow.sm,
			settings.slidesToShow.xs,
		];
	}

	let breakPoint = parseInt( slideBreakPoints[ 0 ] );
	const tabletPreview = document.querySelector( '.is-tablet-preview' );
	const mobilePreview = document.querySelector( '.is-mobile-preview' );
	if ( tabletPreview && tabletPreview.clientWidth ) {
		breakPoint = parseInt( slideBreakPoints[ 1 ] );
	} else if ( mobilePreview && mobilePreview.clientWidth ) {
		breakPoint = parseInt( slideBreakPoints[ 2 ] );
	}

	const responsive = {
		slidesToShow: breakPoint,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 780,
				settings: {
					slidesToShow: parseInt( slideBreakPoints[ 1 ] ),
					slidesToScroll: 1,
				},
			},
			{
				breakpoint: 360,
				settings: {
					slidesToShow: parseInt( slideBreakPoints[ 2 ] ),
					slidesToScroll: 1,
				},
			},
		],
	};

	const common = {
		arrows: true,
		dots: true,
		infinite: true,
		speed: 500,
		autoplay: true,
		autoplaySpeed: 3000,
		cssEase: 'linear',
		fade: false,
		lazyLoad: true,
		touchMove: false,
	};
	return Object.assign( {}, common, settings, responsive );
};

const renderPagination = (
	paginationNav,
	paginationAjax,
	paginationText,
	pages = 4
) => {
	const data = paginationText.split( '|' );
	const prevText = data[ 0 ] || __( 'Previous', 'product-blocks' );
	const nextText = data[ 1 ] || __( 'Next', 'product-blocks' );
	return (
		<div
			className={
				'wopb-pagination-wrap' /*+ (paginationAjax ? ' wopb-pagination-ajax-action' : '')*/
			}
		>
			<ul className="wopb-pagination">
				<li>
					<a href="#" className="wopb-prev-page-numbers">
						{ icons.leftAngle2 }
						{ paginationNav == 'textArrow' ? ' ' + prevText : '' }
					</a>
				</li>
				{ pages > 4 && (
					<li className="wopb-first-dot">
						<a href="#">...</a>
					</li>
				) }
				{ new Array( pages > 2 ? 3 : pages )
					.fill( 0 )
					.map( ( v, i ) => (
						<li key={ i }>
							<a href="#">{ i + 1 }</a>
						</li>
					) ) }
				{ pages > 4 && (
					<li className="wopb-last-dot">
						<a href="#">...</a>
					</li>
				) }
				{ pages > 5 && (
					<li className="wopb-last-pages">
						<a href="#">{ pages }</a>
					</li>
				) }
				<li>
					<a href="#" className="wopb-next-page-numbers">
						{ paginationNav == 'textArrow' ? nextText + ' ' : '' }
						{ icons.rightAngle2 }
					</a>
				</li>
			</ul>
		</div>
	);
};

const renderLoadmore = ( loadMoreText ) => {
	return (
		<div className="wopb-loadmore">
			<a className="wopb-loadmore-action">{ loadMoreText }</a>
		</div>
	);
};

export function LoadMore( { loadMoreText, onClick } ) {
	return (
		<div
			className="wopb-loadmore"
			onClick={ ( e ) => {
				onClick( e );
			} }
		>
			<a className="wopb-loadmore-action">{ loadMoreText }</a>
		</div>
	);
}

export function Pagination( {
	paginationNav,
	paginationAjax,
	paginationText,
	pages = 4,
	onClick,
} ) {
	const data = paginationText.split( '|' );
	const prevText = data[ 0 ] || __( 'Previous', 'product-blocks' );
	const nextText = data[ 1 ] || __( 'Next', 'product-blocks' );
	return (
		<div
			className={
				'wopb-pagination-wrap wopb-component' /*+ (paginationAjax ? ' wopb-pagination-ajax-action' : '')*/
			}
			onClick={ ( e ) => {
				onClick( e );
			} }
		>
			<ul className="wopb-pagination wopb-component-hover">
				<li>
					<a href="#" className="wopb-prev-page-numbers">
						{ icons.leftAngle2 }
						{ paginationNav == 'textArrow' ? ' ' + prevText : '' }
					</a>
				</li>
				{ pages > 4 && (
					<li className="wopb-first-dot">
						<a href="#">...</a>
					</li>
				) }
				{ new Array( pages > 2 ? 3 : pages )
					.fill( 0 )
					.map( ( v, i ) => (
						<li key={ i }>
							<a href="#">{ i + 1 }</a>
						</li>
					) ) }
				{ pages > 4 && (
					<li className="wopb-last-dot">
						<a href="#">...</a>
					</li>
				) }
				{ pages > 5 && (
					<li className="wopb-last-pages">
						<a href="#">{ pages }</a>
					</li>
				) }
				<li>
					<a href="#" className="wopb-next-page-numbers">
						{ paginationNav == 'textArrow' ? nextText + ' ' : '' }
						{ icons.rightAngle2 }
					</a>
				</li>
			</ul>
		</div>
	);
}

export function Filter( {
	filterText,
	filterType,
	filterCat,
	filterTag,
	filterAction,
	filterActionText,
	onClick,
} ) {
	const filterData = {
		top_sale: __( 'Top Sale', 'product-blocks' ),
		popular: __( 'Popular', 'product-blocks' ),
		on_sale: __( 'On Sale', 'product-blocks' ),
		most_rated: __( 'Most Rated', 'product-blocks' ),
		top_rated: __( 'Top Rated', 'product-blocks' ),
		featured: __( 'Featured', 'product-blocks' ),
		arrival: __( 'New Arrival', 'product-blocks' ),
	};

	const Arr = filterActionText.split( '|' );
	if ( Arr.length == 7 ) {
		Object.keys( filterData ).map(
			( v, k ) => ( filterData[ v ] = Arr[ k ] )
		);
	}

	return (
		<div
			className="wopb-filter-wrap wopb-component"
			onClick={ ( e ) => {
				onClick( e );
			} }
		>
			<ul className="wopb-flex-menu">
				{ filterText && filterAction.length <= 2 && (
					<li className="filter-item wopb-component-hover">
						<a href="#">{ filterText }</a>
					</li>
				) }
				{ JSON.parse(
					filterType == 'product_cat' ? filterCat : filterTag
				).map( ( v, k ) => {
					const item = v.value ? v.value : v;
					return (
						<li
							key={ k }
							className="filter-item wopb-component-hover"
						>
							<a href="#">{ item.replace( /-/g, ' ' ) }</a>
						</li>
					);
				} ) }
				{ filterAction &&
					JSON.parse( filterAction ).map( ( v, k ) => {
						const item = v.value ? v.value : v;
						return (
							<li
								key={ k }
								className="filter-item wopb-component-hover"
							>
								<a href="#">{ filterData[ item ] }</a>
							</li>
						);
					} ) }
			</ul>
		</div>
	);
}

const renderFilter = (
	filterText,
	filterType,
	filterCat,
	filterTag,
	filterAction,
	filterActionText
) => {
	const filterData = {
		top_sale: __( 'Top Sale', 'product-blocks' ),
		popular: __( 'Popular', 'product-blocks' ),
		on_sale: __( 'On Sale', 'product-blocks' ),
		most_rated: __( 'Most Rated', 'product-blocks' ),
		top_rated: __( 'Top Rated', 'product-blocks' ),
		featured: __( 'Featured', 'product-blocks' ),
		arrival: __( 'New Arrival', 'product-blocks' ),
	};

	const Arr = filterActionText.split( '|' );
	if ( Arr.length == 7 ) {
		Object.keys( filterData ).map(
			( v, k ) => ( filterData[ v ] = Arr[ k ] )
		);
	}

	return (
		<div className="wopb-filter-wrap">
			<ul className="wopb-flex-menu">
				{ filterText && filterAction.length <= 2 && (
					<li className="filter-item">
						<a href="#">{ filterText }</a>
					</li>
				) }
				{ JSON.parse(
					filterType == 'product_cat' ? filterCat : filterTag
				).map( ( v, k ) => {
					const item = v.value ? v.value : v;
					return (
						<li key={ k } className="filter-item">
							<a href="#">{ item.replace( /-/g, ' ' ) }</a>
						</li>
					);
				} ) }
				{ filterAction &&
					JSON.parse( filterAction ).map( ( v, k ) => {
						const item = v.value ? v.value : v;
						return (
							<li key={ k } className="filter-item">
								<a href="#">{ filterData[ item ] }</a>
							</li>
						);
					} ) }
			</ul>
		</div>
	);
};

export function isReloadCategory( prevAttr, attr ) {
	return (
		prevAttr.queryCat != attr.queryCat ||
		prevAttr.queryType != attr.queryType ||
		prevAttr.queryNumber != attr.queryNumber
	);
}

const isReload = (
	prevAttr,
	attr,
	query = [
		'queryNumber',
		'queryType',
		'queryStatus',
		'queryCat',
		'queryOrderBy',
		'queryOrder',
		'queryInclude',
		'queryExclude',
		'queryOffset',
		'queryQuick',
		'queryTax',
		'queryTaxValue',
		'queryRelation',
		'filterShow',
		'filterType',
		'filterCat',
		'filterTag',
		'filterAction',
		'filterText',
		'queryExcludeStock',
		'fallbackImg',
		'queryProductSort',
		'queryAdvanceProductSort',
		'querySpecificProduct',
		'queryIncludeAuthor',
		'queryExcludeAuthor',
		'queryStockStatus',
		'productListLayout',
		'showMoreResult',
		'showProductImage',
		'showProductTitle',
		'showProductPrice',
		'showProductRating',
	]
) => {
	let reload = false;
	for ( let i = 0; i < query.length; i++ ) {
		if ( query[ i ] == 'queryCat' ) {
			if ( prevAttr[ query[ i ] ] && attr[ query[ i ] ] ) {
				if (
					prevAttr[ query[ i ] ].length != attr[ query[ i ] ].length
				) {
					reload = true;
					break;
				}
			}
		} else if ( prevAttr[ query[ i ] ] != attr[ query[ i ] ] ) {
			reload = true;
			break;
		}
	}
	return reload;
};

const attrBuild = ( attr ) => {
	const args = { wpnonce: wopb_data.security };
	const {
		queryNumber,
		queryType,
		queryCat,
		queryOrderBy,
		queryOrder,
		queryOffset,
		queryInclude,
		queryExclude,
		queryStatus,
		queryQuick,
		queryTax,
		queryTaxValue,
		queryRelation,
		filterCat,
		filterTag,
		filterShow,
		filterType,
		filterAction,
		filterText,
		queryExcludeStock,
		fallbackImg,
		queryProductSort,
		queryAdvanceProductSort,
		querySpecificProduct,
		queryIncludeAuthor,
		queryExcludeAuthor,
		queryStockStatus,
	} = attr;

	if ( queryNumber ) {
		args.queryNumber = queryNumber;
	}
	if ( queryType ) {
		args.queryType = queryType;
	}
	if ( queryOrderBy ) {
		args.queryOrderBy = queryOrderBy;
	}
	if ( queryOrder ) {
		args.queryOrder = queryOrder;
	}
	if ( queryOffset ) {
		args.queryOffset = queryOffset;
	}
	if ( queryInclude ) {
		args.queryInclude = queryInclude;
	}
	if ( queryExclude ) {
		args.queryExclude = queryExclude;
	}
	if ( queryStatus ) {
		args.queryStatus = queryStatus;
	}
	if ( queryExcludeStock ) {
		args.queryExcludeStock = queryExcludeStock;
	}
	if ( queryQuick ) {
		args.queryQuick = queryQuick;
	}
	if ( filterCat ) {
		args.filterCat = filterCat;
	}
	if ( filterTag ) {
		args.filterTag = filterTag;
	}
	if ( filterShow ) {
		args.filterShow = filterShow;
	}
	if ( filterType ) {
		args.filterType = filterType;
	}
	if ( filterAction ) {
		args.filterAction = filterAction;
	}
	if ( filterText ) {
		args.filterText = filterText;
	}
	if ( queryCat.length > 2 ) {
		args.queryCat = queryCat;
	}
	if ( fallbackImg ) {
		args.fallbackImg = fallbackImg;
	}
	if ( queryTax ) {
		args.queryTax = queryTax;
	}
	if ( queryTaxValue ) {
		args.queryTaxValue = queryTaxValue;
	}
	if ( queryRelation ) {
		args.queryRelation = queryRelation;
	}
	if ( queryProductSort ) {
		args.queryProductSort = queryProductSort;
	}
	if ( queryAdvanceProductSort ) {
		args.queryAdvanceProductSort = queryAdvanceProductSort;
	}
	if ( querySpecificProduct ) {
		args.querySpecificProduct = querySpecificProduct;
	}
	if ( queryIncludeAuthor ) {
		args.queryIncludeAuthor = queryIncludeAuthor;
	}
	if ( queryExcludeAuthor ) {
		args.queryExcludeAuthor = queryExcludeAuthor;
	}
	if ( queryStockStatus ) {
		args.queryStockStatus = queryStockStatus;
	}

	return args;
};

const renderVariationSwitcher = (
	post,
	isVariationSwitchActive,
	showVariationSwitch,
	onClick,
	key = undefined
) => {
	return post.type === 'variable' &&
		isVariationSwitchActive === 'true' &&
		showVariationSwitch ? (
		<div
			key={ key }
			className={ `wopb-variation-swatche-dummy ${
				onClick ? 'wopb-component-simple' : ''
			}` }
			onClick={ ( e ) => onClick && onClick( e ) }
		>
			<div className="wopb-variation-swatches">
				<span className="wopb-swatch wopb-swatch-color wopb-swatch-red">
					<span className="wopb-variation-swatch-tooltip">Red</span>
				</span>
				<span className="wopb-swatch wopb-swatch-color wopb-swatch-green">
					<span className="wopb-variation-swatch-tooltip">Green</span>
				</span>
				<span className="wopb-swatch wopb-swatch-color wopb-swatch-blue">
					<span className="wopb-variation-swatch-tooltip">Blue</span>
				</span>
			</div>
			<div className="wopb-variation-swatches">
				<span className="wopb-swatch">
					<img
						src={
							wopb_data.url + 'assets/img/wopb-fallback-img.png'
						}
						alt=""
					/>
					<span className="wopb-variation-swatch-tooltip">
						Image 1
					</span>
				</span>
				<span className="wopb-swatch">
					<img
						src={
							wopb_data.url + 'assets/img/wopb-fallback-img.png'
						}
						alt=""
					/>
					<span className="wopb-variation-swatch-tooltip">
						Image 2
					</span>
				</span>
				<span className="wopb-swatch">
					<img
						src={
							wopb_data.url + 'assets/img/wopb-fallback-img.png'
						}
						alt=""
					/>
					<span className="wopb-variation-swatch-tooltip">
						Image 3
					</span>
				</span>
			</div>
			<div className="wopb-variation-swatches">
				<span className="wopb-swatch wopb-swatch-label">
					L
					<span className="wopb-variation-swatch-tooltip">Large</span>
				</span>
				<span className="wopb-swatch wopb-swatch-label">
					M
					<span className="wopb-variation-swatch-tooltip">
						Medium
					</span>
				</span>
				<span className="wopb-swatch wopb-swatch-label">
					S
					<span className="wopb-variation-swatch-tooltip">Small</span>
				</span>
			</div>
		</div>
	) : (
		''
	);
};

const renderCategory = (
	post,
	catShow,
	catPosition,
	onClick,
	key = undefined
) => {
	return post.category && catShow ? (
		<div
			key={ key }
			className={ `${ onClick ? 'wopb-component' : '' }` }
			onClick={ ( e ) => onClick && onClick( e ) }
		>
			<div
				className={ `wopb-category-grid wopb-cat-${ catPosition } ${
					onClick ? 'wopb-component-hover' : ''
				}` }
			>
				{ post.category.map( ( v, k ) => (
					<a key={ k }>{ v.name }</a>
				) ) }
			</div>
		</div>
	) : (
		''
	);
};

export function Price( { post, onClick } ) {
	return (
		<div
			className="wopb-product-price wopb-component-simple"
			dangerouslySetInnerHTML={ { __html: post.price_html } }
			onClick={ ( e ) => {
				onClick( e );
			} }
		></div>
	);
}

const renderTitle = (
	title,
	TitleTag = 'h3',
	onClick,
	key = undefined,
	limit = 0
) => {
	if ( limit && limit != 0 && title ) {
		const titleArr = title.split( ' ' );
		const titleArrL = titleArr.length;
		title =
			titleArr.splice( 0, limit ).join( ' ' ) +
			( titleArrL > limit ? '...' : '' );
	}
	return (
		<TitleTag
			key={ key }
			className={ `wopb-block-title ${
				onClick ? 'wopb-component-simple' : ''
			}` }
			onClick={ ( e ) => onClick && onClick( e ) }
		>
			<a dangerouslySetInnerHTML={ { __html: title } }></a>
		</TitleTag>
	);
};

const renderExcerpt = ( value, limit, onClick, key = undefined ) => {
	const data = value.split( ' ' ).splice( 0, limit ).join( ' ' ) + '...';
	return (
		<div
			key={ key }
			className={ `wopb-short-description ${
				onClick ? 'wopb-component-simple' : ''
			}` }
			onClick={ ( e ) => onClick && onClick( e ) }
			dangerouslySetInnerHTML={ { __html: data } }
		/>
	);
};

const renderCart = ( cartText, layout, leftTooltips, onClick ) => {
	return (
		<a
			className={ `wopb-product-btn ${
				onClick ? 'wopb-component' : ''
			}` }
			onClick={ ( e ) => onClick && onClick( e ) }
		>
			<span
				className={ `wopb-tooltip-text ${
					onClick ? 'wopb-component-hover' : ''
				}` }
			>
				{ icons.cart }
				<span
					className={ `${
						leftTooltips.includes( layout )
							? 'wopb-tooltip-text-left'
							: 'wopb-tooltip-text-top'
					}` }
				>
					{ cartText
						? cartText
						: __( 'Add to cart', 'product-blocks' ) }
				</span>
			</span>
		</a>
	);
};

const renderCompare = ( layout, leftTooltips ) => {
	return (
		<a className="wopb-compare-btn">
			<span className="wopb-tooltip-text">
				{ icons.compare }
				<span
					className={ `${
						leftTooltips.includes( layout )
							? 'wopb-tooltip-text-left'
							: 'wopb-tooltip-text-top'
					}` }
				>
					{ __( 'Compare', 'product-blocks' ) }
				</span>
			</span>
		</a>
	);
};

const renderWishlist = ( layout, leftTooltips, onClick ) => {
	return (
		<a
			className={ `wopb-wishlist-icon ${
				onClick ? 'wopb-component-simple' : ''
			}` }
			onClick={ ( e ) => onClick && onClick( e ) }
		>
			<span className="wopb-tooltip-text">
				{ icons.wishlist }
				{ icons.wishlistFill }
				<span
					className={ `${
						leftTooltips.includes( layout )
							? 'wopb-tooltip-text-left'
							: 'wopb-tooltip-text-top'
					}` }
				>
					{ __( 'Wishlist', 'product-blocks' ) }
				</span>
			</span>
		</a>
	);
};

const renderDeals = ( deal, dealText = '', onClick ) => {
	const Arr = dealText.split( '|' );
	if ( deal ) {
		return (
			<div
				className={ `wopb-product-deals ${
					onClick ? 'wopb-component-simple' : ''
				}` }
				data-date={ deal }
				onClick={ ( e ) => onClick && onClick( e ) }
			>
				<div className="wopb-deals-date">
					<strong className="wopb-deals-counter-days">00</strong>
					<span className="wopb-deals-periods">
						{ Arr[ 0 ] || __( 'Days', 'product-blocks' ) }
					</span>
				</div>
				<div className="wopb-deals-hour">
					<strong className="wopb-deals-counter-hours">00</strong>
					<span className="wopb-deals-periods">
						{ Arr[ 1 ] || __( 'Hours', 'product-blocks' ) }
					</span>
				</div>
				<div className="wopb-deals-minute">
					<strong className="wopb-deals-counter-minutes">00</strong>
					<span className="wopb-deals-periods">
						{ Arr[ 2 ] || __( 'Minutes', 'product-blocks' ) }
					</span>
				</div>
				<div className="wopb-deals-seconds">
					<strong className="wopb-deals-counter-seconds">00</strong>
					<span className="wopb-deals-periods">
						{ Arr[ 3 ] || __( 'Seconds', 'product-blocks' ) }
					</span>
				</div>
			</div>
		);
	}
	return '';
};

const renderQuickview = ( layout, leftTooltips, onClick ) => {
	return (
		<a
			className={ `wopb-quickview-btn ${
				onClick ? 'wopb-simple-component' : ''
			}` }
			onClick={ ( e ) => onClick && onClick( e ) }
		>
			<span className="wopb-tooltip-text">
				{ icons.quickview }
				<span
					className={ `${
						leftTooltips.includes( layout )
							? 'wopb-tooltip-text-left'
							: 'wopb-tooltip-text-top'
					}` }
				>
					{ __( 'Quick View', 'product-blocks' ) }
				</span>
			</span>
		</a>
	);
};

const bgVideoBg = (
	videoUrl = '',
	loop = true,
	start = 0,
	end = 0,
	fallback = ''
) => {
	if ( videoUrl ) {
		let src = '';
		if ( videoUrl.includes( 'youtu' ) ) {
			const regex = /youtu(?:.*\/v\/|.*v\=|\.be\/)([A-Za-z0-9_\-]{11})/gm;
			const match = regex.exec( videoUrl );
			if ( match && match[ 1 ] ) {
				src =
					'//www.youtube.com/embed/' +
					match[ 1 ] +
					'?playlist=' +
					match[ 1 ] +
					'&iv_load_policy=3&controls=0&autoplay=1&disablekb=1&rel=0&enablejsapi=1&showinfo=0&wmode=transparent&widgetid=1&playsinline=1&mute=1';
				src += '&loop=' + ( loop ? 1 : 0 );
				src += start ? '&start=' + start : '';
				src += end ? '&end=' + end : '';
			}
		} else if ( videoUrl.includes( 'vimeo' ) ) {
			const result = videoUrl.match(
				/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/(?:[^\/]*)\/videos\/|album\/(?:\d+)\/video\/|video\/|)(\d+)(?:[a-zA-Z0-9_\-]+)?/i
			);
			if ( result[ 1 ] ) {
				src =
					'//player.vimeo.com/video/' +
					result[ 1 ] +
					'?autoplay=1&title=0&byline=0&portrait=0&transparent=0&background=1';
				src += '&loop=' + ( loop ? 1 : 0 );
			}
		} else {
			return (
				<div className="wopb-rowbg-video">
					<video
						className="wopb-bgvideo"
						poster={ fallback && fallback }
						muted
						loop
						autoPlay
					>
						<source src={ videoUrl } />
					</video>
				</div>
			);
		}
		if ( src ) {
			return (
				<div className="wopb-rowbg-video">
					<iframe
						src={ src }
						frameBorder="0"
						allowFullScreen
					></iframe>
				</div>
			);
		}
		return fallback ? (
			<img src={ fallback } alt="Video Fallback Image" />
		) : (
			''
		);
	}
	return fallback ? <img src={ fallback } alt="Video Fallback Image" /> : '';
};

// From for row column
const updateChildAttr = ( clientId ) => {
	const innerBlocks = wp.data
		.select( 'core/block-editor' )
		.getBlocks( clientId );

	if ( innerBlocks.length > 0 ) {
		innerBlocks.forEach( ( el, _i ) => {
			dispatch( 'core/block-editor' ).updateBlockAttributes(
				el.clientId,
				{ updateChild: el.attributes.updateChild ? false : true }
			); // no need to initialize 'updateChild' key
		} );
	}
};

const getCustomFonts = ( styleCss = '', fontFace = false ) => {
	if ( wopb_data.settings?.wopb_custom_font != 'true' ) {
		return [];
	}
	const customFonts = wopb_option.custom_fonts;
	const customFontLists = [];
	let fonts = '';

	Object.keys( customFonts ).forEach( ( item ) => {
		const weight = [];
		customFonts[ item ].forEach( ( f ) => {
			const fontSrc = [];
			weight.push( f.weight );
			if ( styleCss.includes( item ) ) {
				if ( f.woff ) {
					fontSrc.push( `url(${ f.woff }) format('woff')` );
				}
				if ( f.woff2 ) {
					fontSrc.push( `url(${ f.woff2 }) format('woff2')` );
				}
				if ( f.ttf ) {
					fontSrc.push( `url(${ f.ttf }) format('TrueType')` );
				}
				if ( f.svg ) {
					fontSrc.push( `url(${ f.svg }) format('svg')` );
				}
				if ( f.eot ) {
					fontSrc.push( `url(${ f.eot }) format('eot')` );
				}
				fonts += `@font-face {font-family: "${ item }";font-weight: ${
					f.weight
				};font-display: auto;src: ${ fontSrc.join( ',' ) };}`;
			}
		} );
		customFontLists.push( { n: item, v: weight, f: '' } );
	} );
	if ( fontFace ) {
		return fonts + styleCss;
	}
	return customFontLists || [];
};

const wopbSupport = () => {
	if ( wopb_data.active ) {
		return (
			<div className="wopb-editor-support">
				<div className="wopb-editor-support-content">
					<img
						alt="site logo"
						className="wopb-logo"
						src={ wopb_data.url + `assets/img/logo-option.svg` }
					/>
					<div className="wopb-description">
						{ __( 'Need quick Human Support?', 'product-blocks' ) }
					</div>
					<a
						href="https://www.wpxpo.com/contact/?utm_source=db-wstore-editor&utm_medium=blocks-support&utm_campaign=wstore-dashboard"
						target="_blank"
						rel="noreferrer"
					>
						{ __( 'Get Support', 'product-blocks' ) }
					</a>
				</div>
			</div>
		);
	}

	const text =
		'Want access to advanced features? WowStore Pro is the way to go!';
	const link =
		'https://www.wpxpo.com/wowstore/pricing/?utm_source=db-wstore-editor&utm_medium=blocks-upgrade&utm_campaign=wstore-dashboard';

	return (
		<div className="wopb-editor-support">
			<div className="wopb-editor-support-content">
				<img
					alt="site logo"
					className="wopb-logo"
					src={ wopb_data.url + `assets/img/logo-option.svg` }
				/>
				<div className="wopb-description">
					{ __( text, 'product-blocks' ) }
				</div>
				<a href={ link } target="_blank" rel="noreferrer">
					{ __( 'Upgrade to Pro', 'product-blocks' ) }
				</a>
			</div>
		</div>
	);
};

const InitBlockPremade = ( props ) => {
	const {
		setAttributes,
		attributes: { initPremade },
		clientId,
	} = props.store;
	const { getBlocksByClientId } = wp.data.select( 'core/block-editor' );
	const { innerBlocks } = getBlocksByClientId( clientId )[ 0 ] || [];
	if ( ! initPremade && ! innerBlocks.length ) {
		return (
			<Modal
				isFullScreen
				title="Premade Patterns"
				onRequestClose={ ( e ) => {
					if ( e == undefined ) {
						setAttributes( { initPremade: true } );
					}
				} }
			>
				<DesignTemplate
					store={ props.store }
					setAttributes={ setAttributes }
					initPremade={ initPremade }
					closeModal={ () => {
						setAttributes( { initPremade: true } );
					} }
				/>
			</Modal>
		);
	}
};

const ProBlockLock = ( props ) => {
	const { img, name, upgradeLink, detailsLink } = props;
	if ( ! wopb_data.active ) {
		return (
			<div className="wopb-pro-block-info">
				<div className="wopb-block-info">
					<img src={ img } alt={ __( name, 'product-blocks' ) } />
					<span className="wopb-block-name">
						{ __( name, 'product-blocks' ) }
						<span>({ __( 'PRO', 'product-blocks' ) })</span>
					</span>
				</div>
				<div className="wopb-action-btn">
					<a
						className="wopb-upgrade-btn"
						href={ upgradeLink }
						target="_blank"
						rel="noreferrer"
					>
						{ __( 'Upgrade to Pro', 'product-blocks' ) }
					</a>
					<a
						className="wopb-details-btn wopb-primary-button"
						href={ detailsLink }
						target="_blank"
						style={ { padding: '7px 20px' } }
						rel="noreferrer"
					>
						{ __( 'View Details', 'product-blocks' ) }
					</a>
				</div>
			</div>
		);
	}
};

// for caching in reload
let reloadedTime = 0;
if ( wp.data.select( 'core/edit-site' ) ) {
	reloadedTime = new Date().getTime();
	const fseTemplates =
		wp.data
			.select( 'core' )
			?.getEntityRecords( 'postType', 'wp_template', { per_page: -1 } ) ||
		[];
	const fseTemplateParts =
		wp.data
			.select( 'core' )
			?.getEntityRecords( 'postType', 'wp_template_part', {
				per_page: -1,
			} ) || [];
}
const getAllTemplatePartsInPage = () => {
	let partsData = {};
	const editor_canvas = window.document.getElementsByName( 'editor-canvas' );
	const documentObj = editor_canvas[ 0 ]?.contentDocument
		? editor_canvas[ 0 ].contentDocument
		: window.document;
	const parts = documentObj?.querySelectorAll(
		"*[data-type='core/template-part']"
	);
	parts?.forEach( ( part ) => {
		const wpBlocks = part.querySelectorAll( '.wp-block' );
		const partId = part.dataset.block;
		const partIdBlock = [];
		wpBlocks?.forEach( ( block ) => {
			if (
				block?.dataset?.type?.includes( 'product-blocks/' ) &&
				block?.dataset?.block
			) {
				partIdBlock.push( block.dataset.block );
			}
		} );
		if ( partIdBlock.length ) {
			partsData = {
				...partsData,
				hasItems: 'yes',
				[ partId ]: partIdBlock,
			};
		}
	} );
	return partsData;
};

const updateCurrentPostId = ( data = {} ) => {
	const { setAttributes, currentPostId, clientId, c_pID, blockId } = data;
	const { getBlockAttributes, getBlockRootClientId } =
		wp.data.select( 'core/block-editor' );
	const reference = getBlockAttributes( getBlockRootClientId( clientId ) );

	const _client = clientId.substr( 0, 6 );
	if ( ! blockId ) {
		setAttributes( { blockId: _client } );
	} else if ( blockId && blockId != _client ) {
		if ( ! reference?.hasOwnProperty( 'ref' ) ) {
			setAttributes( { blockId: _client } );
		}
	}

	const settingAttributes = ( val, src ) => {
		if ( c_pID && val && val != currentPostId ) {
			if ( src != 'wp_block' ) {
				setAttributes( { currentPostId: val?.toString() } );
			}
		}
	};

	const __editorPostId = wp.data.select( 'core/editor' )?.getCurrentPostId();
	const fseEditor = wp.data.select( 'core/edit-site' );
	const _fsetype = fseEditor?.getEditedPostType() || '';

	if ( reference?.hasOwnProperty( 'ref' ) ) {
		settingAttributes( reference.ref, 'wp_block' );
	} else if ( document.querySelector( '.widgets-php' ) ) {
		settingAttributes( 'wopb-widget', 'widget' );
	} else if ( _fsetype == 'wp_template_part' || _fsetype == 'wp_template' ) {
		const currentTime = new Date().getTime();
		const timeout =
			reloadedTime && ( currentTime - reloadedTime ) / 1000 >= 4
				? 0
				: 1500;

		setTimeout( () => {
			const fseTemplates =
				wp.data
					.select( 'core' )
					?.getEntityRecords( 'postType', 'wp_template', {
						per_page: -1,
					} ) || [];
			const fseTemplateParts =
				wp.data
					.select( 'core' )
					?.getEntityRecords( 'postType', 'wp_template_part', {
						per_page: -1,
					} ) || [];
			const fseTemplatePartsData = getAllTemplatePartsInPage();
			let partClientId = '';

			if ( fseTemplatePartsData.hasItems ) {
				partClientId = Object.keys( fseTemplatePartsData ).find(
					( item ) => {
						return fseTemplatePartsData[ item ]?.includes(
							clientId
						);
					}
				);
			}
			if ( partClientId ) {
				const partAttr = wp.data
					.select( 'core/block-editor' )
					.getBlockAttributes( partClientId );
				const fsePart = fseTemplateParts.find( ( item ) =>
					item.id.includes( '//' + partAttr?.slug )
				);
				settingAttributes( fsePart?.wp_id, 'fse-part' );
			} else if (
				typeof __editorPostId === 'string' &&
				__editorPostId.includes( '//' )
			) {
				const currentTemplate = fseEditor?.getEditedPostId();
				const fseTemplate = (
					_fsetype == 'wp_template' ? fseTemplates : fseTemplateParts
				).find( ( item ) => item.id == currentTemplate );
				settingAttributes(
					fseTemplate?.wp_id,
					_fsetype == 'wp_template' ? 'fse-template' : 'fse-part'
				);
			} else {
				settingAttributes( __editorPostId, '__editorPostId_fse' );
			}
		}, timeout );
	} else if ( __editorPostId ) {
		settingAttributes( __editorPostId, '__editorPostId' );
	}
};

export {
	// Functions
	isReload,
	attrBuild,
	updateCurrentPostId,

	// Template Part
	renderCart,
	renderCompare,
	renderWishlist,
	renderDeals,
	renderQuickview,

	// Render
	renderTitle,
	renderCategory,
	renderPagination,
	renderLoadmore,
	renderFilter,
	renderExcerpt,
	GeneralSettings,
	QuerySettings,
	HeadingSettings,
	PriceStyle,
	SalesStyle,
	HotStyle,
	DealStyle,
	QuickViewStyle,
	WishlistStyle,
	MetaElementStyle,
	OverlayMetaElementStyle,
	ReviewStyle,
	ShortDescStyle,
	CartStyle,
	CategoryCountStyle,
	ArrowStyle,
	DotStyle,
	ImageStyle,
	TitleStyle,
	ReadMoreStyle,
	CategoryStyle,
	FilterStyle,
	PaginationStyle,
	ContentWrapStyle,
	GeneralAdvanced,
	CustomCssAdvanced,
	ButtonStyle,
	ResponsiveAdvanced,
	Layout,
	CommonToolBar,
	CommonSettings,

	// Settings
	SliderSetting,
	renderVariationSwitcher,
	bgVideoBg,
	updateChildAttr,
	getCustomFonts,
	wopbSupport,
	InitBlockPremade,
	ProBlockLock,
};
