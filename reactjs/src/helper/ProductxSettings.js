const { __ } = wp.i18n;
const { apiFetch, editPost, editSite } = wp;
const { Fragment, useState, useEffect } = wp.element;
const { RadioControl, Modal, ColorPicker, Dropdown } = wp.components;

import icons from './icons';
import Toggle from './fields/Toggle';
import Number from './fields/Number';
import Range from './fields/Range';

const Accordions = ( props ) => {
	const { children, title, icon } = props;
	const [ isShow, setShow ] = useState( false );
	return (
		<div className="wopb-section-accordion">
			<div
				className="wopb-section-accordion-item wopb-global-accordion-item"
				onClick={ () => setShow( ! isShow ) }
			>
				<div className="wopb-global-accordion-title">
					{ icon }
					{ title }
					{ isShow ? (
						<span className="wopb-section-arrow dashicons dashicons-arrow-down-alt2"></span>
					) : (
						<span className="wopb-section-arrow dashicons dashicons-arrow-up-alt2"></span>
					) }
				</div>
			</div>
			<div
				className={ `wopb-section-show ` + ( isShow ? '' : 'is-hide' ) }
			>
				{ children }
			</div>
		</div>
	);
};

const ProductxSettings = ( props ) => {
	const [ stateData, setState ] = useState( {
		presetColor1: '#00ADB5',
		presetColor2: '#F08A5D',
		presetColor3: '#B83B5E',
		presetColor4: '#B83B5E',
		presetColor5: '#71C9CE',
		presetColor6: '#F38181',
		presetColor7: '#FF2E63',
		presetColor8: '#EEEEEE',
		presetColor9: '#F9ED69',
		productxColorOnly: false,
	} );
	// const [ nowPop, setPop ] = useState('');
	// const [ nowTitle, setTitle ] = useState('');

	const {
		productxColorOnly,
		editorType,
		editorWidth,
		breakpointSm,
		breakpointXs,
	} = stateData;

	const _getColor = ( color ) => {
		return (
			'rgba(' +
			color.rgb.r +
			',' +
			color.rgb.g +
			',' +
			color.rgb.b +
			',' +
			color.rgb.a +
			')'
		);
	};

	const setEditor = ( data = {} ) => {
		let styleCss = '';
		if ( data.editorType == 'fullscreen' || data.editorType == 'custom' ) {
			styleCss =
				'.block-editor-page #editor .wp-block{ max-width: ' +
				( data.editorType == 'fullscreen'
					? '100%'
					: ( data.editorWidth || 1200 ) + 'px' ) +
				' !important;}';
		}
		if ( window.document.getElementById( 'wopb-block-global' ) === null ) {
			const cssInline = document.createElement( 'style' );
			cssInline.type = 'text/css';
			cssInline.id = 'wopb-block-global';
			if ( cssInline.styleSheet ) {
				cssInline.styleSheet.cssText = styleCss;
			} else {
				cssInline.innerHTML = styleCss;
			}
			window.document
				.getElementsByTagName( 'head' )[ 0 ]
				.appendChild( cssInline );
		} else {
			window.document.getElementById( 'wopb-block-global' ).innerHTML =
				styleCss;
		}
	};

	const setGlobalColor = ( data = {} ) => {
		let styleCss = '';
		styleCss +=
			':root { --productx-color1: ' +
			( data.presetColor1 || '#037fff' ) +
			';';
		styleCss +=
			'--productx-color2: ' + ( data.presetColor2 || '#026fe0' ) + ';';
		styleCss +=
			'--productx-color3: ' + ( data.presetColor3 || '#071323' ) + ';';
		styleCss +=
			'--productx-color4: ' + ( data.presetColor4 || '#132133' ) + ';';
		styleCss +=
			'--productx-color5: ' + ( data.presetColor5 || '#34495e' ) + ';';
		styleCss +=
			'--productx-color6: ' + ( data.presetColor6 || '#787676' ) + ';';
		styleCss +=
			'--productx-color7: ' + ( data.presetColor7 || '#f0f2f3' ) + ';';
		styleCss +=
			'--productx-color8: ' + ( data.presetColor8 || '#f8f9fa' ) + ';';
		styleCss +=
			'--productx-color9: ' + ( data.presetColor9 || '#ffffff' ) + ';}';

		window.document.getElementById(
			'productx-global-style-inline-css'
		).innerHTML = styleCss;
	};

	useEffect( () => {
		const fetchData = async () => {
			apiFetch( {
				path: '/wopb/v1/action_option',
				method: 'POST',
				data: { type: 'get' },
			} ).then( ( res ) => {
				if ( res.success ) {
					setState( Object.assign( {}, stateData, res.data ) );
					localStorage.setItem(
						'wopbGlobal',
						JSON.stringify( res.data )
					);
					setEditor( res.data );
				}
			} );
		};
		fetchData();
	}, [] );

	const setValue = ( type, value ) => {
		if ( type.indexOf( 'presetColor' ) > -1 ) {
			// For Only Color
			value = _getColor( value );
		}
		const allData = Object.assign( {}, stateData, { [ type ]: value } );
		setState( allData );
		localStorage.setItem( 'wopbGlobal', JSON.stringify( allData ) );
		apiFetch( {
			path: '/wopb/v1/action_option',
			method: 'POST',
			data: { type: 'set', data: allData },
		} );
		if ( type == 'editorType' || type == 'editorWidth' ) {
			setEditor( allData );
		}
		if ( type.indexOf( 'presetColor' ) > -1 ) {
			setGlobalColor( allData );
		}
	};

	// Check for Widget
	if ( ! editPost ) {
		return '';
	}

	const ProductXSideBar = editPost
		? editPost.PluginSidebar
		: editSite.PluginSidebar;

	return (
		<Fragment>
			<ProductXSideBar
				icon={ icons.logo }
				name="productx-settings"
				title={ __( 'WowStore Settings' ) }
			>
				<Accordions
					title={ __( 'Colors', 'product-blocks' ) }
					icon={ icons.editorColor }
				>
					<div className={ `wopb-global-color-label` }>
						Theme Color
					</div>
					{ (
						wp.data.select( 'core/editor' ).getEditorSettings()
							.colors || []
					).map( ( val, k ) => {
						return (
							<span
								key={ k }
								className={ `wopb-global-color` }
								style={ { backgroundColor: val.color } }
							></span>
						);
					} ) }
					<div className={ `wopb-global-custom-color` }>
						<div className={ `wopb-global-color-label` }>
							Set Custom Color
						</div>
						<div className={ `wopb-custom-color-lists` }>
							{ [
								'presetColor1',
								'presetColor2',
								'presetColor3',
								'presetColor4',
								'presetColor5',
								'presetColor6',
								'presetColor7',
								'presetColor8',
								'presetColor9',
							].map( ( v, k ) => {
								return (
									<Dropdown
										key={ k }
										renderToggle={ ( { onToggle } ) => (
											<span
												className={ `wopb-global-color` }
												onClick={ () => onToggle() }
												style={ {
													backgroundColor:
														stateData[ v ],
												} }
											/>
										) }
										renderContent={ () => (
											<ColorPicker
												onChangeComplete={ ( val ) =>
													setValue( v, val )
												}
											/>
										) }
									/>
								);
							} ) }
						</div>
					</div>
					<Toggle
						label={ __(
							'Use only WowStore Blocks Colors?',
							'product-blocks'
						) }
						value={ productxColorOnly }
						onChange={ ( v ) => setValue( 'productxColorOnly', v ) }
					/>
				</Accordions>

				<Accordions
					title={ __( 'Editor Width', 'product-blocks' ) }
					icon={ icons.editorWidth }
				>
					<RadioControl
						selected={ editorType || 'default' }
						options={ [
							{ label: 'Theme Default', value: 'default' },
							{ label: 'Full Screen', value: 'fullscreen' },
							{ label: 'Custom Size', value: 'custom' },
						] }
						onChange={ ( v ) => setValue( 'editorType', v ) }
					/>
					{ editorType == 'custom' && (
						<Range
							value={ editorWidth || 1200 }
							min={ 500 }
							max={ 3000 }
							placeholder={ '1200' }
							onChange={ ( v ) => setValue( 'editorWidth', v ) }
						/>
					) }
				</Accordions>

				<Accordions
					title={ __( 'Breakpoints', 'product-blocks' ) }
					icon={ icons.editorBreak }
				>
					<Range
						value={ breakpointSm || 992 }
						min={ 600 }
						max={ 1200 }
						step={ 1 }
						label={ __( 'Tablet (Max Width)', 'product-blocks' ) }
						onChange={ ( v ) => setValue( 'breakpointSm', v ) }
					/>
					<Range
						value={ breakpointXs || 768 }
						min={ 300 }
						max={ 1000 }
						step={ 1 }
						label={ __( 'Mobile (Max Width)', 'product-blocks' ) }
						onChange={ ( v ) => setValue( 'breakpointXs', v ) }
					/>
				</Accordions>
			</ProductXSideBar>
		</Fragment>
	);
};
export default ProductxSettings;
