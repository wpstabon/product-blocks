const { __ } = wp.i18n;
import icons from '../icons';

const renderGridWishlist = ( data ) => {
	const { tooltipPosition } = data;
	return (
		<div className={ `wopb-wishlist-icon wopb_meta_svg_con` }>
			<span className="wopb-tooltip-text">
				{ icons.wishlist }
				{ icons.wishlistFill }
				<span
					className={ `wopb-tooltip-text-${
						tooltipPosition || 'left'
					}` }
				>
					{ __( 'Wishlist', 'product-blocks' ) }
				</span>
			</span>
		</div>
	);
};

const renderGridQuickview = ( data ) => {
	const { tooltipPosition } = data;
	return (
		<div className={ `wopb-quickview-btn wopb_meta_svg_con` }>
			<span className="wopb-tooltip-text">
				{ icons.quickview }
				<span
					className={ `wopb-tooltip-text-${
						tooltipPosition || 'left'
					}` }
				>
					{ __( 'Quick View', 'product-blocks' ) }
				</span>
			</span>
		</div>
	);
};

const renderGridCompare = ( data ) => {
	const { tooltipPosition } = data;
	return (
		<div className="wopb-compare-btn wopb_meta_svg_con">
			<span className="wopb-tooltip-text">
				{ icons.compare }
				<span
					className={ `wopb-tooltip-text-${
						tooltipPosition || 'left'
					}` }
				>
					{ __( 'Compare', 'product-blocks' ) }
				</span>
			</span>
		</div>
	);
};

const renderGridCart = ( data ) => {
	const { tooltipPosition, cartText } = data;
	return (
		<div className={ `wopb-product-btn wopb_meta_svg_con` }>
			<span className={ `wopb-tooltip-text` }>
				{ icons.cart }
				<span
					className={ `wopb-tooltip-text-${
						tooltipPosition || 'left'
					}` }
				>
					{ cartText
						? cartText
						: __( 'Add to cart', 'product-blocks' ) }
				</span>
			</span>
		</div>
	);
};

export {
	renderGridWishlist,
	renderGridQuickview,
	renderGridCompare,
	renderGridCart,
};
