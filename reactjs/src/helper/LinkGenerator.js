const LinkGenerator = ( url, tagArgs ) => {
	const urlList = {
		//Productx dashboard menu
		menuBuilderUpgrade: [
			{ key: 'utm_source', value: 'db-wstore-builder' },
			{ key: 'utm_medium', value: 'popup-upgrade-pro' },
			{ key: 'utm_campaign', value: 'wstore-dashboard' },
		],

		//Gutenberg block
		blockProFeature: [
			{ key: 'utm_source', value: 'db-wstore-editor' },
			{ key: 'utm_medium', value: 'pro-features' },
			{ key: 'utm_campaign', value: 'wstore-dashboard' },
		],
		menu_save_temp_pro: [
			{ key: 'source', value: 'db-wstore-saved-template' },
			{ key: 'medium', value: 'popup-upgrade' },
			{ key: 'campaign', value: 'wstore-dashboard' },
		],
		menu_addons_popup: [
			{ key: 'source', value: 'db-wstore-addons' },
			{ key: 'medium', value: 'popup' },
			{ key: 'campaign', value: 'wstore-dashboard' },
		],
	};
	url = url ? url : 'https://www.wpxpo.com/wowstore/pricing/?';

	if ( tagArgs ) {
		url = url + ( url.includes( '?' ) ? '' : '?' );
		urlList[ tagArgs ]?.map( ( tag, i ) => {
			return ( url =
				url +
				( tag.key.includes( 'utm_' ) ? tag.key : `utm_${ tag.key }` ) +
				'=' +
				tag.value +
				'&' );
		} );
	} else {
		return url;
	}

	return url.slice( 0, -1 );
};

export default LinkGenerator;
