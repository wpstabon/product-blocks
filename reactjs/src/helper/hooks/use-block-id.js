/** @format */

import { updateCurrentPostId } from '../CommonPanel';

const { useEffect } = wp.element;
export default function useBlockId( props = {}, c_pID = true ) {
	const {
		attributes: { blockId, currentPostId },
		clientId,
		setAttributes,
	} = props;

	useEffect( () => {
		updateCurrentPostId( {
			setAttributes,
			currentPostId,
			clientId,
			blockId,
			c_pID,
		} );
	}, [ blockId, clientId ] );
}
