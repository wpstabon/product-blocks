/** @format */

import {
	resetState,
	restoreState,
	saveSelectedSection,
	saveToolbar,
	scrollSidebarSettings,
	SETTINGS_SECTION,
} from '../ux';
const { useState, useCallback, useEffect } = wp.element;

export default function useFluentSettings( options = {} ) {
	const [ section, setSection ] = useState( {
		...SETTINGS_SECTION,
		general: true,
	} );
	const [ toolbarSettings, _setToolbarSettings ] = useState( '' );

	function setToolbarSettings( title ) {
		_setToolbarSettings( title );
		saveToolbar( title );
	}

	const setCurrSettings = useCallback(
		(
			e,
			title,
			toolbarTitle = undefined,
			preventDefault = false,
			noToolbar = false
		) => {
			e?.stopPropagation();

			if ( preventDefault ) {
				e?.preventDefault();
			}

			setSection( {
				...SETTINGS_SECTION,
				[ title ]: true,
			} );
			saveSelectedSection( title );

			if ( ! noToolbar ) {
				setToolbarSettings( toolbarTitle ? toolbarTitle : title );
			}

			if ( ! options?.noScroll ) {
				scrollSidebarSettings( title );
			}
		},
		[]
	);

	useEffect( () => {
		resetState();
	}, [] );

	useEffect( () => {
		const { props, prevProps } = options;
		if ( props && prevProps ) {
			if (
				prevProps.isSelected !== props.isSelected &&
				props.isSelected
			) {
				restoreState(
					( title ) =>
						setCurrSettings( null, title, null, false, true ),
					setToolbarSettings
				);
			}
		}
	}, [ options?.props ?? false ] );

	return {
		section,
		setSection,
		setCurrSettings,
		toolbarSettings,
	};
}
