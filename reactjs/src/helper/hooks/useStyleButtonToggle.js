/** @format */

const { useState, useRef, useEffect } = wp.element;

function StyleButton( { onClick, hideStyleButton } ) {
	const ref = useRef( null );

	const handleClickOutside = ( event ) => {
		if ( ref.current && ! ref.current.contains( event.target ) ) {
			hideStyleButton();
		}
	};

	useEffect( () => {
		document.addEventListener( 'click', handleClickOutside, true );
		return () => {
			document.removeEventListener( 'click', handleClickOutside, true );
		};
	}, [] );

	return (
		<span
			ref={ ref }
			onClick={ onClick }
			className="wopb-ux-style-btn dashicons dashicons-admin-customizer"
		></span>
	);
}

export default function useStyleButtonToggle( onClick ) {
	const [ isVisible, setIsVisible ] = useState( false );

	const showStyleButton = () => {
		setIsVisible( true );
	};

	const hideStyleButton = () => {
		setIsVisible( false );
	};

	return {
		showStyleButton,
		StyleButton: isVisible ? (
			<StyleButton
				hideStyleButton={ hideStyleButton }
				onClick={ onClick }
			/>
		) : null,
	};
}
