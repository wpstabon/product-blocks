/** @format */

const { useEffect, useRef } = wp.element;

export default function usePrevious( value ) {
	const ref = useRef( null );
	useEffect( () => {
		ref.current = value;
	}, [ value ] );
	return ref.current;
}
