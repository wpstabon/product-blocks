/** @format */

import { attrBuild, isReload, isReloadCategory } from '../CommonPanel';

const { useState, useEffect } = wp.element;
const { addQueryArgs } = wp.url;

export default function useProducts(
	attributes,
	prevAttributes,
	options = {}
) {
	const [ postsList, setPostsList ] = useState( [] );
	const [ loading, setLoading ] = useState( true );
	const [ error, setError ] = useState( false );

	function fetchProducts() {
		setError( false );
		setLoading( true );

		const { queryCat, queryNumber, queryType } = attributes;

		const data = options?.isCategory
			? {
					queryCat: queryType == 'regular' ? '[]' : queryCat,
					queryNumber,
					queryType,
					wpnonce: wopb_data.security,
			  }
			: attrBuild( attributes );

		const endPoint = options?.isCategory ? '/wopb/category' : '/wopb/posts';

		const query = addQueryArgs( endPoint, data );

		wp.apiFetch( { path: query } )
			.then( ( obj ) => {
				setPostsList( obj );
			} )
			.catch( ( error ) => {
				setError( true );
			} )
			.finally( () => {
				setLoading( false );
			} );
	}

	// Fetch products on first load
	useEffect( () => {
		fetchProducts();
	}, [] );

	// Fetch products only if selected attributes change
	useEffect( () => {
		if ( ! prevAttributes ) {
			return;
		}

		const checkFunc = options?.isCategory ? isReloadCategory : isReload;

		if ( checkFunc( prevAttributes, attributes ) ) {
			fetchProducts();
		}
	}, [ attributes ] );

	return {
		loading,
		error,
		postsList,
	};
}
