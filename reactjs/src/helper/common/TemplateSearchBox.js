const TemplateSearchBox = ( { searchQuery, setSearchQuery, template } ) => {
	return (
		<div className="wopb-design-search-wrapper">
			<input
				type="search"
				id="wopb-design-search-form"
				className="wopb-design-search-input"
				placeholder="Search for..."
				value={ searchQuery }
				onChange={ ( e ) => {
					setSearchQuery( e.target.value );
				} }
			/>
		</div>
	);
};

export default TemplateSearchBox;
