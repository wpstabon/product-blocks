const { __ } = wp.i18n;
import Skeleton from '../../dashboard/utility/Skeleton';
import IconPack from '../fields/tools/IconPack';
import icons from '../icons';
import TemplateSearchBox from './TemplateSearchBox';

const PremadeDesignTemplate = ( props ) => {
	const {
		_changeVal,
		splitArchiveData,
		setState,
		setSearchQuery,
		_fetchFile,
		filter,
		state,
		searchQuery,
		dashboard,
		trend,
		setTrend,
		freePro,
		setFreePro,
		wishListArr,
		setWListAction,
		showWishList,
		setShowWishList,
	} = props;
	const {
		current,
		isTemplate,
		error,
		templates,
		designs,
		reload,
		reloadId,
		sidebarOpen,
		templatekitCol,
		fetch,
		loading,
	} = state;

	const localizedData = dashboard ? wopb_dashboard_pannel : wopb_data;

	const templateCat = [
		{ name: __( 'BackPack', 'product-blocks' ), slug: 'backpack' },
		{ name: __( 'Basic', 'product-blocks' ), slug: 'basic' },
		{ name: __( 'Clothing', 'product-blocks' ), slug: 'clothing' },
		{ name: __( 'Fashion', 'product-blocks' ), slug: 'fashion' },
		{ name: __( 'Fast Food', 'product-blocks' ), slug: 'fast-food' },
		{ name: __( 'Home Decor', 'product-blocks' ), slug: 'home-decor' },
		{ name: __( 'Jewelry', 'product-blocks' ), slug: 'jewelry' },
		{ name: __( 'Sun Glass', 'product-blocks' ), slug: 'sun-glass' },
	];
	const patternCat = [
		{ name: __( 'Button', 'product-blocks' ), slug: 'button-group' },
		{ name: __( 'List-WowStore', 'product-blocks' ), slug: 'advance-list' },
		{
			name: __( 'Product List 1', 'product-blocks' ),
			slug: 'product-list-1',
		},
		{
			name: __( 'Product Grid 1', 'product-blocks' ),
			slug: 'product-grid-1',
		},
		{
			name: __( 'Product Grid 2', 'product-blocks' ),
			slug: 'product-grid-2',
		},
		{
			name: __( 'Product Grid 3', 'product-blocks' ),
			slug: 'product-grid-3',
		},
		{
			name: __( 'Product Slider', 'product-blocks' ),
			slug: 'product-slider',
		},
		{
			name: __( 'Product Search', 'product-blocks' ),
			slug: 'product-search',
		},
		{
			name: __( 'Product Category 1', 'product-blocks' ),
			slug: 'product-category-1',
		},
		{
			name: __( 'Product Category 2', 'product-blocks' ),
			slug: 'product-category-2',
		},
		{
			name: __( 'Product Category 3', 'product-blocks' ),
			slug: 'product-category-3',
		},
	];

	const _setData = ( type ) => {
		let data = [];
		const filterTxt = isTemplate ? 'templateFilter' : 'designFilter';
		data = isTemplate ? templates : designs;
		if ( type == 'all' ) {
			setState( { ...state, [ filterTxt ]: type, current: data } );
		} else {
			setState( {
				...state,
				[ filterTxt ]: type,
				current: splitArchiveData( data, type ),
			} );
		}
	};

	const getProLinkUrl = ( name ) => {
		const extra = dashboard
			? `?utm_source=db-wstore-template-kits&utm_medium=${ name.replaceAll(
					' ',
					'-'
			  ) }-upgrade-pro&utm_campaign=wstore-dashboard`
			: wopb_data?.builder_type
			? `?utm_source=db-wstore-builder&utm_medium=${ name.replaceAll(
					' ',
					'-'
			  ) }-buider-library&utm_campaign=wstore-dashboard`
			: `?utm_source=db-wstore-editor&utm_medium=${
					isTemplate ? 'starter' : 'pattern'
			  }-upgrade&utm_campaign=wstore-dashboard`;

		return `https://www.wpxpo.com/wowstore/pricing/${ extra }`;
	};

	return (
		<>
			<div className="wopb-templatekit-wrap ">
				<div className="wopb-templatekit-list-container ">
					<div className="wopb-templatekit-layout-search-container">
						<div className="wopb-templatekit-search-container">
							<span>{ __( 'Filter:', 'product-blocks' ) }</span>
							{ ( ! localizedData.builder_type ||
								localizedData.builder_type == 'home_page' ) && (
								<select
									value={ filter }
									onChange={ ( v ) =>
										_setData( v.target.value )
									}
								>
									<option value="all">All</option>
									{ ( isTemplate
										? templateCat
										: patternCat
									).map( ( item, i ) => (
										<option
											key={ item.slug + i }
											value={ item.slug }
										>
											{ item.name }
										</option>
									) ) }
								</select>
							) }
							{ trend && (
								<select
									value={ trend }
									onChange={ ( v ) => {
										setTrend( v.target.value );
										if (
											v.target.value == 'latest' ||
											v.target.value == 'all'
										) {
											current.sort(
												( a, b ) => b.ID - a.ID
											);
										} else if (
											v.target.value == 'popular'
										) {
											if (
												current[ 0 ] &&
												current[ 0 ].hit
											) {
												current.sort(
													( a, b ) => b.hit - a.hit
												);
											}
										}
									} }
								>
									<option value="all">All</option>
									<option value="popular">
										{ __( 'Popular', 'product-blocks' ) }
									</option>
									<option value="latest">
										{ __( 'Latest', 'product-blocks' ) }
									</option>
								</select>
							) }
							{ freePro && (
								<select
									value={ freePro }
									onChange={ ( v ) =>
										setFreePro( v.target.value )
									}
								>
									<option value="all">All</option>
									<option value="free">
										{ __( 'Free', 'product-blocks' ) }
									</option>
									<option value="pro">
										{ __( 'Pro', 'product-blocks' ) }
									</option>
								</select>
							) }
						</div>
						<div className="wopb-templatekit-layout-container">
							<TemplateSearchBox
								searchQuery={ searchQuery }
								setSearchQuery={ setSearchQuery }
								template={
									isTemplate ? 'Starter Packs' : 'Patterns'
								}
							/>
							<span
								className={ `wopb-templatekit-iconcol2 ${
									templatekitCol == 'wopb-templatekit-col2'
										? 'wopb-lay-active'
										: ''
								}` }
								onClick={ () =>
									setState( {
										...state,
										templatekitCol: 'wopb-templatekit-col2',
									} )
								}
							>
								{ icons.grid_col1 }
							</span>
							<span
								className={ `wopb-templatekit-iconcol3 ${
									templatekitCol == 'wopb-templatekit-col3'
										? 'wopb-lay-active'
										: ''
								}` }
								onClick={ () =>
									setState( {
										...state,
										templatekitCol: 'wopb-templatekit-col3',
									} )
								}
							>
								{ icons.grid_col2 }
							</span>
							<div className="wopb-premade-wishlist-con">
								<span
									className={ `wopb-premade-wishlist cursor ${
										showWishList
											? 'wopb-wishlist-active'
											: ''
									}` }
									onClick={ () => {
										setShowWishList(
											showWishList ? false : true
										);
									} }
								>
									{
										IconPack[
											showWishList
												? 'love_solid'
												: 'love_line'
										]
									}
								</span>
							</div>
							{ dashboard && (
								<div
									onClick={ () => _fetchFile() }
									className="wopb-filter-sync"
								>
									<span
										className={
											'dashicons dashicons-update-alt' +
											( fetch ? ' rotate' : '' )
										}
									></span>
									{ __( 'Synchronize', 'product-blocks' ) }
								</div>
							) }
						</div>
					</div>

					{ current?.length > 0 ? (
						<>
							<div
								className={
									`wopb-premade-grid ` +
									( isTemplate
										? ''
										: ' wopb-templatekit-pattern-designs ' ) +
									templatekitCol
								}
							>
								{ current.map(
									( data ) =>
										data.name
											?.toLowerCase()
											.includes(
												searchQuery.toLowerCase()
											) &&
										( freePro == 'all' ||
											( freePro == 'pro' && data.pro ) ||
											( freePro == 'free' &&
												! data.pro ) ) &&
										( ! showWishList ||
											( showWishList &&
												wishListArr?.includes(
													data.ID
												) ) ) && (
											<div
												key={ data.ID }
												className={ `wopb-item-list` }
											>
												<div className="wopb-item-list-overlay">
													<a
														className={ `wopb-templatekit-img ${
															! (
																localizedData.builder_type &&
																localizedData.builder_type !=
																	'home_page'
															)
																? ''
																: 'builder_temps'
														}` }
														href={
															! (
																localizedData.builder_type &&
																localizedData.builder_type !=
																	'home_page'
															)
																? data.liveurl
																: '#'
														}
														target={
															! (
																localizedData.builder_type &&
																localizedData.builder_type !=
																	'home_page'
															)
																? '_blank'
																: '_self'
														}
														rel="noreferrer"
													>
														<img
															src={ data.image }
															loading="eager"
															alt={ data.title }
														/>
													</a>
													<div
														className={
															! (
																localizedData.builder_type &&
																localizedData.builder_type !=
																	'home_page'
															)
																? 'wopb-list-dark-overlay'
																: 'wopb-list-dark-overlay-none'
														}
													>
														{ data.pro ? (
															! localizedData.active && (
																<span className="wopb-templatekit-premium-btn">
																	{ __(
																		'Pro',
																		'product-blocks'
																	) }
																</span>
															)
														) : (
															<span className="wopb-templatekit-premium-btn wopb-templatekit-premium-free-btn">
																{ __(
																	'Free',
																	'product-blocks'
																) }
															</span>
														) }
														{ ! (
															localizedData.builder_type &&
															localizedData.builder_type !=
																'home_page'
														) && (
															<a
																className="wopb-overlay-view"
																href={
																	`https://www.wpxpo.com/${
																		isTemplate
																			? 'wowstore/starter-packs/'
																			: 'wowstore/patterns/'
																	}#demoid` +
																	data.ID
																}
																target="_blank"
																rel="noreferrer"
															>
																<span className="dashicons dashicons-visibility"></span>{ ' ' }
																{ __(
																	'Live Preview',
																	'product-blocks'
																) }
															</a>
														) }
													</div>
												</div>
												<div className="wopb-item-list-info">
													<span
														className=""
														dangerouslySetInnerHTML={ {
															__html: data.name,
														} }
													></span>
													<span className="wopb-action-btn">
														<span
															className="wopb-premade-wishlist"
															onClick={ () => {
																setWListAction(
																	data.ID,
																	wishListArr?.includes(
																		data.ID
																	)
																		? 'remove'
																		: ''
																);
															} }
														>
															{
																IconPack[
																	wishListArr?.includes(
																		data.ID
																	)
																		? 'love_solid'
																		: 'love_line'
																]
															}
														</span>
														{ data.pro &&
														! localizedData.active ? (
															<a
																className="wopb-btns wopbProBtn"
																target="_blank"
																href={ getProLinkUrl(
																	data.name
																) }
																rel="noreferrer"
															>
																{ __(
																	'Upgrade to Pro',
																	'product-blocks'
																) }
																&nbsp; &#10148;
															</a>
														) : data.pro &&
														  error ? (
															<a
																className="wopb-btns wopb-btn-warning"
																target="_blank"
																href={ getProLinkUrl(
																	data.name
																) }
																rel="noreferrer"
															>
																{ __(
																	'Get License',
																	'product-blocks'
																) }
															</a>
														) : (
															<span
																onClick={ () =>
																	dashboard
																		? _changeVal(
																				data.ID,
																				data.pro,
																				data.name
																		  )
																		: _changeVal(
																				data.ID,
																				data.pro
																		  )
																}
																className="wopb-btns wopb-btn-import"
															>
																{ ! (
																	reload &&
																	reloadId ==
																		data.ID
																) &&
																	IconPack.arrow_down_line }
																{ __(
																	'Import',
																	'product-blocks'
																) }
																{ reload &&
																	reloadId ==
																		data.ID && (
																		<span className="dashicons dashicons-update rotate" />
																	) }
															</span>
														) }
													</span>
												</div>
											</div>
										)
								) }
							</div>
						</>
					) : loading ? (
						<div className="wopb-premade-grid wopb-templatekit-col3 skeletonOverflow">
							{ Array( 3 )
								.fill( 1 )
								.map( ( v, i ) => {
									return (
										<div
											key={ i }
											className="wopb-item-list"
										>
											<div className="wopb-item-list-overlay">
												<Skeleton
													type="custom_size"
													c_s={ {
														size1: 100,
														unit1: '%',
														size2: 400,
														unit2: 'px',
													} }
												/>
											</div>
											<div className="wopb-item-list-info">
												<Skeleton
													type="custom_size"
													c_s={ {
														size1: 50,
														unit1: '%',
														size2: 25,
														unit2: 'px',
														br: 2,
													} }
												/>
												<span className="wopb-action-btn">
													<span className="wopb-premade-wishlist">
														<Skeleton
															type="custom_size"
															c_s={ {
																size1: 30,
																unit1: 'px',
																size2: 25,
																unit2: 'px',
																br: 2,
															} }
														/>
													</span>
													<Skeleton
														type="custom_size"
														c_s={ {
															size1: 70,
															unit1: 'px',
															size2: 25,
															unit2: 'px',
															br: 2,
														} }
													/>
												</span>
											</div>
										</div>
									);
								} ) }
						</div>
					) : (
						<span className="wopb-image-rotate">
							{ __( 'No Data found…', 'product-blocks' ) }
						</span>
					) }
				</div>
			</div>
		</>
	);
};

export default PremadeDesignTemplate;
