import PremadeDesignTemplate from './PremadeDesignTemplate';

const { __ } = wp.i18n;
const { parse } = wp.blocks;
const { Fragment, useState, useEffect } = wp.element;

const Library = ( props ) => {
	const [ state, setState ] = useState( {
		isPopup: props.isShow || false,
		templates: [],
		designs: [],
		reloadId: '',
		reload: false,
		isTemplate: true,
		error: false,
		fetch: false,
		templateFilter: 'all',
		designFilter: 'all',
		current: [],
		sidebarOpen: true,
		templatekitCol: 'wopb-templatekit-col3',
		loading: false,
	} );
	const [ searchQuery, setSearchQuery ] = useState( '' );
	const [ trend, setTrend ] = useState( 'all' );
	const [ freePro, setFreePro ] = useState( 'all' );
	const [ wishListArr, setWishlistArr ] = useState( [] );
	const [ showWishList, setShowWishList ] = useState( false );

	const {
		isPopup,
		isTemplate,
		templateFilter,
		designFilter,
		fetch,
		templates,
		designs,
	} = state;

	const fetchTemplates = async () => {
		setState( { ...state, loading: true } );
		wp.apiFetch( {
			path: '/wopb/v2/get_preset_data',
			method: 'POST',
			data: {
				type:
					wopb_data.builder_type &&
					wopb_data.builder_type != 'home_page'
						? 'premade'
						: 'all',
			},
		} )
			.then( ( response ) => {
				if ( response.success ) {
					const data = JSON.parse( response.data );
					if (
						wopb_data.builder_type &&
						wopb_data.builder_type != 'home_page'
					) {
						const currentData = templateSort(
							splitArchiveData( data, wopb_data.builder_type )
						);
						setState( {
							...state,
							current: currentData,
							loading: false,
						} );
					} else {
						templateSort( data.design );
						let templateData = templateSort( data.template );
						if (
							wopb_option?.post_type === 'wopb_templates' &&
							data.premade
						) {
							const builderData = templateSort(
								splitArchiveData( data.premade, [
									'single_product',
								] )
							);
							if ( builderData ) {
								templateData = [
									...templateData,
									...builderData,
								];
							}
						}
						setState( {
							...state,
							templates: data.template,
							designs: data.design,
							current: isTemplate ? templateData : data.design,
							loading: false,
						} );
					}
				}
			} )
			.catch( ( error ) => {
				console.error( error );
			} );
	};

	const templateSort = ( data ) => {
		if ( data ) {
			data.sort( ( a, b ) => {
				if ( a.pro && ! b.pro ) {
					return 1;
				}
				if ( ! a.pro && b.pro ) {
					return -1;
				}
				return 0;
			} );
		}
		return data;
	};

	const handleClickOutside = ( e ) => {
		if ( e.keyCode === 27 ) {
			const builderModal = document.querySelector(
				'.wopb-builder-modal'
			);
			if ( builderModal ) {
				builderModal.remove();
			}
			setState( { ...state, isPopup: false } );
		}
	};

	useEffect( () => {
		setWListAction( '', '', 'fetchData' );
		document.addEventListener( 'keydown', handleClickOutside );
		fetchTemplates();
		return () =>
			document.removeEventListener( 'keydown', handleClickOutside );
	}, [] );

	const close = () => {
		const element = document.querySelector( '.wopb-builder-modal' );
		if ( element.length > 0 ) {
			element.remove();
		}
		setState( { ...state, isPopup: false } );
	};

	const _changeVal = ( templateID, isPro ) => {
		if ( isPro ) {
			if ( wopb_data.active ) {
				insertBlock( templateID );
			}
		} else {
			insertBlock( templateID );
		}
	};

	const insertBlock = async ( templateID ) => {
		setState( { ...state, reload: true, reloadId: templateID } );

		window
			.fetch(
				'https://wopb.wpxpo.com/wp-json/restapi/v2/single-template',
				{
					method: 'POST',
					body: new URLSearchParams(
						'license=' +
							wopb_data.license +
							'&template_id=' +
							templateID
					),
				}
			)
			.then( ( response ) => response.text() )
			.then( ( jsonData ) => {
				jsonData = JSON.parse( jsonData );
				if ( jsonData.success && jsonData.rawData ) {
					wp.data
						.dispatch( 'core/block-editor' )
						.insertBlocks( parse( jsonData.rawData ) );
					close();
					setState( {
						...state,
						isPopup: false,
						reload: false,
						reloadId: '',
						error: false,
					} );
				} else {
					setState( { ...state, error: true } );
				}
			} )
			.catch( ( error ) => {
				console.error( error );
			} );
	};

	const _fetchFile = () => {
		setState( { ...state, fetch: true } );
		wp.apiFetch( {
			path: '/wopb/v2/get_preset_data',
			method: 'POST',
			data: {
				type: 'reset',
			},
		} ).then( ( response ) => {
			if ( response.success ) {
				fetchTemplates();
				setState( { ...state, fetch: false } );
			}
		} );
	};

	const splitArchiveData = ( data = [], key = '' ) => {
		const newData = data.filter( ( item ) => {
			const newItem = key
				? item?.category.filter( ( v ) =>
						key == 'all'
							? true
							: v.slug == key || key.includes( v.slug )
				  )
				: [];
			return newItem.length > 0 ? newItem : false;
		} );
		return newData;
	};
	const setWListAction = ( id, action = '', type = '' ) => {
		wp.apiFetch( {
			path: '/wopb/v2/premade_wishlist_save',
			method: 'POST',
			data: {
				id,
				action,
				type,
			},
		} ).then( ( res ) => {
			if ( res.success ) {
				setWishlistArr( res.wishListArr );
			}
		} );
	};

	const filter = isTemplate ? templateFilter : designFilter;

	return (
		<Fragment>
			{ isPopup && (
				<div className="wopb-builder-modal-shadow">
					<div className="wopb-popup-wrap">
						<div className="wopb-popup-header">
							<div className="wopb-popup-filter-title">
								<div className="wopb-popup-filter-image-head">
									<img
										src={
											wopb_data.url +
											'assets/img/logo-sm.svg'
										}
									/>
									<span>
										{ wopb_data.builder_type
											? __(
													'Builder Library',
													'product-blocks'
											  )
											: __(
													'Block Library',
													'product-blocks'
											  ) }
									</span>
								</div>
								{ ! wopb_data.builder_type && (
									<div className="wopb-popup-filter-nav">
										<div
											onClick={ () =>
												setState( {
													...state,
													isTemplate: true,
													templateFilter: 'all',
													current: templates,
												} )
											}
											className={
												'wopb-popup-tab-title' +
												( isTemplate
													? ' wopb-active'
													: '' )
											}
										>
											{ __(
												'Starter Packs',
												'product-blocks'
											) }
										</div>
										<div
											onClick={ () =>
												setState( {
													...state,
													isTemplate: false,
													designFilter: 'all',
													current: designs,
												} )
											}
											className={
												'wopb-popup-tab-title' +
												( isTemplate
													? ''
													: ' wopb-active' )
											}
										>
											{ __(
												'Premade Patterns',
												'product-blocks'
											) }
										</div>
									</div>
								) }
								<div className="wopb-popup-filter-sync-close">
									<span
										className={ 'wopb-popup-sync' }
										onClick={ () => _fetchFile() }
									>
										<i
											className={
												'dashicons dashicons-update-alt' +
												( fetch ? ' rotate' : '' )
											}
										/>
										{ __(
											'Synchronize',
											'product-blocks'
										) }
									</span>
									<button
										className="wopb-btn-close"
										onClick={ () => close() }
										id="wopb-btn-close"
									>
										<span className="dashicons dashicons-no-alt"></span>
									</button>
								</div>
							</div>
						</div>
						<PremadeDesignTemplate
							_changeVal={ _changeVal }
							splitArchiveData={ splitArchiveData }
							setState={ setState }
							setSearchQuery={ setSearchQuery }
							filter={ filter }
							state={ state }
							searchQuery={ searchQuery }
							trend={ trend }
							setTrend={ setTrend }
							freePro={ freePro }
							setFreePro={ setFreePro }
							wishListArr={ wishListArr || [] }
							setWListAction={ ( id, action ) =>
								setWListAction( id, action )
							}
							setShowWishList={ setShowWishList }
							showWishList={ showWishList }
						/>
					</div>
				</div>
			) }
		</Fragment>
	);
};

export default Library;
