const { __ } = wp.i18n;
const { useState, useEffect } = wp.element;
import Skeleton from '../../dashboard/utility/Skeleton';
import IconPack from '../fields/tools/IconPack';
import icons from '../icons';

const DesignTemplate = ( props ) => {
	const {
		setAttributes,
		initPremade,
		store: { clientId, attributes, label, name },
	} = props;
	const [ state, setState ] = useState( {
		designList: [],
		error: false,
		reload: false,
		reloadId: '',
		loading: true,
		templatekitCol: 'wopb-templatekit-col3',
		fetch: false,
	} );
	const {
		designList,
		error,
		reload,
		reloadId,
		loading,
		templatekitCol,
		fetch,
	} = state;
	const [ loader, setLoader ] = useState( false );
	const [ wishListArr, setWishlistArr ] = useState( [] );
	const [ showWishList, setShowWishList ] = useState( false );

	const splitArchiveData = ( data = [], key = '' ) => {
		const newData = data.filter( ( item ) => {
			const newItem = key
				? item?.category?.filter( ( v ) =>
						key == 'all' ? true : v.slug == key
				  )
				: [];
			return newItem?.length > 0 ? newItem : false;
		} );
		return newData;
	};

	const fetchData = async () => {
		setState( { ...state, loading: true } );
		const blockName = name.split( '/' );
		wp.apiFetch( {
			path: '/wopb/v2/get_preset_data',
			method: 'POST',
			data: {
				type: 'design',
			},
		} ).then( ( response ) => {
			if ( response.success ) {
				const data = JSON.parse( response.data );
				const splitData = splitArchiveData( data, blockName[ 1 ] );
				if ( blockName[ 1 ] && splitData.length == 0 ) {
					setLoader( true );
					_fetchFile();
				} else {
					const designData = splitData.sort( ( a, b ) => {
						if ( a.pro && ! b.pro ) {
							return 1;
						}
						if ( ! a.pro && b.pro ) {
							return -1;
						}
						return 0;
					} );
					setState( {
						...state,
						loading: false,
						designList: splitData,
					} );
					setLoader( false );
				}
			} else {
				setState( { ...state, loading: false } );
			}
		} );
	};

	const escFunction = ( event ) => {
		if ( event.key === 'Escape' ) {
			props.closeModal();
		}
	};

	useEffect( () => {
		setWListAction( '', '', 'fetchData' );
		fetchData();
		document.addEventListener( 'keydown', escFunction, false );
		return () => {
			document.removeEventListener( 'keydown', escFunction );
		};
	}, [] );

	const _changeVal = ( designID, isPro ) => {
		setState( { ...state, reload: true, reloadId: designID } );
		if ( isPro ) {
			if ( wopb_data.active ) {
				_changeDesign( designID );
			}
		} else {
			_changeDesign( designID );
		}
	};

	const _fetchFile = () => {
		setState( { ...state, fetch: true } );
		wp.apiFetch( {
			path: '/wopb/v2/get_preset_data',
			method: 'POST',
			data: {
				type: 'reset',
			},
		} ).then( ( response ) => {
			if ( response.success ) {
				fetchData();
				setState( { ...state, fetch: false } );
			}
		} );
	};

	const _changeDesign = ( designID ) => {
		const designEndpoint = 'https://wopb.wpxpo.com/wp-json/restapi/v2/';
		const { replaceBlock } = wp.data.dispatch( 'core/block-editor' );
		const removeItem = [
			'querySpecificProduct',
			'queryNumber',
			'queryType',
			'queryTax',
			'queryCat',
			'queryTag',
			'queryOrderBy',
			'queryOrder',
			'queryInclude',
			'queryExclude',
			'queryOffset',
			'metaKey',
			'queryProductSort',
			'queryTaxValue',
		];

		window
			.fetch( designEndpoint + 'single-design', {
				method: 'POST',
				body: new URLSearchParams(
					'license=' + wopb_data.license + '&design_id=' + designID
				),
			} )
			.then( ( response ) => response.text() )
			.then( ( jsonData ) => {
				jsonData = JSON.parse( jsonData );
				if ( jsonData.success && jsonData.rawData ) {
					const parseData = wp.blocks.parse( jsonData.rawData );
					let attr = parseData[ 0 ].attributes;
					for ( let i = 0; i < removeItem.length; i++ ) {
						if ( attr[ removeItem[ i ] ] ) {
							delete attr[ removeItem[ i ] ];
						}
					}
					if (
						attr.hasOwnProperty( 'initPremade' ) &&
						! attr.initPremade
					) {
						attr.initPremade = true;
					}
					attr = Object.assign( {}, attributes, attr );
					parseData[ 0 ].attributes = attr;
					setState( {
						...state,
						error: false,
						reload: false,
						reloadId: '',
					} );
					replaceBlock( clientId, parseData );
				} else {
					setState( {
						...state,
						error: true,
						reload: false,
						reloadId: '',
					} );
				}
			} )
			.catch( ( error ) => {
				console.error( error );
			} );
	};

	const setWListAction = ( id, action = '', type = '' ) => {
		wp.apiFetch( {
			path: '/wopb/v2/premade_wishlist_save',
			method: 'POST',
			data: {
				id,
				action,
				type,
			},
		} ).then( ( res ) => {
			if ( res.success ) {
				setWishlistArr( res.wishListArr );
			}
		} );
	};

	if ( typeof designList === 'undefined' ) {
		return null;
	}

	const modal_title = (
		<span className="wopb-templatekit-design-template-modal-title">
			<img
				src={
					wopb_data.url +
					`assets/img/blocks/${
						props.store.name.split( '/' )[ 1 ]
					}.svg`
				}
			/>
			<span>
				{ props.store.name
					.split( '/' )[ 1 ]
					.replace( '-', ' ' )
					.replace( '-', ' #' ) }
			</span>
		</span>
	);

	const patternsGetProUrl =
		'https://www.wpxpo.com/wowstore/pricing/?utm_source=db-wstore-editor&utm_medium=blocks-premade&utm_campaign=wstore-dashboard';

	return (
		<div className="wopb-templatekit-design-template-container wopb-templatekit-list-container wopb-predefined-patterns">
			{ label && <label>{ label }</label> }
			<div className="wopb-popup-header ">
				<div className="wopb-popup-filter-title">
					<div className="wopb-popup-filter-image-head">
						{ modal_title }
					</div>
					<div className="wopb-popup-filter-sync-close">
						<span
							className={ `wopb-templatekit-iconcol2 ${
								templatekitCol == 'wopb-templatekit-col2'
									? 'wopb-lay-active'
									: ''
							}` }
							onClick={ () =>
								setState( {
									...state,
									templatekitCol: 'wopb-templatekit-col2',
								} )
							}
						>
							{ icons.grid_col1 }
						</span>
						<span
							className={ `wopb-templatekit-iconcol3 ${
								templatekitCol == 'wopb-templatekit-col3'
									? 'wopb-lay-active'
									: ''
							}` }
							onClick={ () =>
								setState( {
									...state,
									templatekitCol: 'wopb-templatekit-col3',
								} )
							}
						>
							{ icons.grid_col2 }
						</span>
						{ ! props.hasOwnProperty( 'initPremade' ) && (
							<div className="wopb-premade-wishlist-con">
								<span
									className={ `wopb-premade-wishlist cursor ${
										showWishList
											? 'wopb-wishlist-active'
											: ''
									}` }
									onClick={ () => {
										setShowWishList(
											showWishList ? false : true
										);
									} }
								>
									{
										IconPack[
											showWishList
												? 'love_solid'
												: 'love_line'
										]
									}
								</span>
							</div>
						) }
						<div
							onClick={ () => _fetchFile() }
							className="wopb-filter-sync"
						>
							<span
								className={
									'dashicons dashicons-update-alt' +
									( fetch ? ' rotate' : '' )
								}
							></span>
							{ __( 'Synchronize', 'product-blocks' ) }
						</div>
						<button
							className="wopb-btn-close"
							onClick={ () => props.closeModal() }
							id="wopb-btn-close"
						>
							<span className="dashicons dashicons-no-alt"></span>
						</button>
					</div>
				</div>
			</div>

			{ designList?.length ? (
				<>
					<div
						className={ `wopb-premade-grid wopb-templatekit-content-designs ${ templatekitCol }` }
					>
						{ props.hasOwnProperty( 'initPremade' ) &&
							! initPremade && (
								<div className="wopb-item-list wopb-build-scratch">
									<div
										className="wopb-item-content"
										onClick={ () =>
											setAttributes( {
												initPremade: true,
											} )
										}
									>
										<span className="dashicons dashicons-plus-alt2" />
										<span
											className="wopb-templatekit-title"
											style={ { fontWeight: '600' } }
										>
											{ __(
												'Build From Scratch',
												'product-blocks'
											) }
										</span>
									</div>
								</div>
							) }
						{ designList.map( ( data, k ) => {
							return (
								( ! showWishList ||
									( showWishList &&
										wishListArr?.includes(
											data.ID
										) ) ) && (
									<div
										key={ k }
										className={ `wopb-item-list` }
									>
										<div className="wopb-item-list-overlay">
											<a
												className="wopb-templatekit-img"
												href={ data.liveurl }
												target="_blank"
												rel="noreferrer"
											>
												<img
													src={ data.image }
													loading="lazy"
													alt={ data.title }
												/>
											</a>
											<div className="wopb-list-dark-overlay">
												{ data.pro ? (
													! wopb_data.active && (
														<span className="wopb-templatekit-premium-btn">
															{ __(
																'Pro',
																'product-blocks'
															) }
														</span>
													)
												) : (
													<span className="wopb-templatekit-premium-btn wopb-templatekit-premium-free-btn">
														{ __(
															'Free',
															'product-blocks'
														) }
													</span>
												) }
												<a
													className="wopb-overlay-view"
													{ ...( ! props.hasOwnProperty(
														'initPremade'
													) && {
														href:
															'https://www.wpxpo.com/wowstore/patterns/#demoid' +
															data.ID,
														target: '_blank',
													} ) }
												>
													<span className="wopb-action-btn">
														{ props.hasOwnProperty(
															'initPremade'
														) ? (
															<>
																<span
																	onClick={ () =>
																		_changeVal(
																			data.ID,
																			data.pro
																		)
																	}
																	className="wopb-btns wopb-btn-import"
																>
																	{ __(
																		'Import',
																		'product-blocks'
																	) }
																	{ reload &&
																		reloadId ==
																			data.ID && (
																			<span className="dashicons dashicons-update rotate" />
																		) }
																</span>
																<a
																	className="wopb-btns wopb-preview"
																	href={
																		`https://www.wpxpo.com/wowstore/patterns/#demoid` +
																		data.ID
																	}
																	target="_blank"
																	rel="noreferrer"
																>
																	{ __(
																		'Preview',
																		'product-blocks'
																	) }
																</a>
															</>
														) : (
															<>
																<span className="dashicons dashicons-visibility" />{ ' ' }
																{ __(
																	'Live Preview',
																	'product-blocks'
																) }
															</>
														) }
													</span>
												</a>
											</div>
										</div>

										{ ! props.hasOwnProperty(
											'initPremade'
										) && (
											<div className="wopb-item-list-info wopb-p10">
												<span
													className="wopb-templatekit-title"
													dangerouslySetInnerHTML={ {
														__html: data.name,
													} }
												></span>
												<span className="wopb-action-btn">
													<span
														className="wopb-premade-wishlist"
														onClick={ () => {
															setWListAction(
																data.ID,
																wishListArr?.includes(
																	data.ID
																)
																	? 'remove'
																	: ''
															);
														} }
													>
														{
															IconPack[
																wishListArr?.includes(
																	data.ID
																)
																	? 'love_solid'
																	: 'love_line'
															]
														}
													</span>
													{ data.pro &&
													! wopb_data.active ? (
														<a
															className="wopb-btns wopbProBtn"
															target="_blank"
															href={
																patternsGetProUrl
															}
															rel="noreferrer"
														>
															{ __(
																'Upgrade to Pro',
																'product-blocks'
															) }
															&nbsp; &#10148;
														</a>
													) : data.pro && error ? (
														<a
															className="wopb-btn wopb-btn-sm wopb-btn-success"
															target="_blank"
															href={
																patternsGetProUrl
															}
															rel="noreferrer"
														>
															{ __(
																'Get License',
																'product-blocks'
															) }
														</a>
													) : (
														<span
															onClick={ () =>
																_changeVal(
																	data.ID,
																	data.pro
																)
															}
															className="wopb-btns wopb-btn-import"
														>
															{
																IconPack.arrow_down_line
															}
															{ __(
																'Import',
																'product-blocks'
															) }
															{ reload &&
															reloadId ==
																data.ID ? (
																<span className="dashicons dashicons-update rotate" />
															) : (
																''
															) }
														</span>
													) }
												</span>
											</div>
										) }
									</div>
								)
							);
						} ) }
					</div>
				</>
			) : loader || loading ? (
				<div className="wopb-premade-grid wopb-templatekit-col3 skeletonOverflow">
					{ Array( 6 )
						.fill( 1 )
						.map( ( v, i ) => {
							return (
								<div key={ i } className="wopb-item-list">
									<div className="wopb-item-list-overlay">
										<Skeleton
											type="custom_size"
											c_s={ {
												size1: 100,
												unit1: '%',
												size2: 300,
												unit2: 'px',
											} }
										/>
									</div>
									{ ! props.hasOwnProperty(
										'initPremade'
									) && (
										<div className="wopb-item-list-info">
											<Skeleton
												type="custom_size"
												c_s={ {
													size1: 50,
													unit1: '%',
													size2: 25,
													unit2: 'px',
													br: 2,
												} }
											/>
											<span className="wopb-action-btn">
												<span className="wopb-premade-wishlist">
													<Skeleton
														type="custom_size"
														c_s={ {
															size1: 30,
															unit1: 'px',
															size2: 25,
															unit2: 'px',
															br: 2,
														} }
													/>
												</span>
												<Skeleton
													type="custom_size"
													c_s={ {
														size1: 70,
														unit1: 'px',
														size2: 25,
														unit2: 'px',
														br: 2,
													} }
												/>
											</span>
										</div>
									) }
								</div>
							);
						} ) }
				</div>
			) : (
				<span className="wopb-image-rotate">
					{ __( 'No Data Found…', 'product-blocks' ) }
				</span>
			) }
		</div>
	);
};
export default DesignTemplate;
