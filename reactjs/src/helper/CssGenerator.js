import { getCustomFonts } from './CommonPanel';
import {
	cssBorder,
	cssBoxShadow,
	cssTypography,
	cssDimension,
	cssColor,
	cssBackgroundImage,
	cssFilter,
} from './CssHelper';

// Replace Value
const replaceData = ( selector, key, value ) => {
	return selector.replace( new RegExp( key, 'g' ), value );
};

// Object Empty Check
const isEmpty = ( obj ) => {
	return typeof obj === 'object' && Object.keys( obj ).length != 0
		? true
		: false;
};

// {{WOPB}} Replace
const replaceWarp = ( selector, ID ) => {
	selector = selector.replace(
		new RegExp( '{{ULTP}}', 'g' ),
		'.ultp-block-' + ID
	);
	return selector.replace(
		new RegExp( '{{WOPB}}', 'g' ),
		'.wopb-block-' + ID
	);
};

// {{REPEAT_CLASS}} Replace
const replaceRepeat = ( selector, ID ) => {
	return selector.replace(
		new RegExp( '{{REPEAT_CLASS}}', 'g' ),
		'.wopb-repeat-' + ID
	);
};

export const objectReplace = ( warp, value ) => {
	let output = '';
	value.forEach( ( sel ) => {
		output += sel + ';';
	} );
	return warp + '{' + output + '}';
};

export const objectAppend = ( warp, value ) => {
	let output = '';
	value.forEach( ( sel ) => {
		output += warp + sel;
	} );
	return output;
};

// Plain String/Number Field CSS Replace
export const singleField = (
	style,
	blockID,
	key,
	value,
	anotherKey = false,
	anotherKeyValue = ''
) => {
	value = typeof value !== 'object' ? value : objectField( value ).data;
	if ( typeof style === 'string' ) {
		if ( style ) {
			if ( value ) {
				let warpData = replaceWarp( style, blockID );
				if ( typeof value === 'boolean' ) {
					return [ warpData ];
				}
				if (
					warpData.indexOf( '{{' ) == -1 &&
					warpData.indexOf( '{' ) < 0
				) {
					return [ warpData + value ];
				}
				if (
					anotherKey &&
					( anotherKeyValue || anotherKeyValue == 0 )
				) {
					warpData = replaceData(
						warpData,
						'{{' + anotherKey + '}}',
						typeof anotherKeyValue === 'object'
							? ''
							: anotherKeyValue
					);
				}
				return [ replaceData( warpData, '{{' + key + '}}', value ) ];
			}
			return [];
		}
		return [ replaceWarp( value, blockID ) ]; // Custom CSS Field
	}
	const output = [];
	style.forEach( ( sel ) => {
		output.push(
			replaceData( replaceWarp( sel, blockID ), '{{' + key + '}}', value )
		);
	} );
	return output;
};

// Object Field CSS Replace
const objectField = ( data ) => {
	if ( data.openTypography ) {
		return { data: cssTypography( data ), action: 'append' }; //Typography
	} else if ( data.openBorder ) {
		return { data: cssBorder( data ), action: 'append' }; //Border
	} else if ( data.openShadow && data.color ) {
		return { data: cssBoxShadow( data ), action: 'append' }; //Shadow
	} else if (
		typeof data.top !== 'undefined' ||
		typeof data.left !== 'undefined' ||
		typeof data.right !== 'undefined' ||
		typeof data.bottom !== 'undefined'
	) {
		if ( ! data.top && ! data.right && ! data.bottom && ! data.left ) {
			return { data: '' };
		}
		return { data: cssDimension( data ), action: 'replace' }; //Dimension
	} else if ( data.openColor ) {
		if ( data.replace ) {
			return { data: cssColor( data ), action: 'replace' }; //Color Advanced
		}
		return { data: cssColor( data ), action: 'append' }; //Color Advanced
	} else if ( data.openFilter ) {
		return { data: cssFilter( data ), action: 'replace' }; //Filter
	}
	//--------------------------
	//  Wrapper Background Style Start
	//--------------------------
	else if ( data.url && data.id ) {
		return { data: cssBackgroundImage( data ), action: 'append' };
	}
	//--------------------------
	//  Wrapper Background Style End
	//--------------------------

	return { data: '', action: 'append' };
};

export const checkDepends = ( settings, selectData ) => {
	let _depends = true;
	if ( selectData.hasOwnProperty( 'depends' ) ) {
		selectData.depends.forEach( ( data ) => {
			const previous = _depends;
			if ( data.condition == '==' ) {
				if (
					typeof data.value === 'string' ||
					typeof data.value === 'number' ||
					typeof data.value === 'boolean'
				) {
					_depends = settings[ data.key ] == data.value;
				} else {
					let select = false;
					data.value.forEach( ( arrData ) => {
						if ( settings[ data.key ] == arrData ) {
							select = true;
						}
					} );
					_depends = select;
				}
			} else if ( data.condition == '!=' ) {
				if (
					typeof data.value === 'string' ||
					typeof data.value === 'number' ||
					typeof data.value === 'boolean'
				) {
					_depends = settings[ data.key ] != data.value;
				} else {
					let select = false;
					if ( Array.isArray( data.value ) ) {
						data.value.forEach( ( arrData ) => {
							if ( settings[ data.key ] != arrData ) {
								select = true;
							}
						} );
					}
					if ( select ) {
						_depends = true;
					}
				}
			}
			_depends = previous == false ? false : _depends;
		} );
	}

	return _depends;
};

export const CssGenerator = (
	settings,
	blockName,
	blockID,
	isInline = false
) => {
	if ( ! blockID ) {
		return;
	}
	let __CSS = '',
		lg = [],
		sm = [],
		xs = [],
		notResponsiveCss = [];

	let globalSettings = localStorage.getItem( 'wopbGlobal' );
	globalSettings =
		globalSettings && globalSettings != '[object Object]'
			? JSON.parse( globalSettings )
			: {};

	Object.keys( settings ).forEach( ( key ) => {
		const attributes =
			typeof blockName === 'string'
				? wp.blocks.getBlockType( blockName ).attributes
				: blockName;
		const anotherKey = attributes[ key ]?.anotherKey;
		const anotherKeyValue = settings[ anotherKey ];

		if (
			attributes[ key ] &&
			attributes[ key ].hasOwnProperty( 'style' )
		) {
			attributes[ key ].style.forEach( ( selectData, indexStyle ) => {
				if ( selectData.hasOwnProperty( 'selector' ) ) {
					const cssSelecor = selectData.selector;
					if ( checkDepends( settings, selectData ) ) {
						if ( typeof settings[ key ] === 'object' ) {
							let device = false;
							let dimension = '';
							if ( settings[ key ].lg ) {
								// Exta Large
								device = true;
								dimension =
									typeof settings[ key ].lg === 'object'
										? objectField( settings[ key ].lg ).data
										: settings[ key ].lg +
										  ( settings[ key ].ulg ||
												settings[ key ].unit ||
												'' );
								if ( dimension ) {
									lg = lg.concat(
										singleField(
											cssSelecor,
											blockID,
											key,
											dimension,
											anotherKey,
											typeof anotherKeyValue === 'object'
												? anotherKeyValue?.lg
												: anotherKeyValue
										)
									);
								}
							}
							if ( settings[ key ].sm ) {
								// Tablet
								device = true;
								dimension =
									typeof settings[ key ].sm === 'object'
										? objectField( settings[ key ].sm ).data
										: settings[ key ].sm +
										  ( settings[ key ].usm ||
												settings[ key ].unit ||
												'' );
								if ( dimension ) {
									sm = sm.concat(
										singleField(
											cssSelecor,
											blockID,
											key,
											dimension,
											anotherKey,
											typeof anotherKeyValue === 'object'
												? anotherKeyValue?.sm
												: anotherKeyValue
										)
									);
								}
							}
							if ( settings[ key ].xs ) {
								// Phone
								device = true;
								dimension =
									typeof settings[ key ].xs === 'object'
										? objectField( settings[ key ].xs ).data
										: settings[ key ].xs +
										  ( settings[ key ].uxs ||
												settings[ key ].unit ||
												'' );
								if ( dimension ) {
									xs = xs.concat(
										singleField(
											cssSelecor,
											blockID,
											key,
											dimension,
											anotherKey,
											typeof anotherKeyValue === 'object'
												? anotherKeyValue?.xs
												: anotherKeyValue
										)
									);
								}
							}
							if ( ! device ) {
								// Object Field Type Only
								const objectCss = objectField(
									settings[ key ]
								);
								const repWarp = replaceWarp(
									cssSelecor,
									blockID
								);
								if ( typeof objectCss.data === 'object' ) {
									if (
										Object.keys( objectCss.data ).length !=
										0
									) {
										if ( objectCss.data.background ) {
											notResponsiveCss.push(
												repWarp +
													objectCss.data.background
											);
										}

										// Typography
										if ( isEmpty( objectCss.data.lg ) ) {
											lg.push(
												objectReplace(
													repWarp,
													objectCss.data.lg
												)
											);
										}
										if ( isEmpty( objectCss.data.sm ) ) {
											sm.push(
												objectReplace(
													repWarp,
													objectCss.data.sm
												)
											);
										}
										if ( isEmpty( objectCss.data.xs ) ) {
											xs.push(
												objectReplace(
													repWarp,
													objectCss.data.xs
												)
											);
										}
										if ( objectCss.data.simple ) {
											notResponsiveCss.push(
												repWarp + objectCss.data.simple
											);
										}
										if ( objectCss.data.font ) {
											lg.unshift( objectCss.data.font );
										}

										if ( objectCss.data.shape ) {
											objectCss.data.shape.forEach(
												function ( el ) {
													notResponsiveCss.push(
														repWarp + el
													);
												}
											);
											if (
												isEmpty(
													objectCss.data.data.lg
												)
											) {
												lg.push(
													objectAppend(
														repWarp,
														objectCss.data.data.lg
													)
												);
											}
											if (
												isEmpty(
													objectCss.data.data.sm
												)
											) {
												sm.push(
													objectAppend(
														repWarp,
														objectCss.data.data.sm
													)
												);
											}
											if (
												isEmpty(
													objectCss.data.data.xs
												)
											) {
												xs.push(
													objectAppend(
														repWarp,
														objectCss.data.data.xs
													)
												);
											}
										}
									}
								} else if (
									objectCss.data &&
									objectCss.data.indexOf( '{{' ) == -1
								) {
									if ( objectCss.action == 'append' ) {
										notResponsiveCss.push(
											repWarp + objectCss.data
										);
									} else {
										notResponsiveCss.push(
											singleField(
												cssSelecor,
												blockID,
												key,
												objectCss.data,
												anotherKey,
												anotherKeyValue
											)
										);
									}
								}
							}
						} else if ( key == 'hideExtraLarge' ) {
							if ( isInline ) {
								notResponsiveCss = notResponsiveCss.concat(
									'@media (min-width: ' +
										( globalSettings.breakpointSm ||
											1280 ) +
										'px) {' +
										singleField(
											cssSelecor,
											blockID,
											key,
											settings[ key ],
											anotherKey,
											anotherKeyValue
										) +
										'}'
								);
							}
						} else if ( key == 'hideDesktop' ) {
							if ( isInline ) {
								notResponsiveCss = notResponsiveCss.concat(
									'@media only screen and (max-width: ' +
										( globalSettings.breakpointSm ||
											1280 ) +
										'px) and (min-width: 992px) {' +
										singleField(
											cssSelecor,
											blockID,
											key,
											settings[ key ]
										) +
										'}'
								);
							}
						} else if ( key == 'hideTablet' ) {
							if ( isInline ) {
								notResponsiveCss = notResponsiveCss.concat(
									'@media only screen and (max-width: ' +
										( globalSettings.breakpointSm || 992 ) +
										'px) and (min-width: 767px) {' +
										singleField(
											cssSelecor,
											blockID,
											key,
											settings[ key ],
											anotherKey,
											anotherKeyValue
										) +
										'}'
								);
							}
						} else if ( key == 'hideMobile' ) {
							if ( isInline ) {
								notResponsiveCss = notResponsiveCss.concat(
									'@media (max-width: ' +
										( globalSettings.breakpointXs || 768 ) +
										'px) {' +
										singleField(
											cssSelecor,
											blockID,
											key,
											settings[ key ],
											anotherKey,
											anotherKeyValue
										) +
										'}'
								);
							}
						} else if ( settings[ key ] ) {
							notResponsiveCss = notResponsiveCss.concat(
								singleField(
									cssSelecor,
									blockID,
									key,
									settings[ key ],
									anotherKey,
									anotherKeyValue
								)
							);
						}
					}
				}
			} );
		} else {
			// Repetable Fields
			if ( attributes[ key ] && attributes[ key ].type == 'array' ) {
				if ( attributes[ key ].hasOwnProperty( 'fields' ) ) {
					Object.keys( attributes[ key ].fields ).forEach( ( k ) => {
						if (
							attributes[ key ].fields[ k ].hasOwnProperty(
								'style'
							)
						) {
							attributes[ key ].fields[ k ].style.forEach(
								( data, _k ) => {
									if ( data.hasOwnProperty( 'selector' ) ) {
										if (
											Array.isArray( settings[ key ] )
										) {
											settings[ key ].forEach(
												( el, __k ) => {
													let repWarp = replaceWarp(
														data.selector,
														blockID
													);
													repWarp = replaceRepeat(
														repWarp,
														__k
													);
													repWarp = replaceData(
														repWarp,
														'{{' + k + '}}',
														el[ k ]
													);
													notResponsiveCss.push(
														repWarp
													);
												}
											);
										}
									}
								}
							);
						}
					} );
				}
			}
		}
	} );

	// Join CSS
	if ( lg.length > 0 ) {
		__CSS += lg.join( '' );
	} //lg
	if ( sm.length > 0 ) {
		__CSS +=
			'@media (max-width: ' +
			( globalSettings.breakpointSm || 992 ) +
			'px) {' +
			sm.join( '' ) +
			'}';
	} //sm
	if ( xs.length > 0 ) {
		__CSS +=
			'@media (max-width: ' +
			( globalSettings.breakpointXs || 768 ) +
			'px) {' +
			xs.join( '' ) +
			'}';
	} //xs
	if ( notResponsiveCss.length > 0 ) {
		__CSS += notResponsiveCss.join( '' );
	}

	if ( isInline ) {
		if (
			wopb_data.settings?.wopb_custom_font == 'true' &&
			wopb_data.active
		) {
			return getCustomFonts( __CSS, true );
		}
		return __CSS;
	}

	// set custom font
	if ( wopb_data.settings?.wopb_custom_font == 'true' ) {
		__CSS = getCustomFonts( __CSS, true );
	}

	// Set CSS
	setDevice( __CSS, blockID );
};

// Check Device
const setDevice = ( styleCss, blockID ) => {
	const currentDevice = wp.data.select( 'core/edit-post' );
	if (
		currentDevice ||
		document.body.classList.contains( 'site-editor-php' )
	) {
		// For FSE Checking
		const editor_canvas =
			window.document.getElementsByName( 'editor-canvas' );
		if ( editor_canvas[ 0 ]?.contentDocument ) {
			setTimeout( function () {
				setStyle(
					editor_canvas[ 0 ]?.contentDocument,
					styleCss,
					blockID
				);
			}, 0 );
		} else {
			setStyle( window.document, styleCss, blockID );
		}
	} else {
		setStyle( window.document, styleCss, blockID );
	}
};

// Set CSS to Head
const setStyle = ( styleSelector, styleCss, blockID ) => {
	styleSelector = styleSelector || window.document;
	if ( styleSelector.getElementById( 'wopb-block-' + blockID ) === null ) {
		const cssInline = document.createElement( 'style' );
		cssInline.type = 'text/css';
		cssInline.id = 'wopb-block-' + blockID;
		if ( cssInline.styleSheet ) {
			cssInline.styleSheet.cssText = styleCss;
		} else {
			cssInline.innerHTML = styleCss;
		}
		styleSelector
			.getElementsByTagName( 'head' )[ 0 ]
			.appendChild( cssInline );
	} else {
		styleSelector.getElementById( 'wopb-block-' + blockID ).innerHTML =
			styleCss;
	}
};
