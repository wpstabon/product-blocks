import { createRoot } from 'react-dom/client';
import Panel from '../dashboard/builder/Panel';
import TemplatesKit from '../dashboard/templateskit/TemplatesKit';

if (
	document.body.contains(
		document.getElementById( 'wopb-dashboard-templatekit' )
	)
) {
	const container = document.getElementById( 'wopb-dashboard-templatekit' );
	const root = createRoot( container );
	root.render( <TemplatesKit /> );
}

if ( document.querySelector( '.block-editor-page' ) ) {
	const { subscribe } = wp.data;
	const unsubscribe = subscribe( () => {
		let toolbar = document.querySelector( '.edit-post-header__toolbar' );
		if ( ! toolbar ) {
			toolbar = document.querySelector( '.edit-post-header-toolbar' );
			if ( ! toolbar ) {
				return;
			}
		}
		if (
			wopb_condition.builder_type == 'single-product' ||
			wopb_condition.builder_type == 'single_product' ||
			wopb_condition.builder_type == 'archive' ||
			wopb_condition.builder_type == 'header' ||
			wopb_condition.builder_type == 'footer'
		) {
			const buttonDiv = document.createElement( 'div' );
			buttonDiv.className = 'sab-toolbar-insert-layout';
			buttonDiv.innerHTML =
				'<button id="WopbConditionButton" class="wopb-popup-button" aria-label="Insert Layout"><span class="dashicons dashicons-admin-settings"></span>Condition</button>';
			toolbar.appendChild( buttonDiv );
		}

		// Builder Back Button
		setTimeout( function () {
			if (
				typeof document.getElementsByClassName(
					'edit-post-fullscreen-mode-close'
				)[ 0 ] !== 'undefined'
			) {
				document.getElementsByClassName(
					'edit-post-fullscreen-mode-close'
				)[ 0 ].href = wopb_condition.builder_url;
			}
		}, 0 );

		let isDisable = 1;

		function popupCondition() {
			if ( isDisable ) {
				const node = document.createElement( 'div' );
				node.id = 'wopb-modal-conditions';
				node.className = 'wopb-builder-modal wopb-blocks-layouts';
				document.body.appendChild( node );
				const root = createRoot( node );
				root.render( <Panel notEditor={ 'yes' } /> );
				isDisable = 0;
				setTimeout( function () {
					isDisable = 1;
				}, 2000 );
			}
		}
		if (
			typeof document.getElementsByClassName(
				'editor-post-publish-button__button editor-post-publish-panel__toggle'
			)[ 0 ] !== 'undefined'
		) {
			if (
				wopb_condition.builder_type == 'single-product' ||
				wopb_condition.builder_type == 'single_product' ||
				wopb_condition.builder_type == 'archive' ||
				wopb_condition.builder_type == 'header' ||
				wopb_condition.builder_type == 'footer'
			) {
				popupCondition();
			}
		}

		if (
			wopb_condition.builder_type == 'single-product' ||
			wopb_condition.builder_type == 'single_product' ||
			wopb_condition.builder_type == 'archive' ||
			wopb_condition.builder_type == 'header' ||
			wopb_condition.builder_type == 'footer'
		) {
			setTimeout( function () {
				document
					.getElementById( 'WopbConditionButton' )
					.addEventListener( 'click', function () {
						popupCondition();
					} );
			} );
		}
		unsubscribe();
	} );
}
