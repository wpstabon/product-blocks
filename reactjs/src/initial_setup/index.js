import { createRoot } from 'react-dom/client';
import Main from './Main';
import '../dashboard/dashboard.scss';

if (
	document.body.contains( document.getElementById( 'wopb-initial-setting' ) )
) {
	const container = document.getElementById( 'wopb-initial-setting' );
	const root = createRoot( container );
	root.render( <Main /> );
}
