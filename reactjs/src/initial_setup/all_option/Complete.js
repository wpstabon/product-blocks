import { useState } from 'react';
const { __ } = wp.i18n;

const helpInfo = [
	{
		icon: 'explore_pro.svg',
		label: __( 'Explore All Features', 'product-blocks' ),
		link: 'https://www.wpxpo.com/wowstore/features/?utm_source=db-wstore-wizard&utm_medium=explore-features&utm_campaign=wstore-dashboard',
	},
	{
		icon: 'knowledge_base.svg',
		label: __( 'WowStore Documentation', 'product-blocks' ),
		link: 'https://wpxpo.com/docs/wowstore/?utm_source=db-wstore-wizard&utm_medium=wstore-doc&utm_campaign=wstore-dashboard',
	},
	{
		icon: 'forum_support.svg',
		label: __( 'Forum Support', 'product-blocks' ),
		link: 'https://wordpress.org/support/plugin/product-blocks/',
	},
	{
		icon: 'ticket_support.svg',
		label: __( 'Pre-Sale & Pro Support', 'product-blocks' ),
		link: 'https://www.wpxpo.com/contact/?utm_source=db-wstore-wizard&utm_medium=ticket-support&utm_campaign=wstore-dashboard',
	},
];
const socialMediaHandles = [
	{
		icon: '',
		label: __( 'Join the Community to Stay Up to Date', 'product-blocks' ),
		link: 'https://www.facebook.com/groups/wowcommercecommunity',
	},
];

const Complete = ( { siteArgs, setSiteArgs, sendPluginData } ) => {
	const [ playVideo, setPlayVideo ] = useState( false );
	const [ isLoading, setloading ] = useState( false );
	return (
		<div className="completePage">
			<div className="headerCon">
				<img
					className="ready_image"
					src={ setup_wizard.url + '/assets/img/wizard/congrats.jpg' }
					alt={ __( 'WowStore', 'product-blocks' ) }
				/>
				<h3 className="completeTitle">
					{ __( 'You are Ready!', 'product-blocks' ) }
				</h3>
				<span className="wopb-description">
					{ __(
						'You are ready to start your online journey - Deploy your stunning WooCommerce Store with WowStore Today.',
						'product-blocks'
					) }
				</span>
				<div className="completeStep">
					<a
						className="wopb-primary-button cursor readyButton"
						onClick={ () => {
							sendPluginData();
							setloading( true );
						} }
					>
						<span>{ __( 'Ready to Go', 'product-blocks' ) }</span>
						<span
							className={
								isLoading
									? 'wopb-loading'
									: 'dashicons dashicons-arrow-right-alt2'
							}
						/>
					</a>
				</div>
			</div>
			<div className="completeContent">
				<div className="contentLeft">
					<div className="intro-video">
						<img
							className="thumbnail"
							src={
								setup_wizard.url +
								'/assets/img/dashboard_right_banner.jpg'
							}
							alt={ __(
								'WowStore Intro Video',
								'product-blocks'
							) }
						/>
						{
							<div
								className="wopb-play-icon-container"
								onClick={ () => {
									setPlayVideo( true );
								} }
							>
								<img
									className="wopb-play-icon"
									src={
										setup_wizard.url +
										'/assets/img/wizard/play.png'
									}
									alt={ __( 'Play', 'product-blocks' ) }
								/>
								<span className="animate"></span>
							</div>
						}
						{ playVideo && (
							<iframe
								className="video-frame"
								src={ `https://www.youtube.com/embed/6t9JSvAR-to?autoplay=1` }
								frameBorder="0"
								allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; fullscreen"
								title="Embedded youtube"
							/>
						) }
					</div>
					<div className="inputFields">
						<div className="completepage">
							<input
								id={ 'get_updates' }
								name={ 'get_updates' }
								type="checkbox"
								defaultChecked={
									siteArgs.get_updates &&
									siteArgs.get_updates == 'yes'
										? true
										: false
								}
								onChange={ ( e ) => {
									let _final;
									_final = e.target.checked ? 'yes' : 'no';
									setSiteArgs( {
										...siteArgs,
										get_updates: _final,
									} );
								} }
							/>
							<span>
								<span>
									{ __( 'Get Updates', 'product-blocks' ) }
								</span>
								<span className="fieldDesc">
									{ __(
										'We will send essential tips & tricks for effective usage of WowStore.',
										'product-blocks'
									) }
								</span>
							</span>
						</div>
						<div>
							<input
								id={ 'data_share_consent' }
								name={ 'data_share_consent' }
								type="checkbox"
								defaultChecked={
									siteArgs.data_share_consent &&
									siteArgs.data_share_consent == 'yes'
										? true
										: false
								}
								onChange={ ( e ) => {
									let _final;
									_final = e.target.checked ? 'yes' : 'no';
									setSiteArgs( {
										...siteArgs,
										data_share_consent: _final,
									} );
								} }
							/>
							<span>
								<span>
									{ __(
										'Share Essentials',
										'product-blocks'
									) }
								</span>
								<span className="fieldDesc">
									{ __(
										'Let us collect non-sensitive diagnosis data and usage information.',
										'product-blocks'
									) }
								</span>
							</span>
						</div>
					</div>
				</div>
				<div className="contentRight">
					<div className="wopb-learn-more">
						{ __( 'Learn More', 'product-blocks' ) }
					</div>
					<ul className="learn-more-options">
						{ helpInfo.map( ( option, index ) => {
							return (
								<li key={ index }>
									<span
										className="learn-more-option"
										onClick={ () => {
											window.open(
												option.link,
												'_blank'
											);
										} }
									>
										<img
											className="learn-more-option-icon"
											src={
												setup_wizard.url +
												'assets/img/wizard/' +
												option.icon
											}
											alt={ option.label }
										/>{ ' ' }
										<span className="learn-more-option-label">
											{ option.label }
										</span>{ ' ' }
									</span>
								</li>
							);
						} ) }
					</ul>
					{ socialMediaHandles.length &&
						socialMediaHandles.map( ( socialMedia, index ) => {
							return (
								<button
									key={ index }
									className="social-media-button"
									onClick={ () => {
										window.open(
											socialMedia.link,
											'_blank'
										);
									} }
								>
									<span className="dashicons dashicons-facebook"></span>
									<span className="social-media-button-label">
										{ socialMedia.label }
									</span>
								</button>
							);
						} ) }
				</div>
			</div>
		</div>
	);
};

export default Complete;
