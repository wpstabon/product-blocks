const { __ } = wp.i18n;

const featuresData = [
	{
		label: __( 'Flexible Shopping Experience', 'product-blocks' ),
		link: 'https://www.wpxpo.com/wowstore/features/#wopb_flexible_sopping/?utm_source=db-wstore-wizard&utm_medium=features_flexible-shopping&utm_campaign=wstore-dashboard',
	},
	{
		label: __( 'Sales Boosters', 'product-blocks' ),
		link: 'https://www.wpxpo.com/wowstore/features/#wopb_boost_sales/?utm_source=db-wstore-wizard&utm_medium=features_boost-sales&utm_campaign=wstore-dashboard',
	},
	{
		label: __( 'Comprehensive Blocks Library', 'product-blocks' ),
		link: 'https://www.wpxpo.com/wowstore/blocks/?utm_source=db-wstore-wizard&utm_medium=features_blocks&utm_campaign=wstore-dashboard',
	},
	{
		label: __( 'Rich Premade Templates', 'product-blocks' ),
		link: 'https://www.wpxpo.com/wowstore/templates/?utm_source=db-wstore-wizard&utm_medium=features_templates&utm_campaign=wstore-dashboard',
	},
	{
		label: __( 'Cart Abandonment Reduction', 'product-blocks' ),
		link: 'https://www.wpxpo.com/wowstore/features/#wopb_reduce_abandon/?utm_source=db-wstore-wizard&utm_medium=features_cart-abandon&utm_campaign=wstore-dashboard',
	},
	{
		label: __( 'Powerful Woo Builder', 'product-blocks' ),
		link: 'https://www.wpxpo.com/wowstore/woocommerce-builder/?utm_source=db-wstore-wizard&utm_medium=features_woo-builder&utm_campaign=wstore-dashboard',
	},
	{
		label: __( 'AOV Boosters', 'product-blocks' ),
		link: 'https://www.wpxpo.com/wowstore/features/#wopb_increase_order/?utm_source=db-wstore-wizard&utm_medium=features_AOVup&utm_campaign=wstore-dashboard',
	},
	{
		label: __( 'Increased Conversions', 'product-blocks' ),
		link: 'https://www.wpxpo.com/wowstore/features/#wopb_increase_conversion/?utm_source=db-wstore-wizard&utm_medium=features_conversions&utm_campaign=wstore-dashboard',
	},
];

const CoreFeatures = () => {
	return (
		<div className="coreFeatures">
			<div className="headerCon">
				<div className="wopb-headecon-title">
					{ __( 'Enjoy Exclusive Features!', 'product-blocks' ) }
				</div>
				<span className="wopb-description">
					{ __(
						'Take advantage of the Exclusive Features and build functional WooCommerce Stores - Build Better, Sell Better!',
						'product-blocks'
					) }
				</span>
			</div>
			<div>
				<div className="wopb-feature-group wopb-addons">
					<div className="wopb-feature-group-label">
						{ __(
							'Most Advanced and Conversion Focused Features',
							'product-blocks'
						) }
					</div>
					<div className="wopb-feature-section">
						<ul>
							{ featuresData.map( ( data, key ) => {
								return (
									<li key={ key }>
										<a
											className="wopb-core-feature"
											href={ data.link }
											target="_blank"
											rel="noreferrer"
										>
											<span className="wopb-feature-name">
												{ data.label }
											</span>
										</a>
									</li>
								);
							} ) }
						</ul>
					</div>
				</div>
			</div>
		</div>
	);
};

export default CoreFeatures;
