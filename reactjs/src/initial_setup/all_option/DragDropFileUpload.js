import React, { useState } from 'react';
import './dropFileUpload.scss';
const { __ } = wp.i18n;

const DragDropFileUpload = ( { label, help, onChange, name } ) => {
	const [ dragActive, setDragActive ] = useState( false );
	const [ fileName, setFileName ] = useState( '' );

	// handle drag events
	const handleDrag = function ( e ) {
		e.preventDefault();
		e.stopPropagation();
		if ( e.type === 'dragenter' || e.type === 'dragover' ) {
			setDragActive( true );
		} else if ( e.type === 'dragleave' ) {
			setDragActive( false );
		}
	};

	// triggers when file is dropped
	const handleDrop = function ( e ) {
		e.preventDefault();
		e.stopPropagation();
		setDragActive( false );
		if ( e.dataTransfer.files && e.dataTransfer.files[ 0 ] ) {
			setFileName( e.dataTransfer.files[ 0 ].name );
			onChange( e.dataTransfer.files[ 0 ] );
		}
	};

	const fileOnChange = ( e ) => {
		setFileName( e.target.files[ 0 ].name );
		onChange( e.target.files[ 0 ] );
	};

	return (
		<div className="wopb_drag_drop_file_upload">
			<label className="wopb_drag_drop_file_upload__label">
				{ label }
			</label>
			<input
				type="file"
				id="wopb_file_upload"
				name={ name }
				onChange={ fileOnChange }
			></input>
			<label
				htmlFor="wopb_file_upload"
				className={ `wopb_drag_drop_file_upload__content_wrapper ${
					dragActive ? 'wopb_file_upload__drag_active' : ''
				}` }
				onDragEnter={ handleDrag }
				onDragLeave={ handleDrag }
				onDragOver={ handleDrag }
				onDrop={ handleDrop }
			>
				<div className="wopb_drag_drop_file_upload__content">
					{ fileName === '' && (
						<>
							<span className="dashicons dashicons-cloud-upload wopb_cloud_upload_icon wopb_icon"></span>
							<span className="wopb_drag_drop_file_upload__text">
								{ __( 'Drop File Here or', 'product-blocks' ) }
								<span className="wopb_link_text">
									{ ' ' }
									{ __(
										'Click to upload',
										'product-blocks'
									) }
								</span>
							</span>
						</>
					) }
					{ fileName && (
						<span className="wopb_drag_drop_file_upload__file_name">
							{ fileName }
						</span>
					) }
				</div>
			</label>
			{ help && <div className="wopb_help_message">{ help }</div> }
		</div>
	);
};

export default DragDropFileUpload;
