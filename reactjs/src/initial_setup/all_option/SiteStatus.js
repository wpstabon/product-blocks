const { __ } = wp.i18n;

const SiteStatus = ( props ) => {
	const { siteArgs, setSiteArgs } = props;
	const installationNotice = () => {
		const _woocommerce = siteArgs.install_woocommerce === 'yes';
		const _wow_revenue = siteArgs.install_revenue === 'yes';
		let _message = '';
		if ( _woocommerce && _wow_revenue ) {
			_message = `<span className='installable-plugin'>${ __(
				'WooCommerce',
				'product-blocks'
			) }
			</span> & 
			<span className='installable-plugin'>
				<a 
					class="wob-revenue-link"
					href="https://www.wowrevenue.com?utm_source=wstore-setup&utm_medium=site_status-wowrevenue&utm_campaign=wstore-wizard"
				>
					${ __(
						'WowRevenue',
						'product-blocks'
					) }
				</a>
			</span>`;
		} else if ( _woocommerce ) {
			_message = `
				<span className='installable-plugin'>
					${ __(
						'WooCommerce',
						'product-blocks'
					) }
				</span>`;
		} else if ( _wow_revenue ) {
			_message = `
				<span className='installable-plugin'> 
					<a
						class="wob-revenue-link" 
						href="https://www.wowrevenue.com?utm_source=wstore-setup&utm_medium=site_status-wowrevenue&utm_campaign=wstore-wizard"
					> 
						${ __(
							'WowRevenue',
							'product-blocks'
						) }
					</a>
				</span>`;
		}
		if ( _woocommerce || _wow_revenue ) {
			return (
				__(
					'The following plugins will be installed and activated for you:',
					'product-blocks'
				) + _message
			);
		}
	};

	return (
		<div>
			<div className="headerCon">
				<div className="wopb-headecon-title">
					{ __(
						'Tell Us About Your Dream Store…',
						'product-blocks'
					) }
				</div>
				<span className="wopb-description ">
					{ __(
						'What type of store do you want to build?',
						'product-blocks'
					) }
				</span>
			</div>
			<div className="siteStatusData">
				<div>
					<label className="wopb_aster">
						{ __( 'Site Type', 'product-blocks' ) }
					</label>
					<select
						onChange={ ( e ) =>
							setSiteArgs( {
								...siteArgs,
								siteType: e.target.value,
							} )
						}
					>
						<option value="">
							{ __( 'Select Site Type', 'product-blocks' ) }
						</option>
						<option value="clothing">
							{ __( 'Clothing', 'product-blocks' ) }
						</option>
						<option value="fashion">
							{ __( 'Fashion', 'product-blocks' ) }
						</option>
						<option value="fast_food">
							{ __( 'Fast Food', 'product-blocks' ) }
						</option>
						<option value="jewelry">
							{ __( 'Jewelry', 'product-blocks' ) }
						</option>
						<option value="sunglass">
							{ __( 'Sunglass', 'product-blocks' ) }
						</option>
						<option value="furniture">
							{ __( 'Furniture', 'product-blocks' ) }
						</option>
						<option value="home_decor">
							{ __( 'Home Decor', 'product-blocks' ) }
						</option>
						<option value="backpack">
							{ __( 'Backpack', 'product-blocks' ) }
						</option>
						<option value="others">
							{ __( 'Other', 'product-blocks' ) }
						</option>
					</select>
				</div>
				<div className="inputFields">
					<div>
						<input
							id={ 'install_woocommerce' }
							name={ 'install_woocommerce' }
							type="checkbox"
							defaultChecked={
								siteArgs.install_woocommerce &&
								siteArgs.install_woocommerce == 'yes'
									? true
									: false
							}
							onChange={ ( e ) => {
								let _final;
								_final = e.target.checked ? 'yes' : 'no';
								setSiteArgs( {
									...siteArgs,
									install_woocommerce: _final,
								} );
							} }
						/>
						<span>
							<span>
								{ __( 'WooCommerce', 'product-blocks' ) }
							</span>
						</span>
					</div>
					<div>
						<input
							id={ 'install_revenue' }
							name={ 'install_revenue' }
							type="checkbox"
							defaultChecked={
								siteArgs.install_revenue &&
								siteArgs.install_revenue == 'yes'
									? true
									: false
							}
							onChange={ ( e ) => {
								let _final;
								_final = e.target.checked ? 'yes' : 'no';
								setSiteArgs( {
									...siteArgs,
									install_revenue: _final,
								} );
							} }
						/>
						<span>
							<span>
								{ __(
									'WowRevenue',
									'product-blocks'
								) }
							</span>
						</span>
					</div>
				</div>
				<span
					className="productx-wizard-installation-notice"
					dangerouslySetInnerHTML={ { __html: installationNotice() } }
				></span>
			</div>
		</div>
	);
};

export default SiteStatus;
