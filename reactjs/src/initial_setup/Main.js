const { __ } = wp.i18n;
import { useState } from 'react';
import './wizard.scss';
import CoreFeatures from './all_option/CoreFeatures';
import Complete from './all_option/Complete';
import SiteStatus from './all_option/SiteStatus';

const tabItems = {
	welcome: {
		label: __( 'Welcome', 'product-blocks' ),
		class: 'wopb-welcome ',
	},
	siteStatus: {
		label: __( 'Site Info', 'product-blocks' ),
		class: 'wopb-site-status ',
	},
	coreFeatures: {
		label: __( 'Exclusive Features', 'product-blocks' ),
		class: 'wopb-coreFeatures ',
	},
	ready: {
		label: __( 'Congratulations', 'product-blocks' ),
		class: 'wopb-complete',
	},
};

const tabItemKeys = Object.keys( tabItems );

const Main = () => {
	const url = new URL( window.location );
	const [ loader, setLoader ] = useState( false );
	const [ siteStatusLoader, setSiteStatusLoader ] = useState( false );
	// Current Tab
	const [ currentTab, changeCurrentTab ] = useState(
		url.searchParams.get( 'tab' )
			? url.searchParams.get( 'tab' )
			: 'welcome'
	);
	const [ siteArgs, setSiteArgs ] = useState( {
		siteType: '',
		data_share_consent: 'yes',
		get_updates: 'yes',
		install_woocommerce: 'yes',
		install_revenue: 'yes',
	} );

	url.searchParams.set( 'tab', currentTab );
	history.pushState( '', '', url );

	const sendPluginData = () => {
		if (
			currentTab == 'ready' &&
			( siteArgs.data_share_consent == 'yes' ||
				siteArgs.get_updates == 'yes' )
		) {
			wp.apiFetch( {
				path: '/wopb/v2/wizard_action',
				method: 'POST',
				data: {
					wpnonce: setup_wizard.security,
					site: siteArgs.siteType,
					action: 'send',
				},
			} ).then( ( res ) => {
				if ( res ) {
					completeSetup();
				}
			} );
		}
	};

	const saveSiteStatus = () => {
		if ( ! siteStatusLoader ) {
			setSiteStatusLoader( true );
			const formData = new FormData();
			formData.append( 'wpnonce', setup_wizard.security );
			formData.append( 'action', 'install' );

			if ( siteArgs.siteType ) {
				formData.append( 'siteType', siteArgs.siteType );
			}
			if ( siteArgs.install_woocommerce ) {
				formData.append(
					'install_woocommerce',
					siteArgs.install_woocommerce
				);
			}
			if ( siteArgs.install_revenue ) {
				formData.append(
					'install_revenue',
					siteArgs.install_revenue
				);
			}

			wp.apiFetch( {
				path: '/wopb/v2/wizard_action',
				method: 'POST',
				body: formData,
			} ).then( ( res ) => {
				if ( res ) {
					setSiteStatusLoader( false );
				}
			} );
		}
	};

	const completeSetup = () => {
		window.location.href = setup_wizard.redirect;
	};

	const Welcome = () => {
		return (
			<>
				<h1 className="wopb-title">
					{ __( 'Welcome!', 'product-blocks' ) }
				</h1>
				<span className="wopb-subtitle wopb_st1">
					{ __( 'Start Building with ', 'product-blocks' ) }
					<span>{ __( 'WowStore!', 'product-blocks' ) }</span>
				</span>
				<span className="wopb-subtitle wopb_st2">
					{ __(
						'WowStore is the #1 Gutenberg WooCommerce Builder for building Feature-rich, Conversion-focused, Visually Agile Online Stores',
						'product-blocks'
					) }
				</span>
				<span className="wopb-short-desc">
					{ __(
						'It only takes 4-steps to get started.',
						'product-blocks'
					) }
				</span>
				<a
					className="wopb-primary-button cursor letsStartButton"
					onClick={ () =>
						changeCurrentTab(
							tabItemKeys[ tabItemKeys.indexOf( currentTab ) + 1 ]
						)
					}
				>
					<span>{ __( "Lets's Start", 'product-blocks' ) }</span>
					<span className="dashicons dashicons-arrow-right-alt2" />
				</a>
			</>
		);
	};

	const Pagination = () => {
		return (
			<div className="wopb-pagination">
				<div>
					{ currentTab &&
						currentTab != 'welcome' &&
						currentTab != 'ready' && (
							<a
								className="wopb-secondary-button"
								onClick={ () => {
									window.scrollTo( {
										top: 0,
										behavior: 'smooth',
									} );
									changeCurrentTab(
										tabItemKeys[
											tabItemKeys.indexOf( currentTab ) -
												1
										]
									);
								} }
							>
								<span className="dashicons dashicons-arrow-left-alt2" />
								<span>
									{ __( 'Back', 'product-blocks' ) }
								</span>
							</a>
						) }
					{ currentTab &&
						currentTab != 'ready' &&
						currentTab != 'welcome' && (
							<a
								className="wopb-primary-button "
								onClick={ () => {
									if ( currentTab == 'siteStatus' ) {
										saveSiteStatus();
									}
									window.scrollTo( {
										top: 0,
										behavior: 'smooth',
									} );
									changeCurrentTab(
										tabItemKeys[
											tabItemKeys.indexOf( currentTab ) +
												1
										]
									);
								} }
							>
								<span>{ __( 'Next', 'product-blocks' ) }</span>
								<span className="dashicons dashicons-arrow-right-alt2" />
							</a>
						) }
				</div>
			</div>
		);
	};

	return (
		<div>
			<div className="wopb-setting-header">
				<div className="wopb-setting-logo">
					<img
						className="wopb-setting-header-img"
						loading="lazy"
						src={ setup_wizard.url + '/assets/img/logo-option.svg' }
						alt="WowStore"
					/>
					<span>{ setup_wizard.version }</span>
				</div>
			</div>
			<div className="wopb-setup-wizard-container">
				<div className={ loader ? 'wopb-processing' : '' }>
					<div className="wopb-header">
						<div className="progressbar">
							<ul
								className={ `wopb-tab tab-${
									tabItemKeys.indexOf( currentTab ) + 1
								}` }
							>
								{ tabItemKeys.map( ( key, index ) => {
									index = index + 1;
									const item = tabItems[ key ];
									return (
										<li
											onClick={ () =>
												changeCurrentTab( key )
											}
											className={ `cursor progress-step-wrapper ${
												currentTab &&
												tabItemKeys.indexOf(
													currentTab
												) +
													1 >=
													index
													? 'wopb-tab-pass'
													: ''
											} ` }
											key={ key }
										>
											<div className="progress-step-style"></div>
											<div className="progress-step-label-container">
												<a
													className="progress-step-label"
													onClick={ () =>
														changeCurrentTab( key )
													}
												>
													<span>{ item.label }</span>
												</a>
											</div>
										</li>
									);
								} ) }
							</ul>
						</div>
					</div>

					<div className="wopb-content wopb-settings-tab-wrap">
						<ul className="wopb-content-items ">
							<li className={ tabItems[ currentTab ].class }>
								{ currentTab == 'welcome' && <Welcome /> }
								{ currentTab == 'siteStatus' && (
									<SiteStatus
										siteArgs={ siteArgs }
										setSiteArgs={ setSiteArgs }
									/>
								) }
								{ currentTab == 'coreFeatures' && (
									<CoreFeatures />
								) }
								{ currentTab == 'ready' && (
									<Complete
										sendPluginData={ sendPluginData }
										siteArgs={ siteArgs }
										setSiteArgs={ setSiteArgs }
									/>
								) }
							</li>
							<Pagination />
						</ul>
					</div>
					<div className="readySkipCon">
						{ currentTab && currentTab !== 'ready' && (
							<a
								className="wopb-secondary-button wopb-return-dashboard cursor"
								onClick={ () => completeSetup() }
							>
								{ __(
									'Return to Dashboard',
									'product-blocks'
								) }
							</a>
						) }
					</div>
				</div>
			</div>
		</div>
	);
};

export default Main;
