/** @format */

const { __ } = wp.i18n;
const { useEffect } = wp.element;
const { InspectorControls, RichText } = wp.blockEditor;
import {
	CommonSettings,
	CustomCssAdvanced,
	GeneralAdvanced,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../helper/CommonPanel';
import { CssGenerator } from '../../helper/CssGenerator';
import { Section, Sections } from '../../helper/Sections';
import ToolBarElement from '../../helper/ToolBarElement';
import useBlockId from '../../helper/hooks/use-block-id';
import useFluentSettings from '../../helper/hooks/useFluentSettings';
import icons from '../../helper/icons';
import { AddSettingsToToolbar } from './Settings';

export default function Edit( props ) {
	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: {
			blockId,
			advanceId,
			plusMinusShow,
			showCoupon,
			showUpdate,
			showEmpty,
			showContinue,
			productHead,
			priceHead,
			qtyHead,
			subTotalHead,
			updateText,
			emptyText,
			continueShoppingText,
			couponInputPlaceholder,
			couponBtnText,
			removeBtnPosition,
		},
	} = props;
	const store = { setAttributes, name, attributes, clientId };

	const contentArgs = {
		qtyHead,
		priceHead,
		productHead,
		subTotalHead,
	};

	const { section, setCurrSettings, toolbarSettings } = useFluentSettings();

	useBlockId( props, true );

	function cartItemContent( contentArgs ) {
		const cartItems = [
			{
				cartTitle: 'WordPress Pennant',
				cartPrice: 12,
				cartQty: 4,
				cartSubtotal: 48,
			},
			{
				cartTitle: 'Beanie with Logo',
				cartPrice: 30,
				cartQty: 3,
				cartSubtotal: 90,
			},
		];

		const removeBtn = (
			<div
				className="product-remove wopb-component-simple"
				onClick={ ( e ) => setCurrSettings( e, 'remove-button' ) }
			>
				<a href="#" className="remove wopb-cart-product-remove">
					&times;
				</a>
			</div>
		);

		return cartItems.map( ( cartItem, idx ) => {
			return (
				<tr
					className="woocommerce-cart-form__cart-item cart_item wopb-component-simple"
					onClick={ ( e ) => setCurrSettings( e, 'table-body' ) }
					key={ idx }
				>
					<td className="wopb-cart-table-small">
						{ removeBtnPosition !== 'withImage' && removeBtn }
						<div className="wopb-product-image-section">
							<a href="#">
								<img
									src={
										wopb_data.url +
										'assets/img/wopb-fallback-img.png'
									}
								/>
							</a>
						</div>
					</td>
					<td data-title={ contentArgs.productHead }>
						<div className="wopb-cart-table-product-details">
							<div className="wopb-cart-table-medium">
								{ removeBtnPosition == 'left' && removeBtn }
								<div
									className="wopb-product-image-section"
									onClick={ ( e ) =>
										setCurrSettings( e, 'product-image' )
									}
								>
									<a href="#">
										<img
											src={
												wopb_data.url +
												'assets/img/wopb-fallback-img.png'
											}
										/>
									</a>
									{ removeBtnPosition == 'withImage' &&
										removeBtn }
								</div>
							</div>
							<div className="wopb-cart-table-product-section">
								<a href="#">{ cartItem.cartTitle }</a>
							</div>
						</div>
					</td>
					<td data-title={ contentArgs.priceHead }>
						<span className="woocommerce-Price-amount amount">
							<bdi>
								<span className="woocommerce-Price-currencySymbol">
									$
								</span>
								{ cartItem.cartPrice }.00
							</bdi>
						</span>
					</td>
					<td
						className="wopb-product-qty wopb-component-simple"
						onClick={ ( e ) => setCurrSettings( e, 'quantity' ) }
						data-title={ contentArgs.qtyHead }
					>
						<div className="wopb-qty-wrap">
							{ plusMinusShow && (
								<span
									className={ `wopb-add-to-cart-minus wopb-qty-ctrl` }
								>
									{ icons.minusIcon3 }
								</span>
							) }
							<input
								type="number"
								className="qty wopb-qty"
								value={ cartItem.cartQty }
							/>
							{ plusMinusShow && (
								<span
									className={ `wopb-add-to-cart-plus wopb-qty-ctrl` }
								>
									{ icons.plusIcon5 }
								</span>
							) }
						</div>
					</td>
					<td
						className="wopb-product-subtotal"
						data-title={ contentArgs.subTotalHead }
					>
						<span className="woocommerce-Price-amount amount">
							<bdi>
								<span className="woocommerce-Price-currencySymbol">
									$
								</span>
								{ cartItem.cartSubtotal }.00
							</bdi>
						</span>
						<div className="wopb-cart-table-medium">
							{ removeBtnPosition == 'right' && removeBtn }
						</div>
					</td>
				</tr>
			);
		} );
	}

	cartTableFooterResponsive();

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/cart-table', blockId );
	}

	useEffect( () => {
		window.addEventListener( 'resize', cartTableFooterResponsive );

		return () =>
			window.removeEventListener( 'resize', cartTableFooterResponsive );
	}, [] );

	function cartTableFooterResponsive() {
		/**
		 * @type {HTMLElement} cartTableWrapper
		 */
		const cartTableWrapper = document.querySelector( '.wopb-cart-form' );

		if ( cartTableWrapper ) {
			if ( cartTableWrapper.offsetWidth <= 685 ) {
				if (
					520 <= cartTableWrapper.offsetWidth &&
					cartTableWrapper.offsetWidth <= 644
				) {
					if (
						( showCoupon &&
							showContinue &&
							( ( showEmpty && ! showUpdate ) ||
								( ! showEmpty && showUpdate ) ) ) ||
						( showEmpty &&
							showUpdate &&
							( ( showCoupon && ! showContinue ) ||
								( ! showCoupon && showContinue ) ) )
					) {
						cartTableWrapper.querySelector(
							'.wopb-cart-table-options'
						).style =
							'grid-template-columns: auto; justify-content: normal';
						if (
							cartTableWrapper.querySelector(
								'.wopb-cart-table-option-hidden'
							)
						) {
							cartTableWrapper.querySelector(
								'.wopb-cart-table-option-hidden'
							).style = 'grid-template-columns: auto;';
						}
					} else {
						cartTableWrapper.querySelector(
							'.wopb-cart-table-options'
						).style =
							'grid-template-columns: auto auto; justify-content: space-between';
					}
				}
				if ( showCoupon && showContinue && showEmpty && showUpdate ) {
					cartTableWrapper.querySelector(
						'.wopb-cart-table-options'
					).style =
						'grid-template-columns: auto; justify-content: normal';
				} else {
					cartTableWrapper.querySelector(
						'.wopb-cart-table-options'
					).style =
						'grid-template-columns: auto auto; justify-content: space-between';
					cartTableWrapper.querySelector(
						'.wopb-cart-table-first-option'
					).style = 'grid-template-columns: auto auto';
				}

				if (
					( ( showCoupon &&
						showContinue &&
						( ( showEmpty && ! showUpdate ) ||
							( ! showEmpty && showUpdate ) ) ) ||
						( showEmpty &&
							showUpdate &&
							( ( showCoupon && ! showContinue ) ||
								( ! showCoupon && showContinue ) ) ) ) &&
					cartTableWrapper.offsetWidth <= 520
				) {
					cartTableWrapper.querySelector(
						'.wopb-cart-table-options'
					).style =
						'grid-template-columns: auto; justify-content: normal';
					cartTableWrapper.querySelector(
						'.wopb-cart-table-first-option'
					).style = 'grid-template-columns: auto';
					if (
						cartTableWrapper.querySelector(
							'.wopb-cart-table-option-hidden'
						)
					) {
						cartTableWrapper.querySelector(
							'.wopb-cart-table-option-hidden'
						).style = 'grid-template-columns: auto;';
					}
				} else if (
					( ( showCoupon && ! showContinue ) ||
						( ! showCoupon && showContinue ) ) &&
					( ( showEmpty && ! showUpdate ) ||
						( ! showEmpty && showUpdate ) ) &&
					520 >= cartTableWrapper.offsetWidth &&
					430 <= cartTableWrapper.offsetWidth
				) {
					if (
						cartTableWrapper.querySelector(
							'.wopb-cart-table-option-hidden'
						)
					) {
						cartTableWrapper.querySelector(
							'.wopb-cart-table-option-hidden'
						).style = 'grid-template-columns: auto;';
					}
				} else if ( cartTableWrapper.offsetWidth <= 520 ) {
					cartTableWrapper.querySelector(
						'.wopb-cart-table-options'
					).style =
						'grid-template-columns: auto; justify-content: normal';
					cartTableWrapper.querySelector(
						'.wopb-cart-table-first-option'
					).style = 'grid-template-columns: auto';
				}
			} else {
				cartTableWrapper.querySelector(
					'.wopb-cart-table-options'
				).style =
					'grid-template-columns: auto auto; justify-content: space-between';
				cartTableWrapper.querySelector(
					'.wopb-cart-table-first-option'
				).style = 'grid-template-columns: auto auto';
			}
		}
	}

	return (
		<>
			<ToolBarElement
				store={ store }
				include={ [
					{
						type: 'block_spacing',
						label: __( 'Cart Table Spacing', 'product-blocks' ),
					},
				] }
			/>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Settings', 'product-blocks' ) }
					>
						<CommonSettings
							initialOpen={ true }
							title={ 'inline' }
							store={ store }
							include={ [
								{
									data: {
										type: 'border',
										key: 'tableBorder',
										label: __(
											'Table Border',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'tableRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'table-header' ] }
							title={ 'Table Header' }
							store={ store }
							include={ [
								{
									data: {
										type: 'text',
										key: 'productHead',
										label: __(
											'Product Name Here',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'text',
										key: 'priceHead',
										label: __( 'Price', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'text',
										key: 'qtyHead',
										label: __(
											'Quantity',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'text',
										key: 'subTotalHead',
										label: __(
											'Subtotal',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'tableHeadTextColor',
										label: __(
											'Text Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'headingTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'tableHeadBgColor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'headingBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'headingPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>

						<CommonSettings
							initialOpen={ section[ 'table-body' ] }
							title={ __( 'Table Body', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'tableBodyTextColor',
										label: __(
											'Text Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'tableBodyLinkColor',
										label: __(
											'Link Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'tableBodyLinkHoverColor',
										label: __(
											'Link Hover Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'bodyTypo',
										label: __(
											'Body Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'bodyTitleTypo',
										label: __(
											'Title Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'bodyBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'tableBodyBgColor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'bodyPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>

						<CommonSettings
							initialOpen={ section.quantity }
							title={ __( 'Quantity', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'toggle',
										key: 'plusMinusShow',
										label: __(
											'Plus Minus',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'quantityBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'range',
										key: 'quantityWidth',
										label: __( 'Width', 'product-blocks' ),
										min: 40,
										max: 200,
										step: 1,
									},
								},
								{
									data: {
										type: 'color',
										key: 'quantityBgColor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'quantityColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'quantityRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'quantityPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>

						<CommonSettings
							initialOpen={ section[ 'product-image' ] }
							title={ __( 'Product Image', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'range',
										key: 'imageSize',
										label: __(
											'Image Size',
											'product-blocks'
										),
										min: 20,
										max: 150,
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'imageRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>

						<CommonSettings
							initialOpen={ section[ 'remove-button' ] }
							title={ __( 'Remove Button', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'tag',
										key: 'removeBtnPosition',
										label: __(
											'Position',
											'product-blocks'
										),
										options: [
											{
												value: 'left',
												label: __(
													'Left',
													'product-blocks'
												),
											},
											{
												value: 'right',
												label: __(
													'Right',
													'product-blocks'
												),
											},
											{
												value: 'withImage',
												label: __(
													'With Image',
													'product-blocks'
												),
											},
										],
									},
								},
								{
									data: {
										type: 'color',
										key: 'removeBtnTextColor',
										label: __(
											'Text Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'removeBtnHoverColor',
										label: __(
											'Hover Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'removeBtnBgColor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'removeBtnBgHoverColor',
										label: __(
											'Hover Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'removeBtnBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'range',
										key: 'removeBtnFontSize',
										label: __(
											'Font Size',
											'product-blocks'
										),
										min: 10,
										max: 40,
										step: 1,
									},
								},
								{
									data: {
										type: 'range',
										key: 'removeBtnSize',
										label: __(
											'Icon Size',
											'product-blocks'
										),
										min: 10,
										max: 50,
										step: 1,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'removeBtnRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'table-footer' ] }
							title={ __( 'Table Footer', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'tableFooterBgColor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'tableFooterBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'tableFooterRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},

								{
									data: {
										type: 'dimension',
										key: 'tableFooterPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>

						<CommonSettings
							depend="showCoupon"
							initialOpen={ section.coupon }
							title={ __( 'Coupon', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'text',
										key: 'couponInputPlaceholder',
										label: __(
											'Coupon Code',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'text',
										key: 'couponBtnText',
										label: __(
											'Apply Coupon Button',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'couponInputFontTypo',
										label: __(
											'Input Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'couponInputTextColor',
										label: __(
											'Input Text Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'couponInputBgColor',
										label: __(
											'Input Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'couponInputBorder',
										label: __(
											'Input Border',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'couponInputRadius',
										label: __(
											'Input Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'couponInputPadding',
										label: __(
											'Input Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'color',
										key: 'couponBtnTextColor',
										label: __(
											'Button Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'couponBtnTextHoverColor',
										label: __(
											'Button Hover Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'couponBtnBgColor',
										label: __(
											'Button Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'couponBtnBgHoverColor',
										label: __(
											'Button Hover Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'couponBtnBorder',
										label: __(
											'Button Border',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'couponBtnFontTypo',
										label: __(
											'Button Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'couponBtnRadius',
										label: __(
											'Button Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'couponBtnPadding',
										label: __(
											'Button Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>

						<CommonSettings
							depend="showContinue"
							initialOpen={ section[ 'continue-shopping' ] }
							title={ __(
								'Continue Shopping',
								'product-blocks'
							) }
							store={ store }
							include={ [
								{
									data: {
										type: 'text',
										key: 'continueShoppingText',
										label: __(
											'Continue Shopping Text',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'continueShoppingTextColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'continueShoppingTextHoverColor',
										label: __(
											'Hover Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'continueShoppingBgColor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'continueShoppingBgHoverColor',
										label: __(
											'Hover Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'continueShoppingBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'continueShoppingFontTypo',
										label: __(
											'Button Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'continueShoppingRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'continueShoppingPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>

						<CommonSettings
							depend="showEmpty"
							initialOpen={ section[ 'empty-cart-button' ] }
							title={ __(
								'Empty Cart Button',
								'product-blocks'
							) }
							store={ store }
							include={ [
								{
									data: {
										type: 'text',
										key: 'emptyText',
										label: __(
											'Empty Cart Text',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'emptyTextColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'emptyHoverTextColor',
										label: __(
											'Hover Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'emptyBgColor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'emptyHoverBgColor',
										label: __(
											'Hover Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'emptyBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'emptyFontTypo',
										label: __(
											'Button Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'emptyRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'emptyPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>

						<CommonSettings
							depend="showUpdate"
							initialOpen={ section[ 'cart-update' ] }
							title={ __( 'Cart Update', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'text',
										key: 'updateText',
										label: __(
											'Cart Update Text',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'updateTextColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'updateHoverTextColor',
										label: __(
											'Hover Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'updateBgColor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'updateHoverBgColor',
										label: __(
											'Hover Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'updateBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'updateFontTypo',
										label: __(
											'Button Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'updateRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'updatePadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							depend="showCrossSell"
							initialOpen={ false }
							title={ __(
								'Cross Sell Product',
								'product-blocks'
							) }
							store={ store }
							include={ [
								{
									data: {
										type: 'group',
										key: 'crossSellPosition',
										label: __(
											'Position',
											'product-blocks'
										),
										justify: true,
										options: [
											{
												value: 'right',
												label: __(
													'Table Right',
													'product-blocks'
												),
											},
											{
												value: 'bottom',
												label: __(
													'Table Bottom',
													'product-blocks'
												),
											},
										],
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ false }
							title={ __( 'Cart Empty Body', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'empBodyColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'empBodyBg',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'empBodyBorder',
										label: __(
											'Button Border',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'empBodyBtnColor',
										label: __(
											'Button Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'empBodyBtnBg',
										label: __(
											'Button Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'empBodyBtnHvrColor',
										label: __(
											'Button Hover Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'empBodyBtnHvrBg',
										label: __(
											'Button Hover Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'empBodyBtnBorder',
										label: __(
											'Button Border',
											'product-blocks'
										),
									},
								},
							] }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced initialOpen={ true } store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar
				selected={ toolbarSettings }
				store={ store }
			/>

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div
					className={ 'wopb-product-wrapper wopb-cart-table-wrapper' }
				>
					<form
						action="javascript:"
						className="woocommerce-cart-form wopb-cart-form"
						method="post"
					>
						<table className="woocommerce-cart-form__contents wopb-cart-table">
							<thead>
								<tr>
									<RichText
										tagName={ 'th' }
										keepPlaceholderOnFocus
										placeholder={ __(
											'Add Product Name',
											'product-blocks'
										) }
										onChange={ ( v ) =>
											setAttributes( { productHead: v } )
										}
										value={ productHead }
										onClick={ ( e ) =>
											setCurrSettings( e, 'table-header' )
										}
									/>
									<RichText
										tagName={ 'th' }
										keepPlaceholderOnFocus
										placeholder={ __(
											'Add Price Tag',
											'product-blocks'
										) }
										onChange={ ( v ) =>
											setAttributes( { priceHead: v } )
										}
										value={ priceHead }
										onClick={ ( e ) =>
											setCurrSettings( e, 'table-header' )
										}
									/>
									<RichText
										tagName={ 'th' }
										className={ 'wopb-product-qty' }
										keepPlaceholderOnFocus
										placeholder={ __(
											'Add Quantity Tag',
											'product-blocks'
										) }
										onChange={ ( v ) =>
											setAttributes( { qtyHead: v } )
										}
										value={ qtyHead }
										onClick={ ( e ) =>
											setCurrSettings( e, 'table-header' )
										}
									/>
									<RichText
										tagName={ 'th' }
										className={ 'wopb-product-subtotal' }
										keepPlaceholderOnFocus
										placeholder={ __(
											'Add Sub Total Tag',
											'product-blocks'
										) }
										onChange={ ( v ) =>
											setAttributes( { subTotalHead: v } )
										}
										value={ subTotalHead }
										onClick={ ( e ) =>
											setCurrSettings( e, 'table-header' )
										}
									/>
								</tr>
							</thead>
							<tbody>{ cartItemContent( contentArgs ) }</tbody>
						</table>
						<div
							className="wopb-cart-table-options wopb-component-simple"
							onClick={ ( e ) =>
								setCurrSettings( e, 'table-footer' )
							}
						>
							<div
								className={ `wopb-cart-table-option wopb-cart-table-first-option ${
									! showCoupon || ! showContinue
										? 'wopb-cart-table-option-hidden'
										: ''
								}` }
							>
								{ showCoupon && (
									<div
										className="wopb-cart-coupon-section wopb-component-simple"
										onClick={ ( e ) =>
											setCurrSettings( e, 'coupon' )
										}
									>
										<input
											type="text"
											name="coupon_code"
											className="wopb-coupon-code"
											id="coupon_code"
											placeholder={
												couponInputPlaceholder
											}
										/>
										<button
											type="submit"
											className="wopb-cart-coupon-submit-btn"
											name="apply_coupon"
											value="Apply coupon"
										>
											{ couponBtnText }
										</button>
									</div>
								) }
								{ showContinue && (
									<button
										className="wopb-cart-shopping-btn wopb-component-simple"
										onClick={ ( e ) =>
											setCurrSettings(
												e,
												'continue-shopping'
											)
										}
									>
										&#8592;&nbsp;{ continueShoppingText }
									</button>
								) }
							</div>
							<div
								className={ `wopb-cart-table-option wopb-cart-table-second-option ${
									! showEmpty || ! showUpdate
										? 'wopb-cart-table-option-hidden'
										: ''
								}` }
							>
								{ showEmpty && (
									<button
										className="wopb-cart-empty-btn wopb-component-simple"
										onClick={ ( e ) =>
											setCurrSettings(
												e,
												'empty-cart-button'
											)
										}
									>
										{ emptyText }
									</button>
								) }
								{ showUpdate && (
									<button
										type="submit"
										className="button wopb-cart-update-btn wopb-component-simple"
										name="update_cart"
										value="Update cart"
										onClick={ ( e ) =>
											setCurrSettings( e, 'cart-update' )
										}
									>
										{ updateText }
									</button>
								) }
							</div>
						</div>
					</form>
				</div>
			</div>
		</>
	);
}
