const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';
registerBlockType( 'product-blocks/cart-table', {
	title: __( 'Cart Table', 'product-blocks' ),
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/cart_table.svg' }
			alt={ __( 'Image', 'product-blocks' ) }
			style={ { width: '100%' } }
		/>
	),
	category: 'single-cart',
	description: __(
		'Dynamically control position, size, and typography of cart table.',
		'product-blocks'
	),
	keywords: [
		__( 'cart-table', 'product-blocks' ),
		__( 'cart', 'product-blocks' ),
		__( 'table', 'product-blocks' ),
	],
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
