const attributes = {
	blockId: { type: 'string', default: '' },
	currentPostId: { type: 'string', default: '' },
	showCoupon: { type: 'boolean', default: true },
	showUpdate: { type: 'boolean', default: true },
	showEmpty: { type: 'boolean', default: true },
	showContinue: { type: 'boolean', default: false },
	showCrossSell: { type: 'boolean', default: false },
	productHead: { type: 'string', default: 'Product' },
	priceHead: { type: 'string', default: 'Price' },
	qtyHead: { type: 'string', default: 'Quantity' },
	plusMinusShow: { type: 'boolean', default: false },
	subTotalHead: { type: 'string', default: 'Subtotal' },
	tableBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 0, left: 1 },
			color: '#E6E6E6',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper .wopb-cart-form table',
			},
		],
	},
	tableHeadBgColor: {
		type: 'string',
		default: '#E6E6E6',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table thead th' +
					' { background-color:{{tableHeadBgColor}} !important; }',
			},
		],
	},
	tableHeadTextColor: {
		type: 'string',
		default: '#4A4A4A',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table thead th,' +
					' {{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper tbody tr td:before' +
					' { color:{{tableHeadTextColor}}; }',
			},
		],
	},
	headingTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: '600',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table thead th,' +
					' {{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper tbody tr td:before',
			},
		],
	},
	headingBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 0, right: 0, bottom: 1, left: 0 },
			color: '#D0D0D0',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table thead th',
			},
		],
	},
	tableRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0 }, unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table' +
					' { border-radius:{{tableRadius}}; overflow:hidden; border-collapse: separate !important; }',
			},
		],
	},
	headingPadding: {
		type: 'object',
		default: {
			lg: { top: 14, bottom: 14, left: 32, right: 32, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper table thead th { padding:{{headingPadding}}; }',
			},
		],
	},

	// Table Body
	tableBodyTextColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table tbody tr td ,' +
					' {{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table tbody tr td .qty' +
					' { color:{{tableBodyTextColor}}; }',
			},
		],
	},
	tableBodyLinkColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table tbody tr a { color:{{tableBodyLinkColor}}; }',
			},
		],
	},
	tableBodyLinkHoverColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table tbody tr a:hover' +
					' { color:{{tableBodyLinkHoverColor}}; }',
			},
		],
	},
	bodyTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: 'normal',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table tbody tr td,' +
					' {{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table tbody tr td .qty ',
			},
		],
	},
	bodyTitleTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: 'normal',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table .wopb-cart-table-product-section',
			},
		],
	},
	bodyBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 0, right: 0, bottom: 1, left: 0 },
			color: '#E6E6E6',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table tbody tr td',
			},
		],
	},
	tableBodyBgColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper table tbody tr > td { background-color:{{tableBodyBgColor}} !important; }',
			},
		],
	},
	bodyPadding: {
		type: 'object',
		default: {
			lg: { left: 32, top: 18, right: 32, bottom: 18, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table tbody tr td' +
					' { padding:{{bodyPadding}} !important; }',
			},
		],
	},

	// Quantity
	quantityBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#E6E6E6',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table tbody tr td .qty, ' +
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table tbody tr td .wopb-qty-ctrl ',
			},
		],
	},
	quantityWidth: {
		type: 'string',
		default: '84',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table tbody tr td .qty' +
					' {width:{{quantityWidth}}px; max-width:{{quantityWidth}}px; line-height: normal; text-align: left;}',
			},
		],
	},
	quantityBgColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table tbody tr .qty, ' +
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table tbody tr .wopb-qty-ctrl' +
					' { background-color:{{quantityBgColor}}; }',
			},
		],
	},
	quantityColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table tbody tr input.qty, ' +
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table tbody tr .wopb-qty-ctrl' +
					' { color:{{quantityColor}}; }',
			},
		],
	},
	quantityRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table tbody tr .qty, ' +
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table tbody tr .wopb-qty-ctrl' +
					' { border-radius:{{quantityRadius}} !important; }',
			},
		],
	},
	quantityPadding: {
		type: 'object',
		default: { lg: { top: 8, right: 2, bottom: 8, left: 16, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table tbody tr .qty, ' +
					'{{WOPB}} table.wopb-cart-table .wopb-qty-ctrl' +
					' { padding:{{quantityPadding}} !important; }',
			},
		],
	},

	// Image
	imageSize: {
		type: 'object',
		default: { lg: 40, unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-image-section { margin-right: 14px; width:{{imageSize}}; }',
			},
		],
	},
	imageRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-image-section a img { border-radius:{{imageRadius}}; }',
			},
		],
	},

	// Remove Button
	removeBtnTextColor: {
		type: 'string',
		default: '#A1A1A1',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table tbody tr a.wopb-cart-product-remove' +
					' { color:{{removeBtnTextColor}}; }',
			},
		],
	},
	removeBtnPosition: {
		type: 'string',
		default: 'left',
		style: [
			{
				depends: [
					{
						key: 'removeBtnPosition',
						condition: '==',
						value: 'left',
					},
				],
				selector:
					' {{WOPB}} .wopb-cart-table-medium .product-remove ,' +
					' .block-editor-block-list__block {{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper .product-remove {margin-right:14px}' +
					'  {{WOPB}} .product-remove.wopb-cart-table-small {text-align:{{removeBtnPosition}}}' +
					'  {{WOPB}} table.cart td.product-remove.wopb-cart-table-small a.remove {position: initial}  ',
			},
			{
				depends: [
					{
						key: 'removeBtnPosition',
						condition: '==',
						value: 'right',
					},
				],
				selector: '{{WOPB}} .product-remove {margin-left:12px}',
			},
			{
				depends: [
					{
						key: 'removeBtnPosition',
						condition: '==',
						value: 'withImage',
					},
				],
				selector:
					'{{WOPB}} .product-remove .remove{position: absolute ;}',
			},
		],
	},
	removeBtnHoverColor: {
		type: 'string',
		default: '#FF0303',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper.wopb-cart-table-wrapper table.wopb-cart-table tbody tr a.wopb-cart-product-remove:hover { color:{{removeBtnHoverColor}}; }',
			},
		],
	},
	removeBtnBgColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-product-remove { background:{{removeBtnBgColor}}; }',
			},
		],
	},
	removeBtnBgHoverColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-product-remove:hover { background:{{removeBtnBgHoverColor}}; }',
			},
		],
	},
	removeBtnBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#000',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-cart-product-remove' } ],
	},
	removeBtnFontSize: {
		type: 'string',
		default: '20',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-product-remove { font-size:{{removeBtnFontSize}}px; }',
			},
		],
	},
	removeBtnSize: {
		type: 'string',
		default: '12',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-product-remove { height:{{removeBtnSize}}px; width:{{removeBtnSize}}px; }',
			},
		],
	},
	removeBtnRadius: {
		type: 'object',
		default: {
			lg: { top: 10, right: 10, bottom: 10, left: 10, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-product-remove { border-radius:{{removeBtnRadius}}; }',
			},
		],
	},
	tableFooterBgColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-cart-table-options { background:{{tableFooterBgColor}}; }',
			},
		],
	},

	tableFooterBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#000',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-cart-table-options',
			},
		],
	},
	tableFooterRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-cart-table-options { border-radius:{{tableFooterRadius}}; }',
			},
		],
	},
	tableFooterPadding: {
		type: 'object',
		default: { lg: { top: 24, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-cart-table-options { padding:{{tableFooterPadding}}; }',
			},
		],
	},
	couponInputPlaceholder: {
		type: 'string',
		default: 'Enter Coupon Code.....',
	},
	couponBtnText: { type: 'string', default: 'Apply Coupon' },
	couponInputFontTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: 'normal',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-cart-table-options .wopb-coupon-code',
			},
		],
	},
	couponInputTextColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-table-options .wopb-coupon-code { color:{{couponInputTextColor}}; }',
			},
		],
	},
	couponInputBgColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-table-options .wopb-coupon-code { background:{{couponInputBgColor}}; }',
			},
		],
	},
	couponInputBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#D2D2D2',
			type: 'solid',
		},
		style: [
			{ selector: '{{WOPB}} .wopb-cart-table-options .wopb-coupon-code' },
		],
	},
	couponInputRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-table-options .wopb-coupon-code { border-radius:{{couponInputRadius}}; }',
			},
		],
	},
	couponInputPadding: {
		type: 'object',
		default: { lg: { top: 9, right: 12, bottom: 9, left: 12, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-table-options .wopb-coupon-code { padding:{{couponInputPadding}} !important; line-height: normal; box-shadow:none}',
			},
		],
	},
	couponBtnTextColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-coupon-submit-btn { color:{{couponBtnTextColor}}; }',
			},
		],
	},
	couponBtnTextHoverColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-coupon-submit-btn:hover { color:{{couponBtnTextHoverColor}}; }',
			},
		],
	},
	couponBtnBgColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-coupon-submit-btn { background:{{couponBtnBgColor}}; }',
			},
		],
	},
	couponBtnBgHoverColor: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-coupon-submit-btn:hover { background-color:{{couponBtnBgHoverColor}}; }',
			},
		],
	},
	couponBtnBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 0, right: 0, bottom: 0, left: 0 },
			color: '#000',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-cart-coupon-submit-btn' } ],
	},
	couponBtnFontTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: 'normal',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-coupon-submit-btn , .editor-styles-wrapper {{WOPB}} .wopb-cart-coupon-submit-btn',
			},
		],
	},
	couponBtnRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-coupon-submit-btn { border-radius:{{couponBtnRadius}}; }',
			},
		],
	},
	couponBtnPadding: {
		type: 'object',
		default: {
			lg: { top: 10, right: 20, bottom: 10, left: 20, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-coupon-submit-btn , .editor-styles-wrapper {{WOPB}} .wopb-cart-coupon-submit-btn { padding:{{couponBtnPadding}} !important; line-height: normal;}',
			},
		],
	},
	continueShoppingText: { type: 'string', default: 'Continue Shopping' },
	continueShoppingTextColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-shopping-btn { color:{{continueShoppingTextColor}}; }',
			},
		],
	},
	continueShoppingBgColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-shopping-btn { background-color:{{continueShoppingBgColor}}; }',
			},
		],
	},
	continueShoppingTextHoverColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-table-options .wopb-cart-shopping-btn:hover { color:{{continueShoppingTextHoverColor}}; }',
			},
		],
	},
	continueShoppingBgHoverColor: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-table-options .wopb-cart-shopping-btn:hover { background-color:{{continueShoppingBgHoverColor}}; }',
			},
		],
	},
	continueShoppingBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 0, right: 0, bottom: 0, left: 0 },
			color: '#000',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-cart-shopping-btn' } ],
	},
	continueShoppingFontTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: 'normal',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-shopping-btn , .editor-styles-wrapper {{WOPB}} .wopb-cart-shopping-btn',
			},
		],
	},
	continueShoppingPadding: {
		type: 'object',
		default: {
			lg: { top: 10, right: 20, bottom: 10, left: 20, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-shopping-btn , .editor-styles-wrapper {{WOPB}} .wopb-cart-shopping-btn { padding:{{continueShoppingPadding}} !important; line-height: normal;}',
			},
		],
	},
	continueShoppingRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-shopping-btn { border-radius:{{continueShoppingRadius}}; }',
			},
		],
	},
	emptyText: { type: 'string', default: 'Empty Cart' },
	emptyTextColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-empty-btn { color:{{emptyTextColor}}; }',
			},
		],
	},
	emptyBgColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-empty-btn { background-color:{{emptyBgColor}}; }',
			},
		],
	},
	emptyHoverTextColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-empty-btn:hover { color:{{emptyHoverTextColor}}; }',
			},
		],
	},
	emptyHoverBgColor: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-empty-btn:hover { background-color:{{emptyHoverBgColor}}; }',
			},
		],
	},
	emptyBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 0, right: 0, bottom: 0, left: 0 },
			color: '#000',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-cart-empty-btn' } ],
	},
	emptyFontTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: 'normal',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-empty-btn , .editor-styles-wrapper {{WOPB}} .wopb-cart-empty-btn',
			},
		],
	},
	emptyPadding: {
		type: 'object',
		default: {
			lg: { top: 10, right: 20, bottom: 10, left: 20, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-empty-btn , .editor-styles-wrapper {{WOPB}} .wopb-cart-empty-btn { padding:{{emptyPadding}} !important; line-height: normal;}',
			},
		],
	},
	emptyRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-empty-btn { border-radius:{{emptyRadius}}; }',
			},
		],
	},
	updateText: { type: 'string', default: 'Update Cart' },
	updateTextColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-update-btn { color:{{updateTextColor}}; }',
			},
		],
	},
	updateBgColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-update-btn { background-color:{{updateBgColor}}; }',
			},
		],
	},
	updateHoverTextColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-update-btn:hover { color:{{updateHoverTextColor}}; }',
			},
		],
	},
	updateHoverBgColor: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-update-btn:hover { background-color:{{updateHoverBgColor}}; }',
			},
		],
	},
	updateBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 0, right: 0, bottom: 0, left: 0 },
			color: '#000',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-cart-update-btn' } ],
	},
	updateFontTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: 'normal',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-update-btn , .editor-styles-wrapper {{WOPB}} .wopb-cart-update-btn',
			},
		],
	},
	updatePadding: {
		type: 'object',
		default: {
			lg: { top: 10, right: 20, bottom: 10, left: 20, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-update-btn , .editor-styles-wrapper {{WOPB}} .wopb-cart-update-btn { padding:{{updatePadding}} !important; line-height: normal;}',
			},
		],
	},
	updateRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-update-btn { border-radius:{{updateRadius}}; }',
			},
		],
	},
	crossSellPosition: { type: 'string', default: 'bottom' },
	empBodyColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-table-wrapper:has(.wc-empty-cart-message), {{WOPB}} .wopb-cart-table-wrapper:has(.wc-empty-cart-message) .cart-empty.woocommerce-info { color:{{empBodyColor}}; }',
			},
		],
	},
	empBodyBg: {
		type: 'string',
		default: '#E6E6E6',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-table-wrapper:has(.wc-empty-cart-message) { background-color:{{empBodyBg}}; }',
			},
		],
	},
	empBodyBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 0, right: 0, bottom: 0, left: 0 },
			color: '#000',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-table-wrapper:has(.wc-empty-cart-message)',
			},
		],
	},
	empBodyBtnColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-table-wrapper .return-to-shop a { color:{{empBodyBtnColor}}; }',
			},
		],
	},
	empBodyBtnBg: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-table-wrapper .return-to-shop a { background-color:{{empBodyBtnBg}}; }',
			},
		],
	},
	empBodyBtnHvrColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-table-wrapper .return-to-shop:hover a { color:{{empBodyBtnHvrColor}}; }',
			},
		],
	},
	empBodyBtnHvrBg: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-table-wrapper .return-to-shop:hover a { background-color:{{empBodyBtnHvrBg}}; }',
			},
		],
	},
	empBodyBtnBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 0, right: 0, bottom: 0, left: 0 },
			color: '#000',
			type: 'solid',
		},
		style: [
			{ selector: '{{WOPB}} .wopb-cart-table-wrapper .return-to-shop a' },
		],
	},

	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	wrapMargin: {
		type: 'object',
		default: { lg: { top: '', bottom: '20', unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper { margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#f5f5f5' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#ff176b' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapInnerPadding: {
		type: 'object',
		default: { lg: { unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ padding:{{wrapInnerPadding}}; }',
			},
		],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: { type: 'string', default: '', style: [ { selector: '' } ] },
};
export default attributes;
