/** @format */
const { __ } = wp.i18n;

import { settingsIcon } from '../../helper/CommonPanel';
import WopbToolbarDropdown from '../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar, getData } from '../../helper/ux';

export function AddSettingsToToolbar( { store, selected } ) {
	switch ( selected ) {
		case 'table-header':
			return (
				<WopbToolbarGroup text={ 'Header' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Table Header Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Table Header Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'text',
											key: 'productHead',
											label: __(
												'Product Name Here',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'text',
											key: 'priceHead',
											label: __(
												'Price',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'text',
											key: 'qtyHead',
											label: __(
												'Quantity',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'text',
											key: 'subTotalHead',
											label: __(
												'Subtotal',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'tableHeadTextColor',
											label: __(
												'Text Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'headingTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'tableHeadBgColor',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'headingBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'headingPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'product-image':
			return (
				<WopbToolbarGroup text={ 'Image' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Product Image Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Product Image Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'range',
											key: 'imageSize',
											label: __(
												'Image Size',
												'product-blocks'
											),
											min: 20,
											max: 150,
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'imageRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'empty-cart':
			return (
				<WopbToolbarGroup text={ 'Empty' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Empty Cart Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Empty Cart Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'text',
											key: 'emptyText',
											label: __(
												'Empty Cart Text',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'emptyTextColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'emptyHoverTextColor',
											label: __(
												'Hover Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'emptyBgColor',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'emptyHoverBgColor',
											label: __(
												'Hover Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'emptyBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'emptyFontTypo',
											label: __(
												'Body Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'emptyRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'emptyPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'cart-update':
			return (
				<WopbToolbarGroup text={ 'Update' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Cart Update Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Cart Update Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'text',
											key: 'updateText',
											label: __(
												'Cart Update Text',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'updateTextColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'updateHoverTextColor',
											label: __(
												'Hover Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'updateBgColor',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'updateHoverBgColor',
											label: __(
												'Hover Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'updateBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'range',
											key: 'updateFontSize',
											label: __(
												'Font Size',
												'product-blocks'
											),
											min: 1,
											max: 30,
											step: 1,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'updateRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'updatePadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'continue-shopping':
			return (
				<WopbToolbarGroup
					text={ 'Continue Shopping' }
					textScroll={ true }
				>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Continue Shopping Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Continue Shopping Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'text',
											key: 'continueShoppingText',
											label: __(
												'Continue Shopping Text',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'continueShoppingTextColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'continueShoppingTextHoverColor',
											label: __(
												'Hover Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'continueShoppingBgColor',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'continueShoppingBgHoverColor',
											label: __(
												'Hover Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'continueShoppingBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'continueShoppingFontTypo',
											label: __(
												'Body Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'continueShoppingRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'continueShoppingPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'coupon':
			return (
				<WopbToolbarGroup text={ 'Coupon' } textScroll={ true }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Product Image Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Product Image Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'text',
											key: 'couponInputPlaceholder',
											label: __(
												'Coupon Code',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'text',
											key: 'couponBtnText',
											label: __(
												'Apply Coupon Button',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'couponInputFontTypo',
											label: __(
												'Body Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'couponInputTextColor',
											label: __(
												'Input Text Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'couponInputBgColor',
											label: __(
												'Input Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'couponInputBorder',
											label: __(
												'Input Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'couponInputRadius',
											label: __(
												'Input Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'couponInputPadding',
											label: __(
												'Input Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'color',
											key: 'couponBtnTextColor',
											label: __(
												'Button Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'couponBtnTextHoverColor',
											label: __(
												'Button Hover Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'couponBtnBgColor',
											label: __(
												'Button Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'couponBtnBgHoverColor',
											label: __(
												'Button Hover Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'couponBtnBorder',
											label: __(
												'Button Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'couponBtnFontTypo',
											label: __(
												'Body Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'couponBtnRadius',
											label: __(
												'Button Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'couponBtnPadding',
											label: __(
												'Button Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'remove-button':
			return (
				<WopbToolbarGroup text={ 'Remove' } textScroll={ true }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Remove Button Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Remove Button Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'tag',
											key: 'removeBtnPosition',
											label: __(
												'Position',
												'product-blocks'
											),
											options: [
												{
													value: 'left',
													label: __(
														'Left',
														'product-blocks'
													),
												},
												{
													value: 'right',
													label: __(
														'Right',
														'product-blocks'
													),
												},
												{
													value: 'withImage',
													label: __(
														'With Image',
														'product-blocks'
													),
												},
											],
										},
									},
									{
										data: {
											type: 'color',
											key: 'removeBtnTextColor',
											label: __(
												'Text Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'removeBtnHoverColor',
											label: __(
												'Hover Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'removeBtnBgColor',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'removeBtnBgHoverColor',
											label: __(
												'Hover Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'removeBtnBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'range',
											key: 'removeBtnFontSize',
											label: __(
												'Font Size',
												'product-blocks'
											),
											min: 10,
											max: 40,
											step: 1,
										},
									},
									{
										data: {
											type: 'range',
											key: 'removeBtnSize',
											label: __(
												'Icon Size',
												'product-blocks'
											),
											min: 10,
											max: 50,
											step: 1,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'removeBtnRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'quantity':
			return (
				<WopbToolbarGroup text={ 'Quantity' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Quantity Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Quantity Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'border',
											key: 'quantityBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'range',
											key: 'quantityWidth',
											label: __(
												'Width',
												'product-blocks'
											),
											min: 40,
											max: 200,
											step: 1,
										},
									},
									{
										data: {
											type: 'color',
											key: 'quantityBgColor',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'quantityRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'quantityPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'table-footer':
			return (
				<WopbToolbarGroup text={ 'Table Footer' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Table Footer Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Table Footer Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'tableFooterBgColor',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'tableFooterBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'tableFooterRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},

									{
										data: {
											type: 'dimension',
											key: 'tableFooterPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'table-body':
			return (
				<WopbToolbarGroup text={ 'Table Body' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Table Body Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Table Body Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'tableBodyTextColor',
											label: __(
												'Text Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'tableBodyLinkColor',
											label: __(
												'Link Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'tableBodyLinkHoverColor',
											label: __(
												'Link Hover Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'bodyTypo',
											label: __(
												'Body Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'bodyTitleTypo',
											label: __(
												'Title Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'bodyBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'tableBodyBgColor',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'bodyPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		// case "":
		//     return (
		//         <WopbToolbarGroup text={"Image"}>
		//             <WopbToolbarDropdown
		//                 buttonContent={settingsIcon}
		//                 store={store}
		//                 label={__("Product Image Settings", "product-blocks")}
		//                 content={formatSettingsForToolbar([
		//                     {
		//                         name: "tab",
		//                         title: __(
		//                             "Product Image Settings",
		//                             "product-blocks"
		//                         ),
		//                         options: getData(),
		//                     },
		//                 ])}
		//             />
		//         </WopbToolbarGroup>
		//     );
		default:
			return null;
	}
}
