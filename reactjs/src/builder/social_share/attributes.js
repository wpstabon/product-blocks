const attributes = {
	blockId: {
		type: 'string',
		default: '',
	},
	currentPostId: { type: 'string', default: '' },
	enableLabel: {
		type: 'boolean',
		default: false,
	},
	repeatableSocialShare: {
		type: 'array',
		fields: {
			type: {
				type: 'string',
				default: 'facebook',
			},
			enableLabel: {
				type: 'boolean',
				default: true,
			},
			label: {
				type: 'string',
				default: 'Share',
			},
			iconColor: {
				type: 'string',
				default: '#fff',
				style: [
					{
						selector:
							'' +
							'{{WOPB}} {{REPEAT_CLASS}}.wopb-share-item a .wopb-share-item-icon svg, {{WOPB}} {{REPEAT_CLASS}}.wopb-share-item a .wopb-share-item-icon svg path { fill:{{iconColor}} !important; } ' +
							'{{WOPB}} {{REPEAT_CLASS}}.wopb-share-item .wopb-share-item-label { color:{{iconColor}} }',
					},
				],
			},
			iconColorHover: {
				type: 'string',
				default: '#d2d2d2',
				style: [
					{
						selector:
							'' +
							'{{WOPB}} {{REPEAT_CLASS}}.wopb-share-item:hover .wopb-share-item-icon svg, {{WOPB}} {{REPEAT_CLASS}}.wopb-share-item:hover .wopb-share-item-icon svg path { fill:{{iconColorHover}} !important; } ' +
							'{{WOPB}} {{REPEAT_CLASS}}.wopb-share-item:hover .wopb-share-item-label{ color:{{iconColorHover}} }',
					},
				],
			},
			shareBg: {
				type: 'string',
				default: '#7a49ff',
				style: [
					{
						selector:
							'{{WOPB}} {{REPEAT_CLASS}}.wopb-share-item a { background-color: {{shareBg}}; }',
					},
				],
			},
			shareBgHover: {
				type: 'object',
				default: '#7a49ff',
				style: [
					{
						selector:
							'{{WOPB}} {{REPEAT_CLASS}}.wopb-share-item a:hover { background-color:{{shareBgHover}}; }',
					},
				],
			},
		},
		default: [
			{
				type: 'facebook',
				enableLabel: true,
				label: 'Facebook',
				iconColor: '#fff',
				iconColorHover: '#d2d2d2',
				shareBg: '#3B5998',
				bgHoverColor: '#f5f5f5',
			},
			{
				type: 'twitter',
				enableLabel: true,
				label: 'Twitter',
				iconColor: '#fff',
				iconColorHover: '#d2d2d2',
				shareBg: '#000',
				bgHoverColor: '#f5f5f5',
			},
			{
				type: 'pinterest',
				enableLabel: true,
				label: 'Pinterest',
				iconColor: '#fff',
				iconColorHover: '#d2d2d2',
				shareBg: '#CB2128',
				bgHoverColor: '#f5f5f5',
			},
			{
				type: 'linkedin',
				enableLabel: true,
				label: 'Linkedin',
				iconColor: '#fff',
				iconColorHover: '#d2d2d2',
				shareBg: '#007BB6',
				bgHoverColor: '#f5f5f5',
			},
			{
				type: 'mail',
				enableLabel: true,
				label: 'Mail',
				iconColor: '#fff',
				iconColorHover: '#d2d2d2',
				shareBg: '#EA4335',
				bgHoverColor: '#f5f5f5',
			},
			// {
			//     type: "whatsapp",
			//     enableLabel: true,
			//     label: "WhatsApp",
			//     iconColor: "#fff",
			//     iconColorHover: "#d2d2d2",
			//     shareBg: "#25d366",
			//     bgHoverColor: "#f5f5f5"
			// },
			// {
			//     type: "skype",
			//     enableLabel: true,
			//     label: "Skype",
			//     iconColor: "#fff",
			//     iconColorHover: "#d2d2d2",
			//     shareBg: "#009edc",
			//     bgHoverColor: "#f5f5f5"
			// }
		],
	},
	shareItemTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: {
				lg: '15',
				unit: 'px',
			},
			height: {
				lg: '25',
				unit: 'px',
			},
			decoration: 'none',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{
				selector:
					'{{WOPB}} {{REPEAT_CLASS}}.wopb-share-item a .wopb-share-item-label',
			},
		],
	},
	shareIconSize: {
		type: 'object',
		default: {
			lg: '20',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} {{REPEAT_CLASS}} .wopb-share-item .wopb-share-item-icon svg { height:{{shareIconSize}}; width:{{shareIconSize}};}',
			},
		],
	},
	shareBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#c3c3c3',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} {{REPEAT_CLASS}}.wopb-share-item a',
			},
		],
	},
	shareRadius: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} {{REPEAT_CLASS}}.wopb-share-item a { border-radius:{{shareRadius}}; }',
			},
		],
	},
	disInline: {
		type: 'boolean',
		default: true,
	},
	itemPadding: {
		type: 'object',
		default: {
			lg: {
				top: '10',
				bottom: '10',
				left: '10',
				right: '10',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-share-item-inner-block .wopb-share-item a { padding:{{itemPadding}} !important; }',
			},
		],
	},
	itemSpacing: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '0',
				left: '',
				right: '4',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-share-item-inner-block .wopb-share-item {margin:{{itemSpacing}} !important; }',
			},
		],
	},
	itemContentAlign: {
		type: 'string',
		default: 'flex-start',
		style: [
			{
				depends: [
					{
						key: 'disInline',
						condition: '==',
						value: true,
					},
					{
						key: 'enableSticky',
						condition: '==',
						value: false,
					},
				],
				selector:
					'{{WOPB}} .wopb-share-layout {justify-content:{{itemContentAlign}}; width: 100%;}',
			},
		],
	},
	itemAlign: {
		type: 'string',
		default: 'left',
		style: [
			{
				depends: [
					{
						key: 'disInline',
						condition: '==',
						value: false,
					},
					{
						key: 'enableSticky',
						condition: '==',
						value: false,
					},
				],
				selector: '{{WOPB}} .wopb-share { text-align:{{itemAlign}}; }',
			},
		],
	},
	shareLabelShow: {
		type: 'boolean',
		default: true,
	},
	shareLabelIconColor: {
		type: 'string',
		default: '#ff6345',
		style: [
			{
				depends: [
					{
						key: 'shareLabelShow',
						condition: '==',
						value: true,
					},
					{
						key: 'shareLabelStyle',
						condition: '==',
						value: 'style2',
					},
				],
				selector:
					'{{WOPB}} .wopb-share-icon-section svg{ fill:{{shareLabelIconColor}}; }',
			},
		],
	},
	shareLabelIconSize: {
		type: 'object',
		default: {
			lg: '20',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-share-icon-section svg { height:{{shareLabelIconSize}}; width:{{shareLabelIconSize}};}',
			},
		],
	},
	shareLabelStyle: {
		type: 'string',
		default: 'style2',
		style: [
			{
				depends: [
					{
						key: 'shareLabelShow',
						condition: '==',
						value: true,
					},
					{
						key: 'shareLabelStyle',
						condition: '==',
						value: 'style1',
					},
				],
				selector:
					'{{WOPB}} .wopb-share-layout{display: flex; align-items:center;}',
			},
			{
				depends: [
					{
						key: 'shareLabelShow',
						condition: '==',
						value: true,
					},
					{
						key: 'shareLabelStyle',
						condition: '==',
						value: 'style2',
					},
				],
				selector:
					'{{WOPB}} .wopb-share-layout{display: flex; align-items:center;}',
			},
			{
				depends: [
					{
						key: 'shareLabelShow',
						condition: '==',
						value: true,
					},
					{
						key: 'shareLabelStyle',
						condition: '==',
						value: 'style3',
					},
				],
				selector: '{{WOPB}} .wopb-share-layout{display: inline-block}',
			},
			{
				depends: [
					{
						key: 'shareLabelShow',
						condition: '==',
						value: true,
					},
					{
						key: 'shareLabelStyle',
						condition: '==',
						value: 'style4',
					},
				],
				selector: '{{WOPB}} .wopb-share-layout{display: flex}',
			},
		],
	},
	shareCountShow: {
		type: 'boolean',
		default: false,
		style: [
			{
				depends: [
					{
						key: 'shareLabelStyle',
						condition: '==',
						value: 'style1',
					},
					{
						key: 'shareLabelShow',
						condition: '==',
						value: true,
					},
				],
			},
			{
				depends: [
					{
						key: 'shareLabelStyle',
						condition: '==',
						value: 'style3',
					},
					{
						key: 'shareLabelShow',
						condition: '==',
						value: true,
					},
				],
			},
		],
	},
	Labels1BorderColor: {
		type: 'string',
		default: '#',
		style: [
			{
				depends: [
					{
						key: 'shareLabelShow',
						condition: '==',
						value: true,
					},
					{
						key: 'shareLabelStyle',
						condition: '==',
						value: 'style1',
					},
				],
				selector:
					'{{WOPB}} .wopb-share-count-section-style1, {{WOPB}} .wopb-share-count-section-style1:after {border-color:{{Labels1BorderColor}} !important; }',
			},
		],
	},
	shareCountColor: {
		type: 'string',
		default: '#',
		style: [
			{
				depends: [
					{
						key: 'shareLabelStyle',
						condition: '==',
						value: 'style1',
					},
					{
						key: 'shareCountShow',
						condition: '==',
						value: true,
					},
					{
						key: 'shareLabelShow',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-share-count {color:{{shareCountColor}} !important; }',
			},
			{
				depends: [
					{
						key: 'shareLabelStyle',
						condition: '==',
						value: 'style3',
					},
					{
						key: 'shareCountShow',
						condition: '==',
						value: true,
					},
					{
						key: 'shareLabelShow',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-share-count {color:{{shareCountColor}} !important; }',
			},
		],
	},
	shareCountTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: {
				lg: '14',
				unit: 'px',
			},
			height: {
				lg: '20',
				unit: 'px',
			},
			decoration: 'none',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{
				depends: [
					{
						key: 'shareLabelStyle',
						condition: '==',
						value: 'style1',
					},
					{
						key: 'shareCountShow',
						condition: '==',
						value: true,
					},
					{
						key: 'shareLabelShow',
						condition: '==',
						value: true,
					},
				],
				selector: '{{WOPB}} .wopb-share-count',
			},
			{
				depends: [
					{
						key: 'shareLabelStyle',
						condition: '==',
						value: 'style3',
					},
					{
						key: 'shareCountShow',
						condition: '==',
						value: true,
					},
					{
						key: 'shareLabelShow',
						condition: '==',
						value: true,
					},
				],
				selector: '{{WOPB}} .wopb-share-count',
			},
		],
	},
	shareCountLabel: {
		type: 'string',
		default: 'Shares',
		style: [
			{
				depends: [
					{
						key: 'shareLabelShow',
						condition: '==',
						value: true,
					},
					{
						key: 'shareLabelStyle',
						condition: '!=',
						value: 'style2',
					},
				],
			},
		],
	},
	shareLabelColor: {
		type: 'string',
		default: '#',
		style: [
			{
				depends: [
					{
						key: 'shareLabelStyle',
						condition: '!=',
						value: 'style2',
					},
					{
						key: 'shareCountLabel',
						condition: '!=',
						value: '',
					},
					{
						key: 'shareLabelShow',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-share-count-section .wopb-share-label {color:{{shareLabelColor}}; }',
			},
		],
	},
	shareLabelTypo: {
		type: 'object',
		default: {
			openTypography: 0,
			size: {
				lg: '12',
				unit: 'px',
			},
			height: {
				lg: '',
				unit: 'px',
			},
			decoration: 'none',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{
				depends: [
					{
						key: 'shareLabelStyle',
						condition: '!=',
						value: 'style2',
					},
					{
						key: 'shareCountLabel',
						condition: '!=',
						value: '',
					},
					{
						key: 'shareLabelShow',
						condition: '==',
						value: true,
					},
				],
				selector: '{{WOPB}} .wopb-share-label',
			},
		],
	},
	shareLabelBackground: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [
					{
						key: 'shareLabelShow',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-share-count-section, {{WOPB}} .wopb-share-count-section-style1::after {background-color:{{shareLabelBackground}}; }',
			},
		],
	},
	shareLabelBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#c3c3c3',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{
						key: 'shareLabelShow',
						condition: '==',
						value: true,
					},
					{
						key: 'shareLabelStyle',
						condition: '!=',
						value: 'style1',
					},
				],
				selector:
					'{{WOPB}} .wopb-share-count-section, {{WOPB}} .wopb-share-count-section-style1::after',
			},
		],
	},
	shareLabelRadius: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				depends: [
					{
						key: 'shareLabelShow',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-share-count-section { border-radius:{{shareLabelRadius}}; }',
			},
		],
	},
	shareLabelPadding: {
		type: 'object',
		default: {
			lg: {
				top: '9',
				bottom: '9',
				left: '10',
				right: '10',
				unit: 'px',
			},
		},
		style: [
			{
				depends: [
					{
						key: 'shareLabelShow',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-share-count-section { padding:{{shareLabelPadding}}; }',
			},
		],
	},
	shareLabelMargin: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '4', unit: 'px' },
		},
		style: [
			{
				depends: [
					{ key: 'shareLabelShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-share-count-section { margin:{{shareLabelMargin}}; }',
			},
		],
	},
	enableSticky: {
		type: 'boolean',
		default: false,
		style: [
			{
				depends: [
					{
						key: 'enableSticky',
						condition: '==',
						value: false,
					},
				],
				selector:
					'{{WOPB}} .wopb-share-layout {display: flex; align-items:center;} ',
			},
			{
				depends: [
					{
						key: 'enableSticky',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-share .wopb-share-count-section { margin-bottom: 15px; }',
			},
		],
	},
	itemPosition: {
		type: 'string',
		default: 'bottom',
		style: [
			{
				depends: [
					{
						key: 'enableSticky',
						condition: '==',
						value: true,
					},
				],
			},
		],
	},
	stickyLeftOffset: {
		type: 'string',
		default: '20',
		style: [
			{
				depends: [
					{
						key: 'enableSticky',
						condition: '==',
						value: true,
					},
					{
						key: 'itemPosition',
						condition: '!=',
						value: 'right',
					},
				],
				selector:
					'{{WOPB}} .wopb-share .wopb-share-layout { position:fixed;left:{{stickyLeftOffset}}px;}',
			},
		],
	},
	stickyRightOffset: {
		type: 'string',
		default: '20',
		style: [
			{
				depends: [
					{
						key: 'enableSticky',
						condition: '==',
						value: true,
					},
					{
						key: 'itemPosition',
						condition: '==',
						value: 'right',
					},
				],
				selector:
					'{{WOPB}} .wopb-share .wopb-share-layout { position:fixed; right:{{stickyRightOffset}}px;}',
			},
		],
	},
	stickyTopOffset: {
		type: 'string',
		default: '20',
		style: [
			{
				depends: [
					{
						key: 'enableSticky',
						condition: '==',
						value: true,
					},
					{
						key: 'itemPosition',
						condition: '!=',
						value: 'bottom',
					},
				],
				selector:
					'{{WOPB}} .wopb-share .wopb-share-layout { position:fixed;top:{{stickyTopOffset}}px; z-index:9999999;}',
			},
		],
	},
	stickyBottomOffset: {
		type: 'string',
		default: '20',
		style: [
			{
				depends: [
					{
						key: 'itemPosition',
						condition: '==',
						value: 'bottom',
					},
					{
						key: 'enableSticky',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-share .wopb-share-layout { position:fixed;bottom:{{stickyBottomOffset}}px; z-index:9999999;}',
			},
		],
	},
	resStickyPost: {
		type: 'boolean',
		default: false,
		style: [
			{
				depends: [
					{
						key: 'enableSticky',
						condition: '==',
						value: true,
					},
				],
			},
		],
	},
	floatingResponsive: {
		type: 'string',
		default: '600',
		style: [
			{
				depends: [
					{
						key: 'enableSticky',
						condition: '==',
						value: true,
					},
					{
						key: 'resStickyPost',
						condition: '==',
						value: true,
					},
					{
						key: 'resStickyPost',
						condition: '==',
						value: true,
					},
				],
				selector:
					'@media only screen and (max-width: {{floatingResponsive}}px) {.wopb-share-layout {bottom: 0px !important; top: auto !important; left: 0px !important; right: auto !important;}}',
			},
			{
				depends: [
					{
						key: 'enableSticky',
						condition: '==',
						value: true,
					},
					{
						key: 'resStickyPost',
						condition: '==',
						value: false,
					},
				],
				selector:
					'@media only screen and (max-width: {{floatingResponsive}}px) {.wopb-share-layout {position: unset !important;}}',
			},
		],
	},
	advanceId: {
		type: 'string',
		default: '',
	},
	advanceZindex: {
		type: 'string',
		default: '',
		style: [
			{
				selector: '{{WOPB}} {z-index:{{advanceZindex}};}',
			},
		],
	},
	wrapMargin: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '20',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '',
				left: '',
				right: '',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: {
			openColor: 0,
			type: 'color',
			color: '#f5f5f5',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapRadius: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: {
			openColor: 0,
			type: 'color',
			color: '#ff176b',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapHoverRadius: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapInnerPadding: {
		type: 'object',
		default: {
			lg: {
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ padding:{{wrapInnerPadding}}; }',
			},
		],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: {
		type: 'string',
		default: '',
		style: [
			{
				selector: '',
			},
		],
	},
};
export default attributes;
