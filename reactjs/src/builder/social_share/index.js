const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';

registerBlockType( 'product-blocks/social-share', {
	title: __( 'Social Share', 'product-blocks' ),
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/svg/share.svg' }
			alt={ __( 'Social Share', 'product-blocks' ) }
		/>
	),
	category: 'single-product',
	description: __(
		'Display Social Share Buttons to let visitors share posts to their social profiles.',
		'product-blocks'
	),
	keywords: [
		__( 'Share', 'product-blocks' ),
		__( 'Social Share', 'product-blocks' ),
		__( 'Social Media', 'product-blocks' ),
		__( 'Share', 'product-blocks' ),
	],
	supports: {
		align: [ 'center', 'wide', 'full' ],
	},
	edit: Edit,
	save() {
		return null;
	},
	attributes,
} );
