const { __ } = wp.i18n;
import {
	CommonSettings,
	GeneralAdvanced,
	CustomCssAdvanced,
	ResponsiveAdvanced,
	spacingIcon,
	filterFields,
	SpacingArg,
} from '../../helper/CommonPanel';
import { Sections, Section } from '../../helper/Sections';
import WopbToolbarGroup from '../../helper/toolbar/WopbToolbarGroup';
import WopbToolbarDropdown from '../../helper/toolbar/WopbToolbarDropdown';
import { formatSettingsForToolbar } from '../../helper/ux';

const Settings = ( { store } ) => {
	return (
		<Sections>
			<Section slug="setting" title={ __( 'Setting', 'product-blocks' ) }>
				<CommonSettings
					title={ __( 'General', 'product-blocks' ) }
					store={ store }
					initialOpen={ true }
					include={ [
						{
							position: 1,
							data: {
								type: 'repeatable',
								key: 'repeatableSocialShare',
								label: __(
									'Social Share Style',
									'product-blocks'
								),
								fields: [
									{
										type: 'select',
										key: 'type',
										label: __(
											'Social Media',
											'product-blocks'
										),
										options: [
											{
												value: 'facebook',
												label: __(
													'Facebook',
													'product-blocks'
												),
											},
											{
												value: 'twitter',
												label: __(
													'Twitter',
													'product-blocks'
												),
											},
											{
												value: 'messenger',
												label: __(
													'Messenger',
													'product-blocks'
												),
											},
											{
												value: 'linkedin',
												label: __(
													'Linkedin',
													'product-blocks'
												),
											},
											{
												value: 'reddit',
												label: __(
													'Reddit',
													'product-blocks'
												),
											},
											{
												value: 'mail',
												label: __(
													'Mail',
													'product-blocks'
												),
											},
											{
												value: 'whatsapp',
												label: __(
													'WhatsApp',
													'product-blocks'
												),
											},
											{
												value: 'skype',
												label: __(
													'Skype',
													'product-blocks'
												),
											},
											{
												value: 'pinterest',
												label: __(
													'Pinterest',
													'product-blocks'
												),
											},
										],
									},
									{
										type: 'toggle',
										key: 'enableLabel',
										label: __(
											'Label Enable',
											'product-blocks'
										),
									},
									{
										type: 'text',
										key: 'label',
										label: __( 'Label', 'product-blocks' ),
									},
									{
										type: 'color',
										key: 'iconColor',
										label: __( 'Color', 'product-blocks' ),
									},
									{
										type: 'color',
										key: 'iconColorHover',
										label: __(
											'Color Hover',
											'product-blocks'
										),
									},
									{
										type: 'color',
										key: 'shareBg',
										label: __(
											'Background',
											'product-blocks'
										),
									},
									{
										type: 'color',
										key: 'shareBgHover',
										label: __(
											'Hover Background',
											'product-blocks'
										),
									},
								],
							},
						},
						{
							position: 2,
							data: {
								type: 'toggle',
								key: 'enableLabel',
								label: __( 'Label Enable', 'product-blocks' ),
							},
						},
					] }
				/>
				<CommonSettings
					title={ __( 'Item Setting', 'product-blocks' ) }
					include={ [
						{
							data: {
								type: 'typography',
								key: 'shareItemTypo',
								label: __(
									'Share Item Typography',
									'product-blocks'
								),
							},
						},
						{
							data: {
								type: 'range',
								key: 'shareIconSize',
								min: 0,
								max: 150,
								unit: false,
								responsive: true,
								step: 1,
								label: __( 'Icon Size', 'product-blocks' ),
							},
						},
						{
							data: {
								type: 'border',
								key: 'shareBorder',
								label: __( 'Border', 'product-blocks' ),
							},
						},
						{
							data: {
								type: 'dimension',
								key: 'shareRadius',
								label: __( 'Border Radius', 'product-blocks' ),
								step: 1,
								unit: true,
								responsive: true,
							},
						},
						{
							data: {
								type: 'toggle',
								key: 'disInline',
								label: __( 'Item Inline', 'product-blocks' ),
							},
						},
						{
							data: {
								type: 'dimension',
								key: 'itemPadding',
								min: 0,
								max: 80,
								step: 1,
								unit: true,
								responsive: true,
								label: __( 'Item Padding', 'product-blocks' ),
							},
						},
						{
							data: {
								type: 'dimension',
								key: 'itemSpacing',
								min: 0,
								max: 80,
								step: 1,
								unit: true,
								responsive: true,
								label: __( 'Spacing', 'product-blocks' ),
							},
						},
						{
							data: {
								type: 'group',
								key: 'itemContentAlign',
								options: [
									{ label: 'Left', value: 'flex-start' },
									{ label: 'Center', value: 'center' },
									{ label: 'Right', value: 'flex-end' },
								],
								justify: true,
								label: __( 'Alignment', 'product-blocks' ),
							},
						},
						{
							data: {
								type: 'alignment',
								key: 'itemAlign',
								responsive: false,
								label: __( 'Alignment', 'product-blocks' ),
								disableJustify: true,
							},
						},
					] }
					store={ store }
				/>
				<CommonSettings
					title={ __( 'Share Label & Count', 'product-blocks' ) }
					include={ [
						{
							data: {
								type: 'toggle',
								key: 'shareLabelShow',
								label: __(
									'Show Share label & Count',
									'product-blocks'
								),
							},
						},
						{
							data: {
								type: 'select',
								key: 'shareLabelStyle',
								label: __(
									'Share Label Style',
									'product-blocks'
								),
								options: [
									{
										value: 'style1',
										label: __(
											'Style 1',
											'product-blocks'
										),
									},
									{
										value: 'style2',
										label: __(
											'Style 2',
											'product-blocks'
										),
									},
									{
										value: 'style3',
										label: __(
											'Style 3',
											'product-blocks'
										),
									},
									{
										value: 'style4',
										label: __(
											'Style 4',
											'product-blocks'
										),
									},
								],
							},
						},
						{
							data: {
								type: 'color',
								key: 'shareLabelIconColor',
								label: __( 'Icon Color', 'product-blocks' ),
							},
						},
						{
							data: {
								type: 'range',
								key: 'shareLabelIconSize',
								min: 0,
								max: 150,
								unit: false,
								responsive: true,
								step: 1,
								label: __( 'Icon Size', 'product-blocks' ),
							},
						},

						{
							data: {
								type: 'color',
								key: 'Labels1BorderColor',
								label: __( 'Border Color', 'product-blocks' ),
							},
						},
						{
							data: {
								type: 'color',
								key: 'shareLabelBackground',
								label: __(
									'Background Color',
									'product-blocks'
								),
							},
						},
						{
							data: {
								type: 'border',
								key: 'shareLabelBorder',
								label: __( 'Border', 'product-blocks' ),
							},
						},
						{
							data: {
								type: 'dimension',
								key: 'shareLabelRadius',
								label: __( 'Border Radius', 'product-blocks' ),
								step: 1,
								unit: true,
								responsive: true,
							},
						},
						{
							data: {
								type: 'dimension',
								key: 'shareLabelPadding',
								label: __( 'Padding', 'product-blocks' ),
								step: 1,
								unit: true,
								responsive: true,
							},
						},
						{
							data: {
								type: 'dimension',
								key: 'shareLabelMargin',
								label: __( 'Margin', 'product-blocks' ),
								step: 1,
								unit: true,
								responsive: true,
							},
						},
						{
							data: {
								type: 'toggle',
								key: 'shareCountShow',
								label: __( 'Share Count', 'product-blocks' ),
							},
						},
						{
							data: {
								type: 'color',
								key: 'shareCountColor',
								label: __( 'Count Color', 'product-blocks' ),
							},
						},
						{
							data: {
								type: 'typography',
								key: 'shareCountTypo',
								label: __(
									'Count Typography',
									'product-blocks'
								),
							},
						},
						{
							data: {
								type: 'text',
								key: 'shareCountLabel',
								label: __(
									'Share Count Label',
									'product-blocks'
								),
							},
						},
						{
							data: {
								type: 'color',
								key: 'shareLabelColor',
								label: __( 'Label Color', 'product-blocks' ),
							},
						},
						{
							data: {
								type: 'typography',
								key: 'shareLabelTypo',
								label: __(
									'Label Typography',
									'product-blocks'
								),
							},
						},
					] }
					store={ store }
				/>
				<CommonSettings
					title={ __( 'Sticky Position', 'product-blocks' ) }
					depend="enableSticky"
					include={ [
						{
							data: {
								type: 'select',
								key: 'itemPosition',
								label: __( 'Display Style', 'product-blocks' ),
								options: [
									{
										value: 'left',
										label: __( 'Left', 'product-blocks' ),
									},
									{
										value: 'right',
										label: __( 'right', 'product-blocks' ),
									},
									{
										value: 'bottom',
										label: __( 'bottom', 'product-blocks' ),
									},
								],
							},
						},
						{
							data: {
								type: 'range',
								key: 'stickyLeftOffset',
								min: 0,
								max: 1500,
								unit: false,
								responsive: false,
								step: 1,
								label: __( 'Left Offset', 'product-blocks' ),
							},
						},
						{
							data: {
								type: 'range',
								key: 'stickyRightOffset',
								min: 0,
								max: 1500,
								unit: false,
								responsive: false,
								step: 1,
								label: __( 'Right Offset', 'product-blocks' ),
							},
						},
						{
							data: {
								type: 'range',
								key: 'stickyTopOffset',
								min: 0,
								max: 1500,
								unit: false,
								responsive: false,
								step: 1,
								label: __( 'Top Offset', 'product-blocks' ),
							},
						},
						{
							data: {
								type: 'range',
								key: 'stickyBottomOffset',
								min: 0,
								max: 1500,
								unit: false,
								responsive: false,
								step: 1,
								label: __( 'Bottom Offset', 'product-blocks' ),
							},
						},
						{
							data: {
								type: 'toggle',
								key: 'resStickyPost',
								label: __(
									'Responsive Position Sticky',
									'product-blocks'
								),
							},
						},
						{
							data: {
								type: 'range',
								key: 'floatingResponsive',
								min: 0,
								max: 1200,
								unit: false,
								responsive: false,
								step: 1,
								label: __(
									'Horizontal floating responsiveness',
									'product-blocks'
								),
							},
						},
					] }
					store={ store }
				/>
			</Section>
			<Section
				slug="advanced"
				title={ __( 'Advanced', 'product-blocks' ) }
			>
				<GeneralAdvanced store={ store } />
				<ResponsiveAdvanced pro={ true } store={ store } />
				<CustomCssAdvanced store={ store } />
			</Section>
		</Sections>
	);
};

export default Settings;

export function AddSettingsToToolbar( { store } ) {
	return (
		<WopbToolbarGroup text={ 'Share' }>
			<WopbToolbarDropdown
				buttonContent={ spacingIcon }
				store={ store }
				label={ __( 'Share Social Spacing', 'product-blocks' ) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __( 'Social Share Spacing', 'product-blocks' ),
						options: filterFields(
							[
								{
									data: {
										type: 'range',
										key: 'wrapMargin',
										min: 0,
										max: 100,
										step: 1,
										unit: true,
										responsive: true,
										dimensionKey: 'top',
										label: __(
											'Margin Top',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'wrapMargin',
										min: 0,
										max: 100,
										step: 1,
										unit: true,
										responsive: true,
										dimensionKey: 'bottom',
										label: __(
											'Margin Bottom',
											'product-blocks'
										),
									},
								},
							],
							'__all',
							SpacingArg
						),
					},
				] ) }
			/>
		</WopbToolbarGroup>
	);
}
