/** @format */

const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import icons from '../../helper/icons';
import Settings, { AddSettingsToToolbar } from './Settings';
import { wopbSupport } from '../../helper/CommonPanel';
import ToolBarElement from '../../helper/ToolBarElement';
import useBlockId from '../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const {
		setAttributes,
		className,
		name,
		attributes,
		clientId,
		attributes: {
			blockId,
			advanceId,
			disInline,
			shareLabelShow,
			shareCountLabel,
			shareCountShow,
			repeatableSocialShare,
			shareLabelStyle,
			enableLabel,
		},
	} = props;
	const store = { setAttributes, name, attributes, clientId };

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/social-share', blockId );
	}

	return (
		<>
			<ToolBarElement
				store={ store }
				include={ [
					{
						type: 'block_spacing',
						label: __( 'Social Share Spacing', 'product-blocks' ),
					},
				] }
			/>
			<InspectorControls>
				<Settings store={ store } />
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar store={ store } />

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ `wopb-product-wrapper` }>
					<div className={ `wopb-share` }>
						<div
							className={ `wopb-share-layout wopb-inline-${ disInline }` }
						>
							{ shareLabelShow && (
								<div
									className={
										`wopb-share-count-section wopb-share-count-section-` +
										shareLabelStyle
									}
								>
									{ shareLabelStyle != 'style2' &&
										shareCountShow && (
											<span
												className={ `wopb-share-count` }
											>
												350
											</span>
										) }
									{ shareLabelStyle == 'style2' && (
										<span
											className={ `wopb-share-icon-section` }
										>
											{ icons.share }
										</span>
									) }
									{ shareLabelStyle != 'style2' &&
										shareCountLabel && (
											<span
												className={ `wopb-share-label` }
											>
												{ shareCountLabel }
											</span>
										) }
								</div>
							) }
							<div className={ `wopb-share-item-inner-block` }>
								{ repeatableSocialShare?.map(
									( content, i ) => {
										return (
											<div
												key={ i }
												className={ `wopb-share-item wopb-repeat-${ i } wopb-social-${ content.type }` }
											>
												<a
													href="#"
													className={ `wopb-share-item-${ content.type }` }
												>
													<span
														className={ `wopb-share-item-icon` }
													>
														{
															icons[
																content.type
															]
														}
													</span>
													{ enableLabel &&
														content.enableLabel && (
															<span
																className={ `wopb-share-item-label` }
															>
																{
																	content.label
																}
															</span>
														) }
												</a>
											</div>
										);
									}
								) }
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
}
