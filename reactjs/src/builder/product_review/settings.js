const { __ } = wp.i18n;
import {
	ButtonStyle,
	ButtonStyleArg,
	CommonSettings,
	CustomCssAdvanced,
	GeneralAdvanced,
	ResponsiveAdvanced,
	TypographyTB,
	colorIcon,
	filterFields,
	spacingIcon,
	styleIcon,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Section, Sections } from '../../helper/Sections';
import WopbToolbarDropdown from '../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar } from '../../helper/ux';

export default function Settings( { store } ) {
	const { section } = store;

	return (
		<>
			<Sections>
				<Section
					slug="setting"
					title={ __( 'Style', 'product-blocks' ) }
				>
					<CommonSettings
						initialOpen={ section.heading }
						title={ __( 'Heading', 'product-blocks' ) }
						store={ store }
						include={ [
							{
								position: 1,
								data: {
									type: 'toggle',
									key: 'reviewHeading',
									label: __(
										'Review heading Disable',
										'product-blocks'
									),
								},
							},
							{
								position: 2,
								data: {
									type: 'toggle',
									key: 'reviewAuthor',
									label: __(
										'Review Author Disable',
										'product-blocks'
									),
								},
							},
							{
								position: 3,
								data: {
									type: 'color',
									key: 'reviewHeadinglColor', // mind the typo
									label: __(
										'Heading Color',
										'product-blocks'
									),
								},
							},
							{
								position: 4,
								data: {
									type: 'typography',
									key: 'headingTypography',
									label: __(
										'Heading Typography',
										'product-blocks'
									),
								},
							},
							{
								position: 5,
								data: {
									type: 'range',
									min: 0,
									max: 80,
									key: 'headingSpace',
									label: __( 'Space', 'product-blocks' ),
								},
							},
						] }
					/>
					<CommonSettings
						initialOpen={ section[ 'review-style' ] }
						title={ __( 'Review Style', 'product-blocks' ) }
						store={ store }
						include={ [
							{
								position: 1,
								data: {
									type: 'color',
									key: 'reviewColor',
									label: __( 'Text Color', 'product-blocks' ),
								},
							},
							{
								position: 1,
								data: {
									type: 'color',
									key: 'reviewDescpColor',
									label: __(
										'DESCIPTION Color',
										'product-blocks'
									),
								},
							},
							{
								position: 2,
								data: {
									type: 'color',
									key: 'reviewStarColor',
									label: __(
										'Rating Bg Color',
										'product-blocks'
									),
								},
							},
							{
								position: 3,
								data: {
									type: 'color',
									key: 'reviewEmptyColor',
									label: __(
										'Empty Star Color',
										'product-blocks'
									),
								},
							},
							{
								position: 4,
								data: {
									type: 'typography',
									key: 'authorTypography',
									label: __(
										'Author Typography',
										'product-blocks'
									),
								},
							},
							{
								position: 5,
								data: {
									type: 'typography',
									key: 'dateTypography',
									label: __(
										'Date Typography',
										'product-blocks'
									),
								},
							},
							{
								position: 6,
								data: {
									type: 'typography',
									key: 'descTypography',
									label: __(
										'Desciption Typography',
										'product-blocks'
									),
								},
							},
							{
								position: 7,
								data: {
									type: 'range',
									min: 0,
									max: 100,
									key: 'authorSize',
									label: __(
										'Author Size',
										'product-blocks'
									),
								},
							},
							{
								position: 8,
								data: {
									type: 'range',
									min: 0,
									max: 80,
									key: 'authorSpace',
									label: __(
										'Space Between',
										'product-blocks'
									),
								},
							},
						] }
					/>
					<CommonSettings
						initialOpen={ section[ 'review-form-wrapper' ] }
						title={ __( 'Review Form Wrapper', 'product-blocks' ) }
						store={ store }
						include={ [
							{
								position: 1,
								data: {
									type: 'color',
									key: 'formBg',
									label: __( 'Background', 'product-blocks' ),
								},
							},
							{
								position: 2,
								data: {
									type: 'range',
									min: 0,
									max: 120,
									key: 'formSpace',
									label: __( 'Margin Top', 'product-blocks' ),
								},
							},
							{
								position: 3,
								data: {
									type: 'border',
									key: 'formBorder',
									label: __(
										'Form Border',
										'product-blocks'
									),
								},
							},
							{
								position: 4,
								data: {
									type: 'range',
									min: 0,
									max: 100,
									key: 'formRadius',
									label: __(
										'Form Radius',
										'product-blocks'
									),
								},
							},
							{
								position: 5,
								data: {
									type: 'dimension',
									key: 'reviewFormPadding',
									label: __(
										'Form Wrapper padding',
										'product-blocks'
									),
									step: 1,
									unit: true,
									responsive: true,
								},
							},
						] }
					/>
					<CommonSettings
						initialOpen={ section[ 'review-form-label' ] }
						title={ __( 'Review Form Label', 'product-blocks' ) }
						store={ store }
						include={ [
							{
								position: 1,
								data: {
									type: 'color',
									key: 'reviewFormLabelColor',
									label: __(
										'Label Color',
										'product-blocks'
									),
								},
							},
							{
								position: 3,
								data: {
									type: 'typography',
									key: 'reviewFormLabelTypo',
									label: __(
										'Label Typography',
										'product-blocks'
									),
								},
							},
						] }
					/>
					<CommonSettings
						initialOpen={ section[ 'review-form-input' ] }
						title={ __( 'Review Form Input', 'product-blocks' ) }
						store={ store }
						include={ [
							{
								position: 1,
								data: {
									type: 'color',
									key: 'reviewFormInputColor',
									label: __(
										'Input Color',
										'product-blocks'
									),
								},
							},
							{
								position: 2,
								data: {
									type: 'typography',
									key: 'reviewFormInputTypo',
									label: __(
										'Input Typography',
										'product-blocks'
									),
								},
							},
							{
								position: 3,
								data: {
									type: 'range',
									min: 0,
									max: 70,
									key: 'reviewFormSpace',
									label: __( 'Space', 'product-blocks' ),
								},
							},
							{
								position: 4,
								data: {
									type: 'border',
									key: 'reviewFormInputBorder',
									label: __( 'Border', 'product-blocks' ),
								},
							},
							{
								position: 5,
								data: {
									type: 'border',
									key: 'reviewFormInputFocus',
									label: __(
										'Input Focus',
										'product-blocks'
									),
								},
							},
							{
								position: 6,
								data: {
									type: 'range',
									min: 0,
									max: 100,
									key: 'reviewFormRadius',
									label: __(
										'Input Radius',
										'product-blocks'
									),
								},
							},
							{
								position: 7,
								data: {
									type: 'dimension',
									key: 'reviewFormInputPadding',
									label: __(
										'Input padding',
										'product-blocks'
									),
									step: 1,
									unit: true,
									responsive: true,
								},
							},
						] }
					/>
					<ButtonStyle
						store={ store }
						initialOpen={ section[ 'button-style' ] }
					/>
				</Section>
				<Section
					slug="advanced"
					title={ __( 'Advanced', 'product-blocks' ) }
				>
					<GeneralAdvanced store={ store } />
					<ResponsiveAdvanced pro={ true } store={ store } />
					<CustomCssAdvanced store={ store } />
				</Section>
			</Sections>
			{ wopbSupport() }
		</>
	);
}

export function AddSettingsToToolbar( { selected, store } ) {
	switch ( selected ) {
		case 'heading':
			return (
				<WopbToolbarGroup text={ 'Heading' }>
					<TypographyTB
						store={ store }
						attrKey={ 'headingTypography' }
						label={ __( 'Heading Typography', 'product-blocks' ) }
					/>
					<WopbToolbarDropdown
						store={ store }
						buttonContent={ styleIcon }
						label={ __( 'Heading Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Heading Style', 'product-blocks' ),
								options: [
									{
										type: 'color',
										key: 'reviewHeadinglColor', // mind the typo
										label: __(
											'Heading Color',
											'product-blocks'
										),
									},
									{
										type: 'range',
										min: 0,
										max: 80,
										key: 'headingSpace',
										label: __( 'Space', 'product-blocks' ),
									},
								],
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'astar':
			return (
				<WopbToolbarGroup text={ 'Rating' }>
					<WopbToolbarDropdown
						store={ store }
						buttonContent={ colorIcon }
						label={ __( 'Rating Color', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Rating Color', 'product-blocks' ),
								options: [
									{
										type: 'color',
										key: 'reviewStarColor',
										label: __(
											'Rating Bg Color',
											'product-blocks'
										),
									},
									{
										type: 'color',
										key: 'reviewEmptyColor',
										label: __(
											'Empty Star Color',
											'product-blocks'
										),
									},
								],
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'author':
			return (
				<WopbToolbarGroup text={ 'Author' }>
					<TypographyTB
						store={ store }
						attrKey={ 'authorTypography' }
						label={ __( 'Author Typography', 'product-blocks' ) }
					/>
				</WopbToolbarGroup>
			);
		case 'date':
			return (
				<WopbToolbarGroup text={ 'Date' }>
					<TypographyTB
						store={ store }
						attrKey={ 'dateTypography' }
						label={ __( 'Date Typography', 'product-blocks' ) }
					/>
				</WopbToolbarGroup>
			);
		case 'desc':
			return (
				<WopbToolbarGroup text={ 'Desc.' }>
					<TypographyTB
						store={ store }
						attrKey={ 'descTypography' }
						label={ __(
							'Description Typography',
							'product-blocks'
						) }
					/>
					<WopbToolbarDropdown
						store={ store }
						buttonContent={ colorIcon }
						label={ __( 'Description Color', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Description Color',
									'product-blocks'
								),
								options: [
									{
										type: 'color',
										key: 'reviewColor',
										label: __( 'Color', 'product-blocks' ),
									},
								],
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'image':
			return (
				<WopbToolbarGroup text={ 'Image' }>
					<WopbToolbarDropdown
						store={ store }
						buttonContent={ spacingIcon }
						label={ __( 'Author Dimension', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Author Dimension',
									'product-blocks'
								),
								options: [
									{
										type: 'range',
										min: 0,
										max: 100,
										key: 'authorSize',
										label: __(
											'Author Size',
											'product-blocks'
										),
									},
									{
										type: 'range',
										min: 0,
										max: 80,
										key: 'authorSpace',
										label: __(
											'Space Between',
											'product-blocks'
										),
									},
								],
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'button':
			return (
				<WopbToolbarGroup text={ 'Button' }>
					<TypographyTB
						store={ store }
						attrKey={ 'btnTypo' }
						label={ __( 'Button Typography', 'product-blocks' ) }
					/>
					<WopbToolbarDropdown
						store={ store }
						buttonContent={ spacingIcon }
						label={ __( 'Button Spacing', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Input Spacing', 'product-blocks' ),
								options: filterFields(
									[ 'btnSacing', 'btnPadding' ],
									'__all',
									ButtonStyleArg
								),
							},
						] ) }
					/>
					<WopbToolbarDropdown
						store={ store }
						buttonContent={ styleIcon }
						label={ __( 'Button Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Button Style', 'product-blocks' ),
								options: filterFields(
									null,
									[ 'btnSacing', 'btnPadding', 'btnTypo' ],
									ButtonStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'input':
			return (
				<WopbToolbarGroup text={ 'Input' }>
					<TypographyTB
						store={ store }
						attrKey={ 'reviewFormInputTypo' }
						label={ __( 'Input Typography', 'product-blocks' ) }
					/>
					<WopbToolbarDropdown
						store={ store }
						buttonContent={ spacingIcon }
						label={ __( 'Input Spacing', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Input Spacing', 'product-blocks' ),
								options: [
									{
										type: 'range',
										min: 0,
										max: 70,
										key: 'reviewFormSpace',
										label: __( 'Space', 'product-blocks' ),
									},
									{
										type: 'dimension',
										key: 'reviewFormInputPadding',
										label: __(
											'Input padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								],
							},
						] ) }
					/>
					<WopbToolbarDropdown
						store={ store }
						buttonContent={ styleIcon }
						label={ __( 'Input Style', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Input Style', 'product-blocks' ),
								options: [
									{
										type: 'color',
										key: 'reviewFormInputColor',
										label: __(
											'Input Color',
											'product-blocks'
										),
									},
									{
										type: 'border',
										key: 'reviewFormInputBorder',
										label: __( 'Border', 'product-blocks' ),
									},
									{
										type: 'border',
										key: 'reviewFormInputFocus',
										label: __(
											'Input Focus',
											'product-blocks'
										),
									},
									{
										type: 'range',
										min: 0,
										max: 100,
										key: 'reviewFormRadius',
										label: __(
											'Input Radius',
											'product-blocks'
										),
									},
								],
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'label':
			return (
				<WopbToolbarGroup text={ 'Label' }>
					<TypographyTB
						store={ store }
						attrKey={ 'reviewFormLabelColor' }
						label={ __( 'Label Typography', 'product-blocks' ) }
					/>
					<WopbToolbarDropdown
						store={ store }
						buttonContent={ colorIcon }
						label={ __( 'Label Color', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Label Color', 'product-blocks' ),
								options: [
									{
										type: 'color',
										key: 'reviewFormLabelColor',
										label: __(
											'Label Color',
											'product-blocks'
										),
									},
									{
										type: 'typography',
										key: 'reviewFormLabelTypo',
										label: __(
											'Label Typography',
											'product-blocks'
										),
									},
								],
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		default:
			return null;
	}
}
