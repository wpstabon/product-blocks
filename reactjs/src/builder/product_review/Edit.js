/** @format */

const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import Settings, { AddSettingsToToolbar } from './settings';
import useFluentSettings from '../../helper/hooks/useFluentSettings';
import usePrevious from '../../helper/hooks/use-previous';
import useBlockId from '../../helper/hooks/use-block-id';

export default function Edit( props ) {
	useBlockId( props, true );

	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: { blockId, advanceId },
		context,
	} = props;
	const prevProps = usePrevious( props );
	const { section, setSection, setCurrSettings, toolbarSettings } =
		useFluentSettings( { props, prevProps } );

	const store = {
		setAttributes,
		name,
		attributes,
		clientId,
		section,
		setSection,
		context,
	};

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/product-review', blockId );
	}

	return (
		<>
			<InspectorControls>
				<Settings store={ store } />
			</InspectorControls>

			<AddSettingsToToolbar
				store={ store }
				selected={ toolbarSettings }
			/>

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ `wopb-product-wrapper` }>
					<div id="reviews" className={ `woocommerce-Reviews` }>
						<div id="comments">
							<h2
								className={ `woocommerce-Reviews-title wopb-component-simple` }
								onClick={ ( e ) =>
									setCurrSettings( e, 'heading' )
								}
							>
								1 reviews for{ ' ' }
								<span>Amet quam egestas sempe</span>
							</h2>
							<ol className={ `commentlist` }>
								<li
									className={ `review byuser comment-author-admin bypostauthor odd alt thread-odd thread-alt depth-1` }
								>
									<div className={ `comment_container` }>
										<img
											src="http://1.gravatar.com/avatar/1aedb8d9dc4751e229a335e371db8058?s=60&amp;d=mm&amp;r=g"
											className={ `avatar avatar-60 photo wopb-component-simple` }
											height="60"
											width="60"
											onClick={ ( e ) =>
												setCurrSettings(
													e,
													'review-style',
													'image'
												)
											}
										/>
										<div className={ `comment-text` }>
											<div
												className={ `star-rating wopb-component-simple` }
												onClick={ ( e ) =>
													setCurrSettings(
														e,
														'review-style',
														'astar'
													)
												}
											>
												<span
													style={ { width: '73%' } }
												></span>
											</div>
											<p className={ `meta` }>
												<strong
													className={ `woocommerce-review__author wopb-component-simple` }
													onClick={ ( e ) =>
														setCurrSettings(
															e,
															'review-style',
															'author'
														)
													}
												>
													Admin
												</strong>
												<span
													className={ `woocommerce-review__dash` }
												>
													–
												</span>
												<time
													className={ `woocommerce-review__published-date wopb-component-simple` }
													dateTime="2021-01-27T10:50:14+00:00"
													onClick={ ( e ) =>
														setCurrSettings(
															e,
															'review-style',
															'date'
														)
													}
												>
													January 27, 2021
												</time>
											</p>
											<div
												className={ `description wopb-component-simple` }
												onClick={ ( e ) =>
													setCurrSettings(
														e,
														'review-style',
														'desc'
													)
												}
											>
												<p>
													This is a sample review of
													the product.
												</p>
											</div>
										</div>
									</div>
								</li>
							</ol>
						</div>
						<div id="review_form_wrapper">
							<div id="review_form">
								<div
									id="respond"
									className={ `comment-respond` }
								>
									<span
										id="reply-title"
										className={ `comment-reply-title` }
									>
										Add a review
									</span>
									<form
										id="commentform"
										className={ `comment-form` }
									>
										<div
											className={ `comment-form-rating` }
										>
											<label
												className="wopb-component-simple"
												onClick={ ( e ) =>
													setCurrSettings(
														e,
														'review-form-label',
														'label'
													)
												}
											>
												Your rating&nbsp;
												<span className="required">
													*
												</span>
											</label>
											<p
												className={ `stars` }
												onClick={ ( e ) =>
													setCurrSettings(
														e,
														'review-style',
														'astar'
													)
												}
											>
												<span className="wopb-component-simple">
													<a
														className={ `star-1` }
														href="#"
													>
														1
													</a>
													<a
														className={ `star-2` }
														href="#"
													>
														2
													</a>
													<a
														className={ `star-3` }
														href="#"
													>
														3
													</a>
													<a
														className={ `star-4` }
														href="#"
													>
														4
													</a>
													<a
														className={ `star-5` }
														href="#"
													>
														5
													</a>
												</span>
											</p>
										</div>
										<p className={ `comment-form-comment` }>
											<label
												className="wopb-component-simple"
												onClick={ ( e ) =>
													setCurrSettings(
														e,
														'review-form-label',
														'label'
													)
												}
											>
												Your review&nbsp;
												<span className={ `required` }>
													*
												</span>
											</label>
											<textarea
												id="comment"
												cols={ 45 }
												rows={ 8 }
												required={ true }
												onClick={ ( e ) =>
													setCurrSettings(
														e,
														'review-form-input',
														'input'
													)
												}
											></textarea>
										</p>
										<p className={ `form-submit` }>
											<input
												name="submit"
												type="submit"
												id="submit"
												className="submit wopb-component-simple"
												value="Submit"
												onClick={ ( e ) => {
													e.preventDefault();
													setCurrSettings(
														e,
														'button-style',
														'button'
													);
												} }
											/>
										</p>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
}
