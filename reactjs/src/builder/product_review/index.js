const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';
registerBlockType( 'product-blocks/product-review', {
	title: __( 'Product Review', 'product-blocks' ),
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/builder/review.svg' }
			alt={ __( 'Product Review', 'product-blocks' ) }
		/>
	),
	category: 'single-product',
	description: __(
		'Dynamically control position, size, and typography of product reviews.',
		'product-blocks'
	),
	keywords: [
		__( 'Product Review', 'product-blocks' ),
		__( 'Review', 'product-blocks' ),
		__( 'Description', 'product-blocks' ),
	],
	supports: {
		align: [ 'center', 'wide', 'full' ],
	},
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
