/** @format */

const { __ } = wp.i18n;
const { addQueryArgs } = wp.url;
const { useState, useEffect } = wp.element;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import {
	CommonSettings,
	GeneralAdvanced,
	CustomCssAdvanced,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Sections, Section } from '../../helper/Sections';
import { AddSettingsToToolbar } from './Settings';
import useBlockId from '../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const [ postsList, setPostsList ] = useState( [] );
	const [ data, setData ] = useState( { title: 'Sample Product Title' } );
	const [ prev, setPrev ] = useState( '' );

	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: { blockId, advanceId, titleTag, previews },
	} = props;

	useBlockId( props, true );

	useEffect( () => {
		setPrev( previews );
		fetchProducts();
	}, [] );

	useEffect( () => {
		if ( prev != previews ) {
			fetchProducts();
			setPrev( previews );
		}
	}, [ previews ] );

	function fetchProducts() {
		const query = addQueryArgs( '/wopb/preview', {
			previews,
			wpnonce: wopb_data.security,
			type: 'title',
		} );
		wp.apiFetch( { path: query } ).then( ( obj ) => {
			if ( obj.type == 'data' ) {
				setData( obj.data );
			} else {
				setPostsList( obj.list );
			}
		} );
	}

	const store = { setAttributes, name, attributes, clientId };
	const TitleTag = titleTag;

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/product-title', blockId );
	}

	return (
		<>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Style', 'product-blocks' ) }
					>
						<CommonSettings
							initialOpen={ true }
							title={ 'inline' }
							include={ [
								{
									position: 1,
									data: {
										type: 'select',
										key: 'previews',
										label: __(
											'Preview Products',
											'product-blocks'
										),
										options: postsList || [],
									},
								},
								{
									position: 2,
									data: {
										type: 'tag',
										key: 'titleTag',
										label: __( 'Tag', 'product-blocks' ),
									},
								},
								{
									position: 3,
									data: {
										type: 'alignment',
										key: 'titleAlign',
										disableJustify: true,
										responsive: true,
										label: __(
											'Alignment',
											'product-blocks'
										),
									},
								},
								{
									position: 4,
									data: {
										type: 'color',
										key: 'titleColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									position: 5,
									data: {
										type: 'typography',
										key: 'titleTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
							] }
							store={ store }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar store={ store } />

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ `wopb-product-wrapper` }>
					<TitleTag className={ `wopb-builder-product-title` }>
						{ data.title }
					</TitleTag>
				</div>
			</div>
		</>
	);
}
