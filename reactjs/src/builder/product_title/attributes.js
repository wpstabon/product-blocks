const attributes = {
	blockId: {
		type: 'string',
		default: '',
	},
	currentPostId: { type: 'string', default: '' },
	previews: {
		type: 'string',
		default: '',
	},
	titleTag: {
		type: 'string',
		default: 'h1',
	},
	titleAlign: {
		type: 'object',
		default: {
			lg: 'left',
		},
		style: [
			{
				selector: '{{WOPB}} {text-align:{{titleAlign}};}',
			},
		],
	},
	titleColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-builder-product-title, .edit-post-visual-editor .block-editor-block-list__block {{WOPB}} .wopb-builder-product-title {color:{{titleColor}};}',
			},
		],
	},
	titleTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: {
				lg: '32',
				unit: 'px',
			},
			height: {
				lg: '40',
				unit: 'px',
			},
			decoration: '',
			transform: '',
			family: '',
			weight: '600',
		},
		style: [
			{
				selector:
					'.single-product {{WOPB}} .wopb-builder-product-title ,.editor-styles-wrapper {{WOPB}} .wopb-builder-product-title',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: {
			openColor: 0,
			type: 'color',
			color: '#f5f5f5',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapRadius: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: {
			openColor: 0,
			type: 'color',
			color: '#ff176b',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapHoverRadius: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper:hover{border-radius:{{wrapHoverRadius}};}',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapMargin: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '20',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper {margin:{{wrapMargin}};}',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '',
				left: '',
				right: '',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper {padding:{{wrapOuterPadding}};}',
			},
		],
	},
	advanceId: {
		type: 'string',
		default: '',
	},
	advanceZindex: {
		type: 'string',
		default: '',
		style: [
			{
				selector: '{{WOPB}} {z-index:{{advanceZindex}};}',
			},
		],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: {
		type: 'string',
		default: '',
		style: [
			{
				selector: '',
			},
		],
	},
};
export default attributes;
