const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';
registerBlockType( 'product-blocks/product-title', {
	title: __( 'Product Title', 'product-blocks' ),
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/builder/title.svg' }
			alt={ __( 'Product Title', 'product-blocks' ) }
		/>
	),
	category: 'single-product',
	description: __(
		'Dynamically control position, size, and typography of product titles.',
		'product-blocks'
	),
	keywords: [
		__( 'title', 'product-blocks' ),
		__( 'product title', 'product-blocks' ),
		__( 'heading', 'product-blocks' ),
	],
	supports: {
		align: [ 'center', 'wide', 'full' ],
	},
	edit: Edit,
	save() {
		return null;
	},
	attributes,
} );
