const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';
registerBlockType( 'product-blocks/product-tab', {
	title: __( 'Product Tab', 'product-blocks' ),
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/builder/tab.svg' }
			alt={ __( 'Product Tab', 'product-blocks' ) }
		/>
	),
	category: 'single-product',
	description: __(
		'Dynamically control position, size, and typography of product tabs.',
		'product-blocks'
	),
	keywords: [
		__( 'Tab', 'product-blocks' ),
		__( 'Product Tab', 'product-blocks' ),
	],
	supports: {
		align: [ 'center', 'wide', 'full' ],
	},
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
