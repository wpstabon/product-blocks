/** @format */

const { __ } = wp.i18n;
import { settingsIcon } from '../../helper/CommonPanel';
import WopbToolbarDropdown from '../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar, getData } from '../../helper/ux';

export function AddSettingsToToolbar( { store, selected } ) {
	switch ( selected ) {
		case 'navigation':
			return (
				<WopbToolbarGroup text={ 'Nav' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Navigation Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Navigation Settings',
									'product-blocks'
								),
								options: getData( [
									{
										position: 1,
										data: {
											type: 'divider',
											key: 'navDevider',
											label: __(
												'Navigation Style',
												'product-blocks'
											),
										},
									},
									{
										position: 2,
										data: {
											type: 'alignment',
											key: 'navAlignment',
											responsive: true,
											disableJustify: true,
											label: __(
												'Alignment',
												'product-blocks'
											),
										},
									},
									{
										position: 2,
										data: {
											type: 'typography',
											key: 'navTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										position: 3,
										data: {
											type: 'color',
											key: 'navtextColor',
											label: __(
												'color',
												'product-blocks'
											),
										},
									},
									{
										position: 4,
										data: {
											type: 'color',
											key: 'navHoverTextColor',
											label: __(
												'Hover color',
												'product-blocks'
											),
										},
									},
									{
										position: 5,
										data: {
											type: 'color',
											key: 'navbgcolor',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										position: 6,
										data: {
											type: 'color',
											key: 'navhoverBgColor',
											label: __(
												'hover Bg',
												'product-blocks'
											),
										},
									},
									{
										position: 7,
										data: {
											type: 'border',
											key: 'navBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										position: 8,
										data: {
											type: 'dimension',
											key: 'navRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										position: 9,
										data: {
											type: 'dimension',
											key: 'navpadding',
											step: 1,
											unit: true,
											responsive: true,
											label: __(
												'padding',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'tabBorder',
											label: __(
												'Tab Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'tabRadius',
											label: __(
												'Tab Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'tabPadding',
											label: __(
												'Tab Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										position: 5,
										data: {
											type: 'range',
											min: 0,
											max: 80,
											key: 'tabSpaceBetween',
											label: __(
												'Space Between',
												'product-blocks'
											),
										},
									},
									{
										position: 5,
										data: {
											type: 'range',
											min: 0,
											max: 80,
											key: 'tabBottomSpace',
											label: __(
												'Bottom Space',
												'product-blocks'
											),
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'description-tab':
			return (
				<WopbToolbarGroup text={ 'Desc.' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Description Tab Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Description Tab Settings',
									'product-blocks'
								),
								options: getData( [
									{
										position: 2,
										data: {
											type: 'alignment',
											key: 'descAlignment',
											responsive: true,
											disableJustify: false,
											label: __(
												'Alignment',
												'product-blocks'
											),
										},
									},
									{
										position: 6,
										data: {
											type: 'color',
											key: 'descColor',
											label: __(
												'Text Color',
												'product-blocks'
											),
										},
									},
									{
										position: 7,
										data: {
											type: 'typography',
											key: 'descTypo',
											label: __(
												'Text Typography',
												'product-blocks'
											),
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'additional-information-tab':
			return (
				<WopbToolbarGroup text={ 'Info' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Additional Information Tab Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Additional Information Tab Settings',
									'product-blocks'
								),
								options: getData( [
									{
										position: 2,
										data: {
											type: 'dimension',
											key: 'infoPadding',
											label: __(
												'Table Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										position: 3,
										data: {
											type: 'text',
											key: 'headingText',
											label: __(
												'Heading Text',
												'product-blocks'
											),
										},
									},
									{
										position: 4,
										data: {
											type: 'color',
											key: 'headingColor',
											label: __(
												'Heading Color',
												'product-blocks'
											),
										},
									},
									{
										position: 5,
										data: {
											type: 'typography',
											key: 'HeadingTypo',
											label: __(
												'Heading Typography',
												'product-blocks'
											),
										},
									},
									{
										position: 6,
										data: {
											type: 'divider',
											key: 'additionalLabel',
											label: __(
												'Additional label',
												'product-blocks'
											),
										},
									},
									{
										position: 7,
										data: {
											type: 'typography',
											key: 'labelTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										position: 8,
										data: {
											type: 'color',
											key: 'labelColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										position: 9,
										data: {
											type: 'color',
											key: 'labelBg',
											label: __(
												'Background Color',
												'product-blocks'
											),
										},
									},
									{
										position: 10,
										data: {
											type: 'divider',
											key: 'additionalLabel',
											label: __(
												'Additional value',
												'product-blocks'
											),
										},
									},
									{
										position: 11,
										data: {
											type: 'typography',
											key: 'addInfoValueTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										position: 12,
										data: {
											type: 'color',
											key: 'addInoValueColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										position: 13,
										data: {
											type: 'color',
											key: 'addInfoValueBg',
											label: __(
												'Background Color',
												'product-blocks'
											),
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'review':
			return (
				<WopbToolbarGroup text={ 'Review' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Review Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Review Settings',
									'product-blocks'
								),
								options: getData( [
									{
										position: 2,
										data: {
											type: 'toggle',
											key: 'reviewAuthor',
											label: __(
												'Review Author Disable',
												'product-blocks'
											),
										},
									},
									{
										position: 3,
										data: {
											type: 'color',
											key: 'reviewHeadinglColor',
											label: __(
												'Heading Color',
												'product-blocks'
											),
										},
									},
									{
										position: 4,
										data: {
											type: 'typography',
											key: 'headingTypography',
											label: __(
												'Heading Typography',
												'product-blocks'
											),
										},
									},
									{
										position: 5,
										data: {
											type: 'divider',
											key: 'navDevider',
											label: __(
												'Review Style',
												'product-blocks'
											),
										},
									},
									{
										position: 6,
										data: {
											type: 'color',
											key: 'reviewColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										position: 7,
										data: {
											type: 'color',
											key: 'reviewStarColor',
											label: __(
												'Star Color',
												'product-blocks'
											),
										},
									},
									{
										position: 8,
										data: {
											type: 'color',
											key: 'reviewEmptyColor',
											label: __(
												'Empty Color',
												'product-blocks'
											),
										},
									},
									{
										position: 9,
										data: {
											type: 'typography',
											key: 'authorTypography',
											label: __(
												'Author Typography',
												'product-blocks'
											),
										},
									},
									{
										position: 10,
										data: {
											type: 'typography',
											key: 'dateTypography',
											label: __(
												'Date Typography',
												'product-blocks'
											),
										},
									},
									{
										position: 11,
										data: {
											type: 'typography',
											key: 'descTypography',
											label: __(
												'Desciption Typography',
												'product-blocks'
											),
										},
									},
									{
										position: 12,
										data: {
											type: 'range',
											min: 0,
											max: 100,
											key: 'authorSize',
											label: __(
												'Author Size',
												'product-blocks'
											),
										},
									},
									{
										position: 13,
										data: {
											type: 'divider',
											key: 'navDevider',
											label: __(
												'Review Form Label',
												'product-blocks'
											),
										},
									},
									{
										position: 14,
										data: {
											type: 'color',
											key: 'reviewFormLabelColor',
											label: __(
												'Label Color',
												'product-blocks'
											),
										},
									},
									{
										position: 15,
										data: {
											type: 'color',
											key: 'reviewFormRequiredColor',
											label: __(
												'Required Color',
												'product-blocks'
											),
										},
									},
									{
										position: 16,
										data: {
											type: 'typography',
											key: 'reviewFormLabelTypo',
											label: __(
												'Label Typography',
												'product-blocks'
											),
										},
									},
									{
										position: 17,
										data: {
											type: 'divider',
											key: 'navDevider',
											label: __(
												'Review Form Input',
												'product-blocks'
											),
										},
									},
									{
										position: 18,
										data: {
											type: 'color',
											key: 'reviewFormInputColor',
											label: __(
												'Input Color',
												'product-blocks'
											),
										},
									},
									{
										position: 19,
										data: {
											type: 'color',
											key: 'reviewFormInputBg',
											label: __(
												'Input Background',
												'product-blocks'
											),
										},
									},
									{
										position: 19,
										data: {
											type: 'color',
											key: 'reviewFormInputBorder',
											label: __(
												'Input Border',
												'product-blocks'
											),
										},
									},
									{
										position: 20,
										data: {
											type: 'color',
											key: 'reviewFormInputFocusColor',
											label: __(
												'Input Focus Color',
												'product-blocks'
											),
										},
									},
									{
										position: 21,
										data: {
											type: 'typography',
											key: 'reviewFormInputTypo',
											label: __(
												'Input Typography',
												'product-blocks'
											),
										},
									},
									{
										position: 22,
										data: {
											type: 'range',
											min: 0,
											max: 40,
											key: 'reviewFormSpace',
											label: __(
												'Space',
												'product-blocks'
											),
										},
									},
									{
										position: 23,
										data: {
											type: 'range',
											min: 0,
											max: 40,
											key: 'reviewFormRadius',
											label: __(
												'Input Radius',
												'product-blocks'
											),
										},
									},
									{
										position: 24,
										data: {
											type: 'dimension',
											key: 'reviewFormInputPadding',
											label: __(
												'Input padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										position: 25,
										data: {
											type: 'divider',
											key: 'navDevider',
											label: __(
												'Button Style',
												'product-blocks'
											),
										},
									},
									{
										position: 26,
										data: {
											type: 'typography',
											key: 'btnTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										position: 27,
										data: {
											type: 'color',
											key: 'btnColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										position: 28,
										data: {
											type: 'color2',
											key: 'btnBgColor',
											label: __(
												'Background Color',
												'product-blocks'
											),
										},
									},
									{
										position: 29,
										data: {
											type: 'border',
											key: 'btnBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										position: 30,
										data: {
											type: 'dimension',
											key: 'btnRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										position: 31,
										data: {
											type: 'boxshadow',
											key: 'btnShadow',
											label: __(
												'BoxShadow',
												'product-blocks'
											),
										},
									},
									{
										position: 32,
										data: {
											type: 'dimension',
											key: 'btnPadding',
											label: __(
												'padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										position: 33,
										data: {
											type: 'dimension',
											key: 'btnSpacing',
											label: __(
												'Spacing',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										position: 34,
										data: {
											type: 'divider',
											key: 'navDevider',
											label: __(
												'Hover Style',
												'product-blocks'
											),
										},
									},
									{
										position: 35,
										data: {
											type: 'color',
											key: 'btnHoverColor',
											label: __(
												'Hover Color',
												'product-blocks'
											),
										},
									},
									{
										position: 36,
										data: {
											type: 'color2',
											key: 'btnBgHoverColor',
											label: __(
												'Hover Bg Color',
												'product-blocks'
											),
										},
									},
									{
										position: 37,
										data: {
											type: 'border',
											key: 'btnHoverBorder',
											label: __(
												'Hover Border',
												'product-blocks'
											),
										},
									},
									{
										position: 38,
										data: {
											type: 'dimension',
											key: 'btnHoverRadius',
											label: __(
												'Hover Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										position: 39,
										data: {
											type: 'boxshadow',
											key: 'btnHoverShadow',
											label: __(
												'Hover BoxShadow',
												'product-blocks'
											),
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		default:
			return null;
	}
}
