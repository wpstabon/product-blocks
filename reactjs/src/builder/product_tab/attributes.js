const attributes = {
	blockId: {
		type: 'string',
		default: '',
	},
	currentPostId: { type: 'string', default: '' },
	showDescription: {
		type: 'boolean',
		default: true,
	},
	showAddInfo: {
		type: 'boolean',
		default: true,
	},
	showReview: {
		type: 'boolean',
		default: true,
	},
	navDevider: {
		type: 'string',
		default: '',
	},
	navAlignment: {
		type: 'object',
		default: { lg: 'left' },
		style: [
			{
				selector:
					'.woocommerce {{WOPB}} div.product .woocommerce-tabs ul.tabs, {{WOPB}} div.product .woocommerce-tabs ul.tabs { text-align:{{navAlignment}}; }',
			},
		],
	},
	navTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: {
				lg: '16',
				unit: 'px',
			},
			height: {
				lg: '26',
				unit: 'px',
			},
			decoration: '',
			transform: '',
			family: '',
			weight: '600',
		},
		style: [
			{
				selector:
					`body {{WOPB}} .woocommerce-tabs ul li > a,
					body.woocommerce div.product {{WOPB}} .woocommerce-tabs ul.tabs li a,
					body .woocommerce-js div.product {{WOPB}} .woocommerce-tabs ul li > a,
					body div.editor-styles-wrapper {{WOPB}} .woocommerce-tabs ul li > a
					`
			},
		],
	},
	navtextColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'.woocommerce {{WOPB}} div.product .woocommerce-tabs ul.tabs li a, {{WOPB}} div.product .woocommerce-tabs ul.tabs li a {color:{{navtextColor}}}',
			},
		],
	},
	navHoverTextColor: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				selector:
					'.woocommerce {{WOPB}} div.product .woocommerce-tabs ul.tabs li a:hover, {{WOPB}} div.product .woocommerce-tabs ul.tabs li a:hover, .woocommerce {{WOPB}} div.product .woocommerce-tabs ul.tabs li.active a, div.product .woocommerce-tabs ul.tabs li.active a {color:{{navHoverTextColor}}}',
			},
		],
	},
	navbgcolor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'.woocommerce {{WOPB}} div.product .woocommerce-tabs ul.tabs li a, {{WOPB}} div.product .woocommerce-tabs ul.tabs li a {background:{{navbgcolor}};}',
			},
		],
	},
	navhoverBgColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'.woocommerce {{WOPB}} div.product .woocommerce-tabs ul.tabs li a:hover, {{WOPB}} div.product .woocommerce-tabs ul.tabs li a:hover, .woocommerce {{WOPB}} div.product .woocommerce-tabs ul.tabs li.active a, div.product .woocommerce-tabs ul.tabs li.active a {background-color:{{navhoverBgColor}};}',
			},
		],
	},
	navBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: {
				top: 0,
				right: 0,
				bottom: 0,
				left: 0,
			},
			color: '#333',
			type: 'solid',
		},
		style: [
			{
				selector:
					'.woocommerce {{WOPB}} div.product .woocommerce-tabs ul.tabs li a, {{WOPB}} div.product .woocommerce-tabs ul.tabs li a',
			},
		],
	},
	navRadius: {
		type: 'object',
		default: {
			lg: '2',
			unit: 'px',
		},
		style: [
			{
				selector:
					'.woocommerce {{WOPB}} div.product .woocommerce-tabs ul.tabs li a, {{WOPB}} div.product .woocommerce-tabs ul.tabs li a { border-radius:{{navRadius}}; }',
			},
		],
	},
	tabBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: {
				top: 0,
				right: 0,
				bottom: 0,
				left: 0,
			},
			color: '#333',
			type: 'solid',
		},
		style: [
			{
				selector:
					'.woocommerce {{WOPB}} div.product .woocommerce-tabs ul.tabs,{{WOPB}} div.product .woocommerce-tabs ul.tabs',
			},
		],
	},
	tabRadius: {
		type: 'object',
		default: {
			lg: '2',
			unit: 'px',
		},
		style: [
			{
				selector:
					'.woocommerce {{WOPB}} div.product .woocommerce-tabs ul.tabs, {{WOPB}} div.product .woocommerce-tabs ul.tabs { border-radius:{{tabRadius}}; }',
			},
		],
	},
	tabPadding: {
		type: 'object',
		default: {
			lg: {
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'.woocommerce {{WOPB}} div.product .woocommerce-tabs ul.tabs, {{WOPB}} div.product .woocommerce-tabs ul.tabs {padding:{{tabPadding}}}',
			},
		],
	},
	tabSpaceBetween: {
		type: 'string',
		default: '24',
		style: [
			{
				selector:
					'{{WOPB}} div.product .woocommerce-tabs ul.tabs > li:not(:last-child) { margin-right:{{tabSpaceBetween}}px !important;}',
			},
		],
	},
	tabBottomSpace: {
		type: 'string',
		default: '20',
		style: [
			{
				selector:
					' {{WOPB}} div.product .woocommerce-tabs ul.tabs { margin-bottom: {{tabBottomSpace}}px !important;}',
			},
		],
	},
	navpadding: {
		type: 'object',
		default: {
			lg: { unit: 'px', top: '0', right: '0', bottom: '0', left: '0' },
		},
		style: [
			{
				selector:
					'.woocommerce {{WOPB}} div.wopb-product-wrapper div.product .woocommerce-tabs ul.tabs li a, {{WOPB}} div.product .woocommerce-tabs ul.tabs li a, .editor-styles-wrapper {{WOPB}} div.product .woocommerce-tabs ul.tabs li a{ padding:{{navpadding}}}',
			},
		],
	},
	descColor: {
		type: 'string',
		default: '#4A4A4A',
		style: [
			{
				selector:
					' {{WOPB}} .woocommerce-Tabs-panel--description > p{color:{{descColor}};}',
			},
		],
	},

	descTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: {
				lg: '16',
				unit: 'px',
			},
			height: {
				lg: '26',
				unit: 'px',
			},
			decoration: '',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{
				selector: '{{WOPB}} .woocommerce-Tabs-panel--description > p',
			},
		],
	},
	descAlignment: {
		type: 'object',
		default: { lg: 'left' },
		style: [
			{
				selector:
					'{{WOPB}} .woocommerce-Tabs-panel--description > p { text-align:{{descAlignment}}; }',
			},
		],
	},

	infoPadding: {
		type: 'object',
		default: {
			lg: {
				unit: 'px',
				top: '12',
				right: '20',
				bottom: '12',
				left: '20',
			},
		},
		style: [
			{
				selector:
					'.woocommerce div.product {{WOPB}} .woocommerce-product-attributes tr td, .woocommerce div.product {{WOPB}} .woocommerce-product-attributes tr th, {{WOPB}} .woocommerce-product-attributes tr td, {{WOPB}} .woocommerce-product-attributes tr th { padding:{{infoPadding}} !important;}',
			},
		],
	},

	headingColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .woocommerce-Tabs-panel--additional_information > h2{color:{{headingColor}};}',
			},
		],
	},

	labelTypo: {
		type: 'object',
		default: {
			openTypography: 0,
			size: {
				lg: '',
				unit: 'px',
			},
			height: {
				lg: '',
				unit: 'px',
			},
			decoration: '',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{
				selector:
					'{{WOPB}} .woocommerce-product-attributes-item__label',
			},
		],
	},
	labelColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .woocommerce-product-attributes-item__label {color:{{labelColor}};}',
			},
		],
	},
	labelBg: {
		type: 'string',
		default: '#ededed',
		style: [
			{
				selector:
					'{{WOPB}} .woocommerce-product-attributes-item__label , .woocommerce {{WOPB}} table.shop_attributes tr:nth-child(even) th , .woocommerce {{WOPB}} table.shop_attributes tr > th{ background:{{labelBg}} !important; }',
			},
		],
	},
	addInfoValueBg: {
		type: 'string',
		default: '#f5f5f5',
		style: [
			{
				selector:
					' {{WOPB}} .woocommerce-product-attributes-item__value ,  .woocommerce {{WOPB}} table.shop_attributes tr:nth-child(even) td ,  .editor-styles-wrapper {{WOPB}} table:not( .has-background ) tbody tr:nth-child(2n) td ,  .woocommerce {{WOPB}} table.shop_attributes tr > td {background:{{addInfoValueBg}} !important;} ',
			},
		],
	},
	addInoValueColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .woocommerce-product-attributes-item__value > p, {{WOPB}} .woocommerce-product-attributes-item__value {color:{{addInoValueColor}};}',
			},
		],
	},
	addInfoValueTypo: {
		type: 'object',
		default: {
			openTypography: 0,
			size: {
				lg: '',
				unit: 'px',
			},
			height: {
				lg: '',
				unit: 'px',
			},
			decoration: '',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{
				selector:
					'{{WOPB}} .woocommerce-product-attributes-item__value > p, {{WOPB}} .woocommerce-product-attributes-item__value',
			},
		],
	},
	reviewHeading: {
		type: 'boolean',
		default: true,
	},
	reviewAuthor: {
		type: 'boolean',
		default: false,
		style: [
			{
				depends: [
					{
						key: 'reviewAuthor',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .comment_container > img {display:none;} {{WOPB}} .woocommerce {{WOPB}} #reviews #comments ol.commentlist li .comment-text{margin:0px 0px 0px 0px}',
			},
		],
	},
	reviewHeadinglColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				depends: [
					{
						key: 'reviewHeading',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .woocommerce-Reviews-title , .edit-post-visual-editor .block-editor-block-list__block {{WOPB}} .woocommerce-Reviews h2 {color:{{reviewHeadinglColor}};}',
			},
		],
	},
	headingTypography: {
		type: 'object',
		default: {
			openTypography: 1,
			size: {
				lg: '20',
				unit: 'px',
			},
			height: {
				lg: '28',
				unit: 'px',
			},
			decoration: '',
			transform: '',
			family: '',
			weight: '600',
		},
		style: [
			{
				depends: [
					{
						key: 'reviewHeading',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-tabs .woocommerce-Reviews-title',
			},
		],
	},
	reviewColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .woocommerce-review__author, {{WOPB}} .woocommerce-review__dash, {{WOPB}} .woocommerce-review__published-date {color:{{reviewColor}};}',
			},
		],
	},
	reviewDescpColor: {
		type: 'string',
		default: '#4A4A4A',
		style: [
			{
				selector:
					'{{WOPB}} .description > p {color:{{reviewDescpColor}};}',
			},
		],
	},
	reviewStarColor: {
		type: 'string',
		default: '#FFAF38',
		style: [
			{
				selector:
					'{{WOPB}} .star-rating span::before, {{WOPB}} #reviews p.stars a::before{color:{{reviewStarColor}} !important;}',
			},
		],
	},
	reviewEmptyColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .star-rating::before{color:{{reviewEmptyColor}};}',
			},
		],
	},
	authorTypography: {
		type: 'object',
		default: {
			openTypography: 1,
			size: {
				lg: '16',
				unit: 'px',
			},
			height: {
				lg: '20',
				unit: 'px',
			},
			decoration: '',
			transform: '',
			family: '',
			weight: '600',
		},
		style: [
			{
				selector: '{{WOPB}} .woocommerce-review__author',
			},
		],
	},
	dateTypography: {
		type: 'object',
		default: {
			openTypography: 0,
			size: {
				lg: '',
				unit: 'px',
			},
			height: {
				lg: '',
				unit: 'px',
			},
			decoration: '',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{
				selector: '{{WOPB}} #reviews .commentlist li time',
			},
		],
	},
	descTypography: {
		type: 'object',
		default: {
			openTypography: 0,
			size: {
				lg: '',
				unit: 'px',
			},
			height: {
				lg: '',
				unit: 'px',
			},
			decoration: '',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{
				selector: '{{WOPB}} .description > p',
			},
		],
	},
	authorSize: {
		type: 'string',
		default: '48',
		style: [
			{
				selector:
					'{{WOPB}} #reviews #comments .commentlist li img.avatar, .woocommerce {{WOPB}} #reviews #comments .commentlist li img.avatar , .woocommerce {{WOPB}} #reviews #comments ol.commentlist li img.avatar{width:{{authorSize}}px;}',
			},
		],
	},
	reviewFormLabelColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} #reviews #reply-title, {{WOPB}} #reviews .comment-form-rating label, {{WOPB}} #respond .comment-form-comment label {color:{{reviewFormLabelColor}};}',
			},
		],
	},
	reviewFormRequiredColor: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				selector:
					'{{WOPB}} #reviews .comment-form-rating label .required, {{WOPB}} #respond .comment-form-comment label .required {color:{{reviewFormRequiredColor}};}',
			},
		],
	},
	reviewFormLabelTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: {
				lg: '16',
				unit: 'px',
			},
			height: {
				lg: '25',
				unit: 'px',
			},
			decoration: '',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{
				selector:
					'{{WOPB}} #reviews .comment-form-rating label, {{WOPB}} #respond .comment-form-comment label',
			},
		],
	},
	reviewFormInputColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} #respond .comment-form-comment textarea {color:{{reviewFormInputColor}};}',
			},
		],
	},
	reviewFormInputBg: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} #respond .comment-form-comment textarea {background-color:{{reviewFormInputBg}};}',
			},
		],
	},
	reviewFormInputBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#D2D2D2',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} #respond .comment-form-comment textarea',
			},
		],
	},
	reviewFormInputFocusColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} #respond .comment-form-comment textarea:focus{border-color:{{reviewFormInputFocusColor}};box-shadow:none;}',
			},
		],
	},
	reviewFormInputTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: {
				lg: '14',
				unit: 'px',
			},
			height: {
				lg: '22',
				unit: 'px',
			},
			decoration: '',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{
				selector: '{{WOPB}} #respond .comment-form-comment textarea',
			},
		],
	},
	reviewFormSpace: {
		type: 'string',
		default: '12',
		style: [
			{
				selector:
					'{{WOPB}} #respond .comment-form-comment textarea , {{WOPB}} #respond form .stars {margin-top:{{reviewFormSpace}}px;} ',
			},
		],
	},
	reviewFormRadius: {
		type: 'string',
		default: '0',
		style: [
			{
				selector:
					'{{WOPB}} #respond .comment-form-comment textarea {border-radius:{{reviewFormRadius}}px;}',
			},
		],
	},
	reviewFormInputPadding: {
		type: 'object',
		default: {
			lg: {
				top: 12,
				right: 16,
				left: 16,
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} #respond .comment-form-comment textarea {padding:{{reviewFormInputPadding}};}',
			},
		],
	},
	btnTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: {
				lg: '16',
				unit: 'px',
			},
			height: {
				lg: '20',
				unit: 'px',
			},
			spacing: {
				lg: 0,
				unit: 'px',
			},
			transform: '',
			weight: '500',
			decoration: '',
			family: '',
		},
		style: [
			{
				selector:
					'{{WOPB}} #review_form #respond #submit , .woocommerce {{WOPB}} #review_form #respond #submit',
			},
		],
	},
	btnColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} #review_form #respond #submit , .woocommerce {{WOPB}} #review_form #respond #submit {color:{{btnColor}};}',
			},
		],
	},
	btnBgColor: {
		type: 'object',
		default: {
			openColor: 1,
			type: 'color',
			color: '#FF176B',
		},
		style: [
			{
				selector:
					'{{WOPB}} #review_form #respond #submit, .woocommerce {{WOPB}} #review_form #respond #submit',
			},
		],
	},
	btnBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} #review_form #respond #submit , .woocommerce {{WOPB}} #review_form #respond #submit',
			},
		],
	},
	btnRadius: {
		type: 'object',
		default: {
			lg: '0',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} #review_form #respond #submit , .woocommerce {{WOPB}} #review_form #respond #submit { border-radius:{{btnRadius}};}',
			},
		],
	},
	btnShadow: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} #review_form #respond #submit , .woocommerce {{WOPB}} #review_form #respond #submit',
			},
		],
	},
	btnPadding: {
		type: 'object',
		default: {
			lg: {
				top: '14',
				bottom: '14',
				left: '28',
				right: '28',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} #review_form #respond #submit , .woocommerce {{WOPB}} #review_form #respond #submit{padding:{{btnPadding}};}',
			},
		],
	},
	btnSpacing: {
		type: 'object',
		default: {
			lg: {
				top: '32',
				bottom: '0',
				left: '0',
				right: '0',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} #review_form #respond #submit , .woocommerce {{WOPB}} #review_form #respond #submit {margin:{{btnSpacing}};}',
			},
		],
	},
	btnHoverColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} #review_form #respond #submit:hover , .woocommerce {{WOPB}} #review_form #respond #submit:hover { color:{{btnHoverColor}}; }',
			},
		],
	},
	btnBgHoverColor: {
		type: 'object',
		default: {
			openColor: 1,
			type: 'color',
			color: '#070707',
		},
		style: [
			{
				selector:
					'{{WOPB}} #review_form #respond #submit:hover , .woocommerce {{WOPB}} #review_form #respond #submit:hover',
			},
		],
	},
	btnHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} #review_form #respond #submit :hover , .woocommerce {{WOPB}} #review_form #respond #submit:hover',
			},
		],
	},
	btnHoverRadius: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} #review_form #respond #submit:hover , .woocommerce {{WOPB}} #review_form #respond #submit:hover { border-radius: {{btnHoverRadius}}; }',
			},
		],
	},
	btnHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				selector:
					'{{WOPB}} #review_form #respond #submit:hover , .woocommerce {{WOPB}} #review_form #respond #submit:hover',
			},
		],
	},
	wrapBg: {
		type: 'string',
		default: {
			openColor: 0,
			type: 'color',
			color: '#f5f5f5',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapRadius: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: {
			openColor: 0,
			type: 'color',
			color: '#ff176b',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapHoverRadius: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapMargin: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '20',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '',
				left: '',
				right: '',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	advanceId: {
		type: 'string',
		default: '',
	},
	advanceZindex: {
		type: 'string',
		default: '',
		style: [
			{
				selector: '{{WOPB}} {z-index:{{advanceZindex}};}',
			},
		],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: {
		type: 'string',
		default: '',
		style: [
			{
				selector: '',
			},
		],
	},
};
export default attributes;
