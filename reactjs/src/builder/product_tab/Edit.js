const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import {
	CommonSettings,
	GeneralAdvanced,
	CustomCssAdvanced,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Sections, Section } from '../../helper/Sections';
import useFluentSettings from '../../helper/hooks/useFluentSettings';
import { AddSettingsToToolbar } from './Settings';
import ToolBarElement from '../../helper/ToolBarElement';
import useBlockId from '../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: {
			blockId,
			advanceId,
			showDescription,
			showAddInfo,
			showReview,
			reviewHeading,
		},
	} = props;
	const store = { setAttributes, name, attributes, clientId };

	const { section, toolbarSettings, setCurrSettings } = useFluentSettings();

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/product-tab', blockId );
	}
	const current = showDescription ? 'desc' : showAddInfo ? 'info' : 'revi';
	return (
		<>
			<ToolBarElement
				store={ store }
				include={ [
					{
						type: 'block_spacing',
						label: __( 'Product Tab Spacing', 'product-blocks' ),
					},
				] }
			/>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Style', 'product-blocks' ) }
					>
						<CommonSettings
							initialOpen={ section.navigation }
							title={ __( 'Navigation', 'product-blocks' ) }
							include={ [
								{
									position: 1,
									data: {
										type: 'alignment',
										key: 'navAlignment',
										responsive: true,
										disableJustify: true,
										label: __(
											'Alignment',
											'product-blocks'
										),
									},
								},
								{
									position: 5,
									data: {
										type: 'border',
										key: 'tabBorder',
										label: __(
											'Tab Border',
											'product-blocks'
										),
									},
								},
								{
									position: 10,
									data: {
										type: 'dimension',
										key: 'tabRadius',
										label: __(
											'Tab Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									position: 15,
									data: {
										type: 'dimension',
										key: 'tabPadding',
										label: __(
											'Tab Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									position: 20,
									data: {
										type: 'range',
										min: 0,
										max: 80,
										key: 'tabSpaceBetween',
										label: __(
											'Space Between',
											'product-blocks'
										),
									},
								},
								{
									position: 25,
									data: {
										type: 'range',
										min: 0,
										max: 80,
										key: 'tabBottomSpace',
										label: __(
											'Bottom Space',
											'product-blocks'
										),
									},
								},
							] }
							store={ store }
						/>
						<CommonSettings
							initialOpen={ false }
							title={ __( 'Navigation Item', 'product-blocks' ) }
							include={ [
								{
									position: 2,
									data: {
										type: 'typography',
										key: 'navTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									position: 3,
									data: {
										type: 'color',
										key: 'navtextColor',
										label: __( 'color', 'product-blocks' ),
									},
								},
								{
									position: 4,
									data: {
										type: 'color',
										key: 'navHoverTextColor',
										label: __(
											'Active/Hover color',
											'product-blocks'
										),
									},
								},
								{
									position: 5,
									data: {
										type: 'color',
										key: 'navbgcolor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									position: 6,
									data: {
										type: 'color',
										key: 'navhoverBgColor',
										label: __(
											'Active/Hover Bg',
											'product-blocks'
										),
									},
								},
								{
									position: 7,
									data: {
										type: 'border',
										key: 'navBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									position: 8,
									data: {
										type: 'dimension',
										key: 'navRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									position: 9,
									data: {
										type: 'dimension',
										key: 'navpadding',
										step: 1,
										unit: true,
										responsive: true,
										label: __(
											'padding',
											'product-blocks'
										),
									},
								},
							] }
							store={ store }
						/>
						<CommonSettings
							depend="showDescription"
							initialOpen={ section[ 'description-tab' ] }
							title={ __( 'Description Tab', 'product-blocks' ) }
							include={ [
								{
									position: 2,
									data: {
										type: 'alignment',
										key: 'descAlignment',
										responsive: true,
										disableJustify: false,
										label: __(
											'Alignment',
											'product-blocks'
										),
									},
								},
								{
									position: 6,
									data: {
										type: 'color',
										key: 'descColor',
										label: __(
											'Text Color',
											'product-blocks'
										),
									},
								},
								{
									position: 7,
									data: {
										type: 'typography',
										key: 'descTypo',
										label: __(
											'Text Typography',
											'product-blocks'
										),
									},
								},
							] }
							store={ store }
						/>
						<CommonSettings
							depend="showAddInfo"
							initialOpen={
								section[ 'additional-information-tab' ]
							}
							title={ __(
								'Addional Information Tab',
								'product-blocks'
							) }
							include={ [
								{
									position: 2,
									data: {
										type: 'dimension',
										key: 'infoPadding',
										label: __(
											'Table Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									position: 6,
									data: {
										type: 'divider',
										key: 'additionalLabel',
										label: __(
											'Additional label',
											'product-blocks'
										),
									},
								},
								{
									position: 7,
									data: {
										type: 'typography',
										key: 'labelTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									position: 8,
									data: {
										type: 'color',
										key: 'labelColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									position: 9,
									data: {
										type: 'color',
										key: 'labelBg',
										label: __(
											'Background Color',
											'product-blocks'
										),
									},
								},
								{
									position: 10,
									data: {
										type: 'divider',
										key: 'additionalLabel',
										label: __(
											'Additional value',
											'product-blocks'
										),
									},
								},
								{
									position: 11,
									data: {
										type: 'typography',
										key: 'addInfoValueTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									position: 12,
									data: {
										type: 'color',
										key: 'addInoValueColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									position: 13,
									data: {
										type: 'color',
										key: 'addInfoValueBg',
										label: __(
											'Background Color',
											'product-blocks'
										),
									},
								},
							] }
							store={ store }
						/>
						<CommonSettings
							depend="showReview"
							initialOpen={ section.review }
							title={ __( 'Review', 'product-blocks' ) }
							include={ [
								{
									position: 1,
									data: {
										type: 'toggle',
										key: 'reviewHeading',
										label: __(
											'Review Heading Enable',
											'product-blocks'
										),
									},
								},
								{
									position: 2,
									data: {
										type: 'toggle',
										key: 'reviewAuthor',
										label: __(
											'Review Author Disable',
											'product-blocks'
										),
									},
								},
								{
									position: 3,
									data: {
										type: 'color',
										key: 'reviewHeadinglColor',
										label: __(
											'Heading Color',
											'product-blocks'
										),
									},
								},
								{
									position: 4,
									data: {
										type: 'typography',
										key: 'headingTypography',
										label: __(
											'Heading Typography',
											'product-blocks'
										),
									},
								},
								{
									position: 5,
									data: {
										type: 'divider',
										key: 'navDevider',
										label: __(
											'Review Style',
											'product-blocks'
										),
									},
								},
								{
									position: 6,
									data: {
										type: 'color',
										key: 'reviewColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									position: 6,
									data: {
										type: 'color',
										key: 'reviewDescpColor',
										label: __(
											'DESCIPTION Color',
											'product-blocks'
										),
									},
								},
								{
									position: 7,
									data: {
										type: 'color',
										key: 'reviewStarColor',
										label: __(
											'Star Color',
											'product-blocks'
										),
									},
								},
								{
									position: 8,
									data: {
										type: 'color',
										key: 'reviewEmptyColor',
										label: __(
											'Empty Color',
											'product-blocks'
										),
									},
								},
								{
									position: 9,
									data: {
										type: 'typography',
										key: 'authorTypography',
										label: __(
											'Author Typography',
											'product-blocks'
										),
									},
								},
								{
									position: 10,
									data: {
										type: 'typography',
										key: 'dateTypography',
										label: __(
											'Date Typography',
											'product-blocks'
										),
									},
								},
								{
									position: 11,
									data: {
										type: 'typography',
										key: 'descTypography',
										label: __(
											'Desciption Typography',
											'product-blocks'
										),
									},
								},
								{
									position: 12,
									data: {
										type: 'range',
										min: 0,
										max: 100,
										key: 'authorSize',
										label: __(
											'Author Size',
											'product-blocks'
										),
									},
								},
								{
									position: 13,
									data: {
										type: 'divider',
										key: 'navDevider',
										label: __(
											'Review Form Label',
											'product-blocks'
										),
									},
								},
								{
									position: 14,
									data: {
										type: 'color',
										key: 'reviewFormLabelColor',
										label: __(
											'Label Color',
											'product-blocks'
										),
									},
								},
								{
									position: 15,
									data: {
										type: 'color',
										key: 'reviewFormRequiredColor',
										label: __(
											'Required Color',
											'product-blocks'
										),
									},
								},
								{
									position: 16,
									data: {
										type: 'typography',
										key: 'reviewFormLabelTypo',
										label: __(
											'Label Typography',
											'product-blocks'
										),
									},
								},
								{
									position: 17,
									data: {
										type: 'divider',
										key: 'navDevider',
										label: __(
											'Review Form Input',
											'product-blocks'
										),
									},
								},
								{
									position: 18,
									data: {
										type: 'color',
										key: 'reviewFormInputColor',
										label: __(
											'Input Color',
											'product-blocks'
										),
									},
								},
								{
									position: 18,
									data: {
										type: 'color',
										key: 'reviewFormInputBg',
										label: __(
											'Input Background',
											'product-blocks'
										),
									},
								},
								{
									position: 19,
									data: {
										type: 'border',
										key: 'reviewFormInputBorder',
										label: __(
											'Input Border',
											'product-blocks'
										),
									},
								},
								{
									position: 20,
									data: {
										type: 'color',
										key: 'reviewFormInputFocusColor',
										label: __(
											'Input Focus Color',
											'product-blocks'
										),
									},
								},
								{
									position: 21,
									data: {
										type: 'typography',
										key: 'reviewFormInputTypo',
										label: __(
											'Input Typography',
											'product-blocks'
										),
									},
								},
								{
									position: 22,
									data: {
										type: 'range',
										min: 0,
										max: 40,
										key: 'reviewFormSpace',
										label: __( 'Space', 'product-blocks' ),
									},
								},
								{
									position: 23,
									data: {
										type: 'range',
										min: 0,
										max: 40,
										key: 'reviewFormRadius',
										label: __(
											'Input Radius',
											'product-blocks'
										),
									},
								},
								{
									position: 24,
									data: {
										type: 'dimension',
										key: 'reviewFormInputPadding',
										label: __(
											'Input padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									position: 25,
									data: {
										type: 'divider',
										key: 'navDevider',
										label: __(
											'Button Style',
											'product-blocks'
										),
									},
								},
								{
									position: 26,
									data: {
										type: 'typography',
										key: 'btnTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									position: 27,
									data: {
										type: 'color',
										key: 'btnColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									position: 28,
									data: {
										type: 'color2',
										key: 'btnBgColor',
										label: __(
											'Background Color',
											'product-blocks'
										),
									},
								},
								{
									position: 29,
									data: {
										type: 'border',
										key: 'btnBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									position: 30,
									data: {
										type: 'dimension',
										key: 'btnRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									position: 31,
									data: {
										type: 'boxshadow',
										key: 'btnShadow',
										label: __(
											'BoxShadow',
											'product-blocks'
										),
									},
								},
								{
									position: 32,
									data: {
										type: 'dimension',
										key: 'btnPadding',
										label: __(
											'padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									position: 33,
									data: {
										type: 'dimension',
										key: 'btnSpacing',
										label: __(
											'Spacing',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									position: 34,
									data: {
										type: 'divider',
										key: 'navDevider',
										label: __(
											'Hover Style',
											'product-blocks'
										),
									},
								},
								{
									position: 35,
									data: {
										type: 'color',
										key: 'btnHoverColor',
										label: __(
											'Hover Color',
											'product-blocks'
										),
									},
								},
								{
									position: 36,
									data: {
										type: 'color2',
										key: 'btnBgHoverColor',
										label: __(
											'Hover Bg Color',
											'product-blocks'
										),
									},
								},
								{
									position: 37,
									data: {
										type: 'border',
										key: 'btnHoverBorder',
										label: __(
											'Hover Border',
											'product-blocks'
										),
									},
								},
								{
									position: 38,
									data: {
										type: 'dimension',
										key: 'btnHoverRadius',
										label: __(
											'Hover Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									position: 39,
									data: {
										type: 'boxshadow',
										key: 'btnHoverShadow',
										label: __(
											'Hover BoxShadow',
											'product-blocks'
										),
									},
								},
							] }
							store={ store }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar
				store={ store }
				selected={ toolbarSettings }
			/>

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ `wopb-product-wrapper` }>
					<div className="product">
						<div className="woocommerce-tabs wc-tabs-wrapper">
							<ul
								className="tabs wc-tabs wopb-component-simple"
								role="tablist"
								onClick={ ( e ) =>
									setCurrSettings( null, 'navigation' )
								}
							>
								{ showDescription && (
									<li
										className={
											'description_tab ' +
											( current == 'desc'
												? 'active'
												: '' )
										}
										id="tab-title-description"
										role="tab"
										aria-controls="tab-description"
									>
										<a href="#tab-description">
											Description
										</a>
									</li>
								) }
								{ showAddInfo && (
									<li
										className={
											'additional_information_tab ' +
											( current == 'info'
												? 'active'
												: '' )
										}
										id="tab-title-additional_information"
										role="tab"
										aria-controls="tab-additional_information"
									>
										<a href="#tab-additional_information">
											Additional information
										</a>
									</li>
								) }
								{ showReview && (
									<li
										className={
											'reviews_tab ' +
											( current == 'revi'
												? 'active'
												: '' )
										}
										id="tab-title-reviews"
										role="tab"
										aria-controls="tab-reviews"
									>
										<a href="#tab-reviews">Reviews (1)</a>
									</li>
								) }
							</ul>
							{ showDescription && (
								<div
									onClick={ ( e ) =>
										setCurrSettings( e, 'description-tab' )
									}
									style={ {
										display:
											current == 'desc'
												? 'block'
												: 'none',
									} }
									className="woocommerce-Tabs-panel woocommerce-Tabs-panel--description panel entry-content wc-tab wopb-component-simple"
									id="tab-description"
									role="tabpanel"
									aria-labelledby="tab-title-description"
								>
									<p>
										Lorem ipsum dolor sit amet, consectetur
										adipiscing elit. Vestibulum sagittis
										orci ac odio dictum tincidunt. Donec ut
										metus leo. Class aptent taciti sociosqu
										ad litora torquent per conubia nostra,
										per inceptos himenaeos. Sed luctus, dui
										eu sagittis sodales, nulla nibh sagittis
										augue, vel porttitor diam enim non
										metus. Vestibulum aliquam augue neque.
										Phasellus tincidunt odio eget
										ullamcorper efficitur. Cras placerat ut
										turpis pellentesque vulputate. Nam sed
										consequat tortor. Curabitur finibus
										sapien dolor. Ut eleifend tellus nec
										erat pulvinar dignissim. Nam non arcu
										purus. Vivamus et massa massa.
									</p>
								</div>
							) }
							{ showAddInfo && (
								<div
									onClick={ ( e ) =>
										setCurrSettings(
											e,
											'additional-information-tab'
										)
									}
									style={ {
										display:
											current == 'info'
												? 'block'
												: 'none',
									} }
									className="woocommerce-Tabs-panel woocommerce-Tabs-panel--additional_information panel entry-content wc-tab wopb-component-simple"
									id="tab-additional_information"
									role="tabpanel"
									aria-labelledby="tab-title-additional_information"
								>
									<table className="woocommerce-product-attributes shop_attributes">
										<tbody>
											<tr className="woocommerce-product-attributes-item woocommerce-product-attributes-item--attribute_pa_color">
												<th className="woocommerce-product-attributes-item__label">
													Color
												</th>
												<td className="woocommerce-product-attributes-item__value">
													<p>Blue, Gray</p>
												</td>
											</tr>
											<tr className="woocommerce-product-attributes-item woocommerce-product-attributes-item--attribute_pa_size">
												<th className="woocommerce-product-attributes-item__label">
													Size
												</th>
												<td className="woocommerce-product-attributes-item__value">
													<p>M, L, XL, XXL</p>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							) }
							{ showReview && (
								<div
									onClick={ ( e ) =>
										setCurrSettings( e, 'review' )
									}
									style={ {
										display:
											current == 'revi'
												? 'block'
												: 'none',
									} }
									className="woocommerce-Tabs-panel woocommerce-Tabs-panel--reviews panel entry-content wc-tab wopb-component-simple"
									id="tab-reviews"
									role="tabpanel"
									aria-labelledby="tab-title-reviews"
								>
									<div
										id="reviews"
										className="woocommerce-Reviews"
									>
										<div id="comments">
											{ reviewHeading && (
												<h2 className="woocommerce-Reviews-title">
													{ ' ' }
													1 review for{ ' ' }
													<span>Album</span>{ ' ' }
												</h2>
											) }
											<ol className="commentlist">
												<li
													className="review byuser comment-author-admin bypostauthor even thread-even depth-1"
													id="li-comment-39"
												>
													<div
														id="comment-39"
														className="comment_container"
													>
														<img
															alt=""
															src={
																wopb_data.url +
																'assets/img/wopb-author.jpg'
															}
															srcSet="http://0.gravatar.com/avatar/04574d96b5916ce02d1c369900feb9d4?s=120&amp;d=mm&amp;r=g 2x"
															className="avatar avatar-60 photo"
															height="60"
															width="60"
														/>
														<div className="comment-text">
															<div
																className="star-rating"
																role="img"
																aria-label="Rated 4 out of 5"
															>
																<span
																	style={ {
																		width: '73%',
																	} }
																>
																	Rated{ ' ' }
																	<strong className="rating">
																		4
																	</strong>{ ' ' }
																	out of 5
																</span>
															</div>
															<p className="meta">
																<strong className="woocommerce-review__author">
																	admin{ ' ' }
																</strong>
																<span className="woocommerce-review__dash">
																	–
																</span>{ ' ' }
																<time
																	className="woocommerce-review__published-date"
																	dateTime="2021-08-29T05:22:28+00:00"
																>
																	August 29,
																	2021
																</time>
															</p>
															<div className="description">
																<p>
																	This is a
																	sample
																	review text.
																</p>
															</div>
														</div>
													</div>
												</li>
											</ol>
										</div>
										<div id="review_form_wrapper">
											<div id="review_form">
												<div
													id="respond"
													className="comment-respond"
												>
													<span
														id="reply-title"
														className="comment-reply-title"
													>
														Add a review{ ' ' }
														<small>
															<a
																rel="nofollow"
																id="cancel-comment-reply-link"
																href="/wpxpo_productx/product/album/#respond"
																style={ {
																	display:
																		'none',
																} }
															>
																Cancel reply
															</a>
														</small>
													</span>
													<form
														action="javascript:"
														method="post"
														id="commentform"
														className="comment-form"
														noValidate=""
													>
														<div className="comment-form-rating">
															<label htmlFor="rating">
																Your
																rating&nbsp;
																<span className="required">
																	*
																</span>
															</label>
															<p className="stars">
																<span>
																	<a
																		className="star-1"
																		href="#"
																	>
																		1
																	</a>
																	<a
																		className="star-2"
																		href="#"
																	>
																		2
																	</a>
																	<a
																		className="star-3"
																		href="#"
																	>
																		3
																	</a>
																	<a
																		className="star-4"
																		href="#"
																	>
																		4
																	</a>
																	<a
																		className="star-5"
																		href="#"
																	>
																		5
																	</a>
																</span>
															</p>
															<select
																name="rating"
																id="rating"
																required=""
																style={ {
																	display:
																		'none',
																} }
															>
																<option value="">
																	Rate…
																</option>
																<option value="5">
																	Perfect
																</option>
																<option value="4">
																	Good
																</option>
																<option value="3">
																	Average
																</option>
																<option value="2">
																	Not that bad
																</option>
																<option value="1">
																	Very poor
																</option>
															</select>
														</div>
														<p className="comment-form-comment">
															<label htmlFor="comment">
																Your
																review&nbsp;
																<span className="required">
																	*
																</span>
															</label>
															<textarea
																id="comment"
																name="comment"
																cols="45"
																rows="8"
																required=""
															></textarea>
														</p>
														<p className="form-submit">
															<input
																name="submit"
																type="submit"
																id="submit"
																className="submit"
															/>
															<input
																type="hidden"
																name="comment_post_ID"
																value="23"
																id="comment_post_ID"
															/>
															<input
																type="hidden"
																name="comment_parent"
																id="comment_parent"
															/>
														</p>
														<input
															type="hidden"
															id="_wp_unfiltered_html_comment_disabled"
															name="_wp_unfiltered_html_comment"
														/>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							) }
						</div>
					</div>
				</div>
			</div>
		</>
	);
}
