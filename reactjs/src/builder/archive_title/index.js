const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';
registerBlockType( 'product-blocks/archive-title', {
	title: __( 'Archive Title', 'product-blocks' ),
	icon: (
		<img
			src={
				wopb_data.url + 'assets/img/blocks/builder/archive-title.svg'
			}
		/>
	),
	category: 'single-product',
	description: __(
		'Dynamically control position, size, and typography of archive titles.',
		'product-blocks'
	),
	keywords: [
		__( 'archive', 'product-blocks' ),
		__( 'dynamic title', 'product-blocks' ),
		__( 'title', 'product-blocks' ),
	],
	supports: {
		align: [ 'center', 'wide', 'full' ],
	},
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
