/** @format */

const { __ } = wp.i18n;
import {
	AlignmentTB,
	CommonSettings,
	CustomCssAdvanced,
	GeneralAdvanced,
	ImageStyle,
	ImageStyleArg,
	ResponsiveAdvanced,
	SpacingArg,
	TitleStyle,
	TitleStyleArg,
	TypographyTB,
	colorIcon,
	filterFields,
	settingsIcon,
	spacingIcon,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Section, Sections } from '../../helper/Sections';
import WopbToolbarDropdown from '../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar, getData } from '../../helper/ux';

const TitleSettings = [
	{
		position: 0,
		data: {
			type: 'layout',
			block: 'wopb-taxonomy',
			key: 'layout',
			pro: true,
			options: [
				{
					img: 'assets/img/blocks/builder/archive/al1.png',
					label: __( 'Layout 1', 'product-blocks' ),
					value: '1',
					pro: false,
				},
				{
					img: 'assets/img/blocks/builder/archive/al2.png',
					label: __( 'Layout 2', 'product-blocks' ),
					value: '2',
					pro: true,
				},
			],
		},
	},
	{
		position: 1,
		data: {
			type: 'toggle',
			key: 'customTaxTitleColor',
			label: __( 'Specific Color (Pro)', 'product-blocks' ),
			pro: true,
		},
	},
	{
		position: 2,
		data: {
			type: 'linkbutton',
			key: 'seperatorTaxTitleLink',
			placeholder: __( 'Choose Color', 'product-blocks' ),
			label: __( 'Taxonomy Specific (Pro)', 'product-blocks' ),
			text: 'Choose Color',
		},
	},
	{
		position: 4,
		data: {
			type: 'alignment',
			key: 'titleAlign',
			responsive: true,
			label: __( 'Alignment', 'product-blocks' ),
			disableJustify: true,
		},
	},
];

const PrefixSettings = [
	{
		position: 1,
		data: {
			type: 'text',
			key: 'prefixText',
			label: __( 'Prefix Text', 'product-blocks' ),
		},
	},
	{
		position: 2,
		data: {
			type: 'color',
			key: 'prefixColor',
			label: __( 'Color', 'product-blocks' ),
		},
	},
	{
		position: 3,
		data: {
			type: 'typography',
			key: 'prefixTypo',
			label: __( 'Typography', 'product-blocks' ),
		},
	},
	{
		position: 4,
		data: {
			type: 'dimension',
			key: 'prefixPadding',
			label: __( 'Padding', 'product-blocks' ),
			step: 1,
			unit: true,
			responsive: true,
		},
	},
];

const ExcerptSettings2 = [
	{
		position: 1,
		data: {
			type: 'select',
			key: 'TaxAnimation',
			label: __( 'Hover Animation', 'product-blocks' ),
			options: [
				{
					value: 'none',
					label: __( 'No Animation', 'product-blocks' ),
				},
				{
					value: 'zoomIn',
					label: __( 'Zoom In', 'product-blocks' ),
				},
				{
					value: 'zoomOut',
					label: __( 'Zoom Out', 'product-blocks' ),
				},
				{
					value: 'opacity',
					label: __( 'Opacity', 'product-blocks' ),
				},
				{
					value: 'slideLeft',
					label: __( 'Slide Left', 'product-blocks' ),
				},
				{
					value: 'slideRight',
					label: __( 'Slide Right', 'product-blocks' ),
				},
			],
		},
	},
	{
		position: 2,
		data: {
			type: 'tab',
			content: [
				{
					name: 'normal',
					title: __( 'Normal', 'product-blocks' ),
					options: [
						{
							type: 'color',
							key: 'TaxWrapBg',
							label: __( 'Background Color', 'product-blocks' ),
						},
						{
							type: 'range',
							key: 'customOpacityTax',
							min: 0,
							max: 1,
							step: 0.05,
							label: __( 'Overlay Opacity', 'product-blocks' ),
						},
						{
							type: 'border',
							key: 'TaxWrapBorder',
							label: __( 'Border', 'product-blocks' ),
						},
						{
							type: 'boxshadow',
							key: 'TaxWrapShadow',
							label: __( 'BoxShadow', 'product-blocks' ),
						},
						{
							type: 'dimension',
							key: 'TaxWrapRadius',
							label: __( 'Border Radius', 'product-blocks' ),
							step: 1,
							unit: true,
							responsive: true,
						},
					],
				},
				{
					name: 'hover',
					title: __( 'Hover', 'product-blocks' ),
					options: [
						{
							type: 'color',
							key: 'TaxWrapHoverBg',
							label: __( 'Hover Bg Color', 'product-blocks' ),
						},
						{
							type: 'range',
							key: 'customTaxOpacityHover',
							min: 0,
							max: 1,
							step: 0.05,
							label: __( 'Hover Opacity', 'product-blocks' ),
						},
						{
							type: 'border',
							key: 'TaxWrapHoverBorder',
							label: __( 'Hover Border', 'product-blocks' ),
						},
						{
							type: 'boxshadow',
							key: 'TaxWrapHoverShadow',
							label: __( 'Hover BoxShadow', 'product-blocks' ),
						},
						{
							type: 'dimension',
							key: 'TaxWrapHoverRadius',
							label: __( 'Hover Radius', 'product-blocks' ),
							step: 1,
							unit: true,
							responsive: true,
						},
					],
				},
			],
		},
	},
	{
		position: 3,
		data: {
			type: 'dimension',
			key: 'TaxWrapPadding',
			label: __( 'Padding', 'product-blocks' ),
			step: 1,
			unit: true,
			responsive: true,
		},
	},
];

const ExcerptSettings1 = [
	{
		position: 0,
		data: {
			type: 'alignment',
			key: 'excerptAlign',
			responsive: true,
			label: __( 'Alignment', 'product-blocks' ),
			disableJustify: true,
		},
	},
	{
		position: 1,
		data: {
			type: 'typography',
			key: 'excerptTypo',
			label: __( 'Typography', 'product-blocks' ),
		},
	},
	{
		position: 2,
		data: {
			type: 'color',
			key: 'excerptColor',
			label: __( 'Color', 'product-blocks' ),
		},
	},
	{
		position: 3,
		data: {
			type: 'dimension',
			key: 'excerptPadding',
			label: __( 'Padding', 'product-blocks' ),
			step: 1,
			unit: true,
			responsive: true,
		},
	},
];

export default function Settings( { store, section } ) {
	const { layout } = store.attributes;

	return (
		<>
			<Sections>
				<Section
					slug="setting"
					title={ __( 'Setting', 'product-blocks' ) }
				>
					<TitleStyle
						initialOpen={ section.title }
						store={ store }
						exclude={ [
							'titlePosition',
							'titleHoverColor',
							'titleLength',
							'titleBackground',
						] }
						include={ TitleSettings }
					/>
					<CommonSettings
						depend="prefixShow"
						initialOpen={ section.prefix }
						title={ __( 'Prefix', 'product-blocks' ) }
						store={ store }
						include={ PrefixSettings }
					/>
					{ layout == '2' && (
						<CommonSettings
							initialOpen={ section[ 'excerpt-settings' ] }
							title={ __( 'Excerpt Settings', 'product-blocks' ) }
							store={ store }
							include={ ExcerptSettings2 }
						/>
					) }
					{ layout == '1' && (
						<ImageStyle
							initialOpen={ section.image }
							depend="showImage"
							store={ store }
							exclude={ [
								'imgMargin',
								'imgCropSmall',
								'imgCrop',
								'imgAnimation',
								'imgOverlay',
								'imgOpacity',
								'overlayColor',
								'imgOverlayType',
								'imgGrayScale',
								'imgHoverGrayScale',
								'imgShadow',
								'imgHoverShadow',
								'imgTab',
								'imgHoverRadius',
								'imgRadius',
							] }
							include={ [
								{
									position: 3,
									data: {
										type: 'alignment',
										key: 'contentAlign',
										responsive: false,
										label: __(
											'Alignment',
											'product-blocks'
										),
										disableJustify: true,
									},
								},
								{
									position: 2,
									data: {
										type: 'range',
										key: 'imgSpacing',
										label: __(
											'Img Spacing',
											'product-blocks'
										),
										min: 0,
										max: 100,
										step: 1,
										responsive: true,
									},
								},
							] }
						/>
					) }
					<CommonSettings
						depend="excerptShow"
						initialOpen={ section[ 'excerpt-settings' ] }
						title={ __( 'Excerpt Settings', 'product-blocks' ) }
						store={ store }
						include={ ExcerptSettings2 }
					/>
				</Section>
				<Section
					slug="advanced"
					title={ __( 'Advanced', 'product-blocks' ) }
				>
					<GeneralAdvanced store={ store } />
					<ResponsiveAdvanced pro={ true } store={ store } />
					<CustomCssAdvanced store={ store } />
				</Section>
			</Sections>
			{ wopbSupport() }
		</>
	);
}

export function AddSettingsToToolbar( { store, selected } ) {
	switch ( selected ) {
		case 'title':
			return (
				<WopbToolbarGroup text={ 'Title' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Title Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Title Settings', 'product-blocks' ),
								options: filterFields(
									TitleSettings,
									[
										'titlePosition',
										'titleHoverColor',
										'titleLength',
										'titleBackground',
									],
									TitleStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'prefix':
			return (
				<WopbToolbarGroup text={ 'Prefix' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Prefix Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Prefix Settings',
									'product-blocks'
								),
								options: getData( PrefixSettings ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'image':
			return (
				<WopbToolbarGroup text={ 'Image' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Image Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Image Settings', 'product-blocks' ),
								options: filterFields(
									[
										{
											position: 3,
											data: {
												type: 'alignment',
												key: 'contentAlign',
												responsive: false,
												label: __(
													'Alignment',
													'product-blocks'
												),
												disableJustify: true,
											},
										},
										{
											position: 2,
											data: {
												type: 'range',
												key: 'imgSpacing',
												label: __(
													'Img Spacing',
													'product-blocks'
												),
												min: 0,
												max: 100,
												step: 1,
												responsive: true,
											},
										},
									],
									[
										'imgMargin',
										'imgCropSmall',
										'imgCrop',
										'imgAnimation',
										'imgOverlay',
										'imgOpacity',
										'overlayColor',
										'imgOverlayType',
										'imgGrayScale',
										'imgHoverGrayScale',
										'imgShadow',
										'imgHoverShadow',
										'imgTab',
										'imgHoverRadius',
										'imgRadius',
									],
									ImageStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'excerpt-settings':
			return (
				<WopbToolbarGroup text={ 'Excerpt' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Excerpt Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Excerpt Settings',
									'product-blocks'
								),
								options: getData(
									store.attributes.layout === '1'
										? ExcerptSettings1
										: ExcerptSettings2
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		default:
			return null;
	}
}
