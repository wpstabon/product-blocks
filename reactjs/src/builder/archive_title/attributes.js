const attributes = {
	blockId: { type: 'string', default: '' },
	currentPostId: { type: 'string', default: '' },
	layout: { type: 'string', default: '1' },
	contentAlign: {
		type: 'string',
		default: 'left',
		style: [
			{
				depends: [
					{ key: 'contentAlign', condition: '==', value: 'left' },
				],
				selector:
					'{{WOPB}} .wopb-block-archive-title { text-align:{{contentAlign}}; }',
			},
			{
				depends: [
					{ key: 'contentAlign', condition: '==', value: 'center' },
				],
				selector:
					'{{WOPB}} .wopb-block-archive-title { text-align:{{contentAlign}}; }',
			},
			{
				depends: [
					{ key: 'contentAlign', condition: '==', value: 'right' },
				],
				selector:
					'{{WOPB}} .wopb-block-archive-title { text-align:{{contentAlign}}; }',
			},
		],
	},
	excerptShow: { type: 'boolean', default: true },
	prefixShow: { type: 'boolean', default: false },
	showImage: { type: 'boolean', default: false },
	titleTag: { type: 'string', default: 'h1' },
	customTaxTitleColor: { type: 'boolean', default: false },
	seperatorTaxTitleLink: {
		type: 'string',
		default: wopb_data?.taxonomyCatUrl,
		style: [
			{
				depends: [
					{
						key: 'customTaxTitleColor',
						condition: '==',
						value: true,
					},
				],
			},
		],
	},
	titleColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-archive-title .wopb-archive-name { color:{{titleColor}}; }',
			},
			{
				depends: [
					{
						key: 'customTaxTitleColor',
						condition: '==',
						value: false,
					},
				],
				selector:
					'{{WOPB}} .wopb-block-archive-title .wopb-archive-name { color:{{titleColor}}; }',
			},
		],
	},
	titleTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '40', unit: 'px' },
			spacing: { lg: '0', unit: 'px' },
			height: { lg: '52', unit: 'px' },
			transform: '',
			decoration: 'none',
			family: '',
			weight: '600',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-archive-title .wopb-archive-name',
			},
		],
	},
	titlePadding: {
		type: 'object',
		default: { lg: { bottom: 16, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-archive-title .wopb-archive-name { padding:{{titlePadding}}; }',
			},
		],
	},
	titleAlign: {
		type: 'object',
		default: { lg: 'center', sm: 'center', xs: 'center' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-archive-name { text-align:{{titleAlign}}; }',
			},
		],
	},
	excerptColor: {
		type: 'string',
		default: '#4A4A4A',
		style: [
			{
				depends: [
					{ key: 'excerptShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-block-archive-title .wopb-archive-desc { color:{{excerptColor}}; }',
			},
		],
	},
	excerptTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: 14, unit: 'px' },
			height: { lg: '22', unit: 'px' },
			decoration: 'none',
			family: '',
		},
		style: [
			{
				depends: [
					{ key: 'excerptShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-block-archive-title .wopb-archive-desc',
			},
		],
	},
	excerptPadding: {
		type: 'object',
		default: { lg: { top: '0', bottom: '', unit: 'px' } },
		style: [
			{
				depends: [
					{ key: 'excerptShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-block-archive-title .wopb-archive-desc { padding: {{excerptPadding}}; }',
			},
		],
	},
	excerptAlign: {
		type: 'string',
		default: { lg: 'center', sm: 'center', xs: 'center' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-archive-desc { text-align:{{excerptAlign}}; }',
			},
		],
	},
	prefixText: { type: 'string', default: 'Sample Prefix Text: ' },
	prefixColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				depends: [
					{ key: 'prefixShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-block-archive-title .wopb-archive-prefix { color:{{prefixColor}}; }',
			},
		],
	},
	prefixTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '40', unit: 'px' },
			spacing: { lg: '0', unit: 'px' },
			height: { lg: '52', unit: 'px' },
			transform: 'capitalize',
			decoration: 'none',
			family: '',
			weight: '600',
		},
		style: [
			{
				depends: [
					{ key: 'prefixShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-block-archive-title .wopb-archive-prefix',
			},
		],
	},
	prefixPadding: {
		type: 'object',
		default: { lg: { top: 0, bottom: 0, unit: 'px' } },
		style: [
			{
				depends: [
					{ key: 'prefixShow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-block-archive-title .wopb-archive-prefix { padding:{{prefixPadding}}; }',
			},
		],
	},
	imgWidth: {
		type: 'object',
		default: { lg: '', unit: '%' },
		style: [
			{
				depends: [ { key: 'layout', condition: '==', value: [ '1' ] } ],
				selector:
					'{{WOPB}} .wopb-block-archive-title .wopb-archive-image { max-width: {{imgWidth}}; }',
			},
		],
	},
	imgHeight: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				depends: [ { key: 'layout', condition: '==', value: [ '1' ] } ],
				selector:
					'{{WOPB}} .wopb-block-archive-title .wopb-archive-image {object-fit: cover; height: {{imgHeight}}; }',
			},
		],
	},
	imgSpacing: {
		type: 'object',
		default: { lg: '10' },
		style: [
			{
				depends: [ { key: 'layout', condition: '==', value: [ '1' ] } ],
				selector:
					'{{WOPB}} .wopb-block-archive-title .wopb-archive-image { margin-bottom: {{imgSpacing}}px; }',
			},
		],
	},
	TaxAnimation: { type: 'string', default: 'none' },
	TaxWrapBg: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-archive-title .wopb-archive-content .wopb-archive-overlay { background:{{TaxWrapBg}}; }',
			},
		],
	},
	TaxWrapHoverBg: {
		type: 'string',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-archive-title .wopb-archive-content:hover .wopb-archive-overlay { background:{{TaxWrapHoverBg}}; }',
			},
		],
	},
	TaxWrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [ { key: 'layout', condition: '==', value: [ '2' ] } ],
				selector:
					'{{WOPB}} .wopb-block-archive-title .wopb-archive-content',
			},
		],
	},
	TaxWrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [ { key: 'layout', condition: '==', value: [ '2' ] } ],
				selector:
					'{{WOPB}} .wopb-block-archive-title .wopb-archive-content:hover',
			},
		],
	},
	TaxWrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [ { key: 'layout', condition: '==', value: [ '2' ] } ],
				selector:
					'{{WOPB}} .wopb-block-archive-title .wopb-archive-content',
			},
		],
	},
	TaxWrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [
			{
				depends: [ { key: 'layout', condition: '==', value: [ '2' ] } ],
				selector:
					'{{WOPB}} .wopb-block-archive-title .wopb-archive-content:hover',
			},
		],
	},
	TaxWrapRadius: {
		type: 'object',
		default: { lg: { top: '', bottom: '', unit: 'px' } },
		style: [
			{
				depends: [ { key: 'layout', condition: '==', value: [ '2' ] } ],
				selector:
					'{{WOPB}} .wopb-block-archive-title .wopb-archive-content { border-radius: {{TaxWrapRadius}}; }',
			},
		],
	},
	TaxWrapHoverRadius: {
		type: 'object',
		default: { lg: { top: '', bottom: '', unit: 'px' } },
		style: [
			{
				depends: [ { key: 'layout', condition: '==', value: [ '2' ] } ],
				selector:
					'{{WOPB}} .wopb-block-archive-title .wopb-archive-content:hover { border-radius: {{TaxWrapHoverRadius}}; }',
			},
		],
	},
	customOpacityTax: {
		type: 'string',
		default: 0.6,
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-archive-title .wopb-archive-content .wopb-archive-overlay { opacity: {{customOpacityTax}}; }',
			},
		],
	},
	customTaxOpacityHover: {
		type: 'string',
		default: 0.9,
		style: [
			{
				selector:
					'{{WOPB}} .wopb-taxonomy-items li a:hover .wopb-archive-overlay { opacity: {{customTaxOpacityHover}}; }',
			},
		],
	},
	TaxWrapPadding: {
		type: 'object',
		default: {
			lg: {
				top: '20',
				bottom: '20',
				left: '20',
				right: '20',
				unit: 'px',
			},
		},
		style: [
			{
				depends: [ { key: 'layout', condition: '==', value: [ '2' ] } ],
				selector:
					'{{WOPB}} .wopb-block-archive-title .wopb-archive-content { padding: {{TaxWrapPadding}}; }',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#f5f5f5' },
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper' } ],
	},
	wrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#037fff' },
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-block-wrapper:hover' } ],
	},
	wrapMargin: {
		type: 'object',
		default: { lg: { top: '', bottom: '20', unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-block-wrapper { padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {display:none;}' } ],
	},
};
export default attributes;
