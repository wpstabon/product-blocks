/** @format */

const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import Settings, { AddSettingsToToolbar } from './Settings';
import useFluentSettings from '../../helper/hooks/useFluentSettings';
import ToolBarElement from '../../helper/ToolBarElement';
import useBlockId from '../../helper/hooks/use-block-id';

const { useEffect } = wp.element;

export default function Edit( props ) {
	const {
		setAttributes,
		clientId,
		name,
		attributes,
		className,
		attributes: {
			blockId,
			prefixShow,
			prefixText,
			advanceId,
			showImage,
			layout,
			excerptShow,
			customTaxTitleColor,
			titleTag,
		},
	} = props;

	const store = {
		setAttributes,
		name,
		attributes,
		clientId,
	};

	const { section, setCurrSettings, toolbarSettings } = useFluentSettings();

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/archive-title', blockId );
	}

	// Dummy Content
	const dummyName = 'Archive Title';
	const dummyImage =
		wopb_data.url + 'assets/img/blocks/builder/builder-fallback.jpg';
	const dynamiColor = '#037fff';
	const dummyDesc =
		'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam molestie aliquet molestie.';

	const styleCSS = dummyImage
		? { backgroundImage: `url(${ dummyImage })` }
		: { background: `${ dynamiColor }` };

	const HtmlTag = `${ titleTag }`;

	return (
		<>
			<ToolBarElement
				store={ store }
				include={ [
					{
						type: 'block_spacing',
						label: __( 'Archive Title Spacing', 'product-blocks' ),
					},
				] }
			/>
			<InspectorControls>
				<Settings store={ store } section={ section } />
			</InspectorControls>

			<AddSettingsToToolbar
				store={ store }
				selected={ toolbarSettings }
			/>

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ `wopb-block-wrapper` }>
					<div
						className={ `wopb-block-archive-title  wopb-archive-layout-${ layout }` }
					>
						{ layout == 1 && (
							<div>
								{ dummyImage && showImage && (
									<img
										onClick={ ( e ) =>
											setCurrSettings( e, 'image' )
										}
										className="wopb-archive-image wopb-component-simple"
										src={ dummyImage }
										alt={ dummyName }
									/>
								) }
								<HtmlTag
									className="wopb-archive-name wopb-component-simple"
									style={
										customTaxTitleColor
											? { color: dynamiColor }
											: {}
									}
									onClick={ ( e ) =>
										setCurrSettings( e, 'title' )
									}
								>
									{ prefixShow && (
										<span
											onClick={ ( e ) =>
												setCurrSettings( e, 'prefix' )
											}
											className="wopb-archive-prefix wopb-component-simple"
										>
											{ prefixText }
										</span>
									) }
									{ dummyName }
								</HtmlTag>
								{ excerptShow && (
									<div
										onClick={ ( e ) =>
											setCurrSettings(
												e,
												'excerpt-settings'
											)
										}
										className="wopb-archive-desc wopb-component-simple"
									>
										{ dummyDesc }
									</div>
								) }
							</div>
						) }
						{ layout == 2 && (
							<div
								className="wopb-archive-content"
								style={ styleCSS }
								onClick={ ( e ) =>
									setCurrSettings( e, 'title' )
								}
							>
								<div className="wopb-archive-overlay"></div>
								<HtmlTag
									onClick={ ( e ) =>
										setCurrSettings( e, 'title' )
									}
									className="wopb-archive-name wopb-component-simple"
								>
									{ prefixShow && (
										<span
											onClick={ ( e ) =>
												setCurrSettings( e, 'prefix' )
											}
											className="wopb-archive-prefix wopb-component-simple"
										>
											{ prefixText }
										</span>
									) }
									{ dummyName }
								</HtmlTag>
								{ excerptShow && (
									<div
										onClick={ ( e ) =>
											setCurrSettings(
												e,
												'excerpt-settings'
											)
										}
										className="wopb-archive-desc wopb-component-simple"
									>
										{ dummyDesc }
									</div>
								) }
							</div>
						) }
					</div>
				</div>
			</div>
		</>
	);
}
