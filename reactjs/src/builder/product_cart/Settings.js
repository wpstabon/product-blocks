/** @format */

const { __ } = wp.i18n;
import {
	AlignmentTB,
	SpacingArg,
	TypographyTB,
	colorIcon,
	filterFields,
	spacingIcon,
} from '../../helper/CommonPanel';
import WopbToolbarDropdown from '../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar } from '../../helper/ux';

export function AddSettingsToToolbar( { store } ) {
	return (
		<WopbToolbarGroup text={ 'Add to Cart' }>
			<AlignmentTB
				store={ store }
				attrKey="addtocartAlignment"
				label={ __( 'Add to Cart Alignment', 'product-blocks' ) }
			/>
			<TypographyTB
				store={ store }
				attrKey={ 'btnTypo' }
				label={ __( 'Add to Cart Typography', 'product-blocks' ) }
			/>
			<WopbToolbarDropdown
				buttonContent={ colorIcon }
				store={ store }
				label={ __( 'Add to Cart Color', 'product-blocks' ) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __( 'Add to Cart Color', 'product-blocks' ),
						options: [
							{
								type: 'color',
								key: 'quantityColor',
								label: __( 'Quantity Color', 'product-blocks' ),
							},
							{
								type: 'color',
								key: 'quantityBg',
								label: __(
									'Quantity Background',
									'product-blocks'
								),
							},
							{
								type: 'color',
								key: 'bcrumbLinkHoverColor',
								label: __(
									'Link Hover Color',
									'product-blocks'
								),
							},
							{
								type: 'color',
								key: 'bcrumbSeparatorColor',
								label: __(
									'Separator color',
									'product-blocks'
								),
							},
							{
								type: 'color',
								key: 'plusMinusColor',
								label: __( 'Icon Color', 'product-blocks' ),
							},
							{
								type: 'color',
								key: 'plusMinusBg',
								label: __(
									'Icon Background',
									'product-blocks'
								),
							},
							{
								type: 'color',
								key: 'plusMinusHoverColor',
								label: __(
									'Icon Hover Color',
									'product-blocks'
								),
							},
							{
								type: 'color',
								key: 'plusMinusHoverBg',
								label: __(
									'Icon Hover Background',
									'product-blocks'
								),
							},
						],
					},
				] ) }
			/>
			<WopbToolbarDropdown
				buttonContent={ spacingIcon }
				store={ store }
				label={ __( 'Add to Cart Spacing', 'product-blocks' ) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __( 'Add to Cart Spacing', 'product-blocks' ),
						options: filterFields(
							[
								{
									data: {
										type: 'range',
										key: 'wrapMargin',
										min: 0,
										max: 100,
										step: 1,
										unit: true,
										responsive: true,
										dimensionKey: 'top',
										label: __(
											'Margin Top',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'wrapMargin',
										min: 0,
										max: 100,
										step: 1,
										unit: true,
										responsive: true,
										dimensionKey: 'bottom',
										label: __(
											'Margin Bottom',
											'product-blocks'
										),
									},
								},
							],
							'__all',
							SpacingArg
						),
					},
				] ) }
			/>
		</WopbToolbarGroup>
	);
}
