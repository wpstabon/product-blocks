const attributes = {
	blockId: {
		type: 'string',
		default: '',
	},
	currentPostId: { type: 'string', default: '' },
	preview: {
		type: 'string',
		default: 'simple',
	},
	showQuantity: {
		type: 'boolean',
		default: true,
	},
	addtocartAlignment: {
		type: 'object',
		default: {
			lg: 'left',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-builder-cart .cart {justify-content:{{addtocartAlignment}};} {{WOPB}} .wopb-builder-cart {text-align: {{addtocartAlignment}};}',
			},
		],
	},
	quantityBtnPosition: {
		type: 'string',
		default: 'right',
	},
	showQuantityBtn: {
		type: 'boolean',
		default: true,
	},
	quantityColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'.woocommerce {{WOPB}} .quantity > input , {{WOPB}} input {color:{{quantityColor}}!important;}',
			},
		],
	},
	quantityBg: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'.woocommerce {{WOPB}} .quantity > input , {{WOPB}} .quantity > input {background:{{quantityBg}} !important; box-sizing: content-box;}',
			},
		],
	},
	quantityWrapWidth: {
		type: 'string',
		default: 64,
		style: [
			{
				depends: [
					{ key: 'showQuantity', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} form.cart .quantity > input.qty { max-width: {{quantityWrapWidth}}px; }',
			},
		],
	},
	quantityBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#070707',
			type: 'solid',
		},
		style: [
			{
				selector:
					'.woocommerce {{WOPB}} .quantity > input.qty, {{WOPB}} .quantity > input',
			},
		],
	},
	quantityRadius: {
		type: 'object',
		default: {
			lg: { top: '0', right: '0', bottom: '0', left: '0', unit: 'px' },
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .quantity input , .woocommerce {{WOPB}} .quantity input {border-radius:{{quantityRadius}} !important;}',
			},
		],
	},
	quantityPadding: {
		type: 'object',
		default: {
			lg: {
				top: '12',
				bottom: '12',
				left: '28',
				right: '0',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .quantity input[type="number"] , .woocommerce {{WOPB}} .quantity input[type="number"] { padding:{{quantityPadding}} !important; }',
			},
		],
	},
	plusMinusColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-builder-cart-plus > svg , .wopb-builder-cart-minus > svg {fill:{{plusMinusColor}};}',
			},
		],
	},
	plusMinusBg: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-builder-cart .quantity .wopb-builder-cart-plus , {{WOPB}} .wopb-builder-cart .quantity .wopb-builder-cart-minus {background:{{plusMinusBg}};}',
			},
		],
	},
	plusMinusHoverColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-builder-cart-plus:hover svg , .wopb-builder-cart-minus:hover svg {fill:{{plusMinusHoverColor}};}',
			},
		],
	},
	plusMinusHoverBg: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-builder-cart .quantity .wopb-builder-cart-plus:hover , {{WOPB}} .wopb-builder-cart .quantity .wopb-builder-cart-minus:hover {background:{{plusMinusHoverBg}};}',
			},
		],
	},
	plusMinusSize: {
		type: 'string',
		default: '10',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-builder-cart .quantity .wopb-builder-cart-plus > svg , {{WOPB}} .wopb-builder-cart .quantity .wopb-builder-cart-minus > svg{width:{{plusMinusSize}}px !important; }',
			},
		],
	},
	plusMinusGap: {
		type: 'string',
		default: 4,
		style: [
			{
				depends: [
					{ key: 'showQuantityBtn', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-builder-cart .quantity, {{WOPB}} .wopb-builder-cart .quantity, {{WOPB}} .wopb-builder-cart .quantity .wopb-builder-cart-icon-right, {{WOPB}} .wopb-builder-cart .quantity .wopb-builder-cart-icon-left {gap:{{plusMinusGap}}px; }',
			},
		],
	},
	plusMinusBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#070707',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-builder-cart-plus , .wopb-builder-cart-minus',
			},
		],
	},
	plusMinusRadius: {
		type: 'object',
		default: {
			lg: '0',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-builder-cart-plus , {{WOPB}} .wopb-builder-cart-minus {border-radius:{{plusMinusRadius}}; }',
			},
		],
	},
	plusMinusPadding: {
		type: 'object',
		default: {
			lg: {
				top: '2',
				bottom: '2',
				left: '8',
				right: '8',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-builder-cart .quantity .wopb-builder-cart-plus , {{WOPB}} .wopb-builder-cart .quantity .wopb-builder-cart-minus {padding:{{plusMinusPadding}};}',
			},
		],
	},
	btnTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: {
				lg: 16,
				unit: 'px',
			},
			height: {
				lg: 20,
				unit: 'px',
			},
			spacing: {
				lg: 0,
				unit: 'px',
			},
			transform: 'uppercase',
			weight: '500',
			decoration: 'none',
			family: '',
		},
		style: [
			{
				selector:
					'{{WOPB}} .single_add_to_cart_button , .woocommerce {{WOPB}} .wopb-builder-cart .cart .single_add_to_cart_button',
			},
		],
	},
	btnColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .single_add_to_cart_button , .single.single-product {{WOPB}} .single_add_to_cart_button, .woocommerce {{WOPB}} button.button.alt.disabled.wc-variation-selection-needed {color:{{btnColor}};}',
			},
		],
	},
	btnBgColor: {
		type: 'object',
		default: {
			openColor: 1,
			type: 'color',
			color: '#070707',
		},
		style: [
			{
				selector:
					'.woocommerce {{WOPB}} button.button.alt.disabled.wc-variation-selection-needed, .woocommerce {{WOPB}} .wopb-builder-cart .cart .single_add_to_cart_button , {{WOPB}} .single_add_to_cart_button ,{{WOPB}} .single_add_to_cart_button:not(:hover):not(:active):not(.has-background), {{WOPB}} .single_add_to_cart_button:not(:hover):not(:active):not(.has-background)',
			},
		],
	},
	btnBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: {
				top: 0,
				right: 0,
				bottom: 0,
				left: 0,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .single_add_to_cart_button , .woocommerce {{WOPB}} .wopb-builder-cart .cart .single_add_to_cart_button',
			},
		],
	},
	btnRadius: {
		type: 'object',
		default: {
			lg: '0',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .single_add_to_cart_button , .woocommerce {{WOPB}} .wopb-builder-cart .cart .single_add_to_cart_button {border-radius:{{btnRadius}}; }',
			},
		],
	},
	btnShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				selector:
					'{{WOPB}} .single_add_to_cart_button , .woocommerce {{WOPB}} .wopb-builder-cart .cart .single_add_to_cart_button',
			},
		],
	},
	btnHoverColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .single_add_to_cart_button:hover , .woocommerce {{WOPB}} .wopb-builder-cart .cart .single_add_to_cart_button:hover {color:{{btnHoverColor}}; opacity: unset; }',
			},
		],
	},
	btnBgHoverColor: {
		type: 'object',
		default: {
			openColor: 1,
			type: 'color',
			color: '#FF176B',
		},
		style: [
			{
				selector:
					'{{WOPB}} .single_add_to_cart_button:hover , .woocommerce {{WOPB}} .wopb-builder-cart .cart .single_add_to_cart_button:hover',
			},
		],
	},
	btnHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .single_add_to_cart_button:hover , .woocommerce {{WOPB}} .wopb-builder-cart .cart .single_add_to_cart_button:hover ',
			},
		],
	},
	btnHoverRadius: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .single_add_to_cart_button:hover , .woocommerce {{WOPB}} .wopb-builder-cart .cart .single_add_to_cart_button:hover {border-radius:{{btnHoverRadius}}; }',
			},
		],
	},
	btnHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				selector:
					'{{WOPB}} .single_add_to_cart_button:hover, .woocommerce {{WOPB}} .wopb-builder-cart .cart .single_add_to_cart_button:hover ',
			},
		],
	},
	btnSacing: {
		type: 'object',
		default: {
			lg: {
				top: 0,
				bottom: 0,
				left: 0,
				right: 0,
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .single_add_to_cart_button , .woocommerce {{WOPB}} .wopb-builder-cart .cart .single_add_to_cart_button {margin:{{btnSacing}};}',
			},
		],
	},
	btnPadding: {
		type: 'object',
		default: {
			lg: {
				top: '14',
				bottom: '14',
				left: '24',
				right: '24',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-builder-cart .single_add_to_cart_button , .woocommerce {{WOPB}} .wopb-builder-cart .single_add_to_cart_button {padding:{{btnPadding}} !important;}',
			},
		],
	},
	showBuyNow: {
		type: 'boolean',
		default: false,
	},
	removeProduct: {
		type: 'boolean',
		default: true,
	},
	btnBuyText: {
		type: 'string',
		default: 'Buy Now',
	},
	btnBuyTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: {
				lg: 16,
				unit: 'px',
			},
			height: {
				lg: 20,
				unit: 'px',
			},
			spacing: {
				lg: 0,
				unit: 'px',
			},
			transform: 'uppercase',
			weight: '500',
			decoration: 'none',
			family: '',
		},
		style: [
			{
				depends: [
					{ key: 'showBuyNow', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-buy-button',
			},
		],
	},
	btnBuyColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [
					{ key: 'showBuyNow', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-buy-button {color:{{btnBuyColor}};}',
			},
		],
	},
	btnBuyBgColor: {
		type: 'object',
		default: {
			openColor: 1,
			type: 'color',
			color: '#070707',
		},
		style: [
			{
				depends: [
					{ key: 'showBuyNow', condition: '==', value: true },
				],
				selector:
					'.woocommerce {{WOPB}} button.disabled.wc-variation-selection-needed.wopb-buy-button, ' +
					'{{WOPB}} .wopb-buy-button ,' +
					'{{WOPB}} .wopb-buy-button:not(:hover):not(:active):not(.has-background), ' +
					'{{WOPB}} .wopb-buy-button:not(:hover):not(:active):not(.has-background)',
			},
		],
	},
	btnBuyBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: {
				top: 0,
				right: 0,
				bottom: 0,
				left: 0,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{ key: 'showBuyNow', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-buy-button',
			},
		],
	},
	btnBuyRadius: {
		type: 'object',
		default: {
			lg: '0',
			unit: 'px',
		},
		style: [
			{
				depends: [
					{ key: 'showBuyNow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-buy-button {border-radius:{{btnBuyRadius}}; }',
			},
		],
	},
	btnBuyShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				depends: [
					{ key: 'showBuyNow', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-buy-button , ',
			},
		],
	},
	btnBuyHoverColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [
					{ key: 'showBuyNow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-buy-button:hover {color:{{btnBuyHoverColor}}; opacity: unset; }',
			},
		],
	},
	btnBuyBgHoverColor: {
		type: 'object',
		default: {
			openColor: 1,
			type: 'color',
			color: '#FF176B',
		},
		style: [
			{
				depends: [
					{ key: 'showBuyNow', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-buy-button:hover',
			},
		],
	},
	btnBuyHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{ key: 'showBuyNow', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-buy-button:hover',
			},
		],
	},
	btnBuyHoverRadius: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				depends: [
					{ key: 'showBuyNow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-buy-button:hover {border-radius:{{btnBuyHoverRadius}}; }',
			},
		],
	},
	btnBuyHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				depends: [
					{ key: 'showBuyNow', condition: '==', value: true },
				],
				selector: '{{WOPB}} .wopb-buy-button:hover',
			},
		],
	},
	btnBuyMargin: {
		type: 'object',
		default: {
			lg: {
				top: 0,
				bottom: 0,
				left: 0,
				right: 0,
				unit: 'px',
			},
		},
		style: [
			{
				depends: [
					{ key: 'showBuyNow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-buy-button {margin:{{btnBuyMargin}};}',
			},
		],
	},
	btnBuyPadding: {
		type: 'object',
		default: {
			lg: {
				top: '14',
				bottom: '14',
				left: '24',
				right: '24',
				unit: 'px',
			},
		},
		style: [
			{
				depends: [
					{ key: 'showBuyNow', condition: '==', value: true },
				],
				selector:
					'{{WOPB}} .wopb-builder-cart .wopb-buy-button {padding:{{btnBuyPadding}} !important;}',
			},
		],
	},
	wrapBg: {
		type: 'string',
		default: {
			openColor: 0,
			type: 'color',
			color: '#f5f5f5',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{background-color:red;}',
			},
		],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapRadius: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper { border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: {
			openColor: 0,
			type: 'color',
			color: '#ff176b',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapHoverRadius: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapMargin: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '20',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper { margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '',
				left: '',
				right: '',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper { padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	wrapInnerPadding: {
		type: 'object',
		default: {
			lg: {
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper { padding:{{wrapInnerPadding}}; }',
			},
		],
	},
	advanceId: {
		type: 'string',
		default: '',
	},
	advanceZindex: {
		type: 'string',
		default: '',
		style: [
			{
				selector: '{{WOPB}} {z-index:{{advanceZindex}};}',
			},
		],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: {
		type: 'string',
		default: '',
		style: [
			{
				selector: '',
			},
		],
	},
};

export default attributes;
