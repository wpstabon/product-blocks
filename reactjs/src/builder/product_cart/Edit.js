const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import {
	GeneralAdvanced,
	CustomCssAdvanced,
	ResponsiveAdvanced,
	ButtonStyle,
	CommonSettings,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Sections, Section } from '../../helper/Sections';
import { AddSettingsToToolbar } from './Settings';
import useBlockId from '../../helper/hooks/use-block-id';
const plusIcon = (
	<svg
		version="1.1"
		xmlns="http://www.w3.org/2000/svg"
		width="24"
		height="24"
		viewBox="0 0 32 32"
	>
		<path d="M31 12h-11v-11c0-0.552-0.448-1-1-1h-6c-0.552 0-1 0.448-1 1v11h-11c-0.552 0-1 0.448-1 1v6c0 0.552 0.448 1 1 1h11v11c0 0.552 0.448 1 1 1h6c0.552 0 1-0.448 1-1v-11h11c0.552 0 1-0.448 1-1v-6c0-0.552-0.448-1-1-1z"></path>
	</svg>
);
const minusIcon = (
	<svg
		version="1.1"
		xmlns="http://www.w3.org/2000/svg"
		width="24"
		height="24"
		viewBox="0 0 32 32"
	>
		<path d="M0 13v6c0 0.552 0.448 1 1 1h30c0.552 0 1-0.448 1-1v-6c0-0.552-0.448-1-1-1h-30c-0.552 0-1 0.448-1 1z"></path>
	</svg>
);

export default function Edit( props ) {
	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: {
			blockId,
			advanceId,
			quantityBtnPosition,
			showQuantity,
			showQuantityBtn,
			preview,
			showBuyNow,
			btnBuyText,
		},
	} = props;
	const store = { setAttributes, name, attributes, clientId };

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/product-cart', blockId );
	}

	return (
		<>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Style', 'product-blocks' ) }
					>
						<CommonSettings
							initialOpen={ true }
							title={ 'inline' }
							store={ store }
							include={ [
								{
									position: 1,
									data: {
										type: 'select',
										key: 'preview',
										label: 'Preview (Product Type)',
										options: [
											{
												value: 'simple',
												label: __(
													'Simple Product',
													'product-blocks'
												),
											},
											{
												value: 'grouped',
												label: __(
													'Grouped Product',
													'product-blocks'
												),
											},
											{
												value: 'external',
												label: __(
													'External/Affiliate Product',
													'product-blocks'
												),
											},
											{
												value: 'variable',
												label: __(
													'Variable Product',
													'product-blocks'
												),
											},
										],
									},
								},
								{
									position: 2,
									data: {
										type: 'alignment',
										key: 'addtocartAlignment',
										disableJustify: true,
										responsive: true,
										label: __(
											'Alignment',
											'product-blocks'
										),
									},
								},
							] }
						/>
						<CommonSettings
							depend="showQuantity"
							initialOpen={ false }
							title={ __( 'Quantity', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									position: 1,
									data: {
										type: 'color',
										key: 'quantityColor',
										label: __(
											'Quantity Color',
											'product-blocks'
										),
									},
								},
								{
									position: 2,
									data: {
										type: 'color',
										key: 'quantityBg',
										label: __(
											'Quantity Background',
											'product-blocks'
										),
									},
								},
								{
									position: 2,
									data: {
										type: 'range',
										min: 1,
										max: 160,
										key: 'quantityWrapWidth',
										label: __(
											'Quantity Width',
											'product-blocks'
										),
									},
								},
								{
									position: 3,
									data: {
										type: 'border',
										key: 'quantityBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									position: 4,
									data: {
										type: 'dimension',
										key: 'quantityRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									position: 5,
									data: {
										type: 'dimension',
										key: 'quantityPadding',
										label: __(
											'Quantity Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						{ showQuantity && (
							<CommonSettings
								depend="showQuantityBtn"
								initialOpen={ false }
								title={ __( 'Plus Minus', 'product-blocks' ) }
								store={ store }
								include={ [
									{
										position: 1,
										data: {
											type: 'select',
											key: 'quantityBtnPosition',
											label: 'Quantity Position',
											options: [
												{
													value: 'both',
													label: __(
														'Both Side',
														'product-blocks'
													),
												},
												{
													value: 'right',
													label: __(
														'Right Side',
														'product-blocks'
													),
												},
												{
													value: 'left',
													label: __(
														'Left Side',
														'product-blocks'
													),
												},
											],
										},
									},
									{
										position: 2,
										data: {
											type: 'color',
											key: 'plusMinusColor',
											label: __(
												'Icon Color',
												'product-blocks'
											),
										},
									},
									{
										position: 3,
										data: {
											type: 'color',
											key: 'plusMinusBg',
											label: __(
												'Icon Background',
												'product-blocks'
											),
										},
									},
									{
										position: 4,
										data: {
											type: 'color',
											key: 'plusMinusHoverColor',
											label: __(
												'Icon Hover Color',
												'product-blocks'
											),
										},
									},
									{
										position: 5,
										data: {
											type: 'color',
											key: 'plusMinusHoverBg',
											label: __(
												'Icon Hover Background',
												'product-blocks'
											),
										},
									},
									{
										position: 6,
										data: {
											type: 'range',
											min: '0',
											max: '40',
											key: 'plusMinusSize',
											label: __(
												'Icon Size',
												'product-blocks'
											),
										},
									},
									{
										position: 6,
										data: {
											type: 'range',
											min: 0,
											max: 60,
											key: 'plusMinusGap',
											label: __(
												'Icons/Quantity Gap',
												'product-blocks'
											),
										},
									},

									{
										position: 7,
										data: {
											type: 'border',
											key: 'plusMinusBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										position: 8,
										data: {
											type: 'dimension',
											key: 'plusMinusRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										position: 9,
										data: {
											type: 'dimension',
											key: 'plusMinusPadding',
											label: __(
												'Icon Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] }
							/>
						) }

						<CommonSettings
							depend="showBuyNow"
							initialOpen={ false }
							title={ __( 'Buy Now', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									position: 1,
									data: {
										type: 'toggle',
										key: 'removeProduct',
										label: __(
											'Remove Other Product',
											'product-blocks'
										),
									},
								},
								{
									position: 2,
									data: {
										type: 'text',
										key: 'btnBuyText',
										label: __(
											'Button Text',
											'product-blocks'
										),
									},
								},
								{
									position: 3,
									data: {
										type: 'typography',
										key: 'btnBuyTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									position: 4,
									data: {
										type: 'tab',
										key: 'buyTab',
										content: [
											{
												name: 'normal',
												title: 'Normal',
												options: [
													{
														type: 'color',
														key: 'btnBuyColor',
														label: __(
															'Color',
															'product-blocks'
														),
													},
													{
														type: 'color2',
														key: 'btnBuyBgColor',
														label: __(
															'Background Color',
															'product-blocks'
														),
													},
													{
														type: 'border',
														key: 'btnBuyBorder',
														label: __(
															'Border',
															'product-blocks'
														),
													},
													{
														type: 'dimension',
														key: 'btnBuyRadius',
														label: __(
															'Border Radius',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
													{
														type: 'boxshadow',
														key: 'btnBuyShadow',
														label: __(
															'BoxShadow',
															'product-blocks'
														),
													},
												],
											},
											{
												name: 'hover',
												title: 'Hover',
												options: [
													{
														type: 'color',
														key: 'btnBuyHoverColor',
														label: __(
															'Hover Color',
															'product-blocks'
														),
													},
													{
														type: 'color2',
														key: 'btnBuyBgHoverColor',
														label: __(
															'Hover Bg Color',
															'product-blocks'
														),
													},
													{
														type: 'border',
														key: 'btnBuyHoverBorder',
														label: __(
															'Hover Border',
															'product-blocks'
														),
													},
													{
														type: 'dimension',
														key: 'btnBuyHoverRadius',
														label: __(
															'Hover Radius',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
													{
														type: 'boxshadow',
														key: 'btnBuyHoverShadow',
														label: __(
															'Hover BoxShadow',
															'product-blocks'
														),
													},
												],
											},
										],
									},
								},
								{
									position: 5,
									data: {
										type: 'dimension',
										key: 'btnBuyMargin',
										label: __( 'Margin', 'product-blocks' ),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									position: 6,
									data: {
										type: 'dimension',
										key: 'btnBuyPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>

						<ButtonStyle
							title="Cart Button Style"
							store={ store }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar store={ store } />

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ `wopb-product-wrapper wopb-builder-cart` }>
					<form
						action="javascript:"
						className={
							`cart` +
							( preview == 'variable'
								? ' variations_form'
								: preview == 'grouped'
								? ' grouped_form'
								: '' )
						}
					>
						{ preview == 'variable' && (
							<table className="variations" cellSpacing="0">
								<tbody>
									<tr>
										<td className="label">
											<label>color</label>
										</td>
										<td className="value">
											<select>
												<option value="">
													Choose an option
												</option>
												<option
													value="blue"
													className={ `attached enabled` }
												>
													Blue
												</option>
												<option
													value="gray"
													className={ `attached enabled` }
												>
													Gray
												</option>
											</select>
										</td>
									</tr>
								</tbody>
							</table>
						) }
						{ preview == 'grouped' && (
							<table
								className={ `woocommerce-grouped-product-list group_table` }
							>
								<tbody>
									<tr
										id="product-14"
										className={ `woocommerce-grouped-product-list-item ast-article-single ast-woo-product-no-review product type-product post-14 status-publish first instock product_cat-hoodies has-post-thumbnail sale featured shipping-taxable purchasable product-type-simple` }
									>
										<td
											className={ `woocommerce-grouped-product-list-item__quantity` }
										>
											<div className={ `quantity` }>
												<label
													className={ `screen-reader-text` }
												>
													Hoodie with Pocket quantity
												</label>
												<input
													type="number"
													className="input-text qty text"
													step="1"
													min="0"
													max=""
													name="quantity[14]"
													value=""
													title="Qty"
													size="4"
													placeholder="0"
													inputMode="numeric"
												/>
												<span className="wopb-builder-cart-icon-right">
													<span className="wopb-builder-cart-plus">
														{ plusIcon }
													</span>
													<span className="wopb-builder-cart-minus">
														{ minusIcon }
													</span>
												</span>
											</div>
										</td>
										<td className="woocommerce-grouped-product-list-item__label">
											<label htmlFor="product-14">
												Hoodie with Pocket
											</label>
										</td>
										<td className="woocommerce-grouped-product-list-item__price">
											<del aria-hidden="true">
												<span className="woocommerce-Price-amount amount">
													<bdi>
														<span className="woocommerce-Price-currencySymbol">
															£
														</span>
														45.00
													</bdi>
												</span>
											</del>
											<ins>
												<span className="woocommerce-Price-amount amount">
													<bdi>
														<span className="woocommerce-Price-currencySymbol">
															£
														</span>
														35.00
													</bdi>
												</span>
											</ins>
										</td>
									</tr>
									<tr
										id="product-13"
										className="woocommerce-grouped-product-list-item ast-article-single ast-woo-product-no-review product type-product post-13 status-publish instock product_cat-accessories has-post-thumbnail featured shipping-taxable purchasable product-type-simple"
									>
										<td className="woocommerce-grouped-product-list-item__quantity">
											<div className="quantity">
												<label
													className="screen-reader-text"
													htmlFor="quantity_61d3f7768a1a9"
												>
													Sunglasses quantity
												</label>
												<input
													type="number"
													id="quantity_61d3f7768a1a9"
													className="input-text qty text"
													step="1"
													min="0"
													max=""
													name="quantity[13]"
													value=""
													title="Qty"
													size="4"
													placeholder="0"
													inputMode="numeric"
												/>
												<span className="wopb-builder-cart-icon-right">
													<span className="wopb-builder-cart-plus">
														{ plusIcon }
													</span>
													<span className="wopb-builder-cart-minus">
														{ minusIcon }
													</span>
												</span>
											</div>
										</td>
										<td className="woocommerce-grouped-product-list-item__label">
											<label htmlFor="product-13">
												<a href="#">Sunglasses</a>
											</label>
										</td>
										<td className="woocommerce-grouped-product-list-item__price">
											<span className="woocommerce-Price-amount amount">
												<bdi>
													<span className="woocommerce-Price-currencySymbol">
														£
													</span>
													90.00
												</bdi>
											</span>
										</td>
									</tr>
								</tbody>
							</table>
						) }
						{ preview != 'external' &&
							preview != 'grouped' &&
							showQuantity && (
								<div className={ `quantity` }>
									{ showQuantityBtn && (
										<>
											{ quantityBtnPosition == 'both' && (
												<span
													className={ `wopb-builder-cart-minus` }
												>
													{ minusIcon }
												</span>
											) }
											{ quantityBtnPosition == 'left' && (
												<span
													className={ `wopb-builder-cart-icon-left` }
												>
													<span
														className={ `wopb-builder-cart-plus` }
													>
														{ plusIcon }
													</span>
													<span
														className={ `wopb-builder-cart-minus` }
													>
														{ minusIcon }
													</span>
												</span>
											) }
										</>
									) }
									<input
										type="number"
										className={ `input-text qty text` }
										step="1"
										min="1"
										max=""
										name="quantity"
										value="1"
										title="Qty"
										size="4"
										placeholder=""
										inputMode="numeric"
									/>
									{ showQuantityBtn && (
										<>
											{ quantityBtnPosition ==
												'right' && (
												<span
													className={ `wopb-builder-cart-icon-right` }
												>
													<span
														className={ `wopb-builder-cart-plus` }
													>
														{ plusIcon }
													</span>
													<span
														className={ `wopb-builder-cart-minus` }
													>
														{ minusIcon }
													</span>
												</span>
											) }
											{ quantityBtnPosition == 'both' && (
												<span
													className={ `wopb-builder-cart-plus` }
												>
													{ plusIcon }
												</span>
											) }
										</>
									) }
								</div>
							) }
						<button
							type="submit"
							value="1"
							className="single_add_to_cart_button wopb-cart-button button alt"
						>
							{ 'Add to Cart' }
						</button>
						{ showBuyNow && (
							<div className="wopb-cart-bottom">
								<button
									type="submit"
									value="1"
									className="wopb-buy-button"
								>
									{ __(
										btnBuyText ? btnBuyText : 'Buy Now',
										'product-blocks'
									) }
								</button>
							</div>
						) }
					</form>
				</div>
			</div>
		</>
	);
}
