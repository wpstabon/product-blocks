const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';
registerBlockType( 'product-blocks/product-stock', {
	title: __( 'Product Stock', 'product-blocks' ),
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/builder/stock.svg' }
			alt={ __( 'Product Stock', 'product-blocks' ) }
		/>
	),
	category: 'single-product',
	description: __(
		'Dynamically control position, size, and typography of product stocks.',
		'product-blocks'
	),
	keywords: [
		__( 'Stock', 'product-blocks' ),
		__( 'Out of Stock', 'product-blocks' ),
		__( 'Product Stock', 'product-blocks' ),
	],
	supports: {
		align: [ 'center', 'wide', 'full' ],
	},
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
