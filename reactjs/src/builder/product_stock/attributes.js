const attributes = {
	blockId: {
		type: 'string',
		default: '',
	},
	currentPostId: { type: 'string', default: '' },
	preview: {
		type: 'string',
		default: 'instock',
	},
	stockAlign: {
		type: 'object',
		default: {
			lg: 'left',
		},
		style: [
			{
				selector: '{{WOPB}}{ text-align:{{stockAlign}}; }',
			},
		],
	},
	stockTypo: {
		type: 'object',
		default: {
			openTypography: 0,
			size: {
				lg: '',
				unit: 'px',
			},
			height: {
				lg: '',
				unit: 'px',
			},
			decoration: 'none',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{
				selector: '{{WOPB}}',
			},
		],
	},
	stockIn: {
		type: 'string',
		default: 'Hello',
	},
	stockInLabel: {
		type: 'string',
		default: 'In Stock',
	},
	stockColor: {
		type: 'string',
		default: '#2aab6f',
		style: [
			{
				selector:
					'{{WOPB}} .stock.in-stock, .woocommerce-js div.product {{WOPB}} .stock.in-stock {color:{{stockColor}}}',
			},
		],
	},
	stockOut: {
		type: 'string',
		default: '',
	},
	stockOutLabel: {
		type: 'string',
		default: 'Out of Stock',
	},
	stockOutColor: {
		type: 'string',
		default: '#e2401c',
		style: [
			{
				selector:
					'{{WOPB}} .stock.out-of-stock{color:{{stockOutColor}}}',
			},
		],
	},
	stockBack: {
		type: 'string',
		default: '',
	},
	stockBackLabel: {
		type: 'string',
		default: 'Available on backorder',
	},
	stockBackColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .stock.available-on-backorder{color:{{stockBackColor}}}',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: {
			openColor: 0,
			type: 'color',
			color: '#f5f5f5',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapRadius: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: {
			openColor: 0,
			type: 'color',
			color: '#ff176b',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapHoverRadius: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapMargin: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '20',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '',
				left: '',
				right: '',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	advanceId: {
		type: 'string',
		default: '',
	},
	advanceZindex: {
		type: 'string',
		default: '',
		style: [
			{
				selector: '{{WOPB}}{z-index:{{advanceZindex}};}',
			},
		],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: {
		type: 'string',
		default: '',
		style: [
			{
				selector: '',
			},
		],
	},
};
export default attributes;
