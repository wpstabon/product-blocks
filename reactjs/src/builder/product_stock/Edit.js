const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import {
	CommonSettings,
	GeneralAdvanced,
	CustomCssAdvanced,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Sections, Section } from '../../helper/Sections';
import { AddSettingsToToolbar } from './Settings';
import useBlockId from '../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: {
			blockId,
			advanceId,
			stockInLabel,
			stockOutLabel,
			stockBackLabel,
			preview,
		},
	} = props;
	const store = { setAttributes, name, attributes, clientId };

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/product-stock', blockId );
	}

	return (
		<>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Style', 'product-blocks' ) }
					>
						<CommonSettings
							initialOpen={ true }
							title={ 'inline' }
							include={ [
								{
									position: 1,
									data: {
										type: 'tag',
										key: 'preview',
										label: __(
											'Preview',
											'product-blocks'
										),
										options: [
											{
												value: 'instock',
												label: __(
													'In Stock',
													'product-blocks'
												),
											},
											{
												value: 'outofstock',
												label: __(
													'Out of Stock',
													'product-blocks'
												),
											},
											{
												value: 'onbackorder',
												label: __(
													'On Backorder',
													'product-blocks'
												),
											},
										],
									},
								},
								{
									position: 2,
									data: {
										type: 'alignment',
										key: 'stockAlign',
										disableJustify: true,
										responsive: true,
										label: __(
											'Alignment',
											'product-blocks'
										),
									},
								},
								{
									position: 3,
									data: {
										type: 'typography',
										key: 'stockTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									position: 4,
									data: {
										type: 'divider',
										key: 'stockIn',
										label: __(
											'In Stock',
											'product-blocks'
										),
									},
								},
								{
									position: 5,
									data: {
										type: 'text',
										key: 'stockInLabel',
										label: __( 'Label', 'product-blocks' ),
									},
								},
								{
									position: 6,
									data: {
										type: 'color',
										key: 'stockColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									position: 7,
									data: {
										type: 'divider',
										key: 'stockOut',
										label: __(
											'Out of Stock',
											'product-blocks'
										),
									},
								},
								{
									position: 8,
									data: {
										type: 'text',
										key: 'stockOutLabel',
										label: __( 'Label', 'product-blocks' ),
									},
								},
								{
									position: 9,
									data: {
										type: 'color',
										key: 'stockOutColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									position: 10,
									data: {
										type: 'divider',
										key: 'stockBack',
										label: __(
											'Available on Backorder',
											'product-blocks'
										),
									},
								},
								{
									position: 11,
									data: {
										type: 'text',
										key: 'stockBackLabel',
										label: __( 'Label', 'product-blocks' ),
									},
								},
								{
									position: 12,
									data: {
										type: 'color',
										key: 'stockBackColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
							] }
							store={ store }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar store={ store } />

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ `wopb-product-wrapper` }>
					{ preview == 'instock' && stockInLabel && (
						<p className={ `stock in-stock` }>{ stockInLabel }</p>
					) }
					{ preview == 'outofstock' && stockOutLabel && (
						<p className={ `stock out-of-stock` }>
							{ stockOutLabel }
						</p>
					) }
					{ preview == 'onbackorder' && stockBackLabel && (
						<p className={ `stock available-on-backorder` }>
							{ stockBackLabel }
						</p>
					) }
				</div>
			</div>
		</>
	);
}
