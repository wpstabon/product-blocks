const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';

registerBlockType( 'product-blocks/thankyou-address', {
	title: __( 'Thank You Address', 'product-blocks' ),
	icon: (
		<img
			src={
				wopb_data.url +
				'assets/img/blocks/builder/thank_you/thank_you_address.svg'
			}
			alt={ __( 'Image', 'product-blocks' ) }
			style={ { width: '100%' } }
		/>
	),
	category: 'thank-you',
	description: __(
		'Show addresses that the customer has provided.',
		'product-blocks'
	),
	keywords: [
		__( 'thankyou address', 'product-blocks' ),
		__( 'address', 'product-blocks' ),
		__( 'thank', 'product-blocks' ),
	],
	edit: Edit,
	save() {
		return null;
	},
	attributes,
} );
