const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../../helper/CssGenerator';
import {
	GeneralAdvanced,
	CustomCssAdvanced,
	CommonSettings,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../../helper/CommonPanel';
import { Sections, Section } from '../../../helper/Sections';
import { AddSettingsToToolbar } from './Settings';
import useBlockId from '../../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: { blockId, advanceId, billingText, shippingText },
	} = props;
	const store = { setAttributes, name, attributes, clientId };

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/thankyou-address', blockId );
	}

	return (
		<>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Settings', 'product-blocks' ) }
					>
						<CommonSettings
							initialOpen={ true }
							title="inline"
							store={ store }
							include={ [
								{
									data: {
										type: 'text',
										key: 'billingText',
										label: __(
											'Billing Text',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'text',
										key: 'shippingText',
										label: __(
											'Shipping Text',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'titleColor',
										label: __(
											'Title Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'titleTypo',
										label: __(
											'Title Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'titleSpace',
										label: __(
											'Title Space',
											'product-blocks'
										),
										min: 0,
										max: 50,
										step: 1,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ false }
							title={ __( 'Body', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'bodyColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'bodyTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'alignment',
										key: 'bodyAlign',
										label: __(
											'Alignment',
											'product-blocks'
										),
										disableJustify: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ false }
							title={ __( 'Container', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color2',
										key: 'containerBg',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'containerBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'containerRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'containerPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced initialOpen={ true } store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>
			<AddSettingsToToolbar store={ store } />
			<div
				id={ advanceId ? advanceId : '' }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ 'wopb-product-wrapper ' }>
					<div className="wopb-thankyou-address-container">
						<section className="woocommerce-customer-details">
							<div className="woocommerce-column--billing-address">
								<div className="wopb-billing-shipping-address">
									<h2 className="woocommerce-column__title wopb-address-title">
										{ billingText }
									</h2>
									<address>
										Clarabelle Dickinson<br></br>Marquardt
										PLC<br></br>68286 Bergnaum Mission Apt.
										993<br></br>South Dakota, 75660
										<br></br>Terencefort
										<p className="woocommerce-customer-details--phone">
											00288-1651
										</p>
										<p className="woocommerce-customer-details--email">
											dummy@shop.com
										</p>
									</address>
								</div>
							</div>
							<div className="woocommerce-column--shipping-address">
								<div className="wopb-billing-shipping-address">
									<h2 className="woocommerce-column__title wopb-address-title">
										{ shippingText }
									</h2>
									<address>
										Clarabelle Dickinson<br></br>Marquardt
										PLC<br></br>68286 Bergnaum Mission Apt.
										993<br></br>South Dakota, 75660
										<br></br>Terencefort
										<p className="woocommerce-customer-details--phone">
											00288-1651
										</p>
										<p className="woocommerce-customer-details--email">
											dummy@shop.com
										</p>
									</address>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</>
	);
}
