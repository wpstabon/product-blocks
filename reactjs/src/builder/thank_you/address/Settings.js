/** @format */

import {
	AlignmentTB,
	colorIcon,
	filterFields,
	SpacingArg,
	spacingIcon,
	typoIcon,
} from '../../../helper/CommonPanel';
import WopbToolbarDropdown from '../../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar } from '../../../helper/ux';

const { __ } = wp.i18n;

export function AddSettingsToToolbar( { store } ) {
	return (
		<WopbToolbarGroup text={ 'Thank You Address' }>
			<AlignmentTB
				store={ store }
				label={ __( 'Body Alignment', 'product-blocks' ) }
				attrKey="bodyAlign"
				responsive={ false }
			/>
			<WopbToolbarDropdown
				buttonContent={ typoIcon }
				store={ store }
				label={ __( 'Thank You Address Typography', 'product-blocks' ) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __(
							'Thank You Address Typography',
							'product-blocks'
						),
						options: [
							{
								type: 'typography',
								key: 'titleTypo',
								label: __(
									'Title Typography',
									'product-blocks'
								),
							},
							{
								type: 'typography',
								key: 'bodyTypo',
								label: __(
									'Body Message Typography',
									'product-blocks'
								),
							},
						],
					},
				] ) }
			/>
			<WopbToolbarDropdown
				buttonContent={ colorIcon }
				store={ store }
				label={ __( 'Thank You Address Color', 'product-blocks' ) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __(
							'Thank You Address Color',
							'product-blocks'
						),
						options: [
							{
								type: 'color',
								key: 'titleColor',
								label: __( 'Title Color', 'product-blocks' ),
							},
							{
								type: 'color',
								key: 'bodyColor',
								label: __( 'Body Color', 'product-blocks' ),
							},
							{
								type: 'color2',
								key: 'containerBg',
								label: __(
									'Container Background',
									'product-blocks'
								),
							},
						],
					},
				] ) }
			/>
			<WopbToolbarDropdown
				buttonContent={ spacingIcon }
				store={ store }
				label={ __( 'Thank You Address Spacing', 'product-blocks' ) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __(
							'Thank You Address Spacing',
							'product-blocks'
						),
						options: filterFields(
							[
								{
									data: {
										type: 'range',
										key: 'wrapMargin',
										min: 0,
										max: 100,
										step: 1,
										unit: true,
										responsive: true,
										dimensionKey: 'top',
										label: __(
											'Margin Top',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'wrapMargin',
										min: 0,
										max: 100,
										step: 1,
										unit: true,
										responsive: true,
										dimensionKey: 'bottom',
										label: __(
											'Margin Bottom',
											'product-blocks'
										),
									},
								},
							],
							'__all',
							SpacingArg
						),
					},
				] ) }
			/>
		</WopbToolbarGroup>
	);
}
