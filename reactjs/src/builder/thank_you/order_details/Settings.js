/** @format */

import { settingsIcon } from '../../../helper/CommonPanel';
import WopbToolbarDropdown from '../../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar, getData } from '../../../helper/ux';

/** @format */
const { __ } = wp.i18n;

export function AddSettingsToToolbar( { store, selected } ) {
	switch ( selected ) {
		case 'general':
			return (
				<WopbToolbarGroup text={ 'General' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'General Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'General Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'text',
											key: 'downloadText',
											label: __(
												'Download Text',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'text',
											key: 'orderDetailsText',
											label: __(
												'Order Details Text',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'titleColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'titleTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'tableBorder',
											label: __(
												'Table Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'tableRadius',
											label: __(
												'Table Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'range',
											key: 'titleSpace',
											label: __(
												'Title Space',
												'product-blocks'
											),
											min: 0,
											max: 50,
											step: 1,
										},
									},
									{
										data: {
											type: 'range',
											key: 'twoTableSpace',
											label: __(
												'Space Between',
												'product-blocks'
											),
											min: 0,
											max: 100,
											step: 1,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'table-header':
			return (
				<WopbToolbarGroup text={ 'Header' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Table Header Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'General Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'text',
											key: 'productText',
											label: __(
												'Product',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'text',
											key: 'totalText',
											label: __(
												'Total',
												'product-blocks'
											),
										},
									},

									{
										data: {
											type: 'color',
											key: 'headColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color2',
											key: 'headBg',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'headTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'headBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'headPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'alignment',
											key: 'headAlign',
											label: __(
												'Alignment',
												'product-blocks'
											),
											disableJustify: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'table-body':
			return (
				<WopbToolbarGroup text={ 'Body' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Table Body Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Table Body Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'bodyTextColor',
											label: __(
												'Text Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color2',
											key: 'bodyBgColor',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'bodyLinkColor',
											label: __(
												'Link Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'bodyLinkHoverColor',
											label: __(
												'Link Hover Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'bodyTypo',
											label: __(
												'Body Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'bodyBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'bodyPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'alignment',
											key: 'bodyAlign',
											label: __(
												'Alignment',
												'product-blocks'
											),
											disableJustify: true,
										},
									},

									{
										data: {
											type: 'divider',
											key: 'tableBtnDivider',
											label: __(
												'Button',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'tab',
											content: [
												{
													name: 'normal',
													title: 'Normal',
													options: [
														{
															type: 'color',
															key: 'tableBtnColor',
															label: __(
																'Color',
																'product-blocks'
															),
														},
														{
															type: 'color2',
															key: 'tableBtnBgColor',
															label: __(
																'Background',
																'product-blocks'
															),
														},
														{
															type: 'border',
															key: 'tableBtnBorder',
															label: __(
																'Border',
																'product-blocks'
															),
														},
														{
															type: 'typography',
															key: 'tableBtnTypo',
															label: __(
																'Typography',
																'product-blocks'
															),
														},
														{
															type: 'dimension',
															key: 'tableBtnRadius',
															label: __(
																'Border Radius',
																'product-blocks'
															),
															step: 1,
															unit: true,
															responsive: true,
														},
														{
															type: 'dimension',
															key: 'tableBtnPadding',
															label: __(
																'Padding',
																'product-blocks'
															),
															step: 1,
															unit: true,
															responsive: true,
														},
													],
												},
												{
													name: 'hover',
													title: 'Hover',
													options: [
														{
															type: 'color',
															key: 'tableBtnHoverColor',
															label: __(
																'Color',
																'product-blocks'
															),
														},
														{
															type: 'color2',
															key: 'tableBtnBgHoverColor',
															label: __(
																'Background',
																'product-blocks'
															),
														},
														{
															type: 'border',
															key: 'tableBtnHoverBorder',
															label: __(
																'Border',
																'product-blocks'
															),
														},
													],
												},
											],
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'table-footer':
			return (
				<WopbToolbarGroup text={ 'Footer' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Table Footer Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Table Footer Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'text',
											key: 'subTotalText',
											label: __(
												'Subtotal',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'text',
											key: 'shippingText',
											label: __(
												'Shipping Cost',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'text',
											key: 'payMethodText',
											label: __(
												'Payment Method',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'text',
											key: 'footTotalText',
											label: __(
												'Total',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'footColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color2',
											key: 'footBg',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'footTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'footBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'footPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'alignment',
											key: 'footAlign',
											label: __(
												'Alignment',
												'product-blocks'
											),
											disableJustify: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'container':
			return (
				<WopbToolbarGroup text={ 'Container' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Cart Update Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Cart Update Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'color2',
											key: 'containerBg',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'containerBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'containerRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'containerPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		// case "":
		//     return (
		//         <WopbToolbarGroup text={"Image"}>
		//             <WopbToolbarDropdown
		//                 buttonContent={settingsIcon}
		//                 store={store}
		//                 label={__("Product Image Settings", "product-blocks")}
		//                 content={formatSettingsForToolbar([
		//                     {
		//                         name: "tab",
		//                         title: __(
		//                             "Product Image Settings",
		//                             "product-blocks"
		//                         ),
		//                         options: getData(),
		//                     },
		//                 ])}
		//             />
		//         </WopbToolbarGroup>
		//     );
		default:
			return null;
	}
}
