/** @format */

const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../../helper/CssGenerator';
import {
	GeneralAdvanced,
	CustomCssAdvanced,
	CommonSettings,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../../helper/CommonPanel';
import { Sections, Section } from '../../../helper/Sections';
import ToolBarElement from '../../../helper/ToolBarElement';
import { AddSettingsToToolbar } from './Settings';
import useFluentSettings from '../../../helper/hooks/useFluentSettings';
import useBlockId from '../../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: {
			blockId,
			advanceId,
			downloadText,
			orderDetailsText,
			productText,
			totalText,
			subTotalText,
			payMethodText,
			shippingText,
			footTotalText,
		},
	} = props;

	const store = { setAttributes, name, attributes, clientId };

	const { section, toolbarSettings, setCurrSettings } = useFluentSettings();

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator(
			attributes,
			'product-blocks/thankyou-order-details',
			blockId
		);
	}

	return (
		<>
			<ToolBarElement
				store={ store }
				include={ [
					{
						type: 'block_spacing',
					},
				] }
			/>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Settings', 'product-blocks' ) }
					>
						<CommonSettings
							initialOpen={ section.general }
							title={ __( 'General', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'text',
										key: 'downloadText',
										label: __(
											'Download Text',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'text',
										key: 'orderDetailsText',
										label: __(
											'Order Details Text',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'titleColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'titleTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'tableBorder',
										label: __(
											'Table Border',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'tableRadius',
										label: __(
											'Table Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'range',
										key: 'titleSpace',
										label: __(
											'Title Space',
											'product-blocks'
										),
										min: 0,
										max: 50,
										step: 1,
									},
								},
								{
									data: {
										type: 'range',
										key: 'twoTableSpace',
										label: __(
											'Space Between',
											'product-blocks'
										),
										min: 0,
										max: 100,
										step: 1,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'table-header' ] }
							title={ __( 'Table Header', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'text',
										key: 'productText',
										label: __(
											'Product',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'text',
										key: 'totalText',
										label: __( 'Total', 'product-blocks' ),
									},
								},

								{
									data: {
										type: 'color',
										key: 'headColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color2',
										key: 'headBg',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'headTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'headBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'headPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'alignment',
										key: 'headAlign',
										label: __(
											'Alignment',
											'product-blocks'
										),
										disableJustify: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'table-body' ] }
							title={ __( 'Table Body', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'bodyTextColor',
										label: __(
											'Text Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color2',
										key: 'bodyBgColor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'bodyLinkColor',
										label: __(
											'Link Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'bodyLinkHoverColor',
										label: __(
											'Link Hover Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'bodyTypo',
										label: __(
											'Body Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'bodyBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'bodyPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'alignment',
										key: 'bodyAlign',
										label: __(
											'Alignment',
											'product-blocks'
										),
										disableJustify: true,
									},
								},

								{
									data: {
										type: 'divider',
										key: 'tableBtnDivider',
										label: __( 'Button', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'tab',
										content: [
											{
												name: 'normal',
												title: 'Normal',
												options: [
													{
														type: 'color',
														key: 'tableBtnColor',
														label: __(
															'Color',
															'product-blocks'
														),
													},
													{
														type: 'color2',
														key: 'tableBtnBgColor',
														label: __(
															'Background',
															'product-blocks'
														),
													},
													{
														type: 'border',
														key: 'tableBtnBorder',
														label: __(
															'Border',
															'product-blocks'
														),
													},
													{
														type: 'typography',
														key: 'tableBtnTypo',
														label: __(
															'Typography',
															'product-blocks'
														),
													},
													{
														type: 'dimension',
														key: 'tableBtnRadius',
														label: __(
															'Border Radius',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
													{
														type: 'dimension',
														key: 'tableBtnPadding',
														label: __(
															'Padding',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
												],
											},
											{
												name: 'hover',
												title: 'Hover',
												options: [
													{
														type: 'color',
														key: 'tableBtnHoverColor',
														label: __(
															'Color',
															'product-blocks'
														),
													},
													{
														type: 'color2',
														key: 'tableBtnBgHoverColor',
														label: __(
															'Background',
															'product-blocks'
														),
													},
													{
														type: 'border',
														key: 'tableBtnHoverBorder',
														label: __(
															'Border',
															'product-blocks'
														),
													},
												],
											},
										],
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'table-footer' ] }
							title={ __( 'Table Footer', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'text',
										key: 'subTotalText',
										label: __(
											'Subtotal',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'text',
										key: 'shippingText',
										label: __(
											'Shipping Cost',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'text',
										key: 'payMethodText',
										label: __(
											'Payment Method',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'text',
										key: 'footTotalText',
										label: __( 'Total', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'footColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color2',
										key: 'footBg',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'footTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'footBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'footPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'alignment',
										key: 'footAlign',
										label: __(
											'Alignment',
											'product-blocks'
										),
										disableJustify: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section.container }
							title={ __( 'Container', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color2',
										key: 'containerBg',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'containerBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'containerRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'containerPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced initialOpen={ true } store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar
				store={ store }
				selected={ toolbarSettings }
			/>

			<div
				id={ advanceId ? advanceId : '' }
				className={ `wopb-block-${ blockId } ${ className }` }
				onClick={ ( e ) => setCurrSettings( e, 'container' ) }
			>
				<div className={ 'wopb-product-wrapper ' }>
					<div className="wopb-thankyou-order-details-container">
						{ /* order downloads  */ }
						<section className="woocommerce-order-downloads">
							<h2
								className="woocommerce-order-downloads__title wopb-component-simple"
								onClick={ ( e ) =>
									setCurrSettings( e, 'general' )
								}
							>
								{ downloadText }
							</h2>
							<table className="woocommerce-table woocommerce-table--order-downloads shop_table shop_table_responsive order_details">
								<thead
									onClick={ ( e ) =>
										setCurrSettings( e, 'table-header' )
									}
								>
									<tr>
										<th className="download-product wopb-component-simple">
											<span className="nobr">
												Product
											</span>
										</th>
										<th className="download-remaining wopb-component-simple">
											<span className="nobr">
												Downloads remaining
											</span>
										</th>
										<th className="download-expires wopb-component-simple">
											<span className="nobr">
												Expires
											</span>
										</th>
										<th className="download-file wopb-component-simple">
											<span className="nobr">
												Download
											</span>
										</th>
									</tr>
								</thead>
								<tbody
									onClick={ ( e ) =>
										setCurrSettings( e, 'table-body' )
									}
								>
									<tr>
										<td
											className="download-product wopb-component-simple"
											data-title="Product"
										>
											<a href="#">Beanie with Logo</a>
										</td>
										<td
											className="download-remaining wopb-component-simple"
											data-title="Downloads remaining"
										>
											12
										</td>
										<td
											className="download-expires wopb-component-simple"
											data-title="Expires"
										>
											Never
										</td>
										<td
											className="download-file wopb-component-simple"
											data-title="Download"
										>
											<a
												href="#"
												className="woocommerce-MyAccount-downloads-file button alt"
											>
												dwn-Beanie with Logo
											</a>
										</td>
									</tr>
								</tbody>
							</table>
						</section>

						{ /* Order details  */ }
						<section className="woocommerce-order-details">
							<h2
								className="woocommerce-order-details__title wopb-component-simple"
								onClick={ ( e ) =>
									setCurrSettings( e, 'general' )
								}
							>
								{ orderDetailsText }
							</h2>
							<table className="woocommerce-table woocommerce-table--order-details shop_table order_details">
								<thead
									onClick={ ( e ) =>
										setCurrSettings( e, 'table-header' )
									}
								>
									<tr>
										<th className="woocommerce-table__product-name product-name wopb-component-simple">
											{ productText }
										</th>
										<th className="woocommerce-table__product-table product-total wopb-component-simple">
											{ totalText }
										</th>
									</tr>
								</thead>
								<tbody
									onClick={ ( e ) =>
										setCurrSettings( e, 'table-body' )
									}
								>
									<tr className="woocommerce-table__line-item order_item">
										<td className="woocommerce-table__product-name product-name wopb-component-simple">
											<a href="#">Beanie with Logo</a>{ ' ' }
											<strong className="product-quantity">
												×&nbsp;2
											</strong>
										</td>
										<td className="woocommerce-table__product-total product-total wopb-component-simple">
											<span className="woocommerce-Price-amount amount">
												<bdi>
													36.00
													<span className="woocommerce-Price-currencySymbol">
														$
													</span>
												</bdi>
											</span>
										</td>
									</tr>
								</tbody>
								<tfoot
									onClick={ ( e ) =>
										setCurrSettings( e, 'table-footer' )
									}
								>
									<tr>
										<th
											scope="row"
											className="wopb-component-simple"
										>
											{ subTotalText }
										</th>
										<td className="wopb-component-simple">
											<span className="woocommerce-Price-amount amount">
												36.00
												<span className="woocommerce-Price-currencySymbol">
													$
												</span>
											</span>
										</td>
									</tr>
									<tr>
										<th
											scope="row"
											className="wopb-component-simple"
										>
											{ shippingText }
										</th>
										<td className="wopb-component-simple">
											<span className="woocommerce-Price-amount amount">
												50.00
												<span className="woocommerce-Price-currencySymbol">
													$
												</span>
											</span>
											&nbsp;
											<small className="shipped_via">
												via Flat rate
											</small>
										</td>
									</tr>
									<tr>
										<th
											scope="row"
											className="wopb-component-simple"
										>
											{ payMethodText }
										</th>
										<td className="wopb-component-simple">
											Check payments
										</td>
									</tr>
									<tr>
										<th
											scope="row"
											className="wopb-component-simple"
										>
											{ footTotalText }
										</th>
										<td className="wopb-component-simple">
											<span className="woocommerce-Price-amount amount">
												86.00
												<span className="woocommerce-Price-currencySymbol">
													$
												</span>
											</span>
										</td>
									</tr>
								</tfoot>
							</table>
						</section>
					</div>
				</div>
			</div>
		</>
	);
}
