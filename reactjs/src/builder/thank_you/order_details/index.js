const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';

registerBlockType( 'product-blocks/thankyou-order-details', {
	title: __( 'Thank You Order Details', 'product-blocks' ),
	icon: (
		<img
			src={
				wopb_data.url +
				'assets/img/blocks/builder/thank_you/thankyou_order_details.svg'
			}
			alt={ __( 'Image', 'product-blocks' ) }
			style={ { width: '100%' } }
		/>
	),
	category: 'thank-you',
	description: __(
		'Display all order details including product info, product attributes, prices, etc.',
		'product-blocks'
	),
	keywords: [
		__( 'order details', 'product-blocks' ),
		__( 'order', 'product-blocks' ),
		__( 'details', 'product-blocks' ),
		__( 'address', 'product-blocks' ),
		__( 'thank', 'product-blocks' ),
	],
	edit: Edit,
	save() {
		return null;
	},
	attributes,
} );
