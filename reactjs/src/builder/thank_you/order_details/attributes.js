const attributes = {
	blockId: { type: 'string', default: '' },
	currentPostId: { type: 'string', default: '' },
	downloadText: { type: 'string', default: 'Downloads' },
	orderDetailsText: { type: 'string', default: 'Order Details' },
	titleColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container .woocommerce-order-downloads__title , {{WOPB}} .wopb-thankyou-order-details-container .woocommerce-order-details__title { color:{{titleColor}}; }',
			},
		],
	},
	titleTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '24', unit: 'px' },
			height: { lg: '32', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: '600',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container .woocommerce-order-downloads__title , {{WOPB}} .wopb-thankyou-order-details-container .woocommerce-order-details__title',
			},
		],
	},
	tableBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 0, right: 1, bottom: 0, left: 1 },
			color: '#E6E6E6',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container table , .editor-styles-wrapper {{WOPB}} .wopb-thankyou-order-details-container table ',
			},
		],
	},
	tableRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container table { overflow: hidden; border-radius:{{tableRadius}}; }',
			},
		],
	},
	titleSpace: {
		type: 'string',
		default: '14',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container .woocommerce-order-downloads__title, {{WOPB}} .wopb-thankyou-order-details-container .woocommerce-order-details__title { margin-bottom:{{titleSpace}}px;}',
			},
		],
	},
	twoTableSpace: {
		type: 'string',
		default: '48',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container .woocommerce-order-downloads { margin-bottom:{{twoTableSpace}}px;}',
			},
		],
	},
	productText: { type: 'string', default: 'Product' },
	totalText: { type: 'string', default: 'Total' },
	headColor: {
		type: 'string',
		default: '#4A4A4A',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container table thead tr th { color:{{headColor}}; }',
			},
		],
	},
	headBg: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#E6E6E6' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container table.shop_table thead',
			},
		],
	},
	headTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: '600',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container table thead tr th ',
			},
		],
	},
	headBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 0, right: 0, bottom: 0, left: 0 },
			color: '#d5d5d5',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container table thead tr th ',
			},
		],
	},
	headPadding: {
		type: 'object',
		default: {
			lg: { top: 14, right: 32, bottom: 14, left: 32, unit: 'px' },
		},
		style: [
			{
				selector:
					'.woocommerce-page {{WOPB}} .wopb-thankyou-order-details-container table thead tr th, {{WOPB}} .wopb-thankyou-order-details-container table thead tr th { padding:{{headPadding}}; }',
			},
		],
	},
	headAlign: {
		type: 'string',
		default: 'left',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container table thead tr th { text-align:{{headAlign}}} ',
			},
		],
	},
	bodyTextColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container table tbody tr td { color:{{bodyTextColor}}; }',
			},
		],
	},
	bodyBgColor: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#fff' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container table tbody ',
			},
		],
	},
	bodyLinkColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container table tbody tr td a { color:{{bodyLinkColor}}; }',
			},
		],
	},
	bodyLinkHoverColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container table tbody tr td a:hover { color:{{bodyLinkHoverColor}}; }',
			},
		],
	},
	bodyTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: '400',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container table tbody tr td',
			},
		],
	},
	bodyBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 0, right: 0, bottom: 1, left: 0 },
			color: '#E6E6E6',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container table tbody tr td ',
			},
		],
	},
	bodyPadding: {
		type: 'object',
		default: {
			lg: { top: 14, right: 32, bottom: 14, left: 32, unit: 'px' },
		},
		style: [
			{
				selector:
					'.woocommerce-page {{WOPB}} .wopb-thankyou-order-details-container table tbody tr td, {{WOPB}} .wopb-thankyou-order-details-container table tbody tr td { padding:{{bodyPadding}}; }',
			},
		],
	},
	bodyAlign: {
		type: 'string',
		default: 'left',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container table tbody tr td { text-align:{{bodyAlign}}} ',
			},
		],
	},
	tableBtnDivider: { type: 'string', default: '' },
	tableBtnColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container .woocommerce-MyAccount-downloads-file.button { color:{{tableBtnColor}}; }',
			},
		],
	},
	tableBtnBgColor: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#FF176B' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container .woocommerce-MyAccount-downloads-file.button ',
			},
		],
	},
	tableBtnBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 0, right: 0, bottom: 0, left: 0 },
			color: '#cccccc',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container .woocommerce-MyAccount-downloads-file.button ',
			},
		],
	},
	tableBtnTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: '400',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container table tbody tr td a',
			},
		],
	},
	tableBtnRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container .woocommerce-MyAccount-downloads-file.button { border-radius:{{tableBtnRadius}}; }',
			},
		],
	},
	tableBtnPadding: {
		type: 'object',
		default: {
			lg: { top: 10, right: 20, bottom: 10, left: 20, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container .woocommerce-MyAccount-downloads-file.button { padding:{{tableBtnPadding}} ; }',
			},
		],
	},
	tableBtnHoverColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container .woocommerce-MyAccount-downloads-file.button:hover { color:{{tableBtnHoverColor}}; }',
			},
		],
	},
	tableBtnBgHoverColor: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#070707' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container .woocommerce-MyAccount-downloads-file.button:hover ',
			},
		],
	},
	tableBtnHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 0, right: 0, bottom: 0, left: 0 },
			color: '#cccccc',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container .woocommerce-MyAccount-downloads-file.button:hover ',
			},
		],
	},
	subTotalText: { type: 'string', default: 'Subtotal' },
	shippingText: { type: 'string', default: 'Shipping Cost:' },
	payMethodText: { type: 'string', default: 'Payment Method' },
	footTotalText: { type: 'string', default: 'Total' },
	footColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container table tfoot { color:{{footColor}}; }',
			},
		],
	},
	footBg: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#fff' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container table tfoot ',
			},
		],
	},
	footTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: '600',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container table tfoot, {{WOPB}} .wopb-thankyou-order-details-container table tfoot th',
			},
		],
	},
	footBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 0, right: 0, bottom: 1, left: 0 },
			color: '#E6E6E6',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container table tfoot tr th , {{WOPB}} .wopb-thankyou-order-details-container table tfoot tr td ',
			},
		],
	},
	footPadding: {
		type: 'object',
		default: {
			lg: { top: 14, right: 32, bottom: 14, left: 32, unit: 'px' },
		},
		style: [
			{
				selector:
					'.woocommerce-page {{WOPB}} .wopb-thankyou-order-details-container table tfoot tr th, .woocommerce-page {{WOPB}} .wopb-thankyou-order-details-container table tfoot tr td, {{WOPB}} .wopb-thankyou-order-details-container table tfoot tr th,{{WOPB}} .wopb-thankyou-order-details-container table tfoot tr td  { padding:{{footPadding}} ; }',
			},
		],
	},
	footAlign: {
		type: 'string',
		default: 'left',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-details-container table tfoot tr th , {{WOPB}} .wopb-thankyou-order-details-container table tfoot tr td { text-align:{{footAlign}}} ',
			},
		],
	},
	containerBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-details-container ',
			},
		],
	},
	containerBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#cccccc',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-details-container ',
			},
		],
	},
	containerRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-details-container { border-radius:{{containerRadius}}; }',
			},
		],
	},
	containerPadding: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-details-container { padding:{{containerPadding}} ; }',
			},
		],
	},
	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	wrapMargin: {
		type: 'object',
		default: { lg: { top: '', bottom: '20', unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#f5f5f5' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#ff176b' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapInnerPadding: {
		type: 'object',
		default: { lg: { unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ padding:{{wrapInnerPadding}}; }',
			},
		],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: { type: 'string', default: '', style: [ { selector: '' } ] },
};
export default attributes;
