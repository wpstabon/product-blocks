/** @format */

import {
	AlignmentTB,
	colorIcon,
	filterFields,
	SpacingArg,
	spacingIcon,
	typoIcon,
} from '../../../helper/CommonPanel';
import WopbToolbarDropdown from '../../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar } from '../../../helper/ux';

const { __ } = wp.i18n;

export function AddSettingsToToolbar( { store } ) {
	return (
		<WopbToolbarGroup text={ 'Order Payment' }>
			<AlignmentTB
				store={ store }
				attrKey="ulAlign"
				label={ __( 'Order Payment Alignment', 'product-blocks' ) }
				responsive={ false }
			/>
			<WopbToolbarDropdown
				buttonContent={ typoIcon }
				store={ store }
				label={ __( 'Order Payment Typography', 'product-blocks' ) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __(
							'Order Payment Typography',
							'product-blocks'
						),
						options: [
							{
								type: 'typography',
								key: 'labelTypo',
								label: __(
									'Label Typography',
									'product-blocks'
								),
							},
							{
								type: 'typography',
								key: 'valueTypo',
								label: __(
									'Value Typography',
									'product-blocks'
								),
							},
						],
					},
				] ) }
			/>
			<WopbToolbarDropdown
				buttonContent={ colorIcon }
				store={ store }
				label={ __( 'Order Payment Color', 'product-blocks' ) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __( 'Order Payment Color', 'product-blocks' ),
						options: [
							{
								type: 'color',
								key: 'labelColor',
								label: __( 'Label Color', 'product-blocks' ),
							},
							{
								type: 'color',
								key: 'valueColor',
								label: __( 'Value Color', 'product-blocks' ),
							},
							{
								type: 'color',
								key: 'separatorColor',
								label: __(
									'Separator Color',
									'product-blocks'
								),
							},
							{
								type: 'color2',
								key: 'containerBg',
								label: __(
									'Container Background',
									'product-blocks'
								),
							},
						],
					},
				] ) }
			/>
			<WopbToolbarDropdown
				buttonContent={ spacingIcon }
				store={ store }
				label={ __( 'Order Payment Spacing', 'product-blocks' ) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __( 'Order Payment Spacing', 'product-blocks' ),
						options: filterFields(
							[
								{
									data: {
										type: 'range',
										key: 'wrapMargin',
										min: 0,
										max: 100,
										step: 1,
										unit: true,
										responsive: true,
										dimensionKey: 'top',
										label: __(
											'Margin Top',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'wrapMargin',
										min: 0,
										max: 100,
										step: 1,
										unit: true,
										responsive: true,
										dimensionKey: 'bottom',
										label: __(
											'Margin Bottom',
											'product-blocks'
										),
									},
								},
							],
							'__all',
							SpacingArg
						),
					},
				] ) }
			/>
		</WopbToolbarGroup>
	);
}
