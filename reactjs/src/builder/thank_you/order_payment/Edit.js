const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../../helper/CssGenerator';
import {
	GeneralAdvanced,
	CustomCssAdvanced,
	CommonSettings,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../../helper/CommonPanel';
import { Sections, Section } from '../../../helper/Sections';
import { AddSettingsToToolbar } from './Settings';
import useBlockId from '../../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: {
			blockId,
			advanceId,
			orderText,
			dateText,
			emailText,
			totalText,
			payMethodText,
			pageLayout,
		},
	} = props;

	const store = { setAttributes, name, attributes, clientId };

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator(
			attributes,
			'product-blocks/thankyou-order-payment',
			blockId
		);
	}

	return (
		<>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Settings', 'product-blocks' ) }
					>
						<CommonSettings
							initialOpen={ true }
							title="inline"
							store={ store }
							include={ [
								{
									data: {
										type: 'tag',
										key: 'pageLayout',
										label: __( 'Layout', 'product-blocks' ),
										options: [
											{
												value: 'vertical',
												label: __(
													'Vertical',
													'product-blocks'
												),
											},
											{
												value: 'horizontal',
												label: __(
													'Horizontal',
													'product-blocks'
												),
											},
										],
									},
								},
								{
									data: {
										type: 'text',
										key: 'orderText',
										label: __(
											'Order Number',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'text',
										key: 'dateText',
										label: __( 'Date', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'text',
										key: 'emailText',
										label: __( 'Email', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'text',
										key: 'totalText',
										label: __( 'Total', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'text',
										key: 'payMethodText',
										label: __(
											'Payment Method',
											'product-blocks'
										),
									},
								},
								pageLayout == 'vertical' && {
									data: {
										type: 'alignment',
										key: 'ulAlign',
										label: __(
											'Alignment',
											'product-blocks'
										),
										disableJustify: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ false }
							title={ __( 'List Item', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'border',
										key: 'itemBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'itemPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'itemRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'range',
										key: 'itemSpace',
										label: __( 'Space', 'product-blocks' ),
										min: 0,
										max: 50,
										step: 1,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ false }
							title={ __( 'Label', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'labelColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'labelTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ false }
							title={ __( 'Value', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'valueColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'valueTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'valueSpace',
										label: __( 'Space', 'product-blocks' ),
										min: 0,
										max: 50,
										step: 1,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ false }
							title={ __( 'Separator', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'separatorColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'range',
										key: 'separatorBorder',
										label: __( 'Width', 'product-blocks' ),
										min: 0,
										max: 20,
										step: 1,
									},
								},
								{
									data: {
										type: 'tag',
										key: 'separatorBorderStyle',
										label: __( 'Style', 'product-blocks' ),
										options: [
											{
												value: 'solid',
												label: __(
													'solid',
													'product-blocks'
												),
											},
											{
												value: 'dotted',
												label: __(
													'dotted',
													'product-blocks'
												),
											},
											{
												value: 'dashed',
												label: __(
													'dashed',
													'product-blocks'
												),
											},
										],
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ false }
							title={ __( 'Container', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color2',
										key: 'containerBg',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'containerBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'boxshadow',
										key: 'containerShadow',
										label: __(
											'Box Shadow',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'containerRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'containerPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced initialOpen={ true } store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>
			<AddSettingsToToolbar store={ store } />
			<div
				id={ advanceId ? advanceId : '' }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ 'wopb-product-wrapper ' }>
					<div className="wopb-thankyou-order-payment-container">
						<ul className="woocommerce-order-overview woocommerce-thankyou-order-details order_details">
							<li className="woocommerce-order-overview__order order">
								{ ' ' }
								{ orderText } <strong>2938</strong>{ ' ' }
							</li>
							<div className="wopb-separator"></div>
							<li className="woocommerce-order-overview__date date">
								{ dateText } <strong>June 4, 2024</strong>{ ' ' }
							</li>
							<div className="wopb-separator"></div>
							<li className="woocommerce-order-overview__email email">
								{ ' ' }
								{ emailText }
								<strong>xyz@gmail.coma</strong>{ ' ' }
							</li>
							<div className="wopb-separator"></div>
							<li className="woocommerce-order-overview__total total">
								{ ' ' }
								{ totalText }{ ' ' }
								<strong>
									<span className="woocommerce-Price-amount amount">
										<bdi>
											86.00
											<span className="woocommerce-Price-currencySymbol">
												$
											</span>
										</bdi>
									</span>
								</strong>{ ' ' }
							</li>
							<div className="wopb-separator"></div>
							<li className="woocommerce-order-overview__payment-method method">
								{ ' ' }
								{ payMethodText }{ ' ' }
								<strong>Check payments</strong>{ ' ' }
							</li>
						</ul>
					</div>
				</div>
			</div>
		</>
	);
}
