const attributes = {
	blockId: { type: 'string', default: '' },
	currentPostId: { type: 'string', default: '' },
	pageLayout: {
		type: 'string',
		default: 'vertical',
		style: [
			{
				depends: [
					{ key: 'pageLayout', condition: '==', value: 'vertical' },
				],
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-payment-container ul { flex-direction:row; }',
			},
			{
				depends: [
					{ key: 'pageLayout', condition: '==', value: 'horizontal' },
				],
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-payment-container ul { flex-direction:column; }',
			},
		],
	},
	orderText: { type: 'string', default: 'Order' },
	dateText: { type: 'string', default: 'Date' },
	emailText: { type: 'string', default: 'Email' },
	totalText: { type: 'string', default: 'Total' },
	payMethodText: { type: 'string', default: 'Payment Method' },
	ulAlign: {
		type: 'string',
		default: 'center',
		style: [
			{
				depends: [ { key: 'ulAlign', condition: '==', value: 'left' } ],
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-payment-container ul { justify-content:flex-start; }',
			},
			{
				depends: [
					{ key: 'ulAlign', condition: '==', value: 'center' },
				],
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-payment-container ul { justify-content:center; }',
			},
			{
				depends: [
					{ key: 'ulAlign', condition: '==', value: 'right' },
				],
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-payment-container ul { justify-content:flex-end; }',
			},
		],
	},
	itemPadding: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-payment-container ul li { padding:{{itemPadding}}; }',
			},
		],
	},
	itemBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#cccccc',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-payment-container ul li ',
			},
		],
	},
	itemRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-payment-container ul li { border-radius:{{itemRadius}}; }',
			},
		],
	},
	itemSpace: {
		type: 'object',
		default: { lg: 38, sm: 20, xs: 15 },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-payment-container ul { gap:{{itemSpace}}px; }',
			},
		],
	},
	labelColor: {
		type: 'string',
		default: '#4A4A4A',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-payment-container ul li { color:{{labelColor}}; }',
			},
		],
	},
	labelTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: '',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-payment-container ul li ',
			},
		],
	},
	valueColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-payment-container ul li strong { color:{{valueColor}}; }',
			},
		],
	},
	valueTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '', unit: 'px' },
			decoration: 'none',
			transform: '',
			weight: '600',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-payment-container ul li strong ',
			},
		],
	},
	valueSpace: {
		type: 'string',
		default: '12',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-payment-container ul li strong { padding-top:{{valueSpace}}px;}',
			},
		],
	},
	separatorColor: {
		type: 'string',
		default: '#E6E6E6',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-payment-container ul .wopb-separator { border-color:{{separatorColor}}; }',
			},
		],
	},
	separatorBorder: {
		type: 'string',
		default: '1',
		style: [
			{
				depends: [
					{ key: 'pageLayout', condition: '==', value: 'vertical' },
				],
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-payment-container ul .wopb-separator { border-width: 0px {{separatorBorder}}px 0px 0px ; }',
			},
			{
				depends: [
					{ key: 'pageLayout', condition: '==', value: 'horizontal' },
				],
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-payment-container ul .wopb-separator { border-width: 0px 0px {{separatorBorder}}px 0px ; }',
			},
		],
	},
	separatorBorderStyle: {
		type: 'string',
		default: 'solid',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-payment-container ul .wopb-separator { border-style:{{separatorBorderStyle}}; }',
			},
		],
	},
	containerBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#f4f4f4' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-payment-container ',
			},
		],
	},
	containerBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#E6E6E6',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-payment-container ',
			},
		],
	},
	containerShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '',
		},
		style: [
			{ selector: '{{WOPB}} .wopb-thankyou-order-payment-container' },
		],
	},
	containerRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-payment-container { border-radius:{{containerRadius}}; }',
			},
		],
	},
	containerPadding: {
		type: 'object',
		default: {
			lg: { top: 32, right: 30, bottom: 32, left: 30, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-payment-container { padding:{{containerPadding}}; }',
			},
		],
	},
	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	wrapMargin: {
		type: 'object',
		default: { lg: { top: '', bottom: '20', unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#f5f5f5' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#ff176b' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapInnerPadding: {
		type: 'object',
		default: { lg: { unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ padding:{{wrapInnerPadding}}; }',
			},
		],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: { type: 'string', default: '', style: [ { selector: '' } ] },
};
export default attributes;
