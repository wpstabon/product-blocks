const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';

registerBlockType( 'product-blocks/thankyou-order-payment', {
	title: __( 'Order Payment', 'product-blocks' ),
	icon: (
		<img
			src={
				wopb_data.url +
				'assets/img/blocks/builder/thank_you/order_payment.svg'
			}
			alt={ __( 'Image', 'product-blocks' ) }
			style={ { width: '100%' } }
		/>
	),
	category: 'thank-you',
	description: __( 'Display the payment overview.', 'product-blocks' ),
	keywords: [
		__( 'Order', 'product-blocks' ),
		__( 'Payment', 'product-blocks' ),
		__( 'thankyou', 'product-blocks' ),
	],
	edit: Edit,
	save() {
		return null;
	},
	attributes,
} );
