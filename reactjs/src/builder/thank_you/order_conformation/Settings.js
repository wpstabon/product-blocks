/** @format */

import {
	colorIcon,
	filterFields,
	SpacingArg,
	spacingIcon,
	typoIcon,
} from '../../../helper/CommonPanel';
import WopbToolbarDropdown from '../../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar } from '../../../helper/ux';

const { __ } = wp.i18n;

export function AddSettingsToToolbar( { store } ) {
	return (
		<WopbToolbarGroup text={ 'Order Confirmation' }>
			<WopbToolbarDropdown
				buttonContent={
					<span className="dashicons dashicons-editor-justify"></span>
				}
				store={ store }
				label={ __( 'Order Confirmation Alignment', 'product-blocks' ) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __(
							'Order Confirmation Alignment',
							'product-blocks'
						),
						options: [
							{
								type: 'alignment',
								key: 'orderHeadAlign',
								label: __(
									'Heading Alignment',
									'product-blocks'
								),
								disableJustify: true,
							},
							{
								type: 'alignment',
								key: 'orderMessageAlign',
								label: __(
									'Order Message Alignment',
									'product-blocks'
								),
								disableJustify: true,
							},
						],
					},
				] ) }
			/>
			<WopbToolbarDropdown
				buttonContent={ typoIcon }
				store={ store }
				label={ __(
					'Order Confirmation Typography',
					'product-blocks'
				) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __(
							'Order Confirmation Typography',
							'product-blocks'
						),
						options: [
							{
								type: 'typography',
								key: 'orderHeadTypo',
								label: __(
									'Heading Typography',
									'product-blocks'
								),
							},
							{
								type: 'typography',
								key: 'messageTypo',
								label: __(
									'Order Message Typography',
									'product-blocks'
								),
							},
						],
					},
				] ) }
			/>
			<WopbToolbarDropdown
				buttonContent={ colorIcon }
				store={ store }
				label={ __( 'Order Confirmation Color', 'product-blocks' ) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __(
							'Order Confirmation Color',
							'product-blocks'
						),
						options: [
							{
								type: 'color',
								key: 'orderHeadColor',
								label: __( 'Heading Color', 'product-blocks' ),
							},
							{
								type: 'color',
								key: 'messageColor',
								label: __(
									'Order Message Color',
									'product-blocks'
								),
							},
							{
								type: 'color2',
								key: 'containerBg',
								label: __(
									'Container Background',
									'product-blocks'
								),
							},
						],
					},
				] ) }
			/>
			<WopbToolbarDropdown
				buttonContent={ spacingIcon }
				store={ store }
				label={ __( 'Order Confirmation Spacing', 'product-blocks' ) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __(
							'Order Confirmation Spacing',
							'product-blocks'
						),
						options: filterFields(
							[
								{
									data: {
										type: 'range',
										key: 'wrapMargin',
										min: 0,
										max: 100,
										step: 1,
										unit: true,
										responsive: true,
										dimensionKey: 'top',
										label: __(
											'Margin Top',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'wrapMargin',
										min: 0,
										max: 100,
										step: 1,
										unit: true,
										responsive: true,
										dimensionKey: 'bottom',
										label: __(
											'Margin Bottom',
											'product-blocks'
										),
									},
								},
							],
							'__all',
							SpacingArg
						),
					},
				] ) }
			/>
		</WopbToolbarGroup>
	);
}
