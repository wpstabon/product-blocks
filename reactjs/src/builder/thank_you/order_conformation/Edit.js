const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../../helper/CssGenerator';
import {
	GeneralAdvanced,
	CustomCssAdvanced,
	CommonSettings,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../../helper/CommonPanel';
import { Sections, Section } from '../../../helper/Sections';
import { AddSettingsToToolbar } from './Settings';
import useBlockId from '../../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: {
			blockId,
			advanceId,
			messageText,
			orderHeadText,
			showHead,
			showMessage,
		},
	} = props;

	const store = { setAttributes, name, attributes, clientId };

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator(
			attributes,
			'product-blocks/thankyou-order-conformation',
			blockId
		);
	}

	return (
		<>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Settings', 'product-blocks' ) }
					>
						<CommonSettings
							depend="showHead"
							initialOpen={ true }
							title="Heading"
							store={ store }
							include={ [
								{
									data: {
										type: 'text',
										key: 'orderHeadText',
										label: __(
											'Heading Text',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'orderHeadColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'orderHeadTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'alignment',
										key: 'orderHeadAlign',
										label: __(
											'Alignment',
											'product-blocks'
										),
										disableJustify: true,
									},
								},
								{
									data: {
										type: 'range',
										key: 'orderHeadSpace',
										label: __( 'Space', 'product-blocks' ),
										min: 0,
										max: 50,
										step: 1,
									},
								},
							] }
						/>
						<CommonSettings
							depend="showMessage"
							initialOpen={ false }
							title={ __( 'Order Message', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'text',
										key: 'messageText',
										label: __(
											'Message',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'messageColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'messageTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'alignment',
										key: 'orderMessageAlign',
										label: __(
											'Alignment',
											'product-blocks'
										),
										disableJustify: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ false }
							title={ __( 'Container', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color2',
										key: 'containerBg',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'containerBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'containerRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'containerPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced initialOpen={ true } store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>
			<AddSettingsToToolbar store={ store } />
			<div
				id={ advanceId ? advanceId : '' }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ 'wopb-product-wrapper ' }>
					<div className="wopb-thankyou-order-conformation-container">
						{ showHead && (
							<div className="wopb-order-heading-section">
								<div className="wopb-order-heading">
									{ orderHeadText }2917
								</div>
							</div>
						) }
						{ showMessage && (
							<div className="wopb-order-message-section">
								<div className="wopb-order-message">
									{ ' ' }
									{ messageText }
								</div>
							</div>
						) }
					</div>
				</div>
			</div>
		</>
	);
}
