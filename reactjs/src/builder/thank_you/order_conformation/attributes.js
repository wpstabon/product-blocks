const attributes = {
	blockId: { type: 'string', default: '' },
	currentPostId: { type: 'string', default: '' },
	showHead: { type: 'boolean', default: true },
	showMessage: { type: 'boolean', default: true },
	orderHeadText: { type: 'string', default: 'Order#' },
	orderHeadColor: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-conformation-container .wopb-order-heading-section .wopb-order-heading { color:{{orderHeadColor}}; }',
			},
		],
	},
	orderHeadTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '24', unit: 'px' },
			height: { lg: '32', unit: 'px' },
			decoration: 'none',
			transform: '',
			weight: '600',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-conformation-container .wopb-order-heading-section .wopb-order-heading ',
			},
		],
	},
	orderHeadAlign: {
		type: 'string',
		default: 'left',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-conformation-container .wopb-order-heading-section { text-align:{{orderHeadAlign}}} ',
			},
		],
	},
	orderHeadSpace: {
		type: 'string',
		default: '12',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-conformation-container .wopb-order-heading-section { margin-bottom:{{orderHeadSpace}}px;}',
			},
		],
	},
	messageText: {
		type: 'string',
		default: 'Thank you. Your order has been received.',
	},
	messageColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-conformation-container .wopb-order-message-section .wopb-order-message , {{WOPB}} .wopb-thankyou-order-conformation-container .woocommerce-thankyou-order-failed { color:{{messageColor}}; }',
			},
		],
	},
	messageTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '26', unit: 'px' },
			decoration: 'none',
			transform: '',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-conformation-container .wopb-order-message-section .wopb-order-message ',
			},
		],
	},
	orderMessageAlign: {
		type: 'string',
		default: 'left',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-thankyou-order-conformation-container .wopb-order-message-section { text-align:{{orderMessageAlign}}} ',
			},
		],
	},
	containerBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-conformation-container ',
			},
		],
	},
	containerBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#cccccc',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-conformation-container ',
			},
		],
	},
	containerRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-conformation-container { border-radius:{{containerRadius}}; }',
			},
		],
	},
	containerPadding: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-thankyou-order-conformation-container { padding:{{containerPadding}} !important; }',
			},
		],
	},
	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	wrapMargin: {
		type: 'object',
		default: { lg: { top: '', bottom: '20', unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#f5f5f5' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#ff176b' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapInnerPadding: {
		type: 'object',
		default: { lg: { unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ padding:{{wrapInnerPadding}}; }',
			},
		],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: { type: 'string', default: '', style: [ { selector: '' } ] },
};
export default attributes;
