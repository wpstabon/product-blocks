const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';

registerBlockType( 'product-blocks/thankyou-order-conformation', {
	title: __( 'Order Confirmation', 'product-blocks' ),
	icon: (
		<img
			src={
				wopb_data.url +
				'assets/img/blocks/builder/thank_you/order_confirmation.svg'
			}
			alt={ __( 'Image', 'product-blocks' ) }
			style={ { width: '100%' } }
		/>
	),
	category: 'thank-you',
	description: __(
		'Display a confirmation note once the order has been successfully placed.',
		'product-blocks'
	),
	keywords: [
		__( 'Order', 'product-blocks' ),
		__( 'Confirmation', 'product-blocks' ),
		__( 'thankyou', 'product-blocks' ),
	],
	edit: Edit,
	save() {
		return null;
	},
	attributes,
} );
