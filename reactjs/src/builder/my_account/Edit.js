const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import {
	GeneralAdvanced,
	CustomCssAdvanced,
	CommonSettings,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Sections, Section } from '../../helper/Sections';
import useBlockId from '../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: { blockId, advanceId, showProfile, pageLayout },
	} = props;
	const store = { setAttributes, name, attributes, clientId };

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/my-account', blockId );
	}

	return (
		<>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Settings', 'product-blocks' ) }
					>
						<CommonSettings
							store={ store }
							initialOpen={ true }
							title={ 'inline' }
							include={ [
								{
									data: {
										type: 'tag',
										key: 'pageLayout',
										label: __( 'Layout', 'product-blocks' ),
										options: [
											{
												value: 'vertical',
												label: __(
													'Vertical',
													'product-blocks'
												),
											},
											{
												value: 'horizontal',
												label: __(
													'Horizontal',
													'product-blocks'
												),
											},
										],
									},
								},
							] }
						/>
						<CommonSettings
							store={ store }
							initialOpen={ false }
							title={ 'Profile' }
							depend="showProfile"
							include={ [
								{
									data: {
										type: 'alignment',
										key: 'profileAlign',
										label: __(
											'Alignment',
											'product-blocks'
										),
										disableJustify: true,
									},
								},
								{
									data: {
										type: 'typography',
										key: 'profileTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'profileColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'range',
										key: 'profileImageWidth',
										min: 0,
										max: 280,
										step: 1,
										responsive: true,
										unit: true,
										label: __(
											'Image Width',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'profileImageHeight',
										min: 0,
										max: 280,
										step: 1,
										responsive: true,
										unit: true,
										label: __(
											'Image Height',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'profileImageRadius',
										label: __(
											'Image Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'range',
										key: 'profileSpacing',
										min: 0,
										max: 80,
										step: 1,
										responsive: true,
										unit: true,
										label: __(
											'Spacing',
											'product-blocks'
										),
									},
								},
							] }
						/>
						<CommonSettings
							store={ store }
							initialOpen={ false }
							title={ __( 'Navigation Tab', 'product-blocks' ) }
							include={ [
								{
									data: {
										type: 'typography',
										key: 'navTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								pageLayout == 'horizontal' && {
									data: {
										type: 'alignment',
										key: 'navAlign',
										label: __(
											'Alignment',
											'product-blocks'
										),
										disableJustify: true,
									},
								},
								{
									data: {
										type: 'border',
										key: 'navParentBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'range',
										key: 'navWidth',
										min: 0,
										max: 1400,
										step: 1,
										responsive: true,
										unit: true,
										label: __( 'Width', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'range',
										key: 'navSpacing',
										min: 0,
										max: 80,
										step: 1,
										responsive: true,
										unit: true,
										label: __(
											'Spacing',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'navListSpacing',
										min: 0,
										max: 100,
										step: 1,
										responsive: true,
										unit: true,
										label: __(
											'List Spacing( Horizontal )',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'navListVerticalSpacing',
										min: 0,
										max: 100,
										step: 1,
										responsive: true,
										unit: true,
										label: __(
											'List Spacing( Vertical )',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'tab',
										content: [
											{
												name: 'normal',
												title: 'Normal',
												options: [
													{
														type: 'color',
														key: 'navColor',
														label: __(
															'Color',
															'product-blocks'
														),
													},
													{
														type: 'color2',
														key: 'navBgColor',
														label: __(
															'Background',
															'product-blocks'
														),
													},
													{
														type: 'border',
														key: 'navBorder',
														label: __(
															'Border',
															'product-blocks'
														),
													},
													{
														type: 'border',
														key: 'navHoriBorder',
														label: __(
															'Border(Horizontal)',
															'product-blocks'
														),
													},
													{
														type: 'dimension',
														key: 'navRadius',
														label: __(
															'Border Radius',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
													{
														type: 'dimension',
														key: 'navPadding',
														label: __(
															'Padding',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
												],
											},
											{
												name: 'focus',
												title: 'Active',
												options: [
													{
														type: 'color',
														key: 'navFocusColor',
														label: __(
															'Color',
															'product-blocks'
														),
													},
													{
														type: 'color2',
														key: 'navBgFocusColor',
														label: __(
															'Background',
															'product-blocks'
														),
													},
													{
														type: 'border',
														key: 'navFocusBorder',
														label: __(
															'Border',
															'product-blocks'
														),
													},
												],
											},
										],
									},
								},
							] }
						/>
						<CommonSettings
							store={ store }
							initialOpen={ false }
							title={ __( 'Tab Content', 'product-blocks' ) }
							include={ [
								{
									data: {
										type: 'typography',
										key: 'tabContentTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'tabContentColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color2',
										key: 'tabContentBgColor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'tabContentLinkColor',
										label: __(
											'Link Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'tabContentHeadingTypo',
										label: __(
											'Heading Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'tabContentHeadingColor',
										label: __(
											'Heading Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'tabContentBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'boxshadow',
										key: 'tabContentShadow',
										label: __(
											'Box Shadow',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'tabContentRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'tabContentPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							store={ store }
							initialOpen={ false }
							title={ __( 'Table/Other', 'product-blocks' ) }
							include={ [
								{
									data: {
										type: 'alignment',
										key: 'tableAlignment',
										label: __(
											'Alignment',
											'product-blocks'
										),
										disableJustify: true,
									},
								},
								{
									data: {
										type: 'border',
										key: 'tableBorder',
										label: __(
											'Table Border',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'tableRadius',
										label: __(
											'Table Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'divider',
										key: 'tableHeadDivider',
										label: __(
											'Heading',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'tableHeadTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'tableHeadColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color2',
										key: 'tableHeadBgColor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'tableHeadBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'tableHeadPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},

								{
									data: {
										type: 'divider',
										key: 'tableBodyDivider',
										label: __( 'Body', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'tableBodyTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'tableBodyColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color2',
										key: 'tableBodyBgColor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'tableBodyBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'tableLinkColor',
										label: __(
											'Link Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'tableLinkHoverColor',
										label: __(
											'Link Hover Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'tableBodyPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'divider',
										key: 'tableBtnDivider',
										label: __( 'Button', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'tab',
										content: [
											{
												name: 'normal',
												title: 'Normal',
												options: [
													{
														type: 'color',
														key: 'tableBtnColor',
														label: __(
															'Color',
															'product-blocks'
														),
													},
													{
														type: 'color2',
														key: 'tableBtnBgColor',
														label: __(
															'Background',
															'product-blocks'
														),
													},
													{
														type: 'typography',
														key: 'tableBtnTypo',
														label: __(
															'Typography',
															'product-blocks'
														),
													},
													{
														type: 'border',
														key: 'tableBtnBorder',
														label: __(
															'Border',
															'product-blocks'
														),
													},
													{
														type: 'dimension',
														key: 'tableBtnRadius',
														label: __(
															'Border Radius',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
													{
														type: 'dimension',
														key: 'tableBtnPadding',
														label: __(
															'Padding',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
												],
											},
											{
												name: 'hover',
												title: 'Hover',
												options: [
													{
														type: 'color',
														key: 'tableBtnHoverColor',
														label: __(
															'Color',
															'product-blocks'
														),
													},
													{
														type: 'color2',
														key: 'tableBtnBgHoverColor',
														label: __(
															'Background',
															'product-blocks'
														),
													},
												],
											},
										],
									},
								},
								{
									data: {
										type: 'divider',
										key: 'tableFooterDivider',
										label: __(
											'Table Footer',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color2',
										key: 'tableFooterBgColor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'tableFooterBorder',
										label: __(
											'Body Border',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'tableFooterRadius',
										label: __(
											'Body Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'tableFooterPadding',
										label: __(
											'Body Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'typography',
										key: 'tableFooterBtnTypo',
										label: __(
											'Button Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'tableFooterBtnColor',
										label: __(
											'Button Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color2',
										key: 'tableFooterBtnBgColor',
										label: __(
											'Button Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'tableFooterBtnBorder',
										label: __(
											'Button Border',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'tableFooterBtnRadius',
										label: __(
											'Button Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'tableFooterBtnPadding',
										label: __(
											'Button Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							store={ store }
							initialOpen={ false }
							title={ __( 'Form', 'product-blocks' ) }
							include={ [
								{
									data: {
										type: 'range',
										key: 'formRowGap',
										min: 0,
										max: 80,
										step: 1,
										responsive: true,
										unit: true,
										label: __(
											'Rows Gap',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'formLabelTypo',
										label: __(
											'Label Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'formLabelColor',
										label: __(
											'Label Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'formRequiredColor',
										label: __(
											'Required Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'divider',
										key: 'formInputDivider',
										label: __( 'Input', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'tab',
										content: [
											{
												name: 'normal',
												title: 'Normal',
												options: [
													{
														type: 'color',
														key: 'formInputColor',
														label: __(
															'Color',
															'product-blocks'
														),
													},
													{
														type: 'color2',
														key: 'formInputBgColor',
														label: __(
															'Background',
															'product-blocks'
														),
													},
													{
														type: 'border',
														key: 'formInputBorder',
														label: __(
															'Border',
															'product-blocks'
														),
													},
													{
														type: 'dimension',
														key: 'formInputRadius',
														label: __(
															'Border Radius',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
													{
														type: 'dimension',
														key: 'formInputPadding',
														label: __(
															'Padding',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
												],
											},
											{
												name: 'focus',
												title: 'Focus',
												options: [
													{
														type: 'color',
														key: 'formInputFocusColor',
														label: __(
															'Color',
															'product-blocks'
														),
													},
													{
														type: 'border',
														key: 'formInputFocusBorder',
														label: __(
															'Border',
															'product-blocks'
														),
													},
												],
											},
										],
									},
								},
								{
									data: {
										type: 'divider',
										key: 'formBtnDivider',
										label: __( 'Button', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'formBtnTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'tab',
										content: [
											{
												name: 'normal',
												title: 'Normal',
												options: [
													{
														type: 'color',
														key: 'formBtnColor',
														label: __(
															'Color',
															'product-blocks'
														),
													},
													{
														type: 'color2',
														key: 'formBtnBgColor',
														label: __(
															'Background',
															'product-blocks'
														),
													},
													{
														type: 'border',
														key: 'formBtnBorder',
														label: __(
															'Border',
															'product-blocks'
														),
													},
													{
														type: 'dimension',
														key: 'formBtnRadius',
														label: __(
															'Border Radius',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
													{
														type: 'dimension',
														key: 'formBtnPadding',
														label: __(
															'Padding',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
												],
											},
											{
												name: 'hover',
												title: 'Hover',
												options: [
													{
														type: 'color',
														key: 'formBtnHoverColor',
														label: __(
															'Color',
															'product-blocks'
														),
													},
													{
														type: 'color2',
														key: 'formBtnBgHoverColor',
														label: __(
															'Background',
															'product-blocks'
														),
													},
													{
														type: 'border',
														key: 'formBtnHoverBorder',
														label: __(
															'Border',
															'product-blocks'
														),
													},
												],
											},
										],
									},
								},
							] }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced initialOpen={ true } store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>
			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ 'wopb-product-wrapper' }>
					{ /* Profile  */ }
					{ showProfile && (
						<div className="wopb-my-account-profile-section">
							<div className="wopb-my-account-user-img">
								<img
									src={
										wopb_data.url +
										'assets/img/wopb-author.jpg'
									}
								/>
							</div>
							<div className="wopb-my-account-user-data">
								<span className="wopb-user-name">John Doe</span>
								<span className="wopb-user-email">
									johndoe@gmail.com
								</span>
							</div>
						</div>
					) }
					<div className="wopb-my-account-container wopb-my-account-container-editor">
						{ /* column 1  */ }
						<nav className="woocommerce-MyAccount-navigation">
							<ul>
								<li className="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard is-active">
									<a id="wopb-ma-dashboard" href="#">
										Dashboard
									</a>
								</li>
								<li className="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders">
									<a id="wopb-ma-orders" href="#">
										Orders
									</a>
								</li>
								<li className="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--downloads">
									<a id="wopb-ma-downloads" href="#">
										Downloads
									</a>
								</li>
								<li className="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-address">
									<a id="wopb-ma-address" href="#">
										Addresses
									</a>
								</li>
								<li className="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-account">
									<a id="wopb-ma-account" href="#">
										Account details
									</a>
								</li>
								<li className="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout">
									<a id="wopb-ma-login" href="#">
										Logout
									</a>
								</li>
							</ul>
						</nav>

						{ /* column 2 */ }
						<div className="woocommerce-MyAccount-content">
							{ /* Dashboard  */ }
							<div
								className="wopb-dashboard wopb-myaccount-item wopb-my-account-container-show"
								id="wopb-ma-dashboard-item"
							>
								<p>
									{ ' ' }
									Hello <strong>Jon Doe</strong> (not{ ' ' }
									<strong>Jon Doe</strong>?{ ' ' }
									<a href="#">Log out</a>){ ' ' }
								</p>
								<p>
									{ ' ' }
									From your account dashboard you can view
									your <a href="#">recent orders</a>, manage
									your{ ' ' }
									<a href="#">
										shipping and billing addresses
									</a>
									, and{ ' ' }
									<a href="#">
										edit your password and account details
									</a>
									.{ ' ' }
								</p>
							</div>

							{ /* Order  */ }
							<div
								className="wopb-my-account-orders wopb-myaccount-item "
								id="wopb-ma-orders-item"
							>
								<table className="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table ">
									<thead>
										<tr>
											<th className="woocommerce-orders-table__header woocommerce-orders-table__header-order-number">
												<span className="nobr">
													Order
												</span>
											</th>
											<th className="woocommerce-orders-table__header woocommerce-orders-table__header-order-date">
												<span className="nobr">
													Date
												</span>
											</th>
											<th className="woocommerce-orders-table__header woocommerce-orders-table__header-order-status">
												<span className="nobr">
													Status
												</span>
											</th>
											<th className="woocommerce-orders-table__header woocommerce-orders-table__header-order-total">
												<span className="nobr">
													Total
												</span>
											</th>
											<th className="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions">
												<span className="nobr">
													Actions
												</span>
											</th>
										</tr>
									</thead>
									<tbody>
										<tr className="woocommerce-orders-table__row woocommerce-orders-table__row--status-on-hold order">
											<td
												className="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number"
												data-title="Order"
											>
												<a href="#"> #2229 </a>
											</td>
											<td
												className="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date"
												data-title="Date"
											>
												<time dateTime="2022-08-29T05:18:29+00:00">
													August 29, 2022
												</time>
											</td>
											<td
												className="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status"
												data-title="Status"
											>
												{ ' ' }
												On hold{ ' ' }
											</td>
											<td
												className="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-total"
												data-title="Total"
											>
												<span className="woocommerce-Price-amount amount">
													88.00{ ' ' }
													<span className="woocommerce-Price-currencySymbol">
														$
													</span>
												</span>{ ' ' }
												for 2 items
											</td>
											<td
												className="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions"
												data-title="Actions"
											>
												<a
													href="#"
													className="woocommerce-button button view"
												>
													View
												</a>
											</td>
										</tr>
										<tr className="woocommerce-orders-table__row woocommerce-orders-table__row--status-on-hold order">
											<td
												className="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number"
												data-title="Order"
											>
												<a href="#"> #2203 </a>
											</td>
											<td
												className="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date"
												data-title="Date"
											>
												<time dateTime="2022-08-25T07:24:43+00:00">
													August 25, 2022
												</time>
											</td>
											<td
												className="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status"
												data-title="Status"
											>
												{ ' ' }
												On hold{ ' ' }
											</td>
											<td
												className="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-total"
												data-title="Total"
											>
												<span className="woocommerce-Price-amount amount">
													68.00{ ' ' }
													<span className="woocommerce-Price-currencySymbol">
														$
													</span>
												</span>{ ' ' }
												for 1 item
											</td>
											<td
												className="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions"
												data-title="Actions"
											>
												<a
													href="#"
													className="woocommerce-button button view"
												>
													View
												</a>
											</td>
										</tr>
										<tr className="woocommerce-orders-table__row woocommerce-orders-table__row--status-on-hold order">
											<td
												className="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number"
												data-title="Order"
											>
												<a href="#"> #2198 </a>
											</td>
											<td
												className="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date"
												data-title="Date"
											>
												<time dateTime="2022-08-25T05:34:31+00:00">
													August 25, 2022
												</time>
											</td>
											<td
												className="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status"
												data-title="Status"
											>
												{ ' ' }
												On hold{ ' ' }
											</td>
											<td
												className="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-total"
												data-title="Total"
											>
												<span className="woocommerce-Price-amount amount">
													68.00{ ' ' }
													<span className="woocommerce-Price-currencySymbol">
														$
													</span>
												</span>{ ' ' }
												for 1 item
											</td>
											<td
												className="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions"
												data-title="Actions"
											>
												<a
													href="#"
													className="woocommerce-button button  view"
												>
													View
												</a>
											</td>
										</tr>
										<tr className="woocommerce-orders-table__row woocommerce-orders-table__row--status-on-hold order">
											<td
												className="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number"
												data-title="Order"
											>
												<a href="#"> #2195 </a>
											</td>
											<td
												className="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date"
												data-title="Date"
											>
												<time dateTime="2022-08-25T05:16:01+00:00">
													August 25, 2022
												</time>
											</td>
											<td
												className="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-status"
												data-title="Status"
											>
												{ ' ' }
												On hold{ ' ' }
											</td>
											<td
												className="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-total"
												data-title="Total"
											>
												<span className="woocommerce-Price-amount amount">
													68.00{ ' ' }
													<span className="woocommerce-Price-currencySymbol">
														$
													</span>
												</span>{ ' ' }
												for 1 item
											</td>
											<td
												className="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions"
												data-title="Actions"
											>
												<a
													href="#"
													className="woocommerce-button button view"
												>
													View
												</a>
											</td>
										</tr>
									</tbody>
								</table>
								<div className="woocommerce-pagination woocommerce-pagination--without-numbers woocommerce-Pagination ">
									<a
										className="woocommerce-button woocommerce-button--next woocommerce-Button woocommerce-Button--next button"
										href="#"
									>
										Next
									</a>
								</div>
							</div>

							{ /* Downloads  */ }
							<section
								className="woocommerce-order-downloads wopb-myaccount-item"
								id="wopb-ma-downloads-item"
							>
								<table className="woocommerce-table woocommerce-table--order-downloads shop_table shop_table_responsive order_details">
									<thead>
										<tr>
											<th className="download-product">
												<span className="nobr">
													Product
												</span>
											</th>
											<th className="download-remaining">
												<span className="nobr">
													Downloads remaining
												</span>
											</th>
											<th className="download-expires">
												<span className="nobr">
													Expires
												</span>
											</th>
											<th className="download-file">
												<span className="nobr">
													Download
												</span>
											</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td
												className="download-product"
												data-title="Product"
											>
												<a href="#">Album</a>
											</td>
											<td
												className="download-remaining"
												data-title="Downloads remaining"
											>
												{ ' ' }
												2{ ' ' }
											</td>
											<td
												className="download-expires"
												data-title="Expires"
											>
												<time
													dateTime="2022-09-15"
													title="1663200000"
												>
													September 15, 2022
												</time>
											</td>
											<td
												className="download-file"
												data-title="Download"
											>
												<a
													href="#"
													className="woocommerce-MyAccount-downloads-file button alt"
												>
													Single 1
												</a>
											</td>
										</tr>
										<tr>
											<td
												className="download-product"
												data-title="Product"
											>
												<a href="#">Album</a>
											</td>
											<td
												className="download-remaining"
												data-title="Downloads remaining"
											>
												{ ' ' }
												2{ ' ' }
											</td>
											<td
												className="download-expires"
												data-title="Expires"
											>
												<time
													dateTime="2022-09-15"
													title="1663200000"
												>
													September 15, 2022
												</time>
											</td>
											<td
												className="download-file"
												data-title="Download"
											>
												<a
													href="#"
													className="woocommerce-MyAccount-downloads-file button alt"
												>
													Single 2
												</a>
											</td>
										</tr>
									</tbody>
								</table>
							</section>

							{ /* Addresses  */ }
							<div
								className="wopb-woocommerce-Addresses wopb-myaccount-item"
								id="wopb-ma-address-item"
							>
								<div className="u-columns woocommerce-Addresses col2-set addresses ">
									<div className="u-column1 col-1 woocommerce-Address">
										<header className="woocommerce-Address-title title">
											<h3>Billing address</h3>
											<a href="#" className="edit">
												Edit
											</a>
										</header>
										<address>
											Clarabelle Dickinson <br></br>
											Marquardt PLC<br></br>68286 Bergnaum
											Mission Apt. 993 <br></br>South
											Dakota, 75660<br></br>Terencefort
											<br></br>00288-1651<br></br>{ ' ' }
											dummy@shop.com
										</address>
										<br></br>
										<br></br>
									</div>
									<div className="u-column2 col-2 woocommerce-Address">
										<header className="woocommerce-Address-title title">
											<h3>Shipping address</h3>
											<a href="#" className="edit">
												Edit
											</a>
										</header>
										<address>
											Clarabelle Dickinson <br></br>
											Marquardt PLC<br></br>68286 Bergnaum
											Mission Apt. 993 <br></br>South
											Dakota, 75660<br></br>Terencefort
											<br></br>00288-1651<br></br>{ ' ' }
											dummy@shop.com
										</address>
									</div>
								</div>
							</div>

							{ /* Account Details Form  */ }
							<form
								className="woocommerce-EditAccountForm edit-account wopb-myaccount-item"
								id="wopb-ma-account-item"
								action=""
								method="post"
							>
								<p className="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
									<label htmlFor="account_first_name">
										First name&nbsp;{ ' ' }
										<span className="required">*</span>
									</label>
									<input
										type="text"
										className="woocommerce-Input woocommerce-Input--text input-text"
										name="account_first_name"
										id="account_first_name"
									/>
								</p>
								<p className="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
									<label htmlFor="account_last_name">
										Last name&nbsp;{ ' ' }
										<span className="required">*</span>
									</label>
									<input
										type="text"
										className="woocommerce-Input woocommerce-Input--text input-text"
										name="account_last_name"
										id="account_last_name"
									/>
								</p>
								<div className="clear"></div>
								<p className="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
									<label htmlFor="account_display_name">
										Display name&nbsp;{ ' ' }
										<span className="required">*</span>
									</label>
									<input
										type="text"
										className="woocommerce-Input woocommerce-Input--text input-text"
										name="account_display_name"
										id="account_display_name"
									/>
									<span>
										<em>
											This will be how your name will be
											displayed in the account section and
											in reviews
										</em>
									</span>
								</p>
								<div className="clear"></div>
								<p className="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
									<label htmlFor="account_email">
										Email address&nbsp;{ ' ' }
										<span className="required">*</span>
									</label>
									<input
										type="email"
										className="woocommerce-Input woocommerce-Input--email input-text"
										name="account_email"
										id="account_email"
										autoComplete="email"
									/>
								</p>
								<fieldset>
									<legend>Password change</legend>
									<p className="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
										<label htmlFor="password_current">
											Current password (leave blank to
											leave unchanged)
										</label>
										<span className="password-input">
											<input
												type="password"
												className="woocommerce-Input woocommerce-Input--password input-text"
												name="password_current"
												id="password_current"
												autoComplete="off"
											/>
											<span className="show-password-input"></span>
										</span>
									</p>
									<p className="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
										<label htmlFor="password_1">
											New password (leave blank to leave
											unchanged)
										</label>
										<span className="password-input">
											<input
												type="password"
												className="woocommerce-Input woocommerce-Input--password input-text"
												name="password_1"
												id="password_1"
												autoComplete="off"
											/>
											<span className="show-password-input"></span>
										</span>
									</p>
									<p className="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
										<label htmlFor="password_2">
											Confirm new password
										</label>
										<span className="password-input">
											<input
												type="password"
												className="woocommerce-Input woocommerce-Input--password input-text"
												name="password_2"
												id="password_2"
												autoComplete="off"
											/>
											<span className="show-password-input"></span>
										</span>
									</p>
								</fieldset>
								<div className="clear"></div>
								<p>
									<input
										type="hidden"
										id="save-account-details-nonce"
										name="save-account-details-nonce"
									/>
									<input
										type="hidden"
										name="_wp_http_referer"
									/>
									<button
										type="submit"
										className="woocommerce-Button button"
										name="save_account_details"
									>
										Save changes
									</button>
									<input type="hidden" name="action" />
								</p>
							</form>
							<div
								className=" wopb-myaccount-item"
								id="wopb-ma-login-item"
							>
								<h2>Login</h2>
								<form
									className="woocommerce-form woocommerce-form-login login"
									method="post"
								>
									<p className="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
										<label htmlFor="username">
											Username or email address&nbsp;
											<span className="required">*</span>
										</label>
										<input
											type="text"
											className="woocommerce-Input woocommerce-Input--text input-text"
											name="username"
											id="username"
											autoComplete="username"
											value=""
										/>
									</p>
									<p className="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
										<label htmlFor="password">
											Password&nbsp;
											<span className="required">*</span>
										</label>
										<input
											className="woocommerce-Input woocommerce-Input--text input-text"
											type="password"
											name="password"
											id="password"
											autoComplete="current-password"
										/>
									</p>
									<p className="form-row">
										<label className="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">
											<input
												className="woocommerce-form__input woocommerce-form__input-checkbox"
												name="rememberme"
												type="checkbox"
												id="rememberme"
												value="forever"
											/>{ ' ' }
											<span>Remember me</span>
										</label>
										<input
											type="hidden"
											id="woocommerce-login-nonce"
											name="woocommerce-login-nonce"
											value="c095b32ea0"
										/>
										<input
											type="hidden"
											name="_wp_http_referer"
											value="/wp_st/my-account/"
										/>
										<button
											type="submit"
											className="woocommerce-button button woocommerce-form-login__submit"
											name="login"
											value="Log in"
										>
											Log in
										</button>
									</p>
									<p className="woocommerce-LostPassword lost_password">
										<a href="#">Lost your password?</a>
									</p>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
}
