const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';

registerBlockType( 'product-blocks/my-account', {
	title: __( 'My Account', 'product-blocks' ),
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/builder/my_account.svg' }
			alt={ __( 'Image', 'product-blocks' ) }
			style={ { width: '100%' } }
		/>
	),
	category: 'my-account',
	description: __(
		'Display the account details of users including orders, address, downloadable products, etc.',
		'product-blocks'
	),
	keywords: [
		__( 'my account', 'product-blocks' ),
		__( 'account', 'product-blocks' ),
	],
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
