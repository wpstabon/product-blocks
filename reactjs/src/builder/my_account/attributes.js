const attributes = {
	blockId: { type: 'string', default: '' },
	currentPostId: { type: 'string', default: '' },
	pageLayout: {
		type: 'string',
		default: 'vertical',
		style: [
			{
				depends: [
					{ key: 'pageLayout', condition: '==', value: 'vertical' },
				],
				selector:
					'{{WOPB}} .wopb-my-account-container { flex-direction:row; }',
			},
			{
				depends: [
					{ key: 'pageLayout', condition: '==', value: 'horizontal' },
				],
				selector:
					'{{WOPB}} .wopb-my-account-container { flex-direction:column; } {{WOPB}} .wopb-my-account-container nav.woocommerce-MyAccount-navigation { width:100% } {{WOPB}} .wopb-my-account-container nav.woocommerce-MyAccount-navigation ul {display:flex; flex-direction:row; width:100%; flex-wrap: wrap; row-gap: 10px;}',
			},
		],
	},
	showProfile: { type: 'boolean', default: true },
	profileAlign: {
		type: 'string',
		default: 'left',
		style: [
			{
				depends: [
					{ key: 'profileAlign', condition: '==', value: 'left' },
				],
				selector:
					'{{WOPB}} .wopb-my-account-profile-section { justify-content:flex-start; }',
			},
			{
				depends: [
					{ key: 'profileAlign', condition: '==', value: 'center' },
				],
				selector:
					'{{WOPB}} .wopb-my-account-profile-section { justify-content:center; }',
			},
			{
				depends: [
					{ key: 'profileAlign', condition: '==', value: 'right' },
				],
				selector:
					'{{WOPB}} .wopb-my-account-profile-section { justify-content:flex-end; }',
			},
		],
	},
	profileTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '24', unit: 'px' },
			height: { lg: '32', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '600',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-profile-section .wopb-my-account-user-data .wopb-user-name ',
			},
		],
	},
	profileColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-my-account-profile-section { color:{{profileColor}}; }',
			},
		],
	},
	profileImageWidth: {
		type: 'object',
		default: { lg: '80', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-my-account-profile-section .wopb-my-account-user-img { width:{{profileImageWidth}}; }',
			},
		],
	},
	profileImageHeight: {
		type: 'object',
		default: { lg: '80', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-my-account-profile-section .wopb-my-account-user-img img { height:{{profileImageHeight}}; }',
			},
		],
	},
	profileImageRadius: {
		type: 'object',
		default: {
			lg: {
				top: '80',
				bottom: '80',
				left: '80',
				right: '80',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-my-account-profile-section .wopb-my-account-user-img img { border-radius:{{profileImageRadius}}; }',
			},
		],
	},
	profileSpacing: {
		type: 'object',
		default: { lg: '50', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-my-account-profile-section { margin-bottom:{{profileSpacing}}; }',
			},
		],
	},
	navTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '500',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-navigation li a',
			},
		],
	},
	navAlign: {
		type: 'string',
		default: 'left',
		style: [
			{
				depends: [
					{ key: 'navAlign', condition: '==', value: 'left' },
				],
				selector:
					'{{WOPB}} .wopb-my-account-container nav.woocommerce-MyAccount-navigation ul { justify-content:flex-start; }',
			},
			{
				depends: [
					{ key: 'navAlign', condition: '==', value: 'center' },
				],
				selector:
					'{{WOPB}} .wopb-my-account-container nav.woocommerce-MyAccount-navigation ul { justify-content:center; }',
			},
			{
				depends: [
					{ key: 'navAlign', condition: '==', value: 'right' },
				],
				selector:
					'{{WOPB}} .wopb-my-account-container nav.woocommerce-MyAccount-navigation ul { justify-content:flex-end; }',
			},
		],
	},
	navParentBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 0, right: 0, bottom: 0, left: 0 },
			color: '#070707',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{ key: 'pageLayout', condition: '==', value: 'vertical' },
				],
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-navigation ul',
			},
		],
	},
	navWidth: {
		type: 'object',
		default: { lg: '180', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'pageLayout', condition: '==', value: 'vertical' },
				],
				selector:
					'{{WOPB}} .wopb-my-account-container nav.woocommerce-MyAccount-navigation ul { width:{{navWidth}}; }',
			},
		],
	},
	navSpacing: {
		type: 'object',
		default: { lg: '30', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'pageLayout', condition: '==', value: 'vertical' },
				],
				selector:
					'{{WOPB}} .wopb-my-account-container nav.woocommerce-MyAccount-navigation { margin-right:{{navSpacing}}; }',
			},
			{
				depends: [
					{ key: 'pageLayout', condition: '==', value: 'horizontal' },
				],
				selector:
					'{{WOPB}} .wopb-my-account-container nav.woocommerce-MyAccount-navigation { margin-bottom:{{navSpacing}}; }',
			},
		],
	},
	navListSpacing: {
		type: 'object',
		default: { lg: '20', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'pageLayout', condition: '==', value: 'horizontal' },
				],
				selector:
					'{{WOPB}} .wopb-my-account-container nav.woocommerce-MyAccount-navigation ul li { margin-right:{{navListSpacing}}; }',
			},
		],
	},
	navListVerticalSpacing: {
		type: 'object',
		default: { lg: '8', unit: 'px' },
		style: [
			{
				depends: [
					{ key: 'pageLayout', condition: '==', value: 'vertical' },
				],
				selector:
					'{{WOPB}} .wopb-my-account-container nav.woocommerce-MyAccount-navigation ul li { margin-bottom:{{navListVerticalSpacing}}; }',
			},
		],
	},
	navColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-navigation ul li a , .woocommerce-account {{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-navigation ul li a, {{WOPB}} .wopb-my-account-container .woocommerce-info:has(.wc-forward) .wc-forward  { color:{{navColor}} !important; }',
			},
		],
	},
	navBgColor: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: 'rgba(255,255,255,0)' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-navigation ul li, {{WOPB}} .wopb-my-account-container .woocommerce-info:has(.wc-forward) .wc-forward',
			},
		],
	},
	navBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#070707',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{ key: 'pageLayout', condition: '==', value: 'vertical' },
				],
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-navigation ul li, {{WOPB}} .wopb-my-account-container .woocommerce-info:has(.wc-forward) .wc-forward',
			},
		],
	},
	navHoriBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#070707',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{ key: 'pageLayout', condition: '==', value: 'horizontal' },
				],
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-navigation ul li',
			},
		],
	},
	navRadius: {
		type: 'object',
		default: {
			lg: { top: '0', bottom: '0', left: '0', right: '0', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-navigation ul li { border-radius:{{navRadius}}; }',
			},
		],
	},
	navPadding: {
		type: 'object',
		default: {
			lg: {
				top: '14',
				bottom: '14',
				left: '20',
				right: '15',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-navigation ul li > a, {{WOPB}} .wopb-my-account-container .woocommerce-info:has(.wc-forward) .wc-forward { padding:{{navPadding}}; }',
			},
		],
	},
	navFocusColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-navigation ul li.is-active a , .woocommerce-account {{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-navigation ul li.is-active a, {{WOPB}} .wopb-my-account-container .woocommerce-info:has(.wc-forward) .wc-forward:hover { color:{{navFocusColor}} !important; }',
			},
		],
	},
	navBgFocusColor: {
		type: 'object',
		default: {
			openColor: 1,
			color: '#070707',
			type: 'color',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-navigation ul li.is-active, {{WOPB}} .wopb-my-account-container .woocommerce-info:has(.wc-forward) .wc-forward:hover',
			},
		],
	},
	navFocusBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#E6E6E6',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-navigation ul li.is-active',
			},
		],
	},
	tabContentTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '26', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content',
			},
		],
	},
	tabContentColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content { color:{{tabContentColor}}; }',
			},
		],
	},
	tabContentLinkColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content a { color:{{tabContentLinkColor}}; }',
			},
		],
	},
	tabContentHeadingTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '24', unit: 'px' },
			height: { lg: '', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '600',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container h2 , {{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content h2 , {{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content h3 , {{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content legend',
			},
		],
	},
	tabContentHeadingColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container h2 , {{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content h2 , {{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content h3 , {{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content legend { color:{{tabContentHeadingColor}}; }',
			},
		],
	},
	tabContentBgColor: {
		type: 'object',
		default: {},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content',
			},
		],
	},
	tabContentBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#e0e0e0',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content',
			},
		],
	},
	tabContentShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content',
			},
		],
	},
	tabContentRadius: {
		type: 'object',
		default: {
			lg: { top: '0', bottom: '0', left: '0', right: '0', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content {border-radius:{{tabContentRadius}};}',
			},
		],
	},
	tabContentPadding: {
		type: 'object',
		default: {
			lg: { top: '0', bottom: '0', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content { padding:{{tabContentPadding}};}',
			},
		],
	},
	formRowGap: {
		type: 'object',
		default: { lg: '20', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container form p.form-row { margin-bottom:{{formRowGap}}; }  {{WOPB}} .wopb-my-account-container form fieldset p.form-row:last-child { margin-bottom: 15px; } {{WOPB}} .wopb-my-account-container form p.form-row:last-child { margin-bottom: 15px; }',
			},
		],
	},
	formLabelTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '500',
		},
		style: [
			{ selector: '{{WOPB}} .wopb-my-account-container form p label' },
		],
	},
	formLabelColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container form p label { color:{{formLabelColor}}; }',
			},
		],
	},
	formRequiredColor: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container form .required { color:{{formRequiredColor}}; }',
			},
		],
	},
	formInputDivider: { type: 'string', default: '' },
	formInputColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container form p input { color:{{formInputColor}}; }',
			},
		],
	},
	formInputBgColor: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#fff' },
		style: [
			{ selector: '{{WOPB}} .wopb-my-account-container form p input' },
		],
	},
	formInputBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#D2D2D2',
			type: 'solid',
		},
		style: [
			{ selector: '{{WOPB}} .wopb-my-account-container form p input' },
		],
	},
	formInputRadius: {
		type: 'object',
		default: {
			lg: { top: '0', bottom: '0', left: '0', right: '0', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container form p input { border-radius:{{formInputRadius}}; }',
			},
		],
	},
	formInputPadding: {
		type: 'object',
		default: {
			lg: { top: '0', bottom: '0', left: '10', right: '10', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container form p input { padding:{{formInputPadding}}; }',
			},
		],
	},
	formInputFocusColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container form p input:focus { color:{{formInputFocusColor}}; }',
			},
		],
	},
	formInputFocusBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#e0e0e0',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container form p input:focus',
			},
		],
	},
	formBtnDivider: { type: 'string', default: '' },
	formBtnTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '500',
		},
		style: [
			{ selector: '{{WOPB}} .wopb-my-account-container form button' },
		],
	},
	formBtnColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container form button { color:{{formBtnColor}}; }',
			},
		],
	},
	formBtnBgColor: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#FF176B' },
		style: [
			{ selector: '{{WOPB}} .wopb-my-account-container form button' },
		],
	},
	formBtnBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#e0e0e0',
			type: 'solid',
		},
		style: [
			{ selector: '{{WOPB}} .wopb-my-account-container form button' },
		],
	},
	formBtnRadius: {
		type: 'object',
		default: {
			lg: { top: '0', bottom: '0', left: '0', right: '0', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container form button { border-radius:{{formBtnRadius}}; }',
			},
		],
	},
	formBtnPadding: {
		type: 'object',
		default: {
			lg: {
				top: '12',
				bottom: '12',
				left: '24',
				right: '24',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container form button { padding:{{formBtnPadding}}; }',
			},
		],
	},
	formBtnHoverColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container form button:hover { color:{{formBtnHoverColor}}; }',
			},
		],
	},
	formBtnBgHoverColor: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#070707' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container form button:hover',
			},
		],
	},
	formBtnHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#e0e0e0',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container form button:hover',
			},
		],
	},
	tableAlignment: {
		type: 'string',
		default: 'left',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container table td, {{WOPB}} .wopb-my-account-container table th { text-align:{{tableAlignment}}; }',
			},
		],
	},
	tableBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 0, right: 1, bottom: 0, left: 1 },
			color: '#E6E6E6',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table',
			},
		],
	},
	tableRadius: {
		type: 'object',
		default: {
			lg: { top: '0', bottom: '0', left: '0', right: '0', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table { border-radius:{{tableRadius}}; overflow: hidden;}',
			},
		],
	},
	tableHeadDivider: { type: 'string', default: '' },
	tableHeadTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '600',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table thead th, {{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table tfoot th , {{WOPB}} .wopb-my-account-container table+.woocommerce-pagination a.woocommerce-button--next',
			},
		],
	},
	tableHeadColor: {
		type: 'string',
		default: '#4A4A4A',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table thead th , {{WOPB}} .wopb-my-account-container table+.woocommerce-pagination a.button { color:{{tableHeadColor}}; }',
			},
		],
	},
	tableHeadBgColor: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#E6E6E6' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table thead , {{WOPB}} .wopb-my-account-container table+.woocommerce-pagination a.button',
			},
		],
	},
	tableHeadBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 0, right: 0, bottom: 0, left: 0 },
			color: '#E6E6E6',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table thead th',
			},
		],
	},
	tableHeadPadding: {
		type: 'object',
		default: {
			lg: {
				top: '14',
				bottom: '14',
				left: '32',
				right: '32',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table thead th { padding:{{tableHeadPadding}}; }',
			},
		],
	},
	tableBodyDivider: { type: 'string', default: '' },
	tableBodyTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table tbody td , {{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table tfoot td',
			},
		],
	},
	tableBodyColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table tbody td , {{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table tfoot th , {{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table tfoot td { color:{{tableBodyColor}}; }',
			},
		],
	},
	tableBodyBgColor: {
		type: 'object',
		default: {},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table tbody , {{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table tfoot',
			},
		],
	},
	tableBodyBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 0, right: 1, bottom: 1, left: 0 },
			color: '#E6E6E6',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table tbody td , {{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table tfoot th , {{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table tfoot td',
			},
		],
	},
	tableLinkColor: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table a { color:{{tableLinkColor}}; }',
			},
		],
	},
	tableLinkHoverColor: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table a:hover { color:{{tableLinkHoverColor}}; }',
			},
		],
	},
	tableBodyPadding: {
		type: 'object',
		default: {
			lg: {
				top: '14',
				bottom: '14',
				left: '32',
				right: '32',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table tbody td , {{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table tfoot th , {{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table tfoot td { padding:{{tableBodyPadding}}; }',
			},
		],
	},
	tableBtnDivider: { type: 'string', default: '' },
	tableBtnColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table a.button { color:{{tableBtnColor}} !important; }',
			},
		],
	},
	tableBtnBgColor: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#FF176B' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table a.button',
			},
		],
	},
	tableBtnTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '12', unit: 'px' },
			height: { lg: '16', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '500',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table a.button',
			},
		],
	},
	tableBtnBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#e0e0e0',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table a.button',
			},
		],
	},
	tableBtnRadius: {
		type: 'object',
		default: {
			lg: { top: '0', bottom: '0', left: '0', right: '0', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table a.button { border-radius:{{tableBtnRadius}} ; }',
			},
		],
	},
	tableBtnPadding: {
		type: 'object',
		default: {
			lg: { top: '6', bottom: '6', left: '10', right: '10', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table a.button { padding:{{tableBtnPadding}} !important; }',
			},
		],
	},
	tableBtnHoverColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table a.button:hover { color:{{tableBtnHoverColor}} !important; }',
			},
		],
	},
	tableBtnBgHoverColor: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#000' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container .woocommerce-MyAccount-content table a.button:hover',
			},
		],
	},
	tableFooterDivider: { type: 'string', default: '' },
	tableFooterBgColor: {
		type: 'object',
		default: {},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container table+.woocommerce-pagination:has(a.button)',
			},
		],
	},
	tableFooterBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#e0e0e0',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container table+.woocommerce-pagination:has(a.button)',
			},
		],
	},
	tableFooterRadius: {
		type: 'object',
		default: {
			lg: { top: '0', bottom: '0', left: '0', right: '0', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container table+.woocommerce-pagination:has(a.button) { border-radius:{{tableFooterRadius}}; }',
			},
		],
	},
	tableFooterPadding: {
		type: 'object',
		default: {
			lg: { top: '24', bottom: '', left: '0', right: '0', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container table+.woocommerce-pagination:has(a.button) { padding:{{tableFooterPadding}} !important; }',
			},
		],
	},
	tableFooterBtnTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '500',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container table+.woocommerce-pagination a.button',
			},
		],
	},
	tableFooterBtnColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container table+.woocommerce-pagination a.button { color:{{tableFooterBtnColor}} !important; }',
			},
		],
	},
	tableFooterBtnBgColor: {
		type: 'object',
		default: { openColor: 1, type: 'color', color: '#FF176B' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container table+.woocommerce-pagination a.button',
			},
		],
	},
	tableFooterBtnBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#e0e0e0',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container table+.woocommerce-pagination a.button',
			},
		],
	},
	tableFooterBtnRadius: {
		type: 'object',
		default: {
			lg: { top: '0', bottom: '0', left: '0', right: '0', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container table+.woocommerce-pagination a.button { border-radius:{{tableFooterBtnRadius}}; }',
			},
		],
	},
	tableFooterBtnPadding: {
		type: 'object',
		default: {
			lg: {
				top: '12',
				bottom: '12',
				left: '24',
				right: '24',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-my-account-container table+.woocommerce-pagination a.button { padding:{{tableFooterBtnPadding}} !important; }',
			},
		],
	},
	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	wrapMargin: {
		type: 'object',
		default: { lg: { top: '', bottom: '20', unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#f5f5f5' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#ff176b' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapInnerPadding: {
		type: 'object',
		default: { lg: { unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ padding:{{wrapInnerPadding}}; }',
			},
		],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: { type: 'string', default: '', style: [ { selector: '' } ] },
};
export default attributes;
