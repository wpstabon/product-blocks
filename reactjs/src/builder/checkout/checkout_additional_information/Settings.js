/** @format */

import { settingsIcon } from '../../../helper/CommonPanel';
import WopbToolbarDropdown from '../../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar, getData } from '../../../helper/ux';

const { __ } = wp.i18n;

export function AddSettingsToToolbar( { store, selected } ) {
	switch ( selected ) {
		case 'title':
			return (
				<WopbToolbarGroup text={ 'Title' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Title Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Title Settings', 'product-blocks' ),
								options: getData( [
									{
										data: {
											type: 'text',
											key: 'sectionTitle',
											label: __(
												'Title Text',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'sectionTitleColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'sectionTitleBg',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'sectionTitleTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'sectionTitlePadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'range',
											key: 'sectionTitleSpace',
											label: __(
												'Space',
												'product-blocks'
											),
											min: 0,
											max: 50,
											step: 1,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'label':
			return (
				<WopbToolbarGroup text={ 'Label' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Label Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Label Settings', 'product-blocks' ),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'labelColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'labelTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'labelMargin',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'textarea-field':
			return (
				<WopbToolbarGroup text={ 'Textarea' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Textarea Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Textarea Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'tab',
											content: [
												{
													name: 'normal',
													title: 'Normal',
													options: [
														{
															type: 'color',
															key: 'inputColor',
															label: __(
																'Color',
																'product-blocks'
															),
														},
														{
															type: 'color',
															key: 'placeholderColor',
															label: __(
																'Placeholder Color',
																'product-blocks'
															),
														},
														{
															type: 'color',
															key: 'inputBgColor',
															label: __(
																'Background',
																'product-blocks'
															),
														},
														{
															type: 'typography',
															key: 'inputTypo',
															label: __(
																'Typography',
																'product-blocks'
															),
														},
														{
															type: 'border',
															key: 'inputBorder',
															label: __(
																'Border',
																'product-blocks'
															),
														},
														{
															type: 'dimension',
															key: 'inputRadius',
															label: __(
																'Border Radius',
																'product-blocks'
															),
															step: 1,
															unit: true,
															responsive: true,
														},
														{
															type: 'dimension',
															key: 'inputMargin',
															label: __(
																'Padding',
																'product-blocks'
															),
															step: 1,
															unit: true,
															responsive: true,
														},
													],
												},
												{
													name: 'focus',
													title: 'Focus',
													options: [
														{
															type: 'color',
															key: 'inputFocusColor',
															label: __(
																'Color',
																'product-blocks'
															),
														},
														{
															type: 'color',
															key: 'placeholderFocusColor',
															label: __(
																'Placeholder Color',
																'product-blocks'
															),
														},
													],
												},
											],
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'field-container':
			return (
				<WopbToolbarGroup text={ 'Field Container' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Field Container Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Field Container Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'containerBg',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'containerBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'containerRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'containerPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);

		// case "":
		//     return (
		//         <WopbToolbarGroup text={"Image"} textScroll>
		//             <WopbToolbarDropdown
		//                 buttonContent={settingsIcon}
		//                 store={store}
		//                 label={__("Product Image Settings", "product-blocks")}
		//                 content={formatSettingsForToolbar([
		//                     {
		//                         name: "tab",
		//                         title: __(
		//                             "Product Image Settings",
		//                             "product-blocks"
		//                         ),
		//                         options: getData(),
		//                     },
		//                 ])}
		//             />
		//         </WopbToolbarGroup>
		//     );
		default:
			return null;
	}
}
