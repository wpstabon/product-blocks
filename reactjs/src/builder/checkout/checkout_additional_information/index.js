const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';

registerBlockType( 'product-blocks/checkout-additional-information', {
	title: __( 'Checkout Additional Info', 'product-blocks' ),
	icon: (
		<img
			src={
				wopb_data.url +
				'assets/img/blocks/builder/checkout_additional_information.svg'
			}
			alt={ __( 'Image', 'product-blocks' ) }
			style={ { width: '100%' } }
		/>
	),
	category: 'single-checkout',
	description: __(
		'Help the shoppers to add additional information if required.',
		'product-blocks'
	),
	keywords: [
		__( 'Additional Information', 'product-blocks' ),
		__( 'Additional', 'product-blocks' ),
		__( 'Information', 'product-blocks' ),
	],
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
