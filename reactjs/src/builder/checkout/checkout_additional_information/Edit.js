/** @format */

const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../../helper/CssGenerator';
import {
	GeneralAdvanced,
	CustomCssAdvanced,
	CommonSettings,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../../helper/CommonPanel';
import { Sections, Section } from '../../../helper/Sections';
import useFluentSettings from '../../../helper/hooks/useFluentSettings';
import ToolBarElement from '../../../helper/ToolBarElement';
import { AddSettingsToToolbar } from './Settings';
import useBlockId from '../../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: { blockId, advanceId, showTitle, sectionTitle },
	} = props;

	const store = { setAttributes, name, attributes, clientId };

	const { section, toolbarSettings, setCurrSettings } = useFluentSettings();

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator(
			attributes,
			'product-blocks/checkout-additional-information',
			blockId
		);
	}

	return (
		<>
			<ToolBarElement
				store={ store }
				include={ [
					{
						type: 'block_spacing',
						label: __( 'Spacing', 'product-blocks' ),
					},
				] }
			/>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Settings', 'product-blocks' ) }
					>
						<CommonSettings
							depend="showTitle"
							initialOpen={ section.title }
							title={ __( 'Title', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'text',
										key: 'sectionTitle',
										label: __(
											'Title Text',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'sectionTitleColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'sectionTitleBg',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'sectionTitleTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'sectionTitlePadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'range',
										key: 'sectionTitleSpace',
										label: __( 'Space', 'product-blocks' ),
										min: 0,
										max: 50,
										step: 1,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section.label }
							title={ __( 'Label', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'labelColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'labelTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'labelMargin',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'textarea-field' ] }
							title={ __( 'Textarea Field', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'tab',
										content: [
											{
												name: 'normal',
												title: 'Normal',
												options: [
													{
														type: 'color',
														key: 'inputColor',
														label: __(
															'Color',
															'product-blocks'
														),
													},
													{
														type: 'color',
														key: 'placeholderColor',
														label: __(
															'Placeholder Color',
															'product-blocks'
														),
													},
													{
														type: 'color',
														key: 'inputBgColor',
														label: __(
															'Background',
															'product-blocks'
														),
													},
													{
														type: 'typography',
														key: 'inputTypo',
														label: __(
															'Typography',
															'product-blocks'
														),
													},
													{
														type: 'border',
														key: 'inputBorder',
														label: __(
															'Border',
															'product-blocks'
														),
													},
													{
														type: 'dimension',
														key: 'inputRadius',
														label: __(
															'Border Radius',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
													{
														type: 'dimension',
														key: 'inputMargin',
														label: __(
															'Padding',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
												],
											},
											{
												name: 'focus',
												title: 'Focus',
												options: [
													{
														type: 'color',
														key: 'inputFocusColor',
														label: __(
															'Color',
															'product-blocks'
														),
													},
													{
														type: 'color',
														key: 'placeholderFocusColor',
														label: __(
															'Placeholder Color',
															'product-blocks'
														),
													},
												],
											},
										],
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'field-container' ] }
							title={ __( 'Field Container', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'containerBg',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'containerBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'containerRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'containerPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced initialOpen={ true } store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar
				store={ store }
				selected={ toolbarSettings }
			/>

			<div
				id={ advanceId ? advanceId : '' }
				onClick={ ( e ) => setCurrSettings( e, 'field-container' ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ 'wopb-product-wrapper' }>
					<div className="woocommerce-additional-fields">
						{ showTitle && (
							<h2
								className="wopb-additional-info-section-title wopb-component-simple"
								onClick={ ( e ) =>
									setCurrSettings( e, 'title' )
								}
							>
								{ sectionTitle }
							</h2>
						) }
						<div className="woocommerce-additional-fields__field-wrapper">
							<p
								className="form-row notes"
								id="order_comments_field"
								data-priority=""
							>
								<label
									htmlFor="order_comments"
									className="wopb-component-simple"
									onClick={ ( e ) =>
										setCurrSettings(
											e,
											'label',
											null,
											true
										)
									}
								>
									Order notes&nbsp;
									<span className="optional">(optional)</span>
								</label>
								<span className="woocommerce-input-wrapper">
									<textarea
										name="order_comments"
										className="input-text"
										id="order_comments"
										placeholder="Notes about your order, e.g. special notes for delivery."
										rows={ 2 }
										cols={ 5 }
										onClick={ ( e ) =>
											setCurrSettings(
												e,
												'textarea-field'
											)
										}
									></textarea>
								</span>
							</p>
						</div>
					</div>
				</div>
			</div>
		</>
	);
}
