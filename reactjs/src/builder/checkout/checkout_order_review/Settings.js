/** @format */

import { settingsIcon } from '../../../helper/CommonPanel';
import WopbToolbarDropdown from '../../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar, getData } from '../../../helper/ux';

const { __ } = wp.i18n;

export function AddSettingsToToolbar( { store, selected } ) {
	switch ( selected ) {
		case 'section-title':
			return (
				<WopbToolbarGroup text={ 'Section Title' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Section Title Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Section Title Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'text',
											key: 'sectionTitle',
											label: __(
												'Text',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'sectionTitleColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'sectionTitleBg',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'alignment',
											key: 'headingAlign',
											label: __(
												'Alignment',
												'product-blocks'
											),
											disableJustify: true,
										},
									},
									{
										data: {
											type: 'typography',
											key: 'sectionTitleTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'sectionRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'sectionTitlePadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'range',
											key: 'sectionTitleSpace',
											label: __(
												'Space',
												'product-blocks'
											),
											min: 0,
											max: 50,
											step: 1,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'table-heading':
			return (
				<WopbToolbarGroup text={ 'Table Heading' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Table Heading Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Table Heading Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'headingColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'headingBg',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'headingTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'headingBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'headingPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'table-body':
			return (
				<WopbToolbarGroup text={ 'Table Body' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Table Body Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Table Body Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'bodyTextColor',
											label: __(
												'Text Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'bodyPriceColor',
											label: __(
												'Price Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'bodyBg',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'bodyTextTypo',
											label: __(
												'Text Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'bodyPriceTypo',
											label: __(
												'Price Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'bodyBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'bodyPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'total':
			return (
				<WopbToolbarGroup text={ 'Total' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Total Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Total Settings', 'product-blocks' ),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'totalColor',
											label: __(
												'Text Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'totalPriceColor',
											label: __(
												'Price Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'totalTextTypo',
											label: __(
												'Text Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'totalPriceTypo',
											label: __(
												'Price Typography',
												'product-blocks'
											),
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'field-container':
			return (
				<WopbToolbarGroup text={ 'Field Container' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Field Container Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Field Container Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'containerBg',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'containerBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'containerRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'containerPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		default:
			return null;
	}
}
