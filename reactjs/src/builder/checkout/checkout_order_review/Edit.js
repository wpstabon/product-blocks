/** @format */

const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../../helper/CssGenerator';
import {
	GeneralAdvanced,
	CustomCssAdvanced,
	CommonSettings,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../../helper/CommonPanel';
import { Sections, Section } from '../../../helper/Sections';
import useFluentSettings from '../../../helper/hooks/useFluentSettings';
import ToolBarElement from '../../../helper/ToolBarElement';
import { AddSettingsToToolbar } from './Settings';
import useBlockId from '../../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: {
			blockId,
			advanceId,
			showTitle,
			sectionTitle,
			enableImage,
		},
	} = props;

	const store = { setAttributes, name, attributes, clientId };

	const { section, setCurrSettings, toolbarSettings } = useFluentSettings();

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator(
			attributes,
			'product-blocks/checkout-order-review',
			blockId
		);
	}

	return (
		<>
			<ToolBarElement
				store={ store }
				include={ [
					{
						type: 'block_spacing',
						label: __( 'Spacing', 'product-blocks' ),
					},
				] }
			/>

			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Settings', 'product-blocks' ) }
					>
						<CommonSettings
							depend="showTitle"
							initialOpen={ section[ 'section-title' ] }
							title={ __( 'Section Title', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'text',
										key: 'sectionTitle',
										label: __( 'Text', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'sectionTitleColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'sectionTitleBg',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'alignment',
										key: 'headingAlign',
										label: __(
											'Alignment',
											'product-blocks'
										),
										disableJustify: true,
									},
								},
								{
									data: {
										type: 'typography',
										key: 'sectionTitleTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'sectionRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'sectionTitlePadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'range',
										key: 'sectionTitleSpace',
										label: __( 'Space', 'product-blocks' ),
										min: 0,
										max: 50,
										step: 1,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'table-heading' ] }
							title={ __( 'Table Heading', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'headingColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'headingBg',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'headingTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'headingBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'headingPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'table-body' ] }
							title={ __( 'Table Body', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'bodyTextColor',
										label: __(
											'Text Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'bodyPriceColor',
										label: __(
											'Price Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'bodyBg',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'bodyTextTypo',
										label: __(
											'Text Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'bodyPriceTypo',
										label: __(
											'Price Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'bodyBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'bodyPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							depend="enableImage"
							initialOpen={ section[ 'body-image' ] }
							title={ __( 'Image', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'range',
										key: 'imgWidth',
										label: __( 'Width', 'product-blocks' ),
										min: 50,
										max: 200,
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'border',
										key: 'imgBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'imgRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section.total }
							title={ __( 'Total', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'totalColor',
										label: __(
											'Text Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'totalPriceColor',
										label: __(
											'Price Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'totalTextTypo',
										label: __(
											'Text Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'totalPriceTypo',
										label: __(
											'Price Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'totalSecBorder',
										label: __(
											'Wrap Border',
											'product-blocks'
										),
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'field-container' ] }
							title={ __( 'Field Container', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'containerBg',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'containerBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'containerRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'containerPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced initialOpen={ true } store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar
				store={ store }
				selected={ toolbarSettings }
			/>

			<div
				id={ advanceId ? advanceId : '' }
				className={ `wopb-block-${ blockId } ${ className }` }
				onClick={ ( e ) => setCurrSettings( e, 'field-container' ) }
			>
				<div className={ 'wopb-product-wrapper ' }>
					<div className="wopb-checkout-review-order">
						{ showTitle && (
							<h2
								className="wopb-order-section-title wopb-component-simple"
								onClick={ ( e ) =>
									setCurrSettings( e, 'section-title' )
								}
							>
								{ sectionTitle }
							</h2>
						) }
						<div
							id="order_review"
							className="woocommerce-checkout-review-order "
						>
							<table
								className="shop_table"
								style={ { position: 'static', zoom: '1' } }
							>
								<thead
									className="wopb-component"
									onClick={ ( e ) =>
										setCurrSettings( e, 'table-heading' )
									}
								>
									<tr className="wopb-component-hover">
										<th className="product-name">
											{ __(
												'Product',
												'product-blocks'
											) }
										</th>
										<th className="product-total">
											{ __(
												'Subtotal',
												'product-blocks'
											) }
										</th>
									</tr>
								</thead>
								<tbody
									className="wopb-component-simple"
									onClick={ ( e ) =>
										setCurrSettings( e, 'table-body' )
									}
								>
									<tr className="cart_item">
										<td className="product-name wopb-component-simple">
											<div className="wopb-product-info">
												{ enableImage && (
													<img
														src={
															wopb_data.url +
															'assets/img/dummy_product_shirt.jpg'
														}
														className="wopb-component-simple"
														alt="Dummy Shirt"
														onClick={ ( e ) =>
															setCurrSettings(
																e,
																'body-image'
															)
														}
													/>
												) }
												<span>
													Beanie with Logo&nbsp;{ ' ' }
													<strong className="product-quantity">
														×&nbsp;1
													</strong>
												</span>
											</div>
										</td>
										<td className="product-total wopb-component-simple">
											<span className="woocommerce-Price-amount amount">
												<bdi>
													18.00
													<span className="woocommerce-Price-currencySymbol">
														$
													</span>
												</bdi>
											</span>
										</td>
									</tr>
									<tr className="cart_item">
										<td className="product-name wopb-component-simple">
											<div className="wopb-product-info">
												{ enableImage && (
													<img
														src={
															wopb_data.url +
															'assets/img/dummy_product_shirt.jpg'
														}
														alt="Dummy Shirt"
													/>
												) }
												<span>
													Polo&nbsp;{ ' ' }
													<strong className="product-quantity">
														×&nbsp;1
													</strong>
												</span>
											</div>
										</td>
										<td className="product-total wopb-component-simple">
											<span className="woocommerce-Price-amount amount">
												<bdi>
													40.00
													<span className="woocommerce-Price-currencySymbol">
														$
													</span>
												</bdi>
											</span>
										</td>
									</tr>
									<tr className="cart_item">
										<td className="product-name wopb-component-simple">
											<div className="wopb-product-info">
												{ enableImage && (
													<img
														src={
															wopb_data.url +
															'assets/img/dummy_product_shirt.jpg'
														}
														alt="Dummy Shirt"
													/>
												) }
												<span>
													Long Sleeve Tee&nbsp;{ ' ' }
													<strong className="product-quantity">
														×&nbsp;1
													</strong>
												</span>
											</div>
										</td>
										<td className="product-total wopb-component-simple">
											<span className="woocommerce-Price-amount amount">
												<bdi>
													50.00
													<span className="woocommerce-Price-currencySymbol">
														$
													</span>
												</bdi>
											</span>
										</td>
									</tr>
								</tbody>
								<tfoot
									onClick={ ( e ) =>
										setCurrSettings( e, 'total' )
									}
								>
									<tr className="cart-subtotal">
										<th className="wopb-component-simple">
											Subtotal
										</th>
										<td className="wopb-component-simple">
											<span className="woocommerce-Price-amount amount">
												<bdi>
													108.00
													<span className="woocommerce-Price-currencySymbol">
														$
													</span>
												</bdi>
											</span>
										</td>
									</tr>
									<tr className="woocommerce-shipping-totals shipping">
										<th className="wopb-component-simple">
											Shipping
										</th>
										<td
											data-title="Shipping"
											className="wopb-component-simple"
										>
											<ul
												id="shipping_method"
												className="woocommerce-shipping-methods"
											>
												<li>
													<input
														type="radio"
														name="shipping_method[0]"
														data-index="0"
														id="shipping_method_0_flat_rate5"
														value="flat_rate:5"
														className="shipping_method"
														checked
													/>
													<label htmlFor="shipping_method_0_flat_rate5">
														Flat rate:{ ' ' }
														<span className="woocommerce-Price-amount amount">
															<bdi>
																50.00
																<span className="woocommerce-Price-currencySymbol">
																	$
																</span>
															</bdi>
														</span>
													</label>
												</li>
												<li>
													<input
														type="radio"
														name="shipping_method[0]"
														data-index="0"
														id="shipping_method_0_local_pickup7"
														value="local_pickup:7"
														className="shipping_method"
													/>
													<label htmlFor="shipping_method_0_local_pickup7">
														Local pickup:{ ' ' }
														<span className="woocommerce-Price-amount amount">
															<bdi>
																45.00
																<span className="woocommerce-Price-currencySymbol">
																	$
																</span>
															</bdi>
														</span>
													</label>
												</li>
											</ul>
										</td>
									</tr>
									<tr className="order-total wopb-component-simple">
										<th className="wopb-component-simple">
											Total
										</th>
										<td className="wopb-component-simple">
											<strong>
												<span className="woocommerce-Price-amount amount">
													<bdi>
														158.00
														<span className="woocommerce-Price-currencySymbol">
															$
														</span>
													</bdi>
												</span>
											</strong>
										</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</>
	);
}
