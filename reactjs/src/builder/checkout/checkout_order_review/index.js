const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';
registerBlockType( 'product-blocks/checkout-order-review', {
	title: __( 'Order Review', 'product-blocks' ),
	icon: (
		<img
			src={
				wopb_data.url +
				'assets/img/blocks/builder/checkout_order_review.svg'
			}
			alt={ __( 'Image', 'product-blocks' ) }
			style={ { width: '100%' } }
		/>
	),
	category: 'single-checkout',
	description: __(
		'Display order details by adding the blocks to your desired place on the checkout page.',
		'product-blocks'
	),
	keywords: [
		__( 'Order Review', 'product-blocks' ),
		__( 'Order', 'product-blocks' ),
		__( 'Review', 'product-blocks' ),
	],
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
