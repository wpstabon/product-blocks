const attributes = {
	blockId: { type: 'string', default: '' },
	currentPostId: { type: 'string', default: '' },
	showTitle: { type: 'boolean', default: true },
	sectionTitle: { type: 'string', default: 'Your Order' },
	sectionTitleColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-order-section-title { color:{{sectionTitleColor}}; }',
			},
		],
	},
	sectionTitleBg: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-order-section-title { background-color:{{sectionTitleBg}}; }',
			},
		],
	},
	headingAlign: {
		type: 'string',
		default: 'left',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-order-section-title { text-align:{{headingAlign}}; }',
			},
		],
	},
	sectionTitleTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '24', unit: 'px' },
			height: { lg: '32', unit: 'px' },
			decoration: 'none',
			transform: '',
			weight: '600',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-order-section-title',
			},
		],
	},
	sectionRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-order-section-title { border-radius:{{sectionRadius}}; }',
			},
		],
	},
	sectionTitlePadding: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-order-section-title { padding:{{sectionTitlePadding}}; }',
			},
		],
	},
	sectionTitleSpace: {
		type: 'string',
		default: '24',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-order-section-title { margin-bottom:{{sectionTitleSpace}}px;}',
			},
		],
	},
	headingColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper table thead tr th{ color:{{headingColor}}; }',
			},
		],
	},
	headingBg: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper table thead tr th{ background-color:{{headingBg}}; }',
			},
		],
	},
	headingTypo: {
		type: 'object',
		default: {
			openTypography: 0,
			size: { lg: '16', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: '600',
		},
		style: [
			{ selector: '{{WOPB}} .wopb-product-wrapper table thead tr th' },
		],
	},
	headingBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 0, right: 0, bottom: 1, left: 0 },
			color: '#E6E6E6',
			type: 'solid',
		},
		style: [
			{ selector: '{{WOPB}} .wopb-product-wrapper table thead tr th' },
		],
	},
	headingPadding: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 16, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper table thead tr th { padding:{{headingPadding}} !important; }',
			},
		],
	},
	bodyTextColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper table tbody tr td {color:{{bodyTextColor}}; }',
			},
		],
	},
	bodyPriceColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper table tbody tr td.product-total { color:{{bodyPriceColor}}; }',
			},
		],
	},
	bodyBg: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper table tbody tr td , {{WOPB}} .wopb-product-wrapper table tfoot tr th , {{WOPB}} .wopb-product-wrapper table tfoot tr td { background-color:{{bodyBg}} !important; }',
			},
		],
	},
	bodyTextTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
		},
		style: [
			{ selector: '{{WOPB}} .wopb-product-wrapper table tbody tr td ' },
		],
	},
	bodyPriceTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper table tbody tr td.product-total ',
			},
		],
	},
	bodyBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 0, right: 0, bottom: 1, left: 0 },
			color: '#E6E6E6',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper table tbody tr td , {{WOPB}} .wopb-product-wrapper table tfoot tr th , {{WOPB}} .wopb-product-wrapper table tfoot tr td ',
			},
		],
	},
	bodyPadding: {
		type: 'object',
		default: { lg: { top: 16, right: 0, bottom: 16, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper table tbody tr td , {{WOPB}} .wopb-product-wrapper table tfoot tr th , {{WOPB}} .wopb-product-wrapper table tfoot tr td { padding:{{bodyPadding}} !important; }',
			},
		],
	},
	enableImage: {
		type: 'boolean',
		default: false,
	},
	imgWidth: {
		type: 'object',
		default: { lg: '50', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-info img { max-width: {{imgWidth}}; }',
			},
		],
	},
	imgBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#e8e8e8',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-info img ',
			},
		],
	},
	imgRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-info img { border-radius:{{imgRadius}}; }',
			},
		],
	},
	totalColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper table tfoot tr th {color:{{totalColor}}; }',
			},
		],
	},
	totalPriceColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper table tfoot tr td {color:{{totalPriceColor}}; }',
			},
		],
	},
	totalTextTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: '600',
		},
		style: [
			{ selector: '{{WOPB}} .wopb-product-wrapper table tfoot tr th' },
		],
	},
	totalPriceTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: '400',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper table tfoot tr td, {{WOPB}} .wopb-product-wrapper table tfoot tr td strong',
			},
		],
	},
	totalSecBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 2, right: 0, bottom: 0, left: 0 },
			color: '#070707',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper table tfoot',
			},
		],
	},
	containerBg: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-checkout-review-order { background-color:{{containerBg}}; }',
			},
		],
	},
	containerBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#e8e8e8',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-checkout-review-order ',
			},
		],
	},
	containerRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-checkout-review-order { border-radius:{{containerRadius}}; overflow:hidden; }',
			},
		],
	},
	containerPadding: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-checkout-review-order { padding:{{containerPadding}}; }',
			},
		],
	},
	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	wrapMargin: {
		type: 'object',
		default: { lg: { top: '', bottom: '20', unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#f5f5f5' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#ff176b' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapInnerPadding: {
		type: 'object',
		default: { lg: { unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ padding:{{wrapInnerPadding}}; }',
			},
		],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: { type: 'string', default: '', style: [ { selector: '' } ] },
};
export default attributes;
