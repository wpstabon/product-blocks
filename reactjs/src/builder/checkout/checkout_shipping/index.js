const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';
registerBlockType( 'product-blocks/checkout-shipping', {
	title: __( 'Shipping Address', 'product-blocks' ),
	icon: (
		<img
			src={
				wopb_data.url +
				'assets/img/blocks/builder/checkout_shipping_address.svg'
			}
			alt={ __( 'Image', 'product-blocks' ) }
			style={ { width: '100%' } }
		/>
	),
	category: 'single-checkout',
	description: __(
		'Let shoppers provide their shipping address if that is different from the billing address.',
		'product-blocks'
	),
	keywords: [
		__( 'Shipping Address', 'product-blocks' ),
		__( 'Address', 'product-blocks' ),
		__( 'Shipping', 'product-blocks' ),
	],
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
