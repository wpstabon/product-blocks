const attributes = {
	blockId: { type: 'string', default: '' },
	currentPostId: { type: 'string', default: '' },
	showTitle: { type: 'boolean', default: true },
	shippingTitle: { type: 'string', default: 'Shipping Address' },
	shippingTitleColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-shipping-fields .wopb-shipping-title { color:{{shippingTitleColor}}; }',
			},
		],
	},
	shippingTitleBgColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-shipping-fields .wopb-shipping-title { background-color:{{shippingTitleBgColor}}; }',
			},
		],
	},
	headingAlign: {
		type: 'string',
		default: 'left',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-shipping-fields .wopb-shipping-title { text-align:{{headingAlign}}; }',
			},
		],
	},
	shippingTitleTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '24', unit: 'px' },
			height: { lg: '32', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: '600',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-shipping-fields .wopb-shipping-title ',
			},
		],
	},
	shippingRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-shipping-fields .wopb-shipping-title { border-radius:{{shippingRadius}}; }',
			},
		],
	},
	shippingTitlePadding: {
		type: 'object',
		default: { lg: { top: 0, bottom: 0, left: 0, right: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-shipping-fields .wopb-shipping-title {padding:{{shippingTitlePadding}}; }',
			},
		],
	},
	shippingTitleSpace: {
		type: 'string',
		default: '24',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-shipping-fields .wopb-shipping-title { margin-bottom:{{shippingTitleSpace}}px;}',
			},
		],
	},
	checkboxTitle: { type: 'string', default: 'Ship to Different address ?' },
	checkboxTitleColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper #ship-to-different-address, {{WOPB}} .wopb-product-wrapper #ship-to-different-address span { color:{{checkboxTitleColor}}; }',
			},
		],
	},
	checkboxTitleTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: '500',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper #ship-to-different-address, {{WOPB}} .wopb-product-wrapper #ship-to-different-address span',
			},
		],
	},
	checkboxTitleSpace: {
		type: 'string',
		default: '20',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .shipping_address { margin-top:{{checkboxTitleSpace}}px;}',
			},
		],
	},
	labelColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-shipping-fields__field-wrapper label {color:{{labelColor}}; }',
			},
		],
	},
	requiredColor: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-shipping-fields__field-wrapper .required {color:{{requiredColor}}; }',
			},
		],
	},
	labelTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: '500',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-shipping-fields__field-wrapper label ',
			},
		],
	},
	labelMargin: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 8, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-shipping-fields__field-wrapper label { margin:{{labelMargin}}; }',
			},
		],
	},
	inputHeight: {
		type: 'string',
		default: '48',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields .select2-selection , {{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields input:not([type="checkbox"]), {{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields select { min-height:{{inputHeight}}px;}',
			},
		],
	},
	inputColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields input, {{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields select, {{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields .select2-selection__rendered {color:{{inputColor}}; }',
			},
		],
	},
	inputBgColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields input, {{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields select, {{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields .select2-selection {background:{{inputBgColor}}; }',
			},
		],
	},
	inputFocusColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields input:focus {color:{{inputFocusColor}}; }',
			},
		],
	},
	placeholderColor: {
		type: 'string',
		default: '#6E6E6E',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields input::-webkit-input-placeholder {color:{{placeholderColor}}; }',
			},
		],
	},
	placeholderFocusColor: {
		type: 'string',
		default: '#6E6E6E',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields input:focus::-webkit-input-placeholder {color:{{placeholderFocusColor}}; }',
			},
		],
	},
	inputTypo: {
		type: 'object',
		default: {
			openTypography: 0,
			size: { lg: '14', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields input,' +
					'{{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields select,' +
					'{{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields .select2',
			},
		],
	},
	inputBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#D2D2D2',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields input , {{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields select, {{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields .select2-selection',
			},
		],
	},
	inputFocusBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#070707',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields input:focus , {{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields select:focus, {{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields .select2-selection:focus',
			},
		],
	},
	inputRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields input, {{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields select, {{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields .select2-selection { border-radius:{{inputRadius}}; }',
			},
		],
	},
	inputPadding: {
		type: 'object',
		default: {
			lg: { top: 14, right: 16, bottom: 14, left: 16, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields input, {{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields select, {{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields .select2-selection { padding: {{inputPadding}}; }',
			},
		],
	},
	inputMargin: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 20, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields p { margin:{{inputMargin}}; } {{WOPB}} .wopb-checkout-shipping-container .woocommerce-shipping-fields p:last-child { margin-bottom: 0px }',
			},
		],
	},
	fieldConBgColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-shipping-container {background-color:{{fieldConBgColor}}; }',
			},
		],
	},
	fieldConBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#e0e0e0',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-checkout-shipping-container ' } ],
	},
	fieldConRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-shipping-container { border-radius:{{fieldConRadius}}; }',
			},
		],
	},
	fieldConPadding: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-shipping-container { padding:{{fieldConPadding}}; }',
			},
		],
	},
	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	wrapMargin: {
		type: 'object',
		default: { lg: { top: '', bottom: '20', unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#f5f5f5' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#ff176b' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapInnerPadding: {
		type: 'object',
		default: { lg: { unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ padding:{{wrapInnerPadding}}; }',
			},
		],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: { type: 'string', default: '', style: [ { selector: '' } ] },
};
export default attributes;
