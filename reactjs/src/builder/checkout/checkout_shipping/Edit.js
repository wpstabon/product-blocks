/** @format */

const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../../helper/CssGenerator';
import {
	GeneralAdvanced,
	CustomCssAdvanced,
	CommonSettings,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../../helper/CommonPanel';
import { Sections, Section } from '../../../helper/Sections';
import useFluentSettings from '../../../helper/hooks/useFluentSettings';
import ToolBarElement from '../../../helper/ToolBarElement';
import { AddSettingsToToolbar } from './Settings';
import useBlockId from '../../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const namingWidth = 'wopb-naming-half-width';
	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: {
			blockId,
			advanceId,
			showTitle,
			shippingTitle,
			checkboxTitle,
		},
	} = props;

	const store = { setAttributes, name, attributes, clientId };

	const { section, toolbarSettings, setCurrSettings } = useFluentSettings();

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/checkout-shipping', blockId );
	}

	return (
		<>
			<ToolBarElement
				store={ store }
				include={ [
					{
						type: 'block_spacing',
						label: __( 'Spacing', 'product-blocks' ),
					},
				] }
			/>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Settings', 'product-blocks' ) }
					>
						<CommonSettings
							depend="showTitle"
							initialOpen={ section[ 'shipping-title' ] }
							title={ __( 'Shipping Title', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'text',
										key: 'shippingTitle',
										label: __(
											'Title Text',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'shippingTitleColor',
										label: __(
											'Title Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'shippingTitleBgColor',
										label: __(
											'Title Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'alignment',
										key: 'headingAlign',
										label: __(
											'Alignment',
											'product-blocks'
										),
										disableJustify: true,
									},
								},
								{
									data: {
										type: 'typography',
										key: 'shippingTitleTypo',
										label: __(
											'Title Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'shippingRadius',
										label: __(
											'Title Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'shippingTitlePadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'range',
										key: 'shippingTitleSpace',
										label: __(
											'Title Space',
											'product-blocks'
										),
										min: 0,
										max: 60,
										step: 1,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'ship-add' ] }
							title={ __(
								'Ship to Different address checkbox',
								'product-blocks'
							) }
							store={ store }
							include={ [
								{
									data: {
										type: 'text',
										key: 'checkboxTitle',
										label: __( 'Text', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'checkboxTitleColor',
										label: __(
											'Text Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'checkboxTitleTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'checkboxTitleSpace',
										label: __( 'Space', 'product-blocks' ),
										min: 0,
										max: 60,
										step: 1,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section.label }
							title={ __( 'Label', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'labelColor',
										label: __(
											'Label Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'requiredColor',
										label: __(
											'Required Indicator Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'labelTypo',
										label: __(
											'Label Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'labelMargin',
										label: __(
											'Label Margin',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'input-fields' ] }
							title={ __( 'Input Fields', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'range',
										key: 'inputHeight',
										label: __( 'Height', 'product-blocks' ),
										min: 1,
										max: 100,
										step: 1,
									},
								},
								{
									data: {
										type: 'tab',
										content: [
											{
												name: 'normal',
												title: 'Normal',
												options: [
													{
														type: 'color',
														key: 'inputColor',
														label: __(
															'Color',
															'product-blocks'
														),
													},
													{
														type: 'color',
														key: 'inputBgColor',
														label: __(
															'Background',
															'product-blocks'
														),
													},
													{
														type: 'color',
														key: 'placeholderColor',
														label: __(
															'Placeholder Color',
															'product-blocks'
														),
													},
													{
														type: 'typography',
														key: 'inputTypo',
														label: __(
															'Input Typography',
															'product-blocks'
														),
													},
													{
														type: 'border',
														key: 'inputBorder',
														label: __(
															'Input Border',
															'product-blocks'
														),
													},
													{
														type: 'dimension',
														key: 'inputRadius',
														label: __(
															'Input Border Radius',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
													{
														type: 'dimension',
														key: 'inputPadding',
														label: __(
															'Input Padding',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
													{
														type: 'dimension',
														key: 'inputMargin',
														label: __(
															'Input Margin',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
												],
											},
											{
												name: 'focus',
												title: 'Focus',
												options: [
													{
														type: 'color',
														key: 'inputFocusColor',
														label: __(
															'Color',
															'product-blocks'
														),
													},
													{
														type: 'color',
														key: 'placeholderFocusColor',
														label: __(
															'Placeholder Color',
															'product-blocks'
														),
													},
													{
														type: 'border',
														key: 'inputFocusBorder',
														label: __(
															'Input Border',
															'product-blocks'
														),
													},
												],
											},
										],
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'field-container' ] }
							title={ __( 'Field Container', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'fieldConBgColor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'fieldConBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'fieldConRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'fieldConPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced initialOpen={ true } store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar
				store={ store }
				selected={ toolbarSettings }
			/>

			<div
				id={ advanceId ? advanceId : '' }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div
					className={ 'wopb-product-wrapper' }
					onMouseDown={ ( e ) =>
						setCurrSettings( e, 'field-container' )
					}
				>
					<div className="wopb-checkout-shipping-container">
						<h3 id="ship-to-different-address">
							<label
								className="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox wopb-component-simple"
								onClick={ ( e ) =>
									setCurrSettings( e, 'ship-add' )
								}
							>
								<input
									id="ship-to-different-address-checkbox"
									className="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox"
									type="checkbox"
									name="ship_to_different_address"
									value="1"
									checked
								/>
								<span>{ checkboxTitle }</span>
							</label>
						</h3>
						<div className="shipping_address">
							{ /* <form name="checkout" method="post" className="checkout woocommerce-checkout" action="http://localhost/wp_st/checkout/" enctype="multipart/form-data" novalidate="novalidate"> */ }
							<div className="col2-set" id="customer_details">
								<div className="col-1">
									<div className="woocommerce-shipping-fields">
										{ showTitle && (
											<div
												className="wopb-shipping-title wopb-component-simple"
												onClick={ ( e ) =>
													setCurrSettings(
														e,
														'shipping-title'
													)
												}
											>
												{ shippingTitle }
											</div>
										) }
										<div className="woocommerce-shipping-fields__field-wrapper">
											<p
												className={ `form-row form-row-first validate-required ${ namingWidth }` }
												id="shipping_first_name_field"
												data-priority="10"
											>
												<label
													htmlFor="shipping_first_name"
													className="wopb-billiing-label wopb-component-simple"
													onClick={ ( e ) =>
														setCurrSettings(
															e,
															'label'
														)
													}
												>
													First Name&nbsp;
													<abbr
														className="required"
														title="required"
													>
														*
													</abbr>
												</label>
												<br></br>
												<span className="woocommerce-input-wrapper">
													<input
														type="text"
														className="input-text "
														name="shipping_first_name"
														id="shipping_first_name"
														placeholder=""
														autoComplete="given-name"
														onMouseDown={ ( e ) =>
															setCurrSettings(
																e,
																'input-fields'
															)
														}
													/>
												</span>
											</p>

											<p
												className={ `form-row form-row-last validate-required ${ namingWidth }` }
												id="shipping_last_name_field"
												data-priority="20"
											>
												<label
													htmlFor="shipping_last_name"
													className="wopb-billiing-label wopb-component-simple"
													onClick={ ( e ) =>
														setCurrSettings(
															e,
															'label'
														)
													}
												>
													Last Name&nbsp;
													<abbr
														className="required"
														title="required"
													>
														*
													</abbr>
												</label>
												<br></br>
												<span className="woocommerce-input-wrapper">
													<input
														type="text"
														className="input-text "
														name="shipping_last_name"
														id="shipping_last_name"
														placeholder=""
														autoComplete="family-name"
														onMouseDown={ ( e ) =>
															setCurrSettings(
																e,
																'input-fields'
															)
														}
													/>
												</span>
											</p>

											<p
												className="form-row form-row-wide"
												id="shipping_company_field"
												data-priority="30"
											>
												<label
													htmlFor="shipping_company"
													className="wopb-billiing-label wopb-component-simple"
													onClick={ ( e ) =>
														setCurrSettings(
															e,
															'label'
														)
													}
												>
													Company&nbsp;
													<span className="optional">
														(optional)
													</span>
												</label>
												<br></br>
												<span className="woocommerce-input-wrapper">
													<input
														type="text"
														className="input-text "
														name="shipping_company"
														id="shipping_company"
														placeholder=""
														autoComplete="organization"
														onMouseDown={ ( e ) =>
															setCurrSettings(
																e,
																'input-fields'
															)
														}
													/>
												</span>
											</p>

											<p
												className="form-row form-row-wide address-field update_totals_on_change validate-required"
												id="shipping_country_field"
												data-priority="40"
											>
												<label
													htmlFor="shipping_country"
													className="wopb-billiing-label wopb-component-simple"
													onClick={ ( e ) =>
														setCurrSettings(
															e,
															'label'
														)
													}
												>
													Country&nbsp;
													<abbr
														className="required"
														title="required"
													>
														*
													</abbr>
												</label>
												<br></br>
												<span className="woocommerce-input-wrapper">
													<select
														name="shipping_country"
														id="shipping_country"
														className="country_to_state country_select"
														autoComplete="country"
														data-placeholder="Select a country / region…"
														data-label="Country / Region"
														tabIndex={ -1 }
														aria-hidden="true"
														onMouseDown={ ( e ) =>
															setCurrSettings(
																e,
																'input-fields'
															)
														}
													>
														<option
															value=""
															selected
														>
															Select a country /
															region…
														</option>
														<option value="BS">
															Bahamas
														</option>
														<option value="BH">
															Bahrain
														</option>
														<option value="BD">
															Bangladesh
														</option>
														<option value="BB">
															Barbados
														</option>
														<option value="BY">
															Belarus
														</option>
														<option value="ZM">
															Zambia
														</option>
														<option value="ZW">
															Zimbabwe
														</option>
													</select>
													{ /* <span className="select2 select2-container select2-container--default" dir="ltr"><span className="selection"><span className="select2-selection select2-selection--single" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-label="Country / Region" role="combobox"><span className="select2-selection__rendered" id="select2-shipping_country-container" role="textbox" aria-readonly="true" title="Bangladesh">Bangladesh</span><span className="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span className="dropdown-wrapper" aria-hidden="true"></span></span> */ }
													<noscript>
														<button
															type="submit"
															name="woocommerce_checkout_update_totals"
														>
															Update country /
															region
														</button>
													</noscript>
												</span>
											</p>

											<p
												className="form-row address-field validate-required form-row-wide"
												id="shipping_city_field"
												data-priority="70"
												data-o_className="form-row form-row-wide address-field validate-required"
											>
												<label
													htmlFor="shipping_city"
													className="wopb-billiing-label wopb-component-simple"
													onClick={ ( e ) =>
														setCurrSettings(
															e,
															'label'
														)
													}
												>
													Town&nbsp;
													<abbr
														className="required"
														title="required"
													>
														*
													</abbr>
												</label>
												<br></br>
												<span className="woocommerce-input-wrapper">
													<input
														type="text"
														className="input-text "
														name="shipping_city"
														id="shipping_city"
														placeholder=""
														autoComplete="address-level2"
														onMouseDown={ ( e ) =>
															setCurrSettings(
																e,
																'input-fields'
															)
														}
													/>
												</span>
											</p>

											<>
												<p
													className="form-row address-field validate-required form-row-wide"
													id="shipping_address_1_field"
													data-priority="50"
												>
													<label
														htmlFor="shipping_address_1"
														className="wopb-billiing-label wopb-component-simple"
														onClick={ ( e ) =>
															setCurrSettings(
																e,
																'label'
															)
														}
													>
														Street&nbsp;
														<abbr
															className="required"
															title="required"
														>
															*
														</abbr>
													</label>
													<br></br>
													<span className="woocommerce-input-wrapper">
														<input
															type="text"
															className="input-text "
															name="shipping_address_1"
															id="shipping_address_1"
															placeholder="House number and street name"
															autoComplete="address-line1"
															data-placeholder="House number and street name"
															onMouseDown={ (
																e
															) =>
																setCurrSettings(
																	e,
																	'input-fields'
																)
															}
														/>
													</span>
												</p>
												<p
													className="form-row address-field form-row-wide"
													id="shipping_address_2_field"
													data-priority="60"
												>
													<label
														htmlFor="shipping_address_2"
														className="screen-reader-text wopb-billiing-label wopb-component-simple"
														onClick={ ( e ) =>
															setCurrSettings(
																e,
																'label'
															)
														}
													>
														Apartment, suite, unit,
														etc.&nbsp;
														<span className="optional">
															(optional)
														</span>
													</label>
													<span className="woocommerce-input-wrapper">
														<input
															type="text"
															className="input-text "
															name="shipping_address_2"
															id="shipping_address_2"
															placeholder="Apartment, suite, unit, etc. (optional)"
															autoComplete="address-line2"
															data-placeholder="XApartment, suite, unit, etc. (optional)"
															onMouseDown={ (
																e
															) =>
																setCurrSettings(
																	e,
																	'input-fields'
																)
															}
														/>
													</span>
												</p>{ ' ' }
											</>

											<p
												className="form-row address-field validate-required validate-state form-row-wide"
												id="shipping_state_field"
												data-priority="80"
												data-o_className="form-row form-row-wide address-field validate-required validate-state"
											>
												<label
													htmlFor="shipping_state"
													className="wopb-billiing-label wopb-component-simple"
													onClick={ ( e ) =>
														setCurrSettings(
															e,
															'label'
														)
													}
												>
													State&nbsp;
													<abbr
														className="required"
														title="required"
													>
														*
													</abbr>
												</label>
												<br></br>
												<span className="woocommerce-input-wrapper">
													<select
														name="shipping_state"
														id="shipping_state"
														className="state_select"
														autoComplete="address-level1"
														data-placeholder="Select an option…"
														data-input-classes=""
														data-label="District"
														tabIndex={ -1 }
														aria-hidden="true"
														onMouseDown={ ( e ) =>
															setCurrSettings(
																e,
																'input-fields'
															)
														}
													>
														<option
															value=""
															selected
														>
															Select an option…
														</option>
														<option value="BD-57">
															Sherpur
														</option>
														<option value="BD-59">
															Sirajganj
														</option>
														<option value="BD-61">
															Sunamganj
														</option>
														<option value="BD-60">
															Sylhet
														</option>
														<option value="BD-63">
															Tangail
														</option>
														<option value="BD-64">
															Thakurgaon
														</option>
													</select>
													{ /* <span className="select2 select2-container select2-container--default" dir="ltr" style={{width: "100%"}}><span className="selection"><span className="select2-selection select2-selection--single" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-label="District" role="combobox"><span className="select2-selection__rendered" id="select2-shipping_state-container" role="textbox" aria-readonly="true" title="Dhaka">Dhaka</span><span className="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span className="dropdown-wrapper" aria-hidden="true"></span></span> */ }
												</span>
											</p>

											<p
												className="form-row address-field validate-postcode form-row-wide"
												id="shipping_postcode_field"
												data-priority="90"
												data-o_className="form-row form-row-wide address-field validate-postcode"
											>
												<label
													htmlFor="shipping_postcode"
													className="wopb-billiing-label wopb-component-simple"
													onClick={ ( e ) =>
														setCurrSettings(
															e,
															'label'
														)
													}
												>
													Postcode&nbsp;
													<span className="optional">
														(optional)
													</span>
												</label>
												<br></br>
												<span className="woocommerce-input-wrapper">
													<input
														type="text"
														className="input-text "
														name="shipping_postcode"
														id="shipping_postcode"
														placeholder=""
														autoComplete="postal-code"
														onMouseDown={ ( e ) =>
															setCurrSettings(
																e,
																'input-fields'
															)
														}
													/>
												</span>
											</p>
										</div>
									</div>
								</div>
							</div>
							{ /* </form>  */ }
						</div>
					</div>
				</div>
			</div>
		</>
	);
}
