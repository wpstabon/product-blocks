const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';

registerBlockType( 'product-blocks/checkout-billing', {
	title: __( 'Billing Address', 'product-blocks' ),
	icon: (
		<img
			src={
				wopb_data.url +
				'assets/img/blocks/builder/checkout_billing_address.svg'
			}
			alt={ __( 'Image', 'product-blocks' ) }
			style={ { width: '100%' } }
		/>
	),
	category: 'single-checkout',
	description: __(
		'Let shoppers fill up their billing address by displaying the blocks to the desired position.',
		'product-blocks'
	),
	keywords: [
		__( 'Billing Address', 'product-blocks' ),
		__( 'Address', 'product-blocks' ),
		__( 'Billing', 'product-blocks' ),
	],
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
