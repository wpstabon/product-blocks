/** @format */

const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../../helper/CssGenerator';
import {
	GeneralAdvanced,
	CustomCssAdvanced,
	CommonSettings,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../../helper/CommonPanel';
import { Sections, Section } from '../../../helper/Sections';
import useFluentSettings from '../../../helper/hooks/useFluentSettings';
import ToolBarElement from '../../../helper/ToolBarElement';
import { AddSettingsToToolbar } from './Settings';
import useBlockId from '../../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const namingWidth = 'wopb-naming-half-width';

	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: { blockId, advanceId, showTitle, billingTitle },
	} = props;

	const store = { setAttributes, name, attributes, clientId };

	const { section, toolbarSettings, setCurrSettings } = useFluentSettings();

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/checkout-billing', blockId );
	}

	return (
		<>
			<ToolBarElement
				store={ store }
				include={ [
					{
						type: 'block_spacing',
						label: __( 'Spacing', 'product-blocks' ),
					},
				] }
			/>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Settings', 'product-blocks' ) }
					>
						<CommonSettings
							depend="showTitle"
							initialOpen={ section[ 'billing-title' ] }
							title={ __( 'Billing Title', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'text',
										key: 'billingTitle',
										label: __(
											'Title Text',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'billingTitleColor',
										label: __(
											'Title Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'billingTitleBgColor',
										label: __(
											'Title Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'alignment',
										key: 'headingAlign',
										label: __(
											'Alignment',
											'product-blocks'
										),
										disableJustify: true,
									},
								},
								{
									data: {
										type: 'typography',
										key: 'billingTitleTypo',
										label: __(
											'Title Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'billingRadius',
										label: __(
											'Title Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'billingTitlePadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'range',
										key: 'billingTitleSpace',
										label: __(
											'Title Space',
											'product-blocks'
										),
										min: 0,
										max: 60,
										step: 1,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section.label }
							title={ __( 'Label', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'labelColor',
										label: __(
											'Label Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'requiredColor',
										label: __(
											'Required Indicator Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'labelTypo',
										label: __(
											'Label Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'labelMargin',
										label: __(
											'Label Margin',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'input-fields' ] }
							title={ __( 'Input Fields', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'range',
										key: 'inputHeight',
										label: __( 'Height', 'product-blocks' ),
										min: 25,
										max: 100,
										step: 1,
									},
								},
								{
									data: {
										type: 'tab',
										content: [
											{
												name: 'normal',
												title: 'Normal',
												options: [
													{
														type: 'color',
														key: 'inputColor',
														label: __(
															'Color',
															'product-blocks'
														),
													},
													{
														type: 'color',
														key: 'inputBgColor',
														label: __(
															'Background',
															'product-blocks'
														),
													},
													{
														type: 'color',
														key: 'placeholderColor',
														label: __(
															'Placeholder Color',
															'product-blocks'
														),
													},
													{
														type: 'typography',
														key: 'inputTypo',
														label: __(
															'Input Typography',
															'product-blocks'
														),
													},
													{
														type: 'border',
														key: 'inputBorder',
														label: __(
															'Input Border',
															'product-blocks'
														),
													},
													{
														type: 'dimension',
														key: 'inputRadius',
														label: __(
															'Input Border Radius',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
													{
														type: 'dimension',
														key: 'inputPadding',
														label: __(
															'Input Padding',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
													{
														type: 'dimension',
														key: 'inputMargin',
														label: __(
															'Input Margin',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
												],
											},
											{
												name: 'focus',
												title: 'Focus',
												options: [
													{
														type: 'color',
														key: 'inputFocusColor',
														label: __(
															'Color',
															'product-blocks'
														),
													},
													{
														type: 'color',
														key: 'placeholderFocusColor',
														label: __(
															'Placeholder Color',
															'product-blocks'
														),
													},
													{
														type: 'border',
														key: 'inputFocusBorder',
														label: __(
															'Input Border',
															'product-blocks'
														),
													},
												],
											},
										],
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'field-container' ] }
							title={ __( 'Field Container', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'fieldConBgColor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'fieldConBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'fieldConRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'fieldConPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced initialOpen={ true } store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar
				store={ store }
				selected={ toolbarSettings }
			/>

			<div
				id={ advanceId ? advanceId : '' }
				className={ `wopb-block-${ blockId } ${ className }` }
				onMouseDown={ ( e ) => setCurrSettings( e, 'field-container' ) }
			>
				<div className={ 'wopb-product-wrapper' }>
					<div className="wopb-checkout-billing-container">
						<form
							name="checkout"
							method="post"
							className="checkout woocommerce-checkout"
							action="http://localhost/wp_st/checkout/"
							encType="multipart/form-data"
							noValidate
						>
							<div className="col2-set" id="customer_details">
								<div className="col-1">
									<div className="woocommerce-billing-fields">
										{ showTitle && (
											<div
												className="wopb-billing-title wopb-component-simple"
												onClick={ ( e ) =>
													setCurrSettings(
														e,
														'billing-title'
													)
												}
											>
												{ billingTitle }
											</div>
										) }
										<div className="woocommerce-billing-fields__field-wrapper">
											<p
												className={ `form-row form-row-first validate-required ${ namingWidth }` }
												id="billing_first_name_field"
												data-priority="10"
											>
												<label
													htmlFor="billing_first_name"
													className="wopb-billiing-label wopb-component-simple"
													onClick={ ( e ) =>
														setCurrSettings(
															e,
															'label'
														)
													}
												>
													First Name&nbsp;
													<abbr
														className="required"
														title="required"
													>
														*
													</abbr>
												</label>
												<br></br>
												<span className="woocommerce-input-wrapper">
													<input
														type="text"
														className="input-text "
														name="billing_first_name"
														id="billing_first_name"
														placeholder=""
														autoComplete="given-name"
														onMouseDown={ ( e ) =>
															setCurrSettings(
																e,
																'input-fields'
															)
														}
													/>
												</span>
											</p>

											<p
												className={ `form-row form-row-last validate-required ${ namingWidth }` }
												id="billing_last_name_field"
												data-priority="20"
											>
												<label
													htmlFor="billing_last_name"
													className="wopb-billiing-label wopb-component-simple"
													onClick={ ( e ) =>
														setCurrSettings(
															e,
															'label'
														)
													}
												>
													Last Name &nbsp;
													<abbr
														className="required"
														title="required"
													>
														*
													</abbr>
												</label>
												<br></br>
												<span className="woocommerce-input-wrapper">
													<input
														type="text"
														className="input-text "
														name="billing_last_name"
														id="billing_last_name"
														placeholder=""
														autoComplete="family-name"
														onMouseDown={ ( e ) =>
															setCurrSettings(
																e,
																'input-fields'
															)
														}
													/>
												</span>
											</p>

											<p
												className="form-row form-row-wide validate-required validate-email"
												id="billing_email_field"
												data-priority="110"
											>
												<label
													htmlFor="billing_email"
													className="wopb-billiing-label wopb-component-simple"
													onClick={ ( e ) =>
														setCurrSettings(
															e,
															'label'
														)
													}
												>
													Email&nbsp;
													<abbr
														className="required"
														title="required"
													>
														*
													</abbr>
												</label>
												<br></br>
												<span className="woocommerce-input-wrapper">
													<input
														type="email"
														className="input-text "
														name="billing_email"
														id="billing_email"
														placeholder=""
														autoComplete="email username"
														onMouseDown={ ( e ) =>
															setCurrSettings(
																e,
																'input-fields'
															)
														}
													/>
												</span>
											</p>

											<p
												className="form-row form-row-wide validate-required validate-phone"
												id="billing_phone_field"
												data-priority="100"
											>
												<label
													htmlFor="billing_phone"
													className="wopb-billiing-label wopb-component-simple"
													onClick={ ( e ) =>
														setCurrSettings(
															e,
															'label'
														)
													}
												>
													Phone&nbsp;
													<abbr
														className="required"
														title="required"
													>
														*
													</abbr>
												</label>
												<br></br>
												<span className="woocommerce-input-wrapper">
													<input
														type="tel"
														className="input-text "
														name="billing_phone"
														id="billing_phone"
														placeholder=""
														autoComplete="tel"
														onMouseDown={ ( e ) =>
															setCurrSettings(
																e,
																'input-fields'
															)
														}
													/>
												</span>
											</p>

											<p
												className="form-row form-row-wide"
												id="billing_company_field"
												data-priority="30"
											>
												<label
													htmlFor="billing_company"
													className="wopb-billiing-label wopb-component-simple"
													onClick={ ( e ) =>
														setCurrSettings(
															e,
															'label'
														)
													}
												>
													Company&nbsp;
													<span className="optional">
														(optional)
													</span>
												</label>
												<br></br>
												<span className="woocommerce-input-wrapper">
													<input
														type="text"
														className="input-text "
														name="billing_company"
														id="billing_company"
														placeholder=""
														autoComplete="organization"
														onMouseDown={ ( e ) =>
															setCurrSettings(
																e,
																'input-fields'
															)
														}
													/>
												</span>
											</p>

											<p
												className="form-row form-row-wide address-field update_totals_on_change validate-required"
												id="billing_country_field"
												data-priority="40"
											>
												<label
													htmlFor="billing_country"
													className="wopb-billiing-label wopb-component-simple"
													onClick={ ( e ) =>
														setCurrSettings(
															e,
															'label'
														)
													}
												>
													Country&nbsp;
													<abbr
														className="required"
														title="required"
													>
														*
													</abbr>
												</label>
												<br></br>
												<span className="woocommerce-input-wrapper">
													<select
														name="billing_country"
														id="billing_country"
														className="country_to_state country_select"
														autoComplete="country"
														data-placeholder="Select a country / region…"
														data-label="Country / Region"
														tabIndex={ -1 }
														aria-hidden="true"
														onMouseDown={ ( e ) =>
															setCurrSettings(
																e,
																'input-fields'
															)
														}
													>
														<option
															value=""
															selected
														>
															Select a country /
															region…
														</option>
														<option value="BS">
															Bahamas
														</option>
														<option value="BH">
															Bahrain
														</option>
														<option value="BD">
															Bangladesh
														</option>
														<option value="BB">
															Barbados
														</option>
														<option value="BY">
															Belarus
														</option>
														<option value="ZM">
															Zambia
														</option>
														<option value="ZW">
															Zimbabwe
														</option>
													</select>
													{ /* <span className="select2 select2-container select2-container--default" dir="ltr"><span className="selection"><span className="select2-selection select2-selection--single" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-label="Country / Region" role="combobox"><span className="select2-selection__rendered" id="select2-billing_country-container" role="textbox" aria-readonly="true" title="Bangladesh">Bangladesh</span><span className="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span className="dropdown-wrapper" aria-hidden="true"></span></span> */ }
													<noscript>
														<button
															type="submit"
															name="woocommerce_checkout_update_totals"
														>
															Update country /
															region
														</button>
													</noscript>
												</span>
											</p>

											<p
												className="form-row address-field validate-required form-row-wide"
												id="billing_city_field"
												data-priority="70"
												data-o_className="form-row form-row-wide address-field validate-required"
											>
												<label
													htmlFor="billing_city"
													className="wopb-billiing-label wopb-component-simple"
													onClick={ ( e ) =>
														setCurrSettings(
															e,
															'label'
														)
													}
												>
													Town&nbsp;
													<abbr
														className="required"
														title="required"
													>
														*
													</abbr>
												</label>
												<br></br>
												<span className="woocommerce-input-wrapper">
													<input
														type="text"
														className="input-text "
														name="billing_city"
														id="billing_city"
														placeholder=""
														autoComplete="address-level2"
														onMouseDown={ ( e ) =>
															setCurrSettings(
																e,
																'input-fields'
															)
														}
													/>
												</span>
											</p>

											<>
												<p
													className="form-row address-field validate-required form-row-wide"
													id="billing_address_1_field"
													data-priority="50"
												>
													<label
														htmlFor="billing_address_1"
														className="wopb-billiing-label wopb-component-simple"
														onClick={ ( e ) =>
															setCurrSettings(
																e,
																'label'
															)
														}
													>
														Street&nbsp;
														<abbr
															className="required"
															title="required"
														>
															*
														</abbr>
													</label>
													<br></br>
													<span className="woocommerce-input-wrapper">
														<input
															type="text"
															className="input-text "
															name="billing_address_1"
															id="billing_address_1"
															placeholder="House number and street name"
															autoComplete="address-line1"
															data-placeholder="House number and street name"
															onMouseDown={ (
																e
															) =>
																setCurrSettings(
																	e,
																	'input-fields'
																)
															}
														/>
													</span>
												</p>
												<p
													className="form-row address-field form-row-wide"
													id="billing_address_2_field"
													data-priority="60"
												>
													<label
														htmlFor="billing_address_2"
														className="screen-reader-text wopb-billiing-label wopb-component-simple"
														onClick={ ( e ) =>
															setCurrSettings(
																e,
																'label'
															)
														}
													>
														Apartment, suite, unit,
														etc.&nbsp;
														<span className="optional">
															(optional)
														</span>
													</label>
													<span className="woocommerce-input-wrapper">
														<input
															type="text"
															className="input-text "
															name="billing_address_2"
															id="billing_address_2"
															placeholder="Apartment, suite, unit, etc. (optional)"
															autoComplete="address-line2"
															data-placeholder="XApartment, suite, unit, etc. (optional)"
															onMouseDown={ (
																e
															) =>
																setCurrSettings(
																	e,
																	'input-fields'
																)
															}
														/>
													</span>
												</p>
											</>

											<p
												className="form-row address-field validate-required validate-state form-row-wide"
												id="billing_state_field"
												data-priority="80"
												data-o_className="form-row form-row-wide address-field validate-required validate-state"
											>
												<label
													htmlFor="billing_state"
													className="wopb-billiing-label wopb-component-simple"
													onClick={ ( e ) =>
														setCurrSettings(
															e,
															'label'
														)
													}
												>
													State&nbsp;
													<abbr
														className="required"
														title="required"
													>
														*
													</abbr>
												</label>
												<br></br>
												<span className="woocommerce-input-wrapper">
													<select
														name="billing_state"
														id="billing_state"
														className="state_select"
														autoComplete="address-level1"
														data-placeholder="Select an option…"
														data-input-classes=""
														data-label="District"
														tabIndex={ -1 }
														aria-hidden="true"
														onMouseDown={ ( e ) =>
															setCurrSettings(
																e,
																'input-fields'
															)
														}
													>
														<option
															value=""
															selected={ true }
														>
															Select an option…
														</option>
														<option value="BD-57">
															Sherpur
														</option>
														<option value="BD-59">
															Sirajganj
														</option>
														<option value="BD-61">
															Sunamganj
														</option>
														<option value="BD-60">
															Sylhet
														</option>
														<option value="BD-63">
															Tangail
														</option>
														<option value="BD-64">
															Thakurgaon
														</option>
													</select>
													{ /* <span className="select2 select2-container select2-container--default" dir="ltr" style={{width: "100%"}}><span className="selection"><span className="select2-selection select2-selection--single" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-label="District" role="combobox"><span className="select2-selection__rendered" id="select2-billing_state-container" role="textbox" aria-readonly="true" title="Dhaka">Dhaka</span><span className="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span className="dropdown-wrapper" aria-hidden="true"></span></span> */ }
												</span>
											</p>

											<p
												className="form-row address-field validate-postcode form-row-wide"
												id="billing_postcode_field"
												data-priority="90"
												data-o_className="form-row form-row-wide address-field validate-postcode"
											>
												<label
													htmlFor="billing_postcode"
													className="wopb-billiing-label wopb-component-simple"
													onClick={ ( e ) =>
														setCurrSettings(
															e,
															'label'
														)
													}
												>
													Post Code&nbsp;
													<span className="optional">
														(optional)
													</span>
												</label>
												<br></br>
												<span className="woocommerce-input-wrapper">
													<input
														type="text"
														className="input-text "
														name="billing_postcode"
														id="billing_postcode"
														placeholder=""
														autoComplete="postal-code"
														onMouseDown={ ( e ) =>
															setCurrSettings(
																e,
																'input-fields'
															)
														}
													/>
												</span>
											</p>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</>
	);
}
