/** @format */

import { settingsIcon } from '../../../helper/CommonPanel';
import WopbToolbarDropdown from '../../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar, getData } from '../../../helper/ux';

const { __ } = wp.i18n;

export function AddSettingsToToolbar( { store, selected } ) {
	switch ( selected ) {
		case 'billing-title':
			return (
				<WopbToolbarGroup text={ 'Title' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Billing Title Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Billing Title Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'text',
											key: 'billingTitle',
											label: __(
												'Title Text',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'billingTitleColor',
											label: __(
												'Title Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'billingTitleBgColor',
											label: __(
												'Title Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'alignment',
											key: 'headingAlign',
											label: __(
												'Alignment',
												'product-blocks'
											),
											disableJustify: true,
										},
									},
									{
										data: {
											type: 'typography',
											key: 'billingTitleTypo',
											label: __(
												'Title Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'billingRadius',
											label: __(
												'Title Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'billingTitlePadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'range',
											key: 'billingTitleSpace',
											label: __(
												'Title Space',
												'product-blocks'
											),
											min: 0,
											max: 60,
											step: 1,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'label':
			return (
				<WopbToolbarGroup text={ 'Label' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Label Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Label Settings', 'product-blocks' ),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'labelColor',
											label: __(
												'Label Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'requiredColor',
											label: __(
												'Required Indicator Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'labelTypo',
											label: __(
												'Label Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'labelMargin',
											label: __(
												'Label Margin',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'input-fields':
			return (
				<WopbToolbarGroup text={ 'Input Fields' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Input Fields Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Input Fields Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'range',
											key: 'inputHeight',
											label: __(
												'Height',
												'product-blocks'
											),
											min: 25,
											max: 100,
											step: 1,
										},
									},
									{
										data: {
											type: 'tab',
											content: [
												{
													name: 'normal',
													title: 'Normal',
													options: [
														{
															type: 'color',
															key: 'inputColor',
															label: __(
																'Color',
																'product-blocks'
															),
														},
														{
															type: 'color',
															key: 'inputBgColor',
															label: __(
																'Background',
																'product-blocks'
															),
														},
														{
															type: 'color',
															key: 'placeholderColor',
															label: __(
																'Placeholder Color',
																'product-blocks'
															),
														},
														{
															type: 'typography',
															key: 'inputTypo',
															label: __(
																'Input Typography',
																'product-blocks'
															),
														},
														{
															type: 'border',
															key: 'inputBorder',
															label: __(
																'Input Border',
																'product-blocks'
															),
														},
														{
															type: 'dimension',
															key: 'inputRadius',
															label: __(
																'Input Border Radius',
																'product-blocks'
															),
															step: 1,
															unit: true,
															responsive: true,
														},
														{
															type: 'dimension',
															key: 'inputMargin',
															label: __(
																'Input Margin',
																'product-blocks'
															),
															step: 1,
															unit: true,
															responsive: true,
														},
													],
												},
												{
													name: 'focus',
													title: 'Focus',
													options: [
														{
															type: 'color',
															key: 'inputFocusColor',
															label: __(
																'Color',
																'product-blocks'
															),
														},
														{
															type: 'color',
															key: 'placeholderFocusColor',
															label: __(
																'Placeholder Color',
																'product-blocks'
															),
														},
													],
												},
											],
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'field-container':
			return (
				<WopbToolbarGroup text={ 'Field Container' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Field Container Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Field Container Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'fieldConBgColor',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'fieldConBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'fieldConRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'fieldConPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		// case "":
		//     return (
		//         <WopbToolbarGroup text={"Image"} textScroll>
		//             <WopbToolbarDropdown
		//                 buttonContent={settingsIcon}
		//                 store={store}
		//                 label={__("Product Image Settings", "product-blocks")}
		//                 content={formatSettingsForToolbar([
		//                     {
		//                         name: "tab",
		//                         title: __(
		//                             "Product Image Settings",
		//                             "product-blocks"
		//                         ),
		//                         options: getData(),
		//                     },
		//                 ])}
		//             />
		//         </WopbToolbarGroup>
		//     );
		default:
			return null;
	}
}
