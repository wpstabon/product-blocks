const attributes = {
	blockId: { type: 'string', default: '' },
	currentPostId: { type: 'string', default: '' },
	showTitle: { type: 'boolean', default: true },
	billingTitle: { type: 'string', default: 'Billing Details' },
	billingTitleColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-billing-fields .wopb-billing-title { color:{{billingTitleColor}}; }',
			},
		],
	},
	billingTitleBgColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-billing-fields .wopb-billing-title { background-color:{{billingTitleBgColor}}; }',
			},
		],
	},
	headingAlign: {
		type: 'string',
		default: 'left',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-billing-fields .wopb-billing-title { text-align:{{headingAlign}}; }',
			},
		],
	},
	billingTitleTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '24', unit: 'px' },
			height: { lg: '32', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: '600',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-billing-fields .wopb-billing-title ',
			},
		],
	},
	billingRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-billing-fields .wopb-billing-title { border-radius:{{billingRadius}}; }',
			},
		],
	},
	billingTitlePadding: {
		type: 'object',
		default: { lg: { top: 0, bottom: 0, left: 0, right: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-billing-fields .wopb-billing-title {padding:{{billingTitlePadding}}; }',
			},
		],
	},
	billingTitleSpace: {
		type: 'string',
		default: '24',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-billing-fields .wopb-billing-title { margin-bottom:{{billingTitleSpace}}px;}',
			},
		],
	},
	labelColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-billing-fields__field-wrapper label {color:{{labelColor}}; }',
			},
		],
	},
	requiredColor: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-billing-fields__field-wrapper .required {color:{{requiredColor}}; }',
			},
		],
	},
	labelTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: '500',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-billing-fields__field-wrapper label ',
			},
		],
	},
	labelMargin: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 8, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-billing-fields__field-wrapper label { display: inline-block;  margin:{{labelMargin}}; }',
			},
		],
	},
	inputHeight: {
		type: 'string',
		default: '48',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields input, {{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields select , {{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields .select2-selection { min-height:{{inputHeight}}px;}',
			},
		],
	},
	inputColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields input, {{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields select, {{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields .select2-selection__rendered {color:{{inputColor}}; }',
			},
		],
	},
	inputBgColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields input, {{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields select, {{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields .select2-selection {background:{{inputBgColor}}; }',
			},
		],
	},
	inputFocusColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields input:focus {color:{{inputFocusColor}}; }',
			},
		],
	},
	placeholderColor: {
		type: 'string',
		default: '#6E6E6E',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields input::-webkit-input-placeholder {color:{{placeholderColor}}; }',
			},
		],
	},
	placeholderFocusColor: {
		type: 'string',
		default: '#6E6E6E',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields input:focus::-webkit-input-placeholder {color:{{placeholderFocusColor}}; }',
			},
		],
	},
	inputTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields input,' +
					'{{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields select,' +
					'{{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields .select2',
			},
		],
	},
	inputBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#D2D2D2',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields input , {{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields select, {{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields .select2-selection',
			},
		],
	},
	inputFocusBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#070707',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields input:focus , {{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields select:focus, {{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields .select2-selection:focus',
			},
		],
	},
	inputRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields input, {{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields select, {{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields .select2-selection { border-radius:{{inputRadius}}; }',
			},
		],
	},
	inputPadding: {
		type: 'object',
		default: {
			lg: { top: 14, right: 16, bottom: 14, left: 16, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields input, {{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields select, {{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields .select2-selection { padding: {{inputPadding}}; }',
			},
		],
	},
	inputMargin: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 20, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields p { margin:{{inputMargin}}; } {{WOPB}} .wopb-checkout-billing-container .woocommerce-billing-fields p:last-child { margin-bottom: 0px; } ',
			},
		],
	},
	fieldConBgColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-billing-container {background-color:{{fieldConBgColor}}; }',
			},
		],
	},
	fieldConBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#e0e0e0',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-checkout-billing-container ' } ],
	},
	fieldConRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-billing-container { border-radius:{{fieldConRadius}}; }',
			},
		],
	},
	fieldConPadding: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-billing-container { padding:{{fieldConPadding}}; }',
			},
		],
	},
	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	wrapMargin: {
		type: 'object',
		default: { lg: { top: '', bottom: '20', unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#f5f5f5' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#ff176b' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapInnerPadding: {
		type: 'object',
		default: { lg: { unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ padding:{{wrapInnerPadding}}; }',
			},
		],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: { type: 'string', default: '', style: [ { selector: '' } ] },
};

export default attributes;
