const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';

registerBlockType( 'product-blocks/checkout-login', {
	title: __( 'Checkout Login', 'product-blocks' ),
	icon: (
		<img
			src={
				wopb_data.url + 'assets/img/blocks/builder/checkout_login.svg'
			}
			alt={ __( 'Image', 'product-blocks' ) }
			style={ { width: '100%' } }
		/>
	),
	category: 'single-checkout',
	description: __(
		'Help existing users log in by adding the block to the desired position and customizing it as you need.',
		'product-blocks'
	),
	keywords: [
		__( 'Checkout Login', 'product-blocks' ),
		__( 'Checkout', 'product-blocks' ),
		__( 'Login', 'product-blocks' ),
	],
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
