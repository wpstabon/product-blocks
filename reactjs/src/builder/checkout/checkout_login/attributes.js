const attributes = {
	blockId: { type: 'string', default: '' },
	currentPostId: { type: 'string', default: '' },
	toggleColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login-toggle .woocommerce-info { color:{{toggleColor}}; }',
			},
		],
	},
	linkColor: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login-toggle a { color:{{linkColor}}; }',
			},
		],
	},
	toggleHoverColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login-toggle .woocommerce-info:hover { color:{{toggleHoverColor}}; }',
			},
		],
	},
	toggleBg: {
		type: 'string',
		default: '#E6E6E6',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login-toggle .woocommerce-info { background-color:{{toggleBg}}; }',
			},
		],
	},
	toggleTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: '',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login-toggle .woocommerce-info ',
			},
		],
	},
	toggleBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#E6E6E6',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login-toggle .woocommerce-info ',
			},
		],
	},
	toggleRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login-toggle .woocommerce-info { border-radius:{{toggleRadius}}; }',
			},
		],
	},
	togglePadding: {
		type: 'object',
		default: {
			lg: { top: 14, right: 24, bottom: 14, left: 24, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login-toggle .woocommerce-info { padding:{{togglePadding}} !important; }',
			},
		],
	},
	toggleSpace: {
		type: 'string',
		default: '8',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login-toggle .woocommerce-info { margin-bottom:{{toggleSpace}}px;}',
			},
		],
	},
	descpColor: {
		type: 'string',
		default: '#4A4A4A',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login p:first-child { color:{{descpColor}}; }',
			},
		],
	},
	descpTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '22', unit: 'px' },
			decoration: 'none',
			transform: '',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login p:first-child',
			},
		],
	},
	descpMargin: {
		type: 'string',
		default: '20',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login p:first-child { margin-bottom:{{descpMargin}}px;}',
			},
		],
	},
	labelColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login label { color:{{labelColor}}; }',
			},
		],
	},
	requiredIconColor: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login label .required { color:{{requiredIconColor}}; }',
			},
		],
	},
	labelTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: '',
			weight: '500',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login label',
			},
		],
	},
	labelMargin: {
		type: 'string',
		default: '8',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login label { margin-bottom:{{labelMargin}}px;}',
			},
		],
	},
	inputHeight: {
		type: 'string',
		default: '48',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login input:not([type="checkbox"]) { min-height:{{inputHeight}}px;}',
			},
		],
	},
	inputColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login input { color:{{inputColor}}; }',
			},
		],
	},
	placeholderColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login input::placeholder { color:{{placeholderColor}}; }',
			},
		],
	},
	inputBgColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login input { background-color:{{inputBgColor}}; }',
			},
		],
	},
	inputTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '', unit: 'px' },
			decoration: 'none',
			transform: '',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login input',
			},
		],
	},
	inputBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#D2D2D2',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login input',
			},
		],
	},
	inputRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login input { border-radius:{{inputRadius}}; }',
			},
		],
	},
	inputMargin: {
		type: 'object',
		default: { lg: { top: 6, right: 10, bottom: 6, left: 10, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login .form-row-first input, {{WOPB}} .wopb-product-wrapper .woocommerce-form-login .form-row-last input { padding:{{inputMargin}}; }',
			},
		],
	},
	inputFocusColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login input:focus { color:{{inputFocusColor}}; }',
			},
		],
	},
	placeholderFocusColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login input:focus::placeholder { color:{{placeholderFocusColor}}; }',
			},
		],
	},
	checkboxColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login label.woocommerce-form-login__rememberme { color:{{checkboxColor}}; }',
			},
		],
	},
	checkboxTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: '',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login label.woocommerce-form-login__rememberme ',
			},
		],
	},
	checkboxMargin: {
		type: 'string',
		default: '32',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login label.woocommerce-form-login__rememberme { margin-bottom:{{checkboxMargin}}px;}',
			},
		],
	},
	btnTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: '',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login__submit , .editor-styles-wrapper {{WOPB}} .wopb-product-wrapper .woocommerce-form-login__submit',
			},
		],
	},
	btnColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login__submit { color:{{btnColor}}; }',
			},
		],
	},
	btnBg: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login__submit { background-color:{{btnBg}}; }',
			},
		],
	},
	btnHvrColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login__submit:hover { color:{{btnHvrColor}}; }',
			},
		],
	},
	btnHvrBg: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login__submit:hover { background-color:{{btnHvrBg}}; }',
			},
		],
	},
	btnBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#000',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login__submit',
			},
		],
	},
	btnRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login__submit { border-radius:{{btnRadius}} ; }',
			},
		],
	},
	btnPadding: {
		type: 'object',
		default: {
			lg: { top: 12, right: 24, bottom: 12, left: 24, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login__submit { padding:{{btnPadding}} !important; }',
			},
		],
	},
	containerBg: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login { background-color:{{containerBg}}; }',
			},
		],
	},
	containerLinkColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login .lost_password a { color:{{containerLinkColor}}; }',
			},
		],
	},

	containerBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#d8d8d8',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login ',
			},
		],
	},
	containerRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login { border-radius:{{containerRadius}}; }',
			},
		],
	},
	containerPadding: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-form-login { padding:{{containerPadding}}; }',
			},
		],
	},
	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	wrapMargin: {
		type: 'object',
		default: { lg: { top: '', bottom: '20', unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#f5f5f5' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper { border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#ff176b' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapInnerPadding: {
		type: 'object',
		default: { lg: { unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ padding:{{wrapInnerPadding}}; }',
			},
		],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: { type: 'string', default: '', style: [ { selector: '' } ] },
};
export default attributes;
