/** @format */

const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../../helper/CssGenerator';
import {
	GeneralAdvanced,
	CustomCssAdvanced,
	CommonSettings,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../../helper/CommonPanel';
import { Sections, Section } from '../../../helper/Sections';
import useFluentSettings from '../../../helper/hooks/useFluentSettings';
import ToolBarElement from '../../../helper/ToolBarElement';
import { AddSettingsToToolbar } from './Settings';
import useBlockId from '../../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: { blockId, advanceId },
	} = props;

	const store = { setAttributes, name, attributes, clientId };

	const { section, toolbarSettings, setCurrSettings } = useFluentSettings();

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/checkout-login', blockId );
	}

	return (
		<>
			<ToolBarElement
				store={ store }
				include={ [
					{
						type: 'block_spacing',
						label: __( 'Spacing', 'product-blocks' ),
					},
				] }
			/>

			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Settings', 'product-blocks' ) }
					>
						<CommonSettings
							initialOpen={ section[ 'toggle-text' ] }
							title={ __( 'Toggle Text', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'toggleColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'linkColor',
										label: __(
											'Link Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'toggleHoverColor',
										label: __(
											'Hover Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'toggleBg',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'toggleTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'toggleBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'toggleRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'togglePadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'range',
										key: 'toggleSpace',
										label: __(
											'Space Between',
											'product-blocks'
										),
										min: 0,
										max: 50,
										step: 1,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section.description }
							title={ __( 'Description', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'descpColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'descpTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'descpMargin',
										label: __(
											'Margin Bottom',
											'product-blocks'
										),
										min: 0,
										max: 50,
										step: 1,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'input-label' ] }
							title={ __( 'Input Label', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'labelColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'requiredIconColor',
										label: __(
											'Required Indicator Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'labelTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'labelMargin',
										label: __(
											'Margin Bottom',
											'product-blocks'
										),
										min: 0,
										max: 50,
										step: 1,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'input-fields' ] }
							title={ __( 'Input Fields', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'range',
										key: 'inputHeight',
										label: __( 'Height', 'product-blocks' ),
										min: 36,
										max: 100,
										step: 1,
									},
								},
								{
									data: {
										type: 'tab',
										content: [
											{
												name: 'normal',
												title: 'Normal',
												options: [
													{
														type: 'color',
														key: 'inputColor',
														label: __(
															'Color',
															'product-blocks'
														),
													},
													{
														type: 'color',
														key: 'placeholderColor',
														label: __(
															'Placeholder Color',
															'product-blocks'
														),
													},
													{
														type: 'color',
														key: 'inputBgColor',
														label: __(
															'Background',
															'product-blocks'
														),
													},
													{
														type: 'typography',
														key: 'inputTypo',
														label: __(
															'Typography',
															'product-blocks'
														),
													},
													{
														type: 'border',
														key: 'inputBorder',
														label: __(
															'Border',
															'product-blocks'
														),
													},
													{
														type: 'dimension',
														key: 'inputRadius',
														label: __(
															'Border Radius',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
													{
														type: 'dimension',
														key: 'inputMargin',
														label: __(
															'Padding',
															'product-blocks'
														),
														step: 1,
														unit: true,
														responsive: true,
													},
												],
											},
											{
												name: 'focus',
												title: 'Focus',
												options: [
													{
														type: 'color',
														key: 'inputFocusColor',
														label: __(
															'Color',
															'product-blocks'
														),
													},
													{
														type: 'color',
														key: 'placeholderFocusColor',
														label: __(
															'Placeholder Color',
															'product-blocks'
														),
													},
												],
											},
										],
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'remember-checkbox' ] }
							title={ __(
								'Remember checkbox',
								'product-blocks'
							) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'checkboxColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'checkboxTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'checkboxMargin',
										label: __(
											'Margin Bottom',
											'product-blocks'
										),
										min: 0,
										max: 50,
										step: 1,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section.button }
							title={ __( 'Button', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'btnColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'btnBg',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'btnHvrColor',
										label: __(
											'Hover Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'btnHvrBg',
										label: __(
											'Hover Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'btnTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'btnBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'btnRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'btnPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'field-container' ] }
							title={ __( 'Field Container', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'containerBg',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'containerLinkColor',
										label: __(
											'LInk Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'containerBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'containerRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'containerPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced initialOpen={ true } store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar
				store={ store }
				selected={ toolbarSettings }
			/>

			<div
				id={ advanceId ? advanceId : '' }
				className={ `wopb-block-${ blockId } ${ className }` }
				onClick={ ( e ) => setCurrSettings( e, 'field-container' ) }
			>
				<div className={ 'wopb-product-wrapper ' }>
					<div className="wopb-checkout-login-container">
						<div
							className="woocommerce-form-login-toggle wopb-component-simple"
							onClick={ ( e ) =>
								setCurrSettings( e, 'toggle-text' )
							}
						>
							<div className="woocommerce-info">
								Returning customer?{ ' ' }
								<a href="#" className="showlogin">
									Click here to login
								</a>
							</div>
						</div>
						<form
							className="woocommerce-form woocommerce-form-login login"
							method="post"
						>
							<p
								className="wopb-component-simple"
								onClick={ ( e ) =>
									setCurrSettings( e, 'description' )
								}
							>
								If you have shopped with us before, please enter
								your details below. If you are a new customer,
								please proceed to the Billing section.
							</p>

							<p className="form-row form-row-first">
								<label
									htmlFor="username"
									className="wopb-component-simple"
									onClick={ ( e ) =>
										setCurrSettings(
											e,
											'input-label',
											null,
											true
										)
									}
								>
									Username or email&nbsp;
									<span className="required">*</span>
								</label>
								<input
									type="text"
									className="input-text"
									name="username"
									id="username"
									autoComplete="username"
									onClick={ ( e ) =>
										setCurrSettings( e, 'input-fields' )
									}
								/>
							</p>
							<p className="form-row form-row-last">
								<label
									htmlFor="password"
									className="wopb-component-simple"
									onClick={ ( e ) =>
										setCurrSettings(
											e,
											'input-label',
											null,
											true
										)
									}
								>
									Password&nbsp;
									<span className="required">*</span>
								</label>
								<input
									className="input-text"
									type="password"
									name="password"
									id="password"
									autoComplete="current-password"
									onClick={ ( e ) =>
										setCurrSettings( e, 'input-fields' )
									}
								/>
							</p>
							<div className="clear"></div>

							<p className="form-row">
								<label
									className="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme wopb-component-simple"
									onClick={ ( e ) =>
										setCurrSettings(
											e,
											'remember-checkbox',
											null,
											true
										)
									}
								>
									<input
										className="woocommerce-form__input woocommerce-form__input-checkbox"
										name="rememberme"
										type="checkbox"
										id="rememberme"
										value="forever"
									/>{ ' ' }
									<span>Remember me</span>
								</label>
								<input
									type="hidden"
									id="woocommerce-login-nonce"
									name="woocommerce-login-nonce"
									value="83ebb3f87f"
								/>
								<input
									type="hidden"
									name="_wp_http_referer"
									value="/wp_st/shop/"
								/>
								<input
									type="hidden"
									name="redirect"
									value="http://localhost/wp_st/checkout/"
								/>
								<button
									type="submit"
									className="woocommerce-button button woocommerce-form-login__submit"
									name="login"
									value="Login"
									onClick={ ( e ) =>
										setCurrSettings(
											e,
											'button',
											null,
											true
										)
									}
								>
									Login
								</button>
							</p>
							<p className="lost_password">
								<a href="#">Lost your password?</a>
							</p>
							<div className="clear"></div>
						</form>
					</div>
				</div>
			</div>
		</>
	);
}
