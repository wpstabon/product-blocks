/** @format */

import { settingsIcon } from '../../../helper/CommonPanel';
import WopbToolbarDropdown from '../../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar, getData } from '../../../helper/ux';

const { __ } = wp.i18n;

export function AddSettingsToToolbar( { store, selected } ) {
	switch ( selected ) {
		case 'toggle-text':
			return (
				<WopbToolbarGroup text={ 'Toggle Text' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Toggle Text Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Toggle Text Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'toggleColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'linkColor',
											label: __(
												'Link Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'toggleHoverColor',
											label: __(
												'Hover Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'toggleBg',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'toggleTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'toggleBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'toggleRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'togglePadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'range',
											key: 'toggleSpace',
											label: __(
												'Space Between',
												'product-blocks'
											),
											min: 0,
											max: 50,
											step: 1,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'description':
			return (
				<WopbToolbarGroup text={ 'Desc.' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Description Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Description Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'descpColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'descpTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'range',
											key: 'descpMargin',
											label: __(
												'Margin Bottom',
												'product-blocks'
											),
											min: 0,
											max: 50,
											step: 1,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'input-label':
			return (
				<WopbToolbarGroup text={ 'Label' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Input Label Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Input Label Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'labelColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'requiredIconColor',
											label: __(
												'Required Indicator Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'labelTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'range',
											key: 'labelMargin',
											label: __(
												'Margin Bottom',
												'product-blocks'
											),
											min: 0,
											max: 50,
											step: 1,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'input-fields':
			return (
				<WopbToolbarGroup text={ 'Input' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Input Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Input Settings', 'product-blocks' ),
								options: getData( [
									{
										data: {
											type: 'range',
											key: 'inputHeight',
											label: __(
												'Height',
												'product-blocks'
											),
											min: 1,
											max: 100,
											step: 1,
										},
									},
									{
										data: {
											type: 'tab',
											content: [
												{
													name: 'normal',
													title: 'Normal',
													options: [
														{
															type: 'color',
															key: 'inputColor',
															label: __(
																'Color',
																'product-blocks'
															),
														},
														{
															type: 'color',
															key: 'placeholderColor',
															label: __(
																'Placeholder Color',
																'product-blocks'
															),
														},
														{
															type: 'color',
															key: 'inputBgColor',
															label: __(
																'Background',
																'product-blocks'
															),
														},
														{
															type: 'typography',
															key: 'inputTypo',
															label: __(
																'Typography',
																'product-blocks'
															),
														},
														{
															type: 'border',
															key: 'inputBorder',
															label: __(
																'Border',
																'product-blocks'
															),
														},
														{
															type: 'dimension',
															key: 'inputRadius',
															label: __(
																'Border Radius',
																'product-blocks'
															),
															step: 1,
															unit: true,
															responsive: true,
														},
														{
															type: 'dimension',
															key: 'inputMargin',
															label: __(
																'Margin',
																'product-blocks'
															),
															step: 1,
															unit: true,
															responsive: true,
														},
													],
												},
												{
													name: 'focus',
													title: 'Focus',
													options: [
														{
															type: 'color',
															key: 'inputFocusColor',
															label: __(
																'Color',
																'product-blocks'
															),
														},
														{
															type: 'color',
															key: 'placeholderFocusColor',
															label: __(
																'Placeholder Color',
																'product-blocks'
															),
														},
													],
												},
											],
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'remember-checkbox':
			return (
				<WopbToolbarGroup text={ 'Remember Checkbox' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Remember Checkbox Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Remember Checkbox Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'checkboxColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'checkboxTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'range',
											key: 'checkboxMargin',
											label: __(
												'Margin Bottom',
												'product-blocks'
											),
											min: 0,
											max: 50,
											step: 1,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'button':
			return (
				<WopbToolbarGroup text={ 'Button' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Button Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Button Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'btnColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'btnBg',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'btnTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'btnBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'btnRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'btnPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'field-container':
			return (
				<WopbToolbarGroup text={ 'Field Container' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Field Container Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Field Container Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'containerBg',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'containerBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'containerRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'containerPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		default:
			return null;
	}
}
