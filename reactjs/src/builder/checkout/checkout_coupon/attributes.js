const attributes = {
	blockId: { type: 'string', default: '' },
	currentPostId: { type: 'string', default: '' },
	showToggle: { type: 'boolean', default: true },
	toggleText: { type: 'string', default: 'Enter your promotional code' },
	toggleTextColor: {
		type: 'string',
		default: '#4A4A4A',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-toggle-header { color:{{toggleTextColor}}; font-weight:normal;}',
			},
		],
	},
	toggleTextHoverColor: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-toggle-header:hover { color:{{toggleTextHoverColor}};}',
			},
		],
	},
	toggleTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '26', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '600',
		},
		style: [ { selector: '{{WOPB}} .wopb-toggle-header' } ],
	},
	toggleHeadBgColor: {
		type: 'string',
		default: '#E6E6E6',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-toggle-header { background:{{toggleHeadBgColor}}; }',
			},
		],
	},
	toggleHeadBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#E6E6E6',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-toggle-header' } ],
	},
	toggleHeadRadius: {
		type: 'object',
		default: { lg: '0', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-toggle-header { border-radius:{{toggleHeadRadius}}; }',
			},
		],
	},
	toggleHeadPadding: {
		type: 'object',
		default: {
			lg: { top: 11, right: 20, bottom: 11, left: 20, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-toggle-header { padding:{{toggleHeadPadding}} }',
			},
		],
	},

	// Coupon Body
	couponTitleText: { type: 'string', default: 'Discount Coupon' },
	titleTextColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-coupon-title { color:{{titleTextColor}}; }',
			},
		],
	},
	titleTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '400',
		},
		style: [ { selector: '{{WOPB}} .wopb-coupon-title' } ],
	},
	titlePosition: {
		type: 'string',
		default: 'withCoupon',
		style: [
			{
				depends: [
					{
						key: 'titlePosition',
						condition: '==',
						value: 'aboveCoupon',
					},
				],
				selector:
					'{{WOPB}} .wopb-coupon-title {margin-bottom:10px; display:inline-block;}',
			},
			{
				depends: [
					{
						key: 'titlePosition',
						condition: '==',
						value: 'withCoupon',
					},
				],
				selector:
					'{{WOPB}} .wopb-coupon-title { display:inline-block; margin-right:15px; }',
			},
		],
	},
	couponBodyBgColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-coupon-form { background:{{couponBodyBgColor}}; }',
			},
		],
	},
	couponSectionBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 0, right: 1, bottom: 1, left: 1 },
			color: '#E6E6E6',
			type: 'solid',
		},
		style: [
			{ selector: '{{WOPB}} .wopb-product-wrapper .wopb-coupon-form' },
		],
	},
	couponBodyPadding: {
		type: 'object',
		default: {
			lg: { top: 20, right: 10, bottom: 20, left: 20, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-coupon-form { padding:{{couponBodyPadding}} }',
			},
		],
	},
	couponBodyRadius: {
		type: 'object',
		default: { lg: '0', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-coupon-form { border-radius:{{couponBodyRadius}}; }',
			},
		],
	},
	// Coupon Input Field
	couponInputTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '400',
		},
		style: [ { selector: '{{WOPB}} .wopb-coupon-body .wopb-coupon-code' } ],
	},
	couponInputWidth: {
		type: 'object',
		default: { lg: '230', unit: 'px' },
		style: [
			{
				selector:
					'.editor-styles-wrapper {{WOPB}} .wopb-coupon-code, {{WOPB}} .wopb-coupon-code { width: {{couponInputWidth}}; }',
			},
		],
	},
	couponInputTextColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-coupon-body .wopb-coupon-code { color:{{couponInputTextColor}}; }',
			},
		],
	},
	couponInputBgColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-coupon-body .wopb-coupon-code { background:{{couponInputBgColor}}; }',
			},
		],
	},
	couponInputPlaceholder: {
		type: 'string',
		default: 'Enter Coupon Code Here.....',
	},
	couponInputBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#D2D2D2',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-coupon-body .wopb-coupon-code' } ],
	},
	couponInputFocusBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#D2D2D2',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-coupon-body > #coupon_code.wopb-coupon-code:focus',
			},
		],
	},
	couponInputRadius: {
		type: 'object',
		default: { lg: '0', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-coupon-code { border-radius:{{couponInputRadius}}; }',
			},
		],
	},
	couponInputPadding: {
		type: 'object',
		default: {
			lg: { top: 10, right: 12, bottom: 10, left: 12, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-coupon-code { padding:{{couponInputPadding}} !important; line-height: normal; box-shadow:none}',
			},
		],
	},
	// Coupon Button
	applyBtnPosition: { type: 'string', default: 'withCoupon' },
	couponBtnText: { type: 'string', default: 'Apply Coupon' },
	couponBtnTextColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-coupon-submit-btn { color:{{couponBtnTextColor}}; }',
			},
		],
	},
	couponBtnTextHoverColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-coupon-submit-btn:hover { color:{{couponBtnTextHoverColor}}; }',
			},
		],
	},
	couponBtnBgColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-coupon-submit-btn { background:{{couponBtnBgColor}}; }',
			},
		],
	},
	couponBtnBgHoverColor: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-coupon-submit-btn:hover { background-color:{{couponBtnBgHoverColor}}; }',
			},
		],
	},
	couponBtnTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: '',
			family: '',
			weight: '500',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-coupon-submit-btn , .editor-styles-wrapper .block-editor-block-list__layout {{WOPB}} .wopb-checkout-coupon-submit-btn',
			},
		],
	},
	couponBtnBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 0, right: 0, bottom: 0, left: 0 },
			color: '#e0e0e0',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-checkout-coupon-submit-btn' } ],
	},
	couponBtnSpace: {
		type: 'string',
		default: '12',
		style: [
			{
				depends: [
					{
						key: 'applyBtnPosition',
						condition: '==',
						value: 'belowCoupon',
					},
				],
				selector:
					'{{WOPB}} .wopb-checkout-coupon-submit-btn { margin-top: {{couponBtnSpace}}px; }',
			},
			{
				depends: [
					{
						key: 'applyBtnPosition',
						condition: '==',
						value: 'withCoupon',
					},
				],
				selector:
					'{{WOPB}} .wopb-checkout-coupon-submit-btn { margin-left: {{couponBtnSpace}}px; }',
			},
		],
	},
	couponBtnRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-coupon-submit-btn { border-radius:{{couponBtnRadius}}; }',
			},
		],
	},
	couponBtnPadding: {
		type: 'object',
		default: {
			lg: { top: 11, right: 20, bottom: 11, left: 20, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-checkout-coupon-submit-btn , .editor-styles-wrapper {{WOPB}} .wopb-checkout-coupon-submit-btn { padding:{{couponBtnPadding}} !important;}',
			},
		],
	},
	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	wrapMargin: {
		type: 'object',
		default: { lg: { top: '', bottom: '20', unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#f5f5f5' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#ff176b' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapInnerPadding: {
		type: 'object',
		default: { lg: { unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ padding:{{wrapInnerPadding}}; }',
			},
		],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: { type: 'string', default: '', style: [ { selector: '' } ] },
};

export default attributes;
