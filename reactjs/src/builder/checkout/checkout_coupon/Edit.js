/** @format */

const { __ } = wp.i18n;
const { useEffect } = wp.element;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../../helper/CssGenerator';
import {
	GeneralAdvanced,
	CustomCssAdvanced,
	CommonSettings,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../../helper/CommonPanel';
import { Sections, Section } from '../../../helper/Sections';
import useFluentSettings from '../../../helper/hooks/useFluentSettings';
import ToolBarElement from '../../../helper/ToolBarElement';
import { AddSettingsToToolbar } from './Settings';
import useBlockId from '../../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: {
			blockId,
			advanceId,
			showToggle,
			toggleText,
			couponTitleText,
			titlePosition,
			couponBtnText,
			couponInputPlaceholder,
			applyBtnPosition,
		},
	} = props;

	const store = { setAttributes, name, attributes, clientId };

	const { section, toolbarSettings, setCurrSettings } = useFluentSettings();

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/checkout-coupon', blockId );
	}

	return (
		<>
			<ToolBarElement
				store={ store }
				include={ [
					{
						type: 'block_spacing',
						label: __( 'Coupon Block Spacing', 'product-blocks' ),
					},
				] }
			/>

			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Settings', 'product-blocks' ) }
					>
						{ /* <CommonSettings
                            initialOpen={true}
                            title={"inline"}
                            store={store}
                            include={[
                                {
                                    data: {
                                        type: "border",
                                        key: "couponSectionBorder",
                                        label: __("Border", "product-blocks"),
                                    },
                                },
                            ]}
                        /> */ }
						<CommonSettings
							depend="showToggle"
							initialOpen={ section[ 'coupon-heading' ] }
							title={ __( 'Coupon Heading', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'text',
										key: 'toggleText',
										label: __( 'Text', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'toggleTextColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'toggleTextHoverColor',
										label: __(
											'Hover Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'toggleTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'toggleHeadBgColor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'toggleHeadBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'toggleHeadRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'toggleHeadPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'coupon-body' ] }
							title={ __( 'Coupon Body', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'text',
										key: 'couponTitleText',
										label: __(
											'Discount Title',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'titleTextColor',
										label: __(
											'Title Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'titleTypo',
										label: __(
											'Title Typo',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'select',
										key: 'titlePosition',
										label: __(
											'Title Position',
											'product-blocks'
										),
										options: [
											{
												value: 'withCoupon',
												label: __(
													'With Coupon Field',
													'product-blocks'
												),
											},
											{
												value: 'aboveCoupon',
												label: __(
													'Above Coupon Field',
													'product-blocks'
												),
											},
										],
									},
								},
								{
									data: {
										type: 'color',
										key: 'couponBodyBgColor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'couponSectionBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'couponBodyRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'couponBodyPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'input-field' ] }
							title={ __( 'Input Field', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'typography',
										key: 'couponInputTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'couponInputWidth',
										min: 180,
										max: 500,
										step: 1,
										responsive: true,
										unit: [ 'px' ],
										label: __( 'Width', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'couponInputTextColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'couponInputBgColor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'text',
										key: 'couponInputPlaceholder',
										label: __(
											'Placeholder',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'couponInputBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'border',
										key: 'couponInputFocusBorder',
										label: __(
											'Focus Border',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'couponInputRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'couponInputPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'coupon-button' ] }
							title={ __( 'Coupon Button', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'select',
										key: 'applyBtnPosition',
										label: __(
											'Position',
											'product-blocks'
										),
										options: [
											{
												value: 'withCoupon',
												label: __(
													'With Coupon Field',
													'product-blocks'
												),
											},
											{
												value: 'belowCoupon',
												label: __(
													'Below Coupon Field',
													'product-blocks'
												),
											},
										],
									},
								},
								{
									data: {
										type: 'text',
										key: 'couponBtnText',
										label: __(
											'Button Text',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'couponBtnTextColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'couponBtnTextHoverColor',
										label: __(
											'Hover Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'couponBtnBgColor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'couponBtnBgHoverColor',
										label: __(
											'Hover Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'couponBtnTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'couponBtnBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'range',
										key: 'couponBtnSpace',
										label: __( 'Space', 'product-blocks' ),
										min: 1,
										max: 30,
										step: 1,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'couponBtnRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'couponBtnPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced initialOpen={ true } store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar
				store={ store }
				selected={ toolbarSettings }
			/>

			<div
				id={ advanceId ? advanceId : '' }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ 'wopb-product-wrapper' }>
					<div className="wopb-coupon-section">
						{ showToggle && (
							<div
								className="wopb-toggle-header wopb-component-simple"
								onClick={ ( e ) =>
									setCurrSettings( e, 'coupon-heading' )
								}
							>
								<span className="wopb-toggle-text">
									{ toggleText }
								</span>
								<button className="wopb-toggle-btn"></button>
							</div>
						) }
						<div className="wopb-coupon-form checkout_coupon woocommerce-form-coupon">
							<div className="wopb-coupon-body">
								<div
									className="wopb-coupon-title wopb-component-simple"
									onClick={ ( e ) =>
										setCurrSettings( e, 'coupon-body' )
									}
								>
									{ couponTitleText }
								</div>
								{ titlePosition == 'aboveCoupon' && <br /> }
								<input
									type="text"
									name="coupon_code"
									className="input-text wopb-coupon-code wopb-component-simple"
									id="coupon_code"
									placeholder={ couponInputPlaceholder }
									onClick={ ( e ) =>
										setCurrSettings( e, 'input-field' )
									}
								/>
								{ applyBtnPosition == 'belowCoupon' && <br /> }
								<button
									type="submit"
									className="button wopb-checkout-coupon-submit-btn wopb-component-simple"
									name="apply_coupon"
									value="Apply coupon"
									onClick={ ( e ) => {
										e.preventDefault();
										setCurrSettings( e, 'coupon-button' );
									} }
								>
									{ couponBtnText }
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
}
