const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';

registerBlockType( 'product-blocks/checkout-coupon', {
	title: __( 'Coupon', 'product-blocks' ),
	icon: (
		<img
			src={
				wopb_data.url + 'assets/img/blocks/builder/checkout_coupon.svg'
			}
			alt={ __( 'Image', 'product-blocks' ) }
			style={ { width: '100%' } }
		/>
	),
	category: 'single-checkout',
	description: __(
		'Display the Coupon field to your desired position and customize it as you need.',
		'product-blocks'
	),
	keywords: [
		__( 'coupon', 'product-blocks' ),
		__( 'checkout', 'product-blocks' ),
		__( 'cart', 'product-blocks' ),
	],
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
