/** @format */

import { settingsIcon } from '../../../helper/CommonPanel';
import WopbToolbarDropdown from '../../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar, getData } from '../../../helper/ux';

const { __ } = wp.i18n;

export function AddSettingsToToolbar( { store, selected } ) {
	switch ( selected ) {
		case 'coupon-heading':
			return (
				<WopbToolbarGroup text={ 'Coupon Heading' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Coupon Heading Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Coupon Heading Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'text',
											key: 'toggleText',
											label: __(
												'Text',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'toggleTextColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'toggleTextHoverColor',
											label: __(
												'Hover Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'toggleTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'toggleHeadBgColor',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'toggleHeadBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'toggleHeadRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'toggleHeadPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'coupon-button':
			return (
				<WopbToolbarGroup text={ 'Coupon Button' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Coupon Button Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Coupon Button Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'select',
											key: 'applyBtnPosition',
											label: __(
												'Position',
												'product-blocks'
											),
											options: [
												{
													value: 'withCoupon',
													label: __(
														'With Coupon Field',
														'product-blocks'
													),
												},
												{
													value: 'belowCoupon',
													label: __(
														'Below Coupon Field',
														'product-blocks'
													),
												},
											],
										},
									},
									{
										data: {
											type: 'text',
											key: 'couponBtnText',
											label: __(
												'Button Text',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'couponBtnTextColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'couponBtnTextHoverColor',
											label: __(
												'Hover Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'couponBtnBgColor',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'couponBtnBgHoverColor',
											label: __(
												'Hover Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'couponBtnTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'couponBtnBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'range',
											key: 'couponBtnSpace',
											label: __(
												'Space',
												'product-blocks'
											),
											min: 1,
											max: 30,
											step: 1,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'couponBtnRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'couponBtnPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'input-field':
			return (
				<WopbToolbarGroup text={ 'Input Field' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Input Field Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Input Field Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'typography',
											key: 'couponInputTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'range',
											key: 'couponInputWidth',
											min: 180,
											max: 500,
											step: 1,
											responsive: true,
											unit: [ 'px' ],
											label: __(
												'Width',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'couponInputTextColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'couponInputBgColor',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'text',
											key: 'couponInputPlaceholder',
											label: __(
												'Placeholder',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'couponInputBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'couponInputFocusBorder',
											label: __(
												'Focus Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'couponInputRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'couponInputPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'coupon-body':
			return (
				<WopbToolbarGroup text={ 'Coupon Body' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Coupon Body Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Coupon Body Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'text',
											key: 'couponTitleText',
											label: __(
												'Discount Title',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'titleTextColor',
											label: __(
												'Title Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'titleTypo',
											label: __(
												'Title Typo',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'select',
											key: 'titlePosition',
											label: __(
												'Title Position',
												'product-blocks'
											),
											options: [
												{
													value: 'withCoupon',
													label: __(
														'With Coupon Field',
														'product-blocks'
													),
												},
												{
													value: 'aboveCoupon',
													label: __(
														'Above Coupon Field',
														'product-blocks'
													),
												},
											],
										},
									},
									{
										data: {
											type: 'color',
											key: 'couponBodyBgColor',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'couponBodyRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'couponBodyPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		// case "":
		//     return (
		//         <WopbToolbarGroup text={"Image"} textScroll>
		//             <WopbToolbarDropdown
		//                 buttonContent={settingsIcon}
		//                 store={store}
		//                 label={__("Product Image Settings", "product-blocks")}
		//                 content={formatSettingsForToolbar([
		//                     {
		//                         name: "tab",
		//                         title: __(
		//                             "Product Image Settings",
		//                             "product-blocks"
		//                         ),
		//                         options: getData(),
		//                     },
		//                 ])}
		//             />
		//         </WopbToolbarGroup>
		//     );
		default:
			return null;
	}
}
