const attributes = {
	blockId: { type: 'string', default: '' },
	currentPostId: { type: 'string', default: '' },
	showTitle: { type: 'boolean', default: true },
	sectionTitle: { type: 'string', default: 'Payment Method' },
	sectionTitleColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-payment-section-title { color:{{sectionTitleColor}}; }',
			},
		],
	},
	sectionTitleBg: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-payment-section-title { background-color:{{sectionTitleBg}}; }',
			},
		],
	},
	headingAlign: {
		type: 'string',
		default: 'left',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-payment-section-title { text-align:{{headingAlign}}; }',
			},
		],
	},
	sectionTitleTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '24', unit: 'px' },
			height: { lg: '32', unit: 'px' },
			decoration: 'none',
			transform: '',
			weight: '600',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-payment-section-title',
			},
		],
	},
	sectionRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-payment-section-title { border-radius:{{sectionRadius}}; }',
			},
		],
	},
	sectionTitlePadding: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-payment-section-title { padding:{{sectionTitlePadding}}; }',
			},
		],
	},
	sectionTitleSpace: {
		type: 'string',
		default: '24',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-payment-section-title { margin-bottom:{{sectionTitleSpace}}px;}',
			},
		],
	},
	labelColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper ul li label { color:{{labelColor}}; }',
			},
		],
	},
	labelBg: {
		type: 'string',
		default: '#E6E6E6',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper #payment ul li label { background-color:{{labelBg}}; }',
			},
		],
	},
	labelTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: '',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper ul li label ' } ],
	},
	checkboxSpace: {
		type: 'string',
		default: '4',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper ul li { padding-bottom:{{checkboxSpace}}px;}',
			},
		],
	},
	labelPadding: {
		type: 'object',
		default: {
			lg: { top: 12, right: 16, bottom: 12, left: 16, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper ul li label { padding:{{labelPadding}} !important; }',
			},
		],
	},
	bodyColor: {
		type: 'string',
		default: '#4A4A4A',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper ul li div p { color:{{bodyColor}}; }',
			},
		],
	},
	bodyBg: {
		type: 'string',
		default: '#f5f5f5',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper #payment ul li div { background-color:{{bodyBg}} !important; }',
			},
		],
	},
	bodyTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '', unit: 'px' },
			decoration: 'none',
			transform: '',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper ul li div ' } ],
	},
	bodyPadding: {
		type: 'object',
		default: {
			lg: { top: 16, right: 20, bottom: 16, left: 20, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper ul li .payment_box { padding:{{bodyPadding}} !important; }',
			},
		],
	},
	btnFullWIdth: {
		type: 'boolean',
		default: true,
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper #payment button#place_order { width: 100% !important; }',
			},
		],
	},
	btnAlign: {
		type: 'string',
		default: 'left',
		style: [
			{
				depends: [
					{ key: 'btnAlign', condition: '==', value: 'left' },
				],
				selector:
					'{{WOPB}} .wopb-product-wrapper #payment button#place_order {display: block; margin-right: auto; } ',
			},
			{
				depends: [
					{ key: 'btnAlign', condition: '==', value: 'right' },
				],
				selector:
					'{{WOPB}} .wopb-product-wrapper #payment button#place_order {display: block; margin-left: auto;}',
			},
			{
				depends: [
					{ key: 'btnAlign', condition: '==', value: 'center' },
				],
				selector:
					'{{WOPB}} .wopb-product-wrapper #payment button#place_order { display: block; margin: 0px auto ;}',
			},
		],
	},
	btnTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper #payment button#place_order',
			},
		],
	},
	btnColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper #payment button#place_order { color:{{btnColor}}; }',
			},
		],
	},
	btnHvrColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper #payment button#place_order:hover { color:{{btnHvrColor}}; }',
			},
		],
	},
	btnBg: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper #payment button { background-color:{{btnBg}}; }',
			},
		],
	},
	btnHvrBg: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper #payment button:hover { background-color:{{btnHvrBg}}; }',
			},
		],
	},
	btnBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#cccccc',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper #payment button#place_order',
			},
		],
	},
	btnRadius: {
		type: 'object',
		default: {
			lg: { top: 30, right: 30, bottom: 30, left: 30, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper #payment button#place_order { border-radius:{{btnRadius}}; }',
			},
		],
	},
	btnPadding: {
		type: 'object',
		default: {
			lg: { top: 14, right: 24, bottom: 14, left: 22, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper #payment button#place_order { padding:{{btnPadding}}; }',
			},
		],
	},
	descpColor: {
		type: 'string',
		default: '#4A4A4A',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-privacy-policy-text { color:{{descpColor}}; }',
			},
		],
	},
	linkColor: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-privacy-policy-link { color:{{linkColor}}; }',
			},
		],
	},
	hoverColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-privacy-policy-text:hover { color:{{hoverColor}}; }',
			},
		],
	},
	descpTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '14', unit: 'px' },
			height: { lg: '', unit: 'px' },
			decoration: 'none',
			transform: '',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-privacy-policy-text',
			},
		],
	},
	descpMargin: {
		type: 'object',
		default: { lg: { top: 20, right: 0, bottom: 24, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-privacy-policy-text { margin:{{descpMargin}}; }',
			},
		],
	},
	containerBg: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-checkout-payment-container { background-color:{{containerBg}}; }',
			},
		],
	},
	containerBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#cccccc',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-checkout-payment-container ',
			},
		],
	},
	containerRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-checkout-payment-container { border-radius:{{containerRadius}}; }',
			},
		],
	},
	containerPadding: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wopb-checkout-payment-container { padding:{{containerPadding}} !important; }',
			},
		],
	},
	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	wrapMargin: {
		type: 'object',
		default: { lg: { top: '', bottom: '20', unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#f5f5f5' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#ff176b' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapInnerPadding: {
		type: 'object',
		default: { lg: { unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ padding:{{wrapInnerPadding}}; }',
			},
		],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: { type: 'string', default: '', style: [ { selector: '' } ] },
};
export default attributes;
