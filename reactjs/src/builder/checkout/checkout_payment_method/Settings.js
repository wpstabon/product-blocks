/** @format */

import { settingsIcon } from '../../../helper/CommonPanel';
import WopbToolbarDropdown from '../../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar, getData } from '../../../helper/ux';

const { __ } = wp.i18n;

export function AddSettingsToToolbar( { store, selected } ) {
	switch ( selected ) {
		case 'section-title':
			return (
				<WopbToolbarGroup text={ 'Section Title' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Section Title Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Section Title Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'text',
											key: 'sectionTitle',
											label: __(
												'Text',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'sectionTitleColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'sectionTitleBg',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'alignment',
											key: 'headingAlign',
											label: __(
												'Alignment',
												'product-blocks'
											),
											disableJustify: true,
										},
									},
									{
										data: {
											type: 'typography',
											key: 'sectionTitleTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'sectionRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'sectionTitlePadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'range',
											key: 'sectionTitleSpace',
											label: __(
												'Space',
												'product-blocks'
											),
											min: 0,
											max: 50,
											step: 1,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'check-label':
			return (
				<WopbToolbarGroup text={ 'Checkbox & Label' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Checkbox & Label Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Checkbox & Label Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'labelColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'labelBg',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'labelTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'range',
											key: 'checkboxSpace',
											label: __(
												'Space between checkbox',
												'product-blocks'
											),
											min: 0,
											max: 50,
											step: 1,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'labelPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'body-content':
			return (
				<WopbToolbarGroup text={ 'Body Content' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Body Content Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Body Content Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'bodyColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'bodyBg',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'bodyTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'bodyPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'description':
			return (
				<WopbToolbarGroup text={ 'Desc.' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Description Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Description Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'descpColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'linkColor',
											label: __(
												'Link Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'hoverColor',
											label: __(
												'Hover Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'descpTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'descpMargin',
											label: __(
												'Margin',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'button':
			return (
				<WopbToolbarGroup text={ 'Button' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Button Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Button Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'toggle',
											key: 'btnFullWIdth',
											label: __(
												'Full width button',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'alignment',
											key: 'btnAlign',
											label: __(
												'Alignment',
												'product-blocks'
											),
											disableJustify: true,
										},
									},
									{
										data: {
											type: 'typography',
											key: 'btnTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'btnColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'btnBg',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'btnBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'btnRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'btnPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'field-container':
			return (
				<WopbToolbarGroup text={ 'Field Container' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Field Container Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Field Container Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'containerBg',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'containerBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'containerRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'containerPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		default:
			return null;
	}
}
