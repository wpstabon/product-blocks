/** @format */

const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../../helper/CssGenerator';
import {
	GeneralAdvanced,
	CustomCssAdvanced,
	CommonSettings,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../../helper/CommonPanel';
import { Sections, Section } from '../../../helper/Sections';
import useFluentSettings from '../../../helper/hooks/useFluentSettings';
import ToolBarElement from '../../../helper/ToolBarElement';
import { AddSettingsToToolbar } from './Settings';
import useBlockId from '../../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: { blockId, advanceId, showTitle, sectionTitle },
	} = props;

	const store = { setAttributes, name, attributes, clientId };

	const { section, toolbarSettings, setCurrSettings } = useFluentSettings();

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator(
			attributes,
			'product-blocks/checkout-payment-method',
			blockId
		);
	}

	return (
		<>
			<ToolBarElement
				store={ store }
				include={ [
					{
						type: 'block_spacing',
						label: __( 'Spacing', 'product-blocks' ),
					},
				] }
			/>

			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Settings', 'product-blocks' ) }
					>
						<CommonSettings
							depend="showTitle"
							initialOpen={ section[ 'section-title' ] }
							title={ __( 'Section Title', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'text',
										key: 'sectionTitle',
										label: __( 'Text', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'sectionTitleColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'sectionTitleBg',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'alignment',
										key: 'headingAlign',
										label: __(
											'Alignment',
											'product-blocks'
										),
										disableJustify: true,
									},
								},
								{
									data: {
										type: 'typography',
										key: 'sectionTitleTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'sectionRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'sectionTitlePadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'range',
										key: 'sectionTitleSpace',
										label: __( 'Space', 'product-blocks' ),
										min: 0,
										max: 50,
										step: 1,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'check-label' ] }
							title={ __( 'Checkbox & Label', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'labelColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'labelBg',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'labelTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'checkboxSpace',
										label: __(
											'Space between checkbox',
											'product-blocks'
										),
										min: 0,
										max: 50,
										step: 1,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'labelPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'body-content' ] }
							title={ __( 'Body content', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'bodyColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'bodyBg',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'bodyTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'bodyPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section.description }
							title={ __( 'Description', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'descpColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'linkColor',
										label: __(
											'Link Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'hoverColor',
										label: __(
											'Hover Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'descpTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'descpMargin',
										label: __( 'Margin', 'product-blocks' ),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section.button }
							title={ __( 'Button', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'toggle',
										key: 'btnFullWIdth',
										label: __(
											'Full width button',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'alignment',
										key: 'btnAlign',
										label: __(
											'Alignment',
											'product-blocks'
										),
										disableJustify: true,
									},
								},
								{
									data: {
										type: 'typography',
										key: 'btnTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'btnColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'btnHvrColor',
										label: __(
											'Hover Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'btnBg',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'btnHvrBg',
										label: __(
											'Hover Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'btnBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'btnRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'btnPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ section[ 'field-container' ] }
							title={ __( 'Field Container', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									data: {
										type: 'color',
										key: 'containerBg',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'containerBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'containerRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'containerPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced initialOpen={ true } store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar
				store={ store }
				selected={ toolbarSettings }
			/>

			<div
				id={ advanceId ? advanceId : '' }
				className={ `wopb-block-${ blockId } ${ className }` }
				onClick={ ( e ) => setCurrSettings( e, 'field-container' ) }
			>
				<div className={ 'wopb-product-wrapper' }>
					<div className="wopb-checkout-payment-container">
						{ showTitle && (
							<h2
								className="wopb-payment-section-title wopb-component-simple"
								onClick={ ( e ) =>
									setCurrSettings( e, 'section-title' )
								}
							>
								{ ' ' }
								{ sectionTitle }
							</h2>
						) }
						<div
							id="payment"
							className="woocommerce-checkout-payment"
						>
							<ul className="wc_payment_methods payment_methods methods">
								<li
									className="wc_payment_method payment_method_bacs wopb-component-simple"
									onClick={ ( e ) =>
										setCurrSettings( e, 'check-label' )
									}
								>
									<input
										id="payment_method_bacs"
										type="radio"
										className="input-radio"
										name="payment_method"
										value="bacs"
										checked
										data-order_button_text=""
									/>
									<label htmlFor="payment_method_bacs">
										{ ' ' }
										Direct bank transfer{ ' ' }
									</label>
									<div
										className="payment_box payment_method_bacs wopb-component-simple"
										onClick={ ( e ) =>
											setCurrSettings( e, 'body-content' )
										}
									>
										<p>
											Make your payment directly into our
											bank account. Please use your Order
											ID as the payment reference. Your
											order will not be shipped until the
											funds have cleared in our account.
										</p>
									</div>
								</li>
								<li
									className="wc_payment_method payment_method_cheque wopb-component-simple"
									onClick={ ( e ) =>
										setCurrSettings( e, 'check-label' )
									}
								>
									<input
										id="payment_method_cheque"
										type="radio"
										className="input-radio"
										name="payment_method"
										value="cheque"
										data-order_button_text=""
									/>
									<label htmlFor="payment_method_cheque">
										{ ' ' }
										Check payments{ ' ' }
									</label>
									<div
										className="payment_box payment_method_cheque"
										style={ { display: 'none' } }
									>
										<p>
											Please send a check to Store Name,
											Store Street, Store Town, Store
											State / County, Store Postcode.
										</p>
									</div>
								</li>
								<li
									className="wc_payment_method payment_method_cod wopb-component-simple"
									onClick={ ( e ) =>
										setCurrSettings( e, 'body-content' )
									}
								>
									<input
										id="payment_method_cod"
										type="radio"
										className="input-radio"
										name="payment_method"
										value="cod"
										data-order_button_text=""
									/>
									<label htmlFor="payment_method_cod">
										{ ' ' }
										Cash on delivery{ ' ' }
									</label>
									<div
										className="payment_box payment_method_cod"
										style={ { display: 'none' } }
									>
										<p>Pay with cash upon delivery.</p>
									</div>
								</li>
							</ul>
							<div className="form-row place-order">
								<noscript>
									Since your browser does not support
									JavaScript, or it is disabled, please ensure
									you click the <em>Update Totals</em> button
									before placing your order. You may be
									charged more than the amount stated above if
									you fail to do so.
									<br />
									<button
										type="submit"
										className="button alt"
										name="woocommerce_checkout_update_totals"
										value="Update totals"
									>
										Update totals
									</button>
								</noscript>
								<div className="woocommerce-terms-and-conditions-wrapper">
									<div className="woocommerce-privacy-policy-text">
										<p
											className="wopb-component-simple"
											onClick={ ( e ) =>
												setCurrSettings(
													e,
													'description'
												)
											}
										>
											Your personal data will be used to
											process your order, support your
											experience throughout this website,
											and for other purposes described in
											our
											<a
												href=""
												className="woocommerce-privacy-policy-link"
												target="_blank"
											>
												{ ' ' }
												privacy policy
											</a>
											.
										</p>
									</div>
								</div>
								<button
									type="submit"
									className="button alt"
									name="woocommerce_checkout_place_order"
									id="place_order"
									value="Place order"
									data-value="Place order"
									onClick={ ( e ) =>
										setCurrSettings(
											e,
											'button',
											null,
											true
										)
									}
								>
									Place order
								</button>
								<input
									type="hidden"
									id="woocommerce-process-checkout-nonce"
									name="woocommerce-process-checkout-nonce"
									value="35e154d60e"
								/>
								<input
									type="hidden"
									name="_wp_http_referer"
									value="/wp_st/?wc-ajax=update_order_review"
								/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
}
