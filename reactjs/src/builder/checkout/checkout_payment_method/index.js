const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';
registerBlockType( 'product-blocks/checkout-payment-method', {
	title: __( 'Payment Method', 'product-blocks' ),
	icon: (
		<img
			src={
				wopb_data.url +
				'assets/img/blocks/builder/checkout_payment_method.svg'
			}
			alt={ __( 'Image', 'product-blocks' ) }
			style={ { width: '100%' } }
		/>
	),
	category: 'single-checkout',
	description: __(
		'Let shoppers choose their desired payment method on the checkout page.',
		'product-blocks'
	),
	keywords: [
		__( 'Payment Method', 'product-blocks' ),
		__( 'Payment', 'product-blocks' ),
		__( 'Method', 'product-blocks' ),
	],
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
