const { __ } = wp.i18n;
const { addQueryArgs } = wp.url;
const { useState, useEffect } = wp.element;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import {
	GeneralAdvanced,
	CustomCssAdvanced,
	ResponsiveAdvanced,
	CommonSettings,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Sections, Section } from '../../helper/Sections';
import { AddSettingsToToolbar } from './Settings';
import useBlockId from '../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: { blockId, advanceId, breadcrumbSeparator, previews },
	} = props;
	const store = { setAttributes, name, attributes, clientId };

	const [ postsList, setPostsList ] = useState( [] );
	const [ data, setData ] = useState( {
		title: 'Sample Product Title',
	} );
	const [ prev, setPrev ] = useState( '' );

	useBlockId( props, true );

	useEffect( () => {
		setPrev( previews );
		fetchProducts();
	}, [] );

	useEffect( () => {
		if ( prev != previews ) {
			fetchProducts();
			setPrev( previews );
		}
	}, [ previews ] );

	function fetchProducts() {
		const query = addQueryArgs( '/wopb/preview', {
			previews,
			wpnonce: wopb_data.security,
			type: 'title',
		} );
		wp.apiFetch( { path: query } ).then( ( obj ) => {
			if ( obj.type == 'data' ) {
				setData( obj.data );
			} else {
				setPostsList( obj.list );
			}
		} );
	}

	if ( blockId ) {
		CssGenerator(
			attributes,
			'product-blocks/product-breadcrumb',
			blockId
		);
	}

	return (
		<>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Style', 'product-blocks' ) }
					>
						<CommonSettings
							initialOpen={ true }
							title={ 'inline' }
							include={ [
								{
									position: 1,
									data: {
										type: 'select',
										key: 'previews',
										label: __(
											'Preview',
											'product-blocks'
										),
										options: postsList || [],
									},
								},
								{
									position: 2,
									data: {
										type: 'alignment',
										key: 'breadcrumbAlignment',
										disableJustify: true,
										responsive: true,
										label: __(
											'Alignment',
											'product-blocks'
										),
									},
								},
								{
									position: 3,
									data: {
										type: 'color',
										key: 'breadcrumbColor',
										label: __(
											'Text Color',
											'product-blocks'
										),
									},
								},
								{
									position: 4,
									data: {
										type: 'color',
										key: 'breadcrumbLinkColor',
										label: __(
											'Link Color',
											'product-blocks'
										),
									},
								},
								{
									position: 5,
									data: {
										type: 'color',
										key: 'bcrumbLinkHoverColor',
										label: __(
											'Link Hover Color',
											'product-blocks'
										),
									},
								},
								{
									position: 6,
									data: {
										type: 'typography',
										key: 'breadcrumbTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									position: 7,
									data: {
										type: 'range',
										min: 0,
										max: 50,
										key: 'breadcrumbSpace',
										label: __( 'Space', 'product-blocks' ),
									},
								},
								{
									position: 8,
									data: {
										type: 'divider',
										key: 'bdcrumbSeparatorlabel',
										label: __(
											'Separator',
											'product-blocks'
										),
									},
								},
								{
									position: 9,
									data: {
										type: 'text',
										key: 'breadcrumbSeparator',
										label: __(
											'Separator Text',
											'product-blocks'
										),
									},
								},
								{
									position: 10,
									data: {
										type: 'color',
										key: 'bcrumbSeparatorColor',
										label: __(
											'Separator color',
											'product-blocks'
										),
									},
								},
								{
									position: 11,
									data: {
										type: 'range',
										min: 0,
										max: 50,
										key: 'bcrumbSeparatorSize',
										label: __(
											'Separator Size',
											'product-blocks'
										),
									},
								},
							] }
							store={ store }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar store={ store } />

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ `wopb-product-wrapper` }>
					<nav className={ `woocommerce-breadcrumb` }>
						<a href="#">Home</a>
						{ breadcrumbSeparator && (
							<span className="breadcrumb-separator">
								{ breadcrumbSeparator }
							</span>
						) }
						<a href="#">Category</a>
						{ breadcrumbSeparator && (
							<span className="breadcrumb-separator">
								{ breadcrumbSeparator }
							</span>
						) }
						{ data.title }
					</nav>
				</div>
			</div>
		</>
	);
}
