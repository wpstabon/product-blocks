const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';
registerBlockType( 'product-blocks/product-breadcrumb', {
	title: __( 'Product Breadcrumb', 'product-blocks' ),
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/builder/breadcrumb.svg' }
			alt={ __( 'Breadcrumb', 'product-blocks' ) }
		/>
	),
	category: 'single-product',
	description: __(
		'Dynamically control position, size, and typography of product breadcrumbs.',
		'product-blocks'
	),
	keywords: [
		__( 'Breadcrumb', 'product-blocks' ),
		__( 'Products Breadcrumb', 'product-blocks' ),
	],
	supports: {
		align: [ 'center', 'wide', 'full' ],
	},
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
