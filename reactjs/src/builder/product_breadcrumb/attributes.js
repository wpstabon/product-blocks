const attributes = {
	blockId: {
		type: 'string',
		default: '',
	},
	currentPostId: { type: 'string', default: '' },
	previews: {
		type: 'string',
		default: '',
	},
	breadcrumbAlignment: {
		type: 'object',
		default: {
			lg: 'left',
		},
		style: [
			{
				selector: '{{WOPB}} {text-align:{{breadcrumbAlignment}}; }',
			},
		],
	},
	breadcrumbColor: {
		type: 'string',
		default: '#6E6E6E',
		style: [
			{
				selector:
					'{{WOPB}} .woocommerce-breadcrumb,.woocommerce {{WOPB}} .woocommerce-breadcrumb{color:{{breadcrumbColor}}; }',
			},
		],
	},
	breadcrumbLinkColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .woocommerce-breadcrumb > a, .editor-styles-wrapper {{WOPB}} .woocommerce-breadcrumb > a{color:{{breadcrumbLinkColor}}}',
			},
		],
	},
	bcrumbLinkHoverColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .woocommerce-breadcrumb a:hover, .woocommerce {{WOPB}} .woocommerce-breadcrumb a:hover{color:{{bcrumbLinkHoverColor}}}',
			},
		],
	},
	breadcrumbTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: {
				lg: '14',
				unit: 'px',
			},
			height: {
				lg: '20',
				unit: 'px',
			},
			decoration: 'none',
			transform: '',
			family: '',
			weight: '500',
		},
		style: [
			{
				selector:
					'{{WOPB}} .woocommerce-breadcrumb a, {{WOPB}} .woocommerce-breadcrumb, .woocommerce {{WOPB}} .woocommerce-breadcrumb',
			},
		],
	},
	bdcrumbSeparatorlabel: {
		type: 'string',
		default: '',
	},
	breadcrumbSpace: {
		type: 'string',
		default: 12,
		style: [
			{
				selector:
					'{{WOPB}} .breadcrumb-separator{margin: 0 {{breadcrumbSpace}}px;}',
			},
		],
	},
	breadcrumbSeparator: {
		type: 'string',
		default: '/',
	},
	bcrumbSeparatorColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .breadcrumb-separator{color:{{bcrumbSeparatorColor}};}',
			},
		],
	},
	bcrumbSeparatorSize: {
		type: 'string',
		default: '12',
		style: [
			{
				selector:
					'{{WOPB}} .breadcrumb-separator{font-size:{{bcrumbSeparatorSize}}px;}',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: {
			openColor: 0,
			type: 'color',
			color: '#f5f5f5',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapRadius: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: {
			openColor: 0,
			type: 'color',
			color: '#ff176b',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapHoverRadius: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapMargin: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '20',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '',
				left: '',
				right: '',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	advanceId: {
		type: 'string',
		default: '',
	},
	advanceZindex: {
		type: 'string',
		default: '',
		style: [
			{
				selector: '{{WOPB}} {z-index:{{advanceZindex}};}',
			},
		],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: {
		type: 'string',
		default: '',
		style: [
			{
				selector: '',
			},
		],
	},
};

export default attributes;
