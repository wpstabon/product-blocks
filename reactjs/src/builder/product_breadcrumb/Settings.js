/** @format */

const { __ } = wp.i18n;
import {
	AlignmentTB,
	SpacingArg,
	TypographyTB,
	colorIcon,
	filterFields,
	spacingIcon,
} from '../../helper/CommonPanel';
import WopbToolbarDropdown from '../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar } from '../../helper/ux';

export function AddSettingsToToolbar( { store } ) {
	return (
		<WopbToolbarGroup text={ 'Product Breadcrumb' }>
			<AlignmentTB
				store={ store }
				attrKey="breadcrumbAlignment"
				label={ __( 'Product Breadcrumb Alignment', 'product-blocks' ) }
				responsive={ true }
			/>
			<TypographyTB
				store={ store }
				attrKey={ 'breadcrumbTypo' }
				label={ __(
					'Product Breadcrumb Typography',
					'product-blocks'
				) }
			/>
			<WopbToolbarDropdown
				buttonContent={ colorIcon }
				store={ store }
				label={ __( 'Product Breadcrumb Color', 'product-blocks' ) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __(
							'Product Breadcrumb Color',
							'product-blocks'
						),
						options: [
							{
								type: 'color',
								key: 'breadcrumbColor',
								label: __( 'Text Color', 'product-blocks' ),
							},
							{
								type: 'color',
								key: 'breadcrumbLinkColor',
								label: __( 'Link Color', 'product-blocks' ),
							},
							{
								type: 'color',
								key: 'bcrumbLinkHoverColor',
								label: __(
									'Link Hover Color',
									'product-blocks'
								),
							},
							{
								type: 'color',
								key: 'bcrumbSeparatorColor',
								label: __(
									'Separator color',
									'product-blocks'
								),
							},
						],
					},
				] ) }
			/>
			<WopbToolbarDropdown
				buttonContent={ spacingIcon }
				store={ store }
				label={ __( 'Product Breadcrumb Spacing', 'product-blocks' ) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __(
							'Product Breadcrumb Spacing',
							'product-blocks'
						),
						options: filterFields(
							[
								{
									data: {
										type: 'range',
										key: 'wrapMargin',
										min: 0,
										max: 100,
										step: 1,
										unit: true,
										responsive: true,
										dimensionKey: 'top',
										label: __(
											'Margin Top',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'wrapMargin',
										min: 0,
										max: 100,
										step: 1,
										unit: true,
										responsive: true,
										dimensionKey: 'bottom',
										label: __(
											'Margin Bottom',
											'product-blocks'
										),
									},
								},
							],
							'__all',
							SpacingArg
						),
					},
				] ) }
			/>
		</WopbToolbarGroup>
	);
}
