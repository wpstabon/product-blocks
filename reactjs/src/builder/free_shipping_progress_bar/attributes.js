const attributes = {
	blockId: { type: 'string', default: '' },
	currentPostId: { type: 'string', default: '' },
	showProgress: { type: 'boolean', default: true },
	progressTop: { type: 'boolean', default: false },
	beforePriceText: { type: 'string', default: 'Add' },
	afterPriceText: {
		type: 'string',
		default: 'to cart and get Free shipping!',
	},
	freeShipText: { type: 'string', default: 'You have free shipping!' },
	headerTextColor: {
		type: 'string',
		default: '#4A4A4A',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-progress-msg { color:{{headerTextColor}}; }',
			},
		],
	},
	priceColor: {
		type: 'string',
		default: '#4bbe18',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-shippingRemainingAmount { color:{{priceColor}}; }',
			},
		],
	},
	bodyBgColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-free-progress-bar-section { background-color:{{bodyBgColor}}; }',
			},
		],
	},
	headerTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '', unit: 'px' },
			decoration: 'none',
			transform: 'inherit',
			family: '',
			weight: 'normal',
		},
		style: [ { selector: '{{WOPB}} .wopb-progress-msg' } ],
	},
	bodyBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#d0d0d0',
			type: 'dashed',
		},
		style: [ { selector: '{{WOPB}} .wopb-free-progress-bar-section' } ],
	},
	bodyRadius: {
		type: 'object',
		default: { lg: '0', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-progress-area , {{WOPB}} .wopb-free-progress-bar-section{ border-radius:{{bodyRadius}}; }',
			},
		],
	},
	bodyPadding: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-free-progress-bar-section { padding:{{bodyPadding}}; }',
			},
		],
	},
	progressBarHeight: {
		type: 'string',
		default: '10',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-progress-area { height:{{progressBarHeight}}px; box-sizing: content-box; }',
			},
		],
	},
	emptyBgColor: {
		type: 'string',
		default: '#E6E6E6',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-progress-area { background-color:{{emptyBgColor}}; }',
			},
		],
	},
	filledBgColor: {
		type: 'string',
		default: '#85D11C',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-progress-bar-filled { background-color:{{filledBgColor}}; height:100%; }',
			},
		],
	},
	progressBarBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 0, right: 0, bottom: 0, left: 0 },
			color: 'black',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-progress-area' } ],
	},
	progressBarRadius: {
		type: 'object',
		default: {
			lg: { top: '6', right: '6', bottom: '6', left: '6', unit: 'px' },
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-progress-area , {{WOPB}} .wopb-progress-bar-filled{ border-radius:{{progressBarRadius}}; }',
			},
		],
	},
	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	wrapMargin: {
		type: 'object',
		default: { lg: { top: '', bottom: '20', unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#f5f5f5' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#ff176b' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapInnerPadding: {
		type: 'object',
		default: { lg: { unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ padding:{{wrapInnerPadding}}; }',
			},
		],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: { type: 'string', default: '', style: [ { selector: '' } ] },
};
export default attributes;
