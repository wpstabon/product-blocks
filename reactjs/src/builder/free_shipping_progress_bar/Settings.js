/** @format */

const { __ } = wp.i18n;
import {
	SpacingArg,
	TypographyTB,
	colorIcon,
	filterFields,
	spacingIcon,
} from '../../helper/CommonPanel';
import WopbToolbarDropdown from '../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar } from '../../helper/ux';

export function AddSettingsToToolbar( { store } ) {
	return (
		<WopbToolbarGroup text={ 'Free Shipping Progress Bar' } textScroll>
			<TypographyTB
				store={ store }
				attrKey={ 'headerTypo' }
				label={ __(
					'Free Shipping Progress Bar Typography',
					'product-blocks'
				) }
			/>
			<WopbToolbarDropdown
				buttonContent={ colorIcon }
				store={ store }
				label={ __(
					'Free Shipping Progress Bar Color',
					'product-blocks'
				) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __(
							'Free Shipping Progress Bar Color',
							'product-blocks'
						),
						options: [
							{
								type: 'color',
								key: 'headerTextColor',
								label: __(
									'Header Text Color',
									'product-blocks'
								),
							},
							{
								type: 'color',
								key: 'priceColor',
								label: __( 'Price Color', 'product-blocks' ),
							},
							{
								type: 'color',
								key: 'bodyBgColor',
								label: __( 'Background', 'product-blocks' ),
							},
							{
								type: 'color',
								key: 'filledBgColor',
								label: __(
									'Progress Bar Filled Background',
									'product-blocks'
								),
							},
							{
								type: 'color',
								key: 'emptyBgColor',
								label: __(
									'Progress Bar Empty Background',
									'product-blocks'
								),
							},
						],
					},
				] ) }
			/>
			<WopbToolbarDropdown
				buttonContent={ spacingIcon }
				store={ store }
				label={ __(
					'Free Shipping Progress Bar Spacing',
					'product-blocks'
				) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __(
							'Free Shipping Progress Bar Spacing',
							'product-blocks'
						),
						options: filterFields(
							[
								{
									data: {
										type: 'range',
										key: 'wrapMargin',
										min: 0,
										max: 100,
										step: 1,
										unit: true,
										responsive: true,
										dimensionKey: 'top',
										label: __(
											'Margin Top',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'wrapMargin',
										min: 0,
										max: 100,
										step: 1,
										unit: true,
										responsive: true,
										dimensionKey: 'bottom',
										label: __(
											'Margin Bottom',
											'product-blocks'
										),
									},
								},
							],
							'__all',
							SpacingArg
						),
					},
				] ) }
			/>
		</WopbToolbarGroup>
	);
}
