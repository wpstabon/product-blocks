const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';

registerBlockType( 'product-blocks/free-shipping-progress-bar', {
	title: __( 'Free Shipping Progress Bar', 'product-blocks' ),
	icon: (
		<img
			src={
				wopb_data.url +
				'assets/img/blocks/freeshipping_progress_bar.svg'
			}
			alt={ __( 'Image', 'product-blocks' ) }
			style={ { width: '100%' } }
		/>
	),
	category: 'single-cart',
	description: __(
		'Dynamically control the position, size, and typography of the free shipping progress bar.',
		'product-blocks'
	),
	keywords: [
		__( 'free shipping progress bar', 'product-blocks' ),
		__( 'progress bar', 'product-blocks' ),
		__( 'shipping', 'product-blocks' ),
		__( 'free shipping', 'product-blocks' ),
	],
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
