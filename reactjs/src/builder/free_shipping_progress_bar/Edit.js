const { __ } = wp.i18n;
const { InspectorControls, RichText } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import {
	GeneralAdvanced,
	CustomCssAdvanced,
	CommonSettings,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Sections, Section } from '../../helper/Sections';
import { AddSettingsToToolbar } from './Settings';
import useBlockId from '../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: {
			blockId,
			advanceId,
			showProgress,
			progressTop,
			beforePriceText,
			afterPriceText,
		},
	} = props;

	const store = { setAttributes, name, attributes, clientId };

	const progressBar = showProgress && (
		<div className="wopb-progress-area">
			<div
				className="wopb-progress-bar-filled"
				style={ { width: '66%' } }
			></div>
		</div>
	);

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator(
			attributes,
			'product-blocks/free-shipping-progress-bar',
			blockId
		);
	}

	return (
		<>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Settings', 'product-blocks' ) }
					>
						<CommonSettings
							initialOpen={ true }
							title={ 'inline' }
							store={ store }
							include={ [
								{
									data: {
										type: 'text',
										key: 'beforePriceText',
										label: __(
											'Before Price Text',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'text',
										key: 'afterPriceText',
										label: __(
											'After Price Text',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'text',
										key: 'freeShipText',
										label: __(
											'Free Shipping Text',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'headerTextColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'priceColor',
										label: __(
											'Price Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'bodyBgColor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'headerTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'bodyBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'bodyRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'bodyPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							depend="showProgress"
							initialOpen={ false }
							title={ __( 'Progress Bar', 'product-blocks' ) }
							store={ store }
							include={ [
								showProgress && {
									data: {
										type: 'toggle',
										key: 'progressTop',
										label: __(
											'Progress Position Top',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'progressBarHeight',
										label: __( 'Height', 'product-blocks' ),
										min: 4,
										max: 30,
										step: 1,
									},
								},
								{
									data: {
										type: 'color',
										key: 'emptyBgColor',
										label: __(
											'Empty Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'filledBgColor',
										label: __(
											'Filled Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'progressBarBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'progressBarRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced initialOpen={ true } store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar store={ store } />

			<div
				id={ advanceId ? advanceId : '' }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ 'wopb-product-wrapper' }>
					<div className="wopb-free-progress-bar-section ">
						{ progressTop && progressBar }
						<div className="wopb-progress-msg">
							<RichText
								tagName={ 'span' }
								onChange={ ( value ) =>
									setAttributes( { beforePriceText: value } )
								}
								value={ beforePriceText }
							/>
							<strong>
								<span className="woocommerce-Price-amount amount wopb-shippingRemainingAmount">
									<span className="woocommerce-Price-currencySymbol">
										{ ' ' }
										$
									</span>
									723.00{ ' ' }
								</span>
							</strong>
							<RichText
								tagName={ 'span' }
								onChange={ ( value ) =>
									setAttributes( { afterPriceText: value } )
								}
								value={ afterPriceText }
							/>
						</div>
						{ ! progressTop && progressBar }
					</div>
				</div>
			</div>
		</>
	);
}
