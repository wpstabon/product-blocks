const { __ } = wp.i18n;
const { addQueryArgs } = wp.url;
const { useState, useEffect } = wp.element;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import {
	CommonSettings,
	GeneralAdvanced,
	CustomCssAdvanced,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Sections, Section } from '../../helper/Sections';
import { AddSettingsToToolbar } from './Settings';
import useBlockId from '../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: { blockId, advanceId, previews },
	} = props;

	const store = { setAttributes, name, attributes, clientId };

	const [ postsList, setPostsList ] = useState( [] );
	const [ data, setData ] = useState( {
		description:
			'Sample description of the products. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque metus nulla, sollicitudin nec risus sed, porttitor iaculis risus. Pellentesque neque nulla, tempus vel sapien non, vehicula dignissim lacus. In hac habitasse platea dictumst. Integer non lectus rutrum, iaculis felis quis, fringilla erat. Aliquam et diam lacus. Integer malesuada ornare condimentum. Vestibulum quis varius quam. Vestibulum magna nisi, faucibus ac orci eget, dapibus convallis massa.',
	} );
	const [ prev, setPrev ] = useState( '' );

	useBlockId( props, true );

	useEffect( () => {
		setPrev( previews );
		fetchProducts();
	}, [] );

	useEffect( () => {
		if ( prev != previews ) {
			fetchProducts();
			setPrev( previews );
		}
	}, [ previews ] );

	function fetchProducts() {
		const query = addQueryArgs( '/wopb/preview', {
			previews,
			wpnonce: wopb_data.security,
			type: 'description',
		} );
		wp.apiFetch( { path: query } ).then( ( obj ) => {
			if ( obj.type == 'data' ) {
				setData( obj.data );
			} else {
				setPostsList( obj.list );
			}
		} );
	}

	if ( blockId ) {
		CssGenerator(
			attributes,
			'product-blocks/product-description',
			blockId
		);
	}

	return (
		<>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Style', 'product-blocks' ) }
					>
						<CommonSettings
							initialOpen={ true }
							title={ 'inline' }
							include={ [
								{
									position: 1,
									data: {
										type: 'select',
										key: 'previews',
										label: __(
											'Preview',
											'product-blocks'
										),
										options: postsList || [],
									},
								},
								{
									position: 3,
									data: {
										type: 'alignment',
										key: 'descAlign',
										responsive: true,
										label: __(
											'Alignment',
											'product-blocks'
										),
									},
								},
								{
									position: 8,
									data: {
										type: 'divider',
										key: 'descDevider',
										label: __(
											'Body Style',
											'product-blocks'
										),
									},
								},
								{
									position: 9,
									data: {
										type: 'color',
										key: 'descColor',
										label: __(
											'Text Color',
											'product-blocks'
										),
									},
								},
								{
									position: 10,
									data: {
										type: 'typography',
										key: 'descTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
							] }
							store={ store }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar store={ store } />

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ `wopb-product-wrapper` }>
					{ data.description }
				</div>
			</div>
		</>
	);
}
