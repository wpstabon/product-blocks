const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';
registerBlockType( 'product-blocks/product-short', {
	title: __( 'Product Short Description', 'product-blocks' ),
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/builder/short_desc.svg' }
			alt={ __( 'Product Short Description', 'product-blocks' ) }
		/>
	),
	category: 'single-product',
	description: __(
		'Dynamically control position, size, and typography of product short descriptions.',
		'product-blocks'
	),
	keywords: [
		__( 'Short', 'product-blocks' ),
		__( 'Product Short Description', 'product-blocks' ),
		__( 'Description', 'product-blocks' ),
	],
	supports: {
		align: [ 'center', 'wide', 'full' ],
	},
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
