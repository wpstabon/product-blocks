const { __ } = wp.i18n;
const { addQueryArgs } = wp.url;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import {
	CommonSettings,
	GeneralAdvanced,
	CustomCssAdvanced,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Sections, Section } from '../../helper/Sections';
import { AddSettingsToToolbar } from './Settings';
import useBlockId from '../../helper/hooks/use-block-id';

const { useState, useEffect } = wp.element;

export default function Edit( props ) {
	const [ postsList, setPostsList ] = useState( [] );
	const [ data, setData ] = useState( {
		short: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla id felis sed sapien laoreet aliquet.',
	} );
	const [ prev, setPrev ] = useState( '' );

	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: { blockId, advanceId, previews },
	} = props;
	const store = { setAttributes, name, attributes, clientId };

	useBlockId( props, true );

	useEffect( () => {
		setPrev( previews );
		fetchProducts();
	}, [] );

	useEffect( () => {
		if ( prev != previews ) {
			fetchProducts();
			setPrev( previews );
		}
	}, [ previews ] );

	function fetchProducts() {
		const query = addQueryArgs( '/wopb/preview', {
			previews,
			wpnonce: wopb_data.security,
			type: 'short',
		} );
		wp.apiFetch( { path: query } ).then( ( obj ) => {
			if ( obj.type == 'data' ) {
				setData( obj.data );
			} else {
				setPostsList( obj.list );
			}
		} );
	}

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/product-short', blockId );
	}

	return (
		<>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Style', 'product-blocks' ) }
					>
						<CommonSettings
							initialOpen={ true }
							title={ 'inline' }
							include={ [
								{
									position: 1,
									data: {
										type: 'select',
										key: 'previews',
										label: __(
											'Preview',
											'product-blocks'
										),
										options: postsList || [],
									},
								},
								{
									position: 2,
									data: {
										type: 'alignment',
										key: 'shortAlign',
										disableJustify: true,
										responsive: true,
										label: __(
											'Alignment',
											'product-blocks'
										),
									},
								},
								{
									position: 3,
									data: {
										type: 'color',
										key: 'shortColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									position: 4,
									data: {
										type: 'typography',
										key: 'shortTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
							] }
							store={ store }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar store={ store } />

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ `wopb-product-wrapper` }>{ data.short }</div>
			</div>
		</>
	);
}
