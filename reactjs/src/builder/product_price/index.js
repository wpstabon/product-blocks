const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';
registerBlockType( 'product-blocks/product-price', {
	title: __( 'Product Price', 'product-blocks' ),
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/builder/pricing.svg' }
			alt={ __( 'Product Price', 'product-blocks' ) }
		/>
	),
	category: 'single-product',
	description: __(
		'Dynamically control position, size, and typography of product prices.',
		'product-blocks'
	),
	keywords: [
		__( 'Price', 'product-blocks' ),
		__( 'Pricing', 'product-blocks' ),
		__( 'Product price', 'product-blocks' ),
	],
	supports: {
		align: [ 'center', 'wide', 'full' ],
	},
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
