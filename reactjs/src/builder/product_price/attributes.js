const attributes = {
	blockId: {
		type: 'string',
		default: '',
	},
	currentPostId: { type: 'string', default: '' },
	previews: {
		type: 'string',
		default: '',
	},
	priceAlign: {
		type: 'string',
		default: 'left',
		style: [
			{
				depends: [
					{
						key: 'priceAlign',
						condition: '==',
						value: 'center',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-wrapper {justify-content:center;}',
			},
			{
				depends: [
					{
						key: 'priceAlign',
						condition: '==',
						value: 'left',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-wrapper {justify-content:flex-start;}',
			},
			{
				depends: [
					{
						key: 'priceAlign',
						condition: '==',
						value: 'right',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-wrapper {justify-content:flex-end;}',
			},
		],
	},
	salesLabel: {
		type: 'boolean',
		default: false,
	},
	salesBadge: {
		type: 'boolean',
		default: false,
	},
	priceTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: {
				lg: '16',
				unit: 'px',
			},
			spacing: {
				lg: '',
				unit: 'px',
			},
			height: {
				lg: '22',
				unit: 'px',
			},
			decoration: 'none',
			transform: '',
			family: '',
			weight: '500',
		},
		style: [
			{
				selector: '{{WOPB}} .woocommerce-Price-amount',
			},
		],
	},
	priceColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector: '{{WOPB}} ins .amount {color:{{priceColor}};}',
			},
		],
	},
	priceSpace: {
		type: 'string',
		default: 8,
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper p.price {display:inline-flex;gap:{{priceSpace}}px;}',
			},
		],
	},
	salesColor: {
		type: 'string',
		default: '#b5b5b5',
		style: [
			{
				selector:
					'{{WOPB}} del .woocommerce-Price-amount, {{WOPB}} .woocommerce-Price-amount, .prev-amount-value{color:{{salesColor}};}',
			},
		],
	},
	salesTextLabel: {
		type: 'string',
		default: 'Price: ',
	},
	salesLabelColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				depends: [
					{
						key: 'salesLabel',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-builder-price-label {color:{{salesLabelColor}}}',
			},
		],
	},
	salesLabelTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: {
				lg: '16',
				unit: 'px',
			},
			spacing: {
				lg: '',
				unit: 'px',
			},
			height: {
				lg: '',
				unit: 'px',
			},
			decoration: 'none',
			transform: 'capitalize',
			family: '',
			weight: '500',
		},
		style: [
			{
				depends: [
					{
						key: 'salesLabel',
						condition: '==',
						value: true,
					},
				],
				selector: '{{WOPB}} .wopb-builder-price-label',
			},
		],
	},
	badgeLabel: {
		type: 'string',
		default: 'OFF',
	},
	badgeColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				depends: [
					{
						key: 'salesBadge',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .woocommerce-discount-badge {color:{{badgeColor}}; width: fit-content; }',
			},
		],
	},
	salesBadgeTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: {
				lg: '12',
				unit: 'px',
			},
			spacing: {
				lg: '',
				unit: 'px',
			},
			height: {
				lg: '20',
				unit: 'px',
			},
			decoration: 'none',
			transform: '',
			family: '',
			weight: '',
		},
		style: [
			{
				depends: [
					{
						key: 'salesBadge',
						condition: '==',
						value: true,
					},
				],
				selector: '{{WOPB}} .woocommerce-discount-badge',
			},
		],
	},
	badgeBg: {
		type: 'string',
		default: '#000',
		style: [
			{
				depends: [
					{
						key: 'salesBadge',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .woocommerce-discount-badge {background:{{badgeBg}}}',
			},
		],
	},
	badgeSpace: {
		type: 'string',
		default: 8,
		style: [
			{
				depends: [
					{
						key: 'salesBadge',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .woocommerce-discount-badge {margin-left:{{badgeSpace}}px;}',
			},
		],
	},
	badgeBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{
						key: 'salesBadge',
						condition: '==',
						value: true,
					},
				],
				selector: '{{WOPB}} .woocommerce-discount-badge ',
			},
		],
	},
	badgeRadius: {
		type: 'object',
		default: {
			lg: {
				top: '2',
				bottom: '2',
				left: '2',
				right: '2',
				unit: 'px',
			},
		},
		style: [
			{
				depends: [
					{
						key: 'salesBadge',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .woocommerce-discount-badge { border-radius:{{badgeRadius}}; }',
			},
		],
	},
	badgePading: {
		type: 'object',
		default: {
			lg: {
				top: '2',
				bottom: '2',
				left: '6',
				right: '6',
				unit: 'px',
			},
		},
		style: [
			{
				depends: [
					{
						key: 'salesBadge',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .woocommerce-discount-badge { padding:{{badgePading}}; }',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: {
			openColor: 0,
			type: 'color',
			color: '#f5f5f5',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapRadius: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: {
			openColor: 0,
			type: 'color',
			color: '#ff176b',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapHoverRadius: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapMargin: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '20',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '',
				left: '',
				right: '',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	advanceId: {
		type: 'string',
		default: '',
	},
	advanceZindex: {
		type: 'string',
		default: '',
		style: [
			{
				selector: '{{WOPB}} {z-index:{{advanceZindex}};}',
			},
		],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: {
		type: 'string',
		default: '',
		style: [
			{
				selector: '',
			},
		],
	},
};
export default attributes;
