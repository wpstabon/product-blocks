/** @format */

const { __ } = wp.i18n;
import {
	AlignmentTB,
	SpacingArg,
	TypographyTB,
	colorIcon,
	filterFields,
	spacingIcon,
} from '../../helper/CommonPanel';
import WopbToolbarDropdown from '../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar } from '../../helper/ux';

export function AddSettingsToToolbar( { store } ) {
	return (
		<WopbToolbarGroup text={ 'Product Price' }>
			<AlignmentTB
				store={ store }
				attrKey="priceAlign"
				label={ __( 'Product Price Alignment', 'product-blocks' ) }
				responsive={ false }
			/>
			<TypographyTB
				store={ store }
				attrKey={ 'priceTypo' }
				label={ __( 'Product Price Typography', 'product-blocks' ) }
			/>
			<WopbToolbarDropdown
				buttonContent={ colorIcon }
				store={ store }
				label={ __( 'Product Price Color', 'product-blocks' ) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __( 'Product Price Color', 'product-blocks' ),
						options: [
							{
								type: 'color',
								key: 'priceColor',
								label: __( 'Text Color', 'product-blocks' ),
							},
							{
								type: 'color',
								key: 'salesColor',
								label: __(
									'Regular Price Color',
									'product-blocks'
								),
							},
						],
					},
				] ) }
			/>
			<WopbToolbarDropdown
				buttonContent={ spacingIcon }
				store={ store }
				label={ __( 'Product Price Spacing', 'product-blocks' ) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __( 'Product Price Spacing', 'product-blocks' ),
						options: filterFields(
							[
								{
									data: {
										type: 'range',
										key: 'wrapMargin',
										min: 0,
										max: 100,
										step: 1,
										unit: true,
										responsive: true,
										dimensionKey: 'top',
										label: __(
											'Margin Top',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'wrapMargin',
										min: 0,
										max: 100,
										step: 1,
										unit: true,
										responsive: true,
										dimensionKey: 'bottom',
										label: __(
											'Margin Bottom',
											'product-blocks'
										),
									},
								},
							],
							'__all',
							SpacingArg
						),
					},
				] ) }
			/>
		</WopbToolbarGroup>
	);
}
