const { __ } = wp.i18n;
const { addQueryArgs } = wp.url;
const { useState, useEffect } = wp.element;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import {
	CommonSettings,
	GeneralAdvanced,
	CustomCssAdvanced,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Sections, Section } from '../../helper/Sections';
import { AddSettingsToToolbar } from './Settings';
import useBlockId from '../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const [ postsList, setPostsList ] = useState( [] );
	const [ data, setData ] = useState( {
		sales_price: 80,
		regular_price: 100,
		percentage: 20,
		symbol: '$',
	} );
	const [ prev, setPrev ] = useState( '' );

	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: {
			blockId,
			advanceId,
			salesLabel,
			salesBadge,
			salesTextLabel,
			badgeLabel,
			previews,
		},
	} = props;
	const store = {
		setAttributes,
		name,
		attributes,
		clientId,
	};

	useBlockId( props, true );

	useEffect( () => {
		setPrev( previews );
		fetchProducts();
	}, [] );

	useEffect( () => {
		if ( prev != previews ) {
			fetchProducts();
			setPrev( previews );
		}
	}, [ previews ] );

	function fetchProducts() {
		const query = addQueryArgs( '/wopb/preview', {
			previews,
			wpnonce: wopb_data.security,
			type: 'price',
		} );
		wp.apiFetch( { path: query } ).then( ( obj ) => {
			if ( obj.type == 'data' ) {
				setData( obj.data );
			} else {
				setPostsList( obj.list );
			}
		} );
	}

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/product-price', blockId );
	}

	return (
		<>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Style', 'product-blocks' ) }
					>
						<CommonSettings
							initialOpen={ true }
							title={ 'inline' }
							store={ store }
							include={ [
								{
									position: 1,
									data: {
										type: 'select',
										key: 'previews',
										label: __(
											'Preview',
											'product-blocks'
										),
										options: postsList || [],
									},
								},
								{
									position: 2,
									data: {
										type: 'alignment',
										key: 'priceAlign',
										disableJustify: true,
										label: __(
											'Alignment',
											'product-blocks'
										),
									},
								},
								{
									position: 3,
									data: {
										type: 'typography',
										key: 'priceTypo',
										label: __(
											'Sale Price Typography',
											'product-blocks'
										),
									},
								},
								{
									position: 4,
									data: {
										type: 'color',
										key: 'priceColor',
										label: __(
											'Sale Price Color',
											'product-blocks'
										),
									},
								},
								{
									position: 5,
									data: {
										type: 'range',
										key: 'priceSpace',
										min: -1,
										max: 50,
										label: __( 'Space', 'product-blocks' ),
									},
								},
								{
									position: 6,
									data: {
										type: 'color',
										key: 'salesColor',
										label: __(
											'Regular Price Color',
											'product-blocks'
										),
									},
								},
							] }
						/>
						<CommonSettings
							depend="salesLabel"
							initialOpen={ false }
							title={ __( 'Label', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									position: 1,
									data: {
										type: 'text',
										key: 'salesTextLabel',
										label: __(
											'Label Text',
											'product-blocks'
										),
									},
								},
								{
									position: 2,
									data: {
										type: 'color',
										key: 'salesLabelColor',
										label: __(
											'Label Color',
											'product-blocks'
										),
									},
								},
								{
									position: 3,
									data: {
										type: 'typography',
										key: 'salesLabelTypo',
										label: __(
											'Label Typography',
											'product-blocks'
										),
									},
								},
							] }
						/>
						<CommonSettings
							depend="salesBadge"
							initialOpen={ false }
							title={ __( 'Discount Badge', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									position: 1,
									data: {
										type: 'text',
										key: 'badgeLabel',
										label: __( 'Off', 'product-blocks' ),
									},
								},
								{
									position: 2,
									data: {
										type: 'typography',
										key: 'salesBadgeTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									position: 3,
									data: {
										type: 'color',
										key: 'badgeColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									position: 4,
									data: {
										type: 'color',
										key: 'badgeBg',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									position: 5,
									data: {
										type: 'range',
										key: 'badgeSpace',
										min: 0,
										max: 50,
										label: __(
											'Left Space',
											'product-blocks'
										),
									},
								},
								{
									position: 6,
									data: {
										type: 'border',
										key: 'badgeBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									position: 7,
									data: {
										type: 'dimension',
										key: 'badgeRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									position: 8,
									data: {
										type: 'dimension',
										key: 'badgePading',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar store={ store } />

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ `wopb-product-wrapper` }>
					{ salesLabel && (
						<span className={ `wopb-builder-price-label` }>
							{ salesTextLabel }
						</span>
					) }
					{ data && data.range_price ? (
						<p
							className={ `price` }
							dangerouslySetInnerHTML={ {
								__html: data.range_price,
							} }
							style={ { display: 'flex', gap: '5px' } }
						/>
					) : (
						<p className={ `price` }>
							{ data.regular_price && data.sales_price ? (
								<del>
									<span
										className={ `woocommerce-Price-amount` }
									>
										<bdi>
											<span
												className={ `woocommerce-Price-currencySymbol` }
												dangerouslySetInnerHTML={ {
													__html: data.symbol,
												} }
											></span>
											{ data.regular_price }
										</bdi>
									</span>
								</del>
							) : (
								<span
									className={ `woocommerce-Price-amount  prev-amount-value` }
								>
									<bdi>
										<span
											className={ `woocommerce-Price-currencySymbol` }
											dangerouslySetInnerHTML={ {
												__html: data.symbol,
											} }
										></span>
										{ data.regular_price }
									</bdi>
								</span>
							) }
							{ data.sales_price && (
								<span className="price-block-space">
									<ins>
										<span
											className={ `woocommerce-Price-amount amount amount-value` }
										>
											<bdi>
												<span
													className={ `woocommerce-Price-currencySymbol` }
													dangerouslySetInnerHTML={ {
														__html: data.symbol,
													} }
												></span>
												{ data.sales_price }
											</bdi>
										</span>
									</ins>
								</span>
							) }
						</p>
					) }
					{ salesBadge && data.percentage != 0 && (
						<span className={ `woocommerce-discount-badge` }>
							{ data.percentage }% { badgeLabel }
						</span>
					) }
				</div>
			</div>
		</>
	);
}
