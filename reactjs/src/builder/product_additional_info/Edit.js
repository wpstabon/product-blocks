const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import {
	GeneralAdvanced,
	CustomCssAdvanced,
	ResponsiveAdvanced,
	CommonSettings,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Sections, Section } from '../../helper/Sections';
import { AddSettingsToToolbar } from './Settings';
import useBlockId from '../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: { blockId, advanceId },
	} = props;
	const store = { setAttributes, name, attributes, clientId };

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator(
			attributes,
			'product-blocks/product-additional-info',
			blockId
		);
	}

	return (
		<>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Style', 'product-blocks' ) }
					>
						<CommonSettings
							initialOpen={ true }
							title={ 'inline' }
							store={ store }
							include={ [
								{
									position: 6,
									data: {
										type: 'dimension',
										key: 'infoPadding',
										label: __(
											'Table Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ false }
							title={ __( 'Label', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									position: 1,
									data: {
										type: 'typography',
										key: 'labelTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									position: 2,
									data: {
										type: 'color',
										key: 'labelColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									position: 3,
									data: {
										type: 'color2',
										key: 'labelBg',
										label: __(
											'Background Color',
											'product-blocks'
										),
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ false }
							title={ __( 'Value', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									position: 1,
									data: {
										type: 'typography',
										key: 'valueTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									position: 2,
									data: {
										type: 'color',
										key: 'valueColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									position: 3,
									data: {
										type: 'color2',
										key: 'valueBg',
										label: __(
											'Background Color',
											'product-blocks'
										),
									},
								},
							] }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar store={ store } />

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ `wopb-product-wrapper` }>
					<table
						className={ `woocommerce-product-attributes shop_attributes` }
					>
						<tbody>
							<tr
								className={ `woocommerce-product-attributes-item woocommerce-product-attributes-item--attribute_weight` }
							>
								<th
									className={ `woocommerce-product-attributes-item__label` }
								>
									Weight
								</th>
								<td
									className={ `woocommerce-product-attributes-item__value` }
								>
									<p>One Kg, Two Kg</p>
								</td>
							</tr>
							<tr
								className={ `woocommerce-product-attributes-item woocommerce-product-attributes-item--attribute_pa_color` }
							>
								<th
									className={ `woocommerce-product-attributes-item__label` }
								>
									color
								</th>
								<td
									className={ `woocommerce-product-attributes-item__value` }
								>
									<p>Red, Yellow</p>
								</td>
							</tr>
							<tr
								className={ `woocommerce-product-attributes-item woocommerce-product-attributes-item--attribute_pa_size` }
							>
								<th
									className={ `woocommerce-product-attributes-item__label` }
								>
									size
								</th>
								<td
									className={ `woocommerce-product-attributes-item__value` }
								>
									<p>Large, Medium, Small</p>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</>
	);
}
