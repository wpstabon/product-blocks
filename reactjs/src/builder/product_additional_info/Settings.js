/** @format */

const { __ } = wp.i18n;
import {
	AlignmentTB,
	SpacingArg,
	TypographyTB,
	colorIcon,
	filterFields,
	spacingIcon,
	typoIcon,
} from '../../helper/CommonPanel';
import WopbToolbarDropdown from '../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar } from '../../helper/ux';

export function AddSettingsToToolbar( { store } ) {
	return (
		<WopbToolbarGroup text={ 'Additional Info' }>
			<WopbToolbarDropdown
				buttonContent={ typoIcon }
				store={ store }
				label={ __( 'Additional Info Typography', 'product-blocks' ) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __(
							'Additional Info Typography',
							'product-blocks'
						),
						options: [
							{
								type: 'typography',
								key: 'labelTypo',
								label: __(
									'Label Typography',
									'product-blocks'
								),
							},
							{
								type: 'typography',
								key: 'valueTypo',
								label: __(
									'Value Typography',
									'product-blocks'
								),
							},
						],
					},
				] ) }
			/>
			<WopbToolbarDropdown
				buttonContent={ colorIcon }
				store={ store }
				label={ __( 'Additional Info Color', 'product-blocks' ) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __( 'Additional Info Color', 'product-blocks' ),
						options: [
							{
								type: 'color',
								key: 'labelColor',
								label: __( 'Label Color', 'product-blocks' ),
							},
							{
								type: 'color2',
								key: 'labelBg',
								label: __(
									'Label Background Color',
									'product-blocks'
								),
							},
							{
								type: 'color',
								key: 'valueColor',
								label: __( 'Value Color', 'product-blocks' ),
							},
							{
								type: 'color2',
								key: 'valueBg',
								label: __(
									'Value Background Color',
									'product-blocks'
								),
							},
						],
					},
				] ) }
			/>
			<WopbToolbarDropdown
				buttonContent={ spacingIcon }
				store={ store }
				label={ __( 'Additional Info Spacing', 'product-blocks' ) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __(
							'Additional Info Spacing',
							'product-blocks'
						),
						options: filterFields(
							[
								{
									data: {
										type: 'range',
										key: 'wrapMargin',
										min: 0,
										max: 100,
										step: 1,
										unit: true,
										responsive: true,
										dimensionKey: 'top',
										label: __(
											'Margin Top',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'wrapMargin',
										min: 0,
										max: 100,
										step: 1,
										unit: true,
										responsive: true,
										dimensionKey: 'bottom',
										label: __(
											'Margin Bottom',
											'product-blocks'
										),
									},
								},
							],
							'__all',
							SpacingArg
						),
					},
				] ) }
			/>
		</WopbToolbarGroup>
	);
}
