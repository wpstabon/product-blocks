const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';
registerBlockType( 'product-blocks/product-rating', {
	title: __( 'Product Rating', 'product-blocks' ),
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/builder/rating.svg' }
			alt={ __( 'Product Rating', 'product-blocks' ) }
		/>
	),
	category: 'single-product',
	description: __(
		'Dynamically control position, size, and typography of product ratings.',
		'product-blocks'
	),
	keywords: [
		__( 'Product Rating', 'product-blocks' ),
		__( 'Rating', 'product-blocks' ),
	],
	supports: {
		align: [ 'center', 'wide', 'full' ],
	},
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
