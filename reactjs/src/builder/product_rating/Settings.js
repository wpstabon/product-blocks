/** @format */

const { __ } = wp.i18n;
import {
	AlignmentTB,
	SpacingArg,
	TypographyTB,
	colorIcon,
	filterFields,
	spacingIcon,
} from '../../helper/CommonPanel';
import WopbToolbarDropdown from '../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar } from '../../helper/ux';

export function AddSettingsToToolbar( { store } ) {
	return (
		<WopbToolbarGroup text={ 'Product Rating' }>
			<AlignmentTB
				store={ store }
				attrKey="ratingAlignment"
				label={ __( 'Product Rating Alignment', 'product-blocks' ) }
				responsive={ false }
			/>
			<TypographyTB
				store={ store }
				attrKey={ 'TextLabelTypo' }
				label={ __( 'Product Rating Typography', 'product-blocks' ) }
			/>
			<WopbToolbarDropdown
				buttonContent={ colorIcon }
				store={ store }
				label={ __( 'Product Rating Color', 'product-blocks' ) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __( 'Product Rating Color', 'product-blocks' ),
						options: [
							{
								type: 'color',
								key: 'starBgColor',
								label: __(
									'Star Background Color',
									'product-blocks'
								),
							},
							{
								type: 'color',
								key: 'emptyStarColor',
								label: __(
									'Empty Star Color',
									'product-blocks'
								),
							},
						],
					},
				] ) }
			/>
			<WopbToolbarDropdown
				buttonContent={ spacingIcon }
				store={ store }
				label={ __( 'Product Rating Spacing', 'product-blocks' ) }
				content={ formatSettingsForToolbar( [
					{
						name: 'tab',
						title: __( 'Product Rating Spacing', 'product-blocks' ),
						options: filterFields(
							[
								{
									data: {
										type: 'range',
										key: 'wrapMargin',
										min: 0,
										max: 100,
										step: 1,
										unit: true,
										responsive: true,
										dimensionKey: 'top',
										label: __(
											'Margin Top',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'range',
										key: 'wrapMargin',
										min: 0,
										max: 100,
										step: 1,
										unit: true,
										responsive: true,
										dimensionKey: 'bottom',
										label: __(
											'Margin Bottom',
											'product-blocks'
										),
									},
								},
							],
							'__all',
							SpacingArg
						),
					},
				] ) }
			/>
		</WopbToolbarGroup>
	);
}
