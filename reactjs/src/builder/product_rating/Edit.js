const { __ } = wp.i18n;
const { addQueryArgs } = wp.url;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import {
	CommonSettings,
	GeneralAdvanced,
	CustomCssAdvanced,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Sections, Section } from '../../helper/Sections';
import { AddSettingsToToolbar } from './Settings';
import useBlockId from '../../helper/hooks/use-block-id';

const { useState, useEffect } = wp.element;

export default function Edit( props ) {
	const [ postsList, setPostsList ] = useState( [] );
	const [ data, setData ] = useState( {
		sales: 7,
		rating: 3,
		average: '80%',
	} );
	const [ prev, setPrev ] = useState( '' );

	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: { blockId, advanceId, previews },
	} = props;
	const store = { setAttributes, name, attributes, clientId };

	useEffect( () => {
		setPrev( previews );
		fetchProducts();
	}, [] );

	useEffect( () => {
		if ( prev != previews ) {
			fetchProducts();
			setPrev( previews );
		}
	}, [ previews ] );

	useBlockId( props, true );

	function fetchProducts() {
		const query = addQueryArgs( '/wopb/preview', {
			previews,
			wpnonce: wopb_data.security,
			type: 'rating',
		} );
		wp.apiFetch( { path: query } ).then( ( obj ) => {
			if ( obj.type == 'data' ) {
				setData( obj.data );
			} else {
				setPostsList( obj.list );
			}
		} );
	}

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/product-rating', blockId );
	}

	return (
		<>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Style', 'product-blocks' ) }
					>
						<CommonSettings
							initialOpen={ true }
							title={ 'inline' }
							store={ store }
							include={ [
								{
									position: 1,
									data: {
										type: 'select',
										key: 'previews',
										label: __(
											'Preview',
											'product-blocks'
										),
										options: postsList || [],
									},
								},
								{
									position: 2,
									data: {
										type: 'alignment',
										key: 'ratingAlignment',
										disableJustify: true,
										label: __(
											'Alignment',
											'product-blocks'
										),
									},
								},
								{
									position: 3,
									data: {
										type: 'color',
										key: 'starBgColor',
										label: __(
											'Star Background Color',
											'product-blocks'
										),
									},
								},
								{
									position: 5,
									data: {
										type: 'color',
										key: 'emptyStarColor',
										label: __(
											'Empty Star Color',
											'product-blocks'
										),
									},
								},
								{
									position: 5,
									data: {
										type: 'range',
										key: 'starSize',
										min: -1,
										max: 50,
										label: __(
											'Star Size',
											'product-blocks'
										),
									},
								},
							] }
						/>
						<CommonSettings
							initialOpen={ false }
							title={ __( 'Label', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									position: 1,
									data: {
										type: 'typography',
										key: 'TextLabelTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									position: 2,
									data: {
										type: 'color',
										key: 'ratingLabeColor',
										label: __(
											'Label Color',
											'product-blocks'
										),
									},
								},
								{
									position: 3,
									data: {
										type: 'color',
										key: 'TextHoverColor',
										label: __(
											'Text Hover Color',
											'product-blocks'
										),
									},
								},
							] }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar store={ store } />

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ `wopb-product-wrapper` }>
					<div className={ `woocommerce-product-rating` }>
						<div className={ `star-rating` }>
							<span style={ { width: data.average } }>
								Rated{ ' ' }
								<strong className={ `rating` }>3.00</strong> out
								of 5 based on{ ' ' }
								<span className={ `rating` }>2</span> customer
								ratings
							</span>
						</div>
						<a
							href="#reviews"
							className={ `woocommerce-review-link` }
						>
							(<span className={ `count` }>{ data.rating }</span>{ ' ' }
							customer reviews)
						</a>
					</div>
				</div>
			</div>
		</>
	);
}
