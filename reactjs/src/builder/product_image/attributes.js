const attributes = {
	blockId: {
		type: 'string',
		default: '',
	},
	currentPostId: { type: 'string', default: '' },
	previews: {
		type: 'string',
		default: '',
	},
	showGallery: {
		type: 'boolean',
		default: true,
		style: [
			{
				depends: [
					{
						key: 'showGallery',
						condition: '==',
						value: true,
					},
				],
				selector: '{{WOPB}} .thumb-image {display:block;}',
			},
		],
	},
	showSale: {
		type: 'boolean',
		default: true,
	},
	showlightBox: {
		type: 'boolean',
		default: true,
	},
	arrowLargeImage: {
		type: 'boolean',
		default: true,
		style: [
			{
				selector:
					'{{WOPB}} .woocommerce-product-gallery__wrapper .slick-arrow > svg {display:block;}',
			},
		],
	},
	arrowGalleryImage: {
		type: 'boolean',
		default: true,
		style: [
			{
				depends: [
					{
						key: 'arrowGalleryImage',
						condition: '==',
						value: true,
					},
				],
				selector: '{{WOPB}} {display:block;}',
			},
		],
	},
	imageView: {
		type: 'string',
		default: 'onclick',
	},
	imageHeight: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .slick-slide img , .woocommerce {{WOPB}} .wopb-product-gallery-wrapper .slick-slide img { max-height :{{imageHeight}}; height :{{imageHeight}};} {{WOPB}} .wopb-product-gallery-left .woocommerce-product-gallery__wrapper .wopb-builder-slider-nav , {{WOPB}} .wopb-product-gallery-right .woocommerce-product-gallery__wrapper .wopb-builder-slider-nav { height :{{imageHeight}};} ',
			},
		],
	},
	imageScale: {
		type: 'string',
		default: 'cover',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .slick-slide img {object-fit: {{imageScale}};}',
			},
		],
	},
	imageBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-gallery-wrapper img',
			},
		],
	},
	imageRadius: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '',
				left: '',
				right: '',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper img { border-radius:{{imageRadius}};}',
			},
		],
	},
	galleryPosition: {
		type: 'string',
		default: 'bottom',
	},
	galleryColumns: {
		type: 'object',
		default: {
			lg: '4',
			sm: '2',
			xs: '2',
		},
		style: [
			{
				selector:
					'{{WOPB}} .flex-control-nav { grid-template-columns: repeat({{galleryColumns}}, 1fr); }',
			},
		],
	},
	gallerycolumnGap: {
		type: 'object',
		default: {
			lg: '10',
			unit: 'px',
		},
		style: [
			{
				depends: [
					{
						key: 'galleryPosition',
						condition: '==',
						value: 'top',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-top .woocommerce-product-gallery__wrapper .wopb-builder-slider-nav .slick-track { display: flex;gap: {{gallerycolumnGap}};}',
			},
			{
				depends: [
					{
						key: 'galleryPosition',
						condition: '==',
						value: 'bottom',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-bottom .woocommerce-product-gallery__wrapper .wopb-builder-slider-nav .slick-track { display: flex;gap: {{gallerycolumnGap}};}',
			},
			{
				depends: [
					{
						key: 'galleryPosition',
						condition: '==',
						value: 'left',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-left .woocommerce-product-gallery__wrapper .wopb-builder-slider-nav .slick-slide > div { margin-bottom: {{gallerycolumnGap}};}',
			},
			{
				depends: [
					{
						key: 'galleryPosition',
						condition: '==',
						value: 'right',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-right .woocommerce-product-gallery__wrapper .wopb-builder-slider-nav .slick-slide > div { margin-bottom: {{gallerycolumnGap}};}',
			},
		],
	},
	gallerySpace: {
		type: 'object',
		default: {
			lg: '20',
			unit: 'px',
		},
		style: [
			{
				depends: [
					{
						key: 'galleryPosition',
						condition: '==',
						value: 'left',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-left .woocommerce-product-gallery__wrapper .wopb-builder-slider-nav {padding-right:{{gallerySpace}};} {{WOPB}} .wopb-product-gallery-left .woocommerce-product-gallery__wrapper .wopb-builder-slider-nav .slick-arrow {width: calc(100% - {{gallerySpace}});}',
			},
			{
				depends: [
					{
						key: 'galleryPosition',
						condition: '==',
						value: 'right',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-right .woocommerce-product-gallery__wrapper .wopb-builder-slider-nav {padding-left:{{gallerySpace}};} {{WOPB}} .wopb-product-gallery-right .woocommerce-product-gallery__wrapper .wopb-builder-slider-nav .slick-arrow {width: calc(100% - {{gallerySpace}});}',
			},
			{
				depends: [
					{
						key: 'galleryPosition',
						condition: '==',
						value: 'top',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-top .woocommerce-product-gallery__wrapper .wopb-builder-slider-nav {padding-bottom: {{gallerySpace}};} {{WOPB}} .wopb-product-gallery-top .woocommerce-product-gallery__wrapper .wopb-builder-slider-nav .slick-arrow {height: calc(100% - {{gallerySpace}});}',
			},
			{
				depends: [
					{
						key: 'galleryPosition',
						condition: '==',
						value: 'bottom',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-bottom .woocommerce-product-gallery__wrapper .wopb-builder-slider-nav {padding-top: {{gallerySpace}};}',
			},
		],
	},
	saleText: {
		type: 'string',
		default: 'Sale!',
	},
	salePosition: {
		type: 'string',
		default: 'topLeft',
		style: [
			{
				depends: [
					{
						key: 'salePosition',
						condition: '==',
						value: 'topLeft',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-sale-tag{top:0;left:0;}',
			},
			{
				depends: [
					{
						key: 'salePosition',
						condition: '==',
						value: 'topRight',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-sale-tag{top:0;right:0;}',
			},
			{
				depends: [
					{
						key: 'salePosition',
						condition: '==',
						value: 'buttonLeft',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-sale-tag{bottom:0;left:0;}',
			},
			{
				depends: [
					{
						key: 'salePosition',
						condition: '==',
						value: 'buttonRight',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-sale-tag{bottom:0;right:0;}',
			},
		],
	},
	saleDesign: {
		type: 'string',
		default: 'text',
	},
	saleStyle: {
		type: 'string',
		default: 'classic',
		style: [
			{
				depends: [
					{
						key: 'saleStyle',
						condition: '==',
						value: 'classic',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-sale-tag::before{display:none;} .wopb-product-gallery-sale-tag{width:auto;height:auto;}',
			},
			{
				depends: [
					{
						key: 'saleStyle',
						condition: '==',
						value: 'ribbon',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-sale-tag{ width:65px; height:40px; }',
			},
		],
	},
	salesColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-gallery-sale-tag { color:{{salesColor}}; }',
			},
		],
	},
	salesBgColor: {
		type: 'string',
		default: '#31b54e',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-gallery-sale-tag { background:{{salesBgColor}}; } {{WOPB}} .wopb-product-gallery-sale-tag::before { border-left: 32px solid {{salesBgColor}}; border-right: 33px solid {{salesBgColor}}; }',
			},
		],
	},
	salesTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: {
				lg: '12',
				unit: 'px',
			},
			spacing: {
				lg: '0',
				unit: 'px',
			},
			height: {
				lg: '16',
				unit: 'px',
			},
			decoration: 'none',
			transform: 'uppercase',
			family: '',
			weight: '600',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-gallery-sale-tag',
			},
		],
	},
	salesPadding: {
		type: 'object',
		default: {
			lg: {
				top: 6,
				bottom: 6,
				left: 10,
				right: 10,
				unit: 'px',
			},
		},
		style: [
			{
				depends: [
					{
						key: 'saleStyle',
						condition: '==',
						value: 'classic',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-sale-tag { padding:{{salesPadding}}; }',
			},
		],
	},
	salesRadius: {
		type: 'object',
		default: {
			lg: '02',
			unit: 'px',
		},
		style: [
			{
				depends: [
					{
						key: 'saleStyle',
						condition: '==',
						value: 'classic',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-sale-tag { border-radius:{{salesRadius}}; }',
			},
		],
	},
	salesMargin: {
		type: 'object',
		default: {
			lg: {
				top: 12,
				bottom: 8,
				left: 12,
				right: 8,
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-gallery-sale-tag { margin:{{salesMargin}}; }',
			},
		],
	},
	hoverZoom: {
		type: 'boolean',
		default: true,
	},
	iconPosition: {
		type: 'string',
		default: 'topRight',
		style: [
			{
				depends: [
					{
						key: 'iconPosition',
						condition: '==',
						value: 'topRight',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .wopb-product-zoom {top:10px;right:10px;}',
			},
			{
				depends: [
					{
						key: 'iconPosition',
						condition: '==',
						value: 'topLeft',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .wopb-product-zoom{top:10px;left:10px;}',
			},
			{
				depends: [
					{
						key: 'iconPosition',
						condition: '==',
						value: 'buttonLeft',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .wopb-product-zoom{top:auto ; bottom:10px; left:10px;}',
			},
			{
				depends: [
					{
						key: 'iconPosition',
						condition: '==',
						value: 'buttonRight',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .wopb-product-zoom{top:auto; bottom:10px; right:10px;}',
			},
		],
	},
	zoomIconSize: {
		type: 'object',
		default: {
			lg: '20',
			unit: 'px',
		},
		style: [
			{
				depends: [
					{
						key: 'hoverZoom',
						condition: '==',
						value: false,
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .wopb-product-zoom >svg { width:{{zoomIconSize}}; } ' +
					'{{WOPB}} .wopb-product-zoom-wrapper .zoomImg {display:none !important;}',
			},
			{
				depends: [
					{
						key: 'hoverZoom',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .wopb-product-zoom >svg { width:{{zoomIconSize}}; }',
			},
		],
	},
	iconColor: {
		type: 'string',
		default: '#000',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .wopb-product-zoom >svg{fill:{{iconColor}}}',
			},
		],
	},
	iconHoverColor: {
		type: 'string',
		default: '#000',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .wopb-product-zoom:hover >svg{fill:{{iconHoverColor}}}',
			},
		],
	},
	iconBg: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .wopb-product-zoom{background-color:{{iconBg}}}',
			},
		],
	},
	iconHoverBg: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .wopb-product-zoom:hover{background-color:{{iconHoverBg}}}',
			},
		],
	},
	iconBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .wopb-product-zoom',
			},
		],
	},
	iconHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .wopb-product-zoom:hover',
			},
		],
	},
	iconRadius: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '',
				left: '',
				right: '',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .wopb-product-zoom{ border-radius:{{iconRadius}}; }',
			},
		],
	},
	iconHoverRadius: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '',
				left: '',
				right: '',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .wopb-product-zoom:hover{ border-radius:{{iconHoverRadius}}; }',
			},
		],
	},
	iconPadding: {
		type: 'object',
		default: {
			lg: {
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .wopb-product-zoom {padding:{{iconPadding}};}',
			},
		],
	},
	iconMargin: {
		type: 'object',
		default: {
			lg: {
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .wopb-product-zoom{margin:{{iconMargin}};}',
			},
		],
	},
	arrowSize: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				depends: [
					{
						key: 'arrowLargeImage',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .slick-arrow svg { width:{{arrowSize}}; }',
			},
		],
	},
	arrowWidth: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				depends: [
					{
						key: 'arrowLargeImage',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .slick-arrow { width:{{arrowWidth}}; }',
			},
		],
	},
	arrowHeight: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				depends: [
					{
						key: 'arrowLargeImage',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .slick-arrow { height:{{arrowHeight}}; } {{WOPB}} .wopb-product-gallery-wrapper .slick-arrow { line-height:{{arrowHeight}}; }',
			},
		],
	},
	arrowVartical: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				depends: [
					{
						key: 'arrowLargeImage',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-slick-next-large ,.wopb-product-gallery-wrapper .slick-next { right:{{arrowVartical}}; } {{WOPB}} .wopb-slick-prev-large , .wopb-product-gallery-wrapper .slick-prev{ left:{{arrowVartical}}; }',
			},
		],
	},
	arrowHorizontal: {
		type: 'object',
		default: {
			lg: '',
		},
		style: [
			{
				depends: [
					{
						key: 'arrowLargeImage',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .slick-arrow { top:{{arrowHorizontal}}; }',
			},
		],
	},
	arrowColor: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [
					{
						key: 'arrowLargeImage',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .slick-arrow svg { fill:{{arrowColor}}; }',
			},
		],
	},
	arrowHoverColor: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [
					{
						key: 'arrowLargeImage',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .slick-arrow:hover svg { fill:{{arrowHoverColor}}; }',
			},
		],
	},
	arrowBg: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [
					{
						key: 'arrowLargeImage',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .slick-arrow { background:{{arrowBg}}; }',
			},
		],
	},
	arrowHoverBg: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [
					{
						key: 'arrowLargeImage',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .slick-arrow:hover { background:{{arrowHoverBg}}; }',
			},
		],
	},
	arrowBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{
						key: 'arrowLargeImage',
						condition: '==',
						value: true,
					},
				],
				selector: '{{WOPB}} .wopb-product-gallery-wrapper .slick-arrow',
			},
		],
	},
	arrowHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				depends: [
					{
						key: 'arrowLargeImage',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .slick-arrow:hover',
			},
		],
	},
	arrowRadius: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '',
				left: '',
				right: '',
				unit: 'px',
			},
		},
		style: [
			{
				depends: [
					{
						key: 'arrowLargeImage',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .slick-arrow { border-radius:{{arrowRadius}}; }',
			},
		],
	},
	arrowHoverRadius: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '',
				left: '',
				right: '',
				unit: 'px',
			},
		},
		style: [
			{
				depends: [
					{
						key: 'arrowLargeImage',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .slick-arrow:hover{border-radius:{{arrowHoverRadius}};}',
			},
		],
	},
	arrowShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				depends: [
					{
						key: 'arrowLargeImage',
						condition: '==',
						value: true,
					},
				],
				selector: '{{WOPB}} .wopb-product-gallery-wrapper .slick-arrow',
			},
		],
	},
	arrowHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				depends: [
					{
						key: 'arrowLargeImage',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-product-gallery-wrapper .slick-arrow:hover',
			},
		],
	},
	arrowGallerySize: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .thumb-image .slick-arrow > svg{ width:{{arrowGallerySize}};}',
			},
		],
	},
	arrowGalleryColor: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [
					{
						key: 'arrowGalleryImage',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-builder-slider-nav .slick-arrow svg { fill:{{arrowGalleryColor}}; }',
			},
		],
	},
	arrowGalleryHoverColor: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [
					{
						key: 'arrowGalleryImage',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-builder-slider-nav .slick-arrow:hover svg { fill:{{arrowGalleryHoverColor}}; }',
			},
		],
	},
	arrowGalleryBg: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [
					{
						key: 'arrowGalleryImage',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-product-gallery__wrapper .wopb-builder-slider-nav .slick-arrow { background:{{arrowGalleryBg}}; }',
			},
		],
	},
	arrowGalleryHoverBg: {
		type: 'string',
		default: '',
		style: [
			{
				depends: [
					{
						key: 'arrowGalleryImage',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-product-gallery__wrapper .wopb-builder-slider-nav .slick-arrow:hover { background:{{arrowGalleryHoverBg}}; }',
			},
		],
	},
	arrowGalleryPadding: {
		type: 'object',
		default: {
			lg: {
				unit: 'px',
			},
		},
		style: [
			{
				depends: [
					{
						key: 'arrowGalleryImage',
						condition: '==',
						value: true,
					},
				],
				selector:
					'{{WOPB}} .wopb-product-wrapper .woocommerce-product-gallery__wrapper .wopb-builder-slider-nav .slick-arrow { padding:{{arrowGalleryPadding}}; }',
			},
		],
	},
	wrapBg: {
		type: 'string',
		default: {
			openColor: 0,
			type: 'color',
			color: '#f5f5f5',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapRadius: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper { border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: {
			openColor: 0,
			type: 'color',
			color: '#ff176b',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapHoverRadius: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapMargin: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '20',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper { margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '',
				left: '',
				right: '',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper { padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	wrapInnerPadding: {
		type: 'object',
		default: {
			lg: {
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper { padding:{{wrapInnerPadding}}; }',
			},
		],
	},
	advanceId: {
		type: 'string',
		default: '',
	},
	advanceZindex: {
		type: 'string',
		default: '',
		style: [
			{
				selector: '{{WOPB}} {z-index:{{advanceZindex}};}',
			},
		],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: {
		type: 'string',
		default: '',
		style: [
			{
				selector: '',
			},
		],
	},
};

export default attributes;
