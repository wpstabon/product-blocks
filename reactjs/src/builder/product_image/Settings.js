/** @format */

const { __ } = wp.i18n;
import {
	ArrowStyleArg,
	filterFields,
	SalesStyleArg,
	settingsIcon,
} from '../../helper/CommonPanel';
import WopbToolbarDropdown from '../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar, getData } from '../../helper/ux';

export function AddSettingsToToolbar( { store, selected, postsList } ) {
	switch ( selected ) {
		case 'general':
			return (
				<WopbToolbarGroup text={ 'General' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Title Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Title Settings', 'product-blocks' ),
								options: getData( [
									{
										position: 1,
										data: {
											type: 'select',
											key: 'previews',
											label: __(
												'Preview',
												'product-blocks'
											),
											options: postsList || [],
										},
									},
									{
										position: 2,
										data: {
											type: 'group',
											key: 'imageView',
											label: __(
												'Image View',
												'product-blocks'
											),
											justify: true,
											disabled: true,
											options: [
												{
													value: 'onhover',
													label: __(
														'Hover',
														'product-blocks'
													),
												},
												{
													value: 'onclick',
													label: __(
														'Click',
														'product-blocks'
													),
												},
											],
										},
									},
									{
										position: 3,
										data: {
											type: 'range',
											key: 'imageHeight',
											min: 100,
											max: 2000,
											step: 1,
											responsive: true,
											unit: true,
											label: __(
												'Image Height',
												'product-blocks'
											),
										},
									},
									{
										position: 4,
										data: {
											type: 'tag',
											key: 'imageScale',
											label: __(
												'Image scale',
												'product-blocks'
											),
											options: [
												{
													value: '',
													label: __(
														'None',
														'product-blocks'
													),
												},
												{
													value: 'cover',
													label: __(
														'Cover',
														'product-blocks'
													),
												},
												{
													value: 'contain',
													label: __(
														'Contain',
														'product-blocks'
													),
												},
												{
													value: 'fill',
													label: __(
														'Fill',
														'product-blocks'
													),
												},
												{
													value: 'scale-down',
													label: __(
														'Scale Down',
														'product-blocks'
													),
												},
											],
										},
									},
									{
										position: 5,
										data: {
											type: 'border',
											key: 'imageBorder',
											label: __(
												'large Image Border',
												'product-blocks'
											),
										},
									},
									{
										position: 6,
										data: {
											type: 'dimension',
											key: 'imageRadius',
											label: __(
												'Large Image Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'gallery':
			return (
				<WopbToolbarGroup text={ 'Gallery' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Gallery Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Gallery Settings',
									'product-blocks'
								),
								options: getData( [
									{
										position: 1,
										data: {
											type: 'tag',
											key: 'galleryPosition',
											label: 'Position',
											options: [
												{
													value: 'bottom',
													label: __(
														'Bottom',
														'product-blocks'
													),
												},
												{
													value: 'right',
													label: __(
														'Right',
														'product-blocks'
													),
												},
												{
													value: 'left',
													label: __(
														'Left',
														'product-blocks'
													),
												},
												{
													value: 'top',
													label: __(
														'Top',
														'product-blocks'
													),
												},
											],
										},
									},
									{
										position: 2,
										data: {
											type: 'range',
											key: 'galleryColumns',
											min: 1,
											max: 5,
											step: 1,
											responsive: true,
											label: __(
												'Columns',
												'product-blocks'
											),
										},
									},
									{
										position: 3,
										data: {
											type: 'range',
											key: 'gallerycolumnGap',
											min: 0,
											max: 100,
											step: 1,
											responsive: true,
											unit: [ 'px', 'em' ],
											label: __(
												'Column Gap',
												'product-blocks'
											),
										},
									},
									{
										position: 4,
										data: {
											type: 'range',
											key: 'gallerySpace',
											min: 0,
											max: 50,
											step: 1,
											responsive: true,
											unit: [ 'px', 'em' ],
											label: __(
												'Space Between',
												'product-blocks'
											),
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'arrow':
			return (
				<WopbToolbarGroup text={ 'Arrow' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Large Image Arrow Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Large Image Title Settings',
									'product-blocks'
								),
								options: filterFields(
									null,
									[ 'arrowStyle' ],
									ArrowStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'lightbox-zoom':
			return (
				<WopbToolbarGroup text={ 'Zoom' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Lightbox Zoom Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Lightbox Zoom Settings',
									'product-blocks'
								),
								options: getData( [
									{
										position: 1,
										data: {
											type: 'tag',
											key: 'iconPosition',
											label: 'Position',
											options: [
												{
													value: 'topRight',
													label: __(
														'Top Right',
														'product-blocks'
													),
												},
												{
													value: 'topLeft',
													label: __(
														'Top Left',
														'product-blocks'
													),
												},
												{
													value: 'buttonLeft',
													label: __(
														'Button Left',
														'product-blocks'
													),
												},
												{
													value: 'buttonRight',
													label: __(
														'Button Right',
														'product-blocks'
													),
												},
											],
										},
									},
									{
										position: 2,
										data: {
											type: 'range',
											key: 'zoomIconSize',
											min: 1,
											max: 50,
											step: 1,
											responsive: true,
											unit: true,
											label: __(
												'Icon Size',
												'product-blocks'
											),
										},
									},
									{
										position: 3,
										data: {
											type: 'color',
											key: 'iconColor',
											label: __(
												'Icon Color',
												'product-blocks'
											),
										},
									},
									{
										position: 4,
										data: {
											type: 'color',
											key: 'iconHoverColor',
											label: __(
												'Icon Hover Color',
												'product-blocks'
											),
										},
									},
									{
										position: 5,
										data: {
											type: 'color',
											key: 'iconBg',
											label: __(
												'Icon Background',
												'product-blocks'
											),
										},
									},
									{
										position: 6,
										data: {
											type: 'color',
											key: 'iconHoverBg',
											label: __(
												'Icon Hover Background',
												'product-blocks'
											),
										},
									},
									{
										position: 7,
										data: {
											type: 'border',
											key: 'iconBorder',
											label: __(
												'Icon Border',
												'product-blocks'
											),
										},
									},
									{
										position: 8,
										data: {
											type: 'border',
											key: 'iconHoverBorder',
											label: __(
												'Icon Hover Border',
												'product-blocks'
											),
										},
									},
									{
										position: 9,
										data: {
											type: 'dimension',
											key: 'iconRadius',
											label: __(
												'Icon Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										position: 10,
										data: {
											type: 'dimension',
											key: 'iconHoverRadius',
											label: __(
												'Icon Hover Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										position: 11,
										data: {
											type: 'dimension',
											key: 'iconPadding',
											label: __(
												'Icon Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										position: 12,
										data: {
											type: 'dimension',
											key: 'iconMargin',
											label: __(
												'Icon Margin',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'gallery-arrow-style':
			return (
				<WopbToolbarGroup text={ 'Arrow' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Gallery Arrow Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Gallery Arrow Settings',
									'product-blocks'
								),
								options: getData( [
									{
										position: 1,
										data: {
											type: 'range',
											key: 'arrowGallerySize',
											min: 1,
											max: 50,
											step: 1,
											responsive: true,
											unit: true,
											label: __(
												'Icon Size',
												'product-blocks'
											),
										},
									},
									{
										position: 2,
										data: {
											type: 'color',
											key: 'arrowGalleryColor',
											label: __(
												'Arrow Color',
												'product-blocks'
											),
										},
									},
									{
										position: 3,
										data: {
											type: 'color',
											key: 'arrowGalleryHoverColor',
											label: __(
												'Arrow Hover Color',
												'product-blocks'
											),
										},
									},
									{
										position: 4,
										data: {
											type: 'color',
											key: 'arrowGalleryBg',
											label: __(
												'Arrow Background',
												'product-blocks'
											),
										},
									},
									{
										position: 5,
										data: {
											type: 'color',
											key: 'arrowGalleryHoverBg',
											label: __(
												'Arrow Hover Background',
												'product-blocks'
											),
										},
									},
									{
										position: 6,
										data: {
											type: 'dimension',
											key: 'arrowGalleryPadding',
											label: __(
												'Arrow Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'sales':
			return (
				<WopbToolbarGroup text={ 'Sales' }>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Sales Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __( 'Sales Settings', 'product-blocks' ),
								options: filterFields(
									[
										{
											data: {
												type: 'dimension',
												key: 'salesMargin',
												label: __(
													'Sales Margin',
													'product-blocks'
												),
												step: 1,
												unit: true,
												responsive: true,
											},
										},
									],
									null,
									SalesStyleArg
								),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		default:
			return null;
	}
}
