const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';
registerBlockType( 'product-blocks/product-image', {
	title: __( 'Product Image', 'product-blocks' ),
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/builder/image.svg' }
			alt={ __( 'Product Image', 'product-blocks' ) }
		/>
	),
	category: 'single-product',
	description: __(
		'Dynamically control position, size, and typography of product images.',
		'product-blocks'
	),
	keywords: [
		__( 'Image', 'product-blocks' ),
		__( 'Product Image', 'product-blocks' ),
		__( 'Image Gallery', 'product-blocks' ),
	],
	supports: {
		align: [ 'center', 'wide', 'full' ],
	},
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
