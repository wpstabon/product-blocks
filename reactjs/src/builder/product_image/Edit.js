/** @format */
import useDevice from '../../helper/hooks/use-device';

const { __ } = wp.i18n;
const { addQueryArgs } = wp.url;
const { useState, useEffect, useRef } = wp.element;
const { InspectorControls } = wp.blockEditor;
import Slider from 'react-slick';
import { CssGenerator } from '../../helper/CssGenerator';
import {
	CommonSettings,
	SalesStyle,
	ArrowStyle,
	GeneralAdvanced,
	CustomCssAdvanced,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Sections, Section } from '../../helper/Sections';
import useFluentSettings from '../../helper/hooks/useFluentSettings';
import { AddSettingsToToolbar } from './Settings';
import ToolBarElement from '../../helper/ToolBarElement';
import useBlockId from '../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: {
			blockId,
			advanceId,
			arrowGalleryImage,
			showGallery,
			arrowLargeImage,
			saleText,
			showSale,
			showlightBox,
			saleDesign,
			galleryPosition,
			galleryColumns,
			previews,
		},
	} = props;

	const { setCurrSettings, section, toolbarSettings } = useFluentSettings();

	const store = { setAttributes, name, attributes, clientId };

	const [ prev, setPrev ] = useState( '' );

	const [ postsList, setPostsList ] = useState( [] );
	const initData = {
		percentage: '10%',
		images: [
			wopb_data.url + 'assets/img/blocks/builder/sample/1.jpg',
			wopb_data.url + 'assets/img/blocks/builder/sample/2.jpg',
			wopb_data.url + 'assets/img/blocks/builder/sample/3.jpg',
			wopb_data.url + 'assets/img/blocks/builder/sample/1.jpg',
			wopb_data.url + 'assets/img/blocks/builder/sample/2.jpg',
			wopb_data.url + 'assets/img/blocks/builder/sample/3.jpg',
		],
		images_thumb: [
			wopb_data.url + 'assets/img/blocks/builder/sample/1.jpg',
			wopb_data.url + 'assets/img/blocks/builder/sample/2.jpg',
			wopb_data.url + 'assets/img/blocks/builder/sample/3.jpg',
			wopb_data.url + 'assets/img/blocks/builder/sample/1.jpg',
			wopb_data.url + 'assets/img/blocks/builder/sample/2.jpg',
			wopb_data.url + 'assets/img/blocks/builder/sample/3.jpg',
		],
	};
	const [ data, setData ] = useState( initData );
	const [ nav1, setNav1 ] = useState( null );
	const [ nav2, setNav2 ] = useState( null );
	let sliderRef1 = useRef( null );
	let sliderRef2 = useRef( null );

	const PrevArrow = ( { setSection, onClick } ) => {
		return (
			<div
				className={ `slick-arrow wopb-slick-prev-large` }
				onClick={ ( e ) => {
					setSection( e, 'arrow' );
					onClick();
				} }
			>
				<svg
					enableBackground="new 0 0 477.175 477.175"
					version="1.1"
					viewBox="0 0 477.18 477.18"
				>
					<path d="m145.19 238.58 215.5-215.5c5.3-5.3 5.3-13.8 0-19.1s-13.8-5.3-19.1 0l-225.1 225.1c-5.3 5.3-5.3 13.8 0 19.1l225.1 225c2.6 2.6 6.1 4 9.5 4s6.9-1.3 9.5-4c5.3-5.3 5.3-13.8 0-19.1l-215.4-215.5z"></path>
				</svg>
			</div>
		);
	};
	const NextArrow = ( { setSection, onClick } ) => {
		return (
			<div
				className={ `slick-arrow wopb-slick-next-large` }
				onClick={ ( e ) => {
					setSection( e, 'arrow' );
					onClick();
				} }
			>
				<svg
					enableBackground="new 0 0 477.175 477.175"
					version="1.1"
					viewBox="0 0 477.18 477.18"
				>
					<path d="m360.73 229.08-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5-215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4s6.9-1.3 9.5-4l225.1-225.1c5.3-5.2 5.3-13.8 0.1-19z" />
				</svg>
			</div>
		);
	};
	const settings = {
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		dots: false,
		infinite: true,
		arrows: arrowLargeImage,
		prevArrow: <PrevArrow setSection={ setCurrSettings } />,
		nextArrow: <NextArrow setSection={ setCurrSettings } />,
	};

	const PrevArrowNav = ( { setSection, onClick } ) => {
		return (
			<div
				className={ `slick-arrow wopb-slick-prev-nav` }
				onClick={ ( e ) => {
					setSection( e, 'gallery-arrow-style' );
					onClick();
				} }
			>
				<svg viewBox="0 0 492 287" xmlns="http://www.w3.org/2000/svg">
					<path
						transform="translate(0 -.96)"
						d="m485.97 252.68-224.38-245.85c-4.2857-4.3102-9.9871-6.1367-15.585-5.8494-5.6186-0.28724-11.3 1.5392-15.586 5.8494l-224.4 245.85c-8.0384 8.0653-8.0384 21.159 0 29.225s21.081 8.0653 29.12 0l210.86-231.05 210.84 231.05c8.0384 8.0653 21.08 8.0653 29.119 0 8.0384-8.0645 8.0384-21.159 0-29.225z"
					/>
				</svg>
			</div>
		);
	};
	const NextArrowNav = ( { setSection, onClick } ) => {
		return (
			<div
				className={ `slick-arrow wopb-slick-next-nav` }
				onClick={ ( e ) => {
					setSection( e, 'gallery-arrow-style' );
					onClick();
				} }
			>
				<svg viewBox="0 0 492 287" xmlns="http://www.w3.org/2000/svg">
					<path
						transform="translate(0 -.96)"
						d="m485.97 252.68-224.38-245.85c-4.2857-4.3102-9.9871-6.1367-15.585-5.8494-5.6186-0.28724-11.3 1.5392-15.586 5.8494l-224.4 245.85c-8.0384 8.0653-8.0384 21.159 0 29.225s21.081 8.0653 29.12 0l210.86-231.05 210.84 231.05c8.0384 8.0653 21.08 8.0653 29.119 0 8.0384-8.0645 8.0384-21.159 0-29.225z"
					/>
				</svg>
			</div>
		);
	};
	const [ device ] = useDevice();
	const settings_nav = {
		slidesToShow: Number( galleryColumns?.[ device ] ?? galleryColumns.lg ),
		slidesToScroll: 1,
		vertical:
			galleryPosition == 'right' || galleryPosition == 'left'
				? true
				: false,
		focusOnSelect: true,
		dots: false,
		infinite: data.images_thumb.length > Number( galleryColumns.lg ),
		verticalSwiping: true,
		centerMode: true,
		arrows: arrowGalleryImage,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					vertical: false,
					slidesToShow: Number( galleryColumns.sm ),
				},
			},
			{
				breakpoint: 768,
				settings: {
					vertical: false,
					slidesToShow: Number( galleryColumns.xs ),
				},
			},
		],
		prevArrow: <PrevArrowNav setSection={ setCurrSettings } />,
		nextArrow: <NextArrowNav setSection={ setCurrSettings } />,
	};

	useBlockId( props, true );

	useEffect( () => {
		setPrev( previews );
		fetchProducts();
	}, [] );

	useEffect( () => {
		setNav1( sliderRef1 );
		setNav2( sliderRef2 );
	}, [] );

	useEffect( () => {
		if ( previews && prev != previews ) {
			fetchProducts();
		} else {
			setData( initData );
		}
		setPrev( previews );
	}, [ previews ] );

	function fetchProducts() {
		const query = addQueryArgs( '/wopb/preview', {
			previews,
			wpnonce: wopb_data.security,
			type: 'image',
		} );
		wp.apiFetch( { path: query } ).then( ( obj ) => {
			if ( obj.type == 'data' ) {
				setData( obj.data );
			} else {
				setPostsList( obj.list );
			}
		} );
	}

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/product-image', blockId );
	}

	return (
		<>
			<ToolBarElement
				store={ store }
				include={ [
					{
						type: 'block_spacing',
						label: __( 'Product Image Spacing', 'product-blocks' ),
					},
				] }
			/>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Style', 'product-blocks' ) }
					>
						<CommonSettings
							initialOpen={ section.general }
							title={ 'General' }
							store={ store }
							include={ [
								{
									position: 1,
									data: {
										type: 'select',
										key: 'previews',
										label: __(
											'Preview',
											'product-blocks'
										),
										options: postsList || [],
									},
								},
								{
									position: 2,
									data: {
										type: 'toggle',
										key: 'hoverZoom',
										label: __(
											'Hover Zoom',
											'product-blocks'
										),
									},
								},
								{
									position: 2,
									data: {
										type: 'group',
										key: 'imageView',
										label: __(
											'Image View',
											'product-blocks'
										),
										justify: true,
										disabled: true,
										options: [
											{
												value: 'onhover',
												label: __(
													'Hover',
													'product-blocks'
												),
											},
											{
												value: 'onclick',
												label: __(
													'Click',
													'product-blocks'
												),
											},
										],
									},
								},
								{
									position: 3,
									data: {
										type: 'range',
										key: 'imageHeight',
										min: 100,
										max: 2000,
										step: 1,
										responsive: true,
										unit: true,
										label: __(
											'Image Height',
											'product-blocks'
										),
									},
								},
								{
									position: 4,
									data: {
										type: 'tag',
										key: 'imageScale',
										label: __(
											'Image scale',
											'product-blocks'
										),
										options: [
											{
												value: '',
												label: __(
													'None',
													'product-blocks'
												),
											},
											{
												value: 'cover',
												label: __(
													'Cover',
													'product-blocks'
												),
											},
											{
												value: 'contain',
												label: __(
													'Contain',
													'product-blocks'
												),
											},
											{
												value: 'fill',
												label: __(
													'Fill',
													'product-blocks'
												),
											},
											{
												value: 'scale-down',
												label: __(
													'Scale Down',
													'product-blocks'
												),
											},
										],
									},
								},
								{
									position: 5,
									data: {
										type: 'border',
										key: 'imageBorder',
										label: __(
											'large Image Border',
											'product-blocks'
										),
									},
								},
								{
									position: 6,
									data: {
										type: 'dimension',
										key: 'imageRadius',
										label: __(
											'Large Image Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<ArrowStyle
							depend="arrowLargeImage"
							initialOpen={ section.arrow }
							exclude={ [ 'arrowStyle' ] }
							store={ store }
						/>
						<CommonSettings
							depend="arrowGalleryImage"
							initialOpen={ section[ 'gallery-arrow-style' ] }
							title={ __(
								'Gallery Arrow Style',
								'product-blocks'
							) }
							store={ store }
							include={ [
								{
									position: 1,
									data: {
										type: 'range',
										key: 'arrowGallerySize',
										min: 1,
										max: 50,
										step: 1,
										responsive: true,
										unit: true,
										label: __(
											'Icon Size',
											'product-blocks'
										),
									},
								},
								{
									position: 2,
									data: {
										type: 'color',
										key: 'arrowGalleryColor',
										label: __(
											'Arrow Color',
											'product-blocks'
										),
									},
								},
								{
									position: 3,
									data: {
										type: 'color',
										key: 'arrowGalleryHoverColor',
										label: __(
											'Arrow Hover Color',
											'product-blocks'
										),
									},
								},
								{
									position: 4,
									data: {
										type: 'color',
										key: 'arrowGalleryBg',
										label: __(
											'Arrow Background',
											'product-blocks'
										),
									},
								},
								{
									position: 5,
									data: {
										type: 'color',
										key: 'arrowGalleryHoverBg',
										label: __(
											'Arrow Hover Background',
											'product-blocks'
										),
									},
								},
								{
									position: 6,
									data: {
										type: 'dimension',
										key: 'arrowGalleryPadding',
										label: __(
											'Arrow Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							depend="showGallery"
							initialOpen={ section.gallery }
							title={ __( 'Gallery', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									position: 1,
									data: {
										type: 'tag',
										key: 'galleryPosition',
										label: 'Position',
										options: [
											{
												value: 'bottom',
												label: __(
													'Bottom',
													'product-blocks'
												),
											},
											{
												value: 'right',
												label: __(
													'Right',
													'product-blocks'
												),
											},
											{
												value: 'left',
												label: __(
													'Left',
													'product-blocks'
												),
											},
											{
												value: 'top',
												label: __(
													'Top',
													'product-blocks'
												),
											},
										],
									},
								},
								{
									position: 2,
									data: {
										type: 'range',
										key: 'galleryColumns',
										min: 1,
										max: 5,
										step: 1,
										responsive: true,
										label: __(
											'Columns',
											'product-blocks'
										),
									},
								},
								{
									position: 3,
									data: {
										type: 'range',
										key: 'gallerycolumnGap',
										min: 0,
										max: 100,
										step: 1,
										responsive: true,
										unit: [ 'px', 'em' ],
										label: __(
											'Column Gap',
											'product-blocks'
										),
									},
								},
								{
									position: 4,
									data: {
										type: 'range',
										key: 'gallerySpace',
										min: 0,
										max: 50,
										step: 1,
										responsive: true,
										unit: [ 'px', 'em' ],
										label: __(
											'Space Between',
											'product-blocks'
										),
									},
								},
							] }
						/>
						<SalesStyle
							depend="showSale"
							initialOpen={ section.sales }
							include={ [
								{
									data: {
										type: 'dimension',
										key: 'salesMargin',
										label: __(
											'Sales Margin',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
							store={ store }
						/>
						<CommonSettings
							depend="showlightBox"
							initialOpen={ section[ 'lightbox-zoom' ] }
							title={ __( 'Lightbox Zoom', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									position: 1,
									data: {
										type: 'tag',
										key: 'iconPosition',
										label: 'Position',
										options: [
											{
												value: 'topRight',
												label: __(
													'Top Right',
													'product-blocks'
												),
											},
											{
												value: 'topLeft',
												label: __(
													'Top Left',
													'product-blocks'
												),
											},
											{
												value: 'buttonLeft',
												label: __(
													'Button Left',
													'product-blocks'
												),
											},
											{
												value: 'buttonRight',
												label: __(
													'Button Right',
													'product-blocks'
												),
											},
										],
									},
								},
								{
									position: 2,
									data: {
										type: 'range',
										key: 'zoomIconSize',
										min: 1,
										max: 50,
										step: 1,
										responsive: true,
										unit: true,
										label: __(
											'Icon Size',
											'product-blocks'
										),
									},
								},
								{
									position: 3,
									data: {
										type: 'color',
										key: 'iconColor',
										label: __(
											'Icon Color',
											'product-blocks'
										),
									},
								},
								{
									position: 4,
									data: {
										type: 'color',
										key: 'iconHoverColor',
										label: __(
											'Icon Hover Color',
											'product-blocks'
										),
									},
								},
								{
									position: 5,
									data: {
										type: 'color',
										key: 'iconBg',
										label: __(
											'Icon Background',
											'product-blocks'
										),
									},
								},
								{
									position: 6,
									data: {
										type: 'color',
										key: 'iconHoverBg',
										label: __(
											'Icon Hover Background',
											'product-blocks'
										),
									},
								},
								{
									position: 7,
									data: {
										type: 'border',
										key: 'iconBorder',
										label: __(
											'Icon Border',
											'product-blocks'
										),
									},
								},
								{
									position: 8,
									data: {
										type: 'border',
										key: 'iconHoverBorder',
										label: __(
											'Icon Hover Border',
											'product-blocks'
										),
									},
								},
								{
									position: 9,
									data: {
										type: 'dimension',
										key: 'iconRadius',
										label: __(
											'Icon Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									position: 10,
									data: {
										type: 'dimension',
										key: 'iconHoverRadius',
										label: __(
											'Icon Hover Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									position: 11,
									data: {
										type: 'dimension',
										key: 'iconPadding',
										label: __(
											'Icon Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									position: 12,
									data: {
										type: 'dimension',
										key: 'iconMargin',
										label: __(
											'Icon Margin',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar
				store={ store }
				selected={ toolbarSettings }
				postsList={ postsList }
			/>

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
				onClick={ ( e ) => setCurrSettings( e, 'general' ) }
			>
				<div
					className={ `wopb-product-wrapper wopb-product-gallery-${ galleryPosition }` }
				>
					<div className="woocommerce-product-gallery-off woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images slider">
						<figure className="woocommerce-product-gallery__wrapper">
							<div className={ `wopb-product-gallery-wrapper` }>
								{ showlightBox && (
									<a
										href="#"
										className={ `wopb-product-zoom` }
										onClick={ ( e ) => {
											setCurrSettings(
												e,
												'lightbox-zoom'
											);
										} }
									>
										<svg
											enableBackground="new 0 0 612 612"
											version="1.1"
											viewBox="0 0 612 612"
											xmlns="http://www.w3.org/2000/svg"
										>
											<path d="m243.96 340.18-206.3 206.32 0.593-125.75c0-10.557-8.568-19.125-19.125-19.125s-19.125 8.568-19.125 19.125v172.12c0 5.661 2.333 10.232 6.043 13.368 3.462 3.538 8.282 5.757 13.637 5.757h171.57c10.557 0 19.125-8.567 19.125-19.125 0-10.557-8.568-19.125-19.125-19.125h-126.78l206.53-206.51-27.043-27.061zm362-334.42c-3.461-3.538-8.28-5.757-13.616-5.757h-171.59c-10.557 0-19.125 8.568-19.125 19.125s8.568 19.125 19.125 19.125h126.76l-206.51 206.53 27.042 27.042 206.32-206.32-0.612 125.75c0 10.557 8.568 19.125 19.125 19.125s19.125-8.568 19.125-19.125v-172.12c0-5.661-2.333-10.231-6.044-13.368z" />
										</svg>
									</a>
								) }
								{ showSale && (
									<div
										className={ `wopb-product-gallery-sale-tag` }
										onClick={ ( e ) => {
											setCurrSettings( e, 'sales' );
										} }
									>
										{ saleDesign == 'textDigit'
											? '-' +
											  data.percentage +
											  ' ' +
											  saleText
											: saleDesign == 'digit'
											? '-' + data.percentage + ''
											: saleText }
									</div>
								) }
								<div
									className={ `slider wopb-builder-slider-for` }
								>
									<Slider
										{ ...settings }
										{ ...( showGallery &&
											data.images_thumb.length > 1 && {
												asNavFor: nav2,
											} ) }
										ref={ ( slider ) =>
											( sliderRef1 = slider )
										}
									>
										{ data &&
											data.images &&
											data.images.map( ( src, k ) => {
												return (
													<div key={ k }>
														<img src={ src } />
													</div>
												);
											} ) }
									</Slider>
								</div>
							</div>
							{ showGallery && (
								<div
									onClick={ ( e ) => {
										setCurrSettings( e, 'gallery' );
									} }
									className={ `slider wopb-builder-slider-nav thumb-image` }
								>
									{ data.images_thumb.length > 1 && (
										<Slider
											{ ...settings_nav }
											asNavFor={ nav1 }
											ref={ ( slider ) =>
												( sliderRef2 = slider )
											}
										>
											{ data &&
												data.images_thumb &&
												data.images_thumb.map(
													( src, k ) => {
														return (
															<div key={ k }>
																<img
																	src={ src }
																/>
															</div>
														);
													}
												) }
										</Slider>
									) }
								</div>
							) }
						</figure>
					</div>
				</div>
			</div>
		</>
	);
}
