const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';
registerBlockType( 'product-blocks/product-meta', {
	title: __( 'Product Meta', 'product-blocks' ),
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/builder/meta.svg' }
			alt={ __( 'Related Product', 'product-blocks' ) }
		/>
	),
	category: 'single-product',
	description: __(
		'Dynamically control position, size, and typography of product metadata.',
		'product-blocks'
	),
	keywords: [
		__( 'Product Meta', 'product-blocks' ),
		__( 'Product SKU', 'product-blocks' ),
		__( 'Product Tags', 'product-blocks' ),
	],
	supports: {
		align: [ 'center', 'wide', 'full' ],
	},
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
