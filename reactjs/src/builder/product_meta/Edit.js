const { __ } = wp.i18n;
const { addQueryArgs } = wp.url;
const { useState, useEffect } = wp.element;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import {
	CommonSettings,
	GeneralAdvanced,
	CustomCssAdvanced,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Sections, Section } from '../../helper/Sections';
import { AddSettingsToToolbar } from './Settings';
import useBlockId from '../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: {
			blockId,
			advanceId,
			metaCategory,
			metaSku,
			metaTag,
			metaLabelShow,
			labelSku,
			labelCat,
			labelTag,
			previews,
		},
	} = props;
	const store = {
		setAttributes,
		name,
		attributes,
		clientId,
	};

	const [ prev, setPrev ] = useState( '' );
	const [ postsList, setPostsList ] = useState( [] );
	const [ data, setData ] = useState( {
		sku: 'sample-sku',
		category:
			'<div class="meta-block__cat"><a class="meta-block__value" href="#">category 1</a>, <a class="meta-block__value" href="#">category 2</a></div>',
		tag: '<div className="meta-block__tag"><a class="meta-block__value" href="#">tag 1</a>, <a class="meta-block__value" href="#">tag 2</a></div>',
	} );

	useEffect( () => {
		setPrev( previews );
		fetchProducts();
	}, [] );

	useEffect( () => {
		if ( prev != previews ) {
			fetchProducts();
			setPrev( previews );
		}
	}, [ previews ] );

	useBlockId( props, true );

	function fetchProducts() {
		const query = addQueryArgs( '/wopb/preview', {
			previews,
			wpnonce: wopb_data.security,
			type: 'meta',
		} );
		wp.apiFetch( { path: query } ).then( ( obj ) => {
			if ( obj.type == 'data' ) {
				setData( obj.data );
			} else {
				setPostsList( obj.list );
			}
		} );
	}

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/product-meta', blockId );
	}

	return (
		<>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Style', 'product-blocks' ) }
					>
						<CommonSettings
							initialOpen={ true }
							title={ 'inline' }
							store={ store }
							include={ [
								{
									position: 1,
									data: {
										type: 'select',
										key: 'previews',
										label: __(
											'Preview',
											'product-blocks'
										),
										options: postsList || [],
									},
								},
								{
									position: 2,
									data: {
										type: 'group',
										key: 'metaView',
										label: __(
											'Meta Display',
											'product-blocks'
										),
										justify: true,
										options: [
											{
												value: 'stacked',
												label: __(
													'Stacked',
													'product-blocks'
												),
											},
											{
												value: 'inline',
												label: __(
													'Inline',
													'product-blocks'
												),
											},
										],
									},
								},
								{
									position: 3,
									data: {
										type: 'alignment',
										key: 'metaAlign',
										disableJustify: true,
										label: __(
											'Alignment',
											'product-blocks'
										),
									},
								},
								{
									position: 4,
									data: {
										type: 'toggle',
										key: 'metaSku',
										label: __( 'SKU', 'product-blocks' ),
									},
								},
								{
									position: 5,
									data: {
										type: 'toggle',
										key: 'metaCategory',
										label: __(
											'Category',
											'product-blocks'
										),
									},
								},
								{
									position: 6,
									data: {
										type: 'toggle',
										key: 'metaTag',
										label: __( 'Tag', 'product-blocks' ),
									},
								},
								{
									position: 7,
									data: {
										type: 'toggle',
										key: 'metaLabelShow',
										label: __(
											'Label Show',
											'product-blocks'
										),
									},
								},
								{
									position: 8,
									data: {
										type: 'range',
										key: 'metaLabelSpace',
										min: 0,
										max: 50,
										label: __(
											'Space Between',
											'product-blocks'
										),
									},
								},
							] }
						/>
						{ metaLabelShow && (
							<CommonSettings
								initialOpen={ false }
								title={ __( 'Meta Label', 'product-blocks' ) }
								store={ store }
								include={ [
									{
										position: 1,
										data: {
											type: 'text',
											key: 'labelSku',
											label: __(
												'SKU Label',
												'product-blocks'
											),
										},
									},
									{
										position: 2,
										data: {
											type: 'text',
											key: 'labelCat',
											label: __(
												'Category Label',
												'product-blocks'
											),
										},
									},
									{
										position: 3,
										data: {
											type: 'text',
											key: 'labelTag',
											label: __(
												'Tag Label',
												'product-blocks'
											),
										},
									},
									{
										position: 4,
										data: {
											type: 'typography',
											key: 'labelTypography',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										position: 5,
										data: {
											type: 'color',
											key: 'labelColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
								] }
							/>
						) }
						<CommonSettings
							initialOpen={ false }
							title={ __( 'Meta Value', 'product-blocks' ) }
							store={ store }
							include={ [
								{
									position: 1,
									data: {
										type: 'typography',
										key: 'valueTypography',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									position: 2,
									data: {
										type: 'color',
										key: 'valueColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									position: 3,
									data: {
										type: 'color',
										key: 'valueHoverColor',
										label: __(
											'Hover Color',
											'product-blocks'
										),
									},
								},
							] }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar store={ store } />

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
			>
				<div className={ `wopb-product-wrapper` }>
					{ metaSku && (
						<div className={ `wopb-meta-sku` }>
							{ metaLabelShow && (
								<span
									className={ `wopb-meta-label-sku meta-block__label` }
								>
									{ labelSku }
								</span>
							) }
							<span
								className={ `wopb-meta-list-sku meta-block__value` }
							>
								{ data.sku }
							</span>
						</div>
					) }
					{ metaCategory && (
						<div className={ `wopb-meta-cat` }>
							{ metaLabelShow && (
								<div
									className={ `wopb-meta-label-cat meta-block__label` }
								>
									{ labelCat }{ ' ' }
								</div>
							) }
							<div
								className="wopb-meta-list-cat meta-block__value"
								dangerouslySetInnerHTML={ {
									__html: data.category,
								} }
							></div>
						</div>
					) }
					{ metaTag && (
						<div className={ `wopb-meta-tag` }>
							{ metaLabelShow && (
								<span
									className={ `wopb-meta-label-tag meta-block__label` }
								>
									{ labelTag }
								</span>
							) }
							<div
								className="wopb-meta-list-tag meta-block__value"
								dangerouslySetInnerHTML={ { __html: data.tag } }
							></div>
						</div>
					) }
				</div>
			</div>
		</>
	);
}
