const attributes = {
	blockId: {
		type: 'string',
		default: '',
	},
	currentPostId: { type: 'string', default: '' },
	previews: {
		type: 'string',
		default: '',
	},
	metaAlign: {
		type: 'string',
		default: 'left',
		style: [
			{
				depends: [
					{
						key: 'metaAlign',
						condition: '==',
						value: 'left',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-wrapper, {{WOPB}} .wopb-meta-cat, {{WOPB}} .wopb-meta-tag {justify-content:flex-start;text-align:left;}',
			},
			{
				depends: [
					{
						key: 'metaAlign',
						condition: '==',
						value: 'center',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-wrapper, {{WOPB}} .wopb-meta-cat, {{WOPB}} .wopb-meta-tag {justify-content:center;text-align:center;}',
			},
			{
				depends: [
					{
						key: 'metaAlign',
						condition: '==',
						value: 'right',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-wrapper, {{WOPB}} .wopb-meta-cat, {{WOPB}} .wopb-meta-tag {justify-content:flex-end;text-align:right;}',
			},
		],
	},
	metaSku: {
		type: 'boolean',
		default: true,
	},
	metaCategory: {
		type: 'boolean',
		default: true,
	},
	metaTag: {
		type: 'boolean',
		default: true,
	},
	metaLabelShow: {
		type: 'boolean',
		default: true,
	},
	metaView: {
		type: 'string',
		default: 'stacked',
		style: [
			{
				depends: [
					{
						key: 'metaView',
						condition: '==',
						value: 'stacked',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-wrapper {display:flex;flex-direction: column;}',
			},
			{
				depends: [
					{
						key: 'metaView',
						condition: '==',
						value: 'inline',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-wrapper{display:flex;flex-wrap: wrap;}',
			},
		],
	},
	metaLabelSpace: {
		type: 'string',
		default: 12,
		style: [
			{
				depends: [
					{
						key: 'metaView',
						condition: '==',
						value: 'stacked',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-wrapper > div:not(:last-child){margin-bottom:{{metaLabelSpace}}px;}',
			},
			{
				depends: [
					{
						key: 'metaView',
						condition: '==',
						value: 'inline',
					},
				],
				selector:
					'{{WOPB}} .wopb-product-wrapper > div:not(:last-child){margin-right:{{metaLabelSpace}}px;}',
			},
		],
	},
	labelSku: {
		type: 'string',
		default: 'sku : ',
		style: [
			{
				depends: [
					{
						key: 'metaSku',
						condition: '==',
						value: true,
					},
				],
			},
		],
	},
	labelCat: {
		type: 'string',
		default: 'category : ',
		style: [
			{
				depends: [
					{
						key: 'metaCategory',
						condition: '==',
						value: true,
					},
				],
			},
		],
	},
	labelTag: {
		type: 'string',
		default: 'tag : ',
		style: [
			{
				depends: [
					{
						key: 'metaTag',
						condition: '==',
						value: true,
					},
				],
			},
		],
	},
	labelTypography: {
		type: 'object',
		default: {
			openTypography: 1,
			size: {
				lg: '14',
				unit: 'px',
			},
			spacing: {
				lg: '',
				unit: 'px',
			},
			height: {
				lg: '20',
				unit: 'px',
			},
			decoration: 'none',
			transform: 'capitalize',
			family: '',
			weight: '',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper .meta-block__label',
			},
		],
	},
	labelColor: {
		type: 'string',
		default: '#6E6E6E',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .meta-block__label{color:{{labelColor}};}',
			},
		],
	},
	valueTypography: {
		type: 'object',
		default: {
			openTypography: 1,
			size: {
				lg: '14',
				unit: 'px',
			},
			spacing: {
				lg: '',
				unit: 'px',
			},
			height: {
				lg: '20',
				unit: 'px',
			},
			decoration: 'none',
			transform: 'capitalize',
			family: '',
			weight: '500',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-meta-list-cat, {{WOPB}} .wopb-meta-list-tag, {{WOPB}} .wopb-meta-list-sku',
			},
		],
	},
	valueColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper a, .editor-styles-wrapper {{WOPB}} .wopb-product-wrapper a, {{WOPB}} .wopb-meta-list-sku {color:{{valueColor}};}',
			},
		],
	},
	valueHoverColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .meta-block__value:hover{color:{{valueHoverColor}};}',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: {
			openColor: 0,
			type: 'color',
			color: '#f5f5f5',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper',
			},
		],
	},
	wrapRadius: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: {
			openColor: 0,
			type: 'color',
			color: '#ff176b',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
			type: 'solid',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapHoverRadius: {
		type: 'object',
		default: {
			lg: '',
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: {
				top: 1,
				right: 1,
				bottom: 1,
				left: 1,
			},
			color: '#009fd4',
		},
		style: [
			{
				selector: '{{WOPB}} .wopb-product-wrapper:hover',
			},
		],
	},
	wrapMargin: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '20',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: {
				top: '',
				bottom: '',
				left: '',
				right: '',
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	wrapInnerPadding: {
		type: 'object',
		default: {
			lg: {
				unit: 'px',
			},
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ padding:{{wrapInnerPadding}}; }',
			},
		],
	},
	advanceId: {
		type: 'string',
		default: '',
	},
	advanceZindex: {
		type: 'string',
		default: '',
		style: [
			{
				selector: '{{WOPB}} {z-index:{{advanceZindex}};}',
			},
		],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: {
		type: 'string',
		default: '',
		style: [
			{
				selector: '',
			},
		],
	},
};

export default attributes;
