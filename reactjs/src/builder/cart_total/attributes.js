const attributes = {
	blockId: { type: 'string', default: '' },
	currentPostId: { type: 'string', default: '' },
	cartTotalTxt: { type: 'string', default: 'Cart Total' },
	subTotalTxt: { type: 'string', default: 'Subtotal' },
	totalTxt: { type: 'string', default: 'Total' },
	checkoutTxt: { type: 'string', default: 'Proceed to checkout' },
	tableBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#E6E6E6',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-cart-total' } ],
	},
	tableRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0 }, unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-total { border-radius:{{tableRadius}}; overflow:hidden;}',
			},
		],
	},
	tablePadding: {
		type: 'object',
		default: { lg: { top: 20, right: 0, bottom: 24, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-total { padding:{{tablePadding}};}',
			},
		],
	},
	headerTextColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-total .wopb-table-heading { color:{{headerTextColor}}; }',
			},
		],
	},
	headerTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '24', unit: 'px' },
			height: { lg: '32', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: '600',
		},
		style: [
			{ selector: '{{WOPB}} .wopb-cart-total .wopb-table-heading ' },
		],
	},
	headerBgColor: {
		type: 'string',
		default: '',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-total .wopb-table-heading { background-color:{{headerBgColor}}!important; }',
			},
		],
	},
	headerBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 0, right: 0, bottom: 1, left: 0 },
			color: '#8c8f94',
			type: 'solid',
		},
		style: [
			{ selector: '{{WOPB}} .wopb-cart-total .wopb-table-heading ' },
		],
	},
	headerRadius: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0 }, unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-total .wopb-table-heading { border-radius:{{headerRadius}};}',
			},
		],
	},
	headerPadding: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 6, left: 24, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-total .wopb-table-heading { padding:{{headerPadding}}; }',
			},
		],
	},
	headingBottomSpace: {
		type: 'string',
		default: '10',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-total .wopb-table-heading { margin-bottom:{{headingBottomSpace}}px;}',
			},
		],
	},
	titleTextColor: {
		type: 'string',
		default: '#4A4A4A',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-total table tbody tr th { color:{{titleTextColor}}; }',
			},
		],
	},
	priceTextColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-total table tbody tr td, {{WOPB}} .wopb-cart-total form p .select2-selection__rendered , {{WOPB}} .wopb-cart-total form p .input-text{ color:{{priceTextColor}}!important; }',
			},
		],
	},
	titleTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			decoration: 'none',
			height: { lg: '20', unit: 'px' },
			transform: 'capitalize',
			weight: '600',
		},
		style: [ { selector: '{{WOPB}} .wopb-cart-total table tbody tr th' } ],
	},
	priceTypo: {
		type: 'object',
		default: {
			openTypography: 0,
			size: { lg: '16', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: '400',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-total table tbody tr td, wopb-total-price , .woocommerce-shipping-methods label, {{WOPB}} .wopb-product-wrapper .wopb-cart-total bdi',
			},
		],
	},
	bodyBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 0, right: 0, bottom: 1, left: 0 },
			color: '#E6E6E6',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-total table tbody th, {{WOPB}} .wopb-cart-total table tbody td',
			},
		],
	},
	tableBodyBgColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-total table tbody tr td , {{WOPB}} .wopb-cart-total table tbody tr th, {{WOPB}} .wopb-cart-total-section { background-color:{{tableBodyBgColor}} !important ; }',
			},
		],
	},
	bodyPadding: {
		type: 'object',
		default: { lg: { top: 0, right: 0, bottom: 0, left: 0, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-total .wopb-cart-total-section { padding:{{bodyPadding}}; }',
			},
		],
	},
	bodySpacing: {
		type: 'object',
		default: {
			lg: { top: 15, right: 24, bottom: 15, left: 24, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-total-section table tbody tr th, {{WOPB}} .wopb-cart-total-section table tbody tr td{ padding:{{bodySpacing}}; }',
			},
		],
	},
	checkoutText: { type: 'string', default: 'Proceed to Checkout' },
	checkoutTextColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-total .wc-proceed-to-checkout .checkout-button { color:{{checkoutTextColor}}; }',
			},
		],
	},
	checkoutHoverTextColor: {
		type: 'string',
		default: '#fff',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-total .wc-proceed-to-checkout .checkout-button:hover { color:{{checkoutHoverTextColor}}; }',
			},
		],
	},
	checkoutBgColor: {
		type: 'string',
		default: '#070707',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-total .wc-proceed-to-checkout .checkout-button, {{WOPB}} .wc-proceed-to-checkout .checkout-button:focus { background-color:{{checkoutBgColor}}; }',
			},
		],
	},
	checkoutHoverBgColor: {
		type: 'string',
		default: '#FF176B',
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-total .wc-proceed-to-checkout .checkout-button:hover { background-color:{{checkoutHoverBgColor}}; }',
			},
		],
	},
	checkoutTypo: {
		type: 'object',
		default: {
			openTypography: 1,
			size: { lg: '16', unit: 'px' },
			height: { lg: '20', unit: 'px' },
			decoration: 'none',
			transform: 'capitalize',
			weight: '500',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wc-proceed-to-checkout .checkout-button',
			},
		],
	},
	checkoutBorder: {
		type: 'object',
		default: {
			openBorder: 1,
			width: { top: 0, right: 0, bottom: 0, left: 0 },
			color: '#333333',
			type: 'solid',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper .wc-proceed-to-checkout .checkout-button',
			},
		],
	},
	checkoutMargin: {
		type: 'object',
		default: { lg: { top: 0, right: 24, bottom: 0, left: 24, unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-total .wc-proceed-to-checkout { margin:{{checkoutMargin}}; }',
			},
		],
	},
	checkoutPadding: {
		type: 'object',
		default: {
			lg: { top: 14, right: 21, bottom: 14, left: 21, unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-total .wc-proceed-to-checkout .checkout-button { padding:{{checkoutPadding}}; }',
			},
		],
	},
	checkoutRadius: {
		type: 'object',
		default: {
			lg: { top: 24, right: 24, bottom: 24, left: 24 },
			unit: 'px',
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-cart-total .wc-proceed-to-checkout .checkout-button { border-radius:{{checkoutRadius}}; margin-bottom: 0px; }',
			},
		],
	},
	advanceId: { type: 'string', default: '' },
	advanceZindex: {
		type: 'string',
		default: '',
		style: [ { selector: '{{WOPB}} {z-index:{{advanceZindex}};}' } ],
	},
	wrapMargin: {
		type: 'object',
		default: { lg: { top: '', bottom: '20', unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ margin:{{wrapMargin}}; }',
			},
		],
	},
	wrapOuterPadding: {
		type: 'object',
		default: {
			lg: { top: '', bottom: '', left: '', right: '', unit: 'px' },
		},
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{padding:{{wrapOuterPadding}}; }',
			},
		],
	},
	wrapBg: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#f5f5f5' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper' } ],
	},
	wrapRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ border-radius:{{wrapRadius}}; }',
			},
		],
	},
	wrapHoverBackground: {
		type: 'object',
		default: { openColor: 0, type: 'color', color: '#ff176b' },
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverBorder: {
		type: 'object',
		default: {
			openBorder: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
			type: 'solid',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapHoverRadius: {
		type: 'object',
		default: { lg: '', unit: 'px' },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper:hover { border-radius:{{wrapHoverRadius}}; }',
			},
		],
	},
	wrapHoverShadow: {
		type: 'object',
		default: {
			openShadow: 0,
			width: { top: 1, right: 1, bottom: 1, left: 1 },
			color: '#009fd4',
		},
		style: [ { selector: '{{WOPB}} .wopb-product-wrapper:hover' } ],
	},
	wrapInnerPadding: {
		type: 'object',
		default: { lg: { unit: 'px' } },
		style: [
			{
				selector:
					'{{WOPB}} .wopb-product-wrapper{ padding:{{wrapInnerPadding}}; }',
			},
		],
	},
	hideExtraLarge: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideDesktop: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideTablet: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	hideMobile: {
		type: 'boolean',
		default: false,
		style: [
			{
				selector:
					'{{WOPB}} {display:none;} .block-editor-block-list__block {{WOPB}} {display:block;}',
			},
		],
	},
	advanceCss: { type: 'string', default: '', style: [ { selector: '' } ] },
};

export default attributes;
