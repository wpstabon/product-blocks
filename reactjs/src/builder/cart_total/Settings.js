/** @format */

const { __ } = wp.i18n;
import { settingsIcon } from '../../helper/CommonPanel';
import WopbToolbarDropdown from '../../helper/toolbar/WopbToolbarDropdown';
import WopbToolbarGroup from '../../helper/toolbar/WopbToolbarGroup';
import { formatSettingsForToolbar, getData } from '../../helper/ux';

export function AddSettingsToToolbar( { store, selected } ) {
	switch ( selected ) {
		case 'general':
			return (
				<WopbToolbarGroup text={ 'General' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'General Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'General Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'text',
											key: 'cartTotalTxt',
											label: __(
												'Cart Total Text',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'text',
											key: 'subTotalTxt',
											label: __(
												'Subtotal Text',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'text',
											key: 'totalTxt',
											label: __(
												'Total Text',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'text',
											key: 'checkoutTxt',
											label: __(
												'Checkout Text',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'tableBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'tableRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'tablePadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'header':
			return (
				<WopbToolbarGroup text={ 'Header' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Header Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Header Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'headerTextColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'headerTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'headerBgColor',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'headerBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'headerRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'headerPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'range',
											key: 'headingBottomSpace',
											label: __(
												'Bottom Space',
												'product-blocks'
											),
											min: 0,
											max: 80,
											step: 1,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'table-body':
			return (
				<WopbToolbarGroup text={ 'Table Body' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __( 'Table Body Settings', 'product-blocks' ) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Table Body Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'titleTextColor',
											label: __(
												'Text Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'priceTextColor',
											label: __(
												'Price Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'titleTypo',
											label: __(
												'Title Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'priceTypo',
											label: __(
												'Price Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'bodyBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'tableBodyBgColor',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'bodyPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'bodySpacing',
											label: __(
												'Inner Spacing',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		case 'proceed-to-checkout':
			return (
				<WopbToolbarGroup text={ 'Proceed to Checkout' } textScroll>
					<WopbToolbarDropdown
						buttonContent={ settingsIcon }
						store={ store }
						label={ __(
							'Proceed to Checkout Settings',
							'product-blocks'
						) }
						content={ formatSettingsForToolbar( [
							{
								name: 'tab',
								title: __(
									'Proceed to Checkout Settings',
									'product-blocks'
								),
								options: getData( [
									{
										data: {
											type: 'color',
											key: 'checkoutTextColor',
											label: __(
												'Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'checkoutHoverTextColor',
											label: __(
												'Hover Color',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'checkoutBgColor',
											label: __(
												'Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'color',
											key: 'checkoutHoverBgColor',
											label: __(
												'Hover Background',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'typography',
											key: 'checkoutTypo',
											label: __(
												'Typography',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'border',
											key: 'checkoutBorder',
											label: __(
												'Border',
												'product-blocks'
											),
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'checkoutMargin',
											label: __(
												'Margin',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'checkoutRadius',
											label: __(
												'Border Radius',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
									{
										data: {
											type: 'dimension',
											key: 'checkoutPadding',
											label: __(
												'Padding',
												'product-blocks'
											),
											step: 1,
											unit: true,
											responsive: true,
										},
									},
								] ),
							},
						] ) }
					/>
				</WopbToolbarGroup>
			);
		// case "":
		//     return (
		//         <WopbToolbarGroup text={"Image"} textScroll>
		//             <WopbToolbarDropdown
		//                 buttonContent={settingsIcon}
		//                 store={store}
		//                 label={__("Product Image Settings", "product-blocks")}
		//                 content={formatSettingsForToolbar([
		//                     {
		//                         name: "tab",
		//                         title: __(
		//                             "Product Image Settings",
		//                             "product-blocks"
		//                         ),
		//                         options: getData(),
		//                     },
		//                 ])}
		//             />
		//         </WopbToolbarGroup>
		//     );
		default:
			return null;
	}
}
