const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
import Edit from './Edit';
import attributes from './attributes';

registerBlockType( 'product-blocks/cart-total', {
	title: __( 'Cart Total', 'product-blocks' ),
	icon: (
		<img
			src={ wopb_data.url + 'assets/img/blocks/cart_total.svg' }
			alt={ __( 'Image', 'product-blocks' ) }
			style={ { width: '100%' } }
		/>
	),
	category: 'single-cart',
	description: __(
		'Dynamically control position, size, and typography of cart total.',
		'product-blocks'
	),
	keywords: [
		__( 'cart', 'product-blocks' ),
		__( 'total', 'product-blocks' ),
		__( 'cart total', 'product-blocks' ),
	],
	attributes,
	edit: Edit,
	save() {
		return null;
	},
} );
