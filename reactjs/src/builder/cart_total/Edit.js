/** @format */

const { __ } = wp.i18n;
const { InspectorControls } = wp.blockEditor;
import { CssGenerator } from '../../helper/CssGenerator';
import {
	GeneralAdvanced,
	CustomCssAdvanced,
	CommonSettings,
	ResponsiveAdvanced,
	wopbSupport,
} from '../../helper/CommonPanel';
import { Sections, Section } from '../../helper/Sections';
import useFluentSettings from '../../helper/hooks/useFluentSettings';
import { AddSettingsToToolbar } from './Settings';
import ToolBarElement from '../../helper/ToolBarElement';
import useBlockId from '../../helper/hooks/use-block-id';

export default function Edit( props ) {
	const {
		setAttributes,
		name,
		attributes,
		clientId,
		className,
		attributes: {
			blockId,
			advanceId,
			cartTotalTxt,
			subTotalTxt,
			totalTxt,
			checkoutTxt,
		},
	} = props;
	const store = { setAttributes, name, attributes, clientId };

	const { section, toolbarSettings, setCurrSettings } = useFluentSettings();

	useBlockId( props, true );

	if ( blockId ) {
		CssGenerator( attributes, 'product-blocks/cart-total', blockId );
	}

	const priceText = (
		<span className="woocommerce-Price-amount amount">
			<bdi>
				<span className="woocommerce-Price-currencySymbol">$</span>
				123.00
			</bdi>
		</span>
	);

	return (
		<>
			<ToolBarElement
				store={ store }
				include={ [
					{
						type: 'block_spacing',
						label: __( 'Cart Total Spacing', 'product-blocks' ),
					},
				] }
			/>
			<InspectorControls>
				<Sections>
					<Section
						slug="setting"
						title={ __( 'Settings', 'product-blocks' ) }
					>
						<CommonSettings
							store={ store }
							initialOpen={ section.general }
							title={ __( 'General', 'product-blocks' ) }
							include={ [
								{
									data: {
										type: 'text',
										key: 'cartTotalTxt',
										label: __(
											'Cart Total Text',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'text',
										key: 'subTotalTxt',
										label: __(
											'Subtotal Text',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'text',
										key: 'totalTxt',
										label: __(
											'Total Text',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'text',
										key: 'checkoutTxt',
										label: __(
											'Checkout Text',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'tableBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'tableRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'tablePadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							store={ store }
							initialOpen={ section.header }
							title={ __( 'Header', 'product-blocks' ) }
							include={ [
								{
									data: {
										type: 'color',
										key: 'headerTextColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'headerTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'headerBgColor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'headerBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'headerRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'headerPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'range',
										key: 'headingBottomSpace',
										label: __(
											'Bottom Space',
											'product-blocks'
										),
										min: 0,
										max: 80,
										step: 1,
									},
								},
							] }
						/>
						<CommonSettings
							store={ store }
							initialOpen={ 'table-body' }
							title={ __( 'Table Body', 'product-blocks' ) }
							include={ [
								{
									data: {
										type: 'color',
										key: 'titleTextColor',
										label: __(
											'Text Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'priceTextColor',
										label: __(
											'Price Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'titleTypo',
										label: __(
											'Title Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'priceTypo',
										label: __(
											'Price Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'bodyBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'tableBodyBgColor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'bodyPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'bodySpacing',
										label: __(
											'Inner Spacing',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
						<CommonSettings
							store={ store }
							initialOpen={ section[ 'proceed-to-checkout' ] }
							title={ __(
								'Proceed to Checkout',
								'product-blocks'
							) }
							include={ [
								{
									data: {
										type: 'color',
										key: 'checkoutTextColor',
										label: __( 'Color', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'color',
										key: 'checkoutHoverTextColor',
										label: __(
											'Hover Color',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'checkoutBgColor',
										label: __(
											'Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'color',
										key: 'checkoutHoverBgColor',
										label: __(
											'Hover Background',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'typography',
										key: 'checkoutTypo',
										label: __(
											'Typography',
											'product-blocks'
										),
									},
								},
								{
									data: {
										type: 'border',
										key: 'checkoutBorder',
										label: __( 'Border', 'product-blocks' ),
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'checkoutMargin',
										label: __( 'Margin', 'product-blocks' ),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'checkoutRadius',
										label: __(
											'Border Radius',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
								{
									data: {
										type: 'dimension',
										key: 'checkoutPadding',
										label: __(
											'Padding',
											'product-blocks'
										),
										step: 1,
										unit: true,
										responsive: true,
									},
								},
							] }
						/>
					</Section>
					<Section
						slug="advanced"
						title={ __( 'Advanced', 'product-blocks' ) }
					>
						<GeneralAdvanced initialOpen={ true } store={ store } />
						<ResponsiveAdvanced pro={ true } store={ store } />
						<CustomCssAdvanced store={ store } />
					</Section>
				</Sections>
				{ wopbSupport() }
			</InspectorControls>

			<AddSettingsToToolbar
				store={ store }
				selected={ toolbarSettings }
			/>

			<div
				{ ...( advanceId && { id: advanceId } ) }
				className={ `wopb-block-${ blockId } ${ className }` }
				onClick={ ( e ) => setCurrSettings( e, 'general' ) }
			>
				<div className={ 'wopb-product-wrapper' }>
					<div className="wopb-cart-total calculated_shipping">
						{ cartTotalTxt && (
							<div
								className="wopb-table-heading wopb-component-simple"
								onClick={ ( e ) =>
									setCurrSettings( e, 'header' )
								}
							>
								{ cartTotalTxt }
							</div>
						) }
						<div className="wopb-cart-total-section">
							<table
								cellSpacing="0"
								className="shop_table shop_table_responsive"
								onClick={ ( e ) =>
									setCurrSettings( e, 'table-body' )
								}
							>
								<tbody>
									<tr className="cart-subtotal">
										<th
											className="wopb-component-simple"
											onClick={ ( e ) =>
												setCurrSettings( e, 'general' )
											}
										>
											{ subTotalTxt }
										</th>
										<td className="wopb-total-price wopb-component-simple">
											{ priceText }
										</td>
									</tr>
									<tr className="shipping">
										<th
											className="wopb-component-simple"
											onClick={ ( e ) =>
												setCurrSettings( e, 'general' )
											}
										>
											Shipping
										</th>
										<td className="wopb-total-price wopb-component-simple">
											Calculate shipping
										</td>
									</tr>
									<tr className="order-total">
										<th
											className="wopb-component-simple wopb-component-simple"
											onClick={ ( e ) =>
												setCurrSettings( e, 'general' )
											}
										>
											{ totalTxt }
										</th>
										<td className="wopb-total-price">
											{ priceText }
										</td>
									</tr>
								</tbody>
							</table>
							<div
								className="wc-proceed-to-checkout wopb-component-simple"
								onClick={ ( e ) =>
									setCurrSettings( e, 'proceed-to-checkout' )
								}
							>
								<a
									href="#"
									className="checkout-button button alt wc-forward"
								>
									{ checkoutTxt } &rarr;
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
}
