const { __ } = wp.i18n;
import { SketchPicker } from 'react-color';

const Color = ( props ) => {
	const {
		formData,
		setFormData,
		colorPicker,
		openColorPicker,
		val,
		fieldKey,
		parentObj,
		childField,
		value,
	} = props;
	const pickerKey = fieldKey + ( childField ? '_' + childField : '' );
	const handleColorChange = ( color ) => {
		const rgbCode = `rgba(${ color.rgb.r }, ${ color.rgb.g }, ${ color.rgb.b }, ${ color.rgb.a })`;
		setFormData( {
			...formData,
			[ fieldKey ]: childField
				? { ...parentObj, [ childField ]: rgbCode }
				: rgbCode,
		} );
	};
	const resetButton = () => {
		if ( childField && value.default?.[ childField ] === val ) {
			return;
		} else if ( value?.default === val ) {
			return;
		}
		return (
			<span
				className="wopb-reset-settings dashicons dashicons-image-rotate"
				onClick={ () => {
					setFormData( {
						...formData,
						[ fieldKey ]: childField
							? {
									...parentObj,
									[ childField ]:
										value.default?.[ childField ],
							  }
							: value?.default,
					} );
				} }
			/>
		);
	};
	const emptyColor = () => {
		return (
			<div
				className="wopb-empty-color-wrap"
				onClick={ () => {
					setFormData( {
						...formData,
						[ fieldKey ]: childField
							? { ...parentObj, [ childField ]: '' }
							: '',
					} );
				} }
			>
				<span className="wopb-empty-color"></span>
				{ __( 'None', 'product-blocks' ) }
			</div>
		);
	};

	return (
		<div className="wopb-field-color">
			<span className="wopb-color-select">
				<span
					className="wopb-color wopb-tooltip"
					style={ {
						...( val &&
							val !== 'transparent00' && { background: val } ),
					} }
					onClick={ () => {
						openColorPicker(
							colorPicker.open && colorPicker.key == pickerKey
								? { key: '', open: false }
								: { key: pickerKey, open: true }
						);
					} }
				>
					{ value?.tooltip && (
						<span className="wopb-tooltip-text">
							{ value?.tooltip }
						</span>
					) }
				</span>
				{ colorPicker.open && colorPicker.key == pickerKey && (
					<div className="wopb-color-popup">
						<div className="wopb-top-toolbar">
							{ resetButton() }
							{ emptyColor() }
						</div>
						<SketchPicker
							presetColors={ [
								'#FF176B',
								'#F5A623',
								'#F8E71C',
								'#8B572A',
								'#7ED321',
								'#417505',
								'#BD10E0',
								'#9013FE',
								'#4A90E2',
								'#50E3C2',
								'#B8E986',
								'#000000',
								'#4A4A4A',
								'#9B9B9B',
								'#FFFFFF',
							] }
							color={ val }
							onChange={ handleColorChange }
						/>
					</div>
				) }
			</span>
		</div>
	);
};
export default Color;
