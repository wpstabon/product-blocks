const { __ } = wp.i18n;
import Number from './Number';
const Dimension = ( props ) => {
	const { val } = props;
	props = { ...props, parentObj: val };
	return (
		<div className="wopb-dimension-field">
			{ [ 'top', 'bottom', 'left', 'right' ].map( ( key, index ) => (
				<span key={ index }>
					<Number
						{ ...{
							...props,
							childField: key,
							val: val[ key ] ?? '',
						} }
					/>
					<div
						style={ {
							width: '100%',
							textAlign: 'center',
							marginTop: '3px',
							fontSize: '11px',
							textTransform: 'capitalize',
						} }
					>
						{ key }
					</div>
				</span>
			) ) }
			<span className="wopb-unit">PX</span>
		</div>
	);
};
export default Dimension;
