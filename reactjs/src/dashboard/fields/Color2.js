const { __ } = wp.i18n;
import Color from './Color';

const Color2 = ( props ) => {
	const { val, value } = props;
	props = { ...props, parentObj: val };
	return (
		<div className="wopb-color-group">
			{ value.field1 && (
				<Color
					{ ...{
						...props,
						childField: value.field1 ?? '',
						val:
							val && value.field1 && val[ value.field1 ]
								? val[ value.field1 ]
								: '',
						value: {
							...value,
							tooltip: __( 'Normal', 'product-blocks' ),
						},
					} }
				/>
			) }
			{ value.field2 && (
				<Color
					{ ...{
						...props,
						childField: value.field2 ?? '',
						val:
							val && value.field2 && val[ value.field2 ]
								? val[ value.field2 ]
								: '',
						value: {
							...value,
							tooltip: __( 'Hover', 'product-blocks' ),
						},
					} }
				/>
			) }
		</div>
	);
};
export default Color2;
