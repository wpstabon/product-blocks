const { __ } = wp.i18n;
import icons from '../../helper/icons';
const Number = ( props ) => {
	const {
		formData,
		setFormData,
		val,
		fieldKey,
		parentObj,
		childField,
		value,
	} = props;
	const setValue = ( data, event ) => {
		if ( event && event.type === 'blur' && data === '' ) {
			data = 0;
		} else {
			data = data < 0 ? 0 : data;
		}
		setFormData( {
			...formData,
			[ fieldKey ]:
				childField && parentObj
					? { ...parentObj, [ childField ]: data }
					: data,
		} );
	};
	return (
		<div className="wopb-number-field wopb-tooltip">
			<input
				type="number"
				name={ fieldKey }
				value={ val ?? 0 }
				onChange={ ( event ) => {
					setValue( event.target.value );
				} }
				onBlur={ ( event ) => {
					setValue( event.target.value, event );
				} }
			/>
			<div className="wopb-arrow-control">
				<span
					onClick={ () => {
						setValue( val ? parseInt( val ) + 1 : 1 );
					} }
					onMouseDown={ ( event ) => event.preventDefault() }
				>
					{ icons.upArrow }
				</span>
				<span
					onClick={ () => {
						setValue( val - 1 );
					} }
					onMouseDown={ ( event ) => event.preventDefault() }
				>
					{ icons.downArrow }
				</span>
			</div>
			{ value && value.tooltip && (
				<span className="wopb-tooltip-text">{ value.tooltip }</span>
			) }
		</div>
	);
};
export default Number;
