const { __ } = wp.i18n;
import icons from '../../helper/icons';
import Number from './Number';
import Color from './Color';
import Color2 from './Color2';
const Typography = ( props ) => {
	const { formData, setFormData, val, value, fieldKey } = props;
	props = { ...props, parentObj: val };
	return (
		<div className="wopb-typo-field">
			{
				<Number
					{ ...{
						...props,
						childField: 'size',
						val: val?.size,
					} }
				/>
			}
			{ ( 'bold' in value?.default ||
				'italic' in value?.default ||
				'underline' in value?.default ) && (
				<div className="wopb-tools-item">
					<span
						className={
							'wopb-tooltip' + ( val?.bold ? ' wopb-active' : '' )
						}
						onClick={ () => {
							setFormData( {
								...formData,
								[ fieldKey ]: {
									...val,
									bold:
										typeof value?.default?.bold !==
											'boolean' && ! val?.bold
											? value?.default?.bold
											: ! val?.bold,
								},
							} );
						} }
					>
						{ icons.bold }
						<span className="wopb-tooltip-text">
							{ __( 'Bold', 'product-blocks' ) }
						</span>
					</span>
					<span
						className={
							'wopb-tooltip' +
							( val?.italic ? ' wopb-active' : '' )
						}
						onClick={ () =>
							setFormData( {
								...formData,
								[ fieldKey ]: { ...val, italic: ! val?.italic },
							} )
						}
					>
						{ icons.italic }
						<span className="wopb-tooltip-text">
							{ __( 'Italic', 'product-blocks' ) }
						</span>
					</span>
					<span
						className={
							'wopb-tooltip' +
							( val?.underline ? ' wopb-active' : '' )
						}
						onClick={ () =>
							setFormData( {
								...formData,
								[ fieldKey ]: {
									...val,
									underline: ! val?.underline,
								},
							} )
						}
					>
						{ icons.underLine }
						<span className="wopb-tooltip-text">
							{ __( 'Underline', 'product-blocks' ) }
						</span>
					</span>
				</div>
			) }
			<div className="wopb-color-group">
				<Color
					{ ...{
						...props,
						childField: 'color',
						val: val?.color,
						value: {
							...value,
							tooltip: __( 'Normal', 'product-blocks' ),
						},
					} }
				/>
				{ 'hover_color' in value?.default && (
					<Color
						{ ...{
							...props,
							childField: 'hover_color',
							val: val?.hover_color,
							value: {
								...value,
								tooltip: __( 'Hover', 'product-blocks' ),
							},
						} }
					/>
				) }
			</div>
		</div>
	);
};
export default Typography;
