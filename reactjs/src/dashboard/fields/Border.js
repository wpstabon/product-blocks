const { __ } = wp.i18n;
import Number from './Number';
import Color from './Color';
const Border = ( props ) => {
	const { val, value, fieldKey, setFormData, formData } = props;
	props = { ...props, parentObj: val };
	return (
		<div className="wopb-border-field">
			{
				<Number
					{ ...{
						...props,
						childField: 'border',
						val: val?.border ?? value?.default?.border,
					} }
				/>
			}
			{
				<>
					<Color
						{ ...{
							...props,
							childField: 'color',
							val: val?.color ?? value?.default?.color,
							value: {
								...value,
								tooltip: __( 'Color', 'product-blocks' ),
							},
						} }
					/>
				</>
			}
		</div>
	);
};
export default Border;
