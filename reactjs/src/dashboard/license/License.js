import React, { useState, useEffect } from 'react';
import Toast from '../utility/Toast';
import './license.scss';
const { __ } = wp.i18n;

const License = ( props ) => {
	const { expire } = props;
	const [ licenseKey, setLicenseKey ] = useState(
		wopb_dashboard_pannel.license
	);
	const [ setting, setSetting ] = useState( false );
	const [ toastMessages, setToastMessages ] = useState( {
		state: false,
		status: '',
	} );
	const [ type, setType ] = useState( props.status );
	const [ saving, setSaving ] = useState( false );

	useEffect( () => {
		setType( props.status );
	}, [ props.status ] );

	const licenseAction = () => {
		setSaving( true );
		wp.apiFetch( {
			path: '/wopb/v2/dashborad',
			method: 'POST',
			data: { type: 'license_action', edd_wopb_license_key: licenseKey },
		} ).then( ( res ) => {
			if ( res.success ) {
				setType( 'valid' );
				setSetting( false );
			} else if ( res.requirePlugin ) {
				setType( '' );
				setSetting( true );
			} else {
				setType( 'invalid' );
			}
			setSaving( false );
			setToastMessages( {
				status: res.success ? 'success' : 'error',
				messages: [ res.message ],
				state: true,
			} );
		} );
	};

	const licenseContent = () => {
		if ( type == 'valid' && ! setting && wopb_dashboard_pannel.active ) {
			return (
				<div className="wopb-dash-item-con licenseActivated">
					<span className="dashicons dashicons-saved activatedIcon"></span>
					<div className="activationMessage">
						{ __( 'Your License is', 'product-blocks' ) }{ ' ' }
						<strong>
							{ __( 'Activated.', 'product-blocks' ) }
						</strong>
						{ expire && (
							<>
								{ __( 'Expiry Date:', 'product-blocks' ) }
								<strong>{ expire }</strong>
							</>
						) }
					</div>
					<div className="deactiveMessage">
						{ __(
							'Want to reset this license?',
							'product-blocks'
						) }
						<a
							onClick={ ( e ) => {
								setSetting( true );
							} }
							className="cursor deactiveLink"
						>
							{ __( 'Click Here.', 'product-blocks' ) }
						</a>
					</div>
				</div>
			);
		} else if ( type == 'invalid' && ! setting ) {
			return (
				<div className="wopb-dash-item-con licenseExpired">
					<span className="dashicons dashicons-megaphone expiredIcon"></span>
					<div className="expiredMessage">
						{ __( 'Your License is', 'product-blocks' ) }
						<strong>
							{ __( 'Invalid / Expired.', 'product-blocks' ) }
						</strong>
						{ __(
							'To unlock pro features, please',
							'product-blocks'
						) }{ ' ' }
						<a
							target="_blank"
							className="renewLink"
							href="https://www.wpxpo.com/wowstore/pricing/?utm_source=wstore-menu&utm_medium=license_page-guide-renew&utm_campaign=wstore-dashboard"
							rel="noreferrer"
						>
							{ __( 'Renew your license', 'product-blocks' ) }
						</a>
					</div>
					<div className="deactiveMessage">
						{ __( 'Want to add a new license?', 'product-blocks' ) }{ ' ' }
						<a
							onClick={ () => setSetting( true ) }
							className="cursor deactiveLink"
						>
							{ __( 'Click here', 'product-blocks' ) }
						</a>
					</div>
				</div>
			);
		}
		return (
			<div className="wopb-dash-item-con newLicense">
				<label htmlFor="wopb_license_key" className="newLicenseLabel">
					{ __( 'Add New License', 'product-blocks' ) }
				</label>
				<div className="newLicenseFields">
					<input
						type="password"
						value={ licenseKey }
						name="edd_wopb_license_key"
						placeholder="Enter your license key here"
						onChange={ ( e ) => {
							setLicenseKey( e.target.value );
						} }
					></input>
					<button
						onClick={ () => {
							licenseAction();
						} }
						className="wopb-primary-button cursor"
					>
						{ __( 'Activate License', 'product-blocks' ) }
						{ saving && (
							<span className="dashicons dashicons-update rotate" />
						) }
					</button>
				</div>
				<p className="wopb-description">
					{ __( 'Enter your license key', 'product-blocks' ) }
				</p>
			</div>
		);
	};

	const licenseDoc = () => {
		if (
			type == 'valid' &&
			! setting &&
			wopb_dashboard_pannel.active &&
			expire !== 'lifetime'
		) {
			return (
				<>
					<div className="wopb-heading">
						{ __( 'Upgrade License', 'product-blocks' ) }
					</div>
					<div className="wopb-description">
						{ __(
							'Enjoy a special discount while upgrading your existing license.',
							'product-blocks'
						) }
					</div>
					<a
						href="https://www.wpxpo.com/account/?utm_source=wstore-menu&utm_medium=license_page-guide-upgrade&utm_campaign=wstore-dashboard"
						target="_blank"
						className="wopb-upgrade-pro-btn"
						rel="noreferrer"
					>
						{ __( 'Upgrade Now', 'product-blocks' ) }
					</a>
				</>
			);
		} else if ( type == 'invalid' && ! setting ) {
			return (
				<>
					<div className="wopb-heading">
						{ __(
							'Want to get rid of the renewal hassle?',
							'product-blocks'
						) }
					</div>
					<div className="wopb-description">
						{ __(
							'Purchase a lifetime plan of WowStore and keep using all pro features forever without any renewal charges. Use this coupon,',
							'product-blocks'
						) }
						<strong>
							{ __(
								'{ special30 } to get a 30%',
								'product-blocks'
							) }
						</strong>
						{ __(
							'discount for any lifetime plan.',
							'product-blocks'
						) }
					</div>
					<a
						href="https://www.wpxpo.com/wowstore/pricing/?utm_source=wstore-menu&utm_medium=license_page-renewal_discount&utm_campaign=wstore-dashboard"
						target="_blank"
						className="wopb-upgrade-pro-btn"
						rel="noreferrer"
					>
						{ __( 'Claim Your Discount', 'product-blocks' ) }
					</a>
				</>
			);
		}
		return (
			<>
				<div className="wopb-heading">
					{ __(
						'Just Getting Started with WowStore?',
						'product-blocks'
					) }
				</div>
				<div className="wopb-description">
					{ __(
						'Check out the documentation to learn how you can effectively use the features of WowStore.',
						'product-blocks'
					) }
				</div>
				<a
					href="https://wpxpo.com/docs/wowstore/"
					target="_blank"
					className="wopb-upgrade-pro-btn"
					rel="noreferrer"
				>
					{ __( 'Explore Documentation', 'product-blocks' ) }
				</a>
			</>
		);
	};
	const licenseInstruction = () => {
		if (
			type == 'valid' &&
			! setting &&
			wopb_dashboard_pannel.active &&
			expire !== 'lifetime'
		) {
			return (
				<>
					<div className="wopb-heading">
						{ __(
							'How to upgrade the existing plan?',
							'product-blocks'
						) }
					</div>
					<div className="guidMessage">
						{ __(
							'You can use WowStore Pro on multiple sites and for lifetime without any renewal charges. Follow the below steps to update your plan.',
							'product-blocks'
						) }
					</div>
					<div className="guidMessage">
						<div className="wopb-heading">
							{ __( 'Step 1:', 'product-blocks' ) }
						</div>
						{ __( 'Go to wpxpo.com', 'product-blocks' ) }
						<strong>
							{ __(
								'> my account > My Order > View Licenses > View Upgrades.',
								'product-blocks'
							) }
						</strong>
						{ __(
							'And then click on the Update License Button.',
							'product-blocks'
						) }
					</div>
					<div className="guidMessage">
						<div className="wopb-heading">
							{ __( 'Step 2:', 'product-blocks' ) }
						</div>
						{ __(
							'Complete the Payment and enjoy your plan.',
							'product-blocks'
						) }
					</div>
				</>
			);
		} else if ( type == 'invalid' && ! setting ) {
			return (
				<>
					<div className="wopb-heading">
						{ __(
							'How to renew the existing license?',
							'product-blocks'
						) }
					</div>
					<div className="guidMessage">
						<div className="wopb-heading">
							{ __( 'Step 1:', 'product-blocks' ) }
						</div>
						{ __(
							'To renew your existing license you need to go to wpxpo.com',
							'product-blocks'
						) }
						<strong>
							{ __(
								'> My account > My Order > View Licenses.',
								'product-blocks'
							) }
						</strong>
					</div>
					<div className="guidMessage">
						<div className="wopb-heading">
							{ __( 'Step 2:', 'product-blocks' ) }
						</div>
						{ __(
							'Then click on the “renew now” button and complete the payment process.',
							'product-blocks'
						) }
					</div>
				</>
			);
		}
		return (
			<>
				<div className="wopb-heading">
					{ __( 'How to use license key?', 'product-blocks' ) }
				</div>
				<div className="guidMessage">
					<div className="errorNote">
						<strong>{ __( 'Note:', 'product-blocks' ) }</strong>
						{ __(
							'Make sure you have installed both the',
							'product-blocks'
						) }
						<strong>
							{ __(
								'free and pro version of WowStore.',
								'product-blocks'
							) }
						</strong>
					</div>
				</div>
				<div className="guidMessage">
					<div className="wopb-heading">
						{ __( 'Step 1:', 'product-blocks' ) }
					</div>
					{ __( 'Go to wpxpo.com', 'product-blocks' ) }
					<strong>
						{ __(
							'> My Account > Order > View License.',
							'product-blocks'
						) }
					</strong>
					{ __(
						'Then Click Key Icon and copy the license key.',
						'product-blocks'
					) }
				</div>
				<div className="guidMessage">
					<div className="wopb-heading">
						{ __( 'Step 2:', 'product-blocks' ) }
					</div>
					{ __(
						'Paste the copied license on the above field. Then Click on the Activate License Button.',
						'product-blocks'
					) }
				</div>
			</>
		);
	};

	return (
		<div className="licenseWrapper">
			{ toastMessages.state && (
				<Toast
					delay={ 2000 }
					toastMessages={ toastMessages }
					setToastMessages={ setToastMessages }
				/>
			) }
			<div className="wopbLicense">
				{ licenseContent() }
				<div className="licenseGuide">
					<div className="licenseGuideLeft">
						{ licenseInstruction() }
					</div>
					<div className="licenseGuideRight wopb-dash-item-con">
						{ licenseDoc() }
					</div>
				</div>
			</div>
		</div>
	);
};

export default License;
