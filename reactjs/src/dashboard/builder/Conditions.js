import ExtendFields from './ExtendFields';
import Fields from './Fields';
import { useState } from 'react';

const Conditions = ( props ) => {
	const { id, type, settings, defaults, setShowCondition } = props;
	const [ value, setValue ] = useState(
		id &&
			typeof settings[ type ] !== 'undefined' &&
			typeof settings[ type ][ id ] !== 'undefined'
			? settings[ type ][ id ]
			: [ 'include/' + type ]
	);
	const [ saveData, setSaveData ] = useState( {
		reload: false,
		dataSaved: false,
	} );

	return (
		<div className="wopb-modal-content">
			<div className="wopb-condition-wrap">
				<div className="wopb-heading">
					Where Do You Want to Display Your Template?
				</div>
				<p className="wopb-description">
					Set the conditions that determine where your Template is
					used throughout your site.
				</p>
				<div className="wopb-condition-items">
					{ value.map( ( val, k ) => {
						if ( val ) {
							return (
								<div
									key={ k }
									className="wopb-condition-wrap__field"
								>
									{ type == 'header' || type == 'footer' ? (
										<ExtendFields
											key={ k }
											id={ id }
											index={ k }
											type={ type }
											value={ val }
											defaults={ defaults }
											setChange={ ( v, _k ) => {
												setSaveData( {
													dataSaved: false,
												} );
												const _v = JSON.parse(
													JSON.stringify( value )
												);
												_v[ _k ] = v;
												setValue( _v );
											} }
										/>
									) : (
										<Fields
											key={ k }
											id={ id }
											index={ k }
											type={ type }
											value={ val }
											defaults={ defaults }
											setChange={ ( v, _k ) => {
												setSaveData( {
													dataSaved: false,
												} );
												const _v = JSON.parse(
													JSON.stringify( value )
												);
												_v[ _k ] = v;
												setValue( _v );
											} }
										/>
									) }

									<span
										className="dashicons dashicons-no-alt wopb-condition_cancel"
										onClick={ () => {
											setSaveData( { dataSaved: false } );
											const _v = JSON.parse(
												JSON.stringify( value )
											);
											_v.splice( k, 1 );
											setValue( _v );
										} }
									></span>
								</div>
							);
						}
					} ) }
				</div>
				<button
					className={ `btnCondition cursor` }
					onClick={ () => {
						setValue( [
							...value,
							'include/' +
								( [ 'header', 'footer' ].includes( type )
									? type + '/entire_site'
									: type ),
						] );
					} }
				>
					Add Conditions
				</button>
			</div>
			<button
				className="wopb-save-condition cursor"
				onClick={ () => {
					setSaveData( { reload: true } );
					const _v = Object.assign( {}, settings );
					if ( typeof _v[ type ] !== 'undefined' ) {
						_v[ type ][ id ] = value.filter( ( n ) => n );
					} else {
						_v[ type ] = {};
						_v[ type ][ id ] = value.filter( ( n ) => n );
					}

					wp.apiFetch( {
						path: '/wopb/v2/condition_save',
						method: 'POST',
						data: { settings: _v },
					} ).then( ( response ) => {
						if ( response.success ) {
							setSaveData( { reload: false, dataSaved: true } );
							setTimeout( function () {
								setSaveData( {
									reload: false,
									dataSaved: false,
								} );
								setShowCondition && setShowCondition( '' );
							}, 2000 );
						}
					} );
				} }
			>
				{ saveData.dataSaved ? 'Condition Saved.' : 'Save Condition' }
				<span
					style={ {
						visibility: `${
							saveData.reload ? 'visible' : 'hidden'
						}`,
					} }
					className="dashicons dashicons-update rotate wopb-builder-import"
				/>
			</button>
		</div>
	);
};
export default Conditions;
