const { __ } = wp.i18n;
import { useState, useEffect, Fragment } from 'react';
import Conditions from './Conditions';
import LinkGenerator from '../../helper/LinkGenerator';
import './builder.scss';
import IconPack from '../../helper/fields/tools/IconPack';
import { getProHtml } from '../Settings';
import Toast from '../../dashboard/utility/Toast';
import Skeleton from '../utility/Skeleton';

const Panel = ( props ) => {
	const { notEditor } = props;
	const defaultType = [
		'home_page',
		'single_product',
		'shop',
		'cart',
		'checkout',
		'my_account',
		'thank_you',
		'product_search',
		'archive',
		'header',
		'footer',
		'404',
	]; // Default menu items
	const [ type, setType ] = useState( '' ); // Type
	const [ postList, setPostList ] = useState( [] ); // List of Save Posts
	const [ menu, setMenu ] = useState( 'all' ); // Menu active deactive check
	const [ isNew, setIsNew ] = useState( false ); // New screen check
	const [ settings, setSettings ] = useState( [] ); // Current Save Settings
	const [ showCondition, setShowCondition ] = useState( notEditor || '' ); // Condition Popup
	const [ defaults, setDefault ] = useState( [] ); // Default Array
	const [ fetch, setFetch ] = useState( false ); // Default Array
	const [ premade, setPremade ] = useState( '' ); // Default String
	const [ premadeList, setPremadeList ] = useState( [] ); // Default String
	const [ reload, setReload ] = useState( '' ); // Reload Animation Toggle
	const [ isLock, setIsLock ] = useState( false ); // Reload Animation Toggle
	const [ isError, setError ] = useState( false ); // Reload Animation Toggle
	const [ isLoading, setLoading ] = useState( false ); // Reload Animation Toggle
	const [ valAction, setAction ] = useState( '' );
	const [ actionPop, setActionPop ] = useState( false );
	const postId =
		notEditor == 'yes'
			? wp.data.select( 'core/editor' ).getCurrentPostId()
			: '';

	const [ toastMessages, setToastMessages ] = useState( {
		state: false,
		status: '',
	} );
	const [ backOpt, setBackOpt ] = useState( false );
	let defScratchIds = {};

	const setList = async () => {
		await wp
			.apiFetch( {
				path: '/wopb/v2/get_preset_data',
				method: 'POST',
				data: {
					type: 'all',
				},
			} )
			.then( ( response ) => {
				if ( response.success ) {
					if ( response.data ) {
						let data = JSON.parse( response.data );
						const _data = data;
						data = data.premade;
						if ( _data.template ) {
							data = data.concat(
								_data.template.map( ( m ) => {
									m.category = [
										{
											name: 'Home Page',
											slug: 'home_page',
										},
									];
									return m;
								} )
							);
						}
						setPremadeList( data );
					}
				}
			} );
	};

	const fetchData = async () => {
		await wp
			.apiFetch( {
				path: '/wopb/v2/data_builder',
				method: 'POST',
				data: { pid: postId },
			} )
			.then( ( response ) => {
				if ( response.success ) {
					setPostList( response.postlist );
					setSettings( response.settings );
					setDefault( response.defaults );
					setType( response.type );
					setLoading( true );
				}
			} );
	};

	const handleClickOutside = ( e ) => {
		if (
			e.target &&
			! e.target.classList?.contains( 'wopb-reserve-button' )
		) {
			setAction( '' );
			setActionPop( false );
		}
	};

	useEffect( () => {
		setList();
		fetchData();
		document.addEventListener( 'mousedown', handleClickOutside );
		return () =>
			document.removeEventListener( 'mousedown', handleClickOutside );
	}, [] );

	const importAction = ( id ) => {
		if ( ! id ) {
			id = defScratchIds[ premade ];
		}
		wp.apiFetch( {
			path: '/wopb/v2/get_single_premade',
			method: 'POST',
			data: { type: premade, ID: id },
		} ).then( ( response ) => {
			if ( response.success ) {
				setReload( '' );
				window.open(
					response.link.replaceAll( '&amp;', '&' ),
					'_blank'
				);
			} else {
				setIsLock( true );
				setError( true );
			}
		} );
	};

	const _fetchFile = () => {
		setFetch( true );
		wp.apiFetch( {
			path: '/wopb/v2/get_preset_data',
			method: 'POST',
			data: {
				type: 'reset',
			},
		} ).then( ( response ) => {
			if ( response.success ) {
				fetchData();
				setFetch( false );
			}
		} );
	};

	const filterData = function ( _list, _type ) {
		return _list.filter(
			( val, k ) => _type == val.type || _type == 'all'
		);
	};

	const templateSort = ( data ) => {
		if ( data ) {
			data.sort( ( a, b ) => {
				if ( a.pro && ! b.pro ) {
					return 1;
				}
				if ( ! a.pro && b.pro ) {
					return -1;
				}
				return 0;
			} );
		}
		return data;
	};

	const getPremadeScreen = () => {
		return (
			<div
				className={ `premadeScreen ${
					premade && 'wopb-builder-items'
				}` }
			>
				<div className="wopb-list-blank-img wopb-item-list wopb-premade-item">
					<div className="wopb-item-list-overlay wopb-p20 wopb-premade-img__blank">
						{ /*<img src={wopb_condition.url + 'assets/img/start-scratch.svg'}/>*/ }
						<a
							href="#"
							onClick={ ( e ) => {
								e.preventDefault();
								importAction();
							} }
						>
							<span className="wopb-list-white-overlay">
								<span className="dashicons dashicons-plus-alt" />{ ' ' }
								Start from Scratch{ ' ' }
							</span>
						</a>
					</div>
				</div>
				{ premadeList &&
					premadeList.length > 0 &&
					templateSort( premadeList ).map( ( val, k ) => {
						if ( val.category ) {
							return val.category.map( ( el ) => {
								if ( el.slug == premade ) {
									if ( val.dTemp ) {
										defScratchIds = {
											...defScratchIds,
											[ premade ]: val.ID,
										};
									}
									return (
										<div
											key={ k }
											className="wopb-item-list wopb-premade-item"
										>
											<div className="listInfo">
												<div className="title">
													{ val.name }
												</div>
												{ val.pro &&
												! wopb_condition.active ? (
													<a
														className="wopb-upgrade-pro-btn"
														target="_blank"
														href={ `https://www.wpxpo.com/wowstore/pricing/?utm_source=db-wstore-builder&utm_medium=${ val.name.replaceAll(
															' ',
															'-'
														) }-template&utm_campaign=wstore-dashboard` }
														rel="noreferrer"
													>
														{ __(
															'Upgrade to Pro',
															'product-blocks'
														) }
														&nbsp; &#10148;
													</a>
												) : val.pro && isError ? (
													<a
														className="wopb-btn-success"
														target="_blank"
														href={ `https://www.wpxpo.com/wowstore/pricing/?utm_source=db-wstore-builder&utm_medium=${ val.name.replaceAll(
															' ',
															'-'
														) }-template&utm_campaign=wstore-dashboard` }
														rel="noreferrer"
													>
														{ __(
															'Get License',
															'product-blocks'
														) }
													</a>
												) : (
													<span
														onClick={ () => {
															setReload( val.ID );
															importAction(
																val.ID
															);
														} }
														className="btnImport cursor"
													>
														{ __(
															'Import',
															'product-blocks'
														) }
														{ reload &&
														reload == val.ID ? (
															<span className="dashicons dashicons-update rotate" />
														) : (
															''
														) }
													</span>
												) }
											</div>
											<div className="listOverlay">
												<img
													src={ val.image }
													alt={ val.name }
												/>
											</div>
										</div>
									);
								}
							} );
						}
					} ) }
			</div>
		);
	};

	const getNewScreen = () => {
		return (
			<div className="wopb-builder-items">
				{ ( menu == 'all' ? defaultType : [ menu ] ).map(
					( val, k ) => {
						return (
							<div
								key={ k }
								onClick={ () => {
									setPremade( val );
									if ( premadeList.length <= 0 ) {
										setList();
									}
								} }
							>
								<div className="newScreen wopb-item-list wopb-premade-item">
									<div className="listInfo">
										{ val.replace( '_', ' ' ) }
									</div>
									<div className="listOverlays">
										<span className="wopb-list-white-overlay">
											<span className="dashicons dashicons-plus-alt" />
											Add { val.replace( '_', ' ' ) }
										</span>
									</div>
								</div>
							</div>
						);
					}
				) }
			</div>
		);
	};

	const getData = ( _type = 'all' ) => {
		const _data = filterData( postList, _type );
		return (
			<div className="wopb-template-list__tab">
				{ isNew == false && _data.length > 0 ? (
					_data.map( ( val, k ) => {
						return (
							<div
								key={ k }
								className="wopb-template-list__wrapper"
							>
								<div className="wopb-template-list__heading">
									<div className="wopb-template-list__meta">
										<div>
											<span>{ val.label } :</span>{ ' ' }
											{ val.title } <span>ID :</span> #
											{ val.id }
										</div>
										{ val.id &&
											typeof settings[ val.type ] !==
												'undefined' &&
											settings[ val.type ][ val.id ] && (
												<div className="wopb-condition__previews">
													(
													{ (
														settings[ val.type ][
															val.id
														] || []
													).map( ( el, k ) => {
														if ( el ) {
															const _v =
																el.split( '/' );
															return (
																<Fragment
																	key={ k }
																>
																	{ k == 0
																		? ''
																		: ', ' }
																	<span>
																		{ typeof _v[ 2 ] !==
																		'undefined'
																			? _v[ 2 ].replace(
																					'_',
																					' '
																			  )
																			: _v[ 1 ].replace(
																					'_',
																					' '
																			  ) }
																	</span>
																</Fragment>
															);
														}
													} ) }
													)
												</div>
											) }
									</div>
									<div className="wopb-template-list__control wopb-template-list__content">
										{ ( val.type === 'single_product' ||
											val.type === 'archive' ||
											val.type === 'header' ||
											val.type === 'footer' ) && (
											<button
												onClick={ () => {
													setType( val.type );
													setShowCondition( val.id );
												} }
												className="wopb-condition__edit"
											>
												Conditions
											</button>
										) }
										<a className="status">
											{ ' ' }
											{ val.status == 'publish'
												? 'Published'
												: val.status }
										</a>
										<a
											target="_blank"
											href={ val?.edit?.replaceAll(
												'&amp;',
												'&'
											) }
											className="wopb-condition-action"
											rel="noreferrer"
										>
											<span className="dashicons dashicons-edit-large" />
											Edit
										</a>
										<a
											className="wopb-condition-action wopb-single-popup__btn cursor"
											onClick={ ( e ) => {
												e.preventDefault();
												if (
													confirm(
														'Are you sure you want to duplicate this template?'
													)
												) {
													wp.apiFetch( {
														path: '/wopb/v2/template_action',
														method: 'POST',
														data: {
															id: val.id,
															type: 'duplicate',
														},
													} ).then( ( res ) => {
														if ( res.success ) {
															fetchData();
															setToastMessages( {
																status: 'success',
																messages: [
																	res.message,
																],
																state: true,
															} );
														}
													} );
												}
											} }
										>
											<span className="dashicons dashicons-admin-page" />
											Duplicate
										</a>

										<a
											className="wopb-condition-action wopb-single-popup__btn cursor"
											onClick={ ( e ) => {
												e.preventDefault();
												if (
													confirm(
														'Are you sure you want to delete this template?'
													)
												) {
													wp.apiFetch( {
														path: '/wopb/v2/template_action',
														method: 'POST',
														data: {
															id: val.id,
															type: 'delete',
															section: 'builder',
														},
													} ).then( ( res ) => {
														if ( res.success ) {
															setPostList(
																postList.filter(
																	( v ) =>
																		v.id !=
																		val.id
																)
															);
															setToastMessages( {
																status: 'error',
																messages: [
																	res.message,
																],
																state: true,
															} );
														}
													} );
												}
											} }
											href="#"
										>
											<span className="dashicons dashicons-trash" />
											Delete
										</a>
										<span
											onClick={ ( e ) => {
												setActionPop( ! actionPop );
												setAction( val.id );
											} }
										>
											<span className="dashicons dashicons-ellipsis wopb-builder-dashboard__action wopb-reserve-button"></span>
										</span>
										{ valAction == val.id && actionPop && (
											<span
												className={ `${
													valAction == val.id &&
													actionPop
														? 'wopb-builder-action__active wopb-reserve-button'
														: ''
												}` }
												onClick={ ( e ) => {
													setAction( '' );
													e.preventDefault();
													if (
														confirm(
															'Are you sure?'
														)
													) {
														wp.apiFetch( {
															path: '/wopb/v2/template_action',
															method: 'POST',
															data: {
																id: val.id,
																type: 'status',
																status:
																	val.status ==
																	'publish'
																		? 'draft'
																		: 'publish',
															},
														} ).then( ( res ) => {
															if ( res.success ) {
																fetchData();
																setToastMessages(
																	{
																		status: 'success',
																		messages:
																			[
																				res.message,
																			],
																		state: true,
																	}
																);
															}
														} );
													}
												} }
											>
												Set to{ ' ' }
												{ val.status == 'publish'
													? 'Draft'
													: 'Publish' }
											</span>
										) }
									</div>
								</div>
							</div>
						);
					} )
				) : (
					<>
						{ menu != 'all'
							? ( getPremadeScreen(), setPremade( menu ) )
							: getNewScreen() }
					</>
				) }
			</div>
		);
	};

	return (
		<div className="wopb-builder-dashboard">
			{ toastMessages.state && (
				<Toast
					delay={ 2000 }
					toastMessages={ toastMessages }
					setToastMessages={ setToastMessages }
				/>
			) }
			{ ! notEditor && (
				<div className="wopb-builder-dashboard__content wopb-builder-tab">
					<div className="wopb-builder-tab__option">
						<span
							onClick={ () => _fetchFile() }
							className="wopb-popup-sync"
						>
							<i
								className={
									'dashicons dashicons-update-alt' +
									( fetch ? ' rotate' : '' )
								}
							></i>
							Synchronize
						</span>
						<ul>
							<li
								{ ...( menu == 'all' && {
									className: 'active',
								} ) }
								onClick={ () => {
									setMenu( 'all' );
									setIsNew( false );
									setPremade( '' );
								} }
							>
								<span className="dashicons dashicons-admin-home" />{ ' ' }
								All Template
							</li>
							<li
								{ ...( menu == 'home_page' && {
									className: 'active',
								} ) }
								onClick={ () => {
									setMenu( 'home_page' );
									setIsNew( false );
									setPremade( '' );
								} }
							>
								<img
									src={
										wopb_condition.url +
										`addons/builder/assets/img/home_page.svg`
									}
								/>
								Home Page
							</li>
							<li
								{ ...( menu == 'single_product' && {
									className: 'active',
								} ) }
								onClick={ () => {
									setMenu( 'single_product' );
									setIsNew( false );
									setPremade( '' );
								} }
							>
								<img
									src={
										wopb_condition.url +
										`addons/builder/assets/img/single_product.svg`
									}
								/>
								Single Product
							</li>
							<li
								{ ...( menu == 'shop' && {
									className: 'active',
								} ) }
								onClick={ () => {
									setMenu( 'shop' );
									setIsNew( false );
									setPremade( '' );
								} }
							>
								<img
									src={
										wopb_condition.url +
										`addons/builder/assets/img/shop.svg`
									}
								/>
								Shop
							</li>
							<li
								{ ...( menu == 'cart' && {
									className: 'active',
								} ) }
								onClick={ () => {
									setMenu( 'cart' );
									setIsNew( false );
									setPremade( '' );
								} }
							>
								<img
									src={
										wopb_condition.url +
										`addons/builder/assets/img/cart.svg`
									}
								/>
								Cart
							</li>
							<li
								{ ...( menu == 'checkout' && {
									className: 'active',
								} ) }
								onClick={ () => {
									setMenu( 'checkout' );
									setIsNew( false );
									setPremade( '' );
								} }
							>
								<img
									src={
										wopb_condition.url +
										`addons/builder/assets/img/checkout.svg`
									}
								/>
								Checkout
							</li>
							<li
								{ ...( menu == 'my_account' && {
									className: 'active',
								} ) }
								onClick={ () => {
									setMenu( 'my_account' );
									setIsNew( false );
									setPremade( '' );
								} }
							>
								<img
									src={
										wopb_condition.url +
										`addons/builder/assets/img/my_account.svg`
									}
								/>
								My Account
							</li>
							<li
								{ ...( menu == 'thank_you' && {
									className: 'active',
								} ) }
								onClick={ () => {
									setMenu( 'thank_you' );
									setIsNew( false );
									setPremade( '' );
								} }
							>
								<img
									src={
										wopb_condition.url +
										`addons/builder/assets/img/thank_you.svg`
									}
								/>
								Thank You
							</li>
							<li
								{ ...( menu == 'product_search' && {
									className: 'active',
								} ) }
								onClick={ () => {
									setMenu( 'product_search' );
									setIsNew( false );
									setPremade( '' );
								} }
							>
								<img
									src={
										wopb_condition.url +
										`addons/builder/assets/img/product_search.svg`
									}
								/>
								Product Search
							</li>
							<li
								{ ...( menu == 'archive' && {
									className: 'active',
								} ) }
								onClick={ () => {
									setMenu( 'archive' );
									setIsNew( false );
									setPremade( '' );
								} }
							>
								<img
									src={
										wopb_condition.url +
										`addons/builder/assets/img/archive.svg`
									}
								/>
								Archive
							</li>

							<li
								{ ...( menu == 'header' && {
									className: 'active',
								} ) }
								onClick={ () => {
									setMenu( 'header' );
									setIsNew( false );
									setPremade( '' );
								} }
							>
								<img
									src={
										wopb_condition.url +
										`addons/builder/assets/img/header.svg`
									}
								/>
								{ __( 'Header', 'product-blocks' ) }
							</li>
							<li
								{ ...( menu == 'footer' && {
									className: 'active',
								} ) }
								onClick={ () => {
									setMenu( 'footer' );
									setIsNew( false );
									setPremade( '' );
								} }
							>
								<img
									src={
										wopb_condition.url +
										`addons/builder/assets/img/footer.svg`
									}
								/>
								{ __( 'Footer', 'product-blocks' ) }
							</li>
							<li
								{ ...( menu == '404' && {
									className: 'active',
								} ) }
								onClick={ () => {
									setMenu( '404' );
									setIsNew( false );
									setPremade( '' );
								} }
							>
								<img
									src={
										wopb_condition.url +
										`addons/builder/assets/img/404.svg`
									}
								/>
								404
							</li>
						</ul>
					</div>
					<div className="wopb-builder-tab__content wopb-builder-tab__template">
						<div className="wopb-builder-tab__heading">
							<div className="wopb-builder-heading__title">
								{ ( premade != '' || isNew ) && backOpt && (
									<span
										onClick={ () => {
											setBackOpt( false );
											setIsNew( false );
											setPremade( '' );
										} }
									>
										{ ' ' }
										{ IconPack.leftAngle2 }
										{ __( 'Back', 'product-blocks' ) }
									</span>
								) }
								<div className="wopb-heading">
									All{ ' ' }
									{ menu == 'all'
										? ''
										: menu.replace( '_', ' ' ) }{ ' ' }
									Templates
								</div>
							</div>
							{ postList.length > 0 &&
							premade == '' &&
							! isNew ? (
								<button
									className="cursor wopb-primary-button wopb-builder-create-btn"
									onClick={ () => {
										setIsNew( true );
										setBackOpt( true );
										setPremade( menu == 'all' ? '' : menu );
									} }
								>
									{ ' ' }
									+ Create{ ' ' }
									{ menu == 'all'
										? ''
										: menu.replace( '_', ' ' ) }{ ' ' }
									Template
								</button>
							) : ! isLoading ? (
								<Skeleton
									type="custom_size"
									c_s={ {
										size1: 170,
										unit1: 'px',
										size2: 42,
										unit2: 'px',
										br: 4,
									} }
								/>
							) : (
								''
							) }
						</div>
						<div className={ `wopb-tab__content active` }>
							{ isLoading ? (
								premade == '' ? (
									getData( menu )
								) : (
									getPremadeScreen()
								)
							) : (
								<div
									className={ `skeletonOverflow` }
									label={ __( 'Loading…', 'product-blocks' ) }
								>
									{ Array( 6 )
										.fill( 1 )
										.map( ( v, k ) => {
											return (
												<div
													key={ k }
													className="wopb-template-list__tab"
													style={ {
														marginBottom: '15px',
													} }
												>
													<div className="wopb-template-list__wrapper">
														<div className="wopb-template-list__heading">
															<Skeleton
																type="custom_size"
																c_s={ {
																	size1: 40,
																	unit1: '%',
																	size2: 22,
																	unit2: 'px',
																	br: 2,
																} }
															/>
															<div className="wopb-template-list__control wopb-template-list__content">
																{ ( k == 2 ||
																	k ==
																		4 ) && (
																	<Skeleton
																		type="custom_size"
																		c_s={ {
																			size1: 60,
																			unit1: 'px',
																			size2: 20,
																			unit2: 'px',
																			br: 2,
																		} }
																	/>
																) }
																<Skeleton
																	type="custom_size"
																	c_s={ {
																		size1: 42,
																		unit1: 'px',
																		size2: 20,
																		unit2: 'px',
																		br: 2,
																	} }
																/>
																<Skeleton
																	type="custom_size"
																	c_s={ {
																		size1: 42,
																		unit1: 'px',
																		size2: 20,
																		unit2: 'px',
																		br: 2,
																	} }
																/>
																<Skeleton
																	type="custom_size"
																	c_s={ {
																		size1: 56,
																		unit1: 'px',
																		size2: 20,
																		unit2: 'px',
																		br: 2,
																	} }
																/>
																<Skeleton
																	type="custom_size"
																	c_s={ {
																		size1: 25,
																		unit1: 'px',
																		size2: 12,
																		unit2: 'px',
																		br: 2,
																	} }
																/>
															</div>
														</div>
													</div>
												</div>
											);
										} ) }
								</div>
							) }
						</div>
					</div>
				</div>
			) }

			{ showCondition && (
				<div
					className={ `wopb-condition-wrapper wopb-condition--active` }
				>
					<div className="wopb-condition-popup wopb-popup-wrap">
						<button
							className="wopb-save-close"
							onClick={ () => setShowCondition( '' ) }
						>
							{ IconPack.close_line }
						</button>
						{ Object.keys( defaults ).length && type ? (
							<Conditions
								type={ type }
								id={
									showCondition == 'yes'
										? postId
										: showCondition
								}
								settings={ settings }
								defaults={ defaults }
								setShowCondition={
									notEditor == 'yes' ? setShowCondition : ''
								}
							/>
						) : (
							<div className="wopb-modal-content">
								<div className="wopb-condition-wrap">
									<Skeleton
										type="custom_size"
										c_s={ {
											size1: 330,
											unit1: 'px',
											size2: 22,
											unit2: 'px',
											br: 2,
										} }
									/>
									<Skeleton
										type="custom_size"
										c_s={ {
											size1: 460,
											unit1: 'px',
											size2: 22,
											unit2: 'px',
											br: 2,
										} }
									/>
									<div className="wopb-condition-items">
										<div className="wopb-condition-wrap__field">
											<Skeleton
												type="custom_size"
												c_s={ {
													size1: 100,
													unit1: '%',
													size2: 30,
													unit2: 'px',
													br: 2,
												} }
											/>
										</div>
										<div className="wopb-condition-wrap__field">
											<Skeleton
												type="custom_size"
												c_s={ {
													size1: 100,
													unit1: '%',
													size2: 30,
													unit2: 'px',
													br: 2,
												} }
											/>
										</div>
										<div className="wopb-condition-wrap__field">
											<Skeleton
												type="custom_size"
												c_s={ {
													size1: 100,
													unit1: '%',
													size2: 30,
													unit2: 'px',
													br: 2,
												} }
											/>
										</div>
									</div>
								</div>
							</div>
						) }
					</div>
				</div>
			) }
			{ isLock &&
				getProHtml( {
					tags: 'menuBuilderUpgrade',
					func: ( val ) => {
						setIsLock( val );
					},
					data: {
						icon: 'lock.svg',
						title: __(
							'Create Unlimited Templates With WowStore Pro',
							'product-blocks'
						),
						description: __(
							'We are sorry. Unfortunately, the free version of WowStore lets you create only one template. Please upgrade to a pro version that unlocks the full capabilities of the WooCommerce builder.',
							'product-blocks'
						),
					},
				} ) }
		</div>
	);
};
export default Panel;
