const { __ } = wp.i18n;
import { useState } from 'react';
import icons from '../../helper/revenueIcons';
import Skeleton from '../utility/Skeleton';

const Revenue = ( props ) => {
	const [ loading, setLoading ] = useState( false );
	const [ activated, setActivated ] = useState( wopb_option.revenue_active );
	const [ loadSkeleton, setLoadSkeleton ] = useState( false );

	const revenueData = {
		'campaign': {
			name: 'WowRevenue Features',
			desc: 'We have 12+ campaigns to create amazing offers.',
			data: [
				{
					name: __('Bundle Discount', 'product-blocks'),
					desc: __("Bundle products easily to boost profits and attract more buyers.", 'product-blocks'),
					icon: icons.bundleDiscount,
				},
				{
					name: __('Volume / Quantity Discount', 'product-blocks'),
					desc: __("Offer quantity discounts to encourage larger purchases and boost sales.", 'product-blocks'),
					icon: icons.volumeDiscount,
				},
				{
					name: __('Buy X Get Y', 'product-blocks'),
					desc: __("Attract buyers with unbeatable BOGO deals on best-selling products.", 'product-blocks'),
					icon: icons.buyXgetY,
				},
				{
					name: __('Frequently Bought Together', 'product-blocks'),
					desc: __("Display frequently bought together items to encourage more purchases.", 'product-blocks'),
					icon: icons.frequentBought,
				},
				{
					name: __('Next Order Coupon', 'product-blocks'),
					desc: __("Offer incentives - offer coupons to recurring customers.", 'product-blocks'),
					icon: icons.nextOrder,
					upcoming: true,
				},
				{
					name: __('Product Mix & Match', 'product-blocks'),
					desc: __("Allow users to mix and match products for a hassle-free shopping experience.", 'product-blocks'),
					icon: icons.productMix,
				},
				{
					name: __('Double Order', 'product-blocks'),
					desc: __("Push customers to multiply their entire cart or specific product by 2x, 3x, or more at checkout.", 'product-blocks'),
					icon: icons.doubleOrder,
				},
				{
					name: __('Normal Discount', 'product-blocks'),
					desc: __("Boost sales and increase revenue by offering discounts on high-demand products.", 'product-blocks'),
					icon: icons.normalDiscount,
				},
				{
					name: __('Spending Goal', 'product-blocks'),
					desc: __("Grow your revenue by allowing users to complete goals.", 'product-blocks'),
					icon: icons.spendingGoal,
				},
				{
					name: __('Free Shipping', 'product-blocks'),
					desc: __("Turn shoppers into bigger spenders by offering free shipping for spending a particular amount.", 'product-blocks'),
					icon: icons.freeShipping,
					upcoming: true,
				},
				{
					name: __('Stock Scarcity', 'product-blocks'),
					desc: __("Improve sales by introducing scarcity of lucrative items.", 'product-blocks'),
					icon: icons.stockScarcity,
					upcoming: true,
				},
				{
					name: __('Countdown Timer', 'product-blocks'),
					desc: __("Create limited-time offers for popular store items without coding.", 'product-blocks'),
					icon: icons.countDown,
					upcoming: true,
				},
			]
		},
	};
	const activeRevenue = () => {
		if( activated ) {
			window.location.href = wopb_option.revenue_url
		}else {
			setLoading(true);
			wp.apiFetch({
				path: '/wopb/v2/install-extra-plugin',
				method: 'POST',
				data: {
					wpnonce: wopb_option.security,
					name: 'revenue',
				},
			}).then((res) => {
				if (res) {
					setLoadSkeleton(true)
					setLoading(false);
					setActivated(true);
					setTimeout(() => {
						setLoadSkeleton(false);
					}, 1500);
				}
			});
		}
	};
	const getRevFeatures = ( data ) => {
		return (
			<div className="wopb-addons-grid ">
				{ data.map( ( addon, index ) => {
					return (
						<div
							className="wopb-addon-item wopb-dash-item-con"
							key={ index }
						>
							<div className="wopb-addon-item-contents">
								<div className="wopb-addon-item-name">
									<div className="wopb-addon-heading">
										{addon.name}
										{addon.upcoming &&
											<div className="wopb-revenue-upcoming">
												{__('Upcoming', 'product-blocks')}
											</div>
										}
									</div>
									<div className="wopb-description">
										{addon.desc}
									</div>
								</div>
								<div className="wopb-revenue-icon">
									{addon.icon}
								</div>
							</div>
						</div>
					);
				} ) }
			</div>
		);
	};

	const skeletonContent = () => {
		return (
			<div className="skeletonOverflow">
				<div className="wopb-revenue-head">
					<div className="wopb-revenue-head-title">
						<Skeleton type="button" size="400" height="35"/>
					</div>
					<div className="wopb-revenue-head-desc">
						<Skeleton type="button" size="500" height="28"/>
					</div>
				</div>

				<div className="wopb-revenue-banner-wrap">
					<div className="wopb-revenue-banner">
						<div className="wopb-revenue-left">
							<div className="wopb-revenue-icon">
								<Skeleton type="button" size="70" height="70"/>
							</div>
							<div className="wopb-revenue-content">
								<div className="wopb-revenue-banner-title">
									<Skeleton type="button" size="175" height="30"/>
								</div>
								<div className="wopb-revenue-banner-desc">
									<Skeleton type="button" size="300" height="20"/>
								</div>
							</div>
						</div>

						<div className="wopb-revenue-banner-right">
							<Skeleton type="button" size="170" height="35"/>
						</div>
					</div>
				</div>
			</div>
		)
	}

	return (
		<>
			<div className="wopb-dashboard-addons-container wopb-dashboard-revenue">
				{loadSkeleton && skeletonContent()}
				{!loadSkeleton &&
					<>
						<div className="wopb-revenue-head">
							<div className="wopb-revenue-head-title">
								{(activated) ?
									<>
										<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
											 xmlns="http://www.w3.org/2000/svg">
											<path
												d="M23.6808 2.51187C23.4761 2.30699 23.1985 2.19189 22.909 2.19189C22.6194 2.19189 22.3419 2.30699 22.1372 2.51187L12.0001 12.649L8.40817 9.05709C8.13252 8.78143 7.73055 8.67376 7.3539 8.77458C6.97722 8.8756 6.68314 9.16971 6.58208 9.54639C6.48126 9.92307 6.58893 10.325 6.86459 10.6007L11.2283 14.9644C11.433 15.1693 11.7105 15.2844 12.0001 15.2844C12.2897 15.2844 12.5672 15.1693 12.7719 14.9644L23.6809 4.05536C23.8858 3.85067 24.0009 3.57311 24.0009 3.28355C24.0009 2.99401 23.8858 2.71628 23.6809 2.51174L23.6808 2.51187Z"
												fill="#09B29C"></path>
											<path
												d="M23.7595 9.60286C23.6815 9.22086 23.4057 8.90946 23.0361 8.786C22.6663 8.66253 22.2588 8.74567 21.967 9.0042C21.6751 9.26274 21.5435 9.65728 21.6213 10.0391C22.0708 12.2341 21.7562 14.5167 20.7295 16.508C19.7028 18.4995 18.0258 20.0798 15.9772 20.9866C13.9285 21.8935 11.6312 22.0725 9.46681 21.4936C7.3025 20.9149 5.40107 19.6133 4.07854 17.8049C2.75583 15.9965 2.0915 13.7901 2.19557 11.5522C2.29982 9.31404 3.16638 7.17899 4.65136 5.50159C6.13635 3.82389 8.15043 2.7045 10.3591 2.32935C12.5679 1.9542 14.8386 2.3459 16.7945 3.4392C17.1354 3.6302 17.5523 3.62468 17.8883 3.42493C18.2243 3.22499 18.428 2.86126 18.4229 2.47029C18.4178 2.07955 18.2045 1.72113 17.8634 1.53012C15.565 0.242762 12.9067 -0.252234 10.2987 0.121205C7.69094 0.494643 5.27814 1.71597 3.43335 3.59684C1.58861 5.47753 0.413897 7.91316 0.0906689 10.528C-0.232545 13.1426 0.313627 15.7911 1.6451 18.0644C2.97639 20.3378 5.01896 22.1098 7.45756 23.1069C9.89602 24.1043 12.5945 24.2714 15.1377 23.5825C17.6806 22.8936 19.9262 21.3872 21.5277 19.2955C23.1294 17.2037 23.9982 14.6431 23.9999 12.0084C23.9995 11.2006 23.9192 10.3948 23.76 9.60301L23.7595 9.60286Z"
													fill="#09B29C"></path>
											</svg>
											{__('WowRevenue Activated', 'product-blocks')}
										</>
										: __('Boost Your Sales & AOV with WowRevenue!', 'product-blocks')
									}
								</div>

							<div className="wopb-revenue-head-desc">
								{__('Skyrocket Your Sales & Revenue without extra marketing costs!', 'product-blocks')}
							</div>
						</div>

						<div className="wopb-revenue-banner-wrap">
							<div className="wopb-revenue-banner">
								<div className="wopb-revenue-left">
									<div className="wopb-revenue-icon">
										{icons.revenue}
									</div>
									<div className="wopb-revenue-content">
										<div className="wopb-revenue-banner-title">
											{activated
												? __('Create your first campaign and boost revenue', 'product-blocks')
												: __('Install and Activated WowRevenue', 'product-blocks')
											}
										</div>
									</div>
								</div>

								<div
									className="wopb-revenue-active-btn"
									onClick={() => activeRevenue()}
								>
									{activated
										? __('Go to WowRevenue', 'product-blocks')
										: __('Activate WowRevenue', 'product-blocks')
									}
									{loading && <span className="wopb-loading"/>}
								</div>
							</div>
						</div>
					</>
				}
				<div>
					{Object.keys(revenueData).map((val, k) => {
						return (
							<span key={k}>
								{revenueData[val].data.length > 0 && (
									<div className="wopb-revenue-body">
										<div className="wopb-addon-parent-heading mt">
											{revenueData[val].name}
										</div>
										<span className="wopb-revenue-desc">{revenueData[val].desc}</span>
										{getRevFeatures(
											revenueData[val].data
										)}
									</div>
								)}
							</span>
						);
					})}
				</div>
			</div>
		</>
	);
};

export default Revenue;
