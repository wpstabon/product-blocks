const { __ } = wp.i18n;
import { useEffect, useState } from 'react';
import { createRoot } from 'react-dom/client';
import './dashboard.scss';
import Blocks from './blocks/Blocks';
import Panel from './builder/Panel';
import SavedTemplates from './saved_templates/SavedTemplates';
import Addons from './addons/Addons';
import GeneralSettings from './general_settings/GeneralSettings';
import TemplatesKit from './templateskit/TemplatesKit';
import License from './license/License';
import Support from './support/Support';
import SizeChart from './size_chart/SizeChart';
import Revenue from "./revenue/Revenue";
import './CustomPostType.scss';
import IconPack from '../helper/fields/tools/IconPack';

const Dashborad = () => {
	const [ current, setCurrent ] = useState( 'home' );
	const [ helloBar, setHelloBar ] = useState(
		wopb_dashboard_pannel.helloBar
	);
	const status = wopb_dashboard_pannel.status;
	const expire = wopb_dashboard_pannel.expire;
	const [ showFAQ, setShowFAQ ] = useState( false );

	const dashMenu = [
		{
			link: '#home',
			label: __( 'Addons', 'product-blocks' ),
			showin: 'both',
		},
		{
			link: '#builder',
			label: __( 'Woo Builder', 'product-blocks' ),
			showin:
				wopb_dashboard_pannel.settings.hasOwnProperty(
					'wopb_builder'
				) && wopb_dashboard_pannel.settings.wopb_builder != 'false'
					? 'both'
					: 'none',
			showhide: true,
		},
		{
			link: '#revenue',
			label: __( 'Revenue', 'product-blocks' ) + '<span class="wopb-revenue-tag">New</span>',
			showin: 'both',
		},
		{
			link: '#templatekit',
			label: __( 'Template kits', 'product-blocks' ),
			showin: 'both',
		},
		{
			link: '#blocks',
			label: __( 'Blocks', 'product-blocks' ),
			showin: 'both',
		},
		{
			link: '#settings',
			label: __( 'Settings', 'product-blocks' ),
			showin: 'both',
		},
	];
	const dashHelpMenu = [
		{
			label: __( 'Get Support', 'product-blocks' ),
			icon: 'dashicons-phone',
			link: wopb_dashboard_pannel.active
				? 'https://www.wpxpo.com/contact/?utm_source=db-wstore-started&utm_medium=feature_page_support&utm_campaign=wstore-dashboard'
				: 'https://wordpress.org/support/plugin/product-blocks/',
		},
		{
			label: __( 'Welcome Guide', 'product-blocks' ),
			icon: 'dashicons-megaphone',
			link: wopb_dashboard_pannel.setup_wizard_link,
		},
		{
			label: __( 'Join Community', 'product-blocks' ),
			icon: 'dashicons-facebook-alt',
			link: 'https://www.facebook.com/groups/wowcommercecommunity',
		},
		{
			label: __( 'Feature Request', 'product-blocks' ),
			icon: 'dashicons-email-alt',
			link: 'https://www.wpxpo.com/wowstore/roadmap/?utm_source=db-wstore-started&utm_medium=feature_page_roadmap&utm_campaign=wstore-dashboard',
		},
		{
			label: __( 'Youtube Tutorials', 'product-blocks' ),
			icon: 'dashicons-youtube',
			link: 'https://www.youtube.com/@wpxpo',
		},
		{
			label: __( 'Documentation', 'product-blocks' ),
			icon: 'dashicons-book',
			link: 'https://wpxpo.com/docs/wowstore/?utm_source=db-wstore-started&utm_medium=feature_page_docs&utm_campaign=wstore-dashboard',
		},
	];

	const _fetchQuery = () => {
		let pageUrl = window.location.href;
		if ( pageUrl.includes( 'page=wopb-settings#' ) ) {
			pageUrl = pageUrl.split( 'page=wopb-settings#' );
			if ( pageUrl[ 1 ] ) {
				setCurrent( pageUrl[ 1 ] );
			}
		}
	};
	const helloBarAction = ( key ) => {
		setHelloBar( key );
		wp.apiFetch( {
			path: '/wopb/v2/dashborad',
			method: 'POST',
			data: {
				type: 'helloBarAction',
				helloData: key,
			},
		} );
	};

	const handleClickOutside = ( e ) => {
		if (
			e.target &&
			! e.target.classList?.contains( 'wopb-reserve-button' )
		) {
			if ( e.target.href ) {
				if ( e.target.href.indexOf( 'page=wopb-settings#' ) > 0 ) {
					const slug = e.target.href.split( '#' );
					if ( slug[ 1 ] ) {
						setCurrent( slug[ 1 ] );
						window.scrollTo( { top: 0, behavior: 'smooth' } );
						window.history.pushState( null, null, e.target.href );
					}
				}
			}
		}
		if (
			! e.target.closest( '.dash-faq-container' ) &&
			! e.target.classList?.contains( 'wopb-reserve-button' )
		) {
			setShowFAQ( false );
		}
	};

	useEffect( () => {
		_fetchQuery();
		document.addEventListener( 'mousedown', handleClickOutside );
		return () =>
			document.removeEventListener( 'mousedown', handleClickOutside );
	}, [] );

	const currentDate = new Date();
	const startDate = new Date( '2024-12-23' );
	const endDate = new Date( '2025-01-10' );
	const dateCondition = currentDate >= startDate && currentDate <= endDate;

	return (
		<>
			<div className="wopb-settings-tab-wrap">
				{ helloBar != 'hide400' && (
					<div className="wopb-setting-hellobar">
						{/*{ ! wopb_dashboard_pannel.active && dateCondition && (*/}
						{/*	<>*/}
						{/*		<span className="dashicons dashicons-bell wopb-ring"></span>*/}
						{/*		{ __( 'Holiday Offer:', 'product-blocks' ) }*/}
						{/*		{ __( 'Enjoy', 'product-blocks' ) }*/}
						{/*		<strong>*/}
						{/*			{ __(*/}
						{/*				'Up to 60% Discount',*/}
						{/*				'product-blocks'*/}
						{/*			) }*/}
						{/*		</strong>{ ' ' }*/}
						{/*		on WowStore Pro*/}
						{/*		<a*/}
						{/*			href="https://www.wpxpo.com/wowstore/pricing?utm_source=db-wstore-topbar&utm_medium=explore_now&utm_campaign=wstore-dashboard"*/}
						{/*			target="_blank"*/}
						{/*			rel="noreferrer"*/}
						{/*		>*/}
						{/*			{ __( 'Grab Now', 'product-blocks' ) }{ ' ' }*/}
						{/*			&nbsp;&#10148;*/}
						{/*		</a>*/}
						{/*	</>*/}
						{/*) }*/}
						{/*{ ( wopb_dashboard_pannel.active ||*/}
						{/*	! dateCondition ) && (*/}
						{/*	<>*/}
						{/*		<span className="dashicons dashicons-bell wopb-ring"></span>*/}
						{/*		<strong>*/}
						{/*			{ ' ' }*/}
						{/*			{ __(*/}
						{/*				'ProductX',*/}
						{/*				'product-blocks'*/}
						{/*			) }{ ' ' }*/}
						{/*		</strong>*/}
						{/*		{ __( 'is now', 'product-blocks' ) }*/}
						{/*		<strong>*/}
						{/*			{ ' ' }*/}
						{/*			{ __(*/}
						{/*				'WowStore:',*/}
						{/*				'product-blocks'*/}
						{/*			) }{ ' ' }*/}
						{/*		</strong>*/}
						{/*		{ __(*/}
						{/*			'with exciting new features -',*/}
						{/*			'product-blocks'*/}
						{/*		) }*/}
						{/*		<a*/}
						{/*			href="https://www.wpxpo.com/wowstore/?utm_source=db-wstore-topbar&utm_medium=explore_now&utm_campaign=wstore-dashboard"*/}
						{/*			target="_blank"*/}
						{/*			rel="noreferrer"*/}
						{/*		>*/}
						{/*			{ __( 'See What’s New', 'product-blocks' ) }{ ' ' }*/}
						{/*			&nbsp;&#10148;*/}
						{/*		</a>*/}
						{/*	</>*/}
						{/*) }*/}
							<span className="dashicons dashicons-bell wopb-ring"></span>
							<strong>
								{ ' ' }
								{ __(
									'WowRevenue:',
									'product-blocks'
								) }{ ' ' }
							</strong>
							{ __(
								'Skyrocket Your Sales & Revenue Instantly -',
								'product-blocks'
							) }
							<a
								href="https://www.wowrevenue.com/?utm_source=db-wstore-topbar&utm_medium=get_started&utm_campaign=wstore-dashboard"
								target="_blank"
								rel="noreferrer"
							>
								{ __( 'Get Started', 'product-blocks' ) }{ ' ' }
								&nbsp;&#10148;
							</a>
						<span
							className="helobarClose"
							onClick={ () => {
								helloBarAction( 'hide400' );
							} }
						>
							{ IconPack.close_line }
						</span>
					</div>
				) }
				<div className="wopb-setting-header">
					<div className="wopb-setting-logo">
						<img
							className="wopb-setting-header-img"
							loading="lazy"
							src={
								wopb_dashboard_pannel.url +
								'/assets/img/logo-option.svg'
							}
							alt="WowStore"
						/>
						<span>{ wopb_dashboard_pannel.version }</span>
					</div>
					<ul
						className="wopb-settings-tab"
						id="wopb-dashboard-wopb-settings-tab"
					>
						{ dashMenu?.map( ( menu, i ) => {
							if (
								menu.isPro &&
								! wopb_dashboard_pannel.active
							) {
								return;
							}
							return menu.showin == 'both' ||
								menu.showin == 'menu' ||
								menu.showhide ? (
								<li
									key={ i }
									style={ {
										display:
											menu.showin == 'none' ? 'none' : '',
									} }
									id={
										'wopb-dasnav-' +
										( menu.id ??
											menu.link.replace( '#', '' ) )
									}
									className={
										menu.link == '#' + current
											? 'current'
											: ''
									}
									onClick={ () =>
										setCurrent(
											menu.link.replace( '#', '' )
										)
									}
								>
									<a href={ menu.link } dangerouslySetInnerHTML={{__html: menu.label}} />
								</li>
							) : (
								''
							);
						} ) }
					</ul>
					<div className="wopb-dash-faq-con">
						<span
							onClick={ () => setShowFAQ( ! showFAQ ) }
							className="wopb-dash-faq-icon wopb-reserve-button dashicons dashicons-editor-help"
						></span>
						{ showFAQ && (
							<div className="dash-faq-container">
								{ dashHelpMenu?.map( ( menu, i ) => {
									return (
										<a
											key={ i }
											href={ menu.link }
											target="_blank"
											rel="noreferrer"
										>
											<span
												className={ `dashicons ${ menu.icon }` }
											/>
											{ menu.label }
										</a>
									);
								} ) }
							</div>
						) }
					</div>
				</div>

				<div className="wopb-settings-container">
					<ul className="wopb-settings-content">
						<li>
							{ current == 'home' && (
								<Addons setCurrent={ setCurrent } />
							) }
							{ current == 'size-chart' && (
								<SizeChart type="wopb-size-chart" />
							) }
							{ current == 'saved-templates' && (
								<SavedTemplates type="wopb_templates" />
							) }
							{ current == 'custom-font' && (
								<SavedTemplates type="wopb_custom_font" />
							) }
							{ current == 'builder' && <Panel /> }
							{ current == 'templatekit' && <TemplatesKit /> }
							{ current == 'blocks' && <Blocks /> }
							{ current == 'settings' && <GeneralSettings /> }
							{ current == 'license' && (
								<License status={ status } expire={ expire } />
							) }
							{ current == 'support' && (
								<Support status={ status } expire={ expire } />
							) }
							{ current == 'revenue' && <Revenue /> }
						</li>
					</ul>
				</div>
			</div>
		</>
	);
};

if ( document.body.contains( document.getElementById( 'wopb-dashboard' ) ) ) {
	const container = document.getElementById( 'wopb-dashboard' );
	const root = createRoot( container );
	root.render( <Dashborad /> );
}
