const { __ } = wp.i18n;
import { useState, useEffect } from 'react';
import Toast from '../utility/Toast';
import './blocks.scss';
import Skeleton from '../utility/Skeleton';

const blockSettings = {
	product_grid: {
		label: __( 'Product Grid Blocks', 'product-blocks' ),
		attr: {
			product_grid_1: {
				label: __( 'Product Grid #1', 'product-blocks' ),
				default: true,
				live: 'https://www.wpxpo.com/wowstore/blocks/#demoid353',
				docs: 'https://wpxpo.com/docs/wowstore/all-blocks/product-grid-1/',
				icon: 'product-grid-1.svg',
			},
			product_grid_2: {
				label: __( 'Product Grid #2', 'product-blocks' ),
				default: true,
				live: 'https://www.wpxpo.com/wowstore/blocks/#demoid425',
				docs: 'https://wpxpo.com/docs/wowstore/all-blocks/product-grid-2/',
				icon: 'product-grid-2.svg',
			},
			product_grid_3: {
				label: __( 'Product Grid #3', 'product-blocks' ),
				default: true,
				live: 'https://www.wpxpo.com/wowstore/blocks/#demoid424',
				docs: 'https://wpxpo.com/docs/wowstore/all-blocks/product-grid-3/',
				icon: 'product-grid-3.svg',
			},
		},
	},
	product_list: {
		label: __( 'Product List Blocks', 'product-blocks' ),
		attr: {
			product_list_1: {
				label: __( 'Product List #1', 'product-blocks' ),
				default: true,
				live: 'https://www.wpxpo.com/wowstore/blocks/#demoid426',
				docs: 'https://wpxpo.com/docs/wowstore/all-blocks/product-list-1/',
				icon: 'product-list-1.svg',
			},
		},
	},
	product_category: {
		label: __( 'Product Category', 'product-blocks' ),
		attr: {
			product_category_1: {
				label: __( 'Product Category #1', 'product-blocks' ),
				default: true,
				live: 'https://www.wpxpo.com/wowstore/blocks/#demoid427',
				docs: 'https://wpxpo.com/docs/wowstore/all-blocks/product-category-1/',
				icon: 'product-category-1.svg',
			},
			product_category_2: {
				label: __( 'Product Category #2', 'product-blocks' ),
				default: true,
				live: 'https://www.wpxpo.com/wowstore/blocks/#demoid504',
				docs: 'https://wpxpo.com/docs/wowstore/all-blocks/product-category-2/',
				icon: 'product-category-2.svg',
			},
			product_category_3: {
				label: __( 'Product Category #3', 'product-blocks' ),
				default: true,
				live: 'https://www.wpxpo.com/wowstore/blocks/#demoid1606',
				docs: 'https://wpxpo.com/docs/wowstore/all-blocks/product-category-3/',
				icon: 'product-category-3.svg',
			},
		},
	},

	single_product_builder: {
		label: __( 'Single Product Builder', 'product-blocks' ),
		attr: {
			builder_product_title: {
				label: __( 'Product Title', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/product-title-block/',
				icon: 'builder/title.svg',
			},
			builder_product_short_description: {
				label: __( 'Product Short Description', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/product-short-description-block/',
				icon: 'builder/short_desc.svg',
			},
			builder_product_description: {
				label: __( 'Product Description', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/product-description-block/',
				icon: 'builder/description.svg',
			},
			builder_product_stock: {
				label: __( 'Product Stock', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/product-stock-block/',
				icon: 'builder/stock.svg',
			},
			builder_product_price: {
				label: __( 'Product Price', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/product-price-block/',
				icon: 'builder/pricing.svg',
			},
			builder_product_rating: {
				label: __( 'Product Rating', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/product-rating-block/',
				icon: 'builder/rating.svg',
			},
			builder_product_meta: {
				label: __( 'Product Meta', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/product-meta-block/',
				icon: 'builder/meta.svg',
			},
			builder_product_breadcrumb: {
				label: __( 'Product Breadcrumb', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/product-breadcrumb-block/',
				icon: 'builder/breadcrumb.svg',
			},
			builder_product_image: {
				label: __( 'Product Image', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/product-image-block/',
				icon: 'builder/image.svg',
			},
			builder_product_cart: {
				label: __( 'Product Add To Cart', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/product-add-to-cart-block/',
				icon: 'builder/cart.svg',
			},
			builder_product_additional_info: {
				label: __( 'Product Additional Info', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/product-additional-info-block/',
				icon: 'builder/info.svg',
			},
			builder_product_tab: {
				label: __( 'Product Tab', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/product-tab-block/',
				icon: 'builder/tab.svg',
			},
			builder_product_review: {
				label: __( 'Product Review', 'product-blocks' ),
				default: true,
				live: '',
				docs: '',
				icon: 'builder/review.svg',
			},
			builder_social_share: {
				label: __( 'Social Share', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/social-share-block/',
				icon: 'builder/share.svg',
			},
		},
	},

	cart_builder: {
		label: __( 'Cart Builder', 'product-blocks' ),
		attr: {
			builder_cart_table: {
				label: __( 'Cart Table', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/cart-table-block/',
				icon: 'cart_table.svg',
			},
			builder_cart_total: {
				label: __( 'Cart Total', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/cart-total-block/',
				icon: 'cart_total.svg',
			},
			builder_free_shipping_progress_bar: {
				label: __( 'Free Shipping Progress Bar', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/free-shipping-progress-bar/',
				icon: 'freeshipping_progress_bar.svg',
			},
		},
	},

	checkout_builder: {
		label: __( 'Checkout Builder', 'product-blocks' ),
		attr: {
			builder_checkout_login: {
				label: __( 'Checkout Login', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/checkout-login-block/',
				icon: 'builder/checkout_login.svg',
			},
			builder_checkout_billing: {
				label: __( 'Billing Address', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/billing-address-block/',
				icon: 'builder/checkout_billing_address.svg',
			},
			builder_checkout_shipping: {
				label: __( 'Shipping Address', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/shipping-address-block/',
				icon: 'builder/checkout_shipping_address.svg',
			},
			builder_checkout_additional_information: {
				label: __( 'Additional Information', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/additional-information-block/',
				icon: 'builder/checkout_additional_information.svg',
			},
			builder_checkout_coupon: {
				label: __( 'Coupon', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/coupon-block/',
				icon: 'builder/checkout_coupon.svg',
			},
			builder_checkout_payment_method: {
				label: __( 'Payment Method', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/payment-method-block/',
				icon: 'builder/checkout_payment_method.svg',
			},
			builder_checkout_order_review: {
				label: __( 'Order Review', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/order-review-block/',
				icon: 'builder/checkout_order_review.svg',
			},
		},
	},

	thank_you: {
		label: __( 'Thank You Builder', 'product-blocks' ),
		attr: {
			builder_thankyou_address: {
				label: __( 'Thank You Address', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/thank-you-address-block/',
				icon: 'builder/thank_you/thank_you_address.svg',
			},
			builder_thankyou_order_conformation: {
				label: __( 'Order Confirmation', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/order-confirmation-block/',
				icon: 'builder/thank_you/order_confirmation.svg',
			},
			builder_thankyou_order_details: {
				label: __( 'Thank You Order Details', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/thank-you-order-details-block/',
				icon: 'builder/thank_you/thankyou_order_details.svg',
			},
			builder_thankyou_order_payment: {
				label: __( 'Order Payment', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/order-payment-block/',
				icon: 'builder/thank_you/order_payment.svg',
			},
		},
	},
	other_builder: {
		label: __( 'Other Builder Blocks', 'product-blocks' ),
		attr: {
			builder_my_account: {
				label: __( 'My Account', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/my-account-block/',
				icon: 'builder/my_account.svg',
			},
			builder_archive_title: {
				label: __( 'Archive Title', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/builder-blocks/archive-title-block/',
				icon: 'builder/archive-title.svg',
			},
		},
	},

	other: {
		label: __( 'Others Product Blocks', 'product-blocks' ),
		attr: {
			heading: {
				label: __( 'Heading', 'product-blocks' ),
				default: true,
				live: 'https://www.wpxpo.com/wowstore/blocks/#demoid409',
				docs: 'https://wpxpo.com/docs/wowstore/all-blocks/heading/',
				icon: 'heading.svg',
			},
			image: {
				label: __( 'Image', 'product-blocks' ),
				default: true,
				live: 'https://www.wpxpo.com/wowstore/blocks/#demoid422',
				docs: 'https://wpxpo.com/docs/wowstore/all-blocks/image/',
				icon: 'image.svg',
			},
			wrapper: {
				label: __( 'Wrapper', 'product-blocks' ),
				default: true,
				live: 'https://www.wpxpo.com/wowstore/blocks/#demoid428',
				docs: 'https://wpxpo.com/docs/wowstore/all-blocks/wrapper/',
				icon: 'wrapper.svg',
			},
			product_filter: {
				label: __( 'Product Filtering', 'product-blocks' ),
				default: true,
				live: 'https://www.wpxpo.com/wowstore/woocommerce-product-filter/',
				docs: 'https://wpxpo.com/docs/wowstore/all-blocks/woocommerce-product-filter-blocks/',
				icon: 'filter.svg',
			},
			currency_switcher: {
				label: __( 'Currency Switcher', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/all-blocks/currency-switcher/',
				icon: 'currency_switcher.svg',
			},
			product_search: {
				label: __( 'Product Search', 'product-blocks' ),
				default: true,
				live: 'https://www.wpxpo.com/wowstore/blocks/#demoid1653',
				docs: 'https://wpxpo.com/docs/wowstore/all-blocks/product-search-block/',
				icon: 'search-block.svg',
			},
			product_slider: {
				label: __( 'Product Slider', 'product-blocks' ),
				default: true,
				live: 'https://www.wpxpo.com/wowstore/blocks/#demoid1910',
				docs: 'https://wpxpo.com/docs/wowstore/all-blocks/product-slider-block/',
				icon: 'slider.svg',
			},
			row: {
				label: __( 'Row', 'product-blocks' ),
				default: true,
				live: '',
				docs: 'https://wpxpo.com/docs/wowstore/all-blocks/row-column/',
				icon: 'row.svg',
			},
			button_group: {
				label: __( 'Button Group', 'product-blocks' ),
				default: true,
				live: 'https://www.wpxpo.com/wowstore/blocks/#demoid2093',
				docs: 'https://wpxpo.com/docs/wowstore/all-blocks/button-group-block/',
				icon: 'button-group.svg',
			},
			advance_list: {
				label: __( 'List - WowStore', 'product-blocks' ),
				default: true,
				live: 'https://www.wpxpo.com/wowstore/blocks/#demoid2130',
				docs: 'https://wpxpo.com/docs/wowstore/all-blocks/list-block/',
				icon: 'advance_list.svg',
			},
			banner: {
				label: __( 'Banner Maker', 'product-blocks' ),
				default: true,
				live: 'https://www.wpxpo.com/wowstore/blocks/#demoid2296',
				docs: 'https://wpxpo.com/docs/wowstore/all-blocks/banner-maker-block/',
				icon: 'banner.svg',
			},
		},
	},
};

const Blocks = () => {
	// const [settings, setSettings] = useState(wopb_dashboard_pannel.settings);
	const [ settings, setSettings ] = useState( {} );
	const [ toastMessages, setToastMessages ] = useState( {
		state: false,
		status: '',
	} );
	const [ loading, setLoading ] = useState( false );

	useEffect( () => {
		getAllSettings();
	}, [] );

	const getAllSettings = () => {
		setLoading( true );
		wp.apiFetch( {
			path: '/wopb/v2/get_all_settings',
			method: 'POST',
			data: {
				key: 'key',
				value: 'value',
			},
		} ).then( ( res ) => {
			if ( res.success ) {
				setSettings( res.settings );
				setLoading( false );
			}
		} );
	};

	const changeBlockStatus = ( key ) => {
		const value =
			settings?.hasOwnProperty( key ) && settings[ key ] != 'yes'
				? 'yes'
				: '';
		setSettings( { ...settings, [ key ]: value } );

		wp.apiFetch( {
			path: '/wopb/v2/addon_block_action',
			method: 'POST',
			data: {
				key,
				value,
			},
		} ).then( ( res ) => {
			if ( res.success ) {
				setToastMessages( {
					status: 'success',
					messages: [ res.message ],
					state: true,
				} );
			}
		} );
	};

	return (
		<div className="wopb-dashboard-blocks-container">
			{ toastMessages.state && (
				<Toast
					delay={ 2000 }
					toastMessages={ toastMessages }
					setToastMessages={ setToastMessages }
				/>
			) }
			{ Object.keys( blockSettings ).map( ( key, index ) => {
				const blockList = blockSettings[ key ];

				return (
					<div className="wopb-dashboard-blocks-group" key={ index }>
						{ ! loading ? (
							<>
								<div className="wopb-heading">
									{ blockList.label }
								</div>
								<div className="wopb-dashboard-group-blocks">
									{ Object.keys( blockList.attr ).map(
										( key, i ) => {
											const block = blockList.attr[ key ];
											let checked = block.default
												? true
												: false;
											if ( settings[ key ] == '' ) {
												checked =
													settings[ key ] == 'yes'
														? true
														: false;
											}
											return (
												<div
													className="wopb-dashboard-group-blocks-item wopb-dash-item-con"
													key={ i }
												>
													<div className="wopb-blocks-item-meta">
														<img
															width="22"
															height="22"
															src={ `${ wopb_dashboard_pannel.url }assets/img/blocks/${ block.icon }` }
															alt={ block.label }
														/>
														<div>
															{ block.label }
														</div>
													</div>
													<div className="wopb-blocks-control-option wopb-dash-control-options">
														{ block.docs && (
															<a
																href={
																	block.docs +
																	'?utm_source=db-wstore-blocks&utm_medium=docs&utm_campaign=wstore-dashboard'
																}
																className="wopb-option-link"
																target="_blank"
																rel="noreferrer"
															>
																<div className="dashicons dashicons-media-document"></div>
																{ __(
																	'Docs',
																	'product-blocks'
																) }
															</a>
														) }
														{ block.live && (
															<a
																href={
																	block.live
																}
																className="wopb-option-link"
																target="_blank"
																rel="noreferrer"
															>
																<div className="dashicons dashicons-external"></div>
																{ __(
																	'Live',
																	'product-blocks'
																) }
															</a>
														) }
														<input
															type="checkbox"
															className="wopb-blocks-enable"
															id={ key }
															defaultChecked={
																checked
															}
															onChange={ () => {
																changeBlockStatus(
																	key
																);
															} }
														/>
														<label
															className="wopb-control__label"
															htmlFor={ key }
														></label>
													</div>
												</div>
											);
										}
									) }
								</div>
							</>
						) : (
							<>
								<Skeleton
									type="custom_size"
									c_s={ {
										size1: 180,
										unit1: 'px',
										size2: 32,
										unit2: 'px',
										br: 4,
									} }
								/>
								<div className="wopb-dashboard-group-blocks">
									{ Array( 6 )
										.fill( 1 )
										.map( ( val, i ) => {
											return (
												<div
													className="wopb-dashboard-group-blocks-item wopb-dash-item-con"
													key={ i }
												>
													<div className="wopb-blocks-item-meta">
														<Skeleton
															type="custom_size"
															c_s={ {
																size1: 22,
																unit1: 'px',
																size2: 25,
																unit2: 'px',
																br: 4,
															} }
														/>
														<Skeleton
															type="custom_size"
															c_s={ {
																size1: 100,
																unit1: 'px',
																size2: 20,
																unit2: 'px',
																br: 2,
															} }
														/>
													</div>
													<div className="wopb-blocks-control-option wopb-dash-control-options">
														<Skeleton
															type="custom_size"
															c_s={ {
																size1: 46,
																unit1: 'px',
																size2: 20,
																unit2: 'px',
																br: 2,
															} }
														/>
														<Skeleton
															type="custom_size"
															c_s={ {
																size1: 40,
																unit1: 'px',
																size2: 20,
																unit2: 'px',
																br: 2,
															} }
														/>
														<Skeleton
															type="custom_size"
															c_s={ {
																size1: 36,
																unit1: 'px',
																size2: 20,
																unit2: 'px',
																br: 8,
															} }
														/>
													</div>
												</div>
											);
										} ) }
								</div>
							</>
						) }
					</div>
				);
			} ) }
		</div>
	);
};

export default Blocks;
