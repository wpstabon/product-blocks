const { __ } = wp.i18n;
import React from 'react';

const Modal = ( props ) => {
	const { title, modalContent, setModalContent } = props;
	return (
		<div
			className="wopb-dashboard-modal-wrapper"
			onClick={ ( e ) => {
				if ( ! e.target.closest( '.wopb-dashboard-modal' ) ) {
					setModalContent( '' );
				}
			} }
		>
			<div className="wopb-dashboard-modal">
				<div className="wopb-modal-header">
					{ title && (
						<span className="wopb-modal-title">{ title }</span>
					) }
					<a
						className="wopb-popup-close"
						onClick={ () => setModalContent( '' ) }
					/>
				</div>
				<div className="wopb-modal-body">{ modalContent }</div>
			</div>
		</div>
	);
};

export default Modal;
