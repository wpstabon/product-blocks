const { __ } = wp.i18n;
import { React, useEffect, useState } from 'react';
import IconPack from '../../helper/fields/tools/IconPack';
import { getProHtml } from '../Settings';
import Skeleton from '../utility/Skeleton';
import Toast from '../../dashboard/utility/Toast';

const SizeChart = ( props ) => {
	const [ data, setData ] = useState( [] );
	const [ noData, setNoData ] = useState( false );
	const [ pages, setPages ] = useState( 1 );
	const [ found, setFound ] = useState( 0 );
	const [ newLink, setNewLink ] = useState( '' );
	const [ totalPage, setTotalPage ] = useState( 0 );
	const [ bulkSelect, setBulkSelect ] = useState( false );
	const [ bulkType, setBulkType ] = useState( '' );
	const [ checkedState, setCheckedState ] = useState( [] );
	const [ actionId, setActionId ] = useState( '' );
	const [ actionPop, setActionPop ] = useState( false );
	const [ toastMessages, setToastMessages ] = useState( {
		state: false,
		status: '',
	} );
	const [ lockShow, setLockShow ] = useState( false );

	useEffect( () => {
		_fetchData();
		document.addEventListener( 'mousedown', handleClickOutside );
		return () =>
			document.removeEventListener( 'mousedown', handleClickOutside );
	}, [] );

	const _fetchData = ( arg = {} ) => {
		_fetchQuery( {
			action: 'dashborad',
			data: Object.assign(
				{},
				{
					type: 'wopb-size-chart',
					pages,
					pType: props.type,
				},
				arg
			),
		} );
	};

	const _fetchQuery = ( attr ) => {
		wp.apiFetch( {
			path: '/wopb/v2/' + attr.action,
			method: 'POST',
			data: attr.data,
		} ).then( ( res ) => {
			if ( res.success ) {
				switch ( attr.data.type ) {
					case 'wopb-size-chart':
						setCheckedState(
							Array( res.data.length ).fill( false )
						);
						setData( res.data );
						setNoData( res.data.length > 0 ? false : true );
						setNewLink( res.new );
						setFound( res.found );
						setTotalPage( res.pages );
						setBulkType( '' );
						setBulkSelect( false );
						if ( attr.data.search ) {
							setPages( 1 );
						}
						break;
					case 'status':
					case 'delete':
					case 'action_draft':
					case 'action_delete':
					case 'action_publish':
						_fetchData();
						setBulkSelect( false );
						setToastMessages( {
							status: attr.data.type.includes( 'delete' )
								? 'error'
								: 'success',
							messages: [ res.message ],
							state: true,
						} );
						break;
					default:
						break;
				}
			}
		} );
	};
	const handleClickOutside = ( e ) => {
		if (
			! e.target.parentNode.classList.contains( 'wopb-reserve-button' )
		) {
			setActionId( '' );
			setActionPop( false );
		}
	};

	return (
		<div className="wopb-size-chart-container">
			{ toastMessages.state && (
				<Toast
					delay={ 2000 }
					toastMessages={ toastMessages }
					setToastMessages={ setToastMessages }
				/>
			) }
			{ newLink ? (
				<>
					{ props.type == 'wopb-size-chart' &&
					! wopb_dashboard_pannel.active &&
					data?.length > 0 ? (
						<a
							className="wopb-primary-button cursor"
							onClick={ () => setLockShow( true ) }
						>
							{ __( 'Add New', 'product-blocks' ) }
						</a>
					) : (
						<>
							<a
								className="wopb-primary-button "
								target="_blank"
								href={ newLink }
								rel="noreferrer"
							>
								{ __( 'Add New', 'product-blocks' ) }
							</a>
						</>
					) }
				</>
			) : (
				<Skeleton
					type="custom_size"
					c_s={ {
						size1: 108,
						unit1: 'px',
						size2: 46,
						unit2: 'px',
						br: 4,
					} }
				/>
			) }
			<div className="wopb-table-con">
				<div className="wopb-bulk-con wopb-dash-item-con">
					<div>
						<select
							value={ bulkType }
							onChange={ ( v ) => setBulkType( v.target.value ) }
						>
							<option value="">
								{ __( 'Bulk Action', 'product-blocks' ) }
							</option>
							<option value="publish">
								{ __( 'Publish', 'product-blocks' ) }
							</option>
							<option value="draft">
								{ __( 'Draft', 'product-blocks' ) }
							</option>
							<option value="delete">
								{ __( 'Delete', 'product-blocks' ) }
							</option>
						</select>
						<a
							className="wopb-primary-button cursor"
							onClick={ () => {
								const items = checkedState.filter( ( v ) =>
									Number.isInteger( v )
								);
								if ( bulkType && items.length > 0 ) {
									if ( bulkType == 'delete' ) {
										if (
											confirm(
												'Are you sure you want to apply the action?'
											)
										) {
											_fetchQuery( {
												action: 'dashborad',
												data: {
													type: 'action_' + bulkType,
													ids: items,
												},
											} );
										}
									} else {
										_fetchQuery( {
											action: 'dashborad',
											data: {
												type: 'action_' + bulkType,
												ids: items,
											},
										} );
									}
								}
							} }
						>
							{ __( 'Apply', 'product-blocks' ) }
						</a>
					</div>
					<input
						type="text"
						placeholder="Search..."
						onChange={ ( e ) => {
							_fetchData( { search: e.target.value } );
						} }
					/>
				</div>

				<div className="wopbTable">
					<table
						className={ `${
							data.length == 0 && ! noData
								? 'skeletonOverflow'
								: ''
						}` }
					>
						<thead>
							<tr>
								<th>
									<input
										type="checkbox"
										checked={ bulkSelect }
										onChange={ ( e ) => {
											if ( bulkSelect ) {
												setCheckedState(
													Array( data.length ).fill(
														false
													)
												);
											} else {
												setCheckedState(
													data.map( ( v ) => v.id )
												);
											}
											setBulkSelect( ! bulkSelect );
										} }
									/>
								</th>
								<th>{ __( 'Title', 'product-blocks' ) }</th>
								<th>{ __( 'Category', 'product-blocks' ) }</th>
								<th>{ __( 'Include', 'product-blocks' ) }</th>
								<th>{ __( 'Exclude', 'product-blocks' ) }</th>
								<th>{ __( 'Date', 'product-blocks' ) }</th>
								<th>{ __( 'Action', 'product-blocks' ) }</th>
							</tr>
						</thead>
						<tbody>
							{ data?.map( ( val, key ) => {
								return (
									<tr key={ key }>
										<td>
											<input
												type="checkbox"
												checked={
													checkedState[ key ]
														? true
														: false
												}
												onChange={ () => {
													const final = [
														...checkedState,
													];
													final.splice(
														key,
														1,
														checkedState[ key ]
															? false
															: val.id
													);
													setCheckedState( final );
												} }
											/>
										</td>
										<td className="title">
											<a
												href={ val?.edit?.replace(
													'&amp;',
													'&'
												) }
												target="_blank"
												rel="noreferrer"
											>
												{ val.title }
											</a>
										</td>
										<td>
											<div
												style={ { maxWidth: '250px' } }
											>
												{ val.categories }
											</div>
										</td>
										<td>
											<div
												style={ { maxWidth: '250px' } }
											>
												{ val.include_products }
											</div>
										</td>
										<td>
											<div
												style={ { maxWidth: '250px' } }
											>
												{ val.exclude_products }
											</div>
										</td>
										<td className="wopb-date">
											{ val.status == 'publish'
												? 'Published'
												: val.status }
											<br />
											{ val.date.toUpperCase() }
										</td>
										<td>
											<span
												className="actions wopb-reserve-button"
												onClick={ ( e ) => {
													setActionPop( ! actionPop );
													setActionId( val.id );
												} }
											>
												<span className="dashicons dashicons-ellipsis"></span>
												{ actionId == val.id &&
													actionPop && (
														<ul className="wopb-dash-item-con actionPopUp wopb-reserve-button">
															<li className="wopb-reserve-button">
																<a
																	target="_blank"
																	href={ val?.edit?.replace(
																		'&amp;',
																		'&'
																	) }
																	rel="noreferrer"
																>
																	<span className="dashicons dashicons-edit-large"></span>
																	{ __(
																		'Edit',
																		'product-blocks'
																	) }
																</a>
															</li>
															<li
																onClick={ (
																	e
																) => {
																	setActionId(
																		''
																	);
																	setActionPop(
																		false
																	);
																	e.preventDefault();
																	if (
																		confirm(
																			'Are you sure?'
																		)
																	) {
																		_fetchQuery(
																			{
																				action: 'template_action',
																				data: {
																					type: 'status',
																					id: val.id,
																					status:
																						val.status ==
																						'publish'
																							? 'draft'
																							: 'publish',
																				},
																			}
																		);
																	}
																} }
															>
																<span className="dashicons dashicons-open-folder"></span>
																{ __(
																	'Set to',
																	'product-blocks'
																) }{ ' ' }
																{ val.status ==
																'draft'
																	? 'Published'
																	: 'Draft' }
															</li>
															<li
																onClick={ (
																	e
																) => {
																	setActionId(
																		''
																	);
																	setActionPop(
																		false
																	);
																	e.preventDefault();
																	if (
																		confirm(
																			'Are you sure you want to delete?'
																		)
																	) {
																		_fetchQuery(
																			{
																				action: 'template_action',
																				data: {
																					type: 'delete',
																					id: val.id,
																				},
																			}
																		);
																	}
																} }
															>
																<span className="dashicons dashicons-trash"></span>
																{ __(
																	'Delete',
																	'product-blocks'
																) }
															</li>
														</ul>
													) }
											</span>
										</td>
									</tr>
								);
							} ) }
							{ data.length == 0 && noData && (
								<tr>
									<td colSpan={ 10 }>
										<div className="wopb-heading">
											{ __(
												'No Data Found !!!',
												'product-blocks'
											) }
										</div>
									</td>
								</tr>
							) }
							{ data.length == 0 && ! noData && (
								<>
									{ Array( 5 )
										.fill( 1 )
										.map( ( val, k ) => {
											return (
												<tr key={ k }>
													<td>
														<Skeleton
															type="custom_size"
															c_s={ {
																size1: 22,
																unit1: 'px',
																size2: 20,
																unit2: 'px',
																br: 4,
															} }
														/>
													</td>
													<>
														{ Array( 6 )
															.fill( 1 )
															.map(
																( v, key ) => (
																	<td
																		key={
																			key
																		}
																	>
																		<Skeleton
																			type="title"
																			size="99"
																		/>
																	</td>
																)
															) }
													</>
												</tr>
											);
										} ) }
								</>
							) }
						</tbody>
					</table>
				</div>
				<div className="pageCon">
					<div>
						{ __( 'Page', 'product-blocks' ) }{ ' ' }
						{ totalPage > 0 ? pages : totalPage }{ ' ' }
						{ __( 'of', 'product-blocks' ) } { totalPage } [{ ' ' }
						{ found } { __( 'items', 'product-blocks' ) } ]
					</div>
					{ totalPage > 0 && (
						<div className="wopbPages">
							{ pages > 1 && (
								<span
									onClick={ () => {
										const count = pages - 1;
										_fetchData( { pages: count } );
										setPages( count );
									} }
								>
									{ IconPack.leftAngle2 }
								</span>
							) }
							<span className="currentPage">{ pages }</span>
							{ totalPage > pages && (
								<span
									onClick={ () => {
										const count = pages + 1;
										_fetchData( { pages: count } );
										setPages( count );
									} }
								>
									{ IconPack.rightAngle2 }
								</span>
							) }
						</div>
					) }
				</div>
			</div>
			{ lockShow &&
				getProHtml( {
					tags: 'menu_save_temp_pro',
					func: ( val ) => {
						setLockShow( val );
					},
					data: {
						icon: 'lock.svg',
						title: __(
							'Create Unlimited Size Charts with WowStore Pro',
							'product-blocks'
						),
						description: __(
							'You can create only one size chart template with the free version of WowStore. Please upgrade to a pro plan to create unlimited size chart templates.',
							'product-blocks'
						),
					},
				} ) }
		</div>
	);
};

export default SizeChart;
