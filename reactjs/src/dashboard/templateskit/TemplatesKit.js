const { __ } = wp.i18n;
import { useState, useEffect } from 'react';
import PremadeDesignTemplate from '../../helper/common/PremadeDesignTemplate';
import Toast from '../utility/Toast';

const TemplatesKit = ( props ) => {
	const [ state, setState ] = useState( {
		templates: [],
		designs: [],
		reloadId: '',
		reload: false,
		isTemplate: true,
		error: false,
		fetch: false,
		current: [],

		templateFilter: 'all',
		designFilter: 'all',
		sidebarOpen: true,
		templatekitCol: 'wopb-templatekit-col3',
		loading: false,
	} );

	const { isTemplate, templateFilter, designFilter } = state;

	const [ searchQuery, setSearchQuery ] = useState( '' );
	const [ trend, setTrend ] = useState( 'all' );
	const [ freePro, setFreePro ] = useState( 'all' );
	const [ wishListArr, setWishlistArr ] = useState( [] );
	const [ showWishList, setShowWishList ] = useState( false );
	const [ toastMessages, setToastMessages ] = useState( {
		state: false,
		status: '',
	} );

	const templateSort = ( data ) => {
		if ( data ) {
			data.sort( ( a, b ) => {
				if ( a.pro && ! b.pro ) {
					return 1;
				}
				if ( ! a.pro && b.pro ) {
					return -1;
				}
				return 0;
			} );
		}
		return data;
	};

	const fetchTemplates = async () => {
		setState( { ...state, loading: true } );
		wp.apiFetch( {
			path: '/wopb/v2/get_preset_data',
			method: 'POST',
			data: {
				type: 'all',
			},
		} ).then( ( response ) => {
			if ( response.success ) {
				if ( response.data ) {
					const data = JSON.parse( response.data );
					setState( {
						...state,
						templates: data.template,
						designs: data.design,
						current: templateSort( data.template ),
						loading: false,
					} );
				}
			}
		} );
	};

	useEffect( () => {
		setWListAction( '', '', 'fetchData' );
		fetchTemplates();
	}, [] );

	const _changeVal = ( templateID, isPro, templateTitle ) => {
		if ( isPro ) {
			if ( wopb_condition.active ) {
				insertBlock( templateID, templateTitle );
			}
		} else {
			insertBlock( templateID, templateTitle );
		}
	};

	const insertBlock = async ( templateID, templateTitle ) => {
		setState( { ...state, reload: true, reloadId: templateID } );
		window
			.fetch(
				'https://wopb.wpxpo.com/wp-json/restapi/v2/single-template',
				{
					method: 'POST',
					body: new URLSearchParams(
						'license=' +
							wopb_condition.license +
							'&template_id=' +
							templateID
					),
				}
			)
			.then( ( response ) => response.text() )
			.then( ( jsonData ) => {
				jsonData = JSON.parse( jsonData );
				if ( jsonData.success && jsonData.rawData ) {
					if ( isTemplate ) {
						wp.apiFetch( {
							path: '/wopb/v2/template_page_insert',
							method: 'POST',
							data: {
								blockCode: jsonData.rawData,
								title: templateTitle,
							},
						} ).then( ( res ) => {
							if ( res.success ) {
								window.open( res.link.replace( '&amp;', '&' ) );
							}
						} );
					} else {
						navigator.clipboard.writeText( jsonData.rawData );
					}
					setState( {
						...state,
						reload: false,
						reloadId: '',
						error: false,
					} );
				} else {
					setState( { ...state, error: true } );
				}
			} )
			.catch( ( error ) => {
				console.error( error );
			} );
	};

	const _fetchFile = () => {
		setState( { ...state, fetch: true } );
		wp.apiFetch( {
			path: '/wopb/v2/get_preset_data',
			method: 'POST',
			data: {
				type: 'reset',
			},
		} ).then( ( response ) => {
			if ( response.success ) {
				fetchTemplates();
				setState( { ...state, fetch: false } );
			}
		} );
	};

	const splitArchiveData = ( data = [], key = '' ) => {
		const newData = data.filter( ( item ) => {
			const newItem = key
				? item?.category?.filter( ( v ) =>
						key == 'all' ? true : v.slug == key
				  )
				: [];
			return newItem?.length > 0 ? newItem : false;
		} );
		return newData;
	};

	const setWListAction = ( id, action = '', type = '' ) => {
		wp.apiFetch( {
			path: '/wopb/v2/premade_wishlist_save',
			method: 'POST',
			data: {
				id,
				action,
				type,
			},
		} ).then( ( res ) => {
			if ( res.success ) {
				setWishlistArr( res.wishListArr );
				if ( type != 'fetchData' ) {
					setToastMessages( {
						status: 'success',
						messages: [ res.message ],
						state: true,
					} );
				}
			}
		} );
	};

	const filter = isTemplate ? templateFilter : designFilter;

	return (
		<>
			<div>
				{ toastMessages.state && (
					<Toast
						delay={ 2000 }
						toastMessages={ toastMessages }
						setToastMessages={ setToastMessages }
					/>
				) }
				<PremadeDesignTemplate
					_changeVal={ _changeVal }
					splitArchiveData={ splitArchiveData }
					setState={ setState }
					setSearchQuery={ setSearchQuery }
					_fetchFile={ _fetchFile }
					filter={ filter }
					state={ state }
					searchQuery={ searchQuery }
					dashboard={ true }
					trend={ trend }
					setTrend={ setTrend }
					freePro={ freePro }
					setFreePro={ setFreePro }
					wishListArr={ wishListArr || [] }
					setWListAction={ ( id, action ) =>
						setWListAction( id, action )
					}
					setShowWishList={ setShowWishList }
					showWishList={ showWishList }
				/>
			</div>
		</>
	);
};

export default TemplatesKit;
