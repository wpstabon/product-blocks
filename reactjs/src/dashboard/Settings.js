const { __ } = wp.i18n;
import LinkGenerator from '../helper/LinkGenerator';
import Tooltip from '../helper/common/tooltip/Tooltip';
import IconPack from '../helper/fields/tools/IconPack';
import icons from '../helper/icons';
import { Fragment, useEffect, useState } from 'react';
import Typography from './fields/Typography';
import Color from './fields/Color';
import Color2 from './fields/Color2';
import Dimension from './fields/Dimension';
import Border from './fields/Border';
import Number from './fields/Number';

const Multiselect = ( { multikey, value, multiValue } ) => {
	const [ multiFieldValues, setMultiFieldValues ] = useState( [
		...multiValue,
	] );
	const [ open, setOpen ] = useState( false );

	const addItem = ( val ) => {
		if ( multiFieldValues.indexOf( val ) == -1 ) {
			setMultiFieldValues( [ ...multiFieldValues, val ] );
		}
	};
	const removeItem = ( val ) => {
		setMultiFieldValues(
			multiFieldValues.filter( ( item ) => item != val )
		);
	};
	const handleClickOutside = ( e ) => {
		if ( ! e.target.closest( '.wopb-ms-container' ) ) {
			setOpen( false );
		}
	};
	useEffect( () => {
		document.addEventListener( 'mousedown', handleClickOutside );
		return () =>
			document.removeEventListener( 'mousedown', handleClickOutside );
	}, [] );

	return (
		<div className="wopb-settings-field">
			<input
				type="hidden"
				name={ multikey }
				value={ multiFieldValues }
				data-customprop="custom_multiselect"
			/>
			<div className="wopb-ms-container">
				<div
					onClick={ () => setOpen( ! open ) }
					className="wopb-ms-results-con cursor"
				>
					<div className="wopb-ms-results">
						{ multiFieldValues.length > 0 ? (
							multiFieldValues?.map( ( key, index ) => {
								return (
									<span
										key={ index }
										className="wopb-ms-selected"
									>
										{ value.options[ key ] }
										<span
											className="wopb-ms-remove cursor"
											onClick={ ( e ) => {
												e.stopPropagation();
												removeItem( key );
											} }
										>
											{ IconPack.close_circle_line }
										</span>
									</span>
								);
							} )
						) : (
							<span>{ __( 'Select options' ) }</span>
						) }
					</div>
					<span
						onClick={ () => setOpen( ! open ) }
						className="wopb-ms-results-collapse cursor"
					>
						{ IconPack.collapse_bottom_line }
					</span>
				</div>
				{ open && value.options && (
					<div className="wopb-ms-options">
						{ Object.keys( value.options )?.map( ( key, index ) => {
							return (
								<span
									className="wopb-ms-option cursor"
									onClick={ () => addItem( key ) }
									key={ index }
									value={ key }
								>
									{ value.options[ key ] }
								</span>
							);
						} ) }
					</div>
				) }
			</div>
		</div>
	);
};

let selectedValue = '';
let dropDownClass = '';
const getSettingsData = (
	data,
	settingsData,
	params = {},
	spinner = '',
	setSpinner
) => {
	const {
		formData,
		setFormData,
		editableColumns,
		setEditableColumns,
		settingActiveTab,
		dropdown,
		openDropdown,
		setLayoutPreview,
		dragElement,
		setDragElement,
		accordion,
		setAccordion,
		addonKey,
	} = params;

	const proLinkUrl = `https://www.wpxpo.com/wowstore/pricing/?utm_source=db-wstore-addons&utm_medium=${ addonKey }-setting&utm_campaign=wstore-dashboard`;
	const proLink = (
		<a
			className="wopb-setting-pro-tag"
			href={ proLinkUrl }
			target="_blank"
			rel="noreferrer"
		>
			&nbsp;{ __( '[Pro]', 'product-blocks' ) }
		</a>
	);

	const settingBody = document.querySelector( '.wopb-addon-setting-body' );
	const footerContent = document.querySelector(
		'.wopb-addon-settings-popup .wopb-setting-footer'
	);
	const footerTop = footerContent
		? Math.round( footerContent.getBoundingClientRect().top )
		: '';
	const getField = ( key, value, settingsData ) => {
		let options = value.options;
		const val =
			formData && key in formData
				? formData[ key ]
				: key in settingsData
				? settingsData[ key ]
				: value.default
				? value.default
				: value.type == 'multiselect'
				? []
				: '';

		const noteDependsData =
			formData && value?.note?.depends?.key in formData
				? formData
				: settingsData;
		let note = value?.note?.text ? value?.note?.text : '';
		if (
			value?.note?.depends &&
			! (
				noteDependsData[ value?.note?.depends?.key ] ==
				value?.note?.depends?.value
			)
		) {
			note = '';
		}
		params = { ...params, val, value, fieldKey: key };
		switch ( value.type ) {
			case 'select':
				options = Object.keys( value.options ).sort( ( a, b ) => {
					if ( a === '' && b !== '' ) {
						return -1; // Move empty values to the top
					} else if ( a !== '' && b === '' ) {
						return 1; // Keep non-empty values in their current position
					}
					return 0; // Maintain the current order for both empty and non-empty values
				} );

				const handleDropDown = ( e ) => {
					if (
						footerTop &&
						footerTop -
							Math.round(
								e.target
									.closest( '.wopb-select-field' )
									.getBoundingClientRect().bottom
							) <
							250
					) {
						dropDownClass = ' wopb-dropdown-bottom';
					} else {
						dropDownClass = '';
					}
					openDropdown(
						dropdown && dropdown != key
							? key
							: dropdown && dropdown == key
							? ''
							: key
					);
				};
				return (
					<div className="wopb-settings-field wopb-select-field">
						<div
							className="wopb-selected-content"
							onClick={ ( e ) => handleDropDown( e ) }
						>
							<span>{ value.options[ val ] }</span>
							<span
								className={
									'wopb-select-arrow' +
									( dropdown && dropdown === key
										? ' wopb-arrow-up'
										: '' )
								}
							>
								{ icons.downArrow }
							</span>
						</div>
						{ dropdown && dropdown == key && (
							<div
								className={
									'wopb-select-dropdown' + dropDownClass
								}
							>
								{ options.map( ( k, index ) => {
									return (
										<div
											className={
												'wopb-select-item' +
												( val == k ? ' active' : '' )
											}
											key={ k }
											onClick={ () => {
												setFormData( {
													...formData,
													[ key ]: k,
												} );
												openDropdown( false );
											} }
										>
											{ value.options[ k ] }
										</div>
									);
								} ) }
							</div>
						) }

						{ value.desc && (
							<span className="wopb-description">
								{ value.desc }
							</span>
						) }
						{ note && <div className="wopb-note">{ note }</div> }
					</div>
				);
			case 'color':
				return <Color { ...params } />;
			case 'color2':
				return <Color2 { ...params } />;
			case 'preset_color':
				return (
					<div className="wopb-select-field">
						<div
							className="wopb-selected-content"
							onClick={ () =>
								openDropdown(
									dropdown && dropdown != key
										? key
										: dropdown && dropdown == key
										? ''
										: key
								)
							}
						>
							<div className="wopb-select-preset-list">
								{ Object.keys( value.options[ val ] ).map(
									( k, index ) => {
										const color = value.options[ val ][ k ];
										return (
											<span
												className="wopb-preset-item"
												style={ { background: color } }
												key={ k }
											/>
										);
									}
								) }
							</div>
							<span
								className={
									'wopb-select-arrow' +
									( dropdown && dropdown === key
										? ' wopb-arrow-up'
										: '' )
								}
							>
								{ icons.downArrow }
							</span>
						</div>
						{ dropdown && dropdown == key && (
							<div className="wopb-select-dropdown">
								{ Object.keys( value.options ).map(
									( k, index ) => {
										const colors = value.options[ k ];
										return (
											<div
												className={
													'wopb-select-preset-list' +
													( val == k
														? ' active'
														: '' )
												}
												key={ k }
												onClick={ () => {
													const variationObj = {};

													//Variation field data set
													if ( value.variations ) {
														colors.map(
															(
																colorCode,
																index
															) => {
																if (
																	value
																		.variations[
																		index
																	]
																) {
																	const variations =
																		value
																			.variations[
																			index
																		];

																	Object.keys(
																		variations
																	).map(
																		(
																			fieldKey,
																			index
																		) => {
																			// loop all the variations af selected preset color
																			const variation =
																				variations[
																					fieldKey
																				];
																			if (
																				index ===
																				fieldKey
																			) {
																				// check variation is a single key, no data in variation
																				variationObj[
																					variation
																				] =
																					colorCode;
																			} else {
																				// if variation has value may be string or array
																				if (
																					variation &&
																					Array.isArray(
																						variation
																					)
																				) {
																					// if variation has value and the value is array
																					let variationChildObj =
																						{};
																					variation.map(
																						(
																							childField
																						) => {
																							// loop the array value of variation
																							variationChildObj =
																								{
																									...variationChildObj,
																									[ childField ]:
																										colorCode,
																								};
																						}
																					);
																					variationObj[
																						fieldKey
																					] =
																						{
																							...formData[
																								fieldKey
																							],
																							...variationChildObj,
																						};
																				} else if (
																					variation
																				) {
																					// if variation has single value
																					variationObj[
																						fieldKey
																					] =
																						{
																							...formData[
																								fieldKey
																							],
																							[ variation ]:
																								colorCode,
																						};
																				}
																			}
																		}
																	);
																}
															}
														);
													}
													setFormData( {
														...formData,
														...variationObj,
														[ key ]: k,
													} );
													openDropdown( false );
												} }
											>
												{ colors &&
													Object.keys( colors )
														.length > 0 &&
													Object.keys( colors ).map(
														( k, index ) => {
															return (
																<span
																	className="wopb-preset-item"
																	style={ {
																		background:
																			colors[
																				k
																			],
																	} }
																	key={ k }
																/>
															);
														}
													) }
											</div>
										);
									}
								) }
							</div>
						) }
					</div>
				);
			case 'container':
				return (
					<>
						{ value.group && (
							<div className="wopb-field-container wopb-field-group">
								{ Object.keys( value.group ).map(
									( key, index ) => {
										return (
											<div key={ key }>
												<div className="wopb-group-label">
													{ value.group[ key ].label }
												</div>
												{ getField(
													key,
													value.group[ key ],
													settingsData
												) }
											</div>
										);
									}
								) }
							</div>
						) }
						{ value.fields && (
							<div className="wopb-field-group">
								{ Object.keys( value.fields ).map(
									( key, index ) => {
										const fieldValue = value.fields[ key ];
										return (
											<div
												className="wopb-settings-field wopb-flex-field"
												key={ key }
											>
												<span className="wopb-field-label">
													{ __(
														value.fields[ key ]
															.label
													) }
												</span>
												<span>
													{ getField(
														key,
														fieldValue,
														settingsData
													) }
													{ fieldValue.fields && (
														<span className="wopb-sub-fields">
															{ getSettingsData(
																fieldValue.fields,
																settingsData,
																params
															) }
														</span>
													) }
												</span>
											</div>
										);
									}
								) }
							</div>
						) }
						{ value.attr &&
							getSettingsData(
								value.attr,
								settingsData,
								params
							) }
					</>
				);
			case 'number':
				return <Number { ...params } />;
				break;
			case 'checkbox':
				return (
					<div
						className={ `wopb-settings-field wopb-radio-field wopb-display-inline-box` }
					>
						<div className="wopb-input-section">
							{ Object.keys( value.options ).map( ( k, i ) => {
								return (
									<label
										className="wopb-input-group"
										key={ k }
										htmlFor={ key + '_' + k }
									>
										<span className="wopb-checkmark">
											{ icons.checkmark }
										</span>
										<input
											type="checkbox"
											id={ key + '_' + k }
											name={ key }
											defaultValue={ k }
											defaultChecked={ ( Array.isArray(
												val
											)
												? val
												: []
											).includes( k ) }
											onChange={ ( e ) => {
												let _v = Array.isArray( val )
													? val
													: [];
												if (
													_v.includes(
														e.target.value
													)
												) {
													_v = _v.filter(
														( item ) =>
															item !==
															e.target.value
													);
												} else {
													_v.push( e.target.value );
												}
												setFormData( {
													...formData,
													[ key ]: _v,
												} );
											} }
										/>
										<span
											dangerouslySetInnerHTML={ {
												__html: value.options[ k ],
											} }
										/>
									</label>
								);
							} ) }
						</div>
					</div>
				);
			case 'switchButton':
				return (
					<div className="wopb-settings-field wopb-settings-field-inline">
						<input
							type="checkbox"
							value="yes"
							name={ key }
							defaultChecked={ val == 'yes' || val == 'on' }
							onChange={ ( event ) => {
								setFormData( {
									...formData,
									[ key ]: event.target.checked
										? value.value ?? 'yes'
										: '',
								} );
							} }
						/>
						{ value.desc && (
							<span className="wopb-description">
								{ value.desc }
							</span>
						) }
						<div>
							<span
								onClick={ () => {
									setSpinner( true );
									wp.apiFetch( {
										path: '/wopb/v1/action_option',
										method: 'POST',
										data: { type: 'regenerate_font' },
									} ).then( ( res ) => {
										if ( res.success ) {
											setSpinner( false );
										}
									} );
								} }
								id="wopb-regenerate-css"
								className={ `wopb-primary-button ${
									spinner ? 'wopb-spinner' : ''
								} cursor ${ val == 'yes' ? 'active' : '' } ` }
							>
								<span className="dashicons dashicons-admin-generic"></span>
								<span className="wopb-text">
									{ __(
										'Re-Generate Google Font',
										'product-blocks'
									) }
								</span>
							</span>
						</div>
					</div>
				);
			case 'multiselect':
				const multiValue = Array.isArray( val ) ? val : [ val ];
				return (
					<Multiselect
						multikey={ key }
						value={ value }
						multiValue={ multiValue }
					/>
				);
			case 'text':
				return (
					<div className="wopb-settings-field wopb-settings-input-field">
						<input
							type="text"
							name={ key }
							value={ val }
							required={ value.required }
							onChange={ ( event ) =>
								setFormData( {
									...formData,
									[ key ]: event.target.value,
								} )
							}
						/>
						<span
							className="wopb-description"
							dangerouslySetInnerHTML={ {
								__html: value.desc,
							} }
						>
							{ value.link && (
								<a
									className="settingsLink"
									target="_blank"
									href={ value.link }
									rel="noreferrer"
								>
									{ value.linkText }
								</a>
							) }
						</span>
					</div>
				);
			case 'radio':
				const settingFieldClass = value.display
					? ' wopb-display-' + value.display
					: '';
				const radioClick = ( k ) => {
					const variationObj = {};
					if ( value.variations && value.variations[ k ] ) {
						const variation = value.variations[ k ];
						Object.keys( variation ).map( ( k ) => {
							variationObj[ k ] = variation[ k ];
						} );
					}
					setFormData( {
						...formData,
						[ key ]: k,
						...variationObj,
					} );
				};
				return (
					<div
						className={
							`wopb-settings-field wopb-radio-field` +
							settingFieldClass
						}
					>
						{ value.desc && (
							<span className="wopb-description">
								{ value.desc }
							</span>
						) }
						<div className="wopb-input-section">
							{ Object.keys( value.options ).map(
								( k, index ) => {
									return (
										<label
											className={
												'wopb-input-group' +
												( val == k
													? ' wopb-active'
													: '' )
											}
											key={ k }
											htmlFor={ key + '_' + k }
											onClick={ () => radioClick( k ) }
										>
											<span className="wopb-checkmark">
												{ icons.checkmark }
											</span>
											<input
												type="radio"
												id={ key + '_' + k }
												name={ key }
												defaultValue={ k }
												defaultChecked={ val == k }
												onChange={ () => {} }
											/>
											<span
												className="wopb-field-icon"
												dangerouslySetInnerHTML={ {
													__html: value.options[ k ],
												} }
											/>
										</label>
									);
								}
							) }
							{ value.pro &&
								Object.keys( value.pro ).map( ( k, index ) => {
									return (
										<label
											className="wopb-input-group"
											key={ k }
											htmlFor={ key + '_' + k }
											onClick={ () => radioClick( k ) }
										>
											<span className="wopb-checkmark">
												{ icons.checkmark }
											</span>
											<input
												type="radio"
												disabled={
													! wopb_option.active
												}
												id={ key + '_' + k }
												name={ key }
												defaultValue={ k }
												defaultChecked={ val == k }
											/>
											<span
												dangerouslySetInnerHTML={ {
													__html: value.pro[ k ],
												} }
											/>
											{ ! wopb_option.active && proLink }
											<br />
										</label>
									);
								} ) }
						</div>
					</div>
				);
			case 'toggle':
				return (
					<>
						<div className="wopb-settings-field wopb-control-option">
							<input
								type="checkbox"
								value={ value.value ?? 'yes' }
								name={ key }
								className={
									'wopb-addons-enable wopb-toggle-checkbox' +
									( val == ( value.value ?? 'yes' )
										? ' wopb-active'
										: '' )
								}
								id={ key }
								defaultChecked={
									val == ( value.value ?? 'yes' )
								}
								disabled={ value.pro && ! wopb_option.active }
								onChange={ ( event ) => {
									setFormData( {
										...formData,
										[ key ]: event.target.checked
											? value.value ?? 'yes'
											: '',
									} );
								} }
							/>
							<label
								htmlFor={ key }
								className="wopb-toggle-switch"
							/>
							{ value.desc && (
								<span className="wopb-description">
									{ value.desc }
									{ value.pro &&
										! wopb_option.active &&
										proLink }
								</span>
							) }
						</div>
						{ note && <div className="wopb-note">{ note }</div> }
					</>
				);
			case 'tab':
				if (
					settingActiveTab != '' &&
					options[ settingActiveTab ].attr
				) {
					return (
						<div className={ 'wopb-setting-tab-content' }>
							{ getSettingsData(
								options[ settingActiveTab ].attr,
								settingsData,
								params
							) }
						</div>
					);
				}
			case 'tag':
				return (
					<div className="wopb-settings-field wopb-settings-field-tag">
						<span
							className="wopb-tag wopb-tag-remove"
							onClick={ () => {
								setFormData( {
									...formData,
									[ key ]: '',
								} );
							} }
						>
							{ icons.minusIcon2 }
						</span>
						{ Object.keys( value.options ).map( ( k, index ) => {
							const tag = value.options[ k ];
							return (
								<span
									className={
										'wopb-tag' +
										( k == val ? ' wopb-active-tag' : '' )
									}
									onClick={ () => {
										setFormData( {
											...formData,
											[ key ]: k,
										} );
									} }
									key={ k }
								>
									{ tag }
								</span>
							);
						} ) }
					</div>
				);
			case 'layout':
				return (
					<div className="wopb-settings-field wopb-settings-field-layout">
						{ Object.keys( value.options ).map( ( k, index ) => {
							const option = value.options[ index ];
							return (
								<div
									className={
										'wopb-layout' +
										( val == option.key
											? ' wopb-active-layout'
											: '' )
									}
									key={ index }
								>
									{ val == option.key && (
										<span className="wopb-checkmark">
											{ icons.checkmark }
										</span>
									) }
									<div
										className={
											'wopb-layout-icon' +
											( option.pro ? ' pro' : '' )
										}
									>
										{ option.pro && (
											<span className="wopb-pro-text">
												{ __(
													'Pro',
													'product-blocks'
												) }
											</span>
										) }
										<span
											onClick={ () => {
												if ( option.pro ) {
													window.open(
														proLinkUrl,
														'_blank'
													);
												} else {
													const variationObj = {};
													if (
														value.variations &&
														value.variations[
															option.key
														]
													) {
														const variation =
															value.variations[
																option.key
															];
														Object.keys(
															variation
														).map( ( k ) => {
															variationObj[ k ] =
																variation[ k ];
														} );
													}
													setFormData( {
														...formData,
														[ key ]: option.key,
														...variationObj,
													} );
												}
											} }
										>
											<img src={ option.image } />
										</span>
										{ value.preview && (
											<div
												className="wopb-layout-preview-button"
												onClick={ () =>
													setLayoutPreview(
														option.image
													)
												}
											>
												{ __(
													'Preview',
													'product-blocks'
												) }
											</div>
										) }
									</div>
								</div>
							);
						} ) }
					</div>
				);
			case 'section':
				return (
					<div className="wopb-settings-section">
						<div
							className="wopb-section-header"
							id={ 'wopb-section-header-' + key }
							onClick={ ( e ) => {
								setAccordion( ( prevAccordion ) =>
									prevAccordion === key ? '' : key
								);
							} }
						>
							{ value.label && (
								<span className="wopb-settings-label">
									{ value.label }
								</span>
							) }
							{ icons.downArrow }
						</div>
						{ accordion === key && (
							<div className="wopb-section-body">
								{ value.attr &&
									getSettingsData(
										value.attr,
										settingsData,
										params
									) }
							</div>
						) }
					</div>
				);
			case 'select_item':
				options = val
					? options.filter(
							( item1 ) =>
								! val.some(
									( item2 ) => item2.key === item1.key
								)
					  )
					: options;
				const addToTable = () => {
					let finalValue = val;
					if ( selectedValue ) {
						if ( ! finalValue ) {
							finalValue = [];
						}
						finalValue.push( options[ selectedValue ] );
						setFormData( { ...formData, [ key ]: finalValue } );
					}
					selectedValue = '';
				};
				const removeFromTable = ( index ) => {
					val.splice( index, 1 );
					setFormData( { ...formData, [ key ]: val } );
				};
				const editColumnData = ( event, index ) => {
					if (
						editableColumns[ key ] &&
						editableColumns[ key ][ index ]
					) {
						if ( editableColumns[ key ][ index ].label ) {
							setFormData( ( prevFormData ) => {
								if ( prevFormData[ key ] ) {
									return {
										...prevFormData,
										[ key ]: [
											...prevFormData[ key ].slice(
												0,
												index
											), // Copy items before the updated index
											{
												...prevFormData[ key ][ index ],
												key: val[ index ].key,
												label: editableColumns[ key ][
													index
												].label, // Update the label property for the object at the given index
											},
											...prevFormData[ key ].slice(
												index + 1
											), // Copy items after the updated index
										],
									};
								}
								return {
									...prevFormData,
									[ key ]: [
										...val.slice( 0, index ), // Copy items before the updated index
										{
											key: val[ index ].key,
											label: editableColumns[ key ][
												index
											].label
												? editableColumns[ key ][
														index
												  ].label
												: val[ index ].label, // Update the label property for the object at the given index
										},
										...val.slice( index + 1 ), // Copy items after the updated index
									],
								};
							} );
						}
						setEditableColumns( ( prevEditableColumns ) => {
							const updatedObject = {
								...prevEditableColumns[ key ],
							}; // Create a copy of the object
							delete updatedObject[ index ]; // Remove the specific index from the object
							return {
								...prevEditableColumns,
								[ key ]: updatedObject,
							};
						} );
					} else {
						setEditableColumns( ( prevEditableColumns ) => ( {
							...prevEditableColumns,
							[ key ]: {
								...( prevEditableColumns[ key ] ?? {} ), // Provide an empty object if prevEditableColumns[key] is undefined
								[ index ]: {
									...prevEditableColumns[ key ]?.[ index ], // Provide an empty object if prevEditableColumns[key][index] is undefined
									label:
										formData[ key ] &&
										formData[ key ][ index ]
											? formData[ key ][ index ].label
											: val[ index ].label,
								},
							},
						} ) );
					}
				};
				const dragStart = ( event, index ) => {
					setDragElement( {
						...dragElement,
						dragFrom: index,
						offsetY:
							event.clientY -
							event.target
								.closest( '.wopb-drag-column' )
								.getBoundingClientRect().top,
					} );
				};
				const dragOver = ( event, index ) => {
					event.preventDefault();
					if ( dragElement.dragFrom != null ) {
						//scroll when mouse move top or bottom
						const rect = event.target.getBoundingClientRect();
						const distanceToTop =
							rect.top - settingBody.getBoundingClientRect().top;
						const distanceToBottom =
							settingBody.getBoundingClientRect().bottom -
							rect.bottom;
						if ( distanceToTop < 50 ) {
							settingBody.scrollTop -= 10;
						} else if ( distanceToBottom < 50 ) {
							settingBody.scrollTop += 10;
						}

						if (
							( dragElement.key == key &&
								( index || index == 0 ) &&
								dragElement.dragTo != index ) ||
							! dragElement.dragTo
						) {
							let newList = val;
							const itemDragged = newList[ dragElement.dragFrom ];
							const remainingItems = newList.filter(
								( item, k ) => k != dragElement.dragFrom
							);
							newList = [
								...remainingItems.slice( 0, index ),
								itemDragged,
								...remainingItems.slice( index ),
							];
							setDragElement( {
								...dragElement,
								key,
								dragFrom: index,
								dragTo: index,
							} );
							setFormData( { ...formData, [ key ]: newList } );
						}
						if ( dragElement.dragTo == index ) {
							const dragHighlight = document.querySelector(
								'.wopb-drag-highlight'
							);
							updateStyle(
								event.clientX -
									dragHighlight.getBoundingClientRect().left,
								event.clientY -
									( dragHighlight.getBoundingClientRect()
										.top +
										dragElement.offsetY )
							);
						}
					}
				};
				const dropElement = ( event ) => {
					if ( dragElement.dragFrom != null ) {
						setDragElement( {} );
					}
				};

				const updateStyle = ( left, top ) => {
					setDragElement( {
						...dragElement,
						key,
						style: {
							left: left - 8 + 'px',
						},
					} );
				};

				return (
					<div className="wopb-settings-field wopb-select-item">
						{ value.desc && (
							<>
								<span className="wopb-description">
									{ value.desc }
								</span>
							</>
						) }
						{ val && (
							<div className="wopb-selected-item">
								<ul>
									{ val.map( ( item, index ) => {
										return (
											<li
												className={
													'wopb-column-item-list' +
													( dragElement.key == key &&
													( dragElement.dragTo ||
														dragElement.dragTo ==
															0 ) &&
													dragElement.dragTo == index
														? ' wopb-drag-highlight'
														: '' )
												}
												key={ index }
												onMouseMove={ ( event ) =>
													dragOver( event, index )
												}
												onMouseUp={ ( event ) =>
													dropElement( event, index )
												}
												data-index={ index }
											>
												<div
													className="wopb-drag-section"
													style={
														dragElement.key ==
															key &&
														( dragElement.dragTo ||
															dragElement.dragTo ==
																0 ) &&
														dragElement.dragTo ==
															index
															? dragElement.style
															: {}
													}
												>
													<span
														className="wopb-drag-column"
														onMouseDown={ (
															event
														) =>
															dragStart(
																event,
																index
															)
														}
													>
														{ icons.drag }
													</span>
													<span className="wopb-data-column">
														<span className="wopb-column-data">
															<span
																className={
																	'wopb-column-data-text' +
																	( editableColumns[
																		key
																	] &&
																	editableColumns[
																		key
																	][
																		index
																	] &&
																	editableColumns[
																		key
																	][ index ]
																		.label ==
																		''
																		? ' empty'
																		: '' )
																}
																onInput={ () => {
																	setEditableColumns(
																		(
																			prevEditableColumns
																		) => ( {
																			...prevEditableColumns,
																			[ key ]:
																				{
																					...( prevEditableColumns[
																						key
																					] ??
																						{} ), // Provide an empty object if prevEditableColumns[key] is undefined
																					[ index ]:
																						{
																							...prevEditableColumns[
																								key
																							]?.[
																								index
																							], // Provide an empty object if prevEditableColumns[key][index] is undefined
																							label: event
																								.target
																								.innerText,
																						},
																				},
																		} )
																	);
																} }
																contentEditable={
																	!! (
																		editableColumns[
																			key
																		] &&
																		editableColumns[
																			key
																		][
																			index
																		]
																	)
																}
																suppressContentEditableWarning={
																	true
																}
															>
																{ __(
																	item.label
																) }
															</span>
															<span
																className={
																	'wopb-edit-icon' +
																	( editableColumns[
																		key
																	] &&
																	editableColumns[
																		key
																	][
																		index
																	] &&
																	editableColumns[
																		key
																	][ index ]
																		.label ==
																		''
																		? ' disable'
																		: '' )
																}
																onClick={ () =>
																	( editableColumns[
																		key
																	] &&
																		editableColumns[
																			key
																		][
																			index
																		] &&
																		editableColumns[
																			key
																		][
																			index
																		]
																			.label !=
																			'' ) ||
																	! (
																		editableColumns[
																			key
																		] &&
																		editableColumns[
																			key
																		][
																			index
																		]
																	)
																		? editColumnData(
																				event,
																				index
																		  )
																		: ''
																}
															>
																{ editableColumns[
																	key
																] &&
																editableColumns[
																	key
																][ index ]
																	? icons.save
																	: icons.edit }
															</span>
														</span>
													</span>
													<span
														className="wopb-action-column"
														onClick={ () =>
															removeFromTable(
																index
															)
														}
													>
														{ icons.delete }
													</span>
												</div>
											</li>
										);
									} ) }
								</ul>
							</div>
						) }
						<div className="wopb-select">
							<div className="wopb-input-group">
								<div className="wopb-settings-field wopb-select-field">
									<div
										className="wopb-selected-content"
										onClick={ () =>
											openDropdown(
												dropdown && dropdown != key
													? key
													: dropdown &&
													  dropdown == key
													? ''
													: key
											)
										}
									>
										<span>
											{ selectedValue
												? options[ selectedValue ].label
												: options[ 0 ]
												? options[ 0 ].label
												: __(
														'Select Item',
														'product-blocks'
												  ) }
										</span>
										<span
											className={
												'wopb-select-arrow' +
												( dropdown && dropdown === key
													? ' wopb-arrow-up'
													: '' )
											}
										>
											{ icons.downArrow }
										</span>
									</div>

									{ dropdown && dropdown == key && (
										<div className="wopb-select-dropdown">
											{ options.map( ( item, index ) => {
												return (
													<div
														className={
															'wopb-select-item' +
															( val == index
																? ' active'
																: '' )
														}
														key={ index }
														onClick={ () => {
															openDropdown(
																false
															);
															selectedValue =
																index;
														} }
													>
														{ item.label }
													</div>
												);
											} ) }
										</div>
									) }
								</div>
								<a
									className="wopb-add-item"
									onClick={ () => addToTable() }
								>
									{ icons.plusIcon3 }{ ' ' }
									{ __( 'Add', 'product-blocks' ) }
								</a>
							</div>
						</div>
					</div>
				);
			case 'typography':
				return <Typography { ...params } />;
				break;
			case 'border':
				return <Border { ...params } />;
				break;
			case 'dimension':
				return <Dimension { ...params } />;
				break;
			default:
				break;
		}
	};

	const dependCheck = ( conditionData ) => {
		const dependsValue =
			formData && conditionData?.key in formData
				? formData[ conditionData?.key ]
				: conditionData?.key in settingsData
				? settingsData[ conditionData?.key ]
				: '';
		switch ( conditionData?.condition ) {
			case '==':
				if (
					! ( dependsValue == conditionData?.value ) &&
					! (
						Array.isArray( conditionData?.value ) &&
						(conditionData?.value).includes( dependsValue )
					)
				) {
					return false;
				}
			default:
				break;
		}

		return true;
	};

	return (
		<>
			{ Object.keys( data ).map( ( key, index ) => {
				const value = data[ key ];
				if ( value.depends ) {
					let conditionPassed = false;
					if ( Array.isArray( value.depends ) ) {
						conditionPassed = value.depends.every(
							( conditionData ) => {
								return dependCheck( conditionData );
							}
						);
					} else {
						conditionPassed = dependCheck( value?.depends );
					}
					if ( ! conditionPassed ) {
						return;
					}
				}
				const noChildAttr =
					! value.attr && ! value.fields && value.type !== 'tab';
				let wrapperClass =
					`wopb-${ value.type }-wrap` +
					( noChildAttr ? ' wopb-settings-wrap' : '' );
				wrapperClass += addonKey === key ? ' wopb-addon-key' : '';
				wrapperClass += value.alignment
					? ' wopb-alignment-' + value.alignment
					: '';

				return (
					<Fragment key={ index }>
						{ value.type == 'hidden' && (
							<input
								key={ key }
								type="hidden"
								name={ key }
								defaultValue={ value.value }
							/>
						) }
						{ value.type != 'hidden' && (
							<>
								{ ( value.hr_line || value.hr_line2 ) && (
									<div
										className={
											'wopb-hr-line' +
											( value.hr_line2
												? ' wopb-hr-line2'
												: '' )
										}
									/>
								) }
								{ value.type == 'heading' && (
										<div className="wopb-settings-heading">
											{ value.label }
										</div>
									) && (
										<>
											{ value.desc && (
												<div className="wopb-settings-subheading">
													{ value.desc }
												</div>
											) }
										</>
									) }
								{ value.type != 'heading' && (
									<>
										<div className={ wrapperClass }>
											{ value.label &&
												value.type !== 'section' && (
													<strong>
														{ value.label }
														{ value.help && (
															<Tooltip
																content={
																	value.help
																}
															>
																<span className=" cursor dashicons dashicons-editor-help"></span>
															</Tooltip>
														) }
													</strong>
												) }
											<div
												className={
													noChildAttr
														? 'wopb-settings-field-wrap'
														: ''
												}
											>
												{ getField(
													key,
													value,
													settingsData
												) }
											</div>
										</div>
									</>
								) }
							</>
						) }
					</Fragment>
				);
			} ) }
		</>
	);
};

const getProHtml = ( { tags, func, data } ) => {
	return (
		<div className="wopb-addon-lock-container-overlay">
			<div className="wopb-addon-lock-container">
				<div className="wopb-popup-unlock">
					<img
						src={ `${ wopb_option.url }/assets/img/${ data.icon }` }
						alt="lock icon"
					/>
					<div className="wopb-heading wopb-center">
						{ data?.title }
					</div>
					<div className="wopb-description">
						{ data?.description }
					</div>
					{ getUpgradeProBtn( '', tags ) }
					<a
						onClick={ () => {
							func( false );
						} }
						className="wopb-popup-close"
					>
						{ IconPack.close_line }
					</a>
				</div>
			</div>
		</div>
	);
};

const getUpgradeProBtn = ( url, tags, label = '' ) => {
	const title = label ? label : __( 'Upgrade to Pro', 'product-blocks' );
	return (
		<a
			href={ LinkGenerator( url, tags ) }
			className={ 'wopb-upgrade-pro-btn' }
			target="_blank"
			rel="noreferrer"
		>
			{ title }&nbsp; &#10148;
		</a>
	);
};

const getPrimaryButton = ( url, tags, label, classname = '' ) => {
	return (
		<a
			href={ LinkGenerator( url, tags ) }
			className={ 'wopb-primary-button ' + classname }
			target="_blank"
			rel="noreferrer"
		>
			{ label }
		</a>
	);
};

const getSecondaryButton = ( url, tags, label, classname = '' ) => {
	return (
		<a
			href={ LinkGenerator( url, tags ) }
			className={ 'wopb-secondary-button ' + classname }
			target="_blank"
			rel="noreferrer"
		>
			{ label }
		</a>
	);
};

export {
	getSettingsData,
	getUpgradeProBtn,
	getProHtml,
	getPrimaryButton,
	getSecondaryButton,
};
