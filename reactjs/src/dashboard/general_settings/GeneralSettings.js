import React, { useEffect, useState } from 'react';
const { __ } = wp.i18n;
import { getSecondaryButton, getSettingsData } from '../Settings';
import Skeleton from '../utility/Skeleton';
import Toast from '../utility/Toast';
import './generalSettings.scss';

const GeneralSettings = () => {
	const [ settings, setSettings ] = useState( {} );
	const [ toastMessages, setToastMessages ] = useState( {
		state: false,
		status: '',
	} );
	const [ formData, setFormData ] = useState( {} );
	const [ dropdown, openDropdown ] = useState();
	const [ spinner, setSpinner ] = useState( false );
	const [ saveLoading, setSaveLoading ] = useState( false );

	const generalAttr = {
		container_width: {
			type: 'number',
			label: __( 'Container Width', 'product-blocks' ),
			default: '1140',
			desc: __(
				'Change Container Width of the WowStore Template.',
				'product-blocks'
			),
			help: __(
				'Here you can increase or decrease the container width. It will be applicable when you create any dynamic template with the WowStore Builder or select WowStore while creating a page.',
				'product-blocks'
			),
		},
		editor_container: {
			type: 'select',
			label: __( 'Editor Container', 'product-blocks' ),
			options: {
				theme_default: __( 'Theme Default', 'product-blocks' ),
				full_width: __( 'Full Width', 'product-blocks' ),
			},
			default: 'full_width',
			desc: __( 'Select Editor Container Width.', 'product-blocks' ),
			help: __(
				'Decide and select whether you want to use theme width or full width while editing a page.',
				'product-blocks'
			),
		},
		hide_import_btn: {
			type: 'toggle',
			label: __( 'Hide Import Button', 'product-blocks' ),
			default: '',
			desc: __(
				'Hide Import Layout Button from the Gutenberg Editor.',
				'product-blocks'
			),
			help: __(
				'Click on the check box to hide the block library (layout importer) option from Gutenberg editor.',
				'product-blocks'
			),
		},
		disable_google_font: {
			type: 'switchButton',
			label: __( 'Disable All Google Fonts', 'product-blocks' ),
			default: '',
			desc: __(
				'Disable All Google Fonts From Frontend and Backend WowStore Blocks.',
				'product-blocks'
			),
			help: __(
				"Click the check box to disable all Google Fonts from WowStore's typography options.",
				'product-blocks'
			),
		},
	};

	const params = {
		formData,
		setFormData,
		dropdown,
		openDropdown,
	};

	useEffect( () => {
		getAllSettings();
	}, [] );

	const getAllSettings = () => {
		wp.apiFetch( {
			path: '/wopb/v2/get_all_settings',
			method: 'POST',
			data: {
				key: 'key',
				value: 'value',
			},
		} ).then( ( res ) => {
			if ( res.success ) {
				setSettings( res.settings );
			}
		} );
	};

	const handleFormSubmit = ( e ) => {
		e.preventDefault();
		setSaveLoading( true );
		const data = new FormData( e.target );
		let formObject = {};

		for ( const element of e.target.elements ) {
			if ( element.name ) {
				if ( element.type === 'select-multiple' ) {
					formObject[ element.name ] = data.getAll( element.name );
				} else if (
					element.dataset.customprop == 'custom_multiselect'
				) {
					formObject[ element.name ] = element.value
						? element.value.split( ',' )
						: [];
				} else if ( element.type === 'radio' && element.checked ) {
					formObject[ element.name ] = element.value;
				}
			}
		}
		formObject = { ...formObject, ...formData };
		wp.apiFetch( {
			path: '/wopb/v2/save_plugin_settings',
			method: 'POST',
			data: {
				settings: formObject,
				type: 'add',
			},
		} ).then( ( res ) => {
			if ( res.success ) {
				setSaveLoading( false );
				setToastMessages( {
					status: 'success',
					messages: [ res.message ],
					state: true,
				} );
			}
		} );
	};

	return (
		<div className="wopb-dashboard-general-settings-container">
			<div className="wopb-general-settings wopb-dash-item-con">
				<div>
					{ toastMessages.state && (
						<Toast
							delay={ 2000 }
							toastMessages={ toastMessages }
							setToastMessages={ setToastMessages }
						/>
					) }
					<div className="wopb-heading">
						{ __( 'General Settings', 'product-blocks' ) }
					</div>
					{ Object.keys( settings ).length > 0 ? (
						<form onSubmit={ handleFormSubmit } action="">
							{ getSettingsData(
								generalAttr,
								settings,
								params,
								spinner,
								setSpinner
							) }
							<div className="wopb-data-message"></div>
							<div>
								<button
									type="submit"
									className="cursor wopb-form-save-btn wopb-primary-button"
								>
									{ __( 'Save Settings', 'product-blocks' ) }
									{ saveLoading && (
										<span className="wopb-loading" />
									) }
								</button>
							</div>
						</form>
					) : (
						<div className="skeletonOverflow">
							{ Array( 6 )
								.fill( 1 )
								.map( ( val, k ) => {
									return (
										<div key={ k }>
											<Skeleton
												type="custom_size"
												classes="loop"
												c_s={ {
													size1: 150,
													unit1: 'px',
													size2: 20,
													unit2: 'px',
													br: 2,
												} }
											/>
											<Skeleton
												type="custom_size"
												classes="loop"
												c_s={ {
													size1: '',
													unit1: '',
													size2: 34,
													unit2: 'px',
													br: 2,
												} }
											/>
											<Skeleton
												type="custom_size"
												classes="loop"
												c_s={ {
													size1: 350,
													unit1: 'px',
													size2: 20,
													unit2: 'px',
													br: 2,
												} }
											/>
										</div>
									);
								} ) }
							<Skeleton
								type="custom_size"
								c_s={ {
									size1: 120,
									unit1: 'px',
									size2: 36,
									unit2: 'px',
									br: 2,
								} }
							/>
						</div>
					) }
				</div>
			</div>
			<div className="wopb-general-settings-content-right">
				<div className="wopb-dashboard-promotions-features">
					<div className="wopb-dashboard-feature-request wopb-dash-item-con">
						<div className="wopb-heading">
							{ __( 'Feature Request', 'product-blocks' ) }
						</div>
						<span className="wopb-description">
							{ __(
								'Can’t find your desired feature? Let us know your requirements. We will definitely take them into our consideration.',
								'product-blocks'
							) }
						</span>
						{ getSecondaryButton(
							'https://www.wpxpo.com/wowstore/roadmap/?utm_source=db-wstore-setting&utm_medium=feature-request&utm_campaign=wstore-dashboard',
							'',
							'Request a Feature',
							''
						) }
					</div>
					<div className="wopb-dashboard-web-community wopb-dash-item-con">
						<div className="wopb-heading">
							{ __( 'WowStore Community', 'product-blocks' ) }
						</div>
						<span className="wopb-description">
							{ __(
								'Join the Facebook community of WowStore to stay up-to-date and share your thoughts and feedback.',
								'product-blocks'
							) }
						</span>
						{ getSecondaryButton(
							'https://www.facebook.com/groups/wowcommercecommunity',
							'',
							'Join WowStore Community',
							''
						) }
					</div>
					<div className="wopb-dashboard-news-tips wopb-dash-item-con">
						<div className="wopb-heading">
							{ __( 'News, Tips & Update', 'product-blocks' ) }
						</div>
						<a
							target="_blank"
							href="https://wpxpo.com/docs/wowstore/getting-started/?utm_source=db-wstore-setting&utm_medium=gsW&utm_campaign=wowstore-dashboard"
							rel="noreferrer"
						>
							{ __(
								'Getting Started with WowStore',
								'product-blocks'
							) }
						</a>
						<a
							target="_blank"
							href="https://wpxpo.com/docs/wowstore/woo-builder/?utm_source=db-wstore-setting&utm_medium=builder_guide&utm_campaign=wowstore-dashboard"
							rel="noreferrer"
						>
							{ __(
								'How to use the WooCommerce Builder',
								'product-blocks'
							) }
						</a>
						<a
							target="_blank"
							href="https://wpxpo.com/docs/wowstore/add-ons/?utm_source=db-wstore-setting&utm_medium=addons_guide&utm_campaign=wowstore-dashboard"
							rel="noreferrer"
						>
							{ __(
								'How to use the WowStore Addons',
								'product-blocks'
							) }
						</a>
						<a
							target="_blank"
							href="https://www.wpxpo.com/category/wowstore/?utm_source=db-wstore-setting&utm_medium=wowstore_blog&utm_campaign=wowstore-dashboard"
							rel="noreferrer"
						>
							{ __( 'WowStore Blog', 'product-blocks' ) }
						</a>
					</div>
					<div className="wopb-dashboard-rating wopb-dash-item-con">
						<div className="wopb-heading">
							{ __( 'Show your love', 'product-blocks' ) }
						</div>
						<span className="wopb-description">
							{ __(
								'Enjoying WowStore? Give us a 5 Star review to support our ongoing work.',
								'product-blocks'
							) }
						</span>
						{ getSecondaryButton(
							'https://wordpress.org/support/plugin/product-blocks/reviews/',
							'',
							'Rate it Now',
							''
						) }
					</div>
				</div>
			</div>
		</div>
	);
};

export default GeneralSettings;
