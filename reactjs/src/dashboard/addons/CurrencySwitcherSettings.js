const { __ } = wp.i18n;
import React, { useState } from 'react';

const CurrencySwitcherSettings = ( props ) => {
	const {
		attr,
		addonKey,
		settings,
		handleSwictherFormSubmit,
		header,
		footer,
	} = props;

	const [ lockShow, setLockShow ] = useState( 0 );

	const currencyDuplicate = {
		wopb_currency:
			attr.wopb_default_currency.value == 'USD' ? 'BDT' : 'USD',
		wopb_currency_decimals: '2',
		wopb_currency_symbol_position: 'left',
		wopb_currency_rate: 1,
		wopb_currency_exchange_fee: 0,
		wopb_currency_exclude_gateways: [],
	};
	const [ args, setArgs ] = useState( {
		wopb_currency_switcher: 'true',
		wopb_default_currency: attr.wopb_default_currency.value,
		wopb_currencies: settings.wopb_currencies || [ currencyDuplicate ],
	} );

	const handleDuplication = () => {
		setArgs( {
			...args,
			wopb_currencies: [ ...args.wopb_currencies, currencyDuplicate ],
		} );
	};

	const handleRemove = ( index ) => {
		const newCurr = args.wopb_currencies;
		if ( index > -1 ) {
			newCurr.splice( index, 1 );
			setArgs( { ...args, wopb_currencies: newCurr } );
		}
	};

	const setValue = ( key, val, index ) => {
		const newCurr = args.wopb_currencies;
		newCurr[ index ][ key ] = val;

		setArgs( {
			...args,
			wopb_currencies: newCurr,
		} );
	};

	const setNumValue = ( key, val, index, min = 0 ) => {
		const newCurr = args.wopb_currencies;
		val = val === '' || isNaN( val ) ? '' : val < min ? min : val;
		newCurr[ index ][ key ] = val;

		setArgs( {
			...args,
			wopb_currencies: newCurr,
		} );
	};

	const wopbCurrencyFields = attr.wopb_currencies.fields;
	const inputField = ( type, typeArgs ) => {
		if ( type == 'select' ) {
			return (
				<div className="wopb-settings-wrap wopb-select-wrap">
					<strong className="wopb-title">{ typeArgs.label }</strong>
					<div className="wopb-settings-field-wrap">
						<div className="wopb-settings-field">
							<select
								name={ typeArgs.name }
								onChange={ ( val ) =>
									setValue(
										typeArgs.name,
										val.target.value,
										typeArgs.index
									)
								}
								defaultValue={ typeArgs.value }
							>
								{ Object.keys( typeArgs.options ).map(
									( key, i ) => {
										return (
											<option
												key={ i }
												value={ key }
												dangerouslySetInnerHTML={ {
													__html: typeArgs.options[
														key
													],
												} }
											/>
										);
									}
								) }
							</select>
						</div>
					</div>
				</div>
			);
		} else if ( type == 'number' ) {
			return (
				<div className="wopb-settings-wrap wopb-number-wrap ">
					<strong className="wopb-title">{ typeArgs.label }</strong>
					<div className="wopb-settings-field-wrap">
						<div className="wopb-settings-field">
							<input
								type="number"
								name={ typeArgs.name }
								value={ typeArgs.value }
								onChange={ ( val ) =>
									setNumValue(
										typeArgs.name,
										val.target.value,
										typeArgs.index,
										typeArgs.min
									)
								}
							/>
						</div>
					</div>
				</div>
			);
		} else if ( type == 'toggle' ) {
			return (
				<>
					<input
						value={ typeArgs.value }
						onChange={ ( val ) => {
							const newCurrencies = args.wopb_currencies;
							newCurrencies[
								typeArgs.index
							].wopb_currency_rate = 1;
							newCurrencies[
								typeArgs.index
							].wopb_currency_exchange_fee = 0;
							val.target.checked &&
								setArgs( {
									...args,
									wopb_default_currency: val.target.value,
									wopb_currencies: newCurrencies,
								} );
						} }
						type="checkbox"
						id={ typeArgs.id }
						className={
							'wopb-addons-enable wopb-toggle-checkbox wopb-currency-reserve-btn' +
							( typeArgs.ischecked ? ' wopb-active' : '' )
						}
						checked={ typeArgs.ischecked }
					/>
					<label
						htmlFor={ typeArgs.id }
						className="wopb-toggle-switch wopb-currency-reserve-btn"
					/>
				</>
			);
		}
	};

	return (
		<form
			onSubmit={ ( e ) => handleSwictherFormSubmit( e, args, addonKey ) }
		>
			{ header }
			<div className="wopb-addon-setting-body">
				<div className="wopb-settings-wrap wopb-repeatable-wrap">
					<div className="wopb-settings-field-wrap">
						{ args.wopb_currencies.map( ( item, index ) => {
							const ischecked =
								item.wopb_currency ==
								args.wopb_default_currency;
							return (
								<div
									key={ index }
									className="wopb-repeat-section"
									data-base-repeater="wopb_currencies"
								>
									<div
										className="wopb-collapse-header"
										onClick={ () => {
											if ( lockShow == index + 1 ) {
												setLockShow( 0 );
											} else {
												setLockShow( index + 1 );
											}
										} }
									>
										<span className="wopb-header-content">
											<span className="wopb-control-option">
												{ inputField( 'toggle', {
													id: 'check' + index,
													index,
													value: item.wopb_currency,
													ischecked,
												} ) }
											</span>
											<span
												className="wopb-header-text"
												dangerouslySetInnerHTML={ {
													__html: attr.wopb_currencies
														.depend_options[
														item.wopb_currency
													],
												} }
											/>
										</span>
										<span className="wopb-control">
											{ ! ischecked && (
												<span
													onClick={ () =>
														item.wopb_currency !==
															args.wopb_default_currency &&
														handleRemove( index )
													}
													className="dashicons dashicons-no-alt cursor"
												/>
											) }
											{ lockShow == index + 1 ? (
												<span className="dashicons dashicons-arrow-right-alt2 wopb-right-arrow" />
											) : (
												<span className="dashicons dashicons-arrow-down-alt2 wopb-down-arrow" />
											) }
										</span>
									</div>

									<div
										className="wopb-collapse-body"
										style={ {
											display:
												lockShow == index + 1
													? 'block'
													: 'none',
										} }
									>
										<div className="wopb-settings-wrap wopb-toggle-wrap">
											<div className="wopb-settings-field-wrap">
												<div className="wopb-settings-field wopb-control-option">
													{ inputField( 'toggle', {
														id: 'checkx' + index,
														index,
														value: item.wopb_currency,
														ischecked,
													} ) }
													<span>Set as Default</span>
												</div>
											</div>
										</div>

										{ inputField( 'select', {
											name: 'wopb_currency',
											label: wopbCurrencyFields
												.wopb_currency.label,
											index,
											value: item.wopb_currency,
											options:
												wopbCurrencyFields.wopb_currency
													.options,
										} ) }
										{ inputField( 'number', {
											name: 'wopb_currency_decimals',
											label: wopbCurrencyFields
												.wopb_currency_decimals.label,
											index,
											value: item.wopb_currency_decimals,
										} ) }
										{ inputField( 'select', {
											name: 'wopb_currency_symbol_position',
											label: wopbCurrencyFields
												.wopb_currency_symbol_position
												.label,
											index,
											value: item.wopb_currency_symbol_position,
											options:
												wopbCurrencyFields
													.wopb_currency_symbol_position
													.options,
										} ) }
										{ inputField( 'number', {
											name: 'wopb_currency_rate',
											label: wopbCurrencyFields
												.wopb_currency_rate.label,
											index,
											value: item.wopb_currency_rate,
										} ) }
										{ inputField( 'number', {
											name: 'wopb_currency_exchange_fee',
											label: wopbCurrencyFields
												.wopb_currency_exchange_fee
												.label,
											index,
											value: item.wopb_currency_exchange_fee,
										} ) }

										<div className="wopb-settings-wrap wopb-switch-wrap ">
											<strong className="wopb-title">
												{
													wopbCurrencyFields
														.wopb_currency_exclude_gateways
														.label
												}
											</strong>
											<div className="wopb-settings-field-wrap">
												<div className="wopb-settings-field wopb-settings-field-inline">
													{ Object.keys(
														wopbCurrencyFields
															.wopb_currency_exclude_gateways
															.options
													)?.map( ( key, i ) => {
														return (
															<div key={ key }>
																<label className="wopb-multi-switch">
																	<input
																		type="checkbox"
																		value={
																			key
																		}
																		name="wopb_currency_exclude_gateways"
																		defaultChecked={ item.wopb_currency_exclude_gateways?.includes(
																			key
																		) }
																		onChange={ (
																			val
																		) => {
																			const excludePayments =
																				item.wopb_currency_exclude_gateways ||
																				[];
																			if (
																				val
																					.target
																					.checked
																			) {
																				if (
																					! excludePayments.includes(
																						val
																							.target
																							.value
																					)
																				) {
																					excludePayments.push(
																						val
																							.target
																							.value
																					);
																				}
																			} else if (
																				excludePayments.includes(
																					val
																						.target
																						.value
																				)
																			) {
																				excludePayments.splice(
																					excludePayments.indexOf(
																						val
																							.target
																							.value
																					),
																					1
																				);
																			}
																			setValue(
																				'wopb_currency_exclude_gateways',
																				excludePayments,
																				index
																			);
																		} }
																	/>
																	{
																		wopbCurrencyFields
																			.wopb_currency_exclude_gateways
																			.options[
																			key
																		]
																	}
																</label>
															</div>
														);
													} ) }
												</div>
											</div>
										</div>
									</div>
								</div>
							);
						} ) }
					</div>
					<span
						className="cursor wopb-add-btn"
						onClick={ handleDuplication }
					>
						Add new{ ' ' }
						<span className="dashicons dashicons-plus-alt2" />
					</span>
				</div>
				<div className="wopb-data-message" />
			</div>
			{ footer }
		</form>
	);
};

export default CurrencySwitcherSettings;
