const { __ } = wp.i18n;
import { useEffect, useState } from 'react';
import IconPack from '../../helper/fields/tools/IconPack';
import icons from '../../helper/icons';
import Toast from '../utility/Toast';
import Skeleton from '../utility/Skeleton';
import './scss/addons.scss';
import CurrencySwitcherSettings from './CurrencySwitcherSettings';
import Modal from '../utility/Modal';
import {
	getPrimaryButton,
	getSecondaryButton,
	getProHtml,
	getSettingsData,
} from '../Settings';

const Addons = ( props ) => {
	const [ popUp, setPopUp ] = useState( '' );
	const [ lockShow, setLockShow ] = useState( false );
	const [ settings, setSettings ] = useState( {} );
	const [ saveLoading, setSaveLoading ] = useState( false );
	const [ toastMessages, setToastMessages ] = useState( {
		state: false,
		messages: [],
		status: '',
	} );
	const [ formData, setFormData ] = useState( {} );
	const [ editableColumns, setEditableColumns ] = useState( [] );
	const [ settingActiveTab, setSettingActiveTab ] = useState( '' );
	const [ colorPicker, openColorPicker ] = useState( {
		key: '',
		open: false,
	} );
	const [ dropdown, openDropdown ] = useState();
	const [ layoutPreview, setLayoutPreview ] = useState();
	const [ dragElement, setDragElement ] = useState( {
		key: '',
		dragFrom: null,
		dragTo: null,
		offsetX: null,
		offsetY: null,
		style: null,
	} );
	const [ accordion, setAccordion ] = useState();

	const addons = wopb_dashboard_pannel.addons;
	const addons_settings = wopb_dashboard_pannel.addons_settings || {};
	// const [addons_settings, setAddonSettings] = useState({});
	const [ modalContent, setModalContent ] = useState( '' );
	const { setCurrent } = props;
	const addonMenus = {
		wopb_builder: 'builder',
		wopb_custom_font: 'custom-font',
		wopb_templates: 'saved-templates',
		wopb_size_chart: 'size-chart',
	};

	const params = {
		setSettings,
		formData,
		setFormData,
		editableColumns,
		setEditableColumns,
		settingActiveTab,
		colorPicker,
		openColorPicker,
		dropdown,
		openDropdown,
		setLayoutPreview,
		dragElement,
		setDragElement,
		accordion,
		setAccordion,
	};

	useEffect( () => {
		document.addEventListener( 'mousemove', handleMouseMove );
	}, [ dragElement ] );

	useEffect( () => {
		getAllSettings();
		document.addEventListener( 'mousedown', handleClickOutside );
		return () =>
			document.removeEventListener( 'mousedown', handleClickOutside );
	}, [] );

	useEffect( () => {
		if ( accordion ) {
			const element = document.getElementById(
				`wopb-section-header-${ accordion }`
			);
			if ( element ) {
				element.scrollIntoView();
			}
		}
	}, [ accordion ] );

	const getAllSettings = () => {
		wp.apiFetch( {
			path: '/wopb/v2/get_all_settings',
			method: 'POST',
			data: {
				key: 'key',
				value: 'value',
			},
		} ).then( ( res ) => {
			if ( res.success ) {
				setSettings( res.settings );
				// setAddonSettings(res.addons_settings)
			}
		} );
	};

	const addonData = () => {
		const _data = wopb_dashboard_pannel.addons;
		const final = {
			build: { name: 'Build Store Smartly', data: [] },
			sales: { name: 'Sales Boosters', data: [] },
			checkout_cart: { name: 'Checkout & Cart', data: [] },
			exclusive: { name: 'Exclusive Flexibility', data: [] },
			// conversion: {name: 'Conversion Focus', data: []},
			integration: { name: 'Integration Addons', data: [] },
		};
		Object.keys( _data ).forEach( ( el ) => {
			final[ _data[ el ].type || 'build' ].data.push(
				Object.assign( _data[ el ], { key: el } )
			);
		} );

		Object.keys( final ).forEach( ( el ) => {
			const temp = final[ el ].data;
			temp.sort( ( a, b ) => a.priority - b.priority );
			final[ el ].data = temp;
		} );

		return final;
	};

	const defaultArray = ( settingsArr, obj, prev = '' ) => {
		for ( const k in obj ) {
			if ( typeof obj[ k ] === 'object' && k != 'default' ) {
				defaultArray( settingsArr, obj[ k ], k );
			}
			if ( prev && k == 'default' && ! settings.hasOwnProperty( prev ) ) {
				settingsArr[ prev ] = obj[ k ];
			}
		}
		return settingsArr;
	};

	const changeAddonStatus = ( key ) => {
		const value = settings[ key ] == 'true' ? 'false' : 'true';
		if ( ! wopb_dashboard_pannel.active && addons[ key ].is_pro ) {
			setSettings( { ...settings, [ key ]: 'false' } );
			setLockShow( true );
		} else {
			setSettings( { ...settings, [ key ]: value } );
			wp.apiFetch( {
				path: '/wopb/v2/addon_block_action',
				method: 'POST',
				data: {
					key,
					value,
				},
			} ).then( ( res ) => {
				if ( res.success ) {
					let subMenu;
					if (
						[
							'wopb_templates',
							'wopb_custom_font',
							'wopb_builder',
							'wopb_size_chart',
						].includes( key )
					) {
						subMenu = document.getElementById(
							'productx-submenu-' +
								key
									.replace( 'templates', 'saved_templates' )
									.replace( 'wopb_', '' )
									.replace( '_', '-' )
						);
						subMenu
							? ( subMenu.style.display =
									value == 'true' ? 'block' : 'none' )
							: '';
						const otherMenu = document.getElementById(
							key
								.replace( 'templates', 'saved_templates' )
								.replace( 'wopb_', 'wopb-dasnav-' )
								.replace( '_', '-' )
						);
						otherMenu
							? ( otherMenu.style.display =
									value == 'true' ? '' : 'none' )
							: '';
					}

					// Filter Default data and Generate CSS
					let arr = {};
					if ( value == 'true' ) {
						arr = defaultArray( {}, addons_settings[ key ], key );
					}
					wp.apiFetch( {
						path: '/wopb/v2/save_plugin_settings',
						method: 'POST',
						data: {
							key,
							settings: arr,
							type: value == 'true' ? 'add' : 'delete',
						},
					} );

					setTimeout( function () {
						setToastMessages( {
							status: 'success',
							messages: [ res.message ],
							state: true,
						} );
						if ( value === 'true' ) {
							if ( addonMenus[ key ] ) {
								if ( subMenu ) {
									subMenu.click();
								} else {
									window.location.hash =
										'#' + addonMenus[ key ];
								}
								setCurrent( addonMenus[ key ] );
								window.scrollTo( 0, 0 );
							}
						}
					}, 400 );
				}
			} );
		}
	};
	const handleFormSubmit = ( e, switcherData = '', key = '' ) => {
		setSaveLoading( true );
		e.preventDefault();
		let formObject = {};
		if ( ! switcherData ) {
			const data = new FormData( e.target );

			for ( const element of e.target.elements ) {
				if ( element.name ) {
					if ( element.type === 'select-multiple' ) {
						formObject[ element.name ] = data.getAll(
							element.name
						);
					} else if (
						element.dataset.customprop == 'custom_multiselect'
					) {
						formObject[ element.name ] = element.value
							? element.value.split( ',' )
							: [];
					} else if ( element.type === 'radio' && element.checked ) {
						formObject[ element.name ] = element.value;
					}
				}
			}
		} else {
			formObject = switcherData;
		}
		formObject = { ...formObject, ...formData };
		wp.apiFetch( {
			path: '/wopb/v2/save_plugin_settings',
			method: 'POST',
			data: {
				key,
				type: 'add',
				settings: formObject,
			},
		} ).then( ( res ) => {
			if ( res.success ) {
				setSettings( res.settings );
				setSaveLoading( false );
				setToastMessages( {
					status: 'success',
					messages: [ res.message ],
					state: true,
				} );
			}
		} );
	};

	const handleClickOutside = ( e ) => {
		if (
			! e.target.closest( '.wopb-addon-settings-popup' ) &&
			! e.target.closest( '.wopb-layout-preview-wrapper' )
		) {
			setPopUp( '' );
			setSettingActiveTab( '' );
		}
		if ( ! e.target.closest( '.wopb-color-select' ) ) {
			openColorPicker( { key: '', open: false } );
		}
		if ( ! e.target.closest( '.wopb-select-field' ) ) {
			openDropdown( '' );
		}
		if ( ! e.target.closest( '.wopb-layout-preview' ) ) {
			setLayoutPreview( '' );
		}
		if (
			e.target.closest( '.wopb-addon-lock-container-overlay' ) &&
			! e.target.closest( '.wopb-addon-lock-container' )
		) {
			setLockShow( false );
		}
	};

	const handleMouseMove = ( e ) => {
		if (
			! e.target.closest( '.wopb-addon-setting-body' ) &&
			dragElement.dragFrom != null
		) {
			setDragElement( {} );
		}
	};

	const headerClose = () => {
		return (
			<a
				className="wopb-popup-close"
				onClick={ () => {
					setPopUp( '' );
					setSettingActiveTab( '' );
				} }
			/>
		);
	};

	const header = ( addon_key, attr = {} ) => {
		const addon = addons[ addon_key ];
		return (
			<div className="wopb-addon-setting-header">
				<div className="wopb-addon-settings-title">
					<div className="wopb-heading">{ addon.name } Settings</div>
				</div>
				{ attr.tab && attr.tab.options && headerTab( addon, attr ) }
				<div className="wopb-close-section">
					<span className="wopb-separator" />
					{ headerClose() }
				</div>
			</div>
		);
	};

	const headerTab = ( addon_key, attr ) => {
		const options = attr.tab.options;
		if ( ! settingActiveTab ) {
			setSettingActiveTab( Object.keys( options )[ 0 ] );
		}
		return (
			<div className="wopb-settings-tab">
				{ Object.keys( options )?.map( ( key, index ) => {
					return (
						<a
							className={
								'wopb-tab' +
								( settingActiveTab == key
									? ' wopb-active-tab'
									: '' )
							}
							onClick={ () => setSettingActiveTab( key ) }
							key={ index }
						>
							{ __( options[ key ].label ) }
						</a>
					);
				} ) }
			</div>
		);
	};

	const footer = () => {
		return (
			<div className="wopb-setting-footer">
				<button
					type="submit"
					className="cursor wopb-form-save-btn wopb-primary-button"
				>
					{ __( 'Save Settings', 'product-blocks' ) }
					{ saveLoading && <span className="wopb-loading" /> }
				</button>
			</div>
		);
	};

	let count = 0;
	const getAddons = ( data ) => {
		return (
			<div className="wopb-addons-grid">
				{ data.map( ( addon, index ) => {
					count++;

					let isChecked = true;
					isChecked =
						settings[ addon.key ] == 'true' ||
						settings[ addon.key ] == true
							? addon.is_pro && ! wopb_dashboard_pannel.active
								? false
								: true
							: false;
					params.addonKey = addon.key;

					return (
						<div
							className="wopb-addon-item wopb-dash-item-con"
							key={ index }
						>
							<div className="wopb-addon-item-contents">
								<div className="wopb-addon-item-name">
									<div className="wopb-addon-heading">
										{ addon.name }
									</div>
								</div>
								<div className="wopb-description">
									{ addon.desc }
								</div>
								{ addon.required && addon.required?.name && (
									<span className="wopb-plugin-required">
										{ ' ' }
										{ __(
											'This addon required this plugin:',
											'product-blocks'
										) }
										{ addon.required.name }
									</span>
								) }
								{ addon.is_pro &&
									! wopb_dashboard_pannel.active && (
										<span
											/*onClick={()=> {setLockShow(true)}}*/ className="wopb-pro-lock"
										>
											{ __( 'PRO', 'product-blocks' ) }
										</span>
									) }
							</div>
							<div className="wopb-addon-item-actions wopb-dash-control-options">
								<span className="wopb-control-switch">
									<input
										type="checkbox"
										datatype={ addon.key }
										className={
											'wopb-addons-enable ' +
											( addon.is_pro &&
											! wopb_dashboard_pannel.active
												? 'disabled'
												: '' )
										}
										id={ addon.key }
										checked={ isChecked }
										onChange={ () => {
											changeAddonStatus( addon.key );
										} }
									/>
									<label
										htmlFor={ addon.key }
										className="wopb-control__label"
									>
										{ addon.is_pro &&
											! wopb_dashboard_pannel.active && (
												<span className="dashicons dashicons-lock"></span>
											) }
									</label>
									<span className="wopb-control-switch-text">
										{ __( 'Enable', 'product-blocks' ) }
									</span>
								</span>
								<div style={ { display: 'flex' } }>
									<div className="wopb-docs-action">
										{ addon.live && (
											<a
												href={ addon.live.replace(
													'live_demo_args',
													`?utm_source=db-wstore-addons&utm_medium=${ addon.key }-demo&utm_campaign=wstore-dashboard`
												) }
												className="wopb-option-tooltip"
												target="_blank"
												rel="noreferrer"
											>
												<div className="dashicons dashicons-external"></div>
												{ __(
													'Demo',
													'product-blocks'
												) }
											</a>
										) }
										{ addon.docs && (
											<a
												href={ addon.docs.replace(
													'addon_doc_args',
													`?utm_source=db-wstore-addons&utm_medium=docs&utm_campaign=wstore-dashboard`
												) }
												className="wopb-option-tooltip"
												target="_blank"
												rel="noreferrer"
											>
												<div className="dashicons dashicons-media-document"></div>
												{ __(
													'Docs',
													'product-blocks'
												) }
											</a>
										) }
										{ popUp == addon.key && (
											<div className="wopb-addon-settings">
												<div className="wopb-addon-settings-popup">
													{ addons_settings[
														addon.key
													] && (
														<>
															{ addon.key ==
															'wopb_currency_switcher' ? (
																<CurrencySwitcherSettings
																	handleSwictherFormSubmit={
																		handleFormSubmit
																	}
																	attr={
																		addons_settings[
																			addon
																				.key
																		].attr
																	}
																	addonKey={
																		addon.key
																	}
																	settings={
																		settings
																	}
																	header={ header(
																		addon.key,
																		addons_settings[
																			addon
																				.key
																		].attr
																	) }
																	footer={ footer() }
																/>
															) : (
																<form
																	onSubmit={ (
																		e
																	) =>
																		handleFormSubmit(
																			e,
																			'',
																			addon.key
																		)
																	}
																	action=""
																>
																	{ header(
																		addon.key,
																		addons_settings[
																			addon
																				.key
																		].attr
																	) }
																	<div className="wopb-addon-setting-body">
																		{ getSettingsData(
																			addons_settings[
																				addon
																					.key
																			]
																				.attr,
																			settings,
																			params
																		) }
																		<div className="wopb-data-message" />
																	</div>
																	{ footer() }
																</form>
															) }
														</>
													) }
												</div>
											</div>
										) }
									</div>
									{ ( addons_settings[ addon.key ] ||
										( addon.is_pro &&
											! addons_settings[
												addon.key
											] ) ) && (
										<span
											className="wopb-popup-setting dashicons dashicons-admin-generic"
											onClick={ () => {
												if (
													addon.is_pro &&
													! addons_settings[
														addon.key
													]
												) {
													setSettings( {
														...settings,
														[ addon.key ]: 'false',
													} );
													setLockShow( true );
												} else {
													const defaultData =
														defaultArray(
															{},
															addons_settings[
																addon.key
															],
															addon.key
														);
													setPopUp( addon.key );
													setFormData( {
														...defaultData,
														...formData,
													} );
												}
											} }
										/>
									) }
								</div>
							</div>
						</div>
					);
				} ) }
			</div>
		);
	};

	const bannerPopup = () => {
		return (
			<iframe
				width="1100"
				height="500"
				src="https://www.youtube.com/embed/6t9JSvAR-to?autoplay=1"
				title={ __(
					'How to add Product Filter to WooCommerce Shop Page',
					'product-blocks'
				) }
				allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
				allowFullScreen
			/>
		);
	};

	const _finalData = addonData();

	const startProList = [
		'A Wide Range of PRO Page Templates',
		'Addons to Boost Sales!',
		'Advance Customization Features',
		'Improve Conversions',
		'Reduction in Cart Abandonment',
		'A lot more...',
	];

	return (
		<>
			<div className="wopb-dashboard-addons-container">
				{ Object.keys( settings ).length > 0 ? (
					<>
						{ lockShow &&
							getProHtml( {
								tags: 'menu_addons_popup',
								func: ( val ) => {
									setLockShow( val );
								},
								data: {
									icon: 'lock.svg',
									title: __(
										'Unlock with WowStore Pro…',
										'product-blocks'
									),
									description: __(
										'Unfortunately, you need the PRO version of WowStore to use this feature. Unlock all Addons, templates, and features with WowStore PRO.',
										'product-blocks'
									),
								},
							} ) }
						<div className="wopb-gettingstart-message">
							<div className="wopb-start-left">
								<img
									src={
										wopb_dashboard_pannel.url +
										'assets/img/dashboard_banner.jpg'
									}
									alt="Banner"
								/>
								<div className="wopb-start-content">
									<span className="wopb-start-text">
										{ __(
											'Enjoy Pro-level Ready Templates!',
											'product-blocks'
										) }
									</span>
									<div className="wopb-start-btns">
										{ getPrimaryButton(
											'https://www.wpxpo.com/wowstore/templates/?utm_source=db-wstore-started&utm_medium=templates&utm_campaign=wstore-dashboard',
											'',
											__(
												'Go to Templates Library',
												'product-blocks'
											),
											''
										) }
										{ getSecondaryButton(
											'https://www.wpxpo.com/wowstore/?utm_source=db-wstore-started&utm_medium=details&utm_campaign=wstore-dashboard',
											'',
											__(
												'Plugin Details',
												'product-blocks'
											),
											''
										) }
									</div>
								</div>
							</div>
							<div className="wopb-start-right">
								<div
									className="wopb-dashborad-banner"
									style={ { cursor: 'pointer' } }
									onClick={ () =>
										setModalContent( bannerPopup() )
									}
								>
									<img
										src={
											wopb_dashboard_pannel.url +
											'assets/img/dashboard_right_banner.jpg'
										}
										className="wopb-banner-img"
									/>
									<div className="wopb-play-icon-container">
										{ /*{icons.play}*/ }
										<img
											className="wopb-play-icon"
											src={
												wopb_dashboard_pannel.url +
												'/assets/img/play.png'
											}
											alt={ __(
												'Play',
												'product-blocks'
											) }
										/>
										<span className="wopb-animate"></span>
									</div>
								</div>
								<div className="wopb-dashboard-content">
									<div>
										<div className="wopb-title">
											{ ! wopb_dashboard_pannel.active ? (
												<>
													{ __(
														'Do More with ',
														'product-blocks'
													) }
													<span
														style={ {
															color: '#FF176B',
														} }
													>
														{ __(
															'PRO:',
															'product-blocks'
														) }
													</span>
												</>
											) : (
												__(
													'What Do You Need?',
													'product-blocks'
												)
											) }
										</div>
										{ ! wopb_dashboard_pannel.active ? (
											<>
												<div className="wopb-desc">
													{ __(
														'With the Pro features, you unlock access to:',
														'product-blocks'
													) }
												</div>
												<div className="wopb-lists">
													{ startProList.map(
														( content, index ) => {
															return (
																<span
																	className="wopb-list"
																	key={
																		index
																	}
																>
																	{
																		icons.rightMark
																	}{ ' ' }
																	{ content }
																</span>
															);
														}
													) }
												</div>
												<a
													href={
														'https://www.wpxpo.com/wowstore/pricing/?utm_source=db-wstore-started&utm_medium=upgrade-pro-hero&utm_campaign=wstore-dashboard'
													}
													target="_blank"
													className="wopb-upgrade-btn"
													rel="noreferrer"
												>
													{ __(
														'Upgrade to Pro',
														'product-blocks'
													) }
													{ icons.rocketPro }
												</a>
											</>
										) : (
											<>
												<div
													style={ {
														marginBottom: '32px',
														fontSize: '14px',
													} }
												>
													{ __(
														'Do you have something in mind you want to share? Both we and our users would like to hear about it. Share your ideas on our Facebook group and let us know what you need.',
														'product-blocks'
													) }
												</div>
												<a
													href="https://www.facebook.com/groups/wowcommercecommunity/"
													target="_blank"
													className="wopb-upgrade-btn"
													rel="noreferrer"
												>
													{ __(
														'Share Ideas',
														'product-blocks'
													) }
												</a>
											</>
										) }
									</div>
								</div>
							</div>
						</div>
						<div>
							{ Object.keys( _finalData ).map( ( val, k ) => {
								return (
									<span key={ k }>
										{ _finalData[ val ].data.length > 0 && (
											<>
												<div className="wopb-addon-parent-heading mt">
													{ _finalData[ val ].name }
												</div>
												{ getAddons(
													_finalData[ val ].data
												) }
											</>
										) }
									</span>
								);
							} ) }
						</div>
						{ layoutPreview && (
							<div className="wopb-layout-preview-wrapper">
								<div className="wopb-layout-preview">
									<a
										className="wopb-popup-close"
										onClick={ () => setLayoutPreview( '' ) }
									>
										{ IconPack.close_line }
									</a>
									<img src={ layoutPreview } />
								</div>
							</div>
						) }
					</>
				) : (
					<div className="skeletonOverflow">
						<div>
							<div className="wopb-addon-parent-heading">
								<Skeleton type="button" size="200" />
							</div>
							<div className="wopb-addons-grid">
								{ Array( 6 )
									.fill( 1 )
									.map( ( val, k ) => {
										return (
											<div
												key={ k }
												className="wopb-addon-item wopb-dash-item-con"
											>
												<div className="wopb-addon-item-contents">
													<div
														className="wopb-addon-item-name"
														style={ {
															display:
																'inline-block',
														} }
													>
														{ /*<Skeleton type="custom_size" c_s={{ size1: 50, unit1: 'px',  size2: 50, unit2: 'px', br: 6 }}/>*/ }
														<Skeleton
															type="custom_size"
															c_s={ {
																size1: 190,
																unit1: 'px',
																size2: 28,
																unit2: 'px',
																br: 4,
															} }
														/>
													</div>
													<div className="wopb-description">
														<Skeleton
															type="custom_size"
															loop={ 2 }
															classes="loop"
															c_s={ {
																size1: 100,
																unit1: '%',
																size2: 14,
																unit2: 'px',
																br: 2,
															} }
														/>
														<Skeleton
															type="custom_size"
															classes="loop"
															c_s={ {
																size1: 70,
																unit1: '%',
																size2: 14,
																unit2: 'px',
																br: 2,
																display:
																	'inline-block',
															} }
														/>
													</div>
												</div>
												<div
													className="wopb-addon-item-actions wopb-dash-control-options"
													style={ {
														alignItems: 'center',
														padding: '12px 15px',
													} }
												>
													<Skeleton
														type="custom_size"
														c_s={ {
															size1: 40,
															unit1: 'px',
															size2: 20,
															unit2: 'px',
															br: 8,
														} }
													/>
													<div className="wopb-docs-action">
														<Skeleton
															type="custom_size"
															loop={ 3 }
															c_s={ {
																size1: 60,
																unit1: 'px',
																size2: 22,
																unit2: 'px',
																br: 2,
															} }
														/>
													</div>
												</div>
											</div>
										);
									} ) }
							</div>
						</div>
					</div>
				) }
				{ toastMessages.state && (
					<Toast
						delay={ 2000 }
						toastMessages={ toastMessages }
						setToastMessages={ setToastMessages }
					/>
				) }
			</div>
			{ modalContent && (
				<Modal
					title={ __( 'Start WowStore', 'product-blocks' ) }
					modalContent={ modalContent }
					setModalContent={ setModalContent }
				/>
			) }
		</>
	);
};

export default Addons;
