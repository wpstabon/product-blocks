const { __ } = wp.i18n;
import React, { useState } from 'react';
import Toast from '../utility/Toast';
import './support.scss';
import IconPack from '../../helper/fields/tools/IconPack';
import { getSecondaryButton } from '../Settings';

const Support = ( props ) => {
	const [ name, setName ] = useState( '' );
	const [ email, setEmail ] = useState( '' );
	const [ subject, setSubject ] = useState( '' );
	const [ desc, setDesc ] = useState( '' );
	const [ loading, setLoading ] = useState( false );

	const [ type, setType ] = useState( '' );
	const [ showForm, setShowForm ] = useState( false );
	const [ contactLink, setContactLink ] = useState( false );
	const [ toastMessages, setToastMessages ] = useState( {
		state: false,
		status: '',
	} );

	const fieldData = [
		{ value: 'Technical Support', pro: true },
		{ value: 'Free Support (WordPress ORG)' },
		{ value: 'Presale Questions' },
		{ value: 'License Activation Issues' },
		{ value: 'Bug Report' },
		{ value: 'Compatibility Issues' },
		{ value: 'Feature Request' },
	];
	const supportLink = [
		{
			label: 'WowStore Pro Installation',
			pro: true,
			link: 'https://wpxpo.com/docs/wowstore/getting-started/pro-version-installation/',
		},
		{
			label: 'WooCommerce Builder',
			link: 'https://wpxpo.com/docs/wowstore/woo-builder/',
		},
		{
			label: 'How to Use WowStore With Elementor Builder',
			link: 'https://wpxpo.com/docs/wowstore/add-ons/elementor-addon/',
		},
		{
			label: 'How to Use WowStore With Divi Builder',
			link: 'https://wpxpo.com/docs/wowstore/add-ons/divi-addon/',
		},
		{
			label: 'How to Use WowStore With Oxygen Builder',
			link: 'https://wpxpo.com/docs/wowstore/add-ons/oxygen-builder-addon/',
		},
		{
			label: 'How to Use WowStore With Beaver Builder',
			link: 'https://wpxpo.com/docs/wowstore/add-ons/beaver-builder-addon/',
		},
	];

	const fetchData = ( action = 'support_data' ) => {
		if ( action == 'support_action' ) {
			let error = name && subject && desc ? false : true;
			error = error ? error : /\S+@\S+\.\S+/.test( email ) ? false : true;

			if ( error || ! type ) {
				setToastMessages( {
					status: 'error',
					messages: [
						! type
							? __(
									'Please Select Support type.',
									'product-blocks'
							  )
							: __(
									'Please Fill all the Input Field.',
									'product-blocks'
							  ),
					],
					state: true,
				} );
				return;
			}
			setLoading( true );
		}

		const actionObj =
			action == 'support_data'
				? { type: action }
				: {
						type: action,
						name,
						email,
						subject: '[WowStore- ' + type + '] ' + subject,
						desc,
				  };

		wp.apiFetch( {
			path: '/wopb/v2/dashborad',
			method: 'POST',
			data: actionObj,
		} ).then( ( res ) => {
			if ( res.success ) {
				if ( action == 'support_data' ) {
					setName( res.data.name );
					setEmail( res.data.email );
				} else {
					setSubject( '' );
					setDesc( '' );
				}
			} else {
				setSubject( '' );
				setDesc( '' );
				setContactLink( true );
			}
			if ( res.message ) {
				setToastMessages( {
					status: res.success ? 'success' : 'error',
					messages: [ res.message ],
					state: true,
				} );
			}
			setLoading( false );
		} );
	};

	return (
		<div className="wopb-dashboard-general-settings-container wopb-dashboard-support">
			<div className="wopb-general-settings wopb-dash-item-con">
				<div className="wopb-support">
					{ toastMessages.state && (
						<Toast
							delay={ 2000 }
							toastMessages={ toastMessages }
							setToastMessages={ setToastMessages }
						/>
					) }
					<div className="wopb-support-heading">
						{ __(
							'Having Difficulties? We are here to help',
							'product-blocks'
						) }
					</div>
					<div className="wopb-support-sub-heading">
						{ __(
							'Let us know how we can help you.',
							'product-blocks'
						) }
					</div>

					<div className="wopb-support-options">
						{ fieldData.map( ( data, key ) => {
							return (
								( data.value !=
									'Free Support (WordPress ORG)' ||
									( data.value ==
										'Free Support (WordPress ORG)' &&
										! wopb_dashboard_pannel.active ) ) && (
									<div
										className="wopb-support-option"
										key={ key }
									>
										<input
											type="radio"
											id={ data.value.replace(
												/[()\s]/g,
												''
											) }
											name="type"
											value={ data.value }
											onClick={ () => {
												let openState = false;
												if (
													data.value ==
														'Technical Support' &&
													wopb_dashboard_pannel.active
												) {
													window.open(
														'https://www.wpxpo.com/contact/?utm_source=db-wstore-plugin&utm_medium=quick-support&utm_campaign=wstore-dashboard',
														'_blank'
													);
												} else if (
													data.value ==
														'Technical Support' &&
													! wopb_dashboard_pannel.active
												) {
													window.open(
														'https://www.wpxpo.com/wowstore/pricing/?utm_source=db-wstore-plugin&utm_medium=quick-support&utm_campaign=wstore-dashboard',
														'_blank'
													);
												} else if (
													data.value ==
													'Free Support (WordPress ORG)'
												) {
													window.open(
														'https://wordpress.org/support/plugin/product-blocks/',
														'_blank'
													);
												} else {
													openState = true;
												}

												setShowForm( openState );
												setContactLink( false );
												openState && fetchData();
												openState &&
													setType( data.value );
											} }
										/>
										<label
											htmlFor={ data.value.replace(
												/[()\s]/g,
												''
											) }
										>
											{ data.value }
											{ data.pro &&
												! wopb_dashboard_pannel.active && (
													<a
														target="_blank"
														href="https://www.wpxpo.com/wowstore/pricing/?utm_source=quick_support&utm_medium=pro&utm_campaign=wstore-dashboard"
														rel="noreferrer"
													>
														(Pro)
													</a>
												) }
										</label>
									</div>
								)
							);
						} ) }
					</div>
					{ ! showForm && (
						<div className="wopb-support-create">
							<a
								href="#"
								className={ 'wopb-primary-button' }
								onClick={ ( e ) => {
									e.preventDefault();
									setShowForm( true );
									fetchData();
								} }
							>
								{ __( 'Create a Ticket', 'product-blocks' ) }
							</a>
						</div>
					) }
					{ showForm && (
						<div className="wopb-support-form">
							{ ! type && (
								<div className="wopb-support-type">
									<span className="dashicons dashicons-info-outline"></span>{ ' ' }
									{ __(
										'Select Support Type from above',
										'product-blocks'
									) }
								</div>
							) }
							<div id="wopb-contact">
								<div className="wopb-support-form-double">
									<input
										type="text"
										placeholder="Name"
										required
										defaultValue={ name }
										onChange={ ( e ) =>
											setName( e.target.value )
										}
									/>
									<input
										type="email"
										placeholder="Email"
										required
										defaultValue={ email }
										onChange={ ( e ) =>
											setEmail( e.target.value )
										}
									/>
								</div>
								<input
									type="text"
									placeholder="Subject"
									required
									value={ subject }
									onChange={ ( e ) =>
										setSubject( e.target.value )
									}
								/>
								<textarea
									placeholder="Description"
									required
									value={ desc }
									onChange={ ( e ) =>
										setDesc( e.target.value )
									}
								/>
							</div>
							{ contactLink ? (
								<div>
									{ __(
										'You can Contact in Support via our',
										'product-blocks'
									) }
									<a
										className=""
										target="_blank"
										href="https://www.wpxpo.com/contact/?utm_source=quick_support&utm_medium=tech_support&utm_campaign=wstore-dashboard"
										rel="noreferrer"
									>
										{ ' ' }
										{ __(
											'Contact Form',
											'product-blocks'
										) }
									</a>
								</div>
							) : (
								<a
									href="#"
									className={ 'wopb-primary-button' }
									onClick={ ( e ) => {
										e.preventDefault();
										fetchData( 'support_action' );
									} }
								>
									{ __( 'Submit Ticket', 'product-blocks' ) }
									{ loading && (
										<span className="dashicons loadericon dashicons-admin-generic" />
									) }
								</a>
							) }
							{ toastMessages.status && (
								<>
									<br />
									<br />
									<div
										className={
											'wopb-support-type ' +
											( toastMessages.status == 'success'
												? 'success'
												: '' )
										}
									>
										<span className="dashicons dashicons-info-outline"></span>{ ' ' }
										{ toastMessages.messages }
									</div>
								</>
							) }
						</div>
					) }
				</div>
			</div>

			<div className="wopb-general-settings-content-right wopb-support-lists">
				<div className="wopb-dashboard-promotions-features">
					<div className="wopb-dashboard-pro-features wopb-dash-item-con">
						<h5 className="wopb-heading">
							{ __( 'Useful Guides', 'product-blocks' ) }
						</h5>
						<span className="wopb-description">
							{ __(
								'Explore in-depth documentations.',
								'product-blocks'
							) }
						</span>
						<div className="wopb-pro-feature-lists">
							{ supportLink.map( ( data, key ) => {
								return (
									( ! data.pro ||
										( data.pro &&
											! wopb_dashboard_pannel.active ) ) && (
										<div key={ key }>
											{ IconPack.dot_solid }
											<span>
												<a
													target="_blank"
													className="wopb-item-label"
													href={ data.link }
													rel="noreferrer"
												>
													{ data.label }
												</a>
											</span>
										</div>
									)
								);
							} ) }
						</div>
						{ getSecondaryButton(
							'https://wpxpo.com/docs/wowstore/?utm_source=quick_support&utm_medium=docs&utm_campaign=wstore-dashboard',
							'',
							'Explore Docs',
							''
						) }
					</div>
					<div className="wopb-dashboard-web-community wopb-dash-item-con">
						<h5 className="wopb-heading">
							{ __( 'WowStore Community', 'product-blocks' ) }
						</h5>
						<span className="wopb-description">
							{ __(
								'Join the Facebook community of WowStore to stay up-to-date and share your thoughts and feedback.',
								'product-blocks'
							) }
						</span>
						{ getSecondaryButton(
							'https://www.facebook.com/groups/wowcommercecommunity',
							'',
							__( 'Join Community', 'product-blocks' ),
							''
						) }
					</div>
				</div>
			</div>
		</div>
	);
};

export default Support;
