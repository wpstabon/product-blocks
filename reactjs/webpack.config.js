'use strict';

const path = require( 'path' );
const TerserPlugin = require( 'terser-webpack-plugin' );
const mode = process.env.NODE_ENV || 'development';

const config = {
	mode,
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: { loader: 'babel-loader' },
			},
			{
				test: /\.(s(a|c)ss)$/,
				use: [ 'style-loader', 'css-loader', 'sass-loader' ],
			},
		],
	},
	optimization:
		mode === 'production'
			? {
					minimize: true,
					concatenateModules: false,
					minimizer: [
						new TerserPlugin( {
							parallel: true,
							terserOptions: {
								output: {
									comments: /translators:/i,
								},
								compress: {
									passes: 2,
								},
								mangle: {
									reserved: [ '__', '_n', '_nx', '_x' ],
								},
							},
							extractComments: false,
						} ),
					],
			  }
			: {},
};
var allBlocks = Object.assign( {}, config, {
	entry: {
		'./assets/js/editor.blocks.min': './src/index.js',
	},
	output: {
		path: path.join( __dirname, '../' ),
		filename: '[name].js',
	},
} );

var builderCondition = Object.assign( {}, config, {
	entry: {
		'./addons/builder/assets/js/conditions.min':
			'./src/conditions/index.js',
	},
	output: {
		path: path.join( __dirname, '../' ),
		filename: '[name].js',
	},
} );

var setupWizard = Object.assign( {}, config, {
	entry: {
		'./assets/js/setup.wizard': './src/initial_setup/index.js',
	},
	output: {
		path: path.join( __dirname, '../' ),
		filename: '[name].js',
	},
} );

var wopbDashboard = Object.assign( {}, config, {
	entry: {
		'./assets/js/wopb_dashboard_min': './src/dashboard/index.js',
	},
	output: {
		path: path.join( __dirname, '../' ),
		filename: '[name].js',
	},
} );
module.exports = [ allBlocks, builderCondition, setupWizard, wopbDashboard ];
