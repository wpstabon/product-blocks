'use strict';

const path = require( 'path' );
const CopyWebpackPlugin = require( 'copy-webpack-plugin' );
const del = require( 'del' );
const archiver = require( 'archiver' );
const fs = require( 'fs' );
const packageJson = require( './package.json' );
const TerserPlugin = require("terser-webpack-plugin");

const folderName = 'product-blocks';
const zipName = `WowStore-v${ packageJson.version }.zip`;

module.exports = {
	mode: 'production',
	output: {
		path: path.resolve( __dirname, '../build' ),
		filename: '../build/dist/zip.js',
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: { loader: 'babel-loader' },
			},
			{
				test: /\.(s(a|c)ss)$/,
				use: [ 'style-loader', 'css-loader', 'sass-loader' ],
			},
		],
	},
	optimization: {
		minimize: true,
		concatenateModules: false,
		minimizer: [
			new TerserPlugin( {
				parallel: true,
				terserOptions: {
					output: {
						comments: /translators:/i,
					},
					compress: {
						passes: 2,
					},
					mangle: {
						reserved: [ '__', '_n', '_nx', '_x' ],
					},
				},
				extractComments: false,
			} ),
		],
	},
	plugins: [
		//Clean the build folder before copy
		{
			apply( compiler ) {
				compiler.hooks.beforeRun.tapAsync(
					'CleanBuildFolder',
					( compilation, callback ) => {
						del( [ path.resolve( __dirname, `../build/*` ) ], {
							force: true,
						} ).then( () => {
							callback();
						} );
					}
				);
			},
		},

		// Copy project files to build folder
		new CopyWebpackPlugin( {
			patterns: [
				{
					from: path.resolve( __dirname, '../' ),
					to: path.resolve( __dirname, `../build/${ folderName }` ),
					globOptions: {
						ignore: [
							'**/addons/builder/assets/js/conditions.min.js.LICENSE.txt',
							'**/addons/builder/assets/js/conditions.js.LICENSE.txt',
							'**/assets/js/editor.blocks.min.js.LICENSE.txt',
							'**/assets/js/editor.blocks.js.LICENSE.txt',
							'**/assets/js/setup.wizard.js.LICENSE.txt',
							'**/assets/js/wopb_dashboard_min.js.LICENSE.txt',

							'**/addons/builder/assets/js/conditions.js',
							'**/assets/js/editor.blocks.js',

							'**/.git/**',
							'**/.idea/**',
							'**/.vscode/**',

							'**/phpcs.xml',
							'**/reactjs/**',
							'**/node_modules/**',
							'**/build/**',

							'**/composer.json',
							'**/composer.lock',
							'**/phpcs.xml.dist',
							'**/phpunit.xml',
							'**/*.xml',
							'**/.gitignore',
							'**/.gitmodules',
							'**/changelog.txt',
						],
					},
				},
			],
		} ),

		// Create a zip file after copied file
		{
			apply( compiler ) {
				compiler.hooks.done.tapAsync(
					'CreateZip',
					( stats, callback ) => {
						const outputPath = path.resolve(
							__dirname,
							'../build',
							folderName
						);
						const outputZipPath = path.resolve(
							__dirname,
							'../build',
							zipName
						);

						const archive = archiver( 'zip', {
							zlib: { level: 9 },
						} );

						// Create the zip file first
						const output = fs.createWriteStream( outputZipPath );
						archive.pipe( output );
						archive.directory( outputPath, folderName );
						archive.finalize();

						// After creating the zip, delete the folder
						output.on( 'close', () => {
							// After the zip process has finished, delete the folder
							del(
								[
									path.resolve(
										__dirname,
										`../build/${ folderName }`
									),
								],
								{ force: true }
							)
								.then( () => {
									callback(); // Proceed after deletion
								} )
								.catch( ( err ) => {
									callback( err ); // Handle error during deletion
								} );
						} );
					}
				);
			},
		},
	],
};
