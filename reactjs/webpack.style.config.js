const path = require( 'path' );
const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' );
const CssMinimizerPlugin = require( 'css-minimizer-webpack-plugin' );
const glob = require( 'glob' );

//Style fiels config
const paths = {
	styles: [
		{
			name: 'modal',
			src: './src/addons/scss/modal.scss',
			dest: '../assets/css',
			file: 'modal.min.css',
		},
		{
			name: 'animation',
			src: './src/addons/scss/animation.scss',
			dest: '../assets/css',
			file: 'animation.min.css',
		},
		{
			name: 'quick_view',
			src: './src/addons/scss/quick_view.scss',
			dest: '../addons/quick_view/css',
			file: 'quick_view.min.css',
		},
		{
			name: 'compare',
			src: './src/addons/scss/compare.scss',
			dest: '../addons/compare/css',
			file: 'compare.min.css',
		},
		{
			name: 'flip_image',
			src: './src/addons/scss/flip_image.scss',
			dest: '../addons/flip_image/css',
			file: 'flip_image.min.css',
		},

		{
			name: 'block',
			src: './src/**/style.scss',
			dest: '../assets/css',
			file: 'blocks.style.css',
		},

		{
			name: 'editor',
			src: './src/**/editor.scss',
			dest: '../assets/css',
			file: 'blocks.editor.css',
		},
	],
};

// Create an entry object dynamically based on paths.styles
const entries = paths.styles.reduce( ( entries, entry ) => {
	if ( entry.name === 'block' || entry.name === 'editor' ) {
		const matchedFiles = glob.sync( entry.src ).map( ( file ) => {
			const resolvedPath = path.resolve( __dirname, file );
			return resolvedPath;
		} );

		if ( matchedFiles.length > 0 ) {
			entries[ entry.name ] = matchedFiles;
		}
	} else {
		entries[ entry.name ] = entry.src;
	}

	return entries;
}, {} );

module.exports = {
	entry: entries,
	output: {
		path: path.resolve( __dirname ),
		filename: `../build/dist/[name].js`,
	},
	resolve: {
		// Add the src directory to module resolution
		modules: [ path.resolve( __dirname, 'src' ), 'node_modules' ],
		extensions: [ '.js', '.scss' ], // Support SCSS files
	},
	module: {
		rules: [
			{
				test: /\.scss$/,
				use: [
					MiniCssExtractPlugin.loader, // extracts CSS into separate files
					{
						loader: 'css-loader',
						options: {
							url: false, // Disable loading fonts during SCSS processing
						},
					},
					'postcss-loader',
					'sass-loader',
				],
			},
		],
	},
	plugins: [
		new MiniCssExtractPlugin( {
			filename: ( pathData ) => {
				const style = paths.styles.find(
					( s ) => s.name === pathData.chunk.name
				);
				if ( style ) {
					return path.join( style.dest, style.file );
				}
				return '[name].min.css'; // Default fallback
			},
		} ),
	],
	optimization: {
		minimize: true, // Minify CSS
		minimizer: [
			new CssMinimizerPlugin({
				minimizerOptions: {
					preset: [
						'default',
						{
							cssDeclarationSorter: false,
							mergeLonghand: false,
						},
					],
				},
			}),
		],
	},
	performance: {
		hints: false, // Disable performance hints
	},
};
